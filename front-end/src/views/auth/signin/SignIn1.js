import React from "react";
import { Card } from "react-bootstrap";
import { NavLink } from 'react-router-dom';

import logoDark from '../../../assets/images/logo-dark.png';
import Breadcrumb from '../../../layouts/AdminLayout/Breadcrumb';

//import FirebaseLogin from './FirebaseLogin';
//import JWTLogin from './JWTLogin';
import Auth0Login from './Auth0Login';

const Signin1 = () => {
    return (
        <React.Fragment>
            <Breadcrumb/>
            <div className="auth-wrapper">
                <div className="auth-content text-center">
                    <Card className="borderless">
                        <Card.Body>
                            <img src={logoDark} alt="" className="img-fluid mb-4" />
                            <Auth0Login />
                            <hr/>
                            <p className="mb-2 text-muted">Forgot password? <NavLink to="/auth/reset-password-1" className="f-w-400">Reset</NavLink></p>
                            <p className="mb-0 text-muted">Don’t have an account? <NavLink to="/auth/signup-1" className="f-w-400">Signup</NavLink></p>
                        </Card.Body>
                    </Card>
                </div>
            </div>
        </React.Fragment>
    );
}

export default Signin1;