# 프로젝트 셋팅 및 빌드 방법 

## 프로젝트 구조 설명
1. src/main/java/packagename.config  
스프링부트 프로젝트 커스텀 설정 및 외부 라이브러리 설정

2. src/main/java/packagename.dataloader  
초기 프로젝트 실행시 데이터베이스에 입력되어야할 데이터 입력 로직 

3. src/main/java/packagename.domain
도메인 엔티티 클리스 패키지  
도메인 엔티티(테이블정의클래스)를 모아두는 패키지 입니다.

4. src/main/java/packagename.property
스프링부트 properties 에 정의된 리터럴 클래스를 모아두는 패키지 입니다.     

5. src/main/java/packagename.service
비즈니스 로직을 구현한 클래스들을 모아두는 패키지 입니다.  
모든 비즈니스 로직은 이 패키지내에 존재 합니다.  

6. src/main/java/packagename.utils
프로젝트 내에 유틸 기능 관련된 클래스들을 모아두는 패키지 입니다.  

7. src/main/java/packagename.vo  
Value Object 클래스를 모아두는 패키지 입니다.   
RestAPI 의 Request, Response 모델 및 프로젝트 내부에서 사용하는 VO 들이 있습니다.  

8. src/main/java/packagename.web
스프링의 Controller 및 RestController, ControllerAdvice 들을 모아두는 패키지입니다.  
모든 endpoints 는 여기에서 확인 하시면 됩니다.  
- admin 패키지: 관리자페이지의 endpoints  
- api 패키지: RestAPI 방식의 endpoints  
- view 패키지: freemarker 서버렌더 방식의 endpoints  

9. src/main/resources/static
웹서빙시 제공될 static 한 파일들의 폴더 입니다.  
- ad: 관리자페이지의 static 파일  
- assets 폴더 및 이하 파일들: 서비스페이지에의 static 파일  

10. src/main/resources/templates  
서버렌더시 사용되는 freemarker template 파일들 입니다.  

11. src/main/resources 이하 *.properties/*.yml 파일  
스프링 auto-configuration 설정 파일들 입니다.  

12. build.gradle  
프로젝트의 gradle 설정 파일입니다.  

## 개발환경
1. 개발언어: Java (JDK v1.8)
2. 빌드도구: Gradle v6.2.x
3. Framework: SpringBoot v2.2.10.RELEASE
4. 관리자페이지 프론트-엔드 환경:
   - NodeJS v6.11.1 (npm v3.10.10)
   - bower v1.8.8
   - jQuery v2.x
   - Bootstrap v3.x (UI Framework)

## 셋팅  

## Back-end   

1. Spring Boot 실행  
```
$ gradle bootRun
```  

2. Spring Boot 빌드  
```
$ gradle clea build
```  

## Front-end  
### 1. Tool setup
``` 

// install bower (global)
$ npm install -g bower

// install gulp (global)
$ npm install -g gulp
```

### 2. 관리자페이지 설정  

Build Setup  
root path : 'src\main\resources\static\ad\master'
  
```
// install dependencies
$ bower install
$ npm install

// package bundle(js, css)
// gulpfile.js 에서 task 확인.
$ gulp default
```  