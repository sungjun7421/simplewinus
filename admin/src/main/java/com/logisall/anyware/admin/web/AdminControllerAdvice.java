package com.logisall.anyware.admin.web;

import com.logisall.anyware.admin.config.property.meta.AdminMetaPlugin;
import com.logisall.anyware.admin.config.property.sidebar.Section;
import com.logisall.anyware.admin.config.property.sidebar.Sidebar;
import com.logisall.anyware.admin.config.property.sidebar.SubSection;
import com.logisall.anyware.core.domain.International.InternationalMode;
import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.setting.ComponentMode;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.service.setting.AppSettingService;
import com.logisall.anyware.core.utils.StringUtils;
import com.logisall.anyware.core.config.security.CurrentUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Slf4j
@ControllerAdvice(basePackages = {"com.logisall.anyware.admin.web"})
public class AdminControllerAdvice {

  @Autowired
  private Sidebar sidebar;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private AdminMetaPlugin adminMetaPlugin;

  @Autowired
  private AppSettingService settingService;

  @Value("${spring.application.name}")
  private String appName;

  @ModelAttribute("appName")
  public String appName() {
    log.debug("appName ::: {}", appName);
    return this.appName;
  }

  @ModelAttribute("currentRole")
  public String getRole(@AuthenticationPrincipal CurrentUser currentUser) {
//    log.debug("> role");

    if (currentUser != null) {
      Optional<User> userOpt = userRepository.findById(currentUser.getId());
      if (userOpt.isPresent()) {
        return userOpt.get().getRoleTopLevel().getRole();
      }
    }
    return null;
  }

  // 환경설정
  @ModelAttribute("metaPlugin")
  public AdminMetaPlugin metaPlugin() {
    return adminMetaPlugin;
  }

  // 회원등급 노출여부

  @ModelAttribute("setting")
  public AppSetting setting(HttpServletRequest request) {
//    log.debug("> setting");
//    log.debug(">>> URI ::: {}", request.getRequestURI());
//    log.debug(">>> Method ::: {}", request.getMethod());
    return !request.getMethod().equals("HEAD") ? settingService.getSetting() : new AppSetting();
  }

  // 국제화 모드
  @ModelAttribute("international")
  public boolean international(HttpServletRequest request) {
//    log.debug("> international");
    return this.setting(request) != null && this.setting(request).isInternational();
  }

  // 국제 언어 모드
  @ModelAttribute("im")
  public InternationalMode internationalMode(HttpServletRequest request) {
//    log.debug("> internationalMode");

    AppSetting setting = this.setting(request);

    if (setting != null) {
      if (setting.isInternational()) {
        return setting.getInternationalMode();
      } else if (setting.getDefaultLocale() != null) {
        InternationalMode im = new InternationalMode();
        im.toFalseMode();
        final Locale defaultLocale = setting.getDefaultLocale();
        if (defaultLocale.equals(Locale.KOREA)) {
          im.setKoKr(true);
        }
        if (defaultLocale.equals(Locale.US)) {
          im.setEnUs(true);
        }
        if (defaultLocale.equals(Locale.CHINA)) {
          im.setZhCn(true);
        }
        if (defaultLocale.equals(Locale.TAIWAN)) {
          im.setZhTw(true);
        }
        if (defaultLocale.equals(Locale.JAPAN)) {
          im.setJaJp(true);
        }
//        log.debug("defaultLocale ::: {}", defaultLocale);
//        log.debug("IM ::: {}", im);
        return im;
      }
    }
    return new InternationalMode();
  }

  @ModelAttribute("cm")
  public ComponentMode componentMode() {
    return new ComponentMode();
  }

  @ModelAttribute("sidebar")
  public Sidebar sidebar(HttpServletRequest request,
                         @RequestParam(required = false) OrderStatus checkedOrderStatus) {
    String requestURI = request.getRequestURI();

    if (sidebar != null) {
      if (sidebar.getSections() != null) {


        final String finalOrderStatusParam = checkedOrderStatus != null ? checkedOrderStatus.toString() : "" ;

        sidebar.getSections().forEach(section -> {

          if (section != null && section.getElements() != null) {
            section.getElements().forEach(element -> {

              if (element.getUrl() != null) {
                if (element.getUrl().startsWith("/admin/order")) {
                  // 주문내역 관련 사이드바
                  if (StringUtils.isNotEmpty(finalOrderStatusParam)) {

                    String orderStatusStr = this.getOrderStatusStr(element.getUrl());
                    if (Objects.equals(finalOrderStatusParam, "WAITING")
                            || Objects.equals(finalOrderStatusParam, "COMPLETE")){
//                      || Objects.equals(finalOrderStatusParam, "ORDER_CANCEL,PAYMENT_CANCEL")) {
                      element.setActive(Objects.equals(orderStatusStr, finalOrderStatusParam));
                    } else {
                      element.setActive(requestURI.contains(element.getUrl()));
                    }

                  } else {
                    element.setActive(requestURI.contains(element.getUrl()));
                  }

                } else {
                  element.setActive(requestURI.contains(element.getUrl()));
                }
//                element.setActive(element.getUrl().equals(fullUrl));
              }

              SubSection subSection = element.getSection();
              if (subSection != null && subSection.getElements() != null) {
                subSection.getElements().forEach(selement -> {
                  if (selement.getUrl() != null) {
                    selement.setActive(requestURI.contains(selement.getUrl()));
//                    selement.setActive(selement.getUrl().equals(fullUrl));
                  }
                });
              }
            });
          }
        });
      }
    }
    return sidebar;
  }

  @ModelAttribute("header")
  public Map<String, String> header(HttpServletRequest request) {

//    log.debug("> HEADER");

    Map<String, String> result = new HashMap<>();
    String path = request.getServletPath();
    List<Section> sections = sidebar.getSections();
//    log.debug("sections ::: {}", sections.size());

    if (sections != null) {
      sections.forEach(section -> {

        if (section != null && section.getElements() != null) {
          section.getElements().forEach(element -> {

            if (element != null) {
              if (element.getType().equals("ONE")) {

                if (containsDDLPath(path).contains(element.getUrl())) {
                  result.put("title", element.getTitle());
                  result.put("description", element.getDescription());
                  result.put("url", StringUtils.isEmpty(element.getUrl()) ? null : this.getUrlWithoutParameters(element.getUrl()));
                }
              } else if (element.getType().equals("MULTI")) {
                SubSection subSection = element.getSection();

                if (subSection != null && subSection.getElements() != null) {
                  subSection.getElements().forEach(subElement -> {

                    if (subElement != null) {

                      if (containsDDLPath(path).contains(subElement.getUrl())) {
                        result.put("title", subElement.getTitle());
                        result.put("description", subElement.getDescription());
                        result.put("url", StringUtils.isEmpty(subElement.getUrl()) ? null : this.getUrlWithoutParameters(subElement.getUrl()));
                      }
                    }
                  });
                }
              }
            }

          });
        }
      });
    }

    return result;
  }


  private String getUrlWithoutParameters(String url) {
//    log.debug("url ::: {}", url);
    String result = url.replaceFirst("\\?.*$", "");
//    log.debug("result ::: {}", result);
    return result;
  }

  /**
   * /create , /update 경로는 부로 경로로 변경
   *
   * @param path 경로
   * @return ddl 인 경우 부모 경로
   */
  private String containsDDLPath(String path) {

    String lastPath = "";

    int i = path.lastIndexOf('/');
    if (i > 0) {
      lastPath = path.substring(i);
    }
    return lastPath.equals("/create") || lastPath.equals("/update") ? path.replace(lastPath, "") : path;
  }
  private String getOrderStatusStr(String url) {
    return url.substring(url.indexOf("=") + 1);
  }
}
