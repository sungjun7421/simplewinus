package com.logisall.anyware.admin.web.a_dashbord;

import com.logisall.anyware.admin.service.dashboard.DashboardService;
import com.logisall.anyware.core.config.language.Messages;
import com.logisall.anyware.core.domain.commerce.order.OrderRepository;
import com.logisall.anyware.core.domain.user.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 대시보드
 */
@Slf4j
@Controller
@RequestMapping("admin/dashboard")
public class AdminDashboardController {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private Messages messages;

  @Autowired
  private OrderRepository orderRepository;

  @Autowired
  private DashboardService dashboardService;

  @GetMapping
  public ModelAndView create(Model model,
                             HttpServletRequest httpServletRequest) {

//    long b1 = System.currentTimeMillis();
//    BigDecimal totalSalesAmount = orderRepository.totalCalculate(Arrays.asList(OrderStatus.COMPLETE), null, null, true);
//    model.addAttribute("totalSalesAmount", totalSalesAmount);
//    long a1 = System.currentTimeMillis();
//    executeTime("totalSalesAmount", b1, a1);
//
//    long b4 = System.currentTimeMillis();
//    model.addAttribute("totalOrderCntByDelivery", orderRepository.countByOrderStatus(OrderStatus.V_READY));
//    long a4 = System.currentTimeMillis();
//    executeTime("totalOrderCntByDelivery", b4, a4);
//
//    long b3 = System.currentTimeMillis();
//    model.addAttribute("totalOrderCntByPAYMENT_COMPLETE", orderRepository.countByOrderStatus(OrderStatus.COMPLETE));
//    long a3 = System.currentTimeMillis();
//    executeTime("totalOrderCntByPAYMENT_COMPLETE", b3, a3);
//
//    long b6 = System.currentTimeMillis();
//    model.addAttribute("totalOrderCntByRefund", orderRepository.countByOrderStatus(OrderStatus.CANCEL1));
//    long a6 = System.currentTimeMillis();
//    executeTime("totalOrderCntByRefund", b6, a6);
//
//    long b2 = System.currentTimeMillis();
//    model.addAttribute("totalOrderCount", orderRepository.countAll());
//    long a2 = System.currentTimeMillis();
//    executeTime("totalOrderCount", b2, a2);
//
////    long b7 = System.currentTimeMillis();
////    model.addAttribute("totalStore", storeRepository.countAll());
////    long a7 = System.currentTimeMillis();
////    executeTime("totalStore", b7, a7);
//
//    long b8 = System.currentTimeMillis();
//    model.addAttribute("totalUser", userRepository.countAll());
//    long a8 = System.currentTimeMillis();
//    executeTime("totalUser", b8, a8);
//
//    long b9 = System.currentTimeMillis();
//    model.addAttribute("totalUserLeave", userRepository.countUserLeave());
//    long a9 = System.currentTimeMillis();
//    executeTime("totalUserLeave", b9, a9);
//
////    long b10 = System.currentTimeMillis();
////    model.addAttribute("totalMachine", machineRepository.countAll());
////    long a10 = System.currentTimeMillis();
////    executeTime("totalMachine", b10, a10);
//
//    long b11 = System.currentTimeMillis();
//    model.addAttribute("pageByQna", dashboardService.pageByQna());
//    long a11 = System.currentTimeMillis();
//    executeTime("pageByQna", b11, a11);
//
//    long b12 = System.currentTimeMillis();
//    model.addAttribute("pageByOrder", dashboardService.pageByOrder());
//    long a12 = System.currentTimeMillis();
//    executeTime("pageByOrder", b12, a12);
//
    return new ModelAndView("admin/dashboard/dashboard.ftl");
  }
//
//  private void executeTime(String name, long beforeTime, long afterTime) {
//    long secDiffTime = (afterTime - beforeTime)/* / 1000*/; //두 시간에 차 계산
//    log.debug("executeTime ::: {}, {}", name, secDiffTime);
//  }
}
