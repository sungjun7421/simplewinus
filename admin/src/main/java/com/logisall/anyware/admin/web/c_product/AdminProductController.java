package com.logisall.anyware.admin.web.c_product;

import com.logisall.anyware.core.config.freemarker.PageableModel;
import com.logisall.anyware.core.domain.commerce.category.Category;
import com.logisall.anyware.core.domain.commerce.product.Information;
import com.logisall.anyware.core.domain.commerce.product.Product;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.commerce.category.CategoryService;
import com.logisall.anyware.core.service.commerce.product.ProductService;
import com.logisall.anyware.core.utils.DataBinderUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Slf4j
@Controller
@RequestMapping("/admin/product")
public class AdminProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private CategoryService categoryService;

    @GetMapping
    public ModelAndView page(Model model,
                             @ModelAttribute("setting") AppSetting setting,
                             @PageableDefault(size = 20, sort = {"orderDescending"}, direction = Sort.Direction.DESC) Pageable pageable,
                             @RequestParam(required = false, defaultValue = "") String query,
                             @RequestParam(required = false) String startDate,
                             @RequestParam(required = false) String endDate,
                             @RequestParam(required = false) Long idCategory,
                             @RequestParam(required = false) Boolean active) {

        Filter filter = new Filter(pageable, query, startDate, endDate);
        Page<Product> page = productService.page(setting.getDefaultLocale(), filter, idCategory, active);


        model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
        model.addAttribute("page", pageable.getPageNumber());

        // Search filter
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);
        model.addAttribute("idCategory", idCategory);

        return new ModelAndView("admin/product/list.ftl");
    }

    @GetMapping("/create")
    public ModelAndView create(Model model,
                               @ModelAttribute("setting") AppSetting setting) {

        Product product = new Product();
        product.setDcRate(BigDecimal.valueOf(0));
        product.setActive(true);
        product.setInformation(new Information());

        model.addAttribute("product", product);
        model.addAttribute("categories", categoryService.list(setting.getDefaultLocale()));

        return new ModelAndView("admin/product/create.ftl");
    }

    @GetMapping("update/{id}")
    public ModelAndView update(@PathVariable Long id,
                               @ModelAttribute("setting") AppSetting setting,
                               Model model) {

        Product product = productService.get(setting.getDefaultLocale(), id);

        List<Category> categories = categoryService.list(setting.getDefaultLocale());

        // Category Checked
        if (product.getCategories() != null && product.getCategories().size() > 0) {
            categories.forEach(category -> {
                product.getCategories().forEach(_category -> {
                    if (Objects.equals(category.getId(), _category.getId())) {
                        category.setChecked(true);
                    }
                });
            });
        }

        model.addAttribute("categories", categories);
        model.addAttribute("product", product);

        return new ModelAndView("admin/product/update.ftl");
    }

    @PostMapping("create")
    public ModelAndView productCreate(@Valid Product product,
                                      BindingResult result,
                                      SessionStatus status) {

        DataBinderUtils.objectValidate(result);

        productService.create(product);
        status.setComplete();
        return new ModelAndView("redirect:/admin/product");
    }

    @PostMapping("update")
    public ModelAndView productUpdate(@Valid Product product,
                                      BindingResult result,
                                      SessionStatus status) {

        DataBinderUtils.objectValidate(result);

        productService.update(product);
        status.setComplete();
        return new ModelAndView("redirect:/admin/product/update/" + product.getId() + "?success");
    }

    @PostMapping("/delete")
    public ModelAndView delete(@RequestParam Long id) {
        productService.delete(id);
        return new ModelAndView("redirect:/admin/product");
    }

    @PostMapping("/order")
    public ModelAndView changeOrder(HttpServletRequest request,
                                    @RequestParam Long id,
                                    @RequestParam String mode) {

        productService.changeOrder(id, mode);
        return new ModelAndView("redirect:" + request.getHeader("referer"));
    }
}
