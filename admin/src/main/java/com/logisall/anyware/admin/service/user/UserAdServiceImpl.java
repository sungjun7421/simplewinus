package com.logisall.anyware.admin.service.user;

import com.logisall.anyware.admin.model.SearchUserResBody;
import com.logisall.anyware.core.config.database.PwdEncConfig;
import com.logisall.anyware.core.domain.commerce.buyer.BuyerRepository;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevelRepository;
import com.logisall.anyware.core.domain.user.*;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.resbody.account.UserInfoResBody;
import com.logisall.anyware.core.service.account.user.UserService;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserAdServiceImpl implements UserAdService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private UserService userService;

  @Autowired
  private AuthorityRepository authorityRepository;

  @Autowired
  private PwdEncConfig pwdEncConfig;

  @Autowired
  private PagedResourcesAssembler pagedResourcesAssembler;

  @Autowired
  private BuyerLevelRepository buyerLevelRepository;

  @Autowired
  private BuyerRepository buyerRepository;

  @Override
  @Transactional
  @PreAuthorize("hasRole('ROLE_SUPER')")
  public void leave(Long id) {
    userService.leave(id, "관리자에의해 탈퇴처리됨");
  }

  @Override
  @Transactional
  @PreAuthorize("hasRole('ROLE_SUPER')")
  public void removePrivacy(Long id) {
//    userService.removePrivacy(id);
  }

  @Override
  @Transactional
  public void restoreLeave(Long id) {
    userRepository.findById(id)
        .ifPresent(user -> {
          user.getUserDetailsMeta().setEnabled(true);
          user.getLeaveMeta().setLeave(false);
          user.getLeaveMeta().setLeaveTime(null);
          user.getLeaveMeta().setLeaveReason(null);
        });
  }

  @Override
  @Transactional
  public List<UserInfoResBody> list(String query) {

    List<User> list = Lists.newArrayList(userRepository.findAll(
        UserPredicate.getInstance()
            .search(query)
            .role(Authority.Role.USER)
            .values()));

    return list.stream().map(user -> {
      UserInfoResBody ui = new UserInfoResBody();
      ui.setId(user.getId());
      ui.setName(user.getFullName());
      ui.setEmail(user.getEmail());
      ui.setMobile(user.getMobile());
      return ui;
    }).collect(Collectors.toList());
  }

  @Override
  @Transactional(readOnly = true)
  public Page<User> pageByManager(Filter filter, Authority.Role role) {

    return userRepository.findAll(
        UserPredicate.getInstance()
            .search(filter.getQuery())
            .startDate(filter.getStartDate())
            .endDate(filter.getEndDate())
            .roles(role)
            .roles(role == null ? Lists.newArrayList(Authority.Role.SUPER, Authority.Role.ADMIN) : null)
            .values()
        ,
        filter.getPageable());
  }

  @Override
  @Transactional
  public PagedModel<SearchUserResBody> pagedResources(Pageable pageable, String keyword) {

    Page<User> userPage = userRepository.findAll(
        UserPredicate.getInstance()
            .search(keyword)
            .values(),
        pageable
    );

    List<SearchUserResBody> list = userPage.getContent().stream()
        .map(user -> SearchUserResBody.builder()
            .id(user.getId())
            .email(user.getEmail())
            .fullName(user.getFullName())
            .build()).collect(Collectors.toList());

    PageImpl<SearchUserResBody> page = new PageImpl<>(list, pageable, userPage.getTotalElements());

    return pagedResourcesAssembler.toModel(page);
  }

  @Override
  public boolean isDuplicateEmail(String email) {
    return userRepository.existsByEmail(email);
  }

  @Override
  public boolean isDuplicateMobile(String mobile) {
    return userRepository.existsByMobile(mobile);
  }

  @Override
  @Transactional
  public void resetPassword(Long id, String password) {
    userRepository.findById(id)
        .ifPresent(user -> {
          user.setPassword(pwdEncConfig.getPasswordEncoder().encode(password));
          user.getUserDetailsMeta().setUpdatedPasswordDateTime(LocalDateTime.now());
        });
  }
}
