package com.logisall.anyware.admin.web.push;

import com.logisall.anyware.core.model.fcm.FCMReqBody;
import com.logisall.anyware.core.service.send.push.PushService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@PreAuthorize("hasAnyRole('ROLE_SUPER', 'ROLE_ADMIN')")
@Slf4j
@Controller
@RequestMapping("/admin/push")
public class AdminPushController {

    @Autowired
    private PushService pushService;

    @GetMapping
    public ModelAndView push(@RequestParam(required = false) String title,
                             @RequestParam(required = false) String content,
                             Model model) {

        FCMReqBody fcmReqBody = new FCMReqBody();
        fcmReqBody.setTitle(title);
        fcmReqBody.setContent(content);

        model.addAttribute("fcmReqBody", fcmReqBody);

        return new ModelAndView("admin/push/form.ftl");
    }

    @PostMapping("send")
    public ModelAndView send(Model model, @Valid FCMReqBody fcmReqBody) {

        String result = pushService.sendMessageToAllDevice(fcmReqBody);
        model.addAttribute("result", result);
        log.info("> result ::: {}", result);

        switch (result) {
            case "200":
                model.addAttribute("message", "정상적으로 메세지가 보내졌습니다.");
                break;
            case "500":
                model.addAttribute("message", "실패하였습니다. (발신번호 사전 등록제에 등록제에 의한 미등록 차단)");
                log.error("::: PUSH NOTIFICATION ERROR (500) :::");
                break;
        }

        return new ModelAndView("admin/push/result.ftl");
    }
}
