package com.logisall.anyware.admin.web.c_product;


import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.freemarker.PageableModel;
import com.logisall.anyware.core.domain.board.comment.Comment;
import com.logisall.anyware.core.domain.commerce.review.Review;
import com.logisall.anyware.core.domain.commerce.review.ReviewRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.commerce.review.ReviewService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Locale;

@Slf4j
@Controller
@RequestMapping("/admin/review")
public class AdminReviewController {
    @Autowired
    private ReviewService reviewService;
    @Autowired
    private ReviewRepository reviewRepository;


    @GetMapping
    public ModelAndView page(Model model,
                             @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                             @RequestParam(required = false, defaultValue = "") String query,
                             @RequestParam(required = false) String startDate,
                             @RequestParam(required = false) String endDate,
                             @RequestParam(required = false) Long idUser) {
        Filter filter = new Filter(pageable, query, startDate, endDate);
        Page<Review> page = reviewService.page(filter, idUser);

        model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
        model.addAttribute("page", pageable.getPageNumber());


        // Search filter
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);
        model.addAttribute("idUser", idUser);
        return new ModelAndView("admin/review/list.ftl");
    }
    @GetMapping(value = "/update/{id}")
    public ModelAndView update(@PathVariable Long id,
                               Model model) {

        Review review = reviewService.get(id);

        model.addAttribute("review", review);
        model.addAttribute("locales", Arrays.asList(Locale.getAvailableLocales()));

        return new ModelAndView("/admin/review/update.ftl");
    }



    @PostMapping(value = "/update")
    public ModelAndView reviewUpdate(@Valid Review review,
                                     org.springframework.validation.BindingResult result,
                                     SessionStatus status,
                                     Locale locale
                                     ) {


        if (result.hasErrors()) {
            result.getAllErrors().forEach(objectError -> {
                log.error("name -> " + objectError.getObjectName() + ", msg -> " + objectError.getDefaultMessage());
            });
            throw new BadRequestException(result.getObjectName());
        }
        Review review1 =reviewRepository.findById(review.getId()).get();
        review1.setReply(review.getReply());
        review1.setActive(review.isActive());
        if(review1.getReplyTime() == null){
            review1.setReplyTime(LocalDateTime.now());
        }
        reviewService.update(review1);
        status.setComplete();
        return new ModelAndView("redirect:/admin/review/update/" + review.getId() + "?success");
    }

    @PostMapping(value = "/delete")
    public ModelAndView delete(@RequestParam Long id) {
        reviewService.delete(id);
        return new ModelAndView("redirect:/admin/review");
    }

}
