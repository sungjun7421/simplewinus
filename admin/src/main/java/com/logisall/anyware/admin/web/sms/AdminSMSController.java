package com.logisall.anyware.admin.web.sms;

import com.logisall.anyware.core.domain.sms.SMSHistory;
import com.logisall.anyware.core.domain.sms.SMSHistoryRepository;
import com.logisall.anyware.core.service.send.aligo.AligoService;
import com.logisall.anyware.core.service.send.sms.SendSMSService;
import com.logisall.anyware.core.vo.SMS;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Map;
import java.util.concurrent.Future;

@PreAuthorize("hasAnyRole('ROLE_SUPER', 'ROLE_ADMIN')")
@Slf4j
@Controller
@RequestMapping("/admin/sms")
public class AdminSMSController {

  @Value("${apistore.sms.send-phone}")
  private String sendPhoneNumber;

  @Autowired
  private SendSMSService sendSmsService;

  @Autowired
  private AligoService aligoService;

  @Autowired
  private SMSHistoryRepository smsHistoryRepository;

  @GetMapping
  public ModelAndView sms(@RequestParam(required = false) String destPhone,
                          @RequestParam(required = false) String destName,
                          @RequestParam(required = false) String subject,
                          @RequestParam(required = false) String msgBody,
                          Model model) {

    SMS sms = new SMS();
    sms.setDestName(destName);
    sms.setDestPhone(destPhone);
    sms.setMsgBody(msgBody);
    sms.setSubject(subject);

    model.addAttribute("types", SMS.Type.values());
    model.addAttribute("sms", sms);
    model.addAttribute("sendPhoneNumber", sendPhoneNumber);
//        model.addAttribute("balance", smsService.balance()); // 잔액 조회 서비스
//        model.addAttribute("listSendNumber", smsService.listSendNumber()); // 잔액 조회 서비스
//        model.addAttribute("deposit", smsService.deposit()); // 충전금액 조회 서비스

    return new ModelAndView("admin/sms/form.ftl");
  }

  @PostMapping("send")
  public ModelAndView send(Model model, @Valid SMS sms) {

    Map result = null;
    try {
      Future<Map> future = sendSmsService.send(sms);
      result = future.get();
    } catch (Exception e) {
      e.printStackTrace();
      log.error("admin sms send", e);
    }

    model.addAttribute("result", result);
    String code = result.get("result_code").toString();

    if (code.equals("200")) {
      model.addAttribute("message", "정상적으로 메세지가 보내졌습니다.");
    } else if (code.equals("100")) {
      model.addAttribute("message", "실패하였습니다. (사용자 에러)");
      log.error("::: SMS ERROR (100) :::");
    } else if (code.equals("300")) {
      model.addAttribute("message", "실패하였습니다. (매개변수 에러)");
      log.error("::: SMS ERROR (300) :::");
    } else if (code.equals("400")) {
      model.addAttribute("message", "실패하였습니다. (기타)");
      log.error("::: SMS ERROR (400) :::");
    } else if (code.equals("500")) {
      model.addAttribute("message", "실패하였습니다. (발신번호 사전 등록제에 등록제에 의한 미등록 차단)");
      log.error("::: SMS ERROR (500) :::");
    } else if (code.equals("600")) {
      model.addAttribute("message", "실패하였습니다. (선불제 충전요금 부족)");
      log.error("::: SMS ERROR (600) :::");
    }

    return new ModelAndView("admin/sms/result");
  }

  @PostMapping("sendAligo")
  public ModelAndView sendAligo(Model model, @Valid SMS sms) {

    String result = aligoService.sendPhoneCodeWithAligo(sms.getDestPhone(), sms.getMsgBody());
    model.addAttribute("result", result);
    log.info("> result ::: {}", result);

    switch (result) {
      case "200":
        this.saveSMSHistory(sms, SMSHistory.Code.OK);
        model.addAttribute("message", "정상적으로 메세지가 보내졌습니다.");
        break;
      case "100":
        this.saveSMSHistory(sms, SMSHistory.Code.USER_ERROR);
        model.addAttribute("message", "실패하였습니다. (사용자 에러)");
        log.error("::: SMS ERROR (100) :::");
        break;
      case "300":
        this.saveSMSHistory(sms, SMSHistory.Code.PARAM_ERROR);
        model.addAttribute("message", "실패하였습니다. (매개변수 에러)");
        log.error("::: SMS ERROR (300) :::");
        break;
      case "400":
        this.saveSMSHistory(sms, SMSHistory.Code.ETC_ERROR);
        model.addAttribute("message", "실패하였습니다. (기타)");
        log.error("::: SMS ERROR (400) :::");
        break;
      case "500":
        this.saveSMSHistory(sms, SMSHistory.Code.UNREG_BLOCK);
        model.addAttribute("message", "실패하였습니다. (발신번호 사전 등록제에 등록제에 의한 미등록 차단)");
        log.error("::: SMS ERROR (500) :::");
        break;
      case "600":
        this.saveSMSHistory(sms, SMSHistory.Code.LOW_RATES);
        model.addAttribute("message", "실패하였습니다. (선불제 충전요금 부족)");
        log.error("::: SMS ERROR (600) :::");
        break;
    }

    return new ModelAndView("admin/sms/result.ftl");
  }

  private void saveSMSHistory(SMS sms, SMSHistory.Code code) {
    SMSHistory smsHistory = new SMSHistory();
    smsHistory.setCode(code);
    smsHistory.setSendTime(sms.getSendTime());
    smsHistory.setDestPhone(sms.getDestPhone());
    smsHistory.setDestName(sms.getSendName());
    smsHistory.setSubject(sms.getSubject());
    smsHistory.setMsgBody(sms.getMsgBody());
    smsHistoryRepository.save(smsHistory);
  }
}
