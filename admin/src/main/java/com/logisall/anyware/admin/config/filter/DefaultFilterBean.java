package com.logisall.anyware.admin.config.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.logisall.anyware.core.config.property.ProcessEnv;

import javax.servlet.FilterConfig;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

/**
 * 필터 기본 설정
 */
@Slf4j
public class DefaultFilterBean implements Filter {

  private ProcessEnv processEnv;

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    this.processEnv = WebApplicationContextUtils.getRequiredWebApplicationContext(filterConfig.getServletContext()).getBean(ProcessEnv.class);
  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {

    if (servletRequest instanceof HttpServletRequest
        && servletResponse instanceof HttpServletResponse) {

      HttpServletRequest request = (HttpServletRequest) servletRequest;
      HttpServletResponse response = (HttpServletResponse) servletResponse;

      log.debug("# REQ : [{}] {}", request.getMethod(), request.getRequestURL());
      if (request.getMethod() != null && request.getMethod().equals("POST")) {

        Enumeration keys = request.getParameterNames();

        log.debug("--------------------");
        while (keys.hasMoreElements()) {
          String key = (String) keys.nextElement();
          String value = request.getParameter(key);
          log.debug("# Property : [" + key + " = " + value + "]");
        }
      }

      /* Spring Data Rest 에서 href 의 https scheme 문제 해결 */
      final HttpServletRequestWrapper wrapped = new HttpServletRequestWrapper(request) {
        @Override
        public StringBuffer getRequestURL() {
          final StringBuffer originalUrl = ((HttpServletRequest) getRequest()).getRequestURL();
//                    final String updatedUrl = processEnv.isProduction() ? originalUrl.toString().replace("http://", "https://") : originalUrl.toString();
          final String updatedUrl = originalUrl.toString();
          return new StringBuffer(updatedUrl);
        }
      };

      /* 폰트 캐시 */
      if (request.getRequestURI().contains("assets/fonts")) {
        response.setHeader("Cache-Control", "public max-age=3600");
        response.setHeader("Pragma", "cache");
      }

      chain.doFilter(wrapped, response);
    } else {
      chain.doFilter(servletRequest, servletResponse);
    }
  }

  @Override
  public void destroy() {

  }
}
