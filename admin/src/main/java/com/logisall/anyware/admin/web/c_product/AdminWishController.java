package com.logisall.anyware.admin.web.c_product;

import com.logisall.anyware.core.config.freemarker.PageableModel;
import com.logisall.anyware.core.domain.commerce.coupon.Coupon;
import com.logisall.anyware.core.domain.commerce.coupon.CouponType;
import com.logisall.anyware.core.domain.commerce.product.Product;
import com.logisall.anyware.core.domain.commerce.product.ProductRepository;
import com.logisall.anyware.core.domain.commerce.wish.Wish;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.commerce.product.ProductService;
import com.logisall.anyware.core.service.commerce.wish.WishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Slf4j
@Controller
@RequestMapping("/admin/wish")
public class AdminWishController {
    @Autowired
    ProductRepository productRepository;

    @Autowired
    WishService wishService;

    @GetMapping
    public ModelAndView page(Model model,
                             @ModelAttribute("setting") AppSetting setting,
                             @PageableDefault(size = 20,sort = {"createdDate"},direction = Sort.Direction.DESC) Pageable pageable,
                             @RequestParam(required = false, defaultValue = "") String query,
                             @RequestParam(required = false, defaultValue = "") String search,
                             @RequestParam(required = false, defaultValue = "") String user,
                             @RequestParam(required = false) String startDate,
                             @RequestParam(required = false) String endDate,
                             @RequestParam(required = false, defaultValue = "") Long productSelected,
                             @RequestParam(required = false, defaultValue = "1") String byProduct
                             ) {
        Filter filter = new Filter(pageable);
        filter.setEndDate(endDate);
        filter.setStartDate(startDate);
        filter.setQuery(search);
        boolean productSearch=byProduct.equals("1") ;
        Long userId=null;
        try{
            userId=Long.parseLong(user);
        }catch (Exception e){

        }

        Page<Wish> page = wishService.page(filter,productSelected,productSearch?null:userId,productSearch);

        // Default
        model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
        model.addAttribute("page", pageable.getPageNumber());

        // Search filter
        model.addAttribute("query", query);
        model.addAttribute("search", search);
        model.addAttribute("startDate",startDate);
        model.addAttribute("endDate",endDate);
        model.addAttribute("user", user);
        model.addAttribute("productSelected",productSelected);
        model.addAttribute("byProduct",byProduct);
        if(byProduct.equals("1")){
            List<Product> products=productRepository.findAllByActiveIsTrueOrderByOrderDescending();
            products.forEach(product ->{
                product.setLocale(setting.getDefaultLocale());
            });
            model.addAttribute("products", products);
        }
        return new ModelAndView("admin/wish/list.ftl");
    }
}
