package com.logisall.anyware.admin.web.email;

import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.service.send.email.SendEmailService;
import com.logisall.anyware.core.utils.StringUtils;
import com.logisall.anyware.core.vo.email.SendEmail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import com.logisall.anyware.core.utils.Utils;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@PreAuthorize("hasAnyRole('ROLE_SUPER', 'ROLE_ADMIN')")
@Slf4j
@Controller
@RequestMapping("admin/email")
public class AdminEmailController {

  @Value("${aws.ses.sender.email}")
  private String sendEmailAddress;

  @Autowired
  private SendEmailService sendEmailService;

  @Autowired
  private UserRepository userRepository;

  @GetMapping
  public ModelAndView sms(@RequestParam(required = false) String to,
                          @RequestParam(required = false) String subject,
                          @RequestParam(required = false) String body,
                          Model model,
                          @ModelAttribute("setting") AppSetting setting) {

    SendEmail sendEmail = new SendEmail();
    sendEmail.setTo(to);
    sendEmail.setSubject(subject);
    sendEmail.setBody(body);

    model.addAttribute("sendEmail", sendEmail);
    model.addAttribute("sendEmailAddress", sendEmailAddress);

    return new ModelAndView("admin/email/form.ftl");
  }

  @GetMapping("popup")
  public ModelAndView popup(@RequestParam(required = false, defaultValue = "All") String type,
                            @RequestParam(required = false) String to,
                            @RequestParam(required = false) String subject,
                            @RequestParam(required = false) String body,
                            Model model,
                            @ModelAttribute("setting") AppSetting setting) {

    SendEmail sendEmail = new SendEmail();
    sendEmail.setTo(to);
    sendEmail.setSubject(subject);
    sendEmail.setBody(body);

    model.addAttribute("sendEmail", sendEmail);
    model.addAttribute("sendEmailAddress", sendEmailAddress);
    model.addAttribute("type", type);

    return new ModelAndView("admin/email/form-popup.ftl");
  }

  @PostMapping("send")
  public ModelAndView send(@Valid SendEmail sendEmail,
                           SessionStatus status,
                           @ModelAttribute("setting") AppSetting setting) {

    log.debug("sendEmail ::: {}", sendEmail);

    String to = sendEmail.getTo().trim();
    String[] toList = to.split(",");

    List<String> destAddresses = new ArrayList<>();
    for (String toAddress : toList) {
      if (StringUtils.isNotEmpty(toAddress)) {
        destAddresses.add(toAddress.trim());
      }
    }

    if (destAddresses.size() > 0) {
      try {
        sendEmailService.send(destAddresses, sendEmail.getSubject(), Utils.convertLineSeparatorToBrTag(sendEmail.getBody()));
      } catch (Exception e) {
        e.printStackTrace();
        log.error("admin email", e);
      }
    }
    status.setComplete();
    return new ModelAndView("redirect:/admin/history-email");
  }

  @PostMapping("send-popup")
  public ModelAndView sendByPopup(@Valid SendEmail sendEmail,
                                  @RequestParam String type,
                                  SessionStatus status,
                                  @ModelAttribute("setting") AppSetting setting,
                                  Model model) {

    log.debug("type ::: {}", type);
    log.debug("sendEmail ::: {}", sendEmail);

    List<String> destAddresses = new ArrayList<>();

    if (Objects.equals(type, "All")) {
      List<User> list = userRepository.findByUserDetailsMetaEnabledTrueAndLeaveMetaLeaveFalse();
      destAddresses = list.stream().map(User::getEmail).collect(Collectors.toList());
    } else if (Objects.equals(type, "Subscriber")) {
      List<User> list = userRepository.findByUserDetailsMetaEnabledTrueAndLeaveMetaLeaveFalseAndTermsAgreeEmailRcvTrue();
      destAddresses = list.stream().map(User::getEmail).collect(Collectors.toList());
    } else if (Objects.equals(type, "Selector")) {

      String to = sendEmail.getTo().trim();
      String[] toList = to.split(",");

      for (String toAddress : toList) {
        if (StringUtils.isNotEmpty(toAddress)) {
          destAddresses.add(toAddress.trim());
        }
      }
    }

    if (destAddresses.size() > 0) {
      log.debug("destAddresses ::: {}", destAddresses);
      try {
        sendEmailService.send(destAddresses, sendEmail.getSubject(), Utils.convertLineSeparatorToBrTag(sendEmail.getBody()));
      } catch (Exception e) {
        e.printStackTrace();
        log.error("admin email", e);
      }
    }
    status.setComplete();
    model.addAttribute("message", "E-mailing completed.");
    return new ModelAndView("admin/popup-result.ftl");
  }
}
