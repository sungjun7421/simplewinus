package com.logisall.anyware.admin.web.b_order;

import com.logisall.anyware.core.config.freemarker.PageableModel;
import com.logisall.anyware.core.service.commerce.buyerlevel.BuyerLevelService;
import com.logisall.anyware.core.service.commerce.coupon.CouponService;
import com.logisall.anyware.core.service.commerce.couponbuyerlevel.CouponBuyerLevelService;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel;
import com.logisall.anyware.core.domain.commerce.coupon.Coupon;
import com.logisall.anyware.core.domain.commerce.coupon.CouponType;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.utils.DataBinderUtils;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

@Slf4j
@Controller
@RequestMapping("/admin/coupon")
public class AdminCouponController {

  @Autowired
  private CouponService couponService;

  @Autowired
  private BuyerLevelService buyerLevelService;

  @Autowired
  private CouponBuyerLevelService couponBuyerLevelService;

  @GetMapping
  public ModelAndView page(Model model,
                           @ModelAttribute("mall") AppSetting setting,
                           @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                           @RequestParam(required = false, defaultValue = "") String query,
                           @RequestParam(required = false) String startDate,
                           @RequestParam(required = false) String endDate,
                           @RequestParam(required = false) CouponType type) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<Coupon> page = couponService.page(setting.getDefaultLocale(), filter, type);

    // Default
    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    model.addAttribute("types", CouponType.values());

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);
    model.addAttribute("type", type);

    return new ModelAndView("admin/coupon/list.ftl");
  }

  @GetMapping("create")
  public ModelAndView create(Model model,
                             @ModelAttribute("mall") AppSetting setting) {

    Coupon coupon = new Coupon();
    coupon.setType(CouponType.DISCOUNT_FIXED);


    model.addAttribute("coupon", coupon);
    model.addAttribute("types", CouponType.values());
    model.addAttribute("levels", buyerLevelService.list(setting.getDefaultLocale()));
    return new ModelAndView("admin/coupon/create.ftl");
  }

  @GetMapping("update/{idCoupon}")
  public ModelAndView update(@PathVariable Long idCoupon,
                             @ModelAttribute("mall") AppSetting setting,
                             Model model) {

    Coupon coupon = couponService.get(setting.getDefaultLocale(), idCoupon);
    // BuyerLevel Checked
    List<BuyerLevel> levels = buyerLevelService.list(setting.getDefaultLocale());
    if (coupon.getBuyerLevels()  != null && coupon.getBuyerLevels().size() > 0) {
      levels.forEach(level -> {
        coupon.getBuyerLevels().forEach(l -> {
          if (Objects.equals(level.getId(), l.getId())) {
            level.setChecked(true);
          }
        });
      });
    }
    model.addAttribute("coupon", coupon);
    model.addAttribute("types", CouponType.values());
    model.addAttribute("levels", levels);
    return new ModelAndView("admin/coupon/update.ftl");
  }


  @PostMapping("create")
  public ModelAndView createSubmit(@Valid @ModelAttribute("coupon") Coupon coupon,
                                   BindingResult result,
                                   SessionStatus status,
                                   @ModelAttribute("mall") AppSetting setting) {

    DataBinderUtils.objectValidate(result);

    // 쿠폰코드가 입력되지 않았을때 자동생성
    if (StringUtils.isEmpty(coupon.getCode())) {
      coupon.setCode(couponService.generateCode());
    }

    couponService.create(coupon);
    status.setComplete();
    return new ModelAndView("redirect:/admin/coupon");
  }

  @PostMapping("update")
  public ModelAndView updateSubmit(@Valid @ModelAttribute("coupon") Coupon coupon,
                                   BindingResult result,
                                   SessionStatus status) {

    DataBinderUtils.objectValidate(result);

    couponService.update(coupon);
    status.setComplete();
    return new ModelAndView("redirect:/admin/coupon/update/" + coupon.getId() + "?success");
  }

  @PreAuthorize("hasRole('ROLE_SUPER')")
  @PostMapping("delete")
  public ModelAndView delete(@RequestParam Long id) {
    couponService.delete(id);
    return new ModelAndView("redirect:/admin/coupon");
  }

}
