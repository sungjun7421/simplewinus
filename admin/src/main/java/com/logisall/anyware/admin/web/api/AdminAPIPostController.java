package com.logisall.anyware.admin.web.api;

import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.model.admin.PostInfoResBody;
import com.logisall.anyware.core.service.board.post.PostService;
import com.logisall.anyware.core.vo.info.AutocompleteAjax;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("admin/api/post")
public class AdminAPIPostController {

  @Autowired
  private PostService postAdService;

  @GetMapping
  public ResponseEntity<?> search(@RequestParam String query,
                                  @ModelAttribute("setting") AppSetting setting) {

    List<PostInfoResBody> list = postAdService.list(query);

    log.debug("### list ::: {}", list.size());

    if (list.size() == 0) {
      return ResponseEntity.badRequest().build();
    } else {
      return ResponseEntity.ok(list.stream().map(postInfo -> {

        AutocompleteAjax<PostInfoResBody> aa = new AutocompleteAjax<>();

        aa.setData(postInfo);
        aa.setValue(String.format("%s | %s", postInfo.getTitle().getTextKoKr(), postInfo.getContent()));
        return aa;
      }).collect(Collectors.toList()));
    }
  }

}
