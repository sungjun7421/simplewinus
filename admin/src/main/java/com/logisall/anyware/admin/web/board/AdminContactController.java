package com.logisall.anyware.admin.web.board;


import com.logisall.anyware.core.config.freemarker.PageableModel;
import com.logisall.anyware.core.domain.board.contact.Contact;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.board.contact.ContactService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.ui.Model;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.Locale;

@Slf4j
@Controller
@RequestMapping("/admin/contact")
public class AdminContactController {

  @Autowired
  private ContactService contactService;

  @GetMapping
  public ModelAndView page(Model model,
                           @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                           @RequestParam(required = false, defaultValue = "") String query,
                           @RequestParam(required = false) String startDate,
                           @RequestParam(required = false) String endDate) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<Contact> page = contactService.page(null, filter);

    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);

    return new ModelAndView("admin/contact/list.ftl");
  }

  @GetMapping("/create")
  public ModelAndView create(Model model) {

    Contact contact = new Contact();

    model.addAttribute("locales", Arrays.asList(Locale.getAvailableLocales()));
    model.addAttribute("contact", contact);
    return new ModelAndView("admin/contact/create.ftl");
  }

  @GetMapping("update/{id}")
  public ModelAndView update(@PathVariable Long id,
                             Model model) {

    Contact contact = contactService.get(null, id);

    model.addAttribute("locales", Arrays.asList(Locale.getAvailableLocales()));
    model.addAttribute("contact", contact);
    return new ModelAndView("/admin/contact/update.ftl");
  }

  @PostMapping("create")
  public ModelAndView contactCreate(@Valid Contact contact,
                                    org.springframework.validation.BindingResult result,
                                    SessionStatus status) {

    if (result.hasErrors()) {
      result.getAllErrors().forEach(objectError -> {
        log.error("name -> " + objectError.getObjectName() + ", msg -> " + objectError.getDefaultMessage());
      });
      return new ModelAndView("/admin/contact/create.ftl");
    }
    contactService.create(contact);
    status.setComplete();

    return new ModelAndView("redirect:/admin/contact");
  }

  @PostMapping("update")
  public ModelAndView contactUpdate(@Valid Contact contact,
                                    org.springframework.validation.BindingResult result,
                                    SessionStatus status) {

    if (result.hasErrors()) {
      result.getAllErrors().forEach(objectError -> {
        log.error("name -> " + objectError.getObjectName() + ", msg -> " + objectError.getDefaultMessage());
      });
      return new ModelAndView("/admin/contact/update.ftl");
    }
    contactService.update(contact);
    status.setComplete();

    return new ModelAndView("redirect:/admin/contact/update/" + contact.getId() + "?success");
  }

  @PostMapping("/delete")
  public ModelAndView delete(@RequestParam Long id) {

    contactService.delete(id);
    return new ModelAndView("redirect:/admin/contact");
  }
}
