package com.logisall.anyware.admin.web.board;


import com.logisall.anyware.core.config.freemarker.PageableModel;
import com.logisall.anyware.core.domain.International.InterText;
import com.logisall.anyware.core.domain.board.event.Event;
import com.logisall.anyware.core.domain.commerce.coupon.Coupon;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.board.event.EventService;
import com.logisall.anyware.core.utils.DataBinderUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

@Slf4j
@Controller
@RequestMapping("/admin/event")
public class AdminEventController {

  @Autowired
  private EventService eventService;


  @GetMapping
  public ModelAndView page(Model model,
                           @ModelAttribute("setting") AppSetting setting,
                           @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                           @RequestParam(required = false, defaultValue = "") String query,
                           @RequestParam(required = false) String startDate,
                           @RequestParam(required = false) String endDate) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<Event> page = eventService.page(setting.getDefaultLocale(), filter, null);

    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);

    return new ModelAndView("admin/event/list.ftl");
  }

  @GetMapping("/create")
  public ModelAndView create(Model model,
                             @ModelAttribute("setting") AppSetting setting) {
    Event event = new Event();
    event.setActive(true);

    model.addAttribute("event", event);

    return new ModelAndView("admin/event/create.ftl");
  }

  @GetMapping("update/{idEvent}")
  public ModelAndView update(@PathVariable Long idEvent,
                             @ModelAttribute("setting") AppSetting setting,
                             Model model) {

    Event event = eventService.get(setting.getDefaultLocale(), idEvent);
    model.addAttribute("event", event);

    return new ModelAndView("/admin/event/update.ftl");
  }

  @PostMapping("create")
  public ModelAndView create(@Valid @ModelAttribute("event") Event event,
                             @ModelAttribute("setting") AppSetting setting,
                             BindingResult result,
                             SessionStatus status) {

    DataBinderUtils.objectValidate(result);

    log.debug("event ::: {}", event);

    eventService.create(event);
    status.setComplete();
    return new ModelAndView("redirect:/admin/event");
  }

  @PostMapping("update")
  public ModelAndView update(@Valid @ModelAttribute("event") Event event,
                             BindingResult result,
                             SessionStatus status) {

    DataBinderUtils.objectValidate(result);

    eventService.update(event);
    status.setComplete();
    return new ModelAndView("redirect:/admin/event/update/" + event.getId() + "?success");
  }

  @PostMapping("/delete")
  public ModelAndView delete(@RequestParam Long id) {
    eventService.delete(id);
    return new ModelAndView("redirect:/admin/event");
  }

  @GetMapping(value = "{idEvent}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> page(@PathVariable Long idEvent,
                                @ModelAttribute("setting") AppSetting setting,
                                HttpServletRequest request,
                                Locale locale) {
    if (request.getMethod().equals("GET")) {
      eventService.get(locale, idEvent);
    }
    return ResponseEntity.ok(eventService.get(locale, idEvent));
  }
}
