package com.logisall.anyware.admin.dataloader;

import com.logisall.anyware.core.config.database.PwdEncConfig;
import com.logisall.anyware.core.domain.user.*;
import com.logisall.anyware.core.service.account.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 초기 유저 등록 세팅 값
 */

@Slf4j
public class UserDataLoader {

  @Autowired
  private AuthorityRepository authorityRepository;

  @Autowired
  private UserService userService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private PwdEncConfig pwdEncConfig;


  public void run() throws Exception {
    log.debug("UserDataLoader ::: {}", authorityRepository.count());

    // 권한이 없으면 처리
    if (authorityRepository.count() == 0) {
      authorityRepository.save(Authority.of(Authority.Role.SUPER));
      authorityRepository.save(Authority.of(Authority.Role.ADMIN));
      authorityRepository.save(Authority.of(Authority.Role.USER));
      authorityRepository.save(Authority.of(Authority.Role.MANAGER));
    }

    if (!userRepository.findByEmail("super@super.com").isPresent()) {
      this.createUser("super@super.com", "admin12345", "관리자", "0123456789", Authority.Role.SUPER);
    }

  }

  private User createUser(String email, String password, String fullName, String mobile, Authority.Role role) {
    User createUser = new User();

    createUser.setEmail(email);
    createUser.setPassword(password);
    createUser.setFullName(fullName);

    createUser.setMobile(mobile);

    createUser.setTermsAgree(new TermsAgree());
    createUser.getTermsAgree().setTaService(true);
    createUser.getTermsAgree().setTaPrivacy(true);

    createUser.setRole(role);
    return userService.create(createUser);
  }
}
