package com.logisall.anyware.admin.web.api;

import com.logisall.anyware.core.domain.commerce.point.PointSetting;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.service.commerce.point.PointSettingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
@RestController
@RequestMapping("admin/api/point")
public class AdminAPIPointController {

  @Autowired
  private PointSettingService pointSettingService;

  @PostMapping(value = "possible/{type}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> possible(@RequestBody MultiValueMap<String, String> body,
                                 @PathVariable String type,
                                 @ModelAttribute("setting") AppSetting setting) {

    if (body == null || body.getFirst(type) == null) {
      return ResponseEntity.badRequest().build();
    }

    Map<String, Object> result = new HashMap<>();

    int value = Integer.parseInt(body.getFirst(type));
    PointSetting pointSetting = pointSettingService.get();

//    log.debug("type ::: {}", type);
//    log.debug("value ::: {}", value);
//    log.debug("pointSetting ::: {}", pointSetting);

    if (Objects.equals(type, "point")) {
      if (pointSetting.getMaxSavingPoint() < value) {
        result.put("result", "fail");
      } else {
        result.put("result", "success");
      }
    }

    return ResponseEntity.ok(result);
  }
}
