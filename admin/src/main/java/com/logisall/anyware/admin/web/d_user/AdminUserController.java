package com.logisall.anyware.admin.web.d_user;

import com.logisall.anyware.admin.service.user.UserAdService;
import com.logisall.anyware.core.config.freemarker.PageableModel;
import com.logisall.anyware.core.service.account.user.UserService;
import com.logisall.anyware.core.service.commerce.order.OrderService;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.domain.user.Authority;
import com.logisall.anyware.core.domain.user.TermsAgree;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.setting.AppSettingService;
import com.logisall.anyware.core.service.user.AuthorityService;
import com.logisall.anyware.core.utils.DataBinderUtils;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

@Slf4j
@Controller
@RequestMapping("/admin/user")
public class AdminUserController {

    @Autowired
    private UserAdService userAdService;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private AppSettingService appSettingService;

    @Autowired
    private OrderService orderService;

    @GetMapping
    public ModelAndView page(Model model,
                             @ModelAttribute("setting") AppSetting setting,
                             @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                             @RequestParam(required = false, defaultValue = "") String query,
                             @RequestParam(required = false) String startDate,
                             @RequestParam(required = false) String endDate,
                             @RequestParam(required = false) Authority.Role role) {

        Filter filter = new Filter(pageable, query, startDate, endDate);
        Page<User> page = userService.page(filter, role);

        // Default
        model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
        model.addAttribute("page", pageable.getPageNumber());

        model.addAttribute("roles", Authority.Role.values());
        model.addAttribute("role", role);

        // Search filter
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);

        return new ModelAndView("admin/user/list.ftl");
    }

//  @PostMapping("/excel")
//  public ModelAndView excel(Model model,
//                            @ModelAttribute("setting") AppSetting setting,
//                            @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
//                            @RequestParam(required = false, defaultValue = "all") String mode,
//                            @RequestParam(required = false, defaultValue = "") String query,
//                            @RequestParam(required = false) String startDate,
//                            @RequestParam(required = false) String endDate) {
//
//    Filter filter = new Filter(pageable, query, startDate, endDate);
//
//    List<User> list = userAdService.list(filter);
//
//    if (list == null || list.size() == 0) {
//      return new ModelAndView("redirect:/admin/user");
//    }
//    model.addAttribute("list", list);
//    model.addAttribute("sheetName", "USER");
////            model.addAttribute("fileName", "파일명");
//
//    return new ModelAndView(new ExcelView());
//  }

    @GetMapping(value ="/create")
    public ModelAndView create(Model model,
                               @ModelAttribute("setting") AppSetting setting) {
        User user = new User();
        TermsAgree ta = TermsAgree.builder()
                .taService(true)
                .taPrivacy(true)
                .taYouth(true)
                .taEft(true)
                .taLocation(true)
                .build();
        user.setTermsAgree(ta);

        user.setRole(Authority.Role.USER);
        model.addAttribute("user", user);
//    model.addAttribute("genders", Gender.values());

        model.addAttribute("authorities", authorityService.list(Authority.Role.USER, Authority.Role.MANAGER));

        model.addAttribute("locales", Arrays.asList(Locale.getAvailableLocales()));

        return new ModelAndView("admin/user/create.ftl");
    }

    @GetMapping("update/{idUser}")
    public ModelAndView update(@PageableDefault(size = 10, sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable,
                                @PathVariable Long idUser,
                                @ModelAttribute("setting") AppSetting setting,
                                @RequestParam(required = false) String startDate,
                                @RequestParam(required = false) String endDate,
                                @RequestParam(required = false, defaultValue = "") String query,
                                Model model) {
        Locale defaultLocale = appSettingService.getDefaultLocale();
        Filter filter = new Filter(pageable, query, startDate, endDate);
        User user = userService.get(idUser);

        List<Authority> authorities = authorityService.list(Authority.Role.USER, Authority.Role.MANAGER);

        // ERCategory Checked
        if (user.getAuthorities() != null && user.getAuthorities().size() > 0) {
            authorities.forEach(authority -> {
                user.getAuthorities().forEach(_authority -> {
                    if (Objects.equals(authority.getId(), _authority.getId())) {
                        authority.setChecked(true);
                    }
                });
            });
        }

        model.addAttribute("user", user);
//    model.addAttribute("genders", Gender.values());
        model.addAttribute("authorities", authorities);
        model.addAttribute("locales", Arrays.asList(Locale.getAvailableLocales()));
//        model.addAttribute("data", PageableModel.of(orderResBodies, pageable, query).toModel());
        model.addAttribute("page", pageable.getPageNumber());
//        model.addAttribute("count", storeOrderResBody);
        // Search filter
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);

        return new ModelAndView("admin/user/update.ftl");
    }

    @PostMapping(value ="/create")
    public ModelAndView createSubmit(@Valid User user,
                                     @ModelAttribute("setting") AppSetting setting,
                                     org.springframework.validation.BindingResult result,
                                     SessionStatus status) {

        DataBinderUtils.objectValidate(result);

        userService.create(user);
        status.setComplete();
        return new ModelAndView("redirect:/admin/user");
    }

    @PostMapping("update")
    public ModelAndView updateSubmit(@Valid User user,
                                     @ModelAttribute("setting") AppSetting setting,
                                     org.springframework.validation.BindingResult result,
                                     SessionStatus status) {

        DataBinderUtils.objectValidate(result);

        userService.update(user);
        status.setComplete();
        return new ModelAndView("redirect:/admin/user/update/" + user.getId() + "?success");
    }

    @ResponseBody
    @PostMapping("update/password")
    public ResponseEntity<?> updatePassword(@RequestBody Map<String, String> data) {

        Long idUser = StringUtils.isEmpty(data.get("id")) ? null : Long.valueOf(data.get("id"));
        String password = StringUtils.isEmpty(data.get("password")) ? null : data.get("password");

        if (idUser == null || password == null) {
            return ResponseEntity.badRequest().build();
        }

        userAdService.resetPassword(idUser, password);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasRole('ROLE_SUPER')")
    @PostMapping("leave")
    public ModelAndView leave(HttpServletRequest request,
                              @RequestParam Long id) {
        userAdService.leave(id);
        return new ModelAndView("redirect:" + request.getHeader("referer"));
    }

    @PreAuthorize("hasRole('ROLE_SUPER')")
    @PostMapping("remove-privacy")
    public ModelAndView removePrivacy(HttpServletRequest request,
                                      @RequestParam Long id) {
        userAdService.removePrivacy(id);
        return new ModelAndView("redirect:" + request.getHeader("referer"));
    }

    @PreAuthorize("hasRole('ROLE_SUPER')")
    @PostMapping("restore-leave")
    public ModelAndView restoreLeave(HttpServletRequest request,
                                     @RequestParam Long id) {
        userAdService.restoreLeave(id);
        return new ModelAndView("redirect:" + request.getHeader("referer"));
    }

    @PreAuthorize("hasRole('ROLE_SUPER')")
    @PostMapping("delete")
    public ModelAndView delete(@RequestParam Long id) {

        userService.delete(id);
        return new ModelAndView("redirect:/admin/user");
    }
}
