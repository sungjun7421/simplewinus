package com.logisall.anyware.admin.web.email;

import com.logisall.anyware.core.config.freemarker.PageableModel;
import com.logisall.anyware.core.service.send.email.EmailHistoryService;
import com.logisall.anyware.core.domain.email.EmailHistory;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.model.Filter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@Slf4j
@Controller
@RequestMapping("admin/history-email")
public class AdminEmailHistoryController {

  @Autowired
  private EmailHistoryService emailHistoryService;

  @GetMapping
  public ModelAndView page(Model model,
                           @ModelAttribute("mall") AppSetting setting,
                           @PageableDefault(size = 20, sort = {"sendTime"}, direction = Sort.Direction.DESC) Pageable pageable,
                           @RequestParam(required = false, defaultValue = "") String query,
                           @RequestParam(required = false) EmailHistory.Type type,
                           @RequestParam(required = false) String startDate,
                           @RequestParam(required = false) String endDate) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<EmailHistory> page = emailHistoryService.page(filter, type);

    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);

    return new ModelAndView("admin/email-history/list.ftl");
  }

  @GetMapping("view/{idEmailHistory}")
  public ModelAndView view(@PathVariable Long idEmailHistory,
                           Model model) {

    EmailHistory smsHistory = emailHistoryService.get(null, idEmailHistory);

    model.addAttribute("smsHistory", smsHistory);

    return new ModelAndView("admin/email-history/view.ftl");
  }
}
