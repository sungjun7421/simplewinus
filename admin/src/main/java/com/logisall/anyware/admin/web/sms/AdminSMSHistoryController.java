package com.logisall.anyware.admin.web.sms;

import com.logisall.anyware.core.config.freemarker.PageableModel;
import com.logisall.anyware.core.service.send.sms.SMSHistoryService;
import com.logisall.anyware.core.domain.sms.SMSHistory;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.send.sms.SendSMSService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@RequestMapping("/admin/history-sms")
public class AdminSMSHistoryController {

  @Autowired
  private SMSHistoryService historyService;

  @Autowired
  private SendSMSService sendSmsService;

  @GetMapping
  public ModelAndView page(Model model,
                           @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                           @RequestParam(required = false, defaultValue = "") String query,
                           @RequestParam(required = false) String startDate,
                           @RequestParam(required = false) String endDate,
//                           @RequestParam(required = false) SMSHistory.TypeSMS typeSMS,
                           @RequestParam(required = false) SMSHistory.Code code) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<SMSHistory> page = historyService.page(filter, code);

    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);

    return new ModelAndView("admin/sms-history/list.ftl");
  }

  @GetMapping("view/{idSMSHistory}")
  public ModelAndView view(@PathVariable Long idSMSHistory,
                           Model model) {

    SMSHistory smsHistory = historyService.get(null, idSMSHistory);

    model.addAttribute("smsHistory", smsHistory);

    if (!StringUtils.isEmpty(smsHistory.getCmid())) {
      model.addAttribute("cmidReport", sendSmsService.report(smsHistory.getCmid()));
    }
    return new ModelAndView("/admin/sms-history/view.ftl");
  }
}
