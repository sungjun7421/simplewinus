package com.logisall.anyware.admin.web.api;

import com.logisall.anyware.core.domain.file.FileEntity;
import com.logisall.anyware.core.model.file.FileUploadService;
import com.logisall.anyware.core.service.file.FileEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/admin/api/upload")
public class AdminAPIFileUploadController {

  @Autowired
  private FileUploadService fileUploadService;

  @Autowired
  private FileEntityService fileEntityService;

  @PostMapping("/file")
  public ResponseEntity<?> file(@RequestParam MultipartFile file) {
    return ResponseEntity.ok(fileUploadService.uploadFile(file));
  }

  @PostMapping("/video")
  public ResponseEntity<?> video(@RequestParam MultipartFile file) {
    return ResponseEntity.ok(fileUploadService.uploadVideo(file));
  }

  @PostMapping("/image")
  public ResponseEntity<?> image(@RequestParam MultipartFile file) {
    return ResponseEntity.ok(fileUploadService.uploadImage(file));
  }

  @DeleteMapping("/file")
  public ResponseEntity<?> delete(@RequestBody java.util.Map<String, String> requestBody) {
    String path = requestBody.get("path");
    fileUploadService.delete(path);
    return ResponseEntity.ok().build();
  }

  @DeleteMapping("/file/{id}")
  @PreAuthorize("hasRole('ROLE_SUPER')")
  public ResponseEntity<?> delete(@PathVariable Long id) {

    FileEntity fileEntity = fileEntityService.get(null, id);
    fileUploadService.delete(fileEntity.getUrl());
    fileEntityService.delete(id);
    return ResponseEntity.ok().build();
  }
}
