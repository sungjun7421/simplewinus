package com.logisall.anyware.admin.web.b_order;

import com.logisall.anyware.core.config.freemarker.PageableModel;
import com.logisall.anyware.core.service.commerce.order.OrderService;
import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.model.Filter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@Slf4j
@Controller
@RequestMapping("/admin/order")
public class AdminOrderController {

  @Autowired
  private OrderService orderService;

  @GetMapping
  public ModelAndView list(Model model,
                           @ModelAttribute("setting") AppSetting setting,
                           @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                           @RequestParam(required = false, defaultValue = "") String query,
                           @RequestParam(required = false) String startDate,
                           @RequestParam(required = false) String endDate,
                           @RequestParam(required = false) OrderStatus checkedOrderStatus,
                           @RequestParam(required = false, defaultValue = "") String productName,
                           @RequestParam(required = false, defaultValue = "") String memberQuery) {

//    log.debug("checkedOrderStatus :: {}", checkedOrderStatus);
//    log.debug("checkedOrderStatus.length :: {}", checkedOrderStatus.length);

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<Order> page = orderService.page(filter,
      checkedOrderStatus,
      productName,
      memberQuery);

    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);
    model.addAttribute("checkedOrderStatus", checkedOrderStatus);
    model.addAttribute("productName", productName);
    model.addAttribute("memberQuery", memberQuery);

    model.addAttribute("orderStatus", OrderStatus.values());

    return new ModelAndView("admin/order/list.ftl");
  }



  @GetMapping("view/{idOrder}")
  public ModelAndView update(@PathVariable Long idOrder,
                             @ModelAttribute("setting") AppSetting setting,
                             Model model) {

    Order order = orderService.get(idOrder);

    model.addAttribute("order", order);
    model.addAttribute("orderStatuses", OrderStatus.values());
    return new ModelAndView("admin/order/view.ftl");
  }

  @GetMapping("view/{idOrder}/popup")
  public ModelAndView popup(@PathVariable Long idOrder,
                            @ModelAttribute("setting") AppSetting setting,
                            Model model) {

    Order order = orderService.get(idOrder);


    model.addAttribute("order", order);
    return new ModelAndView("admin/order/view-popup.ftl");
  }

}
