package com.logisall.anyware.admin.web.board;

import com.logisall.anyware.core.domain.board.post.Post;
import com.logisall.anyware.core.domain.board.post.category.PCategory;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.service.board.post.PCategoryService;
import com.logisall.anyware.core.utils.DataBinderUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import org.springframework.ui.Model;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping("/admin/category-notice")
public class AdminPostCategoryNoticeController {

  private static Post.Type TYPE = Post.Type.NOTICE;

  @Autowired
  private PCategoryService categoryService;

  @GetMapping
  public ModelAndView list(Model model,
                           @ModelAttribute("setting") AppSetting setting) {

    model.addAttribute("list", categoryService.list(setting.getDefaultLocale(), TYPE));
    return new ModelAndView("admin/post-category/list.ftl");
  }

  @GetMapping(value = "/create")
  public ModelAndView create(Model model) {
    PCategory category = new PCategory();
    category.setType(TYPE);
    category.setActive(true);

    model.addAttribute("category", category);
    return new ModelAndView("admin/post-category/create.ftl");
  }

  @GetMapping(value = "/update/{idCategory}")
  public ModelAndView update(Model model,
                             @PathVariable Long idCategory,
                             @ModelAttribute("setting") AppSetting setting) {

    model.addAttribute("category", categoryService.get(setting.getDefaultLocale(), idCategory));

    return new ModelAndView("/admin/post-category/update.ftl");
  }

  @PostMapping(value = "/create")
  public ModelAndView postCreate(@Valid PCategory category,
                                 org.springframework.validation.BindingResult result,
                                 SessionStatus status,
                                 @ModelAttribute("setting") AppSetting setting) {

    DataBinderUtils.objectValidate(result);

    categoryService.create(category);
    status.setComplete();
    return new ModelAndView("redirect:/admin/category-notice");
  }

  @PostMapping(value = "/update")
  public ModelAndView postUpdate(@Valid PCategory category,
                                 org.springframework.validation.BindingResult result,
                                 SessionStatus status) {

    DataBinderUtils.objectValidate(result);

    categoryService.update(category);
    status.setComplete();
    return new ModelAndView("redirect:/admin/category-notice/update/" + category.getId() + "?success");
  }

  @PostMapping(value = "/delete")
  public ModelAndView delete(@RequestParam Long id) {
    categoryService.delete(id);
    return new ModelAndView("redirect:/admin/category-notice");
  }

  @PostMapping(value = "/order")
  public ModelAndView changeOrder(HttpServletRequest request,
                                  @RequestParam Long id,
                                  @RequestParam String mode) {
    categoryService.changeOrder(id, mode);
    return new ModelAndView("redirect:" + request.getHeader("referer"));
  }

}
