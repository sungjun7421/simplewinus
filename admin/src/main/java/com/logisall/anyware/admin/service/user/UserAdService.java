package com.logisall.anyware.admin.service.user;

import com.logisall.anyware.admin.model.SearchUserResBody;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.domain.user.Authority;
import com.logisall.anyware.core.domain.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedModel;
import com.logisall.anyware.core.model.resbody.account.UserInfoResBody;

import java.util.List;

/**
 * 관리자 전용 비즈니스 로직
 */
//@PreAuthorize("hasAnyRole('ROLE_SUPER', 'ROLE_ADMIN')")
public interface UserAdService {

    void leave(Long id);
    void removePrivacy(Long id);
    void restoreLeave(Long id);

    List<UserInfoResBody> list(String query);
    Page<User> pageByManager(Filter filter, Authority.Role role);

    PagedModel<SearchUserResBody> pagedResources(Pageable pageable, String keyword);

    boolean isDuplicateEmail(String email);
    boolean isDuplicateMobile(String mobile);

    // 비밀번호 재설정 (아이디, 병견할 비밀번호)
    void resetPassword(Long id, String password);
}
