package com.logisall.anyware.admin.web;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.freemarker.PageableModel;
import com.logisall.anyware.core.domain.file.FileEntity;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.file.FileUploadService;
import com.logisall.anyware.core.service.file.FileEntityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;


@Slf4j
@Controller
@RequestMapping("/admin/file")
public class AdminFileController {

  @Autowired
  private FileEntityService fileEntityService;

  @Autowired
  private FileUploadService fileUploadService;

  @GetMapping
  public ModelAndView page(Model model,
                           @ModelAttribute("setting") AppSetting setting,
                           @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                           @RequestParam(required = false, defaultValue = "") String query,
                           @RequestParam(required = false) String startDate,
                           @RequestParam(required = false) String endDate,
                           @RequestParam(required = false) FileEntity.MimeType mimeType) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<FileEntity> page = fileEntityService.page(setting.getDefaultLocale(), filter);

    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);

    return new ModelAndView("admin/file/list.ftl");
  }

  @GetMapping("/create")
  public ModelAndView documentCreate(Model model,
                                     @ModelAttribute("setting") AppSetting setting) {
    FileEntity file = new FileEntity();

    model.addAttribute("fileEntity", file);

    return new ModelAndView("admin/file/create.ftl");
  }

  @GetMapping("update/{id}")
  public ModelAndView update(@PathVariable Long id,
                             @ModelAttribute("setting") AppSetting setting,
                             Model model) {

    model.addAttribute("fileEntity", fileEntityService.get(setting.getDefaultLocale(), id));

    return new ModelAndView("/admin/file/update.ftl");
  }

  @PostMapping("create")
  public ModelAndView postCreate(@RequestParam("file") MultipartFile file,
                                 SessionStatus status,
                                 @ModelAttribute("setting") AppSetting setting) {

    if(file == null || file.isEmpty()) {
      throw new BadRequestException("없는 파일입니다.");
    }

    log.debug("> file empty : {}", file.isEmpty());
    log.debug("> file size : {}", file.getSize());

    FileEntity fileEntity = fileUploadService.uploadFile(file).toFileEntity();
    log.debug("> fileEntity : {}", fileEntity);

//    if (result.hasErrors()) {
//      result.getAllErrors().forEach(objectError -> {
//        log.error("name -> " + objectError.getObjectName() + ", msg -> " + objectError.getDefaultMessage());
//      });
//      return new ModelAndView("/admin/file/create.ftl");
//    }
//
    fileEntityService.create(fileEntity);
    status.setComplete();
    return new ModelAndView("redirect:/admin/file");
  }
}
