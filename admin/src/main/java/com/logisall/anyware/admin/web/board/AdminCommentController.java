package com.logisall.anyware.admin.web.board;

import com.logisall.anyware.core.config.freemarker.PageableModel;
import com.logisall.anyware.core.domain.board.comment.Comment;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.board.comment.CommentService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.Locale;

@Slf4j
@Controller
@RequestMapping("/admin/comment")
public class AdminCommentController {

  @Autowired
  private CommentService commentService;

  @GetMapping
  public ModelAndView page(Model model,
                           @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                           @RequestParam(required = false, defaultValue = "") String query,
                           @RequestParam(required = false) String startDate,
                           @RequestParam(required = false) String endDate,
                           @RequestParam(required = false) Long idUser,
                           @RequestParam(required = false) Comment.Type type,
                           @RequestParam(required = false) Long idContent,
                           @RequestParam(required = false) Long idParent){

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<Comment> page = commentService.page(filter, idUser, type, idContent, idParent, null);

    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    model.addAttribute("types", Comment.Type.values());

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);

    model.addAttribute("idUser", idUser);
    model.addAttribute("type", type);
    model.addAttribute("idContent", idContent);
    model.addAttribute("idParent", idParent);

    return new ModelAndView("admin/comment/list.ftl");
  }

  @GetMapping("/create")
  public ModelAndView create(Model model) {

    Comment comment = new Comment();
    comment.setType(Comment.Type.EVENT);

    model.addAttribute("locales", Arrays.asList(Locale.getAvailableLocales()));
    model.addAttribute("types", Comment.Type.values());
    model.addAttribute("comment", comment);

    return new ModelAndView("admin/comment/create.ftl");
  }

  @GetMapping("update/{id}")
  public ModelAndView update(@PathVariable Long id, Locale locale,
                             Model model) {

    Comment comment = commentService.get(locale, id);

    model.addAttribute("locales", Arrays.asList(Locale.getAvailableLocales()));
    model.addAttribute("types", Comment.Type.values());
    model.addAttribute("comment", comment);
    return new ModelAndView("/admin/comment/update.ftl");
  }

  @PostMapping("create")
  public ModelAndView commentCreate(@Valid Comment comment,
                                    org.springframework.validation.BindingResult result,
                                    SessionStatus status) {

    if (result.hasErrors()) {
      result.getAllErrors().forEach(objectError -> {
        log.error("name -> " + objectError.getObjectName() + ", msg -> " + objectError.getDefaultMessage());
      });
      return new ModelAndView("/admin/comment/create.ftl");
    }
    commentService.create(comment);
    status.setComplete();

    return new ModelAndView("redirect:/admin/comment");
  }

  @PostMapping("update")
  public ModelAndView commentUpdate(@Valid Comment comment,
                                    org.springframework.validation.BindingResult result,
                                    SessionStatus status) {

    if (result.hasErrors()) {
      result.getAllErrors().forEach(objectError -> {
        log.error("name -> " + objectError.getObjectName() + ", msg -> " + objectError.getDefaultMessage());
      });
      return new ModelAndView("/admin/comment/update.ftl");
    }
    commentService.update(comment);
    status.setComplete();

    return new ModelAndView("redirect:/admin/comment/update/" + comment.getId() + "?success");
  }

  @PostMapping("/delete")
  public ModelAndView delete(@RequestParam Long id) {

    commentService.delete(id);
    return new ModelAndView("redirect:/admin/comment");
  }
}
