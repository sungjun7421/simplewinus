package com.logisall.anyware.admin.web.b_order;

import com.logisall.anyware.core.config.security.CurrentUser;
import com.logisall.anyware.core.domain.commerce.point.PointSetting;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.service.commerce.point.PointSettingService;
import com.logisall.anyware.core.utils.DataBinderUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping("admin/set-point")
public class AdminPointSettingController {

  @Autowired
  private PointSettingService settingService;


  @GetMapping
  public ModelAndView setting(Model model,
                              @ModelAttribute("setting") AppSetting setting) {
    model.addAttribute("pointSetting", settingService.get());
    return new ModelAndView("admin/point-setting/update.ftl");
  }

  @PostMapping
  public ModelAndView postSetting(@Valid @ModelAttribute("pointSetting") PointSetting pointSetting,
                                  BindingResult result,
                                  SessionStatus status,
                                  @AuthenticationPrincipal CurrentUser currentUser) {

    DataBinderUtils.objectValidate(result);


    log.debug("pointSetting ::: {}", pointSetting);
    settingService.setting(pointSetting);
    status.setComplete();
    return new ModelAndView("redirect:/admin/set-point?success");
  }
}
