package com.logisall.anyware.admin.web.push;


import com.logisall.anyware.core.config.freemarker.PageableModel;
import com.logisall.anyware.core.domain.push.Push;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.send.push.PushService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@RequestMapping("/admin/history-push")
public class AdminPushHistoryController {

  @Autowired
  private PushService pushService;

  @GetMapping
  public ModelAndView page(Model model,
                           @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                           @RequestParam(required = false, defaultValue = "") String query,
                           @RequestParam(required = false) String startDate,
                           @RequestParam(required = false) String endDate,
                           @RequestParam(required = false) String deviceToken) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<Push> page = pushService.page(filter);

    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    // Search filter
    model.addAttribute("deviceToken", deviceToken);
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);

    return new ModelAndView("admin/push-history/list.ftl");
  }

  @GetMapping("view/{idPush}")
  public ModelAndView view(@PathVariable Long idPush,
                           Model model) {

    Push push = pushService.get(idPush);

    model.addAttribute("pushHistory", push);

    return new ModelAndView("/admin/push-history/view.ftl");
  }
}
