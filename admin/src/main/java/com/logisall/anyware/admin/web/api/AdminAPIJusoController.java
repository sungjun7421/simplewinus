package com.logisall.anyware.admin.web.api;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.embedd.GPS;
import com.logisall.anyware.core.model.map.naver.NaverMapAddressInfo;
import com.logisall.anyware.core.model.map.juso.JusoEntity;
import com.logisall.anyware.core.service.map.juso.JusoService;
import com.logisall.anyware.core.service.map.google.MapGoogleService;
import com.logisall.anyware.core.service.map.naver.MapNaverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * 주소
 */
@RestController
@RequestMapping("admin/api/")
public class AdminAPIJusoController {

    @Autowired
    private JusoService jusoService;

    @Autowired
    private MapNaverService mapNaverService;

    @Autowired
    private MapGoogleService mapGoogleService;

    /**
     * [util-juso-1] 주소 검색
     */
    @GetMapping("/juso")
    public ResponseEntity<?> juso(@PageableDefault(size = 30) Pageable pageable,
                                  @RequestParam String keyword) throws InterruptedException, ExecutionException {

        if (this.isSQLInjection(keyword)) {
            throw new BadRequestException("잘못된 입력값입니다.");
        }

        Future<JusoEntity> juso = jusoService.geJusoInfo(keyword, pageable.getPageNumber(), pageable.getPageSize());

        if (juso.isDone()) {
            return ResponseEntity.ok(juso.get());
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/juso/naver")
    public ResponseEntity<?> jusoNaver(@PageableDefault(size = 30) Pageable pageable,
                                       @RequestParam String keyword) throws InterruptedException, IOException, ExecutionException {

        if (this.isSQLInjection(keyword)) {
            throw new BadRequestException("잘못된 입력값입니다.");
        }

        Future<NaverMapAddressInfo> juso = mapNaverService.getNaverMapAddressInfo(keyword);

        if (juso.isDone()) {

            return ResponseEntity.ok(juso.get());
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/location")
    public ResponseEntity<?> juso(@RequestParam String address) throws InterruptedException, ExecutionException, IOException {
        Future<GPS> gps = mapGoogleService.getLocationInfo(address);

        if (gps.isDone()) {
            return ResponseEntity.ok(gps.get());
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    private boolean isSQLInjection(String keyword) {
        List sqlArray = Arrays.asList(
                //sql 예약어
                "OR", "SELECT", "INSERT", "DELETE", "UPDATE", "CREATE", "DROP", "EXEC",
                "UNION", "FETCH", "DECLARE", "TRUNCATE"
        );

        return sqlArray.stream().anyMatch(keyword::equals);
    }
}
