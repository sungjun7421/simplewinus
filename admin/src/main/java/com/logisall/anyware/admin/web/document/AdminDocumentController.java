package com.logisall.anyware.admin.web.document;

import com.logisall.anyware.core.config.freemarker.PageableModel;
import com.logisall.anyware.core.service.document.DocumentService;
import com.logisall.anyware.core.domain.document.Document;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.model.Filter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;


/**
 * 문서
 */
@Slf4j
@Controller
@RequestMapping("/admin/document")
public class AdminDocumentController {

  @Autowired
  private DocumentService documentService;

  @GetMapping
  public ModelAndView document(Model model,
                               @ModelAttribute("setting") AppSetting setting,
                               @PageableDefault(size = 20, sort = {"executeDate", "createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                               @RequestParam(required = false, defaultValue = "") String query,
                               @RequestParam(required = false) String startDate,
                               @RequestParam(required = false) String endDate,
                               @RequestParam(required = false) Document.Type type) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<Document> page = documentService.page(setting.getDefaultLocale(), filter, type);

    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);
    model.addAttribute("types", Document.Type.values());
    model.addAttribute("type", type);

    return new ModelAndView("admin/document/list.ftl");
  }

  @GetMapping("/create")
  public ModelAndView documentCreate(Model model,
                                     @ModelAttribute("setting") AppSetting setting) {
    Document document = new Document();
    document.setExecuteDate(LocalDate.now());
    model.addAttribute("document", document);
    model.addAttribute("types", Document.Type.values());
    return new ModelAndView("admin/document/create.ftl");
  }

  @GetMapping("update/{id}")
  public ModelAndView update(@PathVariable Long id,
                             @ModelAttribute("setting") AppSetting setting,
                             Model model) {

    model.addAttribute("document", documentService.get(setting.getDefaultLocale(), id));
    model.addAttribute("types", Document.Type.values());
    return new ModelAndView("/admin/document/update.ftl");
  }

  @PostMapping("create")
  public ModelAndView postCreate(@javax.validation.Valid Document document,
                                 org.springframework.validation.BindingResult result,
                                 SessionStatus status,
                                 @ModelAttribute("setting") AppSetting setting) {

    if (result.hasErrors()) {
      result.getAllErrors().forEach(objectError -> {
        log.error("name -> " + objectError.getObjectName() + ", msg -> " + objectError.getDefaultMessage());
      });
      return new ModelAndView("/admin/document/create.ftl");
    }

    documentService.create(document);
    status.setComplete();
    return new ModelAndView("redirect:/admin/document");
  }

  @PostMapping("update")
  public ModelAndView postUpdate(@javax.validation.Valid Document document,
                                 org.springframework.validation.BindingResult result,
                                 SessionStatus status) {

    if (result.hasErrors()) {
      result.getAllErrors().forEach(objectError -> {
        log.error("name -> " + objectError.getObjectName() + ", msg -> " + objectError.getDefaultMessage());
      });
      return new ModelAndView("/admin/document/update.ftl");
    }

    documentService.update(document);
    status.setComplete();
    return new ModelAndView("redirect:/admin/document/update/" + document.getId() + "?success");
  }

  @PostMapping("/delete")
  public ModelAndView delete(@RequestParam Long id) {

    documentService.delete(id);
    return new ModelAndView("redirect:/admin/document");
  }

//  @GetMapping("/popup")
//  public ModelAndView popupSMS(Model model) {
//
//    SMS sms = new SMS();
//    model.addAttribute("sms", sms);
//
//    return new ModelAndView("admin/document/email-popup.ftl");
//  }
}
