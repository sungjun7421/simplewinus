package com.logisall.anyware.admin.web.b_order;

import com.logisall.anyware.core.config.freemarker.PageableModel;
import com.logisall.anyware.core.service.commerce.point.PointAdService;
import com.logisall.anyware.core.domain.commerce.buyer.BuyerRepository;
import com.logisall.anyware.core.domain.commerce.point.Point;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.commerce.point.PointSettingService;
import com.logisall.anyware.core.utils.DataBinderUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping("/admin/point")
public class AdminPointController {

  @Autowired
  private PointAdService pointAdService;

  @Autowired
  private BuyerRepository buyerRepository;

  @Autowired
  private PointSettingService pointSettingService;

  @GetMapping
  public ModelAndView page(Model model,
                           @ModelAttribute("setting") AppSetting setting,
                           @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                           @RequestParam(required = false, defaultValue = "") String query,
                           @RequestParam(required = false) String startDate,
                           @RequestParam(required = false) String endDate,
                           @RequestParam(required = false) Long idUser,
                           @RequestParam(required = false) Point.Status status) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<Point> page = pointAdService.page(setting.getDefaultLocale(), filter, idUser, status);

    // Default
    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    model.addAttribute("statuses", Point.Status.values());

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);
    model.addAttribute("idUser", idUser);
    model.addAttribute("status", status);

    // Meta 정보
    if (idUser != null) {
      buyerRepository.findById(idUser)
        .ifPresent(buyer -> {
          model.addAttribute("buyer", buyer);
          model.addAttribute("total", pointAdService.total(buyer.getId()));
          model.addAttribute("expireScheduledTotal", pointAdService.expireScheduledTotal(buyer.getId()));
        });
    }

    model.addAttribute("pointSetting", pointSettingService.get());

    return new ModelAndView("admin/point/list.ftl");
  }

  @GetMapping("create")
  public ModelAndView create(Model model,
                             @RequestParam(required = false, defaultValue = "0") long idUser,
                             @ModelAttribute("setting") AppSetting setting) {

    Point point = new Point();
    point.setStatus(Point.Status.INCREASE);

    if (idUser > 0) {
      buyerRepository.findById(idUser)
        .ifPresent(point::setRelativeBuyer);
    }

    model.addAttribute("point", point);
    model.addAttribute("statuses", Point.Status.values());
    model.addAttribute("pointSetting", pointSettingService.get());
    return new ModelAndView("admin/point/create.ftl");
  }

  @GetMapping("update/{idPoint}")
  public ModelAndView update(@PathVariable Long idPoint,
                             @ModelAttribute("setting") AppSetting setting,
                             Model model) {

    model.addAttribute("point", pointAdService.get(setting.getDefaultLocale(), idPoint));
    model.addAttribute("pointSetting", pointSettingService.get());
    return new ModelAndView("admin/point/update.ftl");
  }

  @PostMapping("create")
  public ModelAndView createSubmit(@Valid @ModelAttribute("point") Point point,
                                   BindingResult result,
                                   SessionStatus status,
                                   @ModelAttribute("setting") AppSetting setting) {

    log.debug("point :: {}", point);
    log.debug("point.getRelativeBuyer() :: {}", point.getRelativeBuyer());
//    buyerRepository.findById(point.getRelativeBuyer().getId()).ifPresent(point::setRelativeBuyer);
    DataBinderUtils.objectValidate(result);

    Point created = pointAdService.create(point);
    status.setComplete();
    return new ModelAndView("redirect:/admin/point?idUser=" + created.getRelativeBuyer().getId());
  }

  @PreAuthorize("hasRole('ROLE_SUPER')")
  @PostMapping("delete")
  public ModelAndView delete(@RequestParam Long id) {
    pointAdService.delete(id);
    return new ModelAndView("redirect:/admin/point");
  }
}
