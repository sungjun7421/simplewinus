package com.logisall.anyware.admin.service.dashboard;

import com.logisall.anyware.core.service.commerce.order.OrderService;
import com.logisall.anyware.core.domain.board.qna.Qna;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.board.qna.QnaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class DashboardServiceImpl implements DashboardService {

  final Filter filter = new Filter(PageRequest.of(0, 10, Sort.Direction.DESC, "createdDate"), null, null, null);

  @Autowired
  private QnaService qnaService;

  @Autowired
  private OrderService orderService;

  @Override
  public Page<Qna> pageByQna() {
    return qnaService.page(null, filter);
  }

  @Override
  public Page<Order> pageByOrder() {
    return orderService.page(filter,null, null, null);
  }
}
