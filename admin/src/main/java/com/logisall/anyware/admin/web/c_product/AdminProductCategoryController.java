package com.logisall.anyware.admin.web.c_product;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.commerce.category.Category;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.service.commerce.category.CategoryService;
import com.logisall.anyware.core.utils.DataBinderUtils;
import com.logisall.anyware.core.utils.LocaleUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("/admin/category-product")
public class AdminProductCategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping
    public ModelAndView list(Model model,
                             @ModelAttribute("setting") AppSetting setting) {

        model.addAttribute("list", categoryService.list(setting.getDefaultLocale()));
        return new ModelAndView("admin/product-category/list.ftl");
    }

    @GetMapping("/create")
    public ModelAndView create(Model model) {
        Category category = new Category();
        category.setActive(true);

        model.addAttribute("category", category);

        return new ModelAndView("admin/product-category/create.ftl");
    }

    @GetMapping("update/{id}")
    public ModelAndView update(Model model,
                               @PathVariable Long id,
                               @ModelAttribute("setting") AppSetting setting) {

        model.addAttribute("category", categoryService.get(setting.getDefaultLocale(), id));

        return new ModelAndView("/admin/product-category/update.ftl");
    }

    @PostMapping("create")
    public ModelAndView postCreate(@Valid Category category,
                                   BindingResult result,
                                   SessionStatus status) {

        DataBinderUtils.objectValidate(result);

        categoryService.create(category);
        status.setComplete();
        return new ModelAndView("redirect:/admin/category-product");
    }

    @PostMapping("update")
    public ModelAndView postUpdate(@Valid Category category,
                                   BindingResult result,
                                   SessionStatus status) {

        DataBinderUtils.objectValidate(result);

        categoryService.update(category);
        status.setComplete();
        return new ModelAndView("redirect:/admin/category-product/update/" + category.getId() + "?success");
    }

    @PostMapping("/delete")
    public ModelAndView delete(@RequestParam Long id) {

        categoryService.delete(id);
        return new ModelAndView("redirect:/admin/category-product");
    }

    @PostMapping("/order")
    public ModelAndView changeOrder(HttpServletRequest request,
                                    @RequestParam Long id,
                                    @RequestParam String mode) {
        categoryService.changeOrder(id, mode);

        return new ModelAndView("redirect:" + request.getHeader("referer"));
    }

    // DUPLICATE
    @ResponseBody
    @PostMapping(value = "/duplicate/{language}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> duplicate(@RequestBody MultiValueMap<String, String> body,
                                       @PathVariable Locale language) {

        final String fieldName = LocaleUtils.getFieldNameByCategory(language);
        Map<String, Object> result = new HashMap<>();

        if (body == null || body.getFirst(fieldName) == null) {
            throw new BadRequestException();
        }

        String value = body.getFirst(fieldName);

        if (StringUtils.isEmpty(value)) {
            throw new BadRequestException();
        }

        if (categoryService.isDuplicate(language, value)) {
            result.put("result", "duplicate");
        } else {
            result.put("result", "success");
        }
        return ResponseEntity.ok(result);
    }

    @ResponseBody
    @PostMapping(value = "/{id}/duplicate/{language}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> duplicateUpdate(@RequestBody MultiValueMap<String, String> body,
                                             @PathVariable Long id,
                                             @PathVariable Locale language) {

        final String fieldName = LocaleUtils.getFieldNameByCategory(language);
        Map<String, Object> result = new HashMap<>();

        if (body == null || body.getFirst(fieldName) == null) {
            return ResponseEntity.badRequest().build();
        }

        String value = body.getFirst(fieldName);

        Category category = categoryService.get(language, id);
        final String name = category.getName().getValue();

        if (value.equals(name)) {
            result.put("result", "success");
        } else if (categoryService.isDuplicate(language, value)) {
            result.put("result", "duplicate");
        } else {
            result.put("result", "success");
        }
        return ResponseEntity.ok(result);
    }

}
