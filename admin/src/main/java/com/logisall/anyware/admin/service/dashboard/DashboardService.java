package com.logisall.anyware.admin.service.dashboard;

import com.logisall.anyware.core.domain.board.qna.Qna;
import com.logisall.anyware.core.domain.commerce.order.Order;
import org.springframework.data.domain.Page;

public interface DashboardService {

  Page<Qna> pageByQna();
  Page<Order> pageByOrder();
}
