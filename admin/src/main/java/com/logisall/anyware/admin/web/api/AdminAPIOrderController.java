package com.logisall.anyware.admin.web.api;

import com.logisall.anyware.core.service.commerce.order.OrderService;
import com.logisall.anyware.core.domain.commerce.OrderStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/admin/api/order")
public class AdminAPIOrderController {
    @Autowired
    private OrderService orderService;

    @PatchMapping("/order-status/{idOrder}/{status}")
    public ResponseEntity<?> orderStatus(@PathVariable Long idOrder,
                                         @PathVariable OrderStatus status) {

        orderService.updateByOrderStatus(idOrder, status);
        return ResponseEntity.ok("success");
    }
}
