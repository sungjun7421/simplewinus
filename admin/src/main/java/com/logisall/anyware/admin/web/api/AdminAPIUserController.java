package com.logisall.anyware.admin.web.api;

import com.logisall.anyware.admin.service.user.UserAdService;
import com.logisall.anyware.core.config.security.CurrentUser;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.service.account.user.UserService;
import com.logisall.anyware.core.vo.info.AutocompleteAjax;
import com.logisall.anyware.core.model.resbody.account.UserInfoResBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/admin/api/user")
public class AdminAPIUserController {

  @Autowired
  private UserAdService userAdService;

  @Autowired
  private UserService userService;

  @GetMapping
  public ResponseEntity<?> search(@RequestParam String query,
                                  @ModelAttribute("setting") AppSetting setting) {

    List<UserInfoResBody> list = userAdService.list(query);

    log.debug("### list ::: {}", list.size());

    if (list.size() == 0) {
      return ResponseEntity.badRequest().build();
    } else {
      return ResponseEntity.ok(list.stream().map(userInfo -> {

        AutocompleteAjax<UserInfoResBody> aa = new AutocompleteAjax<>();
        userInfo.setSearchParam(query);
        aa.setData(userInfo);
        aa.setValue(String.format("%s (%s | %s)", userInfo.getName(), userInfo.getEmail(), userInfo.getMobile()));
        return aa;
      }).collect(Collectors.toList()));
    }
  }

  @GetMapping("search")
  public ResponseEntity<?> search(@PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                               @RequestParam String keyword) {

    return ResponseEntity.ok(userAdService.pagedResources(pageable, keyword));
  }

  @PostMapping(value = "/duplicate/{type}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> duplicate(@RequestBody MultiValueMap<String, String> body,
                                     @PathVariable String type,
                                     @AuthenticationPrincipal CurrentUser currentUser) {

    Map<String, Object> result = new HashMap<>();

    if (body == null || body.getFirst(type) == null) {
      return ResponseEntity.badRequest().build();
    }

    String value = body.getFirst(type);

//        if (mySessionValue(type, currentUser, value)) {
//            result.put("result", "success");
//            return ResponseEntity.ok(result);
//        }

    if (type.equals("email")) {
      if (userAdService.isDuplicateEmail(value)) {
        result.put("result", "duplicate");
      } else {
        result.put("result", "success");
      }

    } else if (type.equals("mobile")) {
      if (userAdService.isDuplicateMobile(value)) {
        result.put("result", "duplicate");
      } else {
        result.put("result", "success");
      }
    }

    return ResponseEntity.ok(result);
  }

  @GetMapping(value = "/duplicate/email")
  public ResponseEntity<?> duplicateEmail(@RequestParam String email) {

    Map<String, Object> result = new HashMap<>();

    if (email == null || email == "") {
      return ResponseEntity.badRequest().build();
    }

    if (userAdService.isDuplicateEmail(email)) {
      result.put("result", "duplicate");
    } else {
      result.put("result", "success");
    }

    return ResponseEntity.ok(result);
  }

  @PostMapping(value = "/{idUser}/duplicate/{type}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> duplicateUpdate(@RequestBody MultiValueMap<String, String> body,
                                           @PathVariable Long idUser,
                                           @PathVariable String type,
                                           @AuthenticationPrincipal CurrentUser currentUser) {

    Map<String, Object> result = new HashMap<>();

    if (body == null || body.getFirst(type) == null) {
      return ResponseEntity.badRequest().build();
    }

    String value = body.getFirst(type);

//        if (mySessionValue(type, currentUser, value)) {
//            result.put("result", "success");
//            return ResponseEntity.ok(result);
//        }

    User user = userService.get(idUser);

    if (type.equals("email")) {
      String email = user.getEmail();
      if (value.equals(email)) {
        result.put("result", "success");
      } else if (userAdService.isDuplicateEmail(value)) {
        result.put("result", "duplicate");
      } else {
        result.put("result", "success");
      }

    } else if (type.equals("mobile")) {
      String mobile = user.getMobile();
      if (value.equals(mobile)) {
        result.put("result", "success");
      } else if (userAdService.isDuplicateMobile(value)) {
        result.put("result", "duplicate");
      } else {
        result.put("result", "success");
      }
    }

    return ResponseEntity.ok(result);
  }

  private boolean mySessionValue(String type, CurrentUser currentUser, String value) {
    if (currentUser != null) {
      User user = userService.get(currentUser.getId());
//            if (type.equals("username") && user.getUsername().equals(value)) {
//                return true;
//            } else

      if (type.equals("email") && user.getEmail().equals(value)) {
        return true;
      } else if (type.equals("mobile") && user.getMobile().equals(value)) {
        return true;
      }
    }
    return false;
  }
}
