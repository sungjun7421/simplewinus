package com.logisall.anyware.admin.web.api;

import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.model.admin.EventInfoResBody;
import com.logisall.anyware.core.service.board.event.EventService;
import com.logisall.anyware.core.vo.info.AutocompleteAjax;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("admin/api/event")
public class AdminAPIEventController {

  @Autowired
  private EventService eventAdService;

  @GetMapping
  public ResponseEntity<?> search(@RequestParam String query,
                                  @ModelAttribute("setting") AppSetting setting) {

    List<EventInfoResBody> list = eventAdService.list(query);

    log.debug("### list ::: {}", list.size());

    if (list.size() == 0) {
      return ResponseEntity.badRequest().build();
    } else {
      return ResponseEntity.ok(list.stream().map(eventInfo -> {

        AutocompleteAjax<EventInfoResBody> aa = new AutocompleteAjax<>();

        aa.setData(eventInfo);
        aa.setValue(String.format("%s | %s", eventInfo.getTitle().getTextKoKr(), eventInfo.getContent()));
        return aa;
      }).collect(Collectors.toList()));
    }
  }
}
