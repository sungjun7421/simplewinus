<#-- START widgets box-->
<div class="container-fuild">
<#-- FORM -->
    <form id="form-fcmReqBody" data-type="product-create-update" action="<@spring.url header.url +"/send"/>" method="post" data-parsley-validate="" novalidate="" data-parsley-international="true">
        <div class="row">
            <div class="col-md-12">

                <div class="pull-left mb-lg">
                    <button type="submit" class="btn btn-primary btn-lg">
                        보내기
                    </button>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </div>

                <div class="pull-right mb-lg">
                    <a href="<@spring.url "/admin/history-push"/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top" title="List">
                        <span class="icon-list"></span>
                    </a>
                </div>

            </div>
        </div>
        <div class="row">
        <#-- CONTENT-->
            <div class="col-lg-9 col-md-8">

                <div class="panel panel-default">
                    <div class="panel-body">

<#--                        <div class="form-group">-->
<#--                            <label class="control-label"> DeviceToken <span class="text-require">*</span></label>-->
<#--                            <@spring.formTextarea "fcmReqBody.deviceToken",-->
<#--                            "placeholder=\"DeviceToken\"-->
<#--                            class=\"form-control\"-->
<#--                            rows=\"1\"-->
<#--                            required=\"required\"-->
<#--                            "/>-->
<#--                            <span class="help-block m-b-none"> 예시: 0101111</span>-->
<#--                        </div>-->

                        <div class="form-group">
                            <label class="control-label">메시지의 내용 <span class="text-require">*</span></label>
                            <@spring.formTextarea "fcmReqBody.title",
                            "placeholder=\"Title\"
                            class=\"form-control\"
                            rows=\"3\"
                            required=\"required\"
                            "/>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">메시지의 내용 <span class="text-require">*</span></label>
                        <@spring.formTextarea "fcmReqBody.content",
                        "placeholder=\"Content\"
                            class=\"form-control\"
                            rows=\"3\"
                            required=\"required\"
                            "/>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>
            </div>
        <#-- END : CONTENT-->

        </div>
    </form>
<#-- END : FORM -->
</div>
