<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div class="container-fluid product-create-update">
    <#-- FORM -->
    <form id="form-create-product" action="<@spring.url header.url + "/create"/>" method="post" data-parsley-validate=""
          novalidate="" data-parsley-international="true">
        <div class="row">
            <div class="col-md-12">

                <div class="pull-left mb-lg">
                    <button type="submit" class="btn btn-primary btn-lg">
                        저장
                    </button>

                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </div>

                <div class="pull-right mb-lg">
                    <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top"
                       title="목록보기">
                        <span class="icon-list"></span>
                    </a>
                </div>

            </div>
        </div>
        <div class="row">
            <#-- CONTENT-->
            <div class="col-lg-7">

                <#-- TAB Panel or Panel Wrapper -->
                <div <#if international>role="tabpanel" class="panel panel-transparent" data-type="tabpanel-language"
                     <#else>class="panel panel-default"</#if>>

                    <@spring.bind "product.information"/>
                    <#assign tabPath = spring.status.expression/>
                    <#-- TAB LANGUAGE HEADER -->
                    <@ui.tabListByLanguage tabPath/>
                    <#-- END : TAB LANGUAGE HEADER -->

                    <#-- TAB LANGUAGE BODY -->
                    <div <#if international>class="tab-content bg-white" <#else>class="panel-body"</#if>>

                        <#-- 국문 -->
                        <#if im.koKr>
                            <div id="${tabPath}-ko" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- 제목 -->
                                <@ui.formInputTextByLanguage "product.information.name.textKoKr" "제품명" true 30 "제품명을 입력하세요."/>
                                <@ui.formInputTextByLanguage "product.information.subtitle.textKoKr" "Subtitle" false 255 "Subtitle"/>
                                <@ui.formInputTextByLanguage "product.information.summary.textKoKr" "Sumary" false 500 "Sumary"/>
                                <@ui.wysiswygEdior "product.information.content.textKoKr" "Content KR" false />

                                <@ui.formActiveByLanguage "product.internationalMode.koKr" />
                            </div>
                        <#else>

                            <@spring.formHiddenInput "product.information.name.textKoKr"/>
                            <@spring.formHiddenInput "product.information.subtitle.textKoKr"/>
                            <@spring.formHiddenInput "product.information.sumary.textKoKr"/>
                            <@spring.formHiddenInput "product.information.content.textKoKr"/>

                        </#if>
                        <#-- END : 국문 -->
                        <#-- 영문 -->
                        <#if im.enUs>
                            <div id="${tabPath}-en" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- 제목 -->
                                <@ui.formInputTextByLanguage "product.information.name.textEnUs" "제품명" true 30 "제품명을 입력하세요."/>
                                <@ui.formInputTextByLanguage "product.information.subtitle.textEnUs" "Subtitle" false 255 "Subtitle"/>
                                <@ui.formInputTextByLanguage "product.information.summary.textEnUs" "Sumary" false 500 "Sumary"/>
                                <@ui.wysiswygEdior "product.information.content.textEnUs" "Content EnUs" false />

                                <@ui.formActiveByLanguage "product.internationalMode.EnUs"/>
                            </div>
                        <#else>

                            <@spring.formHiddenInput "product.information.name.textEnUs"/>
                            <@spring.formHiddenInput "product.information.subtitle.textEnUs"/>
                            <@spring.formHiddenInput "product.information.sumary.textEnUs"/>
                            <@spring.formHiddenInput "product.information.content.textEnUs"/>

                        </#if>
                        <#-- END : 영문 -->
                        <#-- 중문 (간체) -->
                        <#if im.zhCn>
                            <div id="${tabPath}-zh-cn" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- 제목 -->
                                <@ui.formInputTextByLanguage "product.information.name.textZhCn" "제품명" true 30 "제품명을 입력하세요."/>
                                <@ui.formInputTextByLanguage "product.information.subtitle.textZhCn" "Subtitle" false 255 "Subtitle"/>
                                <@ui.formInputTextByLanguage "product.information.summary.textZhCn" "Sumary" false 500 "Sumary"/>
                                <@ui.wysiswygEdior "product.information.content.textZhCn" "Content ZhCn" false  />

                                <@ui.formActiveByLanguage "product.internationalMode.ZhCn"/>
                            </div>
                        <#else>

                            <@spring.formHiddenInput "product.information.name.textZhCn"/>
                            <@spring.formHiddenInput "product.information.subtitle.textZhCn"/>
                            <@spring.formHiddenInput "product.information.sumary.textZhCn"/>
                            <@spring.formHiddenInput "product.information.content.textZhCn"/>

                        </#if>
                        <#-- END : 중문 (간체) -->
                        <#-- 중문 (번체) -->
                        <#if im.zhTw>
                            <div id="${tabPath}-zh-tw" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- 제목 -->
                                <@ui.formInputTextByLanguage "product.information.name.textZhTw" "제품명" true 30 "제품명을 입력하세요."/>
                                <@ui.formInputTextByLanguage "product.information.subtitle.textZhTw" "Subtitle" false 255 "Subtitle"/>
                                <@ui.formInputTextByLanguage "product.information.summary.textZhTw" "Sumary" false 500 "Sumary"/>
                                <@ui.wysiswygEdior "product.information.content.textZhTw" "Content ZhTw" false />

                                <@ui.formActiveByLanguage "product.internationalMode.zhTw"/>
                            </div>
                        <#else>

                            <@spring.formHiddenInput "product.information.name.textZhTw"/>
                            <@spring.formHiddenInput "product.information.subtitle.textZhTw"/>
                            <@spring.formHiddenInput "product.information.sumary.textZhTw"/>
                            <@spring.formHiddenInput "product.information.content.textZhTw"/>

                        </#if>
                        <#-- END : 중문 (번체) -->
                        <#-- 일문 -->
                        <#if im.jaJp>
                            <div id="${tabPath}-ja" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- 제목 -->
                                <@ui.formInputTextByLanguage "product.information.name.textJaJp" "제품명" true 30 "제품명을 입력하세요."/>
                                <@ui.formInputTextByLanguage "product.information.subtitle.textJaJp" "Subtitle" false 255 "Subtitle"/>
                                <@ui.formInputTextByLanguage "product.information.summary.textJaJp" "Sumary" false 500 "Sumary"/>
                                <@ui.wysiswygEdior "product.information.content.textJaJp""Content JP" false  />

                                <@ui.formActiveByLanguage "product.internationalMode.jaJp"/>
                            </div>
                        <#else>

                            <@spring.formHiddenInput "product.name.textJaJp"/>
                            <@spring.formHiddenInput "product.information.subtitle.textJaJp"/>
                            <@spring.formHiddenInput "product.information.sumary.textJaJp"/>
                            <@spring.formHiddenInput "product.information.content.textJaJp"/>

                        </#if>
                        <#-- END : 일문 -->
                    </div>
                </div>
                <#-- END : TAB LANGUAGE BODY -->

<#--                <div class="panel panel-default">-->
<#--                    <div class="panel-body">-->
<#--                        <@ui.wysiswygEdior "product.note" "메모장" false 300 "ONLY_TEXT"/>-->
<#--                    </div>-->
<#--                </div>-->

            </div>
            <#-- END : CONTENT-->
            <#-- SIDBAR -->
            <div class="col-lg-5 pl0-lg">

<#--                <div class="panel panel-default">-->
<#--                    <div class="panel-heading"><h4>정보</h4></div>-->
<#--                    <div class="panel-body">-->
<#--                        <@ui.formDate "product.date1" "강연1 날짜" true "YYYY-MM-DD"/>-->
<#--                        <@ui.formDate "product.date2" "강연2 날짜" false "YYYY-MM-DD"/>-->
<#--                    </div>-->
<#--                </div>-->

                <div class="panel panel-default">
                    <div class="panel-body">

                        <#-- 아이콘 이미지 -->
                        <@ui.uploadImage "아이콘 이미지" "information.thumbnail" "" "44px 이상 정사각형 png 이미지로만 업로드해주세요."/>
                        <#-- END : 아이콘 이미지 -->

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><h4>가격 관리</h4></div>
                    <div class="panel-body">
                        <@ui.formInputText "product.cogs" "COGS" true 10 "Please fill COGS" "number"/>
                        <@ui.formInputText "product.price" "원가격" true 10 "원가격을 입력하세요." "number"/>
                        <@ui.formInputText "product.dcRate" "할인율" true 10 "할인율을 입력하세요." "number"/>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <#-- 카테고리 -->
                        <@spring.bind "product.categories"/>
                        <div class="form-group">
                            <label class="control-label"><h5 style="font-weight: bold">카테고리</h5></label>
                            <#if categories?size == 0>
                                <p class="form-control-static">등록된 카테고리가 없습니다.</p>
                            <#else>
                                <div class="row">

                                    <#list categories as category>
                                        <div class="col-xs-6 col-md-3">
                                            <div class="checkbox c-checkbox">
                                                <label><input name="${spring.status.expression}" type="checkbox" value="${category.id?c}"
                                                              <#if category.checked>checked</#if>><span
                                                            class="fa fa-check"></span>${category.name.value!} (${category.rangeDate!})
                                                </label>
                                            </div>
                                        </div>
                                    </#list>
                                </div>
                            </#if>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><h4>설정</h4></div>
                    <div class="panel-body">
                        <#--              <@ui.formInputText "product.limit" "판매수량" true 10 "판매수량을 입력하세요." "number"/>-->
<#--                        <@ui.formBoolean "product.soldout" "품절모드" "활성" "비활성" "활성일 경우 강제 품절이 됩니다."/>-->

<#--                        order-->
                        <@ui.formInputText "product.orderDescending" "Order Des" true 10 "Order Des" "number"/>
                        <hr/>
                        <!-- 활성 모드 -->
                        <@ui.formActive "product.active"/>
                    </div>
                </div>

            </div>
            <#-- END : SIDBAR -->
        </div>
    </form>
</div>