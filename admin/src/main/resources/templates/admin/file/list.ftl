<#import "/spring.ftl" as spring/>
<#import "../common/ui.ftl" as ui/>
<!DOCTYPE html>
<html lang="ko">

<head>
    <#include "../common/head-meta.ftl">
    <#include "../common/css/list.ftl">
</head>
<body>
<div class="wrapper">
    <#-- top navbar-->
    <#include "../common/top-navbar.ftl">
    <#-- sidebar-->
    <#include "../common/sidebar.ftl">

    <#-- Main section -->
  <section>
      <#-- Page content-->
    <div class="content-wrapper">
        <#include "../common/header.ftl" />
        <#include "body/list.ftl">
    </div>
  </section>

    <#-- Page footer-->
    <#include "../common/footer.ftl">
</div>

<#include "../common/vendor.ftl">
<#include "../common/script/list.ftl">
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.6/clipboard.min.js"></script>
<script>
  $(function () {
    window.deleteFile = function (id) {
      if (confirm("정말로 이 파일을 삭제하시겠습니까?")) {

        $.ajax({
          url: "/admin/api/upload/file/" + id,
          method: 'DELETE',
          contentType: "application/json"
        }).done(function (result) {
          console.debug(result);
          window.location.reload();
        }).fail(function (jqXHR, textStatus) {
          if (jqXHR.status.toString().startsWith("4")) {
            console.debug(jqXHR, "jqXHR");
            console.debug(textStatus, "textStatus");
            if (jqXHR.responseText && jqXHR.responseText.message) {
              $.notify(jqXHR.responseText, {status: "danger"});
            } else
              $.notify("현재페이지에 오류가 있습니다. 페이지를 새로고침(F5)하여 다시 이용해주세요.", {status: "danger"});
          } else {
            $.notify(textStatus.toUpperCase() + ": 관리자에게 문의하세요. <br>STATUS CODE: " + jqXHR.status, {status: "danger"});
          }
        });
      }
    }

    var clipboard = new ClipboardJS('[data-clipboard]');

    clipboard.on('success', function(e) {
      $.notify("URL이 복제되었습니다.\nCtrl+v 로 붙여넣기를 하세요.", {status: "info"});
      console.info('Action:', e.action);
      console.info('Text:', e.text);
      console.info('Trigger:', e.trigger);

      e.clearSelection();
    });
  });
</script>
</body>

</html>