<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div class="container-fluid fileEntity-create-update">
    <#-- FORM -->
  <form id="form-create-fileEntity" action="<@spring.url header.url + "/create"/>" method="post"
        data-parsley-validate=""
        novalidate="" data-parsley-international="true"
        enctype="multipart/form-data">
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            저장
          </button>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top"
             title="목록보기">
            <span class="icon-list"></span>
          </a>
        </div>

      </div>
    </div>
    <div class="row">
        <#-- CONTENT-->
      <div class="col-lg-12">

        <div class="panel panel-default">
          <div class="panel-heading"><h4>파일 정보</h4></div>
          <div class="panel-body">
            <fieldset>
              <div class="form-group">
                <label class="col-sm-2 control-label">파일 (이미지,영상,오디오 파일)</label>
                <div class="col-sm-10">
                  <input name="file" type="file" data-classbutton="btn btn-default" data-classinput="form-control inline" class="form-control filestyle">
                </div>
              </div>
            </fieldset>
          </div>
        </div>
      </div>
        <#-- END : CONTENT-->

    </div>
  </form>
</div>