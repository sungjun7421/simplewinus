<#--<#assign pageparam = "&key=value"/>-->
<#assign pageparam = "&type=${type!}"/>
<div class="container-fluid">
    <#-- 필터 -->
  <div class="row">
    <div class="col-md-12">
      <div id="" class="panel panel-default panel-list-filter">
        <div class="panel-heading">검색 필터

          <a href="#" data-tool="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
            <em class="fa fa-minus"></em>
          </a>

          <a id="initial-list-filter" href="javascript:void(0);" data-tool="panel-refresh" data-toggle="tooltip"
             title="" class="pull-right">
            <em class="fa fa-refresh"></em>
          </a>

        </div>
        <div class="panel-wrapper collapse in">
          <div class="panel-body">
            <form id="form-list-filter" action="" method="get">
                <#include "/admin/common/list/fieldset-common.ftl"/>
                <#include "/admin/common/list/fieldset-date.ftl"/>
                <#--              <fieldset>-->
                <#--                <div class="row">-->
                <#--                  <div class="col-lg-6">-->
                <#--                    <div class="form-group">-->
                <#--                      <label class="col-xs-2 control-label">유형</label>-->

                <#--                      <div class="col-xs-10">-->
                <#--                        <select name="type" class="form-control">-->
                <#--                          <option value="">전체</option>-->
                <#--                            <#list types as _type>-->
                <#--                              <option value="${_type}"-->
                <#--                                      <#if type?has_content && type == _type>selected</#if>>${_type.value}</option>-->
                <#--                            </#list>-->
                <#--                        </select>-->
                <#--                      </div>-->
                <#--                    </div>-->
                <#--                  </div>-->
                <#--                </div>-->
                <#--              </fieldset>-->
                <#include "/admin/common/list/fieldset-search.ftl"/>
            </form>
          </div>
            <#-- 필터 실행 -->
          <div class="panel-footer">

            <button id="submit-list-filter" type="button" class="btn btn-primary">검색</button>

            <div class="btn-group">
              <button type="button" data-toggle="dropdown" class="btn dropdown-toggle btn-default">${data.pageSize} 개씩
                보기<span class="caret"></span></button>
              <ul role="menu" class="dropdown-menu">
                <li>
                  <a href="<@spring.url "?query=${data.query!}&size=10&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">10
                    개씩 보기</a></li>
                <li>
                  <a href="<@spring.url "?query=${data.query!}&size=20&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">20
                    개씩 보기</a></li>
                <li>
                  <a href="<@spring.url "?query=${data.query!}&size=50&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">50
                    개씩 보기</a></li>
                <li>
                  <a href="<@spring.url "?query=${data.query!}&size=100&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">100
                    개씩 보기</a></li>
                <li>
                  <a href="<@spring.url "?query=${data.query!}&size=500&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">500
                    개씩 보기</a></li>
              </ul>
            </div>


              <#--<button id="excel-list-filter" type="button" data-action="<@spring.url "/admin/board/blogCategory/excel"/>" class="btn btn-labeled btn-default">-->
              <#--<span class="text">엑셀 다운로드</span>-->
              <#--<span class="btn-label btn-label-right"><i class="fa fa-file-excel-o"></i></span>-->
              <#--</button>-->
          </div>
            <#-- END : 필터 실행 -->
        </div>
      </div>
    </div>
  </div>
    <#-- END : 필터 -->
    <#-- HEADER -->
    <#assign createparam = header.url +"/create"/>
  <div class="row">
    <div class="col-md-12">
      <header class="clearfix pb-lg mb-sm">
          <#-- 목록 상태 -->
        <div class="pull-left">
          <div class="list-total">총 ${data.page.totalElements!}개,&nbsp;&nbsp;${data.currentIndex}
            /${data.page.totalPages}
            페이지
          </div>
        </div>
          <#-- END : 목록 상태 -->
          <#if createparam?has_content>
          <#-- 등록 버튼 -->
            <div class="pull-right">
                <#--              <a href="#" class="btn btn-labeled btn-default btn-lg" data-type="btn-sms-popup-document"><span class="btn-label"><i class="fa fa-send-o"></i>-->
                <#--                           </span>회원전체 이메일 발송</a>-->
              <a href="<@spring.url createparam/>" class="btn btn-primary btn-lg">새로등록</a>
            </div>
          <#-- 등록 버튼 -->
          </#if>
      </header>
    </div>
  </div>
    <#-- END : HEADER -->
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

          <#-- START table-responsive-->
        <div class="table-responsive">
          <table id="table-ext-1" class="table table-bordered table-hover">
            <colgroup>
              <col width="5%">

              <col width="10%">
              <col width="30%">
              <col width="10%">
              <col width="10%">

              <col width="10%">
              <col width="5%">
            </colgroup>
            <thead>
            <tr>
              <th class="text-center">#</th>

              <th class="text-center">MIME</th>
              <th class="text-center">View/Player</th>
              <th class="text-center">원본파일명</th>
              <th class="text-center">사이즈</th>

              <th class="text-center">등록일</th>
              <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>

            <#if data?has_content>
                <#list data.page.content as item>
                  <tr>
                    <td class="text-center">no.${data.firstNo- (item_index + 1)}<br/>id.${item.id?c}</td>
                    <td class="text-center">
                        ${item.mimeTypeEnum!}
                    </td>
                    <td class="text-center">
                        <#if item.url?has_content>
                            <#if item.mimeTypeEnum == 'IMAGE'>
                              <img src="${item.url}" class="img-responsive" style="max-width: 300px"/>
                            <#elseif item.mimeTypeEnum == 'VIDEO'>
                              <video width="150" controls>
                                <source src="${item.url}" type="${item.mimeType!}">
                                Your browser does not support HTML video.
                              </video>
                            <#elseif item.mimeTypeEnum == 'AUDIO'>
                              <audio controls>
                                <source src="${item.url}" type="${item.mimeType!}">
                                Your browser does not support the audio element.
                              </audio>
                            </#if>
                        </#if>
                        <#if item.url?has_content>
                          <br/><a class="btn btn-xs btn-info" href="${item.url}" target="_blank">파일 URL 바로가기</a>&nbsp;<button class="btn btn-xs btn-info" data-clipboard data-clipboard-text="${item.url}">URL 복제</button>
                        </#if>
                    </td>

                    <td class="text-center">
                        ${item.originalFilename!}
                    </td>
                    <td class="text-center">
                        ${item.size?c}
                    </td>
                    <td class="text-center">${item.createdDate.format('yyyy.MM.dd HH:mm:ss')}</td>
                    <td class="text-center">
                      <a href="javascript:deleteFile(${item.id?c})" class="btn btn-sm btn-default"
                         data-toggle="tooltip" data-placement="top" title="삭제">
                        <em class="icon-trash"></em>
                      </a>
                    </td>
                  </tr>
                </#list>
            </#if>
            </tbody>
          </table>
        </div>
          <#-- END table-responsive-->

          <#-- TABLE FOOTER -->
        <div class="panel-footer">
          <div class="row">
            <div class="col-lg-12 text-center">
                <#include "/admin/common/list/pagination.ftl"/>
            </div>
          </div>
        </div>
          <#-- END : TABLE FOOTER -->
      </div>
    </div>
  </div>
</div>