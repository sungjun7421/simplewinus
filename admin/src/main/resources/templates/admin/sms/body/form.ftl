<#-- START widgets box-->
<div class="container-fuild">
<#-- FORM -->
    <form id="form-sms" data-type="product-create-update" action="<@spring.url header.url +"/send"/>" method="post" data-parsley-validate="" novalidate="" data-parsley-international="true">
        <div class="row">
            <div class="col-md-12">

                <div class="pull-left mb-lg">
                    <button type="submit" class="btn btn-primary btn-lg">
                        보내기
                    </button>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </div>

                <div class="pull-right mb-lg">
                    <a href="https://www.apistore.co.kr/main.do" class="btn btn-default btn-lg" target="_blank">
                        API store 바로가기
                    </a>
                    <a href="<@spring.url "/admin/history-sms"/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top" title="List">
                        <span class="icon-list"></span>
                    </a>
                </div>

            </div>
        </div>
        <div class="row">
        <#-- CONTENT-->
            <div class="col-lg-9 col-md-8">

                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="form-group">
                            <label class="control-label">수신자의 PhoneNumber <span class="text-require">*</span></label>
                            <@spring.formTextarea "sms.destPhone",
                            "placeholder=\"수신자의 PhoneNumber\"
                            class=\"form-control\"
                            rows=\"3\"
                            required=\"required\"
                            "/>
                            <span class="help-block m-b-none"> 동보발송 시 콤마(,) 붙여서 전송, 예시: 01011112222,01011112223</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">수신자의 Fullname</label>
                            <@spring.formInput "sms.destName",
                            "placeholder=\"수신자의 Fullname\"
                            class=\"form-control\"
                            maxlength=\"255\"
                            required=\"required\"
                            " "text"/>
                            <span class="help-block m-b-none">예시: 홍길동</span>
                        </div>

                        <div class="form-group">
                            <label class="control-label">발송시간 (없을 경우 즉시발송)</label>
                            <div id="send-time-datetimepicker" class="input-group date">
                            <@spring.bind path="sms.sendTime"/>
                                <input type="text"
                                       class="form-control"
                                       placeholder="발송시간"
                                       data-input-target="input-send-time"
                                       data-parsley-errors-container="#error-send-time"
                                       value="${spring.stringStatusValue}"
                                >
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            </div>
                            <div id="error-send-time"></div>
                            <input
                                    id="input-send-time"
                                    type="hidden"
                                    name="${spring.status.expression}"
                                    value="${spring.stringStatusValue}"
                            />
                            <span class="help-block m-b-none">예시: "20130529171111" (2013-05-29 17:11:11)</span>
                        </div>
                        <hr/>
                        <div class="form-group">
                            <label class="control-label">메시지의 Title (LMS 전용)</label>
                        <@spring.formInput "sms.subject",
                        "placeholder=\"메시지의 Title\"
                            class=\"form-control\"
                            maxlength=\"255\"
                            " "text"/>
                            <span class="help-block m-b-none">예시: Title</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">메시지의 내용 <span class="text-require">*</span></label>
                        <@spring.formTextarea "sms.msgBody",
                        "placeholder=\"메시지의 내용\"
                            class=\"form-control\"
                            rows=\"3\"
                            required=\"required\"
                            "/>
                            <div class="clearfix"></div>
                            <span id="msg-byte-size" class="help-block m-b-none pull-right">0 Bytes</span>
                        <#--<span id="msg-size" class="help-block m-b-none pull-right mr">SMS&nbsp;&nbsp;</span> -->
                            <span class="help-block m-b-none pull-left">(90byte 초과 시 자동 lms로 전환 발송)</span>
                        </div>

                    </div>
                </div>
            </div>
        <#-- END : CONTENT-->
        <#-- SIDBAR -->
            <div class="col-lg-3 col-md-4 col-sm-6">
                <!-- START widget-->
                <div class="panel widget bg-primary">
                    <div class="row row-table">
                        <div class="col-xs-4 text-center bg-primary-dark pv-lg">
                            <em class="icon-bubbles fa-3x"></em>
                        </div>
                        <div class="col-xs-8 pv-lg">
<#--                            <div class="h2 mt0">￦ ${balance.balance}</div>-->
                            <div class="text-uppercase">잔여 문자 금액</div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="mb0">Type</h4>
                    </div>
                    <div class="panel-body">
                    <@spring.bind path="sms.type"/>

                        <div class="form-group">
                        <#list types as type>
                            <#if type != 'MMS'>
                                <label class="radio-inline c-radio">
                                    <input id="inlin-radio-type-${type_index}" type="radio" name="type" value="${type}" <#if sms.type == type>checked</#if>>
                                    <span class="fa fa-circle"></span>${type}
                                </label>
                            </#if>
                        </#list>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="mb0">가격Information</h4>
                    </div>
                    <div class="panel-body">
                        <ul>
                            <li>SMS : 9.9원 (건당)</li>
                            <li>LMS : 32원 (건당)</li>
                        <#--<li>MMS : 250원 (건당)</li>-->
                        </ul>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="mb0">발신번호 </h4>
                    </div>
                    <div class="panel-body">
                        <h4 class="mt0">${sendPhoneNumber!}</h4>
                    <#--<h5 class="mt0">발신가능번호</h5>-->
                    <#--<ul>-->
                    <#--<#list listSendNumber as number>-->

                    <#--<li>${number.comment} | ${number.sendnumber} | <#if number.use_yn == 'Y'>승인Complete<#else>미승인</#if></li>-->
                    <#--</#list>-->

                    <#--</ul>-->
                    </div>
                </div>
            </div>
        <#-- END : SIDBAR -->
        </div>
    </form>
<#-- END : FORM -->
</div>
