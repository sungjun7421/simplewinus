<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div id="update-think" class="container-fluid think-create-update">
    <#-- FORM -->
  <form id="form-update-think" action="<@spring.url header.url + "/update"/>" method="post" data-parsley-validate=""
        novalidate="" data-parsley-international="true">
      <#-- 수정 완료 -->
      <#include "../../common/modify-success.ftl"/>
      <#-- END : 수정 완료 -->
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            수정
          </button>
            <@spring.formHiddenInput "think.id"/>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top"
             title="목록보기">
            <span class="icon-list"></span>
          </a>
          <button data-type="btn-delete" type="button" class="btn btn-danger btn-lg" data-toggle="tooltip"
                  data-placement="top" title="삭제"
                  data-action="<@spring.url header.url + "/delete"/>"
                  data-id="${think.id?c}">
            <span class="icon-trash"></span>
          </button>
        </div>

      </div>
    </div>
    <div class="row">
        <#-- CONTENT-->
      <div class="col-lg-7">

          <#-- TAB Panel or Panel Wrapper -->
        <div <#if international>role="tabpanel" class="panel panel-transparent" data-type="tabpanel-language"
             <#else>class="panel panel-default"</#if>>

            <@spring.bind "think.title"/>
            <#assign tabPath = spring.status.expression/>
            <#-- TAB LANGUAGE HEADER -->
            <@ui.tabListByLanguage tabPath/>
            <#-- END : TAB LANGUAGE HEADER -->

            <#-- TAB LANGUAGE BODY -->
          <div <#if international>class="tab-content bg-white" <#else>class="panel-body"</#if>>

              <#-- 국문 -->
              <#if im.koKr>
                <div id="${tabPath}-ko" <#if international>role="tabpanel" class="tab-pane"</#if>>

                    <#-- 제목 -->
                    <@ui.formInputTextByLanguage "think.title.textKoKr" "제목" true 255 "제목을 입력하세요."/>
                    <@ui.formInputTextByLanguage "think.subtitle.textKoKr" "부제목" false 255 "부제목을 입력하세요."/>

                  <hr/>
                  <div class="row">
                    <div class="col-md-12">
                      <a target="_blank" href="http://sam.vitis.kr/editor/${think.relativeVersionContent.id?c}"
                         class="btn btn-primary">본문
                        에디터</a>
                    </div>
                  </div>

                    <@ui.formActiveByLanguage "think.internationalMode.koKr" />
                </div>
              <#else>
                  <@spring.formHiddenInput "think.title.textKoKr"/>
                  <@spring.formHiddenInput "think.subtitle.textKoKr"/>
                  <@spring.formHiddenInput "think.content.textKoKr"/>
              </#if>
              <#-- END : 국문 -->
              <#-- 영문 -->
              <#if im.enUs>
                <div id="${tabPath}-en" <#if international>role="tabpanel" class="tab-pane"</#if>>

                    <#-- 제목 -->
                    <@ui.formInputTextByLanguage "think.title.textEnUs" "제목" true 200 "제목을 입력하세요."/>
                    <@ui.formInputTextByLanguage "think.subtitle.textEnUs" "부제목" false 255 "부제목을 입력하세요."/>


                    <@ui.formActiveByLanguage "think.internationalMode.enUs"/>
                </div>
              <#else>
                  <@spring.formHiddenInput "think.title.textEnUs"/>
                  <@spring.formHiddenInput "think.subtitle.textEnUs"/>
                  <@spring.formHiddenInput "think.content.textEnUs"/>
              </#if>
              <#-- END : 영문 -->
              <#-- 중문 (간체) -->
              <#if im.zhCn>
                <div id="${tabPath}-zh-cn" <#if international>role="tabpanel" class="tab-pane"</#if>>

                    <#-- 제목 -->
                    <@ui.formInputTextByLanguage "think.title.textZhCn" "제목" true 100 "제목을 입력하세요."/>
                    <@ui.formInputTextByLanguage "think.subtitle.textZhCn" "부제목" false 255 "부제목을 입력하세요."/>

                    <@ui.formActiveByLanguage "think.internationalMode.zhCn"/>
                </div>
              <#else>
                  <@spring.formHiddenInput "think.title.textZhCn"/>
                  <@spring.formHiddenInput "think.subtitle.textZhCn"/>
                  <@spring.formHiddenInput "think.content.textZhCn"/>
              </#if>
              <#-- END : 중문 (간체) -->
              <#-- 중문 (번체) -->
              <#if im.zhTw>
                <div id="${tabPath}-zh-tw" <#if international>role="tabpanel" class="tab-pane"</#if>>

                    <#-- 제목 -->
                    <@ui.formInputTextByLanguage "think.title.textZhTw" "제목" true 100 "제목을 입력하세요."/>
                    <@ui.formInputTextByLanguage "think.subtitle.textZhTw" "부제목" false 255 "부제목을 입력하세요."/>

                    <@ui.formActiveByLanguage "think.internationalMode.zhTw"/>
                </div>
              <#else>
                  <@spring.formHiddenInput "think.title.textZhTw"/>
                  <@spring.formHiddenInput "think.subtitle.textZhTw"/>
                  <@spring.formHiddenInput "think.content.textZhTw"/>
              </#if>
              <#-- END : 중문 (번체) -->
              <#-- 일문 -->
              <#if im.jaJp>
                <div id="${tabPath}-ja" <#if international>role="tabpanel" class="tab-pane"</#if>>

                    <#-- 제목 -->
                    <@ui.formInputTextByLanguage "think.title.textJaJp" "제목" true 100 "제목을 입력하세요."/>
                    <@ui.formInputTextByLanguage "think.subtitle.textJaJp" "부제목" false 255 "부제목을 입력하세요."/>

                    <@ui.formActiveByLanguage "think.internationalMode.jaJp"/>
                </div>
              <#else>
                  <@spring.formHiddenInput "think.title.textJaJp"/>
                  <@spring.formHiddenInput "think.subtitle.textJaJp"/>
                  <@spring.formHiddenInput "think.content.textJaJp"/>
              </#if>
              <#-- END : 일문 -->
          </div>
        </div>
          <#-- END : TAB LANGUAGE BODY -->

      </div>
        <#-- END : CONTENT-->
        <#-- SIDBAR -->
      <div class="col-lg-5 pl0-lg">

        <div class="panel panel-default">
          <div class="panel-heading"><h4>이미지</h4></div>
          <div class="panel-body">

              <@ui.uploadImage "썸네일 이미지" "thumbnail" "${think.thumbnail!}"/>
          </div>
        </div>


        <div class="panel panel-default">
          <div class="panel-heading"><h4>설정</h4></div>
          <div class="panel-body">

              <@ui.formDate "think.regDate" "등록날짜" true "YYYY-MM-DD HH:mm:ss"/>

            <hr/>
              <#-- 활성 모드 -->
              <@ui.formActive "think.active"/>


          </div>
        </div>

          <@ui.panelMetaInfo think.updatedDate think.createdDate/>
      </div>
        <#-- END : SIDBAR -->
    </div>
  </form>
</div>