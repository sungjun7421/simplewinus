<#-- START widgets box-->
<div class="container-fuild">
    <#-- FORM -->
  <form id="form-email" action="<@spring.url header.url + "/send-popup"/>" method="post" data-parsley-validate=""
        novalidate="" data-parsley-international="true">
    <div class="row">
      <div class="col-lg-6 center-block">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">Send</button>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
          <input type="hidden" name="type" value="${type!}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="javascript:window.close()" class="btn btn-default btn-lg">Close</a>
        </div>

      </div>
    </div>
    <div class="row">
        <#-- CONTENT-->
      <div class="col-lg-6 center-block">

        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="mb0">From Email Address </h4>
          </div>
          <div class="panel-body">
            <h4 class="mt0">${sendEmailAddress!}</h4>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-body">

            <div class="form-group">
              <label class="control-label">To Email Address <span class="text-require">*</span></label>
                <#if type == 'Selector'>
                    <@spring.formTextarea "sendEmail.to",
                    "placeholder=\"Enter the To Email Address.\"
                            class=\"form-control\"
                            rows=\"3\"
                            required=\"required\"
                            "/>
                  <span class="help-block m-b-none">Send a comma (,) to multiple people.<br/>example) user1@msck.com.hk,user2@msck.com.hk</span>
                <#elseif type == 'Subscriber'>
                  <p class="form-control-static">To Subscriber</p>
                <#else>
                  <p class="form-control-static">To All</p>
                </#if>
            </div>

            <hr/>
            <div class="form-group">
              <label class="control-label">Title <span class="text-require">*</span></label>
                <@spring.formInput "sendEmail.subject",
                "placeholder=\"Enter the Title\"
                            class=\"form-control\"
                            maxlength=\"255\"
                            required=\"required\"
                            " "text"/>
              <span class="help-block m-b-none">example) [BOILERPLATE] Title~~~</span>
            </div>
            <div class="form-group">
                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "sendEmail.body" "Message" true 600/>
                <#-- END : WYSISWYG EDITOR -->
            </div>

          </div>
        </div>
      </div>
        <#-- END : CONTENT-->
    </div>
  </form>
    <#-- END : FORM -->
</div>
