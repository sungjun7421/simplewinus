<#-- START widgets box-->
<div class="container-fuild">
    <#-- FORM -->
  <form id="form-email" action="<@spring.url header.url + "/send"/>" method="post" data-parsley-validate=""
        novalidate="" data-parsley-international="true">
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">Send</button>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url "/admin/history-email"/>" class="btn btn-default btn-lg" data-toggle="tooltip"
             data-placement="top" title="View History">
            <span class="icon-list"></span>
          </a>
        </div>

      </div>
    </div>
    <div class="row">
        <#-- CONTENT-->
      <div class="col-lg-9 col-md-8">

        <div class="panel panel-default">
          <div class="panel-body">

            <div class="form-group">
              <label class="control-label">To Email Address <span class="text-require">*</span></label>
                <@spring.formTextarea "sendEmail.to",
                "placeholder=\"Enter the To Email Address.\"
                            class=\"form-control\"
                            rows=\"3\"
                            required=\"required\"
                            "/>
              <span class="help-block m-b-none">Send a comma (,) to multiple people.<br/>example) user1@msck.com.hk,user2@msck.com.hk</span>
            </div>

            <hr/>
            <div class="form-group">
              <label class="control-label">Title <span class="text-require">*</span></label>
                <@spring.formInput "sendEmail.subject",
                "placeholder=\"Enter the Title\"
                            class=\"form-control\"
                            maxlength=\"255\"
                            required=\"required\"
                            " "text"/>
              <span class="help-block m-b-none">example) [BOILERPLATE] Title~~~</span>
            </div>
            <div class="form-group">
                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "sendEmail.body" "Message" true 600/>
                <#-- END : WYSISWYG EDITOR -->
            </div>

          </div>
        </div>
      </div>
        <#-- END : CONTENT-->
        <#-- SIDBAR -->
      <div class="col-lg-3 col-md-4 col-sm-6">

        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="mb0">From Email Address </h4>
          </div>
          <div class="panel-body">
            <h4 class="mt0">${sendEmailAddress!}</h4>
          </div>
        </div>

      </div>
        <#-- END : SIDBAR -->
    </div>
  </form>
    <#-- END : FORM -->
</div>
