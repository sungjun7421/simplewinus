<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div class="container-fluid coupon-create-update">
  <#-- FORM -->
  <form id="form-create-coupon" action="<@spring.url header.url + "/create"/>" method="post" data-parsley-validate="" novalidate="" data-parsley-international="true">
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            Save
          </button>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top" title="List">
            <span class="icon-list"></span>
          </a>
        </div>

      </div>
    </div>
    <div class="row">
      <#-- CONTENT-->
      <div class="col-lg-6">

        <#-- TAB Panel or Panel Wrapper -->
        <div <#if international>role="tabpanel" class="panel panel-transparent" data-type="tabpanel-language" <#else>class="panel panel-default"</#if>>

          <@spring.bind "coupon.name"/>
          <#assign tabPath = spring.status.expression/>
          <#-- TAB LANGUAGE HEADER -->
          <@ui.tabListByLanguage tabPath/>
          <#-- END : TAB LANGUAGE HEADER -->

          <#-- TAB LANGUAGE BODY -->
          <div <#if international>class="tab-content bg-white" <#else>class="panel-body"</#if>>

            <#-- 국문 -->
            <#if im.koKr>
              <div id="${tabPath}-ko" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Coupon Name (Message) -->
                <@ui.formInputTextByLanguage "coupon.name.textKoKr" "Coupon Name (Message)" true 255 "Enter Coupon Name (Message)"/>

                <@ui.formActiveByLanguage "coupon.internationalMode.koKr" />
              </div>
            <#else>
              <@spring.formHiddenInput "coupon.name.textKoKr"/>
            </#if>
            <#-- END : 국문 -->
            <#-- 영문 -->
            <#if im.enUs>
              <div id="${tabPath}-en" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Coupon Name (Message) -->
                <@ui.formInputTextByLanguage "coupon.name.textEnUs" "Coupon Name (Message)" true 255 "Enter Coupon Name (Message)"/>

                <@ui.formActiveByLanguage "coupon.internationalMode.enUs"/>
              </div>
            <#else>
              <@spring.formHiddenInput "coupon.name.textEnUs"/>
            </#if>
            <#-- END : 영문 -->
            <#-- 중문 (간체) -->
            <#if im.zhCn>
              <div id="${tabPath}-zh-cn" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Coupon Name (Message) -->
                <@ui.formInputTextByLanguage "coupon.name.textZhCn" "Coupon Name (Message)" true 255 "Enter Coupon Name (Message)"/>

                <@ui.formActiveByLanguage "coupon.internationalMode.zhCn"/>
              </div>
            <#else>
              <@spring.formHiddenInput "coupon.name.textZhCn"/>
            </#if>
            <#-- END : 중문 (간체) -->
            <#-- 중문 (번체) -->
            <#if im.zhTw>
              <div id="${tabPath}-zh-tw" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Coupon Name (Message) -->
                <@ui.formInputTextByLanguage "coupon.name.textZhTw" "Coupon Name (Message)" true 255 "Enter Coupon Name (Message)"/>

                <@ui.formActiveByLanguage "coupon.internationalMode.zhTw"/>
              </div>
            <#else>
              <@spring.formHiddenInput "coupon.name.textZhTw"/>
            </#if>
            <#-- END : 중문 (번체) -->
            <#-- 일문 -->
            <#if im.jaJp>
              <div id="${tabPath}-ja" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Coupon Name (Message) -->
                <@ui.formInputTextByLanguage "coupon.name.textJaJp" "Coupon Name (Message)" true 255 "Enter Coupon Name (Message)"/>

                <@ui.formActiveByLanguage "coupon.internationalMode.jaJp"/>
              </div>
            <#else>
              <@spring.formHiddenInput "coupon.name.textJaJp"/>
            </#if>
            <#-- END : 일문 -->
          </div>
        </div>
        <#-- END : TAB LANGUAGE BODY -->

        <#-- Issue Type Settings -->
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Issue Type</h4></div>
          <div class="panel-body">

            <@ui.formRadioboxEnum "coupon.type" "Issue Type" true types/>

            <#-- (Type이 수동Issue/Download/자동Issue/생일 일때 만) -->
            <div data-role="coupon-type-user-mode">
              <hr/>

              <#-- membership grade-->
              <div class="form-group">
                <label class="control-label">membership grade </label>

                <@spring.bind path="coupon.buyerLevels"/>
                <select name="${spring.status.expression}" data-type="chosen-select" data-placeholder="Search and select member level" multiple class="form-control">
                  <#--<option value=""></option>-->
                  <#list levels as item>
                    <option value="${item.id?c}" <#if item.checked>selected</#if>>${item.name.value}</option>
                  </#list>
                </select>
              </div>


              <#-- End : Member Settings -->
            </div>


            <#-- (Type이 Download/수동Issue 일때 만) -->
            <div data-role="coupon-type-period">
              <label style="font-size:1.1em;">Coupon Available Period</label>
              <div class="row">
                <div class="col-lg-6">
                  <@ui.formDate "coupon.startDate" "시작일" false "YYYY-MM-DD"/>
                </div>
                <div class="col-lg-6">
                  <@ui.formDate "coupon.endDate" "종료일" false "YYYY-MM-DD"/>
                </div>
              </div>
            </div>

          </div>
        </div>
        <#-- End : Issue Type Settings -->

      </div>
      <#-- END : CONTENT-->
      <#-- SIDBAR -->
      <div class="col-lg-6 pl0-lg">

        <#-- benefit classification Settings -->
        <div class="panel panel-default">
          <div class="panel-heading"><h4>benefit classification</h4></div>
          <div class="panel-body">

            <div data-role="coupon-discount-fixed">
              <@ui.formInputText "coupon.discountFixed" "Amount" false 20 "Please enter the amount." "text" "data-parsley-type=\"number\"" "Purchases can be made at a discount on Fixed Amount."/>
            </div>


          </div>
        </div>
        <#-- END : benefit classification Settings -->

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Setting</h4></div>
          <div class="panel-body">

            <@ui.formActive "coupon.active"/>

          </div>
        </div>

      </div>
      <#-- END : SIDBAR -->
    </div>
  </form>
</div>
