<!-- Modal -->
<div data-modal-relattion-product="" id="modelRelationProduct" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

    <#-- Header -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Product Selection</h4>
      </div>

    <#-- Body -->
      <div class="modal-body">

      <#-- 필터 -->
        <div class="row">
          <div class="col-md-12">

            <div id="" class="panel panel-default panel-list-filter">
              <div class="panel-heading">검색 필터
                <a href="#" data-tool="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right"><em class="fa fa-minus"></em></a>
                <a @click="handleOnClickResetCategory" href="javascript:void(0);" data-tool="" data-toggle="tooltip" title="" class="pull-right"><em class="fa fa-refresh"></em></a>
              </div>

              <div class="panel-wrapper collapse in">
                <div class="panel-body">
                  <form id="form-list-filter" action="" method="get">
                  <#-- 검색어 -->
                    <fieldset>
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="form-group">
                            <label class="col-xs-2 control-label">검색어</label>

                            <div class="col-xs-10">
                              <input name="query" type="text" class="form-control" placeholder="검색어를 입력하세요." v-model="searchParam.query">
                            </div>
                          </div>
                        </div>
                      </div>
                    </fieldset>
                  <#-- Category -->
                    <fieldset>
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="form-group">
                            <label class="col-sm-2 control-label">Category</label>

                            <div class="col-sm-10">
                            <#-- Category1 -->
                              <select v-model="category.value1" name="idCategory" class="form-control" style="display: inline-block; max-width: 120px;">
                                <option value="">None</option>
                                <option v-for="(item, index) in category.group1" :value="item">{{item.content}}</option>
                              </select>
                            <#-- 카테고리2 -->
                              <select v-model="category.value2" name="idCategory" class="form-control" style="display: inline-block; max-width: 120px;">
                                <option value="">None</option>
                                <option v-for="(item, index) in category.group2" :value="item">{{item.content}}</option>
                              </select>
                            <#-- 카테고리3 -->
                              <select v-model="category.value3" name="idCategory" class="form-control" style="display: inline-block; max-width: 120px;">
                                <option value="">None</option>
                                <option v-for="(item, index) in category.group3" :value="item">{{item.content}}</option>
                              </select>
                            <#-- 카테고리4 -->
                              <select v-model="category.value4" name="idCategory" class="form-control" style="display: inline-block; max-width: 120px;">
                                <option value="">None</option>
                                <option v-for="(item, index) in category.group4" :value="item">{{item.content}}</option>
                              </select>
                            <#-- 카테고리5 -->
                              <select v-model="category.value5" name="idCategory" class="form-control" style="display: inline-block; max-width: 120px;">
                                <option value="">None</option>
                                <option v-for="(item, index) in category.group5" :value="item">{{item.content}}</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </fieldset>
                  <#-- Brand -->
                    <fieldset>
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="form-group">
                            <label class="col-xs-2 control-label">Brand</label>

                            <div class="col-xs-5">
                              <select name="idBrand" data-type="chosen-select" data-placeholder="Please search for and select a brand." class="form-control" v-model="searchParam.idBrand">
                                <option value=""></option>
                                <option value="">All</option>
                              <#list brands as item>
                                <option value="${item.id?c}" <#if idBrand?has_content && idBrand == item.id>selected</#if>>${item.name.value}</option>
                              </#list>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              <#-- 필터 실행 -->
                <div class="panel-footer">
                  <button @click="handleOnClickSearch($event)" type="button" class="btn btn-default">검색</button>
                </div>
              <#-- END : 필터 실행 -->
              </div>
            </div>
          </div>
        </div>
      <#-- END : 필터 -->

      <#-- 목록 -->
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default product-list">

            <#-- START table-responsive-->
              <div class="table-responsive">
                <table id="table-ext-1" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th class="text-center" width="5%"><input type="checkbox" @change="handleOnChangeCheckAll"/></th>
                    <th class="text-center" style="width: 10%;">Thumbnail</th>
                    <th class="text-center" width="15%">Brand</th>
                    <th class="text-center" width="55%">Product Name</th>
                    <th class="text-center" width="10%">Price</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr v-if="!productsData.length"><td colspan="5" class="text-center p-lg">No items have been retrieved.</td></tr>
                  <tr v-for="(item, index) in productsData">
                    <td class="text-center"><input type="checkbox" v-model="checkProduct" :value="item"/></td>
                    <td class="text-center"><img :src="item.thumbnail" class="img-responsive text-center"/></td>
                    <td class="text-center">{{item.brand}}</td>
                    <td class="text-left">{{item.name}}</td>
                    <td class="text-right">{{item.price}}</td>
                  </tr>
                  </tbody>
                </table>
              </div>
            <#-- END table-responsive-->
            <#-- TABLE FOOTER -->
              <div v-if="isProgress || pageInfo.totalPages > (pageInfo.number + 1)" class="panel-footer">
                <div class="text-center">
                  <div v-if="isProgress" class="p-lg"><i class="fa fa-circle-o-notch fa-spin" style="font-size: 20px;"></i></div>
                  <button v-if="pageInfo.totalPages > (pageInfo.number + 1)" class="btn btn-default" type="button" @click="handleOnClickMore($event)">More</button>
                </div>
              </div>
            <#-- END : TABLE FOOTER -->
            </div>
          </div>
        </div>
      <#-- END : 목록 -->

      </div>

    <#-- Footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" @click="handleOnClickApply">Select Complete</button>
      </div>
    </div>
  </div>
</div>