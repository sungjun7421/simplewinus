<#--<#assign pageparam = "&key=value"/>-->
<#assign pageparam = ""/>
<div class="container-fluid">

  <#-- 필터 -->
  <div class="row">
    <div class="col-md-12">
      <div id="" class="panel panel-default panel-list-filter">
        <div class="panel-heading">검색 필터

          <a href="#" data-tool="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
            <em class="fa fa-minus"></em>
          </a>

          <a id="initial-list-filter" href="javascript:void(0);" data-tool="panel-refresh" data-toggle="tooltip" title="" class="pull-right">
            <em class="fa fa-refresh"></em>
          </a>

        </div>
        <div class="panel-wrapper collapse in">
          <div class="panel-body">
            <form id="form-list-filter" action="" method="get">
              <#include "/admin/common/list/fieldset-common.ftl"/>
              <#include "/admin/common/list/fieldset-date.ftl"/>
              <#include "/admin/common/list/fieldset-search.ftl"/>
              <fieldset>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">Issue Type</label>

                      <div class="col-xs-10">

                        <div class="form-group">
                          <div <#--style="font-size: 0.85em"-->>
                            <label class="radio-inline c-radio">
                              <input type="radio" name="type" value="" <#if !type?has_content>checked</#if>>
                              <span class="fa fa-circle"></span>All
                            </label>
                            <#list types as item>
                              <label class="radio-inline c-radio">
                                <input data-type="mode" type="radio" name="type" value="${item}"
                                       <#if type?has_content && type == item>checked</#if>>
                                <span class="fa fa-circle"></span>${item.value}
                              </label>
                            </#list>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
          <#-- 필터 실행 -->
          <div class="panel-footer">

            <button id="submit-list-filter" type="button" class="btn btn-primary">검색</button>

            <div class="btn-group">
              <button type="button" data-toggle="dropdown" class="btn dropdown-toggle btn-default">${data.pageSize}개씩 보기 <span class="caret"></span></button>
              <ul role="menu" class="dropdown-menu">
                <li><a href="<@spring.url "?query=${data.query!}&size=10&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 10</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=20&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 20</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=50&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 50</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=100&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 100</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=500&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 500</a></li>
              </ul>
            </div>


            <#--<button id="excel-list-filter" type="button" data-action="<@spring.url "/admin/board/blogCategory/excel"/>" class="btn btn-labeled btn-default">-->
            <#--<span class="text">엑셀 Download</span>-->
            <#--<span class="btn-label btn-label-right"><i class="fa fa-file-excel-o"></i></span>-->
            <#--</button>-->
          </div>
          <#-- END : 필터 실행 -->
        </div>
      </div>
    </div>
  </div>
  <#-- END : 필터 -->
  <#-- META INFORMATION-->
<#--  <#if buyer?has_content>-->
<#--    <div class="row">-->
<#--      <div class="col-md-12">-->

<#--        <div class="panel panel-default">-->
<#--          <div class="panel-heading"><b style="font-size:18px;">${buyer.relativeUser.fullName}</b> 님의 Coupon 보유 Information</div>-->

<#--          <div class="table-responsive table-responsive-full">-->
<#--            <table id="table-ext-1" class="table table-bordered table-hover">-->
<#--              <colgroup>-->
<#--                <col width="40%">-->
<#--                <col width="30%">-->
<#--                <col width="30%">-->
<#--              </colgroup>-->
<#--              <thead>-->
<#--              <tr>-->
<#--                <th class="text-center">Total 보유한 Coupon 수</th>-->
<#--                <th class="text-center">사용 가능한 Coupon 수</th>-->
<#--                <th class="text-center">Used Coupon 수</th>-->
<#--              </tr>-->
<#--              </thead>-->
<#--              <tbody>-->
<#--              <tr>-->
<#--                <td class="text-center">${total} </td>-->
<#--                <td class="text-center">${unusedTotal} </td>-->
<#--                <td class="text-center">${usedTotal} </td>-->
<#--              </tr>-->
<#--              </tbody>-->
<#--            </table>-->
<#--          </div>-->

<#--        </div>-->

<#--      </div>-->
<#--    </div>-->
<#--  </#if>-->
  <#-- END : META INFORMATION-->
  <#-- HEADER -->
  <#assign createparam = header.url +"/create"/>
  <#include "/admin/common/list/header.ftl"/>
  <#-- END : HEADER -->
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

        <#-- START table-responsive-->
        <div class="table-responsive">
          <table id="table-ext-1" class="table table-bordered table-hover">

            <colgroup>
              <col width="5%">

              <col width="15%">
              <col width="10%">
              <col width="10%">
              <col width="15%">
              <col width="10%">
              <col width="10%">
              <col width="10%">
            </colgroup>

            <thead>
            <tr>

              <th class="text-center">#</th>
              <th class="text-center">Name</th>
              <th class="text-center">Coupon Code</th>
              <th class="text-center">Issue Type</th>
              <th class="text-center">Amount</th>
              <th class="text-center">Active</th>
              <th class="text-center">Reg. Date</th>
              <th class="text-center">Action</th>

            </tr>
            </thead>
            <tbody>
            <#if data?has_content>
              <#list data.page.content as item>

                <tr>
                  <td class="text-center">no.${data.firstNo- (item_index + 1)}<br/>id.${item.id?c}</td>

                  <#--                  <td class="text-center">-->
                  <#--                    <#if item.image?has_content>-->
                  <#--                      <img src="${item.image}" class="img-responsive text-center" style="max-height: 60px; display: inline-block;"/>-->
                  <#--                    <#else>-</#if>-->
                  <#--                  </td>-->
                  <td class="text-left">
                    <#if international>
                      <ul <#--style="padding-left: 13px; margin-bottom: 0px"-->>
                        <#if im.koKr>
                        <li <#if !item.internationalMode.koKr>class="inactive"</#if>>Ko : <#if item.internationalMode.koKr>${item.name.textKoKr!}<#else>Inactive</#if></li></#if>
                        <#if im.enUs>
                        <li <#if !item.internationalMode.enUs>class="inactive"</#if>>En : <#if item.internationalMode.enUs>${item.name.textEnUs!}<#else>Inactive</#if></li></#if>
                        <#if im.zhCn>
                        <li <#if !item.internationalMode.zhCn>class="inactive"</#if>>Cn S. : <#if item.internationalMode.zhCn>${item.name.textZhCn!}<#else>Inactive</#if></li></#if>
                        <#if im.zhTw>
                        <li <#if !item.internationalMode.zhTw>class="inactive"</#if>>Cn T. : <#if item.internationalMode.zhTw>${item.name.textZhTw!}<#else>Inactive</#if></li></#if>
                        <#if im.jaJp>
                        <li <#if !item.internationalMode.jaJp>class="inactive"</#if>>Ja : <#if item.internationalMode.jaJp>${item.name.textJaJp!}<#else>Inactive</#if></li></#if>
                      </ul>
                    <#else>
                      ${item.name.value!}
                    </#if>
                  </td>
                  <td class="text-center">

                      ${item.code!}

                  </td>
                  <td class="text-center">${item.type.value}</td>

                  <td class="text-center">
                    <#if item.type == 'DISCOUNT_PERCENT'>
                      ${item.discountFixed}%
                    <#elseif item.type == 'GIFT_POINT'>
                      ${item.discountFixed}Point
                    <#else>
                      ${item.discountFixed}KRW
                    </#if>
                  </td>

                  <td class="text-center"><#if item.active>
                      <div class="label label-success">Active</div><#else>
                      <div class="label label-warning">Inactive</div></#if></td>
                  <td class="text-center">
                    <ul class="list-unstyled">
                      <li data-toggle="tooltip" data-placement="top" title="Reg. Date">${item.createdDate.format('yyyy.MM.dd HH:mm:ss')}</li>
                      <li data-toggle="tooltip" data-placement="top" title="Last Modify Date">${item.updatedDate.format('yyyy.MM.dd HH:mm:ss')}</li>
                    </ul>
                  </td>
                  <td class="text-center">
                    <#if item.type == 'DEFAULT'>
                      <a href="<@spring.url header.url + "/issue/${item.id?c}"/>" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="Coupon Direct Issue">
                        <em class="fa fa-upload"></em>
                      </a>
                    </#if>
                    <a href="<@spring.url header.url + "/update/${item.id?c}"/>" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="View and Edit">
                      <em class="fa fa-pencil"></em>
                    </a>
                  </td>
                </tr>
              </#list>
            </#if>
            </tbody>
          </table>
        </div>
        <#-- END table-responsive-->

        <#-- TABLE FOOTER -->
        <div class="panel-footer">
          <div class="row">
            <div class="col-lg-12 text-center">
              <#include "/admin/common/list/pagination.ftl"/>
            </div>
          </div>
        </div>
        <#-- END : TABLE FOOTER -->
      </div>
    </div>
  </div>
</div>
