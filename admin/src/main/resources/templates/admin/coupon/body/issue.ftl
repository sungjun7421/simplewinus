<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div id="issue-coupon" class="container-fluid coupon-issue">
    <#-- FORM -->
  <form id="form-issue-coupon" action="<@spring.url header.url + "/issue"/>" method="post" data-parsley-validate="" novalidate="" data-parsley-international="true">
      <#-- Modify Complete -->
      <#include "../../common/modify-success.ftl"/>
      <#-- END : Modify Complete -->
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
            <#if coupon.active>
              <button type="submit" class="btn btn-primary btn-lg">
                Issue
              </button>
            <#else>
              <button type="submit" class="btn btn-primary btn-lg" disabled="disabled">
                Issue
              </button>
              <p class="text-danger text-bold mt">The coupon status is inactive.</p>
            </#if>
            <@spring.formHiddenInput "coupon.id"/>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top" title="List">
            <span class="icon-list"></span>
          </a>
        </div>

      </div>
    </div>
    <div class="row">
        <#-- CONTENT-->
      <div class="col-lg-6">

          <#-- CouponInformation -->
        <div class="panel panel-default">
          <div class="panel-heading"><h4>CouponInformation</h4></div>
          <div class="panel-body">
            <div class="form-group">
              <label class="control-label">Coupon Name (Message)</label>
                <#if international>
                  <ul style="padding-left: 13px; margin-bottom: 0px">
                      <#if im.koKr>
                      <li <#if !coupon.internationalMode.koKr>class="inactive"</#if>>Ko : <#if coupon.internationalMode.koKr>${coupon.message.textKoKr!}<#else>Inactive</#if></li></#if>
                      <#if im.enUs>
                      <li <#if !coupon.internationalMode.enUs>class="inactive"</#if>>En : <#if coupon.internationalMode.enUs>${coupon.message.textEnUs!}<#else>Inactive</#if></li></#if>
                      <#if im.zhCn>
                      <li <#if !coupon.internationalMode.zhCn>class="inactive"</#if>>Cn S. : <#if coupon.internationalMode.zhCn>${coupon.message.textZhCn!}<#else>Inactive</#if></li></#if>
                      <#if im.zhTw>
                      <li <#if !coupon.internationalMode.zhTw>class="inactive"</#if>>Cn T. : <#if coupon.internationalMode.zhTw>${coupon.message.textZhTw!}<#else>Inactive</#if></li></#if>
                      <#if im.jaJp>
                      <li <#if !coupon.internationalMode.jaJp>class="inactive"</#if>>Ja : <#if coupon.internationalMode.jaJp>${coupon.message.textJaJp!}<#else>Inactive</#if></li></#if>
                      <#if im.viVn>
                      <li <#if !coupon.internationalMode.viVn>class="inactive"</#if>>Vn : <#if coupon.internationalMode.viVn>${coupon.message.textViVn!}<#else>Inactive</#if></li></#if>
                  </ul>
                <#else>
                  <p class="form-control-static h4 m0">${coupon.message.value!}</p>
                </#if>
            </div>
          </div>
        </div>
          <#-- END : CouponInformation -->

          <#-- Issue Type Settings -->
        <div class="panel panel-default">
          <div class="panel-heading"><h4>Issue Type</h4></div>
          <div class="panel-body">

            <div class="form-group">
              <label class="control-label">Issue Type</label>
              <p class="form-control-static h4 m0">${coupon.type.value}</p>
            </div>

            <hr/>
            <div class="form-group">
              <label class="control-label">Member Mode</label>
              <p class="form-control-static h4 m0">${coupon.userMode.value}</p>
            </div>

              <#if coupon.userMode == 'LEVEL'>
                <div class="form-group">
                  <label class="control-label">membership grade</label>
                  <p class="form-control-static h4 m0">
                      <#list coupon.buyerLevels as level>
                          ${level.name.value} [lv.${level.level}]<#sep>, </#sep>
                      <#else><span class="label label-danger">Not selected</span>
                      </#list>
                  </p>
                </div>
              <#elseif coupon.userMode == 'PERSONAL'>
                  <#if coupon.users?has_content>
                    <div class="table-responsive">
                      <hr class="m0"/>
                      <table class="table">
                        <thead>
                        <tr>
                          <th width="30%">Name</th>
                          <th>Email</th>
                          <th width="20%">PhoneNumber</th>
                        </tr>
                        </thead>
                        <tbody>
                        <#list coupon.users as user>
                          <tr>
                            <td class="text-left">${user.fullname}</td>
                            <td class="text-left">${user.email}</td>
                            <td class="text-left">${user.mobile!"-"}</td>
                          </tr>
                        <#else><span class="label label-danger">Not selected</span>
                        </#list>
                        </tbody>
                      </table>
                    </div>
                  <#else><p class="h4"><span class="label label-danger">Not selected</span></p></#if>
              </#if>

            <hr/>
            <div class="form-group">
              <label class="control-label">Coupon Available Period</label>
              <p class="form-control-static h4 m0">
                  <#if coupon.startDate?has_content && coupon.endDate?has_content>
                      ${coupon.startDate.format("yyyy.MM.dd")}
                    ~
                      ${coupon.endDate.format("yyyy.MM.dd")}
                  <#else><span class="label label-danger">Not entered</span></#if>
              </p>
            </div>

          </div>
        </div>
          <#-- End : Issue Type Settings -->

      </div>
        <#-- END : CONTENT-->

        <#-- SIDBAR -->
      <div class="col-lg-6 pl0-lg">

          <#--        <div class="panel panel-default">-->
          <#--          <div class="panel-heading"><h4>Image</h4></div>-->
          <#--          <div class="panel-body">-->

          <#--            <div class="form-group">-->
          <#--              <label class="control-label">Coupon Image</label>-->
          <#--              <p class="form-control-static h4 m0">-->
          <#--                <#if coupon.image?has_content>-->
          <#--                  <a href="<@spring.url "${coupon.image}"/>" download="image-${coupon.id?c}">-->
          <#--                    <img src="${coupon.image}" class="img-responsive text-center" style="max-height: 60px; display: inline-block;"/>-->
          <#--                  </a>-->
          <#--                </#if>-->
          <#--              </p>-->
          <#--              <span class="help-block mb0">* 클릭시 Download됩니다.</span>-->
          <#--            </div>-->
          <#--          </div>-->
          <#--        </div>-->

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Kind</h4></div>
          <div class="panel-body">
            <div class="form-group">
              <label class="control-label">Coupon Kind</label>
              <p class="form-control-static h4 m0">${coupon.kind.value}</p>
            </div>
          </div>
        </div>

          <#-- benefit classification Settings -->
        <div class="panel panel-default">
          <div class="panel-heading"><h4>benefit classification</h4></div>
          <div class="panel-body">

            <div class="form-group">
              <label class="control-label">Discount Mode</label>
              <p class="form-control-static h4 m0">${coupon.discountMethod.value}</p>
            </div>

            <div class="form-group">
              <label class="control-label">benefit classification</label>
                <#if coupon.discountMethod == 'PRICE'>
                  <p class="form-control-static h4 m0">
                      <#if coupon.discountPrice?has_content>
                          ${coupon.discountPrice!} (${mall.currency})
                      <#else><span class="label label-danger">Not entered</span></#if>
                  </p>
                  <span class="help-block mb0">* You can purchase it with the Certificate Mount.</span>
                <#elseif coupon.discountMethod == 'FIXED_RATE'>
                  <p class="form-control-static h4 m0">
                      <#if coupon.discountExpression?has_content>
                        Discount formula: ${coupon.discountExpression!} = ${coupon.discountRate!}% DC
                      <#else><span class="label label-danger">Not entered</span></#if>
                  </p>
                  <span class="help-block mb0">* You can purchase at a discount rate.</span>
                <#elseif coupon.discountMethod == 'FIXED_AMOUNT'>
                  <p class="form-control-static h4 m0">
                      <#if coupon.discountFixed?has_content>
                          ${coupon.discountFixed!} (${mall.currency})
                      <#else><span class="label label-danger">Not entered</span></#if>
                  </p>
                  <span class="help-block mb0">* Purchases can be made at a discount on Fixed Amount.</span>
                <#elseif coupon.discountMethod == 'POINT'>
                  <p class="form-control-static h4 m0">
                      <#if coupon.discountPoint?has_content>
                          ${coupon.discountPoint!} p
                      <#else><span class="label label-danger">Not entered</span></#if>
                  </p>
                  <span class="help-block mb0">* This is Coupon (DiscountMode) that gets points.</span>
                </#if>
            </div>

          </div>
        </div>
          <#-- END : benefit classification Settings -->

          <#-- Target Settings -->
          <#if coupon.kind == '상품할인'>
            <div class="panel panel-default">
              <div class="panel-heading"><h4>Target</h4></div>
              <div class="panel-body">

                <div class="form-group">
                  <label class="control-label">Target Mode</label>
                  <p class="form-control-static h4 m0">${coupon.target.value}</p>
                </div>

                  <#if coupon.target == 'PRODUCT'>
                      <#if coupon.products?has_content>
                        <div class="table-responsive">
                          <hr class="m0"/>
                          <table class="table">
                            <thead>
                            <tr>
                              <th class="text-center" width="30%">Thumbnail</th>
                              <th>Product Name</th>
                              <th class="text-right" width="20%">Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#list coupon.products as item>
                              <tr>
                                <td class="text-center"><#if item.thumbnail?has_content><img src="${item.thumbnail}" class="img-responsive" style="max-height: 60px;display: inline-block;"/><#else><span>-</span></#if></td>
                                <td class="text-left">${item.name.value!}</td>
                                <td class="text-right">${item.productionCost!} (${mall.currency})</td>
                              </tr>
                            </#list>
                            </tbody>
                          </table>
                          <hr class="mt0"/>
                        </div>
                      <#else><p class="h4"><span class="label label-danger">Not selected</span></p></#if>
                  <#elseif coupon.target == 'CATEGORY'>
                    <div class="form-group">
                      <label class="control-label">Selected Category</label>
                      <p class="form-control-static h4 m0">
                          <#list coupon.categoryValueList as item>
                              ${item.name!} <#sep>&gt;</#sep>
                          <#else><span class="label label-danger">Not selected</span>
                          </#list>
                      </p>
                    </div>
                  <#elseif coupon.target == 'BRAND'>
                    <div class="form-group">
                      <label class="control-label">Selected Brand</label>
                      <p class="form-control-static h4 m0">
                          <#list coupon.brands as item>
                              ${item.name.value}<#sep>,</#sep>
                          <#else><span class="label label-danger">Not selected</span>
                          </#list>
                      </p>
                    </div>
                  <#elseif coupon.target == 'PROMOTION'>
                    <div class="form-group">
                      <label class="control-label">Selected Promotion</label>
                      <p class="form-control-static h4 m0">
                          <#list coupon.groupings as item>
                              ${item.name.value}<#sep>,</#sep>
                          <#else><span class="label label-danger">Not selected</span>
                          </#list>
                      </p>
                    </div>
                  </#if>

              </div>
            </div>
          </#if>
          <#-- END : Target Settings -->

          <#-- Usage policy Settings -->
          <#if coupon.discountMethod != 'POINT'>
            <div class="panel panel-default">
              <div class="panel-heading"><h4>Usage policy</h4></div>
              <div class="panel-body">

                <div class="form-group">
                  <label class="control-label">Minimum purchase amount</label>
                  <p class="form-control-static h4 m0"><#if coupon.minAmount?has_content>${coupon.minAmount} (${mall.currency})<#else>No usage restrictions.</#if></p>
                  <span class="help-block mb0">* Minimum purchase amount for use with Coupon.</span>
                </div>

                <div class="form-group">
                  <label class="control-label">Maximum Discount Amount</label>
                  <p class="form-control-static h4 m0"><#if coupon.maxDiscount?has_content>${coupon.maxDiscount} (${mall.currency})<#else>No usage restrictions.</#if></p>
                  <span class="help-block mb0">* The maximum discount you can get using Coupon.</span>
                </div>
              </div>
            </div>
          </#if>
          <#-- End : Usage policy Settings -->

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Setting</h4></div>
          <div class="panel-body">

              <#if coupon.discountMethod != 'POINT'>
                <div class="form-group">
                  <label class="control-label">Duplicate Use Mode</label>
                  <p class="form-control-static h4 m0"><#if coupon.duplication>
                      <span class="label label-success">Yes</span><#else>
                      <span class="label label-warning">No</span></#if></p>
                </div>
              </#if>

            <hr/>
            <div class="form-group">
              <label class="control-label">Active Mode</label>
              <p class="form-control-static h4 m0"><#if coupon.active>
                  <span class="label label-success">Active</span><#else>
                  <span class="label label-warning">Inactive</span></#if></p>
            </div>

          </div>
        </div>

          <#-- Meta Information (Modify 페이지 필수) -->
          <@ui.panelMetaInfo coupon.updatedDate coupon.createdDate/>
          <#-- END : Meta Information (Modify 페이지 필수) -->

      </div>
        <#-- END : SIDBAR -->
    </div>
  </form>
</div>