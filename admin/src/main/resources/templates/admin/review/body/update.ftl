<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div id="update-qna" class="container-fluid qna-create-update">
    <#-- FORM -->
    <form id="form-update-qna" action="<@spring.url header.url + "/update"/>" method="post" data-parsley-validate=""
          novalidate="">
        <#-- 수정 완료 -->
        <#include "../../common/modify-success.ftl"/>
        <#-- END : 수정 완료 -->
        <div class="row">
            <div class="col-md-12">

                <div class="pull-left mb-lg">
                    <button type="submit" class="btn btn-primary btn-lg">
                        수정
                    </button>
                    <@spring.formHiddenInput "review.id"/>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </div>

                <div class="pull-right mb-lg">
                    <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top"
                       title="목록보기">
                        <span class="icon-list"></span>
                    </a>
                    <button data-type="btn-delete" type="button" class="btn btn-danger btn-lg" data-toggle="tooltip"
                            data-placement="top" title="삭제"
                            data-action="<@spring.url header.url + "/delete"/>"
                            data-id="${review.id?c}">
                        <span class="icon-trash"></span>
                    </button>
                </div>

            </div>
        </div>
        <div class="row">
            <#-- CONTENT-->
            <div class="col-lg-7">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <hr/>

                        <#-- WYSISWYG EDITOR -->
                        <@ui.wysiswygEdior "review.content" "답변" true/>
                        <#-- END : WYSISWYG EDITOR -->

                        <hr/>

                        <#-- WYSISWYG EDITOR -->
                        <@ui.wysiswygEdior "review.reply" "Reply" true/>
                        <#-- END : WYSISWYG EDITOR -->

                    </div>
                </div>

            </div>
            <#-- END : CONTENT-->
            <#-- SIDBAR -->
            <div class="col-lg-5 pl0-lg">

                <div class="panel panel-default">
                    <div class="panel-heading">설정</div>
                    <div class="panel-body">

                        <@ui.formActive "review.active"/>
                    </div>
                </div>

                <#-- 메타 정보 (수정 페이지 필수) -->
                <@ui.panelMetaInfo review.updatedDate review.createdDate/>
                <#-- END : 메타 정보 (수정 페이지 필수) -->

            </div>
            <#-- END : SIDBAR -->
        </div>
    </form>
</div>
