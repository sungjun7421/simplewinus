<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div id="view-push-history" class="container-fluid push-history-view">
<#-- FORM -->

    <div class="row">
        <div class="col-md-12">

            <div class="pull-right mb-lg">
                <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top" title="List">
                    <span class="icon-list"></span>
                </a>
            </div>

        </div>
    </div>
    <div class="row">
    <#-- CONTENT-->
        <div class="col-lg-7">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label">Device Token </label>
                        <p class="form-control-static">${pushHistory.deviceToken!}</p>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label class="control-label">Content </label>
                        <p class="form-control-static">${pushHistory.content!}</p>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label class="control-label">Receiver </label>
                        <p class="form-control-static">${pushHistory.relativeUser.email!} </p>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label class="control-label">Push Time </label>
                        <p class="form-control-static">${pushHistory.pushTime!}</p>
                    </div>

                </div>
            </div>

        </div>
    <#-- END : CONTENT-->
    <#-- SIDBAR -->
        <div class="col-lg-5 pl0-lg">

        <#-- Meta Information (Modify 페이지 필수) -->
        <@ui.panelMetaInfo pushHistory.updatedDate pushHistory.createdDate/>
        <#-- END : Meta Information (Modify 페이지 필수) -->
        </div>
    <#-- END : SIDBAR -->
    </div>

</div>
