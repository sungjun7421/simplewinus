
<#if deviceToken?has_content>
    <#assign pageparam = "&deviceToken=${deviceToken!}"/>
</#if>
<div class="container-fluid">
<#-- 필터 -->
    <div class="row">
        <div class="col-md-12">
            <div id="" class="panel panel-default panel-list-filter">
                <div class="panel-heading">필터링

                    <a href="#" data-tool="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
                        <em class="fa fa-minus"></em>
                    </a>

                    <a id="initial-list-filter" href="javascript:void(0);" data-tool="panel-refresh" data-toggle="tooltip" title="" class="pull-right">
                        <em class="fa fa-refresh"></em>
                    </a>

                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <form id="form-list-filter" action="" method="get">
                        <#include "../../common/list/fieldset-common.ftl"/>
                        <#include "../../common/list/fieldset-date.ftl"/>
                        <#include "../../common/list/fieldset-search.ftl"/>
                        </form>
                    </div>
                <#-- 필터 실행 -->
                    <div class="panel-footer">

                        <button id="submit-list-filter" type="button" class="btn btn-primary">검색하기</button>

                        <div class="btn-group">
                            <button type="button" data-toggle="dropdown" class="btn dropdown-toggle btn-default">${data.pageSize}개 보기 <span class="caret"></span></button>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="<@spring.url "?query=${data.query!}&size=10&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">10개 보기</a></li>
                                <li><a href="<@spring.url "?query=${data.query!}&size=20&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">20개 보기</a></li>
                                <li><a href="<@spring.url "?query=${data.query!}&size=50&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">50개 보기</a></li>
                                <li><a href="<@spring.url "?query=${data.query!}&size=100&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">100개 보기</a></li>
                                <li><a href="<@spring.url "?query=${data.query!}&size=500&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">500개 보기</a></li>
                            </ul>
                        </div>

                    </div>
                <#-- END : 필터 실행 -->
                </div>
            </div>
        </div>
    </div>
<#-- END : 필터 -->
<#-- HEADER -->
<#assign createparam = "/admin/push"/>
<#include "../../common/list/header.ftl"/>
<#-- END : HEADER -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

            <#-- START table-responsive-->
                <div class="table-responsive">
                    <table id="table-ext-1" class="table table-bordered table-hover">
                        <colgroup>
                            <col width="5%">
                            <col width="15%">
                            <col width="20%">
                            <col width="15%">
                            <col width="20%">
                            <col width="5%">
                        </colgroup>
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Device Token</th>
                            <th class="text-center">Title</th>
                            <th class="text-center">Content</th>
                            <th class="text-center">Push Time</th>
                            <th class="text-center">Receiver</th>
                            <th style="width: 80px" class="text-center">작업</th>
                        </tr>
                        </thead>
                        <tbody>

                        <#if data?has_content>
                            <#list data.page.content as item>
                            <tr>
                                <td class="text-center">no.${data.firstNo- (item_index + 1)}<br/>id.${item.id?c}</td>
                                <td class="text-center">${item.deviceToken!}</td>
                                <td class="text-center">${item.title!}</td>
                                <td class="text-center">${item.content!}</td>
                                <td style="width: 200px" class="text-center">${item.pushTime.format('yyyy.MM.dd HH:mm:ss')}</td>
                                <td class="text-center">
                                    <#if item.relativeUser?has_content>
                                       <a href="<@spring.url "/admin/user/${item.relativeUser.id?c}"/>">${item.relativeUser.email} [${item.relativeUser.fullName}]</a>
                                    <#else>
                                        -
                                    </#if>
                                </td>
                                <td style="width: 80px" class="text-center">
                                    <a href="<@spring.url header.url + "/view/${item.id?c}"/>" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="View">
                                        <em class="fa fa-search"></em>
                                    </a>
                                </td>
                            </tr>
                            </#list>
                        </#if>
                        </tbody>
                    </table>
                </div>
            <#-- END table-responsive-->

            <#-- TABLE FOOTER -->
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                        <#include "../../common/list/pagination.ftl"/>
                        </div>
                    </div>
                </div>
            <#-- END : TABLE FOOTER -->
            </div>
        </div>
    </div>
</div>
