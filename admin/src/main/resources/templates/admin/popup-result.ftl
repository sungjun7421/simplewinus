<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="ko">

<head>
<#include "common/head-meta.ftl">
<#include "common/css/create.ftl">
</head>
<body>
<div class="wrapper">
<#-- top navbar-->

<#-- sidebar-->

<#-- Main section -->
  <section style="margin-left: 0;">
  <#-- Page content-->
    <div class="content-wrapper text-center" style="padding: 120px 0">
        <h2 class="mb-lg">${message!}</h2>
      <button class="btn btn-primary" onclick="window.close();">Close</button>
    </div>
  </section>

</div>

<#include "common/vendor.ftl">
<#include "common/script/create.ftl">
<script>
  (function () {
    setTimeout(function () {
      closePopup();
    }, 3000);

    function closePopup() {
      window.close();
      self.close();
      window.opener = window.location.href;
      self.close();
      window.open('about:blank', '_self').close();
    }
  }());
</script>

</body>
</html>