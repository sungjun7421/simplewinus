<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div id="update-comment" class="container-fluid comment-create-update">
  <#-- FORM -->
  <form id="form-update-comment" action="<@spring.url header.url + "/update"/>" method="post" data-parsley-validate="" novalidate="" data-parsley-international="true">
    <#-- Modify Complete -->
    <#include "../../common/modify-success.ftl"/>
    <#-- END : Modify Complete -->
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            Modify
          </button>
          <@spring.formHiddenInput "comment.id"/>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top" title="List">
            <span class="icon-list"></span>
          </a>
          <button data-type="btn-delete" type="button" class="btn btn-danger btn-lg" data-toggle="tooltip" data-placement="top" title="Delete"
                  data-action="<@spring.url header.url + "/delete"/>"
                  data-id="${comment.id?c}">
            <span class="icon-trash"></span>
          </button>
        </div>

      </div>
    </div>
    <div class="row">
      <#-- CONTENT-->
      <div class="col-lg-7">
        <div class="panel panel-default">
          <div class="panel-body">
            <@spring.bind "comment.content"/>
            <#-- WYSISWYG EDITOR -->
            <@ui.wysiswygEdior "comment.content" "Content" true/>
          </div>
        </div>
      </div>

      <div class="col-lg-5 pl0-lg">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="control-label">Comment Type</label>
                  <p class="form-control-static">${comment.type}</p>
                </div>
              </div>
            </div>
            <hr/>
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="control-label">Event ID</label>
                  <p class="form-control-static">${comment.relativeEvent.id}</p>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="control-label">Post ID</label>
                  <p class="form-control-static">${comment.relativePost.id}</p>
                </div>
              </div>
            </div>
            <hr/>
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="control-label">Parent ID</label>
                  <p class="form-control-static">${comment.relativeParent.id}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <@ui.panelMetaInfo comment.updatedDate comment.createdDate/>
      </div>
      <#-- END : CONTENT-->
    </div>
  </form>
</div>