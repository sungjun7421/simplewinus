<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div class="container-fluid comment-create-update">
  <#-- FORM -->
  <form id="form-create-comment" action="<@spring.url header.url + "/create"/>" method="post" data-parsley-validate="" novalidate="" data-parsley-international="true">
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            Save
          </button>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top" title="List">
            <span class="icon-list"></span>
          </a>
        </div>

      </div>
    </div>
    <div class="row">
      <#-- CONTENT-->
      <div class="col-lg-7">
        <div class="panel panel-default">
          <div class="panel-body">
            <@spring.bind "comment.content"/>
            <#-- WYSISWYG EDITOR -->
            <@ui.wysiswygEdior "comment.content" "Content" true/>
          </div>
        </div>
      </div>

      <div class="col-lg-5 pl0-lg">
        <div class="panel panel-default">
          <div class="panel-body">
            <@ui.formRadioboxEnum "comment.type" "Comment Type" true types/>
            <hr/>
            <@ui.formInputText "comment.relativeParent" "Parent ID" false 255 "Enter Parent id" "number" "data-parsley-type=\"integer\""/>
          </div>
        </div>

        <div data-role="comment-type-post" style="display: none;">
          <div class="panel panel-default" >
            <div class="panel-heading">Post</div>
            <div class="panel-body">
              <#if comment.relativePost?has_content>
                <div class="row">
                  <@spring.formHiddenInput "comment.relativePost"/>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="control-label">Post Content</label>
                      <p class="form-control-static">${comment.relativePost.content}</p>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="control-label">Post Title</label>
                      <p class="form-control-static">${comment.relativePost.title}</p>
                    </div>
                  </div>
                </div>
              <#else>
                <div class="form-group">
                  <label class="control-label">Post Search (Automatically search when entered)</label>

                  <input type="text" placeholder="Post Search (Content, Title)"
                         class="form-control"
                         data-type="autocomplete-ajax"
                         data-url="/admin/api/post"
                         data-param-name="query"
                         data-fn-name="callbackByPost">

                </div>

                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="control-label">Post Title</label>

                      <input type="text" placeholder="Title"
                             class="form-control"
                             id="title-post"
                             readonly
                             value="">

                    </div>

                  </div>
                  <div class="col-sm-6">
                    <@ui.formInputText "comment.relativePost" "Post ID" false 255 "Enter Post id" "number" "data-parsley-type=\"integer\""/>
                  </div>
                </div>
              </#if>
            </div>
          </div>
        </div>

        <div data-role="comment-type-event" style="display: none;">
          <div class="panel panel-default">
            <div class="panel-heading">Event</div>
            <div class="panel-body">
              <#if comment.relativeEvent?has_content>
                <div class="row">
                  <@spring.formHiddenInput "comment.relativeEvent"/>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="control-label">Event Content</label>
                      <p class="form-control-static">${comment.relativeEvent.content}</p>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="control-label">Event Title</label>
                      <p class="form-control-static">${comment.relativeEvent.title}</p>
                    </div>
                  </div>
                </div>
              <#else>
                <div class="form-group">
                  <label class="control-label">Event Search (Automatically search when entered)</label>
                  <input type="text" placeholder="Event Search (Content, Title)"
                         class="form-control"
                         data-type="autocomplete-ajax"
                         data-url="/admin/api/event"
                         data-param-name="query"
                         data-fn-name="callbackByEvent">
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="control-label">Event Title</label>
                      <input type="text" placeholder="Title"
                             class="form-control"
                             id="title-event"
                             readonly
                             value="">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <@ui.formInputText "comment.relativeEvent" "Event ID" false 255 "Enter Event id" "number" "data-parsley-type=\"integer\""/>
                  </div>
                </div>
              </#if>
            </div>
          </div>
        </div>

      </div>
      <#-- END : CONTENT-->
    </div>
  </form>
</div>