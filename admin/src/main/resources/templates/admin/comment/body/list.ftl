<#--<#assign pageparam = "&key=value"/>-->
<#assign pageparam = "&type=${type!}"/>
<#if idUser?has_content>
  <#assign pageparam = pageparam + "&idUser=${idUser?c}"/>
</#if>
<#if idType?has_content>
  <#assign pageparam = pageparam + "&idType=${idType?c}"/>
</#if>
<#if idParent?has_content>
  <#assign pageparam = pageparam + "&idParent=${idParent?c}"/>
</#if>
<div class="container-fluid">
  <#-- 필터 -->
  <div class="row">
    <div class="col-md-12">
      <div id="" class="panel panel-default panel-list-filter">
        <div class="panel-heading">검색 필터

          <a href="#" data-tool="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
            <em class="fa fa-minus"></em>
          </a>

          <a id="initial-list-filter" href="javascript:void(0);" data-tool="panel-refresh" data-toggle="tooltip" title="" class="pull-right">
            <em class="fa fa-refresh"></em>
          </a>

        </div>
        <div class="panel-wrapper collapse in">
          <div class="panel-body">
            <form id="form-list-filter" action="" method="get">
              <#include "/admin/common/list/fieldset-common.ftl"/>
              <#include "/admin/common/list/fieldset-date.ftl"/>
              <#include "/admin/common/list/fieldset-search.ftl"/>

              <fieldset>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">User ID</label>

                      <div class="col-xs-10">
                        <input name="idUser" type="text" class="form-control" placeholder="Please enter User ID" value="<#if idUser?has_content>${idUser?c}</#if>">
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>

              <fieldset>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">Type</label>
                      <div class="col-xs-10">
                        <div class="form-group">
                          <div >
                            <label class="radio-inline c-radio">
                              <input type="radio" name="type" value="" <#if !type?has_content>checked</#if>>
                              <span class="fa fa-circle"></span>All
                            </label>
                            <#list types as item>
                              <label class="radio-inline c-radio">
                                <input type="radio" name="type" value="${item}" <#if type?has_content && type == item>checked</#if>>
                                <span class="fa fa-circle"></span>${item.value}
                              </label>
                            </#list>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>

              <fieldset>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">Type ID</label>

                      <div class="col-xs-10">
                        <input name="idType" type="text" class="form-control" placeholder="Please enter Type ID" value="<#if idType?has_content>${idType?c}</#if>">
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>

              <fieldset>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">Parent ID</label>

                      <div class="col-xs-10">
                        <input name="idParent" type="text" class="form-control" placeholder="Please enter Parent ID" value="<#if idParent?has_content>${idParent?c}</#if>">
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>

            </form>
          </div>
          <#-- 필터 실행 -->
          <div class="panel-footer">

            <button id="submit-list-filter" type="button" class="btn btn-primary">검색</button>

            <div class="btn-group">
              <button type="button" data-toggle="dropdown" class="btn dropdown-toggle btn-default">${data.pageSize}개씩 보기 <span class="caret"></span></button>
              <ul role="menu" class="dropdown-menu">
                <li><a href="<@spring.url "?query=${data.query!}&size=10&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 10</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=20&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 20</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=50&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 50</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=100&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 100</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=500&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 500</a></li>
              </ul>
            </div>


            <#--<button id="excel-list-filter" type="button" data-action="<@spring.url "/admin/board/blogCategory/excel"/>" class="btn btn-labeled btn-default">-->
            <#--<span class="text">엑셀 Download</span>-->
            <#--<span class="btn-label btn-label-right"><i class="fa fa-file-excel-o"></i></span>-->
            <#--</button>-->
          </div>
          <#-- END : 필터 실행 -->
        </div>
      </div>
    </div>
  </div>
  <#-- END : 필터 -->
  <#-- HEADER -->
  <#assign createparam = header.url +"/create"/>
  <#include "/admin/common/list/header.ftl"/>
  <#-- END : HEADER -->
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

        <#-- START table-responsive-->
        <div class="table-responsive">
          <table id="table-ext-1" class="table table-bordered table-hover">
            <colgroup>
              <col width="10%">
              <col width="30%">
              <col width="10%">
              <col width="10%">
              <col width="10%">
              <col width="10%">
              <col width="10%">
              <col width="10%">
            </colgroup>
            <thead>
            <tr>
              <th class="text-center">#</th>

              <th class="text-center">Content</th>
              <th class="text-center">User</th>

              <th class="text-center">Post</th>
              <th class="text-center">Event</th>

              <th class="text-center">Parent</th>
              <th class="text-center">Cre. Date</th>
              <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>

            <#if data?has_content>
              <#list data.page.content as item>
                <tr>
                  <td class="text-center">no.${data.firstNo- (item_index + 1)}<br/>id.${item.id?c}</td>
                  <td class="text-center">${item.content!}</td>

                  <td class="text-center">${item.relativeUser.id!}</td>
                  <td class="text-center">${item.relativePost.id!}</td>
                  <td class="text-center">${item.relativeEvent.id!}</td>
                  <td class="text-center">${item.relativeParent.id!}</td>

                  <td class="text-center">${item.createdDate.format('yyyy.MM.dd HH:mm:ss')}</td>
                  <td class="text-center">
                    <a href="<@spring.url header.url + "/update/${item.id?c}"/>" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="View and Edit">
                      <em class="fa fa-pencil"></em>
                    </a>
                  </td>
                </tr>
              </#list>
            </#if>
            </tbody>
          </table>
        </div>
        <#-- END table-responsive-->

        <#-- TABLE FOOTER -->
        <div class="panel-footer">
          <div class="row">
            <div class="col-lg-12 text-center">
              <#include "/admin/common/list/pagination.ftl"/>
            </div>
          </div>
        </div>
        <#-- END : TABLE FOOTER -->
      </div>
    </div>
  </div>
</div>
