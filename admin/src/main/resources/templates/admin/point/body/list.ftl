<#--<#assign pageparam = "&key=value"/>-->
<#assign pageparam = "&status=${status!}"/>
<#if idUser?has_content>
<#assign pageparam = pageparam + "&idUser=${idUser?c}"/>
</#if>
<div class="container-fluid">

<#if !pointSetting.enabled>
  <div class="row">
    <div class="col-md-6">

      <div id="panelDemo11" class="panel panel-warning">
        <div class="panel-heading">Warning</div>
        <div class="panel-body">
          <p>Point Active Status <b>Inactive</b> Status.</p>
          <p>To use point management, please click the button below to activate it.</p>
          <a href="<@spring.url "/admin/set-point"/>" class="btn btn-default btn-lg">Go to Settings</a>
        </div>
        <#--<div class="panel-footer">Panel Footer</div>-->
      </div>

    </div>
  </div>
<#else>
  <#-- 필터 -->
  <div class="row">
    <div class="col-md-12">
      <div id="" class="panel panel-default panel-list-filter">
        <div class="panel-heading">Search filter

          <a href="#" data-tool="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
            <em class="fa fa-minus"></em>
          </a>

          <a id="initial-list-filter" href="javascript:void(0);" data-tool="panel-refresh" data-toggle="tooltip" title="" class="pull-right">
            <em class="fa fa-refresh"></em>
          </a>

        </div>
        <div class="panel-wrapper collapse in">
          <div class="panel-body">
            <form id="form-list-filter" action="" method="get">
              <#include "/admin/common/list/fieldset-common.ftl"/>
              <#include "/admin/common/list/fieldset-date.ftl"/>
  <fieldset>
  <div class="row">
  <div class="col-lg-6">
  <div class="form-group">
<label class="col-xs-2 control-label">Status</label>

  <div class="col-xs-10">

  <div class="form-group">
  <div >
  <label class="radio-inline c-radio">
<input type="radio" name="status" value="" <#if !status?has_content>checked</#if>>
<span class="fa fa-circle"></span>All
  </label>

  <#list statuses as item>
  <label class="radio-inline c-radio">
<input type="radio" name="status" value="${item}" <#if status?has_content && status == item>checked</#if>>
<span class="fa fa-circle"></span>${item.value}
  </label>
  </#list>

  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </fieldset>
              <#include "/admin/common/list/fieldset-search.ftl"/>
              <fieldset>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">Member ID</label>
                      <div class="col-xs-10">
                        <input name="idUser" type="text" class="form-control" placeholder="Enter Member id" value="<#if idUser?has_content>${idUser?c}</#if>">
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
          <#-- 필터 실행 -->
          <div class="panel-footer">

            <button id="submit-list-filter" type="button" class="btn btn-primary">검색</button>

            <div class="btn-group">
              <button type="button" data-toggle="dropdown" class="btn dropdown-toggle btn-default">${data.pageSize}개씩 보기 <span class="caret"></span></button>
              <ul role="menu" class="dropdown-menu">
                <li><a href="<@spring.url "?query=${data.query!}&size=10&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 10</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=20&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 20</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=50&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 50</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=100&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 100</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=500&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 500</a></li>
              </ul>
            </div>


            <#--<button id="excel-list-filter" type="button" data-action="<@spring.url "/admin/board/blogCategory/excel"/>" class="btn btn-labeled btn-default">-->
            <#--<span class="text">엑셀 Download</span>-->
            <#--<span class="btn-label btn-label-right"><i class="fa fa-file-excel-o"></i></span>-->
            <#--</button>-->
          </div>
          <#-- END : 필터 실행 -->
        </div>
      </div>
    </div>
  </div>
  <#-- END : 필터 -->
<#-- META INFORMATION-->
  <#if buyer?has_content>
  <div class="row">
    <div class="col-md-12">

      <div class="panel panel-default">
      <div class="panel-heading"><b style="font-size:18px;">${buyer.relativeUser.fullName}</b> Point Information</div>

        <div class="table-responsive table-responsive-full">
          <table id="table-ext-1" class="table table-bordered table-hover">
            <colgroup>
              <col width="50%">
              <col width="50%">
            </colgroup>
            <thead>
            <tr>
              <th class="text-center">Holding Point</th>
              <th class="text-center">Points to expire this month</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td class="text-center">${total}</td>
              <td class="text-center">${expireScheduledTotal}</td>
            </tr>
            </tbody>
          </table>
        </div>

      </div>

    </div>
  </div>
  </#if>
<#-- END : META INFORMATION-->
  <#-- HEADER -->
  <#assign createparam = header.url +"/create"/>
  <#include "/admin/common/list/header.ftl"/>
  <#-- END : HEADER -->
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

        <#-- START table-responsive-->
        <div class="table-responsive">
          <table id="table-ext-1" class="table table-bordered table-hover">

            <colgroup>
              <col width="5%">

              <col width="10%">
              <col width="35%">
              <col width="10%">
<#--              <col width="10%">-->
              <col width="5%">
              <col width="20%">

              <col width="10%">
              <col width="5%">
            </colgroup>

            <thead>
            <tr>

              <th class="text-center">#</th>

              <th class="text-center">Status</th>
              <th class="text-center">Message</th>
              <th class="text-center">Point</th>
<#--              <th class="text-center">Expiration date</th>-->
              <th class="text-center">Member ID</th>
              <th class="text-center">Member</th>

              <th class="text-center">Create Date</th>
              <th class="text-center">Action</th>

            </tr>
            </thead>
            <tbody>
            <#if data?has_content>
              <#list data.page.content as item>

                <tr>
                <td class="text-center">${data.firstNo - (item_index + 1)}</td>

                <td class="text-center">${item.status.value}</td>
                <td class="text-left">
                <#if international>
                  <ul style="padding-left: 13px; margin-bottom: 0px">
                  <#if im.koKr>
                    <li <#if !item.internationalMode.koKr>class="inactive"</#if>>Ko : <#if item.internationalMode.koKr>${item.message.textKoKr!}<#else>Inactive</#if></li></#if>
                  <#if im.enUs>
                    <li <#if !item.internationalMode.enUs>class="inactive"</#if>>En : <#if item.internationalMode.enUs>${item.message.textEnUs!}<#else>Inactive</#if></li></#if>
                  <#if im.zhCn>
                    <li <#if !item.internationalMode.zhCn>class="inactive"</#if>>Cn S. : <#if item.internationalMode.zhCn>${item.message.textZhCn!}<#else>Inactive</#if></li></#if>
                  <#if im.zhTw>
                    <li <#if !item.internationalMode.zhTw>class="inactive"</#if>>Cn T. : <#if item.internationalMode.zhTw>${item.message.textZhTw!}<#else>Inactive</#if></li></#if>
                  <#if im.jaJp>
                    <li <#if !item.internationalMode.jaJp>class="inactive"</#if>>Ja : <#if item.internationalMode.jaJp>${item.message.textJaJp!}<#else>Inactive</#if></li></#if>
                    <#if im.viVn>
                    <li <#if !item.internationalMode.viVn>class="inactive"</#if>>Vi : <#if item.internationalMode.viVn>${item.message.textViVn!}<#else>Inactive</#if></li></#if>
                  </ul>
                <#else>
                  ${item.message.value!}
                </#if>
                </td>
                <td class="text-center">${item.point}p</td>
<#--                <td class="text-center"><#if item.expirationDate?has_content>${item.expirationDate.format('yyyy.MM.dd')}<#else>-</#if></td>-->
                <td class="text-center">
                  <#if item.relativeBuyer?has_content>
                    ${item.relativeBuyer.relativeUser.id}
                  <#else>-</#if>
                </td>
                <td class="text-center">
                  <#if item.relativeBuyer?has_content>
                    ${item.relativeBuyer.relativeUser.fullName}<br/>
                    (${item.relativeBuyer.relativeUser.email})
                  <#else>-</#if>
                </td>

                <td class="text-center">${item.createdDate.format('yyyy.MM.dd HH:mm:ss')}</td>
                <td class="text-center">
              <a href="<@spring.url header.url + "/update/${item.id?c}"/>" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="상세보기">
                <em class="fa fa-search"></em>
                </a>
                </td>
                </tr>
              </#list>
            </#if>
            </tbody>
          </table>
        </div>
        <#-- END table-responsive-->

        <#-- TABLE FOOTER -->
        <div class="panel-footer">
          <div class="row">
            <div class="col-lg-12 text-center">
              <#include "/admin/common/list/pagination.ftl"/>
            </div>
          </div>
        </div>
        <#-- END : TABLE FOOTER -->
      </div>
    </div>
  </div>
</#if>
</div>
