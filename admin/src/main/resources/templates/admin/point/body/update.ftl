<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div id="update-" class="container-fluid -create-update">
  <#-- FORM -->
  <form id="form-update-" action="<@spring.url header.url + "/update"/>" method="post" data-parsley-validate="" novalidate="" data-parsley-international="true">
    <#-- Modify Complete -->
    <#include "../../common/modify-success.ftl"/>
    <#-- END : Modify Complete -->
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <#--<button type="submit" class="btn btn-primary btn-lg">-->
            <#--Modify-->
          <#--</button>-->
          <@spring.formHiddenInput "point.id"/>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top" title="List">
            <span class="icon-list"></span>
          </a>
          <#if currentRole == 'ROLE_SUPER'>
          <button data-type="btn-delete" type="button" class="btn btn-danger btn-lg" data-toggle="tooltip" data-placement="top" title="Delete"
                  data-action="<@spring.url header.url + "/delete"/>"
                  data-id="${point.id?c}">
            <span class="icon-trash"></span>
          </button>
          </#if>
        </div>

      </div>
    </div>
    <div class="row">
      <#-- CONTENT-->
      <div class="col-lg-7">

        <div class="panel panel-default">
          <div class="panel-body">

            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="control-label">Member email</label>
                  <p class="form-control-static">${point.relativeBuyer.relativeUser.email}</p>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="control-label">Member fullname</label>
                  <p class="form-control-static">${point.relativeBuyer.relativeUser.fullName}</p>
                </div>
              </div>
            </div>

          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-body">

            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="control-label">Point Status</label>
                  <p class="form-control-static">${point.status.value}</p>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="control-label">Value</label>
                  <p class="form-control-static">${point.point} Point</p>
                </div>
              </div>
            </div>

            <hr/>
            <div class="form-group">
              <label class="control-label">Message</label>
              <#if international>
                <ul>
                  <#if im.koKr>
                  <li <#if !point.internationalMode.koKr>class="inactive"</#if>>Ko : <#if point.internationalMode.koKr>${point.message.textKoKr!}<#else>Inactive</#if></li></#if>
                  <#if im.enUs>
                  <li <#if !point.internationalMode.enUs>class="inactive"</#if>>En : <#if point.internationalMode.enUs>${point.message.textEnUs!}<#else>Inactive</#if></li></#if>
                  <#if im.zhCn>
                  <li <#if !point.internationalMode.zhCn>class="inactive"</#if>>Cn S. : <#if point.internationalMode.zhCn>${point.message.textZhCn!}<#else>Inactive</#if></li></#if>
                  <#if im.zhTw>
                  <li <#if !point.internationalMode.zhTw>class="inactive"</#if>>Cn T. : <#if point.internationalMode.zhTw>${point.message.textZhTw!}<#else>Inactive</#if></li></#if>
                  <#if im.jaJp>
                  <li <#if !point.internationalMode.jaJp>class="inactive"</#if>>Ja : <#if point.internationalMode.jaJp>${point.message.textJaJp!}<#else>Inactive</#if></li></#if>
                  <#if im.viVn>
                  <li <#if !point.internationalMode.viVn>class="inactive"</#if>>Vi : <#if point.internationalMode.viVn>${point.message.textViVn!}<#else>Inactive</#if></li></#if>
                </ul>
              <#else>
                <p class="form-control-static">${point.message.value!}</p>
              </#if>
            </div>

          </div>
        </div>

      </div>
      <#-- END : CONTENT-->

      <#-- SIDBAR -->
      <div class="col-lg-5 pl0-lg">

        <div class="panel panel-default">
          <div class="panel-heading">Point</div>
          <div class="panel-body">

            <a href="<@spring.url "/admin/point/create?idUser=${point.relativeBuyer.id?c}"/>" class="btn btn-default btn-md">Create Point with User</a>

          </div>
        </div>

        <#-- Meta Information (Modify 페이지 필수) -->
        <div class="panel panel-default">
          <div class="panel-heading">Meta Information</div>
          <div class="panel-body">

            <label class="control-label">Expiration Date </label>
            <p class="form-control-static"><#if point.expirationDate?has_content>${point.expirationDate.format('yyyy/MM/dd')}<#else>-</#if></p>

            <hr/>

            <label class="control-label">Create Date </label>
            <p class="form-control-static">${point.createdDate.format('yyyy/MM/dd HH:mm:ss')}</p>

          </div>
        </div>
        <#-- END : Meta Information (Modify 페이지 필수) -->

      </div>
      <#-- END : SIDBAR -->
    </div>
  </form>
</div>
