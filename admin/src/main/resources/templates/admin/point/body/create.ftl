<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div class="container-fluid -create-update">
  <#-- FORM -->
  <form id="form-create-" action="<@spring.url header.url + "/create"/>" method="post" data-parsley-validate="" novalidate="" data-parsley-international="true">
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            Save
          </button>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top" title="List">
            <span class="icon-list"></span>
          </a>
        </div>

      </div>
    </div>
    <div class="row">
      <#-- CONTENT-->
      <div class="col-lg-7">

        <div class="panel panel-default">
          <div class="panel-heading">User</div>
          <div class="panel-body">

            <#if point.relativeBuyer?has_content>
              <div class="row">
                <@spring.formHiddenInput "point.relativeBuyer"/>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="control-label">Member email</label>
                    <p class="form-control-static">${point.relativeBuyer.relativeUser.email}</p>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="control-label">Member fullname</label>
                    <p class="form-control-static">${point.relativeBuyer.relativeUser.fullName}</p>
                  </div>
                </div>
              </div>
            <#else>
              <div class="form-group">
                <label class="control-label">Member fullname Search (Automatically search when entered)</label>

                <input type="text" placeholder="Member fullname, email, Search using cell phone number"
                       class="form-control"
                       data-type="autocomplete-ajax"
                       data-url="/admin/api/user"
                       data-param-name="query"
                       data-fn-name="callbackByUser">

              </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Member fullname</label>

                    <input type="text" placeholder="Member fullname"
                           class="form-control"
                           id="user-name"
                           readonly
                           value="">

                  </div>

                </div>
                <div class="col-sm-6">
                  <@ui.formInputText "point.relativeBuyer" "Member ID" false 255 "Enter Member id" "number" "data-parsley-type=\"integer\""/>
                </div>
              </div>
            </#if>


          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-body">

            <@ui.formRadioboxEnum "point.status" "Point Status" true statuses/>

            <hr/>
            <div class="form-group">
              <label class="control-label">Value <span class="text-require">*</span></label>
              <@spring.formInput "point.point",
              "placeholder=\"Please enter a point.\"
                            class=\"form-control\"
                            data-parsley-required=\"true\"
                            maxlength=\"10\"
                            data-parsley-remote=\"/admin/api/point/possible/point\"
                            data-parsley-remote-options='{\"type\": \"POST\"}'
                            data-parsley-remote-validator=\"possible\"
                            data-parsley-remote-message=\"최대 Saving 가능 포인트는 '${pointSetting.maxSavingPoint}p' 입니다.\"
                            data-parsley-type=\"integer\"" "text"/>
              <span class="help-block m-b-none">If the status is decreasing, enter a negative number with '-' sign.</span>
            </div>

          </div>
        </div>

        <#-- TAB Panel or Panel Wrapper -->
        <div <#if international>role="tabpanel" class="panel panel-transparent" data-type="tabpanel-language" <#else>class="panel panel-default"</#if>>

          <@spring.bind "point.message"/>
          <#assign tabPath = spring.status.expression/>
          <#-- TAB LANGUAGE HEADER -->
          <@ui.tabListByLanguage tabPath/>
          <#-- END : TAB LANGUAGE HEADER -->

          <#-- TAB LANGUAGE BODY -->
          <div <#if international>class="tab-content bg-white" <#else>class="panel-body"</#if>>

            <#-- 국문 -->
            <#if im.koKr>
              <div id="${tabPath}-ko" <#if international>role="tabpanel" class="tab-pane"</#if>>

            <#-- 메세지 -->
              <@ui.formInputTextByLanguage "point.message.textKoKr" "Message" true 255 "Please enter a message."/>

              <@ui.formActiveByLanguage "point.internationalMode.koKr" />
              </div>
            <#else>
              <@spring.formHiddenInput "point.message.textKoKr"/>
            </#if>
            <#-- END : 국문 -->
            <#-- 영문 -->
            <#if im.enUs>
              <div id="${tabPath}-en" <#if international>role="tabpanel" class="tab-pane"</#if>>

            <#-- 메세지 -->
              <@ui.formInputTextByLanguage "point.message.textEnUs" "Message" true 255 "Please enter a message."/>

              <@ui.formActiveByLanguage "point.internationalMode.enUs"/>
              </div>
            <#else>
              <@spring.formHiddenInput "point.message.textEnUs"/>
            </#if>
            <#-- END : 영문 -->
            <#-- 중문 (간체) -->
            <#if im.zhCn>
              <div id="${tabPath}-zh-cn" <#if international>role="tabpanel" class="tab-pane"</#if>>

            <#-- 메세지 -->
              <@ui.formInputTextByLanguage "point.message.textZhCn" "Message" true 255 "Please enter a message."/>

              <@ui.formActiveByLanguage "point.internationalMode.zhCn"/>
              </div>
            <#else>
              <@spring.formHiddenInput "point.message.textZhCn"/>
            </#if>
            <#-- END : 중문 (간체) -->
            <#-- 중문 (번체) -->
            <#if im.zhTw>
              <div id="${tabPath}-zh-tw" <#if international>role="tabpanel" class="tab-pane"</#if>>

            <#-- 메세지 -->
              <@ui.formInputTextByLanguage "point.message.textZhTw" "Message" true 255 "Please enter a message."/>

              <@ui.formActiveByLanguage "point.internationalMode.zhTw"/>
              </div>
            <#else>
              <@spring.formHiddenInput "point.message.textZhTw"/>
            </#if>
            <#-- END : 중문 (번체) -->
            <#-- 일문 -->
            <#if im.jaJp>
              <div id="${tabPath}-ja" <#if international>role="tabpanel" class="tab-pane"</#if>>

            <#-- 메세지 -->
              <@ui.formInputTextByLanguage "point.message.textJaJp" "Message" true 255 "Please enter a message."/>

              <@ui.formActiveByLanguage "point.internationalMode.jaJp"/>
              </div>
            <#else>
              <@spring.formHiddenInput "point.message.textJaJp"/>
            </#if>
            <#-- END : 일문 -->
              <#-- 일문 -->
<#--              <#if im.viVn?has_content && im.viVn>-->
<#--                  <div id="${tabPath}-vi" <#if international>role="tabpanel" class="tab-pane"</#if>>-->

<#--                      &lt;#&ndash; 메세지 &ndash;&gt;-->
<#--                      <@ui.formInputTextByLanguage "point.message.textViVn" "Message" true 255 "Please enter a message."/>-->

<#--                      <@ui.formActiveByLanguage "point.internationalMode.viVn"/>-->
<#--                  </div>-->
<#--              <#else>-->
<#--                  <@spring.formHiddenInput "point.message.textViVn"/>-->
<#--              </#if>-->
              <#-- END : 일문 -->
          </div>
        </div>
        <#-- END : TAB LANGUAGE BODY -->

      </div>
      <#-- END : CONTENT-->
      <#-- SIDBAR -->
      <div class="col-lg-5 pl0-lg">

        <#--<div class="panel panel-default">-->
          <#--<div class="panel-body">-->

          <#--</div>-->
        <#--</div>-->

      </div>
      <#-- END : SIDBAR -->
    </div>
  </form>
</div>
