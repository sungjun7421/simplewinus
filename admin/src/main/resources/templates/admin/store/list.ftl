<#import "/spring.ftl" as spring/>
<#import "../common/ui.ftl" as ui/>
<!DOCTYPE html>
<html lang="ko">

<head>
  <#include "../common/head-meta.ftl">
  <#include "../common/css/list.ftl">
  <#-- CHOSEN -->
  <link rel="stylesheet" href="<@spring.url "/ad/vendor/chosen_v1.2.0/chosen.min.css"/>">
</head>
<body>
<div class="wrapper">
  <#-- top navbar-->
  <#include "../common/top-navbar.ftl">
  <#-- sidebar-->
  <#include "../common/sidebar.ftl">

  <#-- Main section -->
  <section>
    <#-- Page content-->
    <div class="content-wrapper">
      <#include "../common/header.ftl" />
      <#include "body/list.ftl">
    </div>
  </section>

  <#-- Page footer-->
  <#include "../common/footer.ftl">
</div>

<#include "../common/vendor.ftl">
<#include "../common/script/list.ftl">
<#-- CHOSEN -->
<script src="<@spring.url "/ad/vendor/chosen_v1.2.0/chosen.jquery.min.js"/>"></script>

</body>

</html>