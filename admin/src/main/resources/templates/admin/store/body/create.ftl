<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div class="container-fluid es-product-create-update" xmlns="http://www.w3.org/1999/html">
    <#-- FORM -->
    <form id="form-create-es-product" action="<@spring.url header.url + "/create"/>" method="post"
          data-parsley-validate=""
          novalidate="" data-parsley-international="true">
        <div class="row">
            <div class="col-md-12">

                <div class="pull-left mb-lg">
                    <button type="submit" class="btn btn-primary btn-lg">
                        Save
                    </button>

                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </div>

                <div class="pull-right mb-lg">
                    <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip"
                       data-placement="top"
                       title="List">
                        <span class="icon-list"></span>
                    </a>
                </div>

            </div>
        </div>
        <div class="row">
            <#-- CONTENT-->
            <div class="col-lg-7">


                <#-- TAB Panel or Panel Wrapper -->
                <div <#if international>role="tabpanel" class="panel panel-transparent" data-type="tabpanel-language"
                     <#else>class="panel panel-default"</#if>>

                    <@spring.bind "store.name"/>
                    <#assign tabPath = spring.status.expression/>
                    <#-- TAB LANGUAGE HEADER -->
                    <@ui.tabListByLanguage tabPath/>
                    <#-- END : TAB LANGUAGE HEADER -->

                    <#-- TAB LANGUAGE BODY -->
                    <div <#if international>class="tab-content bg-white" <#else>class="panel-body"</#if>>

                        <#-- 국문 -->
                        <#if im.koKr>
                            <div id="${tabPath}-ko" <#if international>role="tabpanel"
                                 class="tab-pane ${im.koKr}"</#if>>

                                <#-- Title -->
                                <@ui.formInputTextByLanguage "store.name.textKoKr" "Name" true 255 "Enter the store name."/>
                                <hr/>

                                <#-- WYSISWYG EDITOR -->
                                <@ui.wysiswygEdior "store.introduce.textKoKr" "Content" false/>

                                <@ui.formActiveByLanguage "store.internationalMode.koKr" />
                            </div>
                        <#else>
                            <@spring.formHiddenInput "store.name.textKoKr"/>
                            <@spring.formHiddenInput "store.introduce.textKoKr"/>
                        </#if>
                        <#-- END : 국문 -->
                        <#-- 영문 -->
                        <#if im.enUs>
                            <div id="${tabPath}-en" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- Title -->
                                <@ui.formInputTextByLanguage "store.name.textEnUs" "Name" true 255 "Enter the store name."/>

                                <hr/>
                                <#-- WYSISWYG EDITOR -->
                                <@ui.wysiswygEdior "store.introduce.textEnUs" "Content" false/>
                                <@ui.formActiveByLanguage "store.internationalMode.enUs"/>
                            </div>
                        <#else>
                            <@spring.formHiddenInput "store.name.textEnUs"/>
                            <@spring.formHiddenInput "store.introduce.textEnUs"/>
                        </#if>
                        <#-- END : 영문 -->
                        <#-- 중문 (간체) -->
                        <#if im.zhCn>
                            <div id="${tabPath}-zh-cn" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- Title -->
                                <@ui.formInputTextByLanguage "store.name.textZhCn" "Name" true 255 "Enter the product name."/>
                                <hr/>

                                <#-- WYSISWYG EDITOR -->
                                <@ui.wysiswygEdior "store.introduce.textZhCn" "Content" false/>
                                <@ui.formActiveByLanguage "store.internationalMode.zhCn"/>
                            </div>
                        <#else>
                            <@spring.formHiddenInput "store.name.textZhCn"/>
                            <@spring.formHiddenInput "store.introduce.textZhCn"/>
                        </#if>
                        <#-- END : 중문 (간체) -->
                        <#-- 중문 (번체) -->
                        <#if im.zhTw>
                            <div id="${tabPath}-zh-tw" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- Title -->
                                <@ui.formInputTextByLanguage "store.name.textZhTw" "Name" true 255 "Enter the product name."/>

                                <hr/>

                                <#-- WYSISWYG EDITOR -->
                                <@ui.wysiswygEdior "store.introduce.textZhTw" "Content" false/>
                                <@ui.formActiveByLanguage "store.internationalMode.zhTw"/>
                            </div>
                        <#else>
                            <@spring.formHiddenInput "store.name.textZhTw"/>
                            <@spring.formHiddenInput "store.introduce.textZhTw"/>
                        </#if>
                        <#-- END : 중문 (번체) -->
                        <#-- 일문 -->
                        <#if im.jaJp>
                            <div id="${tabPath}-ja" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- Title -->
                                <@ui.formInputTextByLanguage "store.name.textJaJp" "Name" true 255 "Enter the product name."/>
                                <hr/>
                                <#-- WYSISWYG EDITOR -->
                                <@ui.wysiswygEdior "store.introduce.textJaJp" "Content" false/>
                                <@ui.formActiveByLanguage "store.internationalMode.jaJp"/>
                            </div>
                        <#else>
                            <@spring.formHiddenInput "store.name.textJaJp"/>
                            <@spring.formHiddenInput "store.introduce.textJaJp"/>
                        </#if>
                        <#-- END : 일문 -->
                    </div>
                </div>
                <#-- END : TAB LANGUAGE BODY -->


                <div class="panel panel-default">
                    <div class="panel-heading"><h3>위치 정보</h3></div>
                    <div class="panel-body">

                        <div class="row" data-type="search-address">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">주소검색</label>

                                    <div class="input-group">
                                        <input type="text" data-type="input-search-address" placeholder="검색어를 입력하세요." class="form-control">
                                        <span class="input-group-btn">
                                        <button type="button" data-type="btn-search-address"
                                                class="btn btn-default">검색</button>
                                     </span>
                                    </div>
                                    <span class="help-block m-b-none">예 : 독막로9길 8, 국립중앙박물관, 서교동 394-2</span>

                                    <div class="list-jusos"
                                         style="display: none;max-height: 300px;overflow: auto; border: 1px solid #dde6e9;">
                                        <ul id="list-jusos">
                                        </ul>
                                        <div class="list-next">
                                            <a class="btn btn-info btn-block" href="javascript:void(0);">더보기</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">우편 번호</label>
                                    <@spring.formInput "store.address.postalCode",
                                    "placeholder=\"우편번호를 입력하세요.\"
                                         class=\"form-control\"
                                         data-type=\"postalCode\"
                                         maxlength=\"10\"
                                         readonly"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label">기본 주소</label>
                                    <@spring.formInput "store.address.address1",
                                    "placeholder=\"기본 주소를 입력하세요.\"
                            class=\"form-control\"
                            data-type=\"address1\"
                            data-parsley-maxlength=\"255\"
                            readonly"
                                    />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label">상세 주소</label>
                                    <@spring.formInput "store.address.address2",
                                    "placeholder=\"상세 주소를 입력하세요.\"
                            class=\"form-control\"
                            data-type=\"address2\"
                            data-parsley-maxlength=\"255\""
                                    />
                                </div>
                            </div>
                        </div>
                        <hr/>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label">지번 주소</label>
                                    <@spring.formInput "store.address.jibunAddr",
                                    "placeholder=\"지번 주소를 입력하세요.\"
                            class=\"form-control\"
                            data-type=\"jibunAddr\"
                            data-parsley-maxlength=\"255\"
                            "
                                    />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label">영문 주소</label>
                                    <@spring.formInput "store.address.engAddr",
                                    "placeholder=\"지번 주소를 입력하세요.\"
                            class=\"form-control\"
                            data-type=\"engAddr\"
                            data-parsley-maxlength=\"255\"
                            "
                                    />
                                </div>
                            </div>
                        </div>

                        <hr/>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">지역1 (시도명)</label>
                                    <@spring.formInput "store.address.siNm",
                                    "placeholder=\"지역1 (시도명)를 입력하세요.\"
                            class=\"form-control\"
                            data-type=\"siNm\"
                            data-parsley-maxlength=\"255\"
                            "
                                    />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">지역2 (시군구명)</label>
                                    <@spring.formInput "store.address.sggNm",
                                    "placeholder=\"지역2 (시군구명)를 입력하세요.\"
                            class=\"form-control\"
                            data-type=\"sggNm\"
                            data-parsley-maxlength=\"255\"
                            "
                                    />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">지역3 (읍면동명)</label>
                                    <@spring.formInput "store.address.emdNm",
                                    "placeholder=\"지역3 (읍면동명)를 입력하세요.\"
                            class=\"form-control\"
                            data-type=\"emdNm\"
                            data-parsley-maxlength=\"255\"
                            "
                                    />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">지역4 (법정리명)</label>
                                    <@spring.formInput "store.address.liNm",
                                    "placeholder=\"지역4 (법정리명)를 입력하세요.\"
                            class=\"form-control\"
                            data-type=\"liNm\"
                            data-parsley-maxlength=\"255\"
                            "
                                    />
                                </div>
                            </div>
                        </div>

                        <hr/>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">위도 (가로축) </label>

                                    <@spring.formInput
                                    "store.address.gps.latitude",
                                    "placeholder=\"위도를 입력하세요.\"
                                class=\"form-control\"
                                maxlength=\"20\"
                                data-parsley-maxlength=\"20\"
                                data-parsley-type=\"number\""
                                    />
                                </div>

                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">경도 (세로축) </label>

                                    <@spring.formInput
                                    "store.address.gps.longitude",
                                    "placeholder=\"위도를 입력하세요.\"
                                class=\"form-control\"
                                maxlength=\"20\"
                                data-parsley-maxlength=\"20\"
                                data-parsley-type=\"number\""
                                    />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div id="map" style="height: 300px;"></div>
                            </div>
                        </div>

                    </div>
                </div>



            </div>
            <#-- END : CONTENT-->
            <#-- SIDBAR -->
            <div class="col-lg-5 pl0-lg">


                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Detail store</h4></div>
                    <div class="panel-body">
                        <@ui.formInputText "store.storeId" "Store Id" false 255 "" "text" ""/>
                        <@ui.formInputText "store.businessName" "Business Name" false 255 "" "text" ""/>
                        <@ui.formInputText "store.phone" "Phone" true 255 "" "text" ""/>
<#--                        <@ui.formInputText "store.address.address1" "Address" true 255 "" "text" ""/>-->
<#--                        <@ui.formInputText "store.address.gps.latitude" "Latitude" true 255 "" "text" ""/>-->
<#--                        <@ui.formInputText "store.address.gps.longitude" "Longitude" true 255 "" "text" ""/>-->
                        <div class="form-group">
                            <label class="control-label">Join Date</label>
                            <@spring.formInput "store.joinDate"
                            "data-type=\"datetimepicker\"
                                 data-format=\"YYYY-MM-DD hh:mm:ss\"
                                 type=\"text\"
                                 class=\"form-control\""/>
                            <span class="help-block m-b-none">The latest effective date will be exposed.</span>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Image</h4></div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-6">
                                <@ui.uploadImage "Thumbnail" "store.image" "" "JPEG 800 X 800"/>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Setting</h4></div>
                    <div class="panel-body">
                        <@ui.formBoolean "store.active" "Status" "Active" "Inactive" ""/>
                    </div>

                </div>
            </div>

        </div>
        <#-- END : SIDBAR -->

    </form>
</div>
