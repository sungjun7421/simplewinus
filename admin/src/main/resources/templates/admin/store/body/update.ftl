<#import "/spring.ftl" as spring/>
<#assign pageparam = ""/>
<#-- START widgets box-->
<div id="update-es-product" class="container-fluid es-product-create-update">
  <#-- FORM -->
  <form id="form-update-es-product" action="<@spring.url header.url + "/update"/><#if springMacroRequestContext.queryString?has_content>?${springMacroRequestContext.queryString?replace("success&", "")}</#if>" method="post"
        data-parsley-validate=""
        novalidate="" data-parsley-international="true">
    <#-- Modify Complete -->
    <#include "../../common/modify-success.ftl"/>
    <#-- END : Modify Complete -->
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            Modify
          </button>
          <@spring.formHiddenInput "store.id"/>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">

          <a href="<@spring.url header.url/><#if springMacroRequestContext.queryString?has_content>?${springMacroRequestContext.queryString?replace("success&", "")}</#if>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top"
             title="List">
            <span class="icon-list"></span>
          </a>
          <button data-type="btn-delete" type="button" class="btn btn-danger btn-lg" data-toggle="tooltip"
                  data-placement="top" title="Delete"
                  data-action="<@spring.url header.url + "/delete"/>"
                  data-id="${store.id?c}">
            <span class="icon-trash"></span>
          </button>
        </div>

      </div>
    </div>
    <div class="row">
      <#-- CONTENT-->
      <div class="col-lg-7">


        <#-- TAB Panel or Panel Wrapper -->
        <div <#if international>role="tabpanel" class="panel panel-transparent" data-type="tabpanel-language"
             <#else>class="panel panel-default"</#if>>

          <@spring.bind "store.name"/>
          <#assign tabPath = spring.status.expression/>
          <#-- TAB LANGUAGE HEADER -->
          <@ui.tabListByLanguage tabPath/>
          <#-- END : TAB LANGUAGE HEADER -->

          <#-- TAB LANGUAGE BODY -->
          <div <#if international>class="tab-content bg-white" <#else>class="panel-body"</#if>>

            <#-- 국문 -->
            <#if im.koKr>
              <div id="${tabPath}-ko" <#if international>role="tabpanel"
                   class="tab-pane ${im.koKr}"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "store.name.textKoKr" "Name" true 255 "Enter the store name."/>
                <hr/>

                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "store.introduce.textKoKr" "Content" false/>

                <@ui.formActiveByLanguage "store.internationalMode.koKr" />
              </div>
            <#else>
              <@spring.formHiddenInput "store.name.textKoKr"/>
              <@spring.formHiddenInput "store.introduce.textKoKr"/>
            </#if>
            <#-- END : 국문 -->
            <#-- 영문 -->
            <#if im.enUs>
              <div id="${tabPath}-en" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "store.name.textEnUs" "Name" true 255 "Enter the store name."/>

                <hr/>
                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "store.introduce.textEnUs" "Content" false/>
                <@ui.formActiveByLanguage "store.internationalMode.enUs"/>
              </div>
            <#else>
              <@spring.formHiddenInput "store.name.textEnUs"/>
              <@spring.formHiddenInput "store.introduce.textEnUs"/>
            </#if>
            <#-- END : 영문 -->
            <#-- 중문 (간체) -->
            <#if im.zhCn>
              <div id="${tabPath}-zh-cn" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "store.name.textZhCn" "Name" true 255 "Enter the product name."/>
                <hr/>

                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "store.introduce.textZhCn" "Content" false/>
                <@ui.formActiveByLanguage "store.internationalMode.zhCn"/>
              </div>
            <#else>
              <@spring.formHiddenInput "store.name.textZhCn"/>
              <@spring.formHiddenInput "store.introduce.textZhCn"/>
            </#if>
            <#-- END : 중문 (간체) -->
            <#-- 중문 (번체) -->
            <#if im.zhTw>
              <div id="${tabPath}-zh-tw" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "store.name.textZhTw" "Name" true 255 "Enter the product name."/>

                <hr/>

                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "store.introduce.textZhTw" "Content" false/>
                <@ui.formActiveByLanguage "store.internationalMode.zhTw"/>
              </div>
            <#else>
              <@spring.formHiddenInput "store.name.textZhTw"/>
              <@spring.formHiddenInput "store.introduce.textZhTw"/>
            </#if>
            <#-- END : 중문 (번체) -->
            <#-- 일문 -->
            <#if im.jaJp>
              <div id="${tabPath}-ja" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "store.name.textJaJp" "Name" true 255 "Enter the product name."/>
                <hr/>
                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "store.introduce.textJaJp" "Content" false/>
                <@ui.formActiveByLanguage "store.internationalMode.jaJp"/>
              </div>
            <#else>
              <@spring.formHiddenInput "store.name.textJaJp"/>
              <@spring.formHiddenInput "store.introduce.textJaJp"/>
            </#if>
            <#-- END : 일문 -->
          </div>

          <div class="panel panel-default">
            <div class="panel-heading"><h3>위치 정보</h3></div>
            <div class="panel-body">

              <div class="row" data-type="search-address">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="control-label">주소검색</label>

                    <div class="input-group">
                      <input type="text" data-type="input-search-address" placeholder="검색어를 입력하세요." class="form-control">
                      <span class="input-group-btn">
                                        <button type="button" data-type="btn-search-address"
                                                class="btn btn-default">검색</button>
                                     </span>
                    </div>
                    <span class="help-block m-b-none">예 : 독막로9길 8, 국립중앙박물관, 서교동 394-2</span>

                    <div class="list-jusos"
                         style="display: none;max-height: 300px;overflow: auto; border: 1px solid #dde6e9;">
                      <ul id="list-jusos">
                      </ul>
                      <div class="list-next">
                        <a class="btn btn-info btn-block" href="javascript:void(0);">더보기</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="control-label">우편 번호</label>
                      <@spring.formInput "store.address.postalCode",
                      "placeholder=\"우편번호를 입력하세요.\"
                                         class=\"form-control\"
                                         data-type=\"postalCode\"
                                         maxlength=\"10\"
                                         readonly"/>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="control-label">기본 주소</label>
                      <@spring.formInput "store.address.address1",
                      "placeholder=\"기본 주소를 입력하세요.\"
                            class=\"form-control\"
                            data-type=\"address1\"
                            data-parsley-maxlength=\"255\"
                            readonly"
                      />
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="control-label">상세 주소</label>
                      <@spring.formInput "store.address.address2",
                      "placeholder=\"상세 주소를 입력하세요.\"
                            class=\"form-control\"
                            data-type=\"address2\"
                            data-parsley-maxlength=\"255\""
                      />
                  </div>
                </div>
              </div>
              <hr/>

              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="control-label">지번 주소</label>
                      <@spring.formInput "store.address.jibunAddr",
                      "placeholder=\"지번 주소를 입력하세요.\"
                            class=\"form-control\"
                            data-type=\"jibunAddr\"
                            data-parsley-maxlength=\"255\"
                            "
                      />
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="control-label">영문 주소</label>
                      <@spring.formInput "store.address.engAddr",
                      "placeholder=\"지번 주소를 입력하세요.\"
                            class=\"form-control\"
                            data-type=\"engAddr\"
                            data-parsley-maxlength=\"255\"
                            "
                      />
                  </div>
                </div>
              </div>

              <hr/>

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="control-label">지역1 (시도명)</label>
                      <@spring.formInput "store.address.siNm",
                      "placeholder=\"지역1 (시도명)를 입력하세요.\"
                            class=\"form-control\"
                            data-type=\"siNm\"
                            data-parsley-maxlength=\"255\"
                            "
                      />
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="control-label">지역2 (시군구명)</label>
                      <@spring.formInput "store.address.sggNm",
                      "placeholder=\"지역2 (시군구명)를 입력하세요.\"
                            class=\"form-control\"
                            data-type=\"sggNm\"
                            data-parsley-maxlength=\"255\"
                            "
                      />
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="control-label">지역3 (읍면동명)</label>
                      <@spring.formInput "store.address.emdNm",
                      "placeholder=\"지역3 (읍면동명)를 입력하세요.\"
                            class=\"form-control\"
                            data-type=\"emdNm\"
                            data-parsley-maxlength=\"255\"
                            "
                      />
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="control-label">지역4 (법정리명)</label>
                      <@spring.formInput "store.address.liNm",
                      "placeholder=\"지역4 (법정리명)를 입력하세요.\"
                            class=\"form-control\"
                            data-type=\"liNm\"
                            data-parsley-maxlength=\"255\"
                            "
                      />
                  </div>
                </div>
              </div>

              <hr/>

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="control-label">위도 (가로축) </label>

                      <@spring.formInput
                      "store.address.gps.latitude",
                      "placeholder=\"위도를 입력하세요.\"
                                class=\"form-control\"
                                maxlength=\"20\"
                                data-parsley-maxlength=\"20\"
                                data-parsley-type=\"number\""
                      />
                  </div>

                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="control-label">경도 (세로축) </label>

                      <@spring.formInput
                      "store.address.gps.longitude",
                      "placeholder=\"위도를 입력하세요.\"
                                class=\"form-control\"
                                maxlength=\"20\"
                                data-parsley-maxlength=\"20\"
                                data-parsley-type=\"number\""
                      />
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div id="map" style="height: 300px;"></div>
                </div>
              </div>

            </div>
          </div>
        </div>
        <#-- END : TAB LANGUAGE BODY -->
        <div class="panel panel-default panel-list-filter">
          <div class="panel-heading">검색 필터</div>
          <div class="panel-wrapper">
            <div class="panel-body">

                <#include "../../common/list/fieldset-common.ftl"/>
                <fieldset>
                  <div class="row">
                    <div class="col-lg-8">
                      <div class="form-group">
                        <label class="col-xs-2 control-label">Reg. Date</label>

                        <div class="col-xs-5 ">
                          <div class="date list-filter-date date-wave" data-type="startDate">
                            <input name="startDate" type="text" class="form-control" placeholder="Start"
                                   value="${startDate!}" autocomplete="off">
                          </div>
                        </div>
                        <div class="col-xs-5">
                          <div class="date list-filter-date" data-type="endDate">
                            <input name="endDate" type="text" class="form-control" placeholder="End" value="${endDate!}"
                                   autocomplete="off">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </fieldset>

            </div>
            <#-- 필터 실행 -->
            <div class="panel-footer">

              <button id="submit-list-filter-update" type="button" class="btn btn-primary">검색</button>

            </div>
            <#-- END : 필터 실행 -->
          </div>
        </div>
        <div class="">
          <div class="row">
            <div class="col-md-12">
              <header class="clearfix pb-lg mb-sm">
                <#-- 목록 Status -->
                <div class="pull-left">
                  <div class="list-total">total ${data.page.totalElements!},&nbsp;&nbsp;${data.currentIndex}
                    /${data.page.totalPages} page
                    <span class="badge badge-success" style="color: #fff;background-color: #007bff;">Complete: ${count.countOrder}</span>
                    <span class="badge badge-danger" style="color: #fff;background-color: #dc3545;">Cancel: ${count.countCancelOrder}</span>
                  </div>
                </div>
                <div class="pull-right">
                  <span class="badge badge-success" style="color: #fff;background-color: #28a745;font-size: 28px;">Total Sale: ${count.sumOrder}w</span>
                </div>
                <#-- END : 목록 Status -->
              </header>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">

                <#-- START table-responsive-->
                <div class="table-responsive">
                  <table id="table-ext-1" class="table table-bordered table-hover">
                    <colgroup>
                      <col width="5%">
                      <col width="10%">
                      <col width="20%">
                      <col width="30%">
                      <col width="20%">
                      <col width="10%">
                    </colgroup>
                    <thead>
                    <tr>
                      <th style="vertical-align: middle" class="text-center">#</th>
                      <th style="vertical-align: middle" class="text-center">Order Number<br/>(Invoice Number)</th>
                      <th style="vertical-align: middle" class="text-center">Consumer</th>
                      <th style="vertical-align: middle" class="text-center">Price</th>
                      <th style="vertical-align: middle" class="text-center">Payment</th>
                      <th style="vertical-align: middle" class="text-center">Time</th>
                    </tr>
                    </thead>
                    <tbody>

                    <#if data?has_content>
                      <#list data.page.content as item>
                        <tr>
                          <td class="text-center">no.${data.firstNo- (item_index + 1)}<br/>id.${item.id?c}</td>

                          <td class="text-center">
                            ${item.oid!}<br/>
                            ( Device: ${item.orderDevice.value} )
                          </td>
                          <td class="text-left">

                            <strong>Member</strong>
                            <ul style="padding-left: 13px; margin-bottom: 0px">
                              <#if item.relativeBuyer?has_content>
                                <#assign user = item.relativeBuyer.relativeUser/>
                                <li>Fullname : ${user.fullName!}</li>
                                <li>PhoneNumber : ${user.phoneNumberCode!} ${user.mobile!}</li>
                                <li>이메일 : ${user.email!}</li>
                              </#if>
                            </ul>

                          </td>
                          <td class="text-lef">

                            Total Price Machine : ${item.amount!} KRW<br/>
                            Coupon Used : -${item.fcCoupon!} KRW<br/>
                            Point Used : -${item.fcPoint!} KRW<br/>
                            <hr style="margin: 2px 0">
                            <strong>Total : ${item.totalPrice!} KRW</strong><br/>

                          </td>
                          <td class="text-left">

                            <ul style="padding-left: 13px; margin-bottom: 0px">
                              <li>주문상태 : <div class="label ${item.orderStatus.labelClass!}">${item.orderStatus.value!}</div>
                                <#if item.orderStatus == 'FAILED'>
                                  <br/>
                                  <span>Failed Reason:&nbsp;
                              <#if item.failedReasonByPayDollar?has_content>
                                ${item.failedReasonByPayDollar}
                              <#else>Unknown</#if>
                            </span>
                                </#if>
                              </li>
                              <#--                        <li>PayMethod : ${item.payMethod.value!}</li>-->
                              <#--                        <li>Payment Status : <#if item.paymentComplete>-->
                              <#--                            <div class="label label-success">Complete</div><#else>-->
                              <#--                            <div class="label label-warning">No Payment</div></#if></li>-->
                              <#if item.paymentComplete>
                                <li>Payment Date : ${item.paymentDate.format('yyyy.MM.dd HH:mm:ss')!}</li>
                              </#if>
                            </ul>
                          </td>

                          <td class="text-center">
                            <ul class="list-unstyled">
                              <li data-toggle="tooltip" data-placement="top"
                                  title="Date">${item.relativeOrderMachine.startTime.format('yyyy.MM.dd')}</li>
                              <li data-toggle="tooltip" data-placement="top"
                                  title="Time">${item.relativeOrderMachine.startTime.format('HH:mm')} - ${item.relativeOrderMachine.endTime.format('HH:mm')}</li>
                            </ul>
                          </td>
                        </tr>
                      </#list>
                    </#if>
                    </tbody>
                  </table>
                </div>
                <#-- END table-responsive-->

                <#-- TABLE FOOTER -->
                <div class="panel-footer">
                  <div class="row">
                    <div class="col-lg-12 text-center">
                      <#include "../../common/list/pagination.ftl"/>
                    </div>
                  </div>
                </div>
                <#-- END : TABLE FOOTER -->
              </div>
            </div>
          </div>
        </div>

      </div>
      <#-- END : CONTENT-->
      <#-- SIDBAR -->
      <div class="col-lg-5 pl0-lg">

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Detail store</h4></div>
          <div class="panel-body">
            <@ui.formInputText "store.storeId" "Store Id" true 255 "" "text" ""/>
            <@ui.formInputText "store.businessName" "Business Name" false 255 "" "text" ""/>
            <@ui.formInputText "store.phone" "Phone" true 255 "" "text" ""/>
            <@ui.formInputText "store.address.address1" "Address" true 255 "" "text" ""/>
            <@ui.formInputText "store.address.gps.latitude" "Latitude" true 255 "" "text" ""/>
            <@ui.formInputText "store.address.gps.longitude" "Longitude" true 255 "" "text" ""/>
            <div class="form-group">
              <label class="control-label">Join Date</label>
              <@spring.formInput "store.joinDate"
              "data-type=\"datetimepicker\"
                                 data-format=\"YYYY-MM-DD hh:mm:ss\"
                                 type=\"text\"
                                 class=\"form-control\""/>
              <span class="help-block m-b-none">The latest effective date will be exposed.</span>
            </div>

          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Image</h4></div>
          <div class="panel-body">

            <div class="row">
              <div class="col-md-6">
                <@ui.uploadImage "Thumbnail Image" "store.image" "${store.thumbnail!}" "JPEG 800 X 800"/>
              </div>
              <div class="col-md-6"></div>
            </div>

          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Setting</h4></div>
          <div class="panel-body">
            <@ui.formBoolean "store.active" "Status" "Active" "Inactive" ""/>
          </div>

        </div>

        <@ui.panelMetaInfo store.updatedDate store.createdDate/>
      </div>
      <#-- END : SIDBAR -->
    </div>
  </form>
</div>
