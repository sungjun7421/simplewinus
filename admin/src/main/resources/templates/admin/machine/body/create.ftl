<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div class="container-fluid machine-create-update" xmlns="http://www.w3.org/1999/html">
    <#-- FORM -->
    <form id="form-create-machine" action="<@spring.url header.url + "/create"/>" method="post"
          data-parsley-validate=""
          novalidate="" data-parsley-international="true">
        <div class="row">
            <div class="col-md-12">

                <div class="pull-left mb-lg">
                    <button type="submit" class="btn btn-primary btn-lg">
                        Save
                    </button>

                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </div>

                <div class="pull-right mb-lg">
                    <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip"
                       data-placement="top"
                       title="List">
                        <span class="icon-list"></span>
                    </a>
                </div>

            </div>
        </div>
        <div class="row">
            <#-- CONTENT-->
            <div class="col-lg-7">


                <#-- TAB Panel or Panel Wrapper -->
                <div <#if international>role="tabpanel" class="panel panel-transparent" data-type="tabpanel-language"
                     <#else>class="panel panel-default"</#if>>

                    <@spring.bind "machine.name"/>
                    <#assign tabPath = spring.status.expression/>
                    <#-- TAB LANGUAGE HEADER -->
                    <@ui.tabListByLanguage tabPath/>
                    <#-- END : TAB LANGUAGE HEADER -->

                    <#-- TAB LANGUAGE BODY -->
                    <div <#if international>class="tab-content bg-white" <#else>class="panel-body"</#if>>

                        <#-- 국문 -->
                        <#if im.koKr>
                            <div id="${tabPath}-ko" <#if international>role="tabpanel"
                                 class="tab-pane ${im.koKr}"</#if>>

                                <#-- Title -->
                                <@ui.formInputTextByLanguage "machine.name.textKoKr" "Name" true 255 "Enter the machine name."/>

                                <@ui.formActiveByLanguage "machine.internationalMode.koKr" />
                            </div>
                        <#else>
                            <@spring.formHiddenInput "machine.name.textKoKr"/>
                        </#if>
                        <#-- END : 국문 -->
                        <#-- 영문 -->
                        <#if im.enUs>
                            <div id="${tabPath}-en" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- Title -->
                                <@ui.formInputTextByLanguage "machine.name.textEnUs" "Name" true 255 "Enter the machine name."/>

                                <@ui.formActiveByLanguage "machine.internationalMode.enUs"/>
                            </div>
                        <#else>
                            <@spring.formHiddenInput "machine.name.textEnUs"/>
                        </#if>
                        <#-- END : 영문 -->
                        <#-- 중문 (간체) -->
                        <#if im.zhCn>
                            <div id="${tabPath}-zh-cn" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- Title -->
                                <@ui.formInputTextByLanguage "machine.name.textZhCn" "Name" true 255 "Enter the machine name."/>

                                <#-- WYSISWYG EDITOR -->
                                <@ui.formActiveByLanguage "machine.internationalMode.zhCn"/>
                            </div>
                        <#else>
                            <@spring.formHiddenInput "machine.name.textZhCn"/>
                        </#if>
                        <#-- END : 중문 (간체) -->
                        <#-- 중문 (번체) -->
                        <#if im.zhTw>
                            <div id="${tabPath}-zh-tw" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- Title -->
                                <@ui.formInputTextByLanguage "machine.name.textZhTw" "Name" true 255 "Enter the machine name."/>

                                <#-- WYSISWYG EDITOR -->
                                <@ui.formActiveByLanguage "machine.internationalMode.zhTw"/>
                            </div>
                        <#else>
                            <@spring.formHiddenInput "machine.name.textZhTw"/>
                        </#if>
                        <#-- END : 중문 (번체) -->
                        <#-- 일문 -->
                        <#if im.jaJp>
                            <div id="${tabPath}-ja" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- Title -->
                                <@ui.formInputTextByLanguage "machine.name.textJaJp" "Name" true 255 "Enter the machine name."/>

                                <#-- WYSISWYG EDITOR -->
                                <@ui.formActiveByLanguage "machine.internationalMode.jaJp"/>
                            </div>
                        <#else>
                            <@spring.formHiddenInput "machine.name.textJaJp"/>
                        </#if>
                        <#-- END : 일문 -->
                    </div>
                </div>
                <#-- END : TAB LANGUAGE BODY -->
            </div>
            <#-- END : CONTENT-->
            <#-- SIDBAR -->
            <div class="col-lg-5 pl0-lg">

                <div class="panel panel-default">
                    <div class="panel-heading"><h4>기기 종류</h4></div>
                    <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label">Standard Machine Name Search (Automatically search when entered)</label>
                                <input type="text" placeholder="Standard Machine Name"
                                       class="form-control"
                                       data-type="autocomplete-ajax"
                                       data-url="/admin/api/standardMachine"
                                       data-param-name="query"
                                       data-fn-name="callbackByStandardMachine">
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Standard Machine Name</label>
                                        <input type="text" placeholder="Standard Machine Name"
                                               class="form-control"
                                               id="standard-machine-name"
                                               readonly
                                               value="<#if machine.relativeStandardMachine?has_content><#list machine.relativeStandardMachine as item>${machine.relativeStandardMachine.name.value!}</#list></#if>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <@ui.formInputText "machine.relativeStandardMachine" "Standard Machine ID" true 255 "Enter Standard Machine Id" "number" "data-parsley-type=\"integer\""/>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><h4>매장</h4></div>
                    <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label">Store Name Search (Automatically search when entered)</label>
                                <input type="text" placeholder="Store Name"
                                       class="form-control"
                                       data-type="autocomplete-ajax"
                                       data-url="/admin/api/store"
                                       data-param-name="query"
                                       data-fn-name="callbackByStore">
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Store Name</label>
                                        <input type="text" placeholder="Store Name"
                                               class="form-control"
                                               id="store-name"
                                               readonly
                                               value="<#if machine.relativeStore?has_content>${machine.relativeStore.name.value!}</#if>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <@ui.formInputText "machine.relativeStore" "Store ID" true 255 "Enter Store Id" "number" "data-parsley-type=\"integer\""/>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Detail machine</h4></div>
                    <div class="panel-body">
                        <@ui.formInputText "machine.mid" "Machine Id" false 255 "" "text" ""/>
                        <@ui.formInputText "machine.serialNumber" "Serial Number" false 255 "" "text" ""/>
                        <@ui.formInputText "machine.price" "Price" true 255 "" "number" ""/>
                        <@ui.formInputText "machine.priceStrong" "Price Strong" false 255 "" "number" ""/>
                        <@ui.formInputText "machine.priceDry" "Price Dry" false 255 "" "number" ""/>
                        <div class="form-group">
                            <label class="control-label">Join Date</label>
                            <@spring.formInput "machine.joinDate"
                            "data-type=\"datetimepicker\"
                                 data-format=\"YYYY-MM-DD hh:mm:ss\"
                                 type=\"text\"
                                 class=\"form-control\""/>
                            <span class="help-block m-b-none">The latest effective date will be exposed.</span>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Setting</h4></div>
                    <div class="panel-body">
                        <@ui.formBoolean "machine.active" "Status" "Active" "Inactive" ""/>
                    </div>
                </div>
            </div>

        </div>
        <#-- END : SIDBAR -->

    </form>
</div>
