<#import "/spring.ftl" as spring/>
<#assign pageparam = ""/>
<#-- START widgets box-->
<div id="update-es-machine" class="container-fluid es-machine-create-update">
  <#-- FORM -->
  <form id="form-update-es-machine" action="<@spring.url header.url + "/update"/><#if springMacroRequestContext.queryString?has_content>?${springMacroRequestContext.queryString?replace("success&", "")}</#if>" method="post"
        data-parsley-validate=""
        novalidate="" data-parsley-international="true">
    <#-- Modify Complete -->
    <#include "../../common/modify-success.ftl"/>
    <#-- END : Modify Complete -->
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            Modify
          </button>
          <@spring.formHiddenInput "machine.id"/>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/><#if springMacroRequestContext.queryString?has_content>?${springMacroRequestContext.queryString?replace("success&", "")}</#if>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top"
             title="List">
            <span class="icon-list"></span>
          </a>
          <button data-type="btn-delete" type="button" class="btn btn-danger btn-lg" data-toggle="tooltip"
                  data-placement="top" title="Delete"
                  data-action="<@spring.url header.url + "/delete"/>"
                  data-id="${machine.id?c}">
            <span class="icon-trash"></span>
          </button>
        </div>

      </div>
    </div>
    <div class="row">
      <#-- CONTENT-->
      <div class="col-lg-7">


        <#-- TAB Panel or Panel Wrapper -->
        <div <#if international>role="tabpanel" class="panel panel-transparent" data-type="tabpanel-language"
             <#else>class="panel panel-default"</#if>>

          <@spring.bind "machine.name"/>
          <#assign tabPath = spring.status.expression/>
          <#-- TAB LANGUAGE HEADER -->
          <@ui.tabListByLanguage tabPath/>
          <#-- END : TAB LANGUAGE HEADER -->

          <#-- TAB LANGUAGE BODY -->
          <div <#if international>class="tab-content bg-white" <#else>class="panel-body"</#if>>

            <#-- 국문 -->
            <#if im.koKr>
              <div id="${tabPath}-ko" <#if international>role="tabpanel"
                   class="tab-pane ${im.koKr}"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "machine.name.textKoKr" "Name" true 255 "Enter the machine name."/>

                <@ui.formActiveByLanguage "machine.internationalMode.koKr" />
              </div>
            <#else>
              <@spring.formHiddenInput "machine.name.textKoKr"/>
            </#if>
            <#-- END : 국문 -->
            <#-- 영문 -->
            <#if im.enUs>
              <div id="${tabPath}-en" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "machine.name.textEnUs" "Name" true 255 "Enter the machine name."/>

                <@ui.formActiveByLanguage "machine.internationalMode.enUs"/>
              </div>
            <#else>
              <@spring.formHiddenInput "machine.name.textEnUs"/>
            </#if>
            <#-- END : 영문 -->
            <#-- 중문 (간체) -->
            <#if im.zhCn>
              <div id="${tabPath}-zh-cn" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "machine.name.textZhCn" "Name" true 255 "Enter the machine name."/>

                <@ui.formActiveByLanguage "machine.internationalMode.zhCn"/>
              </div>
            <#else>
              <@spring.formHiddenInput "machine.name.textZhCn"/>
            </#if>
            <#-- END : 중문 (간체) -->
            <#-- 중문 (번체) -->
            <#if im.zhTw>
              <div id="${tabPath}-zh-tw" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "machine.name.textZhTw" "Name" true 255 "Enter the machine name."/>

                <@ui.formActiveByLanguage "machine.internationalMode.zhTw"/>
              </div>
            <#else>
              <@spring.formHiddenInput "machine.name.textZhTw"/>
            </#if>
            <#-- END : 중문 (번체) -->
            <#-- 일문 -->
            <#if im.jaJp>
              <div id="${tabPath}-ja" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "machine.name.textJaJp" "Name" true 255 "Enter the machine name."/>

                <@ui.formActiveByLanguage "machine.internationalMode.jaJp"/>
              </div>
            <#else>
              <@spring.formHiddenInput "machine.name.textJaJp"/>
            </#if>
            <#-- END : 일문 -->
          </div>
        </div>
        <#-- END : TAB LANGUAGE BODY -->

        <div class="panel panel-default panel-list-filter">
          <div class="panel-heading">검색 필터</div>
          <div class="panel-wrapper">
            <div class="panel-body">

              <#include "../../common/list/fieldset-common.ftl"/>
              <fieldset>
                <div class="row">
                  <div class="col-lg-8">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">Reg. Date</label>

                      <div class="col-xs-5 ">
                        <div class="date list-filter-date date-wave" data-type="startDate">
                          <input name="startDate" type="text" class="form-control" placeholder="Start"
                                 value="${startDate!}" autocomplete="off">
                        </div>
                      </div>
                      <div class="col-xs-5">
                        <div class="date list-filter-date" data-type="endDate">
                          <input name="endDate" type="text" class="form-control" placeholder="End" value="${endDate!}"
                                 autocomplete="off">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>

            </div>
            <#-- 필터 실행 -->
            <div class="panel-footer">

              <button id="submit-list-filter-update" type="button" class="btn btn-primary">검색</button>

            </div>
            <#-- END : 필터 실행 -->
          </div>
        </div>
        <#-- End : 옵션 -->
        <div class="">
          <div class="row">
            <div class="col-md-12">
              <header class="clearfix pb-lg mb-sm">
                <#-- 목록 Status -->
                <div class="pull-left">
                  <div class="list-total">total ${data.page.totalElements!},&nbsp;&nbsp;${data.currentIndex}
                    /${data.page.totalPages} page
                    <span class="badge badge-success" style="color: #fff;background-color: #007bff;">Complete: ${count.countOrder}</span>
                    <span class="badge badge-danger" style="color: #fff;background-color: #dc3545;">Cancel: ${count.countCancelOrder}</span>
                  </div>
                </div>
                <div class="pull-right">
                  <span class="badge badge-success" style="color: #fff;background-color: #28a745;font-size: 28px;">Total Sale: ${count.sumOrder}w</span>
                </div>
                <#-- END : 목록 Status -->
              </header>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">

                <#-- START table-responsive-->
                <div class="table-responsive">
                  <table id="table-ext-1" class="table table-bordered table-hover">
                    <colgroup>
                      <col width="5%">
                      <col width="10%">
                      <col width="20%">
                      <col width="30%">
                      <col width="20%">
                      <col width="10%">
                    </colgroup>
                    <thead>
                    <tr>
                      <th style="vertical-align: middle" class="text-center">#</th>
                      <th style="vertical-align: middle" class="text-center">Order Number<br/>(Invoice Number)</th>
                      <th style="vertical-align: middle" class="text-center">Consumer</th>
                      <th style="vertical-align: middle" class="text-center">Price</th>
                      <th style="vertical-align: middle" class="text-center">Payment</th>
                      <th style="vertical-align: middle" class="text-center">Time</th>
                    </tr>
                    </thead>
                    <tbody>

                    <#if data?has_content>
                      <#list data.page.content as item>
                        <tr>
                          <td class="text-center">no.${data.firstNo- (item_index + 1)}<br/>id.${item.id?c}</td>

                          <td class="text-center">
                            ${item.oid!}<br/>
                            ( Device: ${item.orderDevice.value} )
                          </td>
                          <td class="text-left">

                            <strong>Member</strong>
                            <ul style="padding-left: 13px; margin-bottom: 0px">
                              <#if item.relativeBuyer?has_content>
                                <#assign user = item.relativeBuyer.relativeUser/>
                                <li>Fullname : ${user.fullName!}</li>
                                <li>PhoneNumber : ${user.phoneNumberCode!} ${user.mobile!}</li>
                                <li>Email : ${user.email!}</li>
                              </#if>
                            </ul>

                          </td>
                          <td class="text-lef">

                            Total Price Machine : ${item.amount!} KRW<br/>
                            Coupon Used : -${item.fcCoupon!} KRW<br/>
                            Point Used : -${item.fcPoint!} KRW<br/>
                            <hr style="margin: 2px 0">
                            <strong>Total : ${item.totalPrice!} KRW</strong><br/>

                          </td>
                          <td class="text-left">

                            <ul style="padding-left: 13px; margin-bottom: 0px">
                              <li>Order Status : <div class="label ${item.orderStatus.labelClass!}">${item.orderStatus.value!}</div>
                                <#if item.orderStatus == 'FAILED'>
                                  <br/>
                                  <span>Failed Reason:&nbsp;
                              <#if item.failedReasonByPayDollar?has_content>
                                ${item.failedReasonByPayDollar}
                              <#else>Unknown</#if>
                            </span>
                                </#if>
                              </li>
                              <#--                        <li>PayMethod : ${item.payMethod.value!}</li>-->
                              <#--                        <li>Payment Status : <#if item.paymentComplete>-->
                              <#--                            <div class="label label-success">Complete</div><#else>-->
                              <#--                            <div class="label label-warning">No Payment</div></#if></li>-->
                              <#if item.paymentComplete>
                                <li>Payment Date : ${item.paymentDate.format('yyyy.MM.dd HH:mm:ss')!}</li>
                              </#if>
                            </ul>
                          </td>

                          <td class="text-center">
                            <ul class="list-unstyled">
                              <li data-toggle="tooltip" data-placement="top"
                                  title="Date">${item.relativeOrderMachine.startTime.format('yyyy.MM.dd')}</li>
                              <li data-toggle="tooltip" data-placement="top"
                                  title="Time">${item.relativeOrderMachine.startTime.format('HH:mm')} - ${item.relativeOrderMachine.endTime.format('HH:mm')}</li>
                            </ul>
                          </td>
                        </tr>
                      </#list>
                    </#if>
                    </tbody>
                  </table>
                </div>
                <#-- END table-responsive-->

                <#-- TABLE FOOTER -->
                <div class="panel-footer">
                  <div class="row">
                    <div class="col-lg-12 text-center">
                      <#include "../../common/list/pagination.ftl"/>
                    </div>
                  </div>
                </div>
                <#-- END : TABLE FOOTER -->
              </div>
            </div>
          </div>
        </div>
      </div>
      <#-- END : CONTENT-->
      <#-- SIDBAR -->
      <div class="col-lg-5 pl0-lg">

        <div class="panel panel-default">
          <div class="panel-heading"><h4>기기 종류</h4></div>
          <div class="panel-body">
              <div class="form-group">
                <label class="control-label">Standard Machine Name Search (Automatically search when entered)</label>
                <input type="text" placeholder="Standard Machine Name"
                       class="form-control"
                       data-type="autocomplete-ajax"
                       data-url="/admin/api/standardMachine"
                       data-param-name="query"
                       data-fn-name="callbackByStandardMachine">
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Standard Machine Name</label>
                    <input type="text" placeholder="Standard Machine Name"
                           class="form-control"
                           id="standard-machine-name"
                           readonly
                           value="<#if machine.relativeStandardMachine?has_content>${machine.relativeStandardMachine.name.value!}</#if>">
                  </div>
                </div>
                <div class="col-sm-6">
                  <@ui.formInputText "machine.relativeStandardMachine.id" "Standard Machine ID" true 255 "Enter Standard Machine Id" "number" "data-parsley-type=\"integer\""/>
                </div>
              </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading"><h4>매장</h4></div>
          <div class="panel-body">
              <div class="form-group">
                <label class="control-label">Store Name Search (Automatically search when entered)</label>
                <input type="text" placeholder="Store Name"
                       class="form-control"
                       data-type="autocomplete-ajax"
                       data-url="/admin/api/store"
                       data-param-name="query"
                       data-fn-name="callbackByStore">
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Store Name</label>
                    <input type="text" placeholder="Store Name"
                           class="form-control"
                           id="store-name"
                           readonly
                           value="<#if machine.relativeStore?has_content>${machine.relativeStore.name.value!}</#if>">
                  </div>
                </div>
                <div class="col-sm-6">
                  <@ui.formInputText "machine.relativeStore" "Store ID" true 255 "Enter Store Id" "number" "data-parsley-type=\"integer\""/>
                </div>
              </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Detail product</h4></div>
          <div class="panel-body">
            <@ui.formInputText "machine.mid" "Machine Id" true 255 "" "text" ""/>
            <@ui.formInputText "machine.serialNumber" "Serial Number" false 255 "" "text" ""/>
            <@ui.formInputText "machine.price" "Price" false 255 "" "number" ""/>
            <@ui.formInputText "machine.priceStrong" "Price Strong" false 255 "" "number" ""/>
            <@ui.formInputText "machine.priceDry" "Price Dry" false 255 "" "number" ""/>
            <div class="form-group">
              <label class="control-label">Join Date</label>
              <@spring.formInput "machine.joinDate"
              "data-type=\"datetimepicker\"
                                 data-format=\"YYYY-MM-DD hh:mm:ss\"
                                 type=\"text\"
                                 class=\"form-control\""/>
              <span class="help-block m-b-none">The latest effective date will be exposed.</span>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Setting</h4></div>
          <div class="panel-body">
            <@ui.formBoolean "machine.active" "Status" "Active" "Inactive" ""/>
          </div>
        </div>

        <@ui.panelMetaInfo machine.updatedDate machine.createdDate/>
      </div>
      <#-- END : SIDBAR -->
    </div>
  </form>
</div>
