<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div id="update-" class="container-fluid -create-update">
  <#-- FORM -->
  <form id="form-update-point-setting" action="<@spring.url header.url/>" method="post" data-parsley-validate="" novalidate="" data-parsley-international="true">
    <#-- Modify Complete -->
    <#include "../../common/modify-success.ftl"/>
    <#-- END : Modify Complete -->
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            Modify
          </button>
          <@spring.formHiddenInput "pointSetting.id"/>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <#--<div class="pull-right mb-lg">-->
        <#--<a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top" title="List">-->
        <#--<span class="icon-list"></span>-->
        <#--</a>-->
        <#--</div>-->

      </div>
    </div>
    <div class="row">
      <#-- CONTENT-->
      <div class="col-lg-7">
        <div class="panel panel-default">
          <div class="panel-heading">Point Mode Settings</div>
          <div class="panel-body">

            <@ui.formBoolean "pointSetting.enabled" "Active Status" "Active" "Inactive" ""/>

          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading">Standard Settings</div>
          <div class="panel-body">

<#--            <@spring.formHiddenInput "pointSetting.minPoint"/>-->
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="control-label">최소 Payment포인트 <span class="text-require">*</span></label>
                  <@spring.formInput "pointSetting.minPoint",
                  "placeholder=\"Email을 입력하세요.\"
                            class=\"form-control\"
                            maxlength=\"10\"
                            data-parsley-required=\"true\"
                            data-type=\"pointSettingEnabled\"" "number"/>
                  <span class="help-block m-b-none">Member의 보유포인트가 Settings값 이상일 경우에만 Order시 Payment에 사용할 수 있습니다.</span>
                </div>
              </div>
            </div>
            <hr/>

<#--            <div class="row">-->
<#--              <div class="col-lg-6">-->
<#--                <div class="form-group">-->
<#--                  <@spring.bind "pointSetting.unit"/>-->
<#--                  <label class="control-label">Payment Point Unit <span class="text-require">*</span></label>-->
<#--                  <select data-type="pointSettingEnabled" name="${spring.status.expression}" class="form-control" required>-->
<#--                    <option value="1" <#if spring.stringStatusValue == '1'>selected</#if>>1</option>-->
<#--                    <option value="10" <#if spring.stringStatusValue == '10'>selected</#if>>10</option>-->
<#--                    <option value="50" <#if spring.stringStatusValue == '50'>selected</#if>>

50</option>-->
<#--                    <option value="100" <#if spring.stringStatusValue == '100'>selected</#if>>100</option>-->
<#--                  </select>-->
<#--                  <span class="help-block m-b-none">Sets the cut-off unit of the points used for payment.</span>-->
<#--                </div>-->
<#--              </div>-->
<#--            </div>-->
<#--            <hr/>-->

<#--            <@spring.formHiddenInput "pointSetting.maxSavingPoint"/>-->
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="control-label">최대 Saving Point <span class="text-require">*</span></label>
                  <@spring.formInput "pointSetting.maxSavingPoint",
                  "placeholder=\"Email을 입력하세요.\"
                            class=\"form-control\"
                            maxlength=\"10\"
                            data-parsley-required=\"true\"
                            data-type=\"pointSettingEnabled\"" "number"/>
                  <span class="help-block m-b-none">1회 Order 시 최대 Saving 가능한 포인트를 Settings합니다.</span>
                </div>
              </div>
            </div>
            <hr/>

<#--            <@spring.formHiddenInput "pointSetting.expiration"/>-->
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <@spring.bind "pointSetting.expiration"/>
                  <label class="control-label">포인트 유효Period <span class="text-require">*</span></label>
                  <select data-type="pointSettingEnabled" name="${spring.status.expression}" class="form-control" required>
                    <#list 1..10 as item>
                    <option value="${item}" <#if spring.stringStatusValue == item?c>selected</#if>>${item}년</option>
                    </#list>
                  </select>
                  <span class="help-block m-b-none">Saving Point의 유효Period을 Settings합니다. (연단위, ex: 1년을 Settings하면 적립일로 부터 1년뒤에 만료됩니다.)</span>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <#-- END : CONTENT-->
      <#-- SIDBAR -->
      <div class="col-lg-5 pl0-lg">

        <#-- Meta Information (Modify 페이지 필수) -->
        <div class="panel panel-default">
          <div class="panel-heading">Meta Information</div>
          <div class="panel-body">

            <label class="control-label">Recent Modified Date </label>
            <p class="form-control-static">${pointSetting.updatedDate.format('yyyy/MM/dd HH:mm:ss')}</p>

          </div>
        </div>
        <#-- END : Meta Information (Modify 페이지 필수) -->

      </div>
      <#-- END : SIDBAR -->
    </div>
  </form>
</div>
