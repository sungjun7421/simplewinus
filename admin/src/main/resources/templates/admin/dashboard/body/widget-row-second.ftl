<#-- START widgets box-->
<div class="row">

  <div class="col-lg-3 col-sm-4">
    <#-- START widget-->
    <div class="panel widget bg-green">
      <div class="row row-table">
        <div class="col-xs-4 text-center bg-green-dark pv-lg">
          <em class="fa fa-rotate-left fa-3x"></em>
        </div>
        <div class="col-xs-8 pv-lg">
          <div class="h2 mt0">${totalOrderCount}</div>
          <div class="text-uppercase">총 주문 수</div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-9 col-sm-8">
    <div class="row">
      <div class="col-xs-3">
        <div class="panel widget bg-success">
          <div class="row row-table">
            <div class="col-xs-4 text-center bg-success-dark pv-lg">
              <em class="fa fa-check-circle-o fa-3x"></em>
            </div>
            <div class="col-xs-8 pv-lg">
              <div class="h2 mt0">${totalStore}</div>
              <div class="text-uppercase">총 매장 수</div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-3">
        <#-- START widget-->
        <div class="panel widget bg-success">
          <div class="row row-table">
            <div class="col-xs-4 text-center bg-success-dark pv-lg">
              <em class="fa fa-check-circle-o fa-3x"></em>
            </div>
            <div class="col-xs-8 pv-lg">
              <div class="h2 mt0">${totalUser}</div>
              <div class="text-uppercase">총 사용자 수</div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="panel widget bg-purple">
          <div class="row row-table">
            <div class="col-xs-4 text-center bg-purple-dark pv-lg">
              <em class="fa fa-retweet fa-3x"></em>
            </div>
            <div class="col-xs-8 pv-lg">
              <div class="h2 mt0">${totalUserLeave}</div>
              <div class="text-uppercase">탈퇴 한 총 사용자 수</div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-3">
        <#-- START widget-->
        <div class="panel widget bg-success">
          <div class="row row-table">
            <div class="col-xs-4 text-center bg-success-dark pv-lg">
              <em class="fa fa-check-circle-o fa-3x"></em>
            </div>
            <div class="col-xs-8 pv-lg">
              <div class="h2 mt0">${totalMachine}</div>
              <div class="text-uppercase">총 기계 수</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<#--  <div class="col-lg-3 col-sm-6">-->
<#--      &lt;#&ndash; START widget&ndash;&gt;-->
<#--    <div class="panel widget bg-purple">-->
<#--      <div class="row row-table">-->
<#--        <div class="col-xs-4 text-center bg-purple-dark pv-lg">-->
<#--          <em class="icon-user fa-3x"></em>-->
<#--        </div>-->
<#--        <div class="col-xs-8 pv-lg">-->
<#--          <div class="h2 mt0">321</div>-->
<#--          <div class="text-uppercase">페스티벌</div>-->
<#--        </div>-->
<#--      </div>-->
<#--    </div>-->
<#--  </div>-->

<#--  <div class="col-lg-3 col-md-6 col-sm-12">-->
<#--      &lt;#&ndash; START widget&ndash;&gt;-->
<#--    <div class="panel widget bg-green">-->
<#--      <div class="row row-table">-->
<#--        <div class="col-xs-4 text-center bg-green-dark pv-lg">-->
<#--          <em class="fa fa-building-o fa-3x"></em>-->
<#--        </div>-->
<#--        <div class="col-xs-8 pv-lg">-->
<#--          <div class="h2 mt0">123</div>-->
<#--          <div class="text-uppercase">업체</div>-->
<#--        </div>-->
<#--      </div>-->
<#--    </div>-->
<#--  </div>-->

</div>
<#-- END widgets box-->
