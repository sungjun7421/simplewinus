<div class="row">
  <div class="col-lg-6">
      <#-- START messages and activity-->
    <div class="panel panel-default">
      <div class="panel-heading">
        <div class="panel-title">최근 자주 묻는 질문 리스트</div>
      </div>

        <#-- START table responsive-->
      <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
          <thead>
          <tr>
            <th class="text-center" width="15%">작성자</th>
            <th class="text-center">제목</th>
            <th class="text-center">등록일</th>

            <th class="text-center">답변상태</th>
            <th class="text-center">Reply Date</th>
            <th class="text-center">Actions</th>
          </tr>
          </thead>
          <tbody>
          <#if pageByQna?has_content>
              <#list pageByQna.content as item>
                <tr>
                  <td class="text-center">
                      <#if item.relativeUser?has_content>
                        ${item.relativeUser.fullName!}
                      <#else>-</#if>
                  </td>
                  <td class="text-left">${item.title!}</td>
                  <td class="text-center">
                      <#if item.createdDate?has_content>
                          ${item.createdDate.format('yyyy.MM.dd HH:mm:ss')}
                      <#else>-</#if>
                  </td>

                  <td class="text-center"><#if item.hasAnswer>O<#else>X</#if></td>
                  <td class="text-center">
                    <#if item.hasAnswer>
                      ${item.answer.regDate.format('yyyy.MM.dd HH:mm:ss')}
                    <#else>-</#if>
                  </td>
                  <td class="text-center">
                    <a href="<@spring.url "/admin/qna/update/${item.id?c}"/>" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="View and Edit">
                      <em class="fa fa-pencil"></em>
                    </a>
                  </td>
                </tr>
              </#list>
          </#if>
          </tbody>
        </table>
      </div>
        <#-- END table responsive-->

        <#-- START panel footer-->
      <div class="panel-footer clearfix text-center">
        <a href="<@spring.url "/admin/qna"/>"><small>상세 보기</small></a>
      </div>
        <#-- END panel-footer-->
    </div>
      <#-- END messages and activity-->
  </div>

  <div class="col-lg-6">
      <#-- START messages and activity-->
    <div class="panel panel-default">
      <div class="panel-heading">
        <div class="panel-title">주문리스트</div>
      </div>

        <#-- START table responsive-->
      <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
          <thead>
          <tr>
            <th class="text-center">주문번호</th>
            <th class="text-center">기기</th>
            <th class="text-center">금액</th>

            <th class="text-center">주문상태</th>
            <th class="text-center">등록일</th>
            <th class="text-center">Actions</th>
          </tr>
          </thead>
          <tbody>
          <#if pageByOrder?has_content>
              <#list pageByOrder.content as item>
                <tr>
                  <td class="text-center">${item.oid}</td>
                  <td class="text-center">
                      <#if item.relativeMachine?has_content>
                        <a href="<@spring.url "/admin/machine/update/${item.relativeMachine.id?c}"/>" target="_blank" class="m0" data-toggle="tooltip" data-placement="bottom" title="View Machine">
                            <#if item.relativeMachine.thumbnail?has_content>
                              <img src="${item.relativeMachine.thumbnail}" style="max-height: 60px;"/><br/>
                            </#if>
                            ${item.relativeMachine.name.value}
                        </a>
                      <#else>-</#if>
                  </td>
                  <td class="text-center">
                      <#if item.totalPrice?has_content>
                          ${item.totalPrice!}원
                      <#else>-</#if>
                  </td>

                  <td class="text-center">
                    <#if item.orderStatus?has_content>
                      ${item.orderStatus!}(${item.orderStatus.value!})
                    <#else>-</#if>
                  </td>
                  <td class="text-center">${item.createdDate.format('yyyy.MM.dd HH:mm:ss')}</td>
                  <td class="text-center">
                    <a href="<@spring.url "/admin/order/view/${item.id?c}"/>" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="View">
                      <em class="fa fa-search"></em>
                    </a>
                  </td>
                </tr>
              </#list>
          </#if>
          </tbody>
        </table>
      </div>
        <#-- END table responsive-->

        <#-- START panel footer-->
      <div class="panel-footer clearfix text-center">
        <a href="<@spring.url "/admin/order?checkedOrderStatus=WAITING"/>"><small>상세 보기</small></a>
      </div>
        <#-- END panel-footer-->
    </div>
      <#-- END messages and activity-->
  </div>
</div>
