<div class="row" data-type="search-word-list">

  <div class="col-lg-4">
      <#-- START messages and activity-->
    <div class="panel panel-default">
      <div class="panel-heading">
        <div class="panel-title">${label_dateByDaily} (${dateByDaily.format('yyyy-MM-dd')})</div>
      </div>

        <#-- START table responsive-->
      <div class="table-responsive">
        <table id="daily-table" data-from="${dateByDaily!}" data-to="${dateByDaily!}" class="table table-bordered table-hover table-striped">
          <thead>
          <tr>
            <th class="text-center">Rank</th>
            <th class="text-center">Keyword</th>
            <th class="text-center">Accumulated Cnt</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td colspan="3" class="text-center"><i class="fa fa-circle-o-notch fa-spin" style="font-size: 20px;"></i></td>
          </tr>
<#--          <#if listSearchWordByDaily?has_content>-->
<#--              <#list listSearchWordByDaily as item>-->
<#--                <tr>-->
<#--                  <td class="text-center">${item?counter}</td>-->
<#--                  <td class="text-center">${item.word!}</td>-->
<#--                  <td class="text-center">${item.accumulatedCnt!}</td>-->
<#--                </tr>-->
<#--              </#list>-->
<#--          </#if>-->
          </tbody>
        </table>
      </div>
        <#-- END table responsive-->

        <#-- START panel footer-->
      <div class="panel-footer clearfix text-center">
        <a href="<@spring.url "/admin/search-word?startDate=${dateByDaily!}&endDate=${dateByDaily!}"/>">
          <small>상세 보기</small>
        </a>
      </div>
        <#-- END panel-footer-->
    </div>
      <#-- END messages and activity-->
  </div>

  <div class="col-lg-4">
      <#-- START messages and activity-->
    <div class="panel panel-default">
      <div class="panel-heading">
        <div class="panel-title">${label_DateByWeekly} (${fromDateByWeekly.format('yyyy-MM-dd')} ~ ${toDateByWeekly.format('yyyy-MM-dd')})</div>
      </div>

        <#-- START table responsive-->
      <div class="table-responsive">
        <table id="weekly-table" data-from="${fromDateByWeekly!}" data-to="${toDateByWeekly!}" class="table table-bordered table-hover table-striped">
          <thead>
          <tr>
            <th class="text-center">Rank</th>
            <th class="text-center">Keyword</th>
            <th class="text-center">Accumulated Cnt</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td colspan="3" class="text-center"><i class="fa fa-circle-o-notch fa-spin" style="font-size: 20px;"></i></td>
          </tr>
<#--          <#if listSearchWordByWeekly?has_content>-->
<#--              <#list listSearchWordByWeekly as item>-->
<#--                <tr>-->
<#--                  <td class="text-center">${item?counter}</td>-->
<#--                  <td class="text-center">${item.word!}</td>-->
<#--                  <td class="text-center">${item.accumulatedCnt!}</td>-->
<#--                </tr>-->
<#--              </#list>-->
<#--          </#if>-->
          </tbody>
        </table>
      </div>
        <#-- END table responsive-->

        <#-- START panel footer-->
      <div class="panel-footer clearfix text-center">
        <a href="<@spring.url "/admin/search-word?startDate=${fromDateByWeekly!}&endDate=${toDateByWeekly!}"/>">
          <small>상세 보기</small>
        </a>
      </div>
        <#-- END panel-footer-->
    </div>
      <#-- END messages and activity-->
  </div>

  <div class="col-lg-4">
      <#-- START messages and activity-->
    <div class="panel panel-default">
      <div class="panel-heading">
        <div class="panel-title">${label_DateByMonthly} (${fromDateByMonthly.format('yyyy-MM-dd')} ~ ${toDateByMonthly.format('yyyy-MM-dd')})</div>
      </div>

        <#-- START table responsive-->
      <div class="table-responsive">
        <table id="monthly-table" data-from="${fromDateByMonthly!}" data-to="${toDateByMonthly!}" class="table table-bordered table-hover table-striped">
          <thead>
          <tr>
            <th class="text-center">Rank</th>
            <th class="text-center">Keyword</th>
            <th class="text-center">Accumulated Cnt</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td colspan="3" class="text-center"><i class="fa fa-circle-o-notch fa-spin" style="font-size: 20px;"></i></td>
          </tr>
<#--          <#if listSearchWordByMonthly?has_content>-->
<#--              <#list listSearchWordByMonthly as item>-->
<#--                <tr>-->
<#--                  <td class="text-center">${item?counter}</td>-->
<#--                  <td class="text-center">${item.word!}</td>-->
<#--                  <td class="text-center">${item.accumulatedCnt!}</td>-->
<#--                </tr>-->
<#--              </#list>-->
<#--          </#if>-->
          </tbody>
        </table>
      </div>
        <#-- END table responsive-->

        <#-- START panel footer-->
      <div class="panel-footer clearfix text-center">
        <a href="<@spring.url "/admin/search-word?startDate=${fromDateByMonthly!}&endDate=${toDateByMonthly!}"/>">
          <small>상세 보기</small>
        </a>
      </div>
        <#-- END panel-footer-->
    </div>
      <#-- END messages and activity-->
  </div>

</div>