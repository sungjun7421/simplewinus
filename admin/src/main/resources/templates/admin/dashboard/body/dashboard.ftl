<#-- START widgets box-->
<div class="row">

  <div class="col-lg-3 col-sm-6">
      <#-- START widget-->
    <div class="panel widget bg-primary">
      <div class="row row-table">
        <div class="col-xs-4 text-center bg-primary-dark pv-lg">
          <em class="fa fa-money fa-3x"></em>
        </div>
        <div class="col-xs-8 pv-lg">
          <div class="h2 mt0">${totalSalesAmount} <small style="color: white">VND</small></div>
          <div class="text-uppercase">${label_total_sales_amount}</div>
          <div class="text-sm">&nbsp;</div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-3 col-sm-6">
      <#-- START widget-->
    <div class="panel widget bg-info">
      <div class="row row-table">
        <div class="col-xs-4 text-center bg-info-dark pv-lg">
          <em class="fa fa-shopping-cart fa-3x"></em>
        </div>
        <div class="col-xs-8 pv-lg">
          <div class="h2 mt0">${totalOrderCount}</div>
          <div class="text-uppercase">${label_totalOrderCount}</div>
          <div class="text-sm">&nbsp;</div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-3 col-sm-6">
      <#-- START widget-->
    <div class="panel widget bg-info">
      <div class="row row-table">
        <div class="col-xs-4 text-center bg-info-dark pv-lg">
          <em class="fa fa-shopping-cart fa-3x"></em>
        </div>
        <div class="col-xs-8 pv-lg">
          <div class="h2 mt0">${totalOrderCntByPAYMENT_COMPLETE}</div>
          <div class="text-uppercase">${label_totalOrderCntByPAYMENT_COMPLETE}</div>
          <div class="text-sm">(${sub_label_totalOrderCntByPAYMENT_COMPLETE})</div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-3 col-sm-6">
      <#-- START widget-->
    <div class="panel widget bg-info">
      <div class="row row-table">
        <div class="col-xs-4 text-center bg-info-dark pv-lg">
          <em class="fa fa-shopping-cart fa-3x"></em>
        </div>
        <div class="col-xs-8 pv-lg">
          <div class="h2 mt0">${totalOrderCntByDelivery}</div>
          <div class="text-uppercase">${label_totalOrderCntByDelivery}</div>
          <div class="text-sm">(${sub_label_totalOrderCntByDelivery})</div>
        </div>
      </div>
    </div>
  </div>

<#--  <div class="col-lg-3 col-sm-6">-->
<#--      &lt;#&ndash; START widget&ndash;&gt;-->
<#--    <div class="panel widget bg-purple">-->
<#--      <div class="row row-table">-->
<#--        <div class="col-xs-4 text-center bg-purple-dark pv-lg">-->
<#--          <em class="icon-user fa-3x"></em>-->
<#--        </div>-->
<#--        <div class="col-xs-8 pv-lg">-->
<#--          <div class="h2 mt0">321</div>-->
<#--          <div class="text-uppercase">페스티벌</div>-->
<#--        </div>-->
<#--      </div>-->
<#--    </div>-->
<#--  </div>-->

<#--  <div class="col-lg-3 col-md-6 col-sm-12">-->
<#--      &lt;#&ndash; START widget&ndash;&gt;-->
<#--    <div class="panel widget bg-green">-->
<#--      <div class="row row-table">-->
<#--        <div class="col-xs-4 text-center bg-green-dark pv-lg">-->
<#--          <em class="fa fa-building-o fa-3x"></em>-->
<#--        </div>-->
<#--        <div class="col-xs-8 pv-lg">-->
<#--          <div class="h2 mt0">123</div>-->
<#--          <div class="text-uppercase">업체</div>-->
<#--        </div>-->
<#--      </div>-->
<#--    </div>-->
<#--  </div>-->

</div>
<#-- END widgets box-->
