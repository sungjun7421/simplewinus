<div class="row">
    <div class="col-lg-9">

        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">통계 그래프</div>
            </div>
            <div class="panel-body">
                <div data-type="chart-dashboard"
                     data-url="/admin/api/analytics/dashboard/DAY"
                     class="flot-chart"></div>
            </div>
        </div>

    </div>
    <div class="col-lg-3">
        <!-- START messages and activity-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">활동 로그</div>
            </div>

            <!-- START list group-->
            <div class="list-group mb0">
            <#list pageByLog.content as log>
                <div class="list-group-item bt0">
                    <#if log.type == 'SIGNIN'>
                        <span class="label label-success pull-right">SIGNIN</span>
                    <#elseif log.type == 'SIGNOUT'>
                        <span class="label label-success pull-right">SIGNOUT</span>
                    <#elseif log.type == 'SIGNUP'>
                        <span class="label label-success pull-right">SIGNUP</span>
                    </#if>
                    ${log.message!}
                </div>
            </#list>
            </div>
            <!-- END list group-->

            <!-- START panel footer-->
            <div class="panel-footer clearfix">
                <a href="<@spring.url "/admin/log"/>" class="pull-left">
                    <small>더보기</small>
                </a>
            </div>
            <!-- END panel-footer-->
        </div>
        <!-- END messages and activity-->
    </div>
</div>