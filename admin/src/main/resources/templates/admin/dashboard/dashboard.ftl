<#import "/spring.ftl" as spring/>

<!DOCTYPE html>
<html lang="ko" xmlns:spring="http://www.w3.org/1999/XSL/Transform">

<head>
<#include "../common/head-meta.ftl">
<#include "../common/css/create.ftl">
</head>
<body>
<div class="wrapper">
<#-- top navbar-->
<#include "../common/top-navbar.ftl">
<#-- sidebar-->
<#include "../common/sidebar.ftl">

<#-- Main section -->
  <section>
  <#-- Page content-->
    <div class="content-wrapper">
        <#include "../common/header.ftl">
    <#--<#include "body/row-current-product.ftl.">-->
      <div class="container-fluid">
<#--        <h4>[대시보드 작업 TO-DO 목록]</h4>-->
<#--        <ol>-->
<#--          <li>오늘의 쇼핑통계 :-->
<#--            <ul>-->
<#--              <li>오늘Order건, 오늘매출액, Product문의, 1:1문의, 오늘신규Member, 오늘방문자수 등 - 호텔별로 구분 되어야 함</li>-->
<#--            </ul>-->
<#--          </li>-->
<#--          <li>쇼핑몰 운영현황 : Period 대비 실적에 대한 집계Information-->
<#--            <ul>-->
<#--              <li>Payment, Shipping준비, Shipping중, ShippingComplete의 집계</li>-->
<#--              <li>Cancel, 환불, 교환의 집계</li>-->
<#--            </ul>-->
<#--          </li>-->
<#--          <li>호텔 현황-->
<#--            <ul>-->
<#--              <li>호텔수, 호텔리스트, 호텔별 방문수, 디바이스개수 등</li>-->
<#--            </ul>-->
<#--          </li>-->
<#--          <li>게시판 현황-->
<#--            <ul>-->
<#--              <li>Product문의 및 1:1문의 등 주요 문의게시판에 대한 최근 게시글 현황의 보드리스트</li>-->
<#--            </ul>-->
<#--          </li>-->
<#--          <li>날짜별 환율 Information (API 연동)</li>-->
<#--        </ol>-->

      <#-- Widgets -->
<#--      <#include "body/dashboard.ftl"/>-->
<#--      <#include "body/widget-row-first.ftl"/>-->
<#--      <#include "body/widget-row-second.ftl"/>-->
<#--      <#include "body/widget-row-third.ftl"/>-->
<#--      <#include "body/widget-row-fourth.ftl"/>-->
      <#-- End : Widgets -->


      </div>
    </div>
  </section>

<#-- Page footer-->
<#include "../common/footer.ftl">
</div>

<#include "../common/vendor.ftl">

<#-- MOMENT JS-->
<script src="<@spring.url"/ad/vendor/moment/min/moment-with-locales.min.js"/>"></script>


<#-- SPARKLINE-->
<script src="<@spring.url "/ad/vendor/sparkline/index.js"/>"></script>
<#-- FLOT CHART-->
<script src="<@spring.url "/ad/vendor/flot/jquery.flot.js"/>"></script>
<script src="<@spring.url "/ad/vendor/flot.tooltip/js/jquery.flot.tooltip.min.js"/>"></script>
<script src="<@spring.url "/ad/vendor/flot/jquery.flot.resize.js"/>"></script>
<script src="<@spring.url "/ad/vendor/flot/jquery.flot.pie.js"/>"></script>
<script src="<@spring.url "/ad/vendor/flot/jquery.flot.time.js"/>"></script>
<script src="<@spring.url "/ad/vendor/flot/jquery.flot.categories.js"/>"></script>
<script src="<@spring.url "/ad/vendor/flot-spline/js/jquery.flot.spline.min.js"/>"></script>


</body>

</html>
