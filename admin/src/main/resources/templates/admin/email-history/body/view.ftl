<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div id="view-email-history" class="container-fluid email-history-view">
    <#-- FORM -->

  <div class="row">
    <div class="col-md-12">

      <div class="pull-right mb-lg">
        <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip"
           data-placement="top" title="List">
          <span class="icon-list"></span>
        </a>
      </div>

    </div>
  </div>
  <div class="row">
      <#-- CONTENT-->
    <div class="col-lg-7">

      <div class="panel panel-default">
        <div class="panel-body">

          <div class="form-group">
            <label class="control-label">From Email Address </label>
            <p class="form-control-static">${smsHistory.sendAddress!}</p>
          </div>
          <hr/>
          <div class="form-group">
            <label class="control-label">To Email Address </label>
            <p class="form-control-static">${smsHistory.destAddress!}</p>
          </div>
          <hr/>
          <div class="form-group">
            <label class="control-label">Title </label>
            <p class="form-control-static">${smsHistory.subject!}</p>
          </div>
          <hr/>
          <div class="form-group">
            <label class="control-label">Message </label>
            <p class="form-control-static">${smsHistory.msgBody!}</p>
          </div>

        </div>
      </div>

    </div>
      <#-- END : CONTENT-->
      <#-- SIDBAR -->
    <div class="col-lg-5 pl0-lg">
        <#if smsHistory.sendTime?has_content>
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="form-group">
                <label class="control-label">Send Time </label>
                <p class="form-control-static">${smsHistory.sendTime.format('yyyy.MM.dd HH:mm:ss')}</p>
              </div>
            </div>
          </div>
        </#if>
        <#if smsHistory.relativeUser?has_content>
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="form-group">
                <label class="control-label">From User</label>
                <p class="form-control-static">
                  <a href="<@spring.url "/admin/user/update/${smsHistory.relativeUser.id?c}"/>">
                    ${smsHistory.relativeUser.email} [${smsHistory.relativeUser.fullname}]
                  </a>
                </p>
              </div>
            </div>
          </div>
        </#if>

        <#-- Meta Information (Modify 페이지 필수) -->
        <@ui.panelMetaInfo smsHistory.updatedDate smsHistory.createdDate/>
        <#-- END : Meta Information (Modify 페이지 필수) -->
    </div>
      <#-- END : SIDBAR -->
  </div>

</div>