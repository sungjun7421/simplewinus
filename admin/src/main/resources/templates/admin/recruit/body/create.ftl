<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div class="container-fluid recruit-create-update">
    <#-- FORM -->
  <form id="form-create-recruit" action="<@spring.url header.url + "/create"/>" method="post" data-parsley-validate=""
        novalidate="" data-parsley-international="true">
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            저장
          </button>

          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top"
             title="목록보기">
            <span class="icon-list"></span>
          </a>
        </div>

      </div>
    </div>
    <div class="row">
        <#-- CONTENT-->
      <div class="col-lg-7">

          <#-- TAB Panel or Panel Wrapper -->
        <div <#if international>role="tabpanel" class="panel panel-transparent" data-type="tabpanel-language"
             <#else>class="panel panel-default"</#if>>

            <@spring.bind "recruit.title"/>
            <#assign tabPath = spring.status.expression/>
            <#-- TAB LANGUAGE HEADER -->
            <@ui.tabListByLanguage tabPath/>
            <#-- END : TAB LANGUAGE HEADER -->

            <#-- TAB LANGUAGE BODY -->
          <div <#if international>class="tab-content bg-white" <#else>class="panel-body"</#if>>

              <#-- 국문 -->
              <#if im.koKr>
                <div id="${tabPath}-ko" <#if international>role="tabpanel" class="tab-pane"</#if>>

                    <#-- 제목 -->
                    <@ui.formInputTextByLanguage "recruit.title.textKoKr" "제목" true 200 "제목을 입력하세요."/>

                    <#-- WYSISWYG EDITOR -->
                  <hr/>
                    <@ui.wysiswygEdior "recruit.content.textKoKr" "내용" true/>

<#--                    <@ui.formActiveByLanguage "recruit.internationalMode.koKr" />-->
                </div>
              <#else>
                  <@spring.formHiddenInput "recruit.title.textKoKr"/>
<#--                  <@spring.formHiddenInput "recruit.content.textKoKr"/>-->
              </#if>
              <#-- END : 국문 -->
              <#-- 영문 -->
              <#if im.enUs>
                <div id="${tabPath}-en" <#if international>role="tabpanel" class="tab-pane"</#if>>

                    <#-- 제목 -->
                    <@ui.formInputTextByLanguage "recruit.title.textEnUs" "제목" true 200 "제목을 입력하세요."/>

                    <#-- WYSISWYG EDITOR -->
                  <hr/>
                    <@ui.wysiswygEdior "recruit.content.textEnUs" "내용" false/>

<#--                    <@ui.formActiveByLanguage "recruit.internationalMode.enUs"/>-->
                </div>
              <#else>
                  <@spring.formHiddenInput "recruit.title.textEnUs"/>
<#--                  <@spring.formHiddenInput "recruit.content.textEnUs"/>-->
              </#if>
              <#-- END : 영문 -->
              <#-- 중문 (간체) -->
              <#if im.zhCn>
                <div id="${tabPath}-zh-cn" <#if international>role="tabpanel" class="tab-pane"</#if>>

                    <#-- 제목 -->
                    <@ui.formInputTextByLanguage "recruit.title.textZhCn" "제목" true 100 "제목을 입력하세요."/>
                  <hr/>

                    <#-- WYSISWYG EDITOR -->
                    <@ui.wysiswygEdior "recruit.content.textZhCn" "내용" false/>
<#--                    <@ui.formActiveByLanguage "recruit.internationalMode.zhCn"/>-->
                </div>
              <#else>
                  <@spring.formHiddenInput "recruit.title.textZhCn"/>
<#--                  <@spring.formHiddenInput "recruit.content.textZhCn"/>-->
              </#if>
              <#-- END : 중문 (간체) -->
              <#-- 중문 (번체) -->
              <#if im.zhTw>
                <div id="${tabPath}-zh-tw" <#if international>role="tabpanel" class="tab-pane"</#if>>

                    <#-- 제목 -->
                    <@ui.formInputTextByLanguage "recruit.title.textZhTw" "제목" true 100 "제목을 입력하세요."/>
                  <hr/>

                    <#-- WYSISWYG EDITOR -->
                    <@ui.wysiswygEdior "recruit.content.textZhTw" "내용" false/>
<#--                    <@ui.formActiveByLanguage "recruit.internationalMode.zhTw"/>-->
                </div>
              <#else>
                  <@spring.formHiddenInput "recruit.title.textZhTw"/>
<#--                  <@spring.formHiddenInput "recruit.content.textZhTw"/>-->
              </#if>
              <#-- END : 중문 (번체) -->
              <#-- 일문 -->
              <#if im.jaJp>
                <div id="${tabPath}-ja" <#if international>role="tabpanel" class="tab-pane"</#if>>

                    <#-- 제목 -->
                    <@ui.formInputTextByLanguage "recruit.title.textJaJp" "제목" true 100 "제목을 입력하세요."/>
                  <hr/>

                    <#-- WYSISWYG EDITOR -->
                    <@ui.wysiswygEdior "recruit.content.textJaJp" "내용" true/>
<#--                    <@ui.formActiveByLanguage "recruit.internationalMode.jaJp"/>-->
                </div>
              <#else>
                  <@spring.formHiddenInput "recruit.title.textJaJp"/>
<#--                  <@spring.formHiddenInput "recruit.content.textJaJp"/>-->
              </#if>
              <#-- END : 일문 -->
          </div>
        </div>
          <#-- END : TAB LANGUAGE BODY -->
        <div class="panel panel-default">
          <div class="panel-heading">요강</div>
          <div class="panel-body">

              <@ui.formRadioboxEnum "recruit.type" "채용구분" false types/>
              <@ui.formRadioboxEnum "recruit.status" "진행단계" false statuses/>
            <hr/>
              <@ui.formInputDefault "recruit.career" "경력" false 10 "경력을 입력하세요."/>

              <@ui.formBoolean "recruit.untilRecruitment" "채용시까지" "활성" "비활성" "활성일 경우 최상단에 노출됩니다."/>

              <@ui.formDate "recruit.startDate" "시작날짜" true "YYYY-MM-DD"/>
              <@ui.formDate "recruit.endDate" "종료날짜" true "YYYY-MM-DD"/>

              <@ui.formInputText "recruit.recruitment" "모집인원" false 12 "모집인원를 입력하세요." "number"/>

            <hr/>

              <@ui.formTextarea "recruit.jobDescription" "담당 업무" false 500 "담당 업무를 입력하세요."/>
              <@ui.formTextarea "recruit.condition" "근무 조건" false 500 "근무 조건을 입력하세요."/>
              <@ui.formTextarea "recruit.appProcess" "지원서류 및 전형절차" false 500 "지원서류 및 전형절차를 입력하세요."/>
              <@ui.formTextarea "recruit.contact" "지원방법 및 문의" false 500 "지원방법 및 문의를 입력하세요."/>
          </div>
        </div>
      </div>
        <#-- END : CONTENT-->
        <#-- SIDBAR -->
      <div class="col-lg-5 pl0-lg">

        <div class="panel panel-default">
          <div class="panel-heading">설정</div>
          <div class="panel-body">


            <hr/>

            <hr/>
              <@ui.formInputDefault "recruit.recruitment" "모집인원" false 12 "모집인원을 입력하세요."/>
            <hr/>
              <@ui.formInputDefault "recruit.writer" "작성자" false 12 "작성자를 입력하세요."/>

            <hr/>
              <#-- 활성 모드 -->
              <@ui.formActive "recruit.active"/>


          </div>
        </div>

      </div>
        <#-- END : SIDBAR -->
    </div>
  </form>
</div>