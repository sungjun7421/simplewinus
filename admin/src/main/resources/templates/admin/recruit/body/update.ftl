<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div id="update-recruit" class="container-fluid recruit-create-update">
    <#-- FORM -->
  <form id="form-update-recruit" action="<@spring.url header.url + "/update"/>" method="post" data-parsley-validate=""
        novalidate="" data-parsley-international="true">
      <#-- 수정 완료 -->
      <#include "../../common/modify-success.ftl"/>
      <#-- END : 수정 완료 -->
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            수정
          </button>
            <@spring.formHiddenInput "recruit.id"/>

          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top"
             title="목록보기">
            <span class="icon-list"></span>
          </a>
          <button data-type="btn-delete" type="button" class="btn btn-danger btn-lg" data-toggle="tooltip"
                  data-placement="top" title="삭제"
                  data-action="<@spring.url header.url + "/delete"/>"
                  data-id="${recruit.id?c}">
            <span class="icon-trash"></span>
          </button>
        </div>

      </div>
    </div>
    <div class="row">
        <#-- CONTENT-->
      <div class="col-lg-7">

          <#-- TAB Panel or Panel Wrapper -->
        <div <#if international>role="tabpanel" class="panel panel-transparent" data-type="tabpanel-language"
             <#else>class="panel panel-default"</#if>>

            <@spring.bind "recruit.title"/>
            <#assign tabPath = spring.status.expression/>
            <#-- TAB LANGUAGE HEADER -->
            <@ui.tabListByLanguage tabPath/>
            <#-- END : TAB LANGUAGE HEADER -->

            <#-- TAB LANGUAGE BODY -->
          <div <#if international>class="tab-content bg-white" <#else>class="panel-body"</#if>>

              <#-- 국문 -->
              <#if im.koKr>
                <div id="${tabPath}-ko" <#if international>role="tabpanel" class="tab-pane"</#if>>

                    <#-- 제목 -->
                    <@ui.formInputTextByLanguage "recruit.title.textKoKr" "제목" true 200 "제목을 입력하세요."/>

                    <#-- WYSISWYG EDITOR -->
                  <hr/>
                    <@ui.wysiswygEdior "recruit.content.textKoKr" "내용" true/>

                    <@ui.formActiveByLanguage "recruit.internationalMode.koKr" />
                </div>
              <#else>
                  <@spring.formHiddenInput "recruit.title.textKoKr"/>
                  <@spring.formHiddenInput "recruit.content.textKoKr"/>
              </#if>
              <#-- END : 국문 -->
              <#-- 영문 -->
              <#if im.enUs>
                <div id="${tabPath}-en" <#if international>role="tabpanel" class="tab-pane"</#if>>

                    <#-- 제목 -->
                    <@ui.formInputTextByLanguage "recruit.title.textEnUs" "제목" true 200 "제목을 입력하세요."/>

                    <#-- WYSISWYG EDITOR -->
                  <hr/>
                    <@ui.wysiswygEdior "recruit.content.textEnUs" "내용" false/>

                    <@ui.formActiveByLanguage "recruit.internationalMode.enUs"/>
                </div>
              <#else>
                  <@spring.formHiddenInput "recruit.title.textEnUs"/>
                  <@spring.formHiddenInput "recruit.content.textEnUs"/>
              </#if>
              <#-- END : 영문 -->
              <#-- 중문 (간체) -->
              <#if im.zhCn>
                <div id="${tabPath}-zh-cn" <#if international>role="tabpanel" class="tab-pane"</#if>>

                    <#-- 제목 -->
                    <@ui.formInputTextByLanguage "recruit.title.textZhCn" "제목" true 100 "제목을 입력하세요."/>
                  <hr/>

                    <#-- WYSISWYG EDITOR -->
                    <@ui.wysiswygEdior "recruit.content.textZhCn" "내용" false/>
                    <@ui.formActiveByLanguage "recruit.internationalMode.zhCn"/>
                </div>
              <#else>
                  <@spring.formHiddenInput "recruit.title.textZhCn"/>
                  <@spring.formHiddenInput "recruit.content.textZhCn"/>
              </#if>
              <#-- END : 중문 (간체) -->
              <#-- 중문 (번체) -->
              <#if im.zhTw>
                <div id="${tabPath}-zh-tw" <#if international>role="tabpanel" class="tab-pane"</#if>>

                    <#-- 제목 -->
                    <@ui.formInputTextByLanguage "recruit.title.textZhTw" "제목" true 100 "제목을 입력하세요."/>
                  <hr/>

                    <#-- WYSISWYG EDITOR -->
                    <@ui.wysiswygEdior "recruit.content.textZhTw" "내용" false/>
                    <@ui.formActiveByLanguage "recruit.internationalMode.zhTw"/>
                </div>
              <#else>
                  <@spring.formHiddenInput "recruit.title.textZhTw"/>
                  <@spring.formHiddenInput "recruit.content.textZhTw"/>
              </#if>
              <#-- END : 중문 (번체) -->
              <#-- 일문 -->
              <#if im.jaJp>
                <div id="${tabPath}-ja" <#if international>role="tabpanel" class="tab-pane"</#if>>

                    <#-- 제목 -->
                    <@ui.formInputTextByLanguage "recruit.title.textJaJp" "제목" true 100 "제목을 입력하세요."/>
                  <hr/>

                    <#-- WYSISWYG EDITOR -->
                    <@ui.wysiswygEdior "recruit.content.textJaJp" "내용" true/>
                    <@ui.formActiveByLanguage "recruit.internationalMode.jaJp"/>
                </div>
              <#else>
                  <@spring.formHiddenInput "recruit.title.textJaJp"/>
                  <@spring.formHiddenInput "recruit.content.textJaJp"/>
              </#if>
              <#-- END : 일문 -->
          </div>
        </div>
          <#-- END : TAB LANGUAGE BODY -->

      </div>
        <#-- END : CONTENT-->
        <#-- SIDBAR -->
      <div class="col-lg-5 pl0-lg">

        <div class="panel panel-default">
          <div class="panel-heading">설정</div>
          <div class="panel-body">

              <@ui.formDate "recruit.startDate" "시작날짜" true "YYYY-MM-DD"/>
              <@ui.formDate "recruit.endDate" "종료날짜" true "YYYY-MM-DD"/>
            <hr/>
              <@ui.formRadioboxEnum "recruit.type" "채용구분" false types/>
              <@ui.formRadioboxEnum "recruit.status" "진행단계" false statuses/>

            <hr/>
              <@ui.formInputDefault "recruit.recruitment" "모집인원" false 12 "모집인원을 입력하세요."/>
            <hr/>
              <@ui.formInputDefault "recruit.writer" "작성자" false 12 "작성자를 입력하세요."/>

            <hr/>
              <#-- 활성 모드 -->
              <@ui.formActive "recruit.active"/>


          </div>
        </div>

          <@ui.panelMetaInfo recruit.updatedDate recruit.createdDate/>
      </div>
        <#-- END : SIDBAR -->
    </div>
  </form>
</div>