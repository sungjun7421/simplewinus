<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div class="container-fluid -create-update">
  <#-- FORM -->
  <form id="form-create-" action="<@spring.url header.url + "/create"/>" method="post" data-parsley-validate=""
        novalidate="" data-parsley-international="true">
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            Save
          </button>
          <@spring.formHiddenInput "event.type"/>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top"
             title="List">
            <span class="icon-list"></span>
          </a>
        </div>

      </div>
    </div>
    <div class="row">
      <#-- CONTENT-->
      <div class="col-lg-7">

        <#-- TAB Panel or Panel Wrapper -->
        <div <#if international>role="tabpanel" class="panel panel-transparent" data-type="tabpanel-language"
             <#else>class="panel panel-default"</#if>>

          <@spring.bind "event.title"/>
          <#assign tabPath = spring.status.expression/>
          <#-- TAB LANGUAGE HEADER -->
          <@ui.tabListByLanguage tabPath/>
          <#-- END : TAB LANGUAGE HEADER -->

          <#-- TAB LANGUAGE BODY -->

          <div <#if international>class="tab-content bg-white" <#else>class="panel-body"</#if>>

            <#-- 국문 -->
            <#if im.koKr>
              <div id="${tabPath}-ko" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "event.title.textKoKr" "Title" true 200 "Please enter a title."/>
                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "event.content.textKoKr" "Content" false/>
                <hr/>
                <div class="row">
                  <div class="col-xs-4"><@ui.uploadImage "Thumbnail" "thumb.textKoKr" "" "JPEG 800 X 400"/></div>
                </div>

                <@ui.formActiveByLanguage "event.internationalMode.koKr" />
              </div>
            <#else>
              <@spring.formHiddenInput "event.title.textKoKr"/>
              <@spring.formHiddenInput "event.content.textKoKr"/>
              <@spring.formHiddenInput "event.thumb.textKoKr"/>
            </#if>
            <#-- END : 국문 -->
            <#-- 영문 -->
            <#if im.enUs>
              <div id="${tabPath}-en" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "event.title.textEnUs" "Title" true 200 "Please enter a title."/>
                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "event.content.textEnUs" "Content" false/>
                <hr/>
                <div class="row">
                  <div class="col-xs-4"><@ui.uploadImage "Thumbnail" "thumb.textEnUs" "" "JPEG 800 X 400"/></div>
                </div>

                <hr/>

                <@ui.formActiveByLanguage "event.internationalMode.EnUs"/>
              </div>
            <#else>
              <@spring.formHiddenInput "event.title.textEnUs"/>
              <@spring.formHiddenInput "event.content.textEnUs"/>
              <@spring.formHiddenInput "event.thumb.textEnUs"/>
              <@spring.formHiddenInput "event.contentImgPc.textEnUs"/>
              <@spring.formHiddenInput "event.contentImgMb.textEnUs"/>
            <#--              <@spring.formHiddenInput "event.content.textEnUs"/>-->
            </#if>
            <#-- END : 영문 -->
            <#-- 중문 (간체) -->
            <#if im.zhCn>
              <div id="${tabPath}-zh-cn" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "event.title.textZhCn" "Title" true 200 "Please enter a title."/>
                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "event.content.textZhCn" "Content" false/>

                <hr/>
                <div class="row">
                  <div class="col-xs-4"><@ui.uploadImage "Thumbnail" "thumb.textZhCn" "" "JPEG 800 X 400"/></div>
                </div>

                <hr/>
                <@ui.formActiveByLanguage "event.internationalMode.ZhCn"/>
              </div>
            <#else>
              <@spring.formHiddenInput "event.title.textZhCn"/>
              <@spring.formHiddenInput "event.content.textZhCn"/>
              <@spring.formHiddenInput "event.thumb.textZhCn"/>
            </#if>
            <#-- END : 중문 (간체) -->
            <#-- 중문 (번체) -->
            <#if im.zhTw>
              <div id="${tabPath}-zh-tw" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "event.title.textZhTw" "Title" true 200 "Please enter a title."/>
                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "event.content.textZhTw" "Content" false/>
                <hr/>
                <div class="row">
                  <div class="col-xs-4"><@ui.uploadImage "Thumbnail" "thumb.textZhTw" "" "JPEG 800 X 400"/></div>
                </div>

                <hr/>

                <@ui.formActiveByLanguage "event.internationalMode.zhTw"/>
              </div>
            <#else>
              <@spring.formHiddenInput "event.title.textZhTw"/>
              <@spring.formHiddenInput "event.content.textZhTw"/>
              <@spring.formHiddenInput "event.thumb.textZhTw"/>
            </#if>
            <#-- END : 중문 (번체) -->
            <#-- 일문 -->
            <#if im.jaJp>
              <div id="${tabPath}-ja" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "event.title.textJaJp" "Title" true 200 "Please enter a title."/>
                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "event.content.textJaJp" "Content" false/>
                <hr/>
                <div class="row">
                  <div class="col-xs-4"><@ui.uploadImage "Thumbnail" "thumb.textJaJp" "" "JPEG 800 X 400"/></div>
                </div>

                <hr/>

                <@ui.formActiveByLanguage "event.internationalMode.jaJp"/>
              </div>
            <#else>
              <@spring.formHiddenInput "event.title.textJaJp"/>
              <@spring.formHiddenInput "event.content.textJaJp"/>
              <@spring.formHiddenInput "event.thumb.textJaJp"/>
            </#if>
            <#-- END : 일문 -->
            <#-- VietNam -->
            <#if im.viVn>
              <div id="${tabPath}-vi" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "event.title.textViVn" "Title" true 200 "Please enter a title."/>
                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "event.content.textViVn" "Content" true/>
                <hr/>
                <div class="row">
                  <div class="col-xs-4"><@ui.uploadImage "Thumbnail" "thumb.textViVn" "" "JPEG 800 X 400"/></div>
                </div>

                <hr/>

                <@ui.formActiveByLanguage "event.internationalMode.viVn"/>
              </div>
            <#else>
              <@spring.formHiddenInput "event.title.textViVn"/>
              <@spring.formHiddenInput "event.thumb.textViVn"/>
              <@spring.formHiddenInput "event.content.textViVn"/>
            </#if>
            <#-- END : 일문 -->
          </div>
        </div>
        <#-- END : TAB LANGUAGE BODY -->
      </div>
      <#-- END : CONTENT-->
      <#-- SIDBAR -->
      <div class="col-lg-5 pl0-lg">

        <#--        <div class="panel panel-default">-->
        <#--          <div class="panel-heading"><h4>Image Section</h4></div>-->
        <#--          <div class="panel-body">-->

        <#--            <div class="row">-->
        <#--              <div class="col-xs-4"><@ui.uploadImage "Thumbnail" "thumbnail" "" "JPEG 660 X 380"/></div>-->
        <#--            </div>-->

        <#--          </div>-->
        <#--        </div>-->

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Exhibition period Settings</h4></div>
          <div class="panel-body">

            <@ui.formDate "event.startDate" "시작일" true "YYYY-MM-DD"/>
            <@ui.formDate "event.endDate" "종료일" true "YYYY-MM-DD"/>

          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Setting</h4></div>
          <div class="panel-body">

            <#-- Active Mode -->
            <@ui.formActive "event.active"/>

          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Search Engine Optimization</h4></div>
          <div class="panel-body">

            <@ui.uploadImage "Open Graph" "seo.ogImage" "" "JPEG/PNG 600 x 315"/>
            <hr/>
            <#-- SEO Description -->
            <div class="form-group">
              <label class="control-label">SEO Description </label>

              <@spring.formInput "event.seo.description",
              "placeholder=\"Enter an SEO description.\"
                            class=\"form-control\"
                            maxlength=\"255\""/>
              <span class="help-block m-b-none"> Please fill in within 45 characters. </span>
            </div>

            <#-- SEO Keywords -->
            <div class="form-group">
              <label class="control-label">SEO Keywords </label>

              <@spring.formInput "event.seo.keywords",
              "placeholder=\"SEO Enter keywords\"
                            class=\"form-control\"
                            data-role=\"tagsinput\"
                            maxlength=\"255\""/>
              <#--<span class="help-block m-b-none"> </span>-->
            </div>

          </div>
        </div>

        <#--<div class="panel panel-default">-->
        <#--<div class="panel-heading">File</div>-->
        <#--<div class="panel-body">-->

        <#--&lt;#&ndash; File Upload &ndash;&gt;-->
        <#--<@ui.uploadFiles "Upload (복수)" "file" "files" "/admin/api/upload/file" ""/>-->

        <#--</div>-->
        <#--</div>-->

      </div>
      <#-- END : SIDBAR -->
    </div>
  </form>
</div>