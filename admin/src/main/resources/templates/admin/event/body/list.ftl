<#--<#assign pageparam = "&key=value"/>-->
<#assign pageparam = ""/>
<div class="container-fluid">
  <#-- 필터 -->
  <div class="row">
    <div class="col-md-12">
      <div id="" class="panel panel-default panel-list-filter">
        <div class="panel-heading">검색 필터

          <a href="#" data-tool="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
            <em class="fa fa-minus"></em>
          </a>

          <a id="initial-list-filter" href="javascript:void(0);" data-tool="panel-refresh" data-toggle="tooltip" title="" class="pull-right">
            <em class="fa fa-refresh"></em>
          </a>

        </div>
        <div class="panel-wrapper collapse in">
          <div class="panel-body">
            <form id="form-list-filter" action="" method="get">
              <#include "/admin/common/list/fieldset-common.ftl"/>
              <#include "/admin/common/list/fieldset-date.ftl"/>
              <#include "/admin/common/list/fieldset-search.ftl"/>
            </form>
          </div>
          <#-- 필터 실행 -->
          <div class="panel-footer">

            <button id="submit-list-filter" type="button" class="btn btn-primary">검색</button>

            <div class="btn-group">
              <button type="button" data-toggle="dropdown" class="btn dropdown-toggle btn-default">${data.pageSize}개씩 보기 <span class="caret"></span></button>
              <ul role="menu" class="dropdown-menu">
                <li><a href="<@spring.url "?query=${data.query!}&size=10&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 10</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=20&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 20</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=50&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 50</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=100&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 100</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=500&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 500</a></li>
              </ul>
            </div>


            <#--<button id="excel-list-filter" type="button" data-action="<@spring.url "/admin/board/blogCategory/excel"/>" class="btn btn-labeled btn-default">-->
            <#--<span class="text">엑셀 Download</span>-->
            <#--<span class="btn-label btn-label-right"><i class="fa fa-file-excel-o"></i></span>-->
            <#--</button>-->
          </div>
          <#-- END : 필터 실행 -->
        </div>
      </div>
    </div>
  </div>
  <#-- END : 필터 -->
  <#-- HEADER -->
  <#assign createparam = header.url +"/create"/>
  <#include "/admin/common/list/header.ftl"/>
  <#-- END : HEADER -->
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

        <#-- START table-responsive-->
        <div class="table-responsive">
          <table id="table-ext-1" class="table table-bordered table-hover">
            <colgroup>
              <col width="5%">

              <col width="10%">
              <col width="20%">
              <col width="15%">
              <col width="10%">
              <col width="10%">

              <col width="5%">
              <col width="5%">
              <col width="5%">
            </colgroup>
            <thead>
            <tr>
              <th class="text-center">#</th>

              <th class="text-center">Thumbnail</th>
              <th class="text-center">Title</th>
              <th class="text-center">Page Link</th>
              <th class="text-center">Period</th>

              <th class="text-center">Status</th>

              <th class="text-center">Active Status</th>
              <th class="text-center">Reg. Date</th>
              <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>

            <#if data?has_content>
              <#list data.page.content as item>
                <tr>
                  <td class="text-center">no.${data.firstNo- (item_index + 1)}<br/>id.${item.id?c}</td>

                  <td class="text-center">
                    <#if item.thumb?has_content && item.thumb.value?has_content>
                      <img src="${item.thumb.value}" style="max-height: 60px;"/>
                    <#else>-</#if>
                  </td>
                  <td class="text-left">
                    <#if international>
                      <ul>
                        <#if im.koKr>
                        <li <#if !item.internationalMode.koKr>class="inactive"</#if>>Ko : <#if item.internationalMode.koKr>${item.title.textKoKr!}<#else>Inactive</#if></li></#if>
                        <#if im.enUs>
                        <li <#if !item.internationalMode.enUs>class="inactive"</#if>>En : <#if item.internationalMode.enUs>${item.title.textEnUs!}<#else>Inactive</#if></li></#if>
                        <#if im.zhCn>
                        <li <#if !item.internationalMode.zhCn>class="inactive"</#if>>Cn S. : <#if item.internationalMode.zhCn>${item.title.textZhCn!}<#else>Inactive</#if></li></#if>
                        <#if im.zhTw>
                        <li <#if !item.internationalMode.zhTw>class="inactive"</#if>>Cn T. : <#if item.internationalMode.zhTw>${item.title.textZhTw!}<#else>Inactive</#if></li></#if>
                        <#if im.jaJp>
                        <li <#if !item.internationalMode.jaJp>class="inactive"</#if>>Ja : <#if item.internationalMode.jaJp>${item.title.textJaJp!}<#else>Inactive</#if></li></#if>
                      </ul>
                    <#else>
                      ${item.title.value!}
                    </#if>
                  </td>
                  <td class="text-center">
                      <#assign _host = clientHost/>
                      <#if !isProduction>
                          <#assign _host = 'https://boilerplate.com'/>
                      </#if>
                    <a href="${_host}/event/${item.id?c}" target="_blank">
                        ${_host}/event/${item.id?c}
                    </a>
                  </td>
                  <td class="text-center">
                    <#if item.startDate?has_content>${item.startDate.format('yyyy.MM.dd')}<#else>-</#if> ~<br/>
                    <#if item.endDate?has_content>${item.endDate.format('yyyy.MM.dd')}<#else>-</#if>
                  </td>
                  <td class="text-center">
                    <#if item.withinRange>
                      <div class="label label-success">In progress</div><#else>
                      <div class="label label-warning">End</div></#if>
                  </td>


                  <td class="text-center"><#if item.active>
                      <div class="label label-success">Active</div><#else>
                      <div class="label label-warning">Inactive</div></#if></td>
                  <td class="text-center">${item.createdDate.format('yyyy.MM.dd HH:mm:ss')}</td>
                  <td class="text-center">
                    <a href="<@spring.url header.url + "/update/${item.id?c}"/>" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="View and Edit">
                      <em class="fa fa-pencil"></em>
                    </a>
                  </td>
                </tr>
              </#list>
            </#if>
            </tbody>
          </table>
        </div>
        <#-- END table-responsive-->

        <#-- TABLE FOOTER -->
        <div class="panel-footer">
          <div class="row">
            <div class="col-lg-12 text-center">
              <#include "/admin/common/list/pagination.ftl"/>
            </div>
          </div>
        </div>
        <#-- END : TABLE FOOTER -->
      </div>
    </div>
  </div>
</div>
