<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div class="container-fluid es-settlement-update" xmlns="http://www.w3.org/1999/html">
  <#-- FORM -->
  <form id="form-update-es-settlement" action="<@spring.url header.url + "/update"/>" method="post"
        data-parsley-validate=""
        novalidate="" data-parsley-international="true">
    <#-- Modify Complete -->
    <#include "../../common/modify-success.ftl"/>
    <#-- END : Modify Complete -->
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            Modify
          </button>
          <@spring.formHiddenInput "settlement.id"/>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top"
             title="List">
            <span class="icon-list"></span>
          </a>
          <button data-type="btn-delete" type="button" class="btn btn-danger btn-lg" data-toggle="tooltip"
                  data-placement="top" title="Delete"
                  data-action="<@spring.url header.url + "/delete"/>"
                  data-id="${settlement.id?c}">
            <span class="icon-trash"></span>
          </button>
        </div>

      </div>
    </div>
    <div class="row">
      <#-- CONTENT-->
      <div class="col-lg-7">

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Implementer Name</h4></div>
          <div class="panel-body">

            <@ui.formInputText "settlement.nameImplementer" "Implementer Name" false 255 "" "text" ""/>

          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Revenue</h4></div>
          <div class="panel-body">
            <@ui.formInputText "settlement.revenue" "Revenue" false 255 "" "number" ""/>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading"><h4>매장</h4></div>
          <div class="panel-body">
            <@spring.bind "settlement.idStore"/>
            <div data-type="group-store">
              <@spring.bind "settlement.idStore"/>
              <label class="control-label">매장 <span class="text-require">*</span></label>
              <select name="${spring.status.expression}" data-type="chosen-select"
                      data-placeholder="Please search for and select a store." class="form-control">
                <#list stores as item>
                  <option value="${item.id?c}"
                          <#if item.id == settlement.idStore>selected</#if>>${item.name.value}</option>
                </#list>
              </select>
            </div>
            <span class="help-block mb0 mt-lg">※ You can register store in Content Management > Store Management.</span>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading"><h4>User</h4></div>
          <div class="panel-body">
            <@spring.bind "settlement.idUser"/>
            <div data-type="group-user">
              <@spring.bind "settlement.idUser"/>
              <label class="control-label">User <span class="text-require">*</span></label>
              <select name="${spring.status.expression}" data-type="chosen-select"
                      data-placeholder="Please search for and select a user." class="form-control">
                <#list users as item>
                  <option value="${item.id?c}"
                          <#if item.id == settlement.idUser>selected</#if>>${item.fullName}</option>
                </#list>
              </select>
            </div>
            <span class="help-block mb0 mt-lg">※ You can register user in Content Management > User Management.</span>
          </div>
        </div>

      </div>
      <#-- END : SIDBAR -->
    </div>
  </form>
</div>
