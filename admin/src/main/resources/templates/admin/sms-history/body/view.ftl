<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div id="view-sms-history" class="container-fluid sms-history-view">
<#-- FORM -->

    <div class="row">
        <div class="col-md-12">

            <div class="pull-right mb-lg">
                <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top" title="List">
                    <span class="icon-list"></span>
                </a>
            </div>

        </div>
    </div>
    <div class="row">
    <#-- CONTENT-->
        <div class="col-lg-7">
            <div class="panel panel-default">
                <div class="panel-heading">Status</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label">cmid </label>
                        <p class="form-control-static">${smsHistory.cmid!}</p>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label class="control-label">코드 </label>
                        <p class="form-control-static">${smsHistory.code.code!} [${smsHistory.code.message!}]</p>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">

                <div class="panel-body">

                    <div class="form-group">
                        <label class="control-label">Type </label>
                        <p class="form-control-static">${smsHistory.type!}</p>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label class="control-label">보낸PhoneNumber </label>
                        <p class="form-control-static">${smsHistory.destPhone!}</p>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label class="control-label">Recipient </label>
                        <p class="form-control-static">${smsHistory.destName!}</p>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label class="control-label">Title </label>
                        <p class="form-control-static">${smsHistory.subject!}</p>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label class="control-label">메세지 </label>
                        <p class="form-control-static">${smsHistory.msgBody!}</p>
                    </div>
                <#if smsHistory.sendTime?has_content>
                    <hr/>
                    <div class="form-group">
                        <label class="control-label">예약시간 </label>
                        <p class="form-control-static">${smsHistory.sendTime!}</p>
                    </div>
                </#if>
                </div>
            </div>

        </div>
    <#-- END : CONTENT-->
    <#-- SIDBAR -->
        <div class="col-lg-5 pl0-lg">

        <#if cmidReport?has_content>
            <div class="panel panel-default">
                <div class="panel-heading">발송 결과Information</div>
                <div class="panel-body">

                    <div class="form-group">
                        <label class="control-label">결과 메세지</label>
                        <p class="form-control-static">${cmidReport.status_msg!}</p>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label class="control-label">결과 코드</label>
                        <p class="form-control-static">${cmidReport.call_status?c}</p>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label class="control-label">발송한 번호</label>
                        <p class="form-control-static">${cmidReport.dest_phone!}</p>
                    </div>

                </div>
            </div>
        </#if>

        <#-- Meta Information (Modify 페이지 필수) -->
        <@ui.panelMetaInfo smsHistory.updatedDate smsHistory.createdDate/>
        <#-- END : Meta Information (Modify 페이지 필수) -->
        </div>
    <#-- END : SIDBAR -->
    </div>

</div>