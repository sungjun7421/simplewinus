<script id="template-item-autocomplete-ajax-user" type="x-tmpl-mustache">
<tr data-type="item-user">
  <td class="text-left">{{name}}</td>
  <td class="text-left">{{email}}</td>
  <td class="text-left">{{mobile}}</td>
  <td class="text-center">
    <button class="btn btn-default" data-type="btn-item-user-delete" type="button" title="Delete"><i class="fa fa-minus"></i></button>
    <input type="hidden" name="users" data-user-input="" value="{{id}}"/>
  </td>
</tr>
</script>