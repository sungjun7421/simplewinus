<#--<#assign pageparam = "&key=value"/>-->
<#assign pageparam = ""/>
<div class="container-fluid">

  <#-- 필터 -->
  <div class="row">
    <div class="col-md-12">
      <div id="" class="panel panel-default panel-list-filter">
        <div class="panel-heading">검색 필터

          <a href="#" data-tool="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
            <em class="fa fa-minus"></em>
          </a>

          <a id="initial-list-filter" href="javascript:void(0);" data-tool="panel-refresh" data-toggle="tooltip"
             title="" class="pull-right">
            <em class="fa fa-refresh"></em>
          </a>

        </div>
        <div class="panel-wrapper collapse in">
          <div class="panel-body">
            <form id="form-list-filter" action="" method="get">
<#--              <#include "/admin/common/list/fieldset-common.ftl"/>-->
              <fieldset>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">Date</label>

                      <div class="col-xs-5 ">
                        <div class="date list-filter-date date-wave" data-type="startDate" data-format="YYYY-MM-DD">
                          <input name="startDate" type="text" class="form-control" placeholder="Start" value="${startDate!}" autocomplete="off">
                        </div>
                      </div>
                      <div class="col-xs-5">
                        <div class="date list-filter-date" data-type="endDate" data-format="YYYY-MM-DD">
                          <input name="endDate" type="text" class="form-control" placeholder="End" value="${endDate!}" autocomplete="off">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
          <#-- 필터 실행 -->
          <div class="panel-footer">

            <button id="submit-list-filter" type="button" class="btn btn-primary">검색</button>

<#--            <div class="btn-group">-->
<#--              <button type="button" data-toggle="dropdown" class="btn dropdown-toggle btn-default">${data.pageSize}개씩 보기 <span class="caret"></span></button>-->
<#--              <ul role="menu" class="dropdown-menu">-->
<#--                <li><a href="<@spring.url "?query=${data.query!}&size=10&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 10</a></li>-->
<#--                <li><a href="<@spring.url "?query=${data.query!}&size=20&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 20</a></li>-->
<#--                <li><a href="<@spring.url "?query=${data.query!}&size=50&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 50</a></li>-->
<#--                <li><a href="<@spring.url "?query=${data.query!}&size=100&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 100</a></li>-->
<#--                <li><a href="<@spring.url "?query=${data.query!}&size=300&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 300</a></li>-->
<#--                <li><a href="<@spring.url "?query=${data.query!}&size=500&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 500</a></li>-->
<#--              </ul>-->
<#--            </div>-->


            <#--<button id="excel-list-filter" type="button" data-action="<@spring.url "/admin/board/blogCategory/excel"/>" class="btn btn-labeled btn-default">-->
            <#--<span class="text">엑셀 Download</span>-->
            <#--<span class="btn-label btn-label-right"><i class="fa fa-file-excel-o"></i></span>-->
            <#--</button>-->
            <#--          <button type="button" class="btn btn-labeled btn-default" onclick="alert('기다려! 아직 안만들었어. 만들어줄께.');">-->
            <#--          <span class="text">엑셀 Download</span>-->
            <#--          <span class="btn-label btn-label-right"><i class="fa fa-file-excel-o"></i></span>-->
            <#--          </button>-->
          </div>
          <#-- END : 필터 실행 -->
        </div>
      </div>
    </div>
  </div>
  <#-- END : 필터 -->

  <#-- HEADER -->
  <#--<#assign createparam = header.url +"/create"/>-->
  <div class="row">
    <div class="col-md-12">
      <header class="clearfix pb-lg mb-sm">
          <#-- 목록 Status -->
        <div class="pull-left">
          <div class="list-total">Total ${list?size!}</div>
        </div>
          <#-- END : 목록 Status -->
          <#if createparam?has_content>
          <#-- 등록 버튼 -->
            <div class="pull-right">
              <a href="<@spring.url createparam/>" class="btn btn-primary btn-lg">Create</a>
            </div>
          <#-- 등록 버튼 -->
          </#if>
      </header>
    </div>
  </div>
  <#-- END : HEADER -->
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

        <#-- START table-responsive-->
        <div class="table-responsive">
          <table class="table table-bordered table-hover">

            <colgroup>
              <col width="25%">
              <col width="50%">
              <col width="25%">
            </colgroup>

            <thead>
            <tr>
              <th class="text-center">Rank</th>
              <th class="text-center">Keyword</th>
              <th class="text-center">Accumulated Cnt</th>
            </tr>
            </thead>
            <tbody>
            <#if list?has_content>
                <#list list as item>
                  <tr>
                    <td class="text-center">${item?counter}</td>
                    <td class="text-center">${item.word!}</td>
                    <td class="text-center">${item.accumulatedCnt!}</td>
                  </tr>
                </#list>
            </#if>
            </tbody>
          </table>
        </div>
        <#-- END table-responsive-->

      </div>
    </div>
  </div>
</div>