<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div class="container-fluid es-kiosk-update" xmlns="http://www.w3.org/1999/html">
  <#-- FORM -->
  <form id="form-update-es-kiosk" action="<@spring.url header.url + "/update"/>" method="post"
        data-parsley-validate=""
        novalidate="" data-parsley-international="true">
    <#-- Modify Complete -->
    <#include "../../common/modify-success.ftl"/>
    <#-- END : Modify Complete -->
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            Modify
          </button>
          <@spring.formHiddenInput "kiosk.id"/>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top"
             title="List">
            <span class="icon-list"></span>
          </a>
          <button data-type="btn-delete" type="button" class="btn btn-danger btn-lg" data-toggle="tooltip"
                  data-placement="top" title="Delete"
                  data-action="<@spring.url header.url + "/delete"/>"
                  data-id="${kiosk.id?c}">
            <span class="icon-trash"></span>
          </button>
        </div>

      </div>
    </div>
    <div class="row">
      <#-- CONTENT-->
      <div class="col-lg-7">


        <#-- TAB Panel or Panel Wrapper -->
        <div <#if international>role="tabpanel" class="panel panel-transparent" data-type="tabpanel-language"
             <#else>class="panel panel-default"</#if>>

          <@spring.bind "kiosk.name"/>
          <#assign tabPath = spring.status.expression/>
          <#-- TAB LANGUAGE HEADER -->
          <@ui.tabListByLanguage tabPath/>
          <#-- END : TAB LANGUAGE HEADER -->

          <#-- TAB LANGUAGE BODY -->
          <div <#if international>class="tab-content bg-white" <#else>class="panel-body"</#if>>

            <#-- 국문 -->
            <#if im.koKr>
              <div id="${tabPath}-ko" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "kiosk.name.textKoKr" "Name" true 255 "Enter the kiosk name."/>
                <hr/>

                <@ui.wysiswygEdior "kiosk.description.textKoKr" "Description" false/>
                <hr/>

                <@ui.formActiveByLanguage "kiosk.internationalMode.koKr" />
              </div>
            <#else>
              <@spring.formHiddenInput "kiosk.name.textKoKr"/>
              <@spring.formHiddenInput "kiosk.description.textKoKr"/>
            </#if>
            <#-- END : 국문 -->
            <#-- 영문 -->
            <#if im.enUs>
              <div id="${tabPath}-en" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "kiosk.name.textEnUs" "Name" true 255 "Enter the kiosk name."/>

                <hr/>

                <@ui.wysiswygEdior "kiosk.description.textEnUs" "Description" false/>

                <hr/>

                <@ui.formActiveByLanguage "kiosk.internationalMode.enUs"/>
              </div>
            <#else>
              <@spring.formHiddenInput "kiosk.name.textEnUs"/>
              <@spring.formHiddenInput "kiosk.description.textEnUs"/>
            </#if>
            <#-- END : 영문 -->
            <#-- 중문 (간체) -->
            <#if im.zhCn>
              <div id="${tabPath}-zh-cn" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "kiosk.name.textZhCn" "Name" true 255 "Enter the kiosk name."/>

                <hr/>
                <@ui.wysiswygEdior "kiosk.description.textZhCn" "Description" false/>
                <hr/>
                <@ui.formActiveByLanguage "kiosk.internationalMode.zhCn"/>
              </div>
            <#else>
              <@spring.formHiddenInput "kiosk.name.textZhCn"/>
              <@spring.formHiddenInput "kiosk.description.textZhCn"/>
            </#if>
            <#-- END : 중문 (간체) -->
            <#-- 중문 (번체) -->
            <#if im.zhTw>
              <div id="${tabPath}-zh-tw" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "kiosk.name.textZhTw" "Name" true 255 "Enter the kiosk name."/>

                <hr/>
                <@ui.wysiswygEdior "kiosk.description.textZhTw" "Description" false/>
                <hr/>
                <@ui.formActiveByLanguage "kiosk.internationalMode.zhTw"/>
              </div>
            <#else>
              <@spring.formHiddenInput "kiosk.name.textZhTw"/>
              <@spring.formHiddenInput "kiosk.description.textZhTw"/>
            </#if>
            <#-- END : 중문 (번체) -->
            <#-- 일문 -->
            <#if im.jaJp>
              <div id="${tabPath}-ja" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "kiosk.name.textJaJp" "Name" true 255 "Enter the kiosk name."/>

                <hr/>
                <@ui.wysiswygEdior "kiosk.description.textJaJp" "Description" false/>

                <hr/>
                <@ui.formActiveByLanguage "kiosk.internationalMode.jaJp"/>
              </div>
            <#else>
              <@spring.formHiddenInput "kiosk.name.textJaJp"/>
              <@spring.formHiddenInput "kiosk.description.textJaJp"/>
            </#if>
            <#-- END : 일문 -->
          </div>
        </div>
        <#-- END : TAB LANGUAGE BODY -->
      </div>
      <#-- END : CONTENT-->
      <#-- SIDBAR -->
      <div class="col-lg-5 pl0-lg">

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Store</h4></div>
          <div class="panel-body">
            <div class="form-group">
              <label class="control-label">Store Name Search (Automatically search when entered)</label>
              <input type="text" placeholder="Store Name"
                     class="form-control"
                     data-type="autocomplete-ajax"
                     data-url="/admin/api/store"
                     data-param-name="query"
                     data-fn-name="callbackByStore">
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="control-label">Store Name</label>
                  <input type="text" placeholder="Store Name"
                         class="form-control"
                         id="store-name"
                         readonly
                         value="<#if kiosk.relativeStore?has_content>${kiosk.relativeStore.name.value!}</#if>">
                </div>
              </div>
              <div class="col-sm-6">
                <@ui.formInputText "kiosk.relativeStore" "Store ID" true 255 "Enter Store Id" "number" "data-parsley-type=\"integer\""/>
              </div>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Serial Number</h4></div>
          <div class="panel-body">

            <@ui.formInputText "kiosk.serialNumber" "Serial Number" false 255 "" "text" ""/>

          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Status</h4></div>
          <div class="panel-body">
            <@ui.formBoolean "kiosk.active" "Status" "Active" "Inactive" ""/>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Join Date</h4></div>
          <div class="panel-body">
            <@ui.formDate "kiosk.joinDate" "Join Date" true "YYYY-MM-DD HH:mm:ss"/>
          </div>
        </div>

      </div>
      <#-- END : SIDBAR -->
    </div>
  </form>
</div>
