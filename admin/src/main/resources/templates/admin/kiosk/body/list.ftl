<#--<#assign pageparam = "&key=value"/>-->
<#assign pageparam = ""/>
<#if idStore?has_content>
    <#assign pageparam = pageparam + "&idStore=${idStore?c}"/>
</#if>
<div id="list-kiosk" class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div id="" class="panel panel-default panel-list-filter">
        <div class="panel-heading">검색 필터

          <a href="#" data-tool="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
            <em class="fa fa-minus"></em>
          </a>

          <a id="initial-list-filter" href="javascript:void(0);" data-tool="panel-refresh" data-toggle="tooltip"
             title="" class="pull-right">
            <em class="fa fa-refresh"></em>
          </a>

        </div>
        <div class="panel-wrapper collapse in">
          <div class="panel-body">
            <form id="form-list-filter" action="" method="get">
                <#include "../../common/list/fieldset-common.ftl"/>
              <fieldset>
                <div class="row">
                  <div class="col-lg-8">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">Reg. Date</label>

                      <div class="col-xs-5 ">
                        <div class="date list-filter-date date-wave" data-type="startDate">
                          <input name="startDate" type="text" class="form-control" placeholder="Start"
                                 value="${startDate!}" autocomplete="off">
                        </div>
                      </div>
                      <div class="col-xs-5">
                        <div class="date list-filter-date" data-type="endDate">
                          <input name="endDate" type="text" class="form-control" placeholder="End" value="${endDate!}"
                                 autocomplete="off">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <div class="row">
                  <div class="col-lg-8">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">검색어</label>

                      <div class="col-xs-10">
                        <input name="query" type="text" class="form-control" placeholder="검색어를 입력하세요."
                               value="${data.query!}">
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <div class="row">
                  <div class="col-lg-8">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">Store</label>

                      <div class="col-xs-5">
                        <select name="idStore" data-type="chosen-select" data-placeholder="Please search for and select a store."
                                class="form-control">
                          <option value=""></option>
                          <option value="">All</option>
                            <#list stores as item>
                              <option value="${item.id?c}"
                                      <#if idStore?has_content && idStore == item.id>selected</#if>>${item.name.value}</option>
                            </#list>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
          <div class="panel-footer">

            <button id="submit-list-filter" type="button" class="btn btn-primary">검색</button>

            <div class="btn-group">
              <button type="button" data-toggle="dropdown" class="btn dropdown-toggle btn-default">${data.pageSize}개씩 보기 <span class="caret"></span></button>
              <ul role="menu" class="dropdown-menu">
                <li><a href="<@spring.url "?query=${data.query!}&size=10&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 10</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=20&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 20</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=50&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 50</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=100&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 100</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=300&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 300</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=500&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 500</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <#assign createparam = header.url +"/create"/>
  <div class="row">
    <div class="col-md-12">
      <header class="clearfix pb-lg mb-sm">
        <div class="pull-left">
          <div class="list-total">total ${data.page.totalElements!},&nbsp;&nbsp;${data.currentIndex}
            /${data.page.totalPages} page
          </div>
        </div>
          <#if createparam?has_content>
            <div class="pull-right">
              <a href="<@spring.url createparam/>" class="btn btn-primary btn-lg">Create</a>
            </div>
          </#if>
      </header>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

          <#-- START table-responsive-->
        <div class="table-responsive">
          <table id="table-ext-1" class="table table-bordered table-hover">
            <colgroup>
              <col width="5%">
              <col width="30%">
              <col width="15%">
              <col width="20%">
              <col width="10%">
              <col width="15%">
              <col width="5%">
            </colgroup>
            <thead>
            <tr>
              <th class="text-center">#</th>
              <th class="text-center">Kiosk Name</th>
              <th class="text-center">Serial Number</th>
              <th class="text-center">Store Name</th>
              <th class="text-center">Active</th>
              <th class="text-center">Join Date</th>
              <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>

            <#if data?has_content>
                <#list data.page.content as item>
                  <tr>
                    <td class="text-center">no.${data.firstNo- (item_index + 1)}<br/>id.${item.id?c}</td>

                    <td class="text-left">
                        <#if international>
                          <ul style="padding-left: 13px; margin-bottom: 0px">
                              <#if im.koKr>
                              <li <#if !item.internationalMode.koKr>class="inactive"</#if>>Ko : <#if item.internationalMode.koKr>${item.name.textKoKr!}<#else>Inactive</#if></li></#if>
                              <#if im.enUs>
                              <li <#if !item.internationalMode.enUs>class="inactive"</#if>>En : <#if item.internationalMode.enUs>${item.name.textEnUs!}<#else>Inactive</#if></li></#if>
                              <#if im.zhCn>
                              <li <#if !item.internationalMode.zhCn>class="inactive"</#if>>Cn S. : <#if item.internationalMode.zhCn>${item.name.textZhCn!}<#else>Inactive</#if></li></#if>
                              <#if im.zhTw>
                              <li <#if !item.internationalMode.zhTw>class="inactive"</#if>>Cn T. : <#if item.internationalMode.zhTw>${item.name.textZhTw!}<#else>Inactive</#if></li></#if>
                              <#if im.jaJp>
                              <li <#if !item.internationalMode.jaJp>class="inactive"</#if>>Ja : <#if item.internationalMode.jaJp>${item.name.textJaJp!}<#else>Inactive</#if></li></#if>
                          </ul>
                        <#else>
                            ${item.name.value!}
                        </#if>
                    </td>

                    <td class="text-center">${item.serialNumber!}</td>

                    <td class="text-center">${item.relativeStore.name.value!}</td>

                    <td class="text-center"><#if item.active>
                        <div class="label label-success">Active</div><#else>
                        <div class="label label-warning">Inactive</div></#if>
                    </td>

                    <td class="text-center">${item.joinDate.format('yyyy.MM.dd HH:mm:ss')}</td>

                    <td class="text-center">
                      <a href="<@spring.url header.url + "/update/${item.id?c}"/><#if springMacroRequestContext.queryString?has_content>?${springMacroRequestContext.queryString}</#if>"
                         class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top"
                         title="View and edit">
                        <em class="fa fa-pencil"></em>
                      </a>
                    </td>
                  </tr>
                </#list>
            </#if>
            </tbody>
          </table>
        </div>
          <#-- END table-responsive-->

          <#-- TABLE FOOTER -->
        <div class="panel-footer">
          <div class="row">
            <div class="col-lg-12 text-center">
                <#include "../../common/list/pagination.ftl"/>
            </div>
          </div>
        </div>
          <#-- END : TABLE FOOTER -->
      </div>
    </div>
  </div>
</div>
