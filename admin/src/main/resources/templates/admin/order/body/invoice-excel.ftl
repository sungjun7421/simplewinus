<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div class="container-fuild">
  <form id="form-update" data-parsley-validate="" novalidate="" action="<@spring.url header.url + "/invoice/excel"/>" method="post" enctype="multipart/form-data">
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            Upload
          </button>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">

          <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top" title="List">
            <span class="icon-list"></span>
          </a>
        </div>

      </div>
    </div>
    <div class="row">

      <div class="col-lg-6">

        <div class="panel panel-default">
          <div class="panel-heading"><h4>EXCEL File Upload</h4></div>
          <div class="panel-body">

            <div class="form-group">
              <label class="control-label">Invoice File <span class="text-require">*</span></label>
              <input type="file" name="file" class="form-control" required/>
            </div>
          </div>
        </div>

      </div>
      <div class="col-lg-6">

        <div class="panel panel-default">
          <div class="panel-heading"><h4>EXCEL Sample File</h4></div>
          <div class="panel-body">

            <div class="row mb">
              <div class="col-lg-6">
                <a href="<@spring.url "/assets/files/MSCK_invoice_number_sample_190722.xlsx"/>">Sample Download</a>
              </div>
            </div>

          </div>
        </div>

      </div>

    </div>
  </form>
</div>