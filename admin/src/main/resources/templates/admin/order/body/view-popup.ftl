<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div id="view-order" class="container-fluid order-view">
  <div class="row">
    <div class="col-md-12">

      <div class="pull-left mb-lg">
        <a href="javascript:window.close()" class="btn btn-default btn-lg">Close</a>
      </div>

      <div class="pull-right mb-lg">
        <button type="button"
                onclick="javascript:window.print();"
                class="btn btn-labeled btn-default btn-lg">
          <span class="text">Print</span>
          <span class="btn-label btn-label-right"><i class="icon-printer"></i></span>
        </button>
      </div>

    </div>
  </div>

<#-- Start : 프린트 영역 -->
  <div class="row order-print">
  <#-- CONTENT-->

    <div class="col-xs-12">

<#--      <h3>Order details</h3>-->
<#--      <hr/>-->
    <#--Consumer Information (Member)-->
      <div class="panel panel-default">
        <div class="panel-heading"><h4>Consumer Information <span class="text-primary"><#if order.nonMember>(non-member)<#else>(Member)</#if></span></h4></div>
        <div class="panel-body">

          <div class="form-group">
            <label class="control-label">Fullname </label>
            <p class="form-control-static">
            <#if order.nonMember>
                        ${order.shippingInfo.fullname!}
                    <#else>
            ${order.relativeBuyer.relativeUser.fullname!}
            </#if>
            </p>
          </div>

          <div class="form-group">
            <label class="control-label">Phone Number </label>
            <p class="form-control-static">
            <#if order.nonMember>
                        ${order.nonMemberInfo.phoneNumber!}
                    <#else>
            ${order.relativeBuyer.relativeUser.phoneNumber!}
            </#if>
            </p>
          </div>

          <div class="form-group">
            <label class="control-label">이메일 </label>
            <p class="form-control-static">
            <#if order.nonMember>
                        ${order.nonMemberInfo.emailByNm!}
                    <#else>
            ${order.relativeBuyer.relativeUser.email}
            </#if>
            </p>
          </div>

        </div>
      </div>

    <#--Order Product History-->
      <div class="panel panel-default">
        <div class="panel-heading"><h4>Order Product History</h4></div>
        <div class="panel-body p0">

          <div class="table-responsive">
            <table id="table-ext-1" class="table table-bordered table-hover">
              <colgroup>
                <col width="10%">

                <col width="40%">
                <col width="10%">
                <col width="20%">

                <col width="15%">
                <col width="5%">
              </colgroup>
              <thead>
              <tr>
                <th class="text-center">Image</th>

                <th class="text-center">Name</th>
                <th class="text-center">Volume</th>
                <th class="text-center">barCode/prodCode</th>

                <th class="text-center">selling price</th>
                <th class="text-center">qty</th>
              </tr>
              </thead>
              <tbody>

              <#if order.orderProducts?has_content>
                  <#list order.orderProducts as op>
                  <tr>
                    <td class="text-center">
                        <#if op.relativeSKU?has_content && op.relativeSKU.relativeProduct?has_content>
                            <#assign product = op.relativeSKU.relativeProduct/>
                          <img src="${product.thumbnail!}" width="50px;" class="img-thumbnail img-responsive">
                        <#else>-</#if>
                    </td>

                    <td class="text-center">
                        <#if op.relativeSKU?has_content && op.relativeSKU.relativeProduct?has_content>
                            <#assign product = op.relativeSKU.relativeProduct/>
                          ${product.name.value}
                        <#else>
                        ${op.productName!}
                        </#if>
                    </td>
                    <td class="text-center">
                        <#if op.relativeSKU?has_content && op.relativeSKU.relativeProduct?has_content>
                            <#assign product = op.relativeSKU.relativeProduct/>
                            <#if product.volumeStr?has_content>${product.volumeStr}<#else>-</#if>
                        <#else>[-]</#if>
                    </td>
                    <td class="text-center">
                        <#if op.relativeSKU?has_content && op.relativeSKU.relativeProduct?has_content>
                            <#assign product = op.relativeSKU.relativeProduct/>
                          <#if op.relativeSKU.barCode?has_content>${op.relativeSKU.barCode}<#else>-</#if>/<#if op.relativeSKU.productCode?has_content>${op.relativeSKU.productCode}<#else>-</#if>
                        <#else>
                            <#if op.barCode?has_content>${op.barCode}<#else>-</#if>/-
                        </#if>
                    </td>

                    <td class="text-right">
<#--                    ${op.price!}-->
<#--                        <#if op.dc?has_content && op.dc gt 0><br/>-->
<#--                          <small>(D.C)&nbsp;&nbsp;&nbsp;</small>-->
<#--                          <b>-</b> ${op.dc!}</#if>-->
<#--                        <#if op.dcByMembership?has_content && op.dcByMembership gt 0><br/>-->
<#--                          <small>(member D.C)&nbsp;&nbsp;&nbsp;</small>-->
<#--                          <b>-</b> ${op.dcByMembership!}</#if>-->
<#--                        <#if op.addPrice?has_content && op.addPrice gt 0><br/>-->
<#--                          <small>(add price)&nbsp;&nbsp;&nbsp;</small>-->
<#--                          <b>+</b> ${op.addPrice!}</#if>-->
<#--                      <br/>-->
<#--                      <small>(qty)&nbsp;&nbsp;&nbsp;</small>-->
<#--                      <b>x</b> ${op.qty!}-->
<#--                        <#if op.dcByCoupon?has_content && op.dcByCoupon gt 0><br/>-->
<#--                          <small>(coupon D.C)&nbsp;&nbsp;&nbsp;</small>-->
<#--                          <b>-</b> ${op.dcByCoupon}</#if>-->
<#--                        <#if op.hwdc?has_content && op.hwdc gt 0><br/>-->
<#--                          <small>(direct D.C)&nbsp;&nbsp;&nbsp;</small>-->
<#--                          <b>-</b> ${op.hwdc}</#if>-->
<#--                      <hr style="margin: 2px 0"/>-->
                      <strong>${op.amount.amount!} ${op.amount.currency!}</strong>
                    </td>
                    <td class="text-center">${op.qty!}</td>
                  </tr>
                  </#list>
              </#if>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    <#-- Info -->
      <div class="panel panel-default">

        <div class="row">
        <#--Payment Amount Information-->
          <div class="col-xs-6">
            <div class="panel-heading"><h4>Payment Amount Information</h4></div>
            <div class="panel-body">

              <div class="form-group">
                <label class="control-label">Payment Amount</label>
                <p class="form-control-static">${order.amount.amount} ${order.amount.currency!}</p>
              </div>

            <#if order.fcShipping?has_content>
              <div class="form-group">
                <label class="control-label">Shipping Cost </label>
                <p class="form-control-static">${order.fcShipping} ${order.amount.currency!}</p>
              </div>
            </#if>

            <#if order.fcPoint?has_content>
              <div class="form-group">
                <label class="control-label">Used point </label>
                <p class="form-control-static">${order.fcPoint} Point</p>
              </div>
            </#if>

            <#if order.usedCoupons?has_content>
              <div class="form-group">
                <label class="control-label">Used Coupon </label>
                <p class="form-control-static">Discount Coupon Amount: ${order.fcCoupon!} ${order.amount.currency!}</p>

                <hr style="margin: 2px 0"/>
                <ul>
                    <#list order.usedCoupons as couponBox>
                        <#assign coupon = couponBox.relativeCoupon/>
                      <li>[${coupon.type.value}] ${coupon.message.value} </li>
                    </#list>
                </ul>
              </div>
            </#if>

            </div>
          </div>

        <#--Order Information-->
          <div class="col-xs-6">
            <div class="panel-heading"><h4>Order Information</h4></div>
            <div class="panel-body">

              <div class="form-group">
                <label class="control-label">Order Number</label>
                <p class="form-control-static"><strong>${order.oid!}</strong>
              </div>

              <div class="form-group">
                <label class="control-label">주문상태</label>
                <p class="form-control-static">${order.orderStatus.value!}</p>
              </div>

              <div class="form-group">
                <label class="control-label">PayMethod</label>
                <p class="form-control-static">${order.payMethod.value!}</p>
              </div>

              <div class="form-group">
                <label class="control-label">Payment Status</label>
                <div class="form-group" style="display: inline-block">
                <#if order.paymentComplete>
                  <div class="label label-success">Complete</div><#else>
                  <div class="label label-warning">No Payment</div></#if>
                </div>
              </div>

            <#if order.paymentDate?has_content>
              <div class="form-group">
                <label class="control-label">Payment Complete Date</label>
                <p class="form-control-static">${order.paymentDate.format('yyyy.MM.dd HH:mm:ss')}</p>
              </div>
            </#if>

            <#if order.deliveryCompleteDate?has_content>
              <div class="form-group">
                <label class="control-label">Shipping Complete Date</label>
                <p class="form-control-static">${order.deliveryCompleteDate.format('yyyy.MM.dd HH:mm:ss')}</p>
              </div>
            </#if>

            </div>
          </div>
        </div>
        <hr/>
      <#--Shipping Information-->
        <div>
          <div class="panel-heading"><h4>Shipping Information</h4></div>
          <div class="panel-body">

            <div class="row">
              <div class="col-xs-6">
                <div class="form-group">
                  <label class="control-label">Invoice Number</label>
                  <p class="form-control-static"><#if order.shippingInfo.shippingNumber?has_content>${order.shippingInfo.shippingNumber!}<#else>None.</#if></p>
                </div>

                <div class="form-group">
                  <label class="control-label">Recipient </label>
                  <p class="form-control-static">${order.shippingInfo.fullname}</p>
                </div>

                <div class="form-group">
                  <label class="control-label">Phone Number </label>
                  <p class="form-control-static">${order.shippingInfo.phoneNumber!}</p>
                </div>

              <#if order.shippingInfo.customsIdNumber?has_content>
                <div class="form-group">
                  <label class="control-label">customs IdNumber </label>
                  <p class="form-control-static">${order.shippingInfo.customsIdNumber!}</p>
                </div>
              </#if>

                <div class="form-group">
                  <label class="control-label">Shipping Country </label>
                  <p class="form-control-static">${order.shippingInfo.shippingCostMode.value}</p>
                </div>

              <#if order.shippingInfo.shippingCostMode == 'HONG_KONG'>
                <div class="form-group">
                  <label class="control-label">HongKong Shipping Type</label>
                  <p class="form-control-static">${order.shippingInfo.hkShippingType.value}</p>
                </div>
<#--                <div class="form-group">-->
<#--                  <label class="control-label">same-day delivery</label>-->
<#--                  <p class="form-control-static"><#if order.shippingInfo.theDay>-->
<#--                    <span class="label label-success">Application</span><#else>-->
<#--                    <span class="label label-warning">No application</span></#if></p>-->
<#--                </div>-->
                  <#if order.shippingInfo.hkShippingType == 'PICK_UP_CENTER'>
                    <#if order.shippingInfo.pickupDate?has_content && order.shippingInfo.pickupTime?has_content>
                      <div class="form-group">
                        <label class="control-label">Pickup Date</label>
                        <p class="form-control-static">${order.shippingInfo.pickupDate.format('yyyy-MM-dd')} ${order.shippingInfo.pickupTime.format('HH:mm')}</p>
                      </div>
                    </#if>
                  </#if>
              <#elseif order.shippingInfo.shippingCostMode == 'CHINA'>
                  <#if province?has_content>
                    <div class="form-group">
                      <label class="control-label">Chinese province name</label>
                      <p class="form-control-static">${province.name.value}</p>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Chinese zone Type</label>
                      <p class="form-control-static">${province.cnZone.value}</p>
                    </div>
                  </#if>
              <#elseif order.shippingInfo.shippingCostMode == 'OVERSEA'>
                <hr/>
                  <#if osCountry?has_content>
                    <div class="row">
                      <div class="form-group col-xs-6">
                        <label class="control-label">Country name overseas</label>
                        <p class="form-control-static">${osCountry.name.value}</p>
                      </div>
                      <div class="form-group col-xs-6">
                        <label class="control-label">Country zone overseas Type</label>
                        <p class="form-control-static">${osCountry.osZone.value}</p>
                      </div>
                    </div>
                  </#if>
              </#if>
              </div>
              <div class="col-xs-6">

              <#if order.shippingInfo.shippingCostMode == 'HONG_KONG' && order.shippingInfo.hkShippingType == 'PICK_UP_CENTER'>
                <div class="row">
                  <div class="form-group col-xs-6">
                    <label class="control-label">PickupPoint Information</label>
                      <#if pickupPoint?has_content>
                        <p class="form-control-static">Alias: ${pickupPoint.alias.value}</p>
                        <p class="form-control-static">Address: ${pickupPoint.address.value}</p>
                        <p class="form-control-static">PointName: ${pickupPoint.pointName.value}</p>
                          <#if pickupPoint.phone?has_content>
                            <p class="form-control-static">Phone Number: ${pickupPoint.phone!"-"}</p>
                          </#if>
                          <#if pickupPoint.openTime?has_content && pickupPoint.closeTime?has_content>
                            <p class="form-control-static">Operating time: ${pickupPoint.openTime.format('HH:mm')} ~ ${pickupPoint.closeTime.format('HH:mm')}</p>
                          </#if>
                      </#if>
                  </div>
                  <div class="form-group col-xs-6">
                    <label class="control-label">Map Image</label>
                      <#if pickupPoint?has_content && pickupPoint.image?has_content>
                        <p class="form-control-static"><img src="${pickupPoint.image!}" class="img-thumbnail img-responsive"></p>
                      </#if>
                  </div>
                </div>
              <#else>
                <div class="form-group">
                  <label class="control-label">Address </label>
                    <#if order.shippingInfo.postalCode?has_content>
                      <p class="form-control-static">[${order.shippingInfo.postalCode}]</p>
                    </#if>
                    <#if order.shippingInfo.address1?has_content>
                      <p class="form-control-static">${order.shippingInfo.address1} <#if order.shippingInfo.address2?has_content>${order.shippingInfo.address2}</#if></p>
                    </#if>
                </div>
              </#if>

              <#--            <#if order.shippingInfo.shippingCostMode == 'CHINA'>-->
              <#--              <hr/>-->
              <#--              <div class="row">-->
              <#--                <div class="form-group col-xs-6">-->
              <#--                  <label class="control-label">ID Card (1)</label>-->
              <#--                    <#if order.shippingInfo.idCard1?has_content>-->
              <#--                      <p class="form-control-static"><img src="${order.shippingInfo.idCard1!}" class="img-thumbnail img-responsive"/></p>-->
              <#--                    </#if>-->
              <#--                </div>-->
              <#--                <div class="form-group col-xs-6">-->
              <#--                  <label class="control-label">ID Card (2)</label>-->
              <#--                    <#if order.shippingInfo.idCard2?has_content>-->
              <#--                      <p class="form-control-static"><img src="${order.shippingInfo.idCard2!}" class="img-thumbnail img-responsive"/></p>-->
              <#--                    </#if>-->
              <#--                </div>-->
              <#--              </div>-->
              <#--            </#if>-->
              </div>
            </div>
          </div>
        </div>
      <#if order.pickupInfo?has_content>
        <hr/>
        <div>
          <div class="panel-heading"><h4>Pickup Information(Exchange/Refund)</h4></div>
          <div class="panel-body">

            <div class="row">
              <div class="col-xs-6">
                <div class="form-group">
                  <label class="control-label">Fullname </label>
                  <p class="form-control-static">${order.pickupInfo.fullnameByPU}</p>
                </div>

                <div class="form-group">
                  <label class="control-label">Phone Number </label>
                  <p class="form-control-static">${order.pickupInfo.countryCodeByPU!} ${order.pickupInfo.phoneByPU!}</p>
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group">
                  <label class="control-label">Shipping Country </label>
                  <p class="form-control-static">${order.pickupInfo.modeByPU.value}</p>
                </div>

                  <#if order.pickupInfo.modeByPU == 'HONG_KONG'>
                    <div class="form-group">
                      <label class="control-label">HongKong Shipping Type</label>
                      <p class="form-control-static">
                          <#if order.pickupInfo.typeByPU?has_content>
                              ${order.pickupInfo.typeByPU.value}
                          <#else>-</#if>
                      </p>
                    </div>
                  <#elseif order.pickupInfo.modeByPU == 'CHINA'>
                      <#if provinceByPU?has_content>
                        <div class="form-group">
                          <label class="control-label">Chinese province name</label>
                          <p class="form-control-static">${provinceByPU.name.value}</p>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Chinese zone Type</label>
                          <p class="form-control-static">${provinceByPU.cnZone.value}</p>
                        </div>
                      </#if>
                  <#elseif order.pickupInfo.modeByPU == 'OVERSEA'>
                      <#if osCountryByPU?has_content>
                        <div class="form-group">
                          <label class="control-label">Country name overseas</label>
                          <p class="form-control-static">${osCountryByPU.name.value}</p>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Country zone overseas Type</label>
                          <p class="form-control-static">${osCountryByPU.osZone.value}</p>
                        </div>
                      </#if>
                  </#if>

                <div class="form-group">
                  <label class="control-label">Address </label>
                    <#if order.pickupInfo.postalCodeByPU?has_content>
                      <p class="form-control-static">[${order.pickupInfo.postalCodeByPU}]</p>
                    </#if>
                    <#if order.pickupInfo.address1ByPU?has_content>
                      <p class="form-control-static">${order.pickupInfo.address1ByPU} <#if order.pickupInfo.address2ByPU?has_content>${order.pickupInfo.address2ByPU}</#if></p>
                    </#if>
                </div>
              </div>

            </div>

          </div>
        </div>
      </#if>
      </div>

    </div>
  <#-- END : CONTENT-->
  </div>
<#-- End : 프린트 영역 -->

</div>