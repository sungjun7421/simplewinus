<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div id="view-order" class="container-fluid order-view">
  <div class="row">
    <div class="col-md-12">

      <div class="pull-left mb-lg">
      </div>

      <div class="pull-right mb-lg">
<#--        <button id="btn-order-print" type="button"-->
<#--                data-id="${order.id?c}"-->
<#--                class="btn btn-labeled btn-default btn-lg">-->
<#--          <span class="text">Print</span>-->
<#--          <span class="btn-label btn-label-right"><i class="icon-printer"></i></span>-->
<#--        </button>-->
        <a href="<@spring.url header.url/><#if springMacroRequestContext.queryString?has_content>?${springMacroRequestContext.queryString}</#if>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top" title="List">
          <span class="icon-list"></span>
        </a>
      </div>

    </div>
  </div>
  <div class="row">
      <#-- CONTENT-->
    <div class="col-lg-7">
      <div class="panel panel-default">
        <div class="panel-heading"><h4>Machine Info</h4></div>
        <div class="panel-body p0">

          <div class="table-responsive">
            <table id="table-ext-1" class="table table-bordered table-hover">
              <colgroup>
                <col width="20%">
                <col width="25%">
                <col width="25%">
                <col width="15%">
                <col width="15%">

              </colgroup>
              <thead>
              <tr>
                <th class="text-center">Image</th>
                <th class="text-center">Info Machine</th>
                <th class="text-center">price</th>
                <th class="text-center">키오스크</th>
                <th class="text-center">Time</th>

              </tr>
              </thead>
              <tbody>
                <tr>
                    <#assign orderMachine = order.relativeOrderMachine/>
                    <#assign machine = order.relativeOrderMachine.relativeMachine/>
                    <td class="text-center">
                        <img src="${machine.thumbnail!}" class="img-thumbnail img-responsive">
                    </td>
                    <td class="text-center">
                      <a href="/admin/machine/update/${machine.id!}" target="_blank" data-toggle="tooltip" data-placement="top" title="MachineInformation">
                        ${orderMachine.machineName}</a><br>
                      ${orderMachine.weight}kg<br>
                      ${orderMachine.type.value}
                    </td>
                    <td class="text-left">
                      Base price : ${orderMachine.basePrice}KRW<br>
                      <#if orderMachine.washingMode == "STRONG_WASHING">
                        ${orderMachine.washingMode.value} : +${orderMachine.priceStrong}KRW<br>
                      </#if>
                      <#if orderMachine.dryingTimes != "NO_TIMES">
                        ${orderMachine.dryingTimes.value} : +${orderMachine.priceDry}KRW<br>
                      </#if>
                      <hr style="margin: 2px 0">
                      <strong>Total Price : ${orderMachine.price}KRW</strong>
                    </td>
                    <td class="text-center">
                      <#if orderMachine.kioskId?has_content>
                        <a href="/admin/kiosk/update/${orderMachine.kioskId?c}" target="_blank" data-toggle="tooltip" data-placement="top" title="MachineInformation">
                          ${orderMachine.kioskName}</a><br>
                      </#if>
                    </td>
                    <td class="text-center">
                      ${orderMachine.startTime.format('yyyy.MM.dd HH:mm:ss')}<br>
                      ${orderMachine.endTime.format('yyyy.MM.dd HH:mm:ss')}<br>
                    </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading"><h4>Address Info</h4></div>
        <div class="panel-body">

            <div class="form-group">
              <label class="control-label">매장</label>
                <p class="form-control-static">${order.relativeOrderMachine.relativeMachine.relativeStore.name.value}</p>
            </div>


          <hr/>

            <div class="form-group">
                <label class="control-label">Address</label>
                <p class="form-control-static">${order.address.address1}</p>
            </div>

        </div>
      </div>

<#--      <div class="panel panel-default" style="display: none">-->
<#--        <div class="panel-heading"><h4>Order Product Cancel/Refund/Exchange History</h4></div>-->
<#--        <div class="panel-body p0">-->

<#--          <div class="table-responsive">-->
<#--            <table id="table-ext-1" class="table table-bordered table-hover">-->
<#--              <colgroup>-->
<#--                <col width="20%">-->

<#--                <col width="10%">-->
<#--                <col width="60%">-->

<#--                <col width="10%">-->
<#--              </colgroup>-->
<#--              <thead>-->
<#--              <tr>-->
<#--                <th class="text-center">Name</th>-->

<#--                <th class="text-center">Status</th>-->
<#--                <th class="text-center">Reason</th>-->

<#--                <th class="text-center">Application Date</th>-->
<#--              </tr>-->
<#--              </thead>-->
<#--              <tbody>-->

<#--              <#if order.orderProducts?has_content>-->
<#--                  <#list order.orderProducts as op>-->
<#--                    <tr style="height: 120px">-->
<#--                      <td class="text-center">-->
<#--                          <#if op.relativeSKU?has_content && op.relativeSKU.relativeProduct?has_content>-->
<#--                              <#assign product = op.relativeSKU.relativeProduct/>-->
<#--                            <a href="/admin/product/update/${product.id?c}" target="_blank" data-toggle="tooltip" data-placement="top"-->
<#--                               title="Product Information">${product.name.value}</a>-->
<#--                          <#else>-->
<#--                              ${op.productName!}-->
<#--                          </#if>-->
<#--                      </td>-->

<#--                      <td class="text-center">${op.status.value}</td>-->
<#--                      <td class="text-left">-->
<#--                          <#if op.cancelShippingCost?has_content>-->
<#--                            shipping charge: ${op.cancelShippingCost.value}<br/>-->
<#--                          </#if>-->
<#--                          ${op.cancelMessage!}-->
<#--                      </td>-->

<#--                      <td class="text-center">-->
<#--                          <#if op.status == 'REFUND' || op.status == 'REFUND_COMPLETE'>-->
<#--                              <#if op.cancelDate?has_content>${op.cancelDate.format('yyyy-MM-dd HH:mm')}<#else>-</#if>-->
<#--                          <#else>-->
<#--                              <#if op.exchangeDate?has_content>${op.exchangeDate.format('yyyy-MM-dd HH:mm')}<#else>-</#if>-->
<#--                          </#if>-->
<#--                      </td>-->
<#--                    </tr>-->
<#--                  </#list>-->
<#--              </#if>-->
<#--              </tbody>-->
<#--            </table>-->
<#--          </div>-->
<#--        </div>-->
<#--      </div>-->

      <div class="panel panel-default">
        <div class="panel-heading"><h4>Consumer Information</h4></div>
        <div class="panel-body">

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label class="control-label">Fullname </label>
                <p class="form-control-static">
                  ${order.relativeBuyer.relativeUser.fullName!}
                </p>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label class="control-label">Phone Number </label>
                <p class="form-control-static">
                  ${order.relativeBuyer.relativeUser.mobile!}
                </p>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label class="control-label">이메일 </label>
                <p class="form-control-static">
                  <a href="/admin/user/update/${order.relativeBuyer.id?c}" target="_blank" data-toggle="tooltip" data-placement="top"
                     title="Member Information">${order.relativeBuyer.relativeUser.email}</a>
                </p>
              </div>
            </div>
            <div class="col-lg-6">
            </div>
          </div>

        </div>
      </div>

    </div>
      <#-- END : CONTENT-->
      <#-- SIDBAR -->
    <div class="col-lg-5 pl0-lg">

      <div class="panel panel-default">
        <div class="panel-heading"><h4>Order Information <span class="text-gray-light"><#if order.locale?has_content>(${order.locale})<#else>-</#if></span></h4></div>
        <div class="panel-body">

          <div class="form-group">
            <label class="control-label">Order Number</label>
            <p class="form-control-static"><strong>${order.oid!}</strong>
          </div>

          <hr/>
          <div class="form-group">
            <label class="control-label">주문상태</label>

            <select name="orderStatus" data-id="${order.id?c}" class="form-control">
                <#list orderStatuses as _orderStatus>
                  <option value="${_orderStatus}" <#if order.orderStatus?has_content && order.orderStatus == _orderStatus>selected</#if>>${_orderStatus.value}</option>
                </#list>
            </select>

            <#if order.orderStatus == 'PAYMENT_FAILED'>
              <br/>
              <p class="text-bold text-danger">* Failed Reason:&nbsp;
                <#if order.failedReasonByPayDollar?has_content>
                    ${order.failedReasonByPayDollar}
                <#else>Unknown</#if>
              </p>
            </#if>
          </div>

          <hr/>
          <div class="form-group">
            <label class="control-label">Order Timeline</label><br>
              <ul class="list-unstyled">
                  <li data-toggle="tooltip" data-placement="top"
                      title="Date">${orderMachine.startTime.format('yyyy.MM.dd')}</li>
                  <li data-toggle="tooltip" data-placement="top"
                      title="Time">${orderMachine.startTime.format('HH:mm')} - ${orderMachine.endTime.format('HH:mm')}</li>
              </ul>
          </div>

<#--          <hr/>-->
<#--          <div class="form-group">-->
<#--            <label class="control-label">Payment Status</label>-->
<#--            <div class="form-group">-->
<#--                <#if order.paymentComplete>-->
<#--                  <div class="label label-success">Complete</div><#else>-->
<#--                  <div class="label label-warning">No Payment</div></#if>-->
<#--            </div>-->
<#--          </div>-->

            <#if order.paymentDate?has_content>
              <hr/>
              <div class="form-group">
                <label class="control-label">Payment Complete Date</label>
                <p class="form-control-static">${order.paymentDate.format('yyyy.MM.dd HH:mm:ss')}</p>
              </div>
            </#if>

<#--            <#if order.deliveryCompleteDate?has_content>-->
<#--              <hr/>-->
<#--              <div class="form-group">-->
<#--                <label class="control-label">Shipping Complete Date</label>-->
<#--                <p class="form-control-static">${order.deliveryCompleteDate.format('yyyy.MM.dd HH:mm:ss')}</p>-->
<#--              </div>-->
<#--            </#if>-->

<#--          <hr/>-->
<#--          <div class="row">-->
<#--            <div class="col-sm-6">-->
<#--                <label class="control-label">PayDollar Refund Complete</label>-->
<#--              <div class="form-group">-->
<#--                  <#if order.refundByPayDollar>-->
<#--                    <div class="label label-success">Complete</div><#else>-->
<#--                    <div class="label label-warning">No Refund</div></#if>-->
<#--              </div>-->
<#--            </div>-->
<#--            <div class="col-sm-6">-->
<#--              <div class="form-group">-->
<#--                <label class="control-label">PayDollar Refund Date</label>-->
<#--                <p class="form-control-static">-->
<#--                  <#if order.refundDateByPayDollar?has_content>-->
<#--                    ${order.refundDateByPayDollar.format('yyyy.MM.dd HH:mm:ss')}-->
<#--                  <#else>-</#if>-->
<#--                </p>-->
<#--              </div>-->
<#--            </div>-->
<#--          </div>-->

        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading"><h4>Payment Amount Information</h4></div>
        <div class="panel-body">

          <div class="form-group">
            <label class="control-label">Payment Amount</label>
            <p class="form-control-static">${order.amount} KRW</p>
          </div>

            <#if order.fcPoint?has_content>
              <hr/>
              <div class="form-group">
                <label class="control-label">Used point </label>
                <p class="form-control-static">- ${order.fcPoint} Point</p>
              </div>
            </#if>

            <#if order.fcCoupon?has_content && order.fcCoupon != 0>
              <hr/>
              <div class="form-group">
                <label class="control-label">Used Coupon </label>
                <p class="form-control-static">- ${order.fcCoupon!} KRW</p>

                <#if order.usedCoupons?has_content>
                <hr style="margin: 2px 0"/>
                <ul>
                    <#list order.usedCoupons as couponBox>
                    <#assign coupon = couponBox.relativeCoupon/>
                      <li>[${coupon.type.value}] ${coupon.message.value} </li>
                    </#list>
                </ul>
                </#if>
              </div>
            </#if>
            <hr/>
            <div class="form-group">
                <label class="control-label">Total pay</label>
              <p class="form-control-static"><strong>${order.totalPrice} KRW</strong></p>
            </div>
        </div>
      </div>


        <#-- Meta Information (Modify 페이지 필수) -->
        <@ui.panelMetaInfo order.updatedDate order.createdDate/>
        <#-- END : Meta Information (Modify 페이지 필수) -->
    </div>
      <#-- END : SIDBAR -->
  </div>

</div>
