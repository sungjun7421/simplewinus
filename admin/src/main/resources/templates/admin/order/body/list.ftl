<#--<#assign pageparam = "&key=value"/>-->
<#assign pageparam = "&productName=${productName!}&memberQuery=${memberQuery!}"/>
<#if checkedOrderStatus?has_content>
<#--    <#assign checkedOrderStatusStr = ""/>-->
<#--    <#list checkedOrderStatus as item>-->
<#--        <#assign checkedOrderStatusStr>${checkedOrderStatusStr}${item}<#sep>,</#sep></#assign>-->
<#--    </#list>-->
    <#assign pageparam = pageparam + "&checkedOrderStatus=${checkedOrderStatus}"/>
</#if>
<div class="container-fluid">
    <#-- 필터 -->
  <div class="row">
    <div class="col-md-12">
      <div id="" class="panel panel-default panel-list-filter">
        <div class="panel-heading">검색 필터

          <a href="#" data-tool="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
            <em class="fa fa-minus"></em>
          </a>

          <a id="initial-list-filter" href="javascript:void(0);" data-tool="panel-refresh" data-toggle="tooltip" title="" class="pull-right">
            <em class="fa fa-refresh"></em>
          </a>

        </div>
        <div class="panel-wrapper collapse in">
          <div class="panel-body">
            <form id="form-list-filter" action="" method="get">
                <#include "../../common/list/fieldset-common.ftl"/>
                <#include "../../common/list/fieldset-date.ftl"/>

              <fieldset>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">주문상태</label>

                      <div class="col-xs-10">
                        <div class="form-group">

                            <select name="checkedOrderStatus" data-type="chosen-select" data-placeholder="주문상태를 선택하세요."
                                    class="form-control">
                              <option value="">Status</option>
                              <#list orderStatus as item>
                                <option value="${item}"
                                        <#if checkedOrderStatus?has_content && checkedOrderStatus == item>selected</#if>>${item.value}</option>
                              </#list>
                            </select>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>


<#--              <fieldset>-->
<#--                <div class="row">-->
<#--                  <div class="col-lg-6">-->
<#--                    <div class="form-group">-->
<#--                      <label class="col-xs-2 control-label">Pay Method</label>-->

<#--                      <div class="col-xs-10">-->

<#--                        <div class="form-group">-->
<#--                          <div &lt;#&ndash;style="font-size: 0.85em"&ndash;&gt;>-->
<#--                            <label class="radio-inline c-radio">-->
<#--                              <input type="radio" name="payMethod" value="" <#if !payMethod?has_content>checked</#if>>-->
<#--                              <span class="fa fa-circle"></span>All-->
<#--                            </label>-->
<#--                              <#list payMethods as item>-->
<#--                                <label class="radio-inline c-radio">-->
<#--                                  <input data-type="mode" type="radio" name="payMethod" value="${item}"-->
<#--                                         <#if payMethod?has_content && payMethod == item>checked</#if>>-->
<#--                                  <span class="fa fa-circle"></span>${item.value}-->
<#--                                </label>-->
<#--                              </#list>-->
<#--                          </div>-->
<#--                        </div>-->
<#--                      </div>-->
<#--                    </div>-->
<#--                  </div>-->
<#--                </div>-->
<#--              </fieldset>-->

              <fieldset>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">검색어</label>

                      <div class="col-xs-10">
                        <input name="query" type="text" class="form-control" placeholder="검색어를 입력하세요." value="${data.query!}">
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>

              <fieldset>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">제품명</label>

                      <div class="col-xs-10">
                        <input name="productName" type="text" class="form-control" placeholder="제품명을 입력하세요." value="${productName!}">
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>

              <fieldset>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">고객정보</label>

                      <div class="col-xs-10">
                        <input name="memberQuery" type="text" class="form-control" placeholder="이메일,이름,전화번호" value="${memberQuery!}">
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>

            </form>
          </div>
            <#-- 필터 실행 -->
          <div class="panel-footer">

            <button id="submit-list-filter" type="button" class="btn btn-primary">검색</button>

            <div class="btn-group">
              <button type="button" data-toggle="dropdown" class="btn dropdown-toggle btn-default">${data.pageSize}개씩 보기 <span class="caret"></span></button>
              <ul role="menu" class="dropdown-menu">
                <li><a href="<@spring.url "?query=${data.query!}&size=10&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 10</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=20&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 20</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=50&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 50</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=100&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 100</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=500&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 500</a></li>
              </ul>
            </div>

          </div>
            <#-- END : 필터 실행 -->
        </div>
      </div>
    </div>
  </div>
    <#-- END : 필터 -->
    <#-- HEADER -->
    <#assign createparam = header.url +"/create"/>
    <#include "/admin/common/list/header.ftl"/>
    <#-- END : HEADER -->
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

          <#-- START table-responsive-->
        <div class="table-responsive">
          <table id="table-ext-1" class="table table-bordered table-hover">
            <colgroup>
              <col width="5%">
              <col width="10%">
              <col width="20%">
              <col width="30%">
              <col width="20%">
              <col width="10%">
              <col width="5%">
            </colgroup>
            <thead>
            <tr>
              <th style="vertical-align: middle" class="text-center">#</th>
              <th style="vertical-align: middle" class="text-center">Order Number<br/>(Invoice Number)</th>
              <th style="vertical-align: middle" class="text-center">Consumer</th>
              <th style="vertical-align: middle" class="text-center">Price</th>
              <th style="vertical-align: middle" class="text-center">Payment</th>
              <th style="vertical-align: middle" class="text-center">Time</th>
              <th style="vertical-align: middle" class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>

            <#if data?has_content>
                <#list data.page.content as item>
                  <tr>
                    <td class="text-center">no.${data.firstNo- (item_index + 1)}<br/>id.${item.id?c}</td>

                    <td class="text-center">
                        ${item.oid!}<br/>
                      ( Device: ${item.orderDevice.value} )
                    </td>
                    <td class="text-left">

                          <strong>Member</strong>
                          <ul style="padding-left: 13px; margin-bottom: 0px">
                              <#if item.relativeBuyer?has_content>
                                <#assign user = item.relativeBuyer.relativeUser/>
                                <li>Fullname : ${user.fullName!}</li>
                                <li>PhoneNumber : ${user.phoneNumberCode!} ${user.mobile!}</li>
                                <li>이메일 : ${user.email!}</li>
                              </#if>
                          </ul>

                    </td>
                    <td class="text-lef">

                          Total Price Machine : ${item.amount!} KRW<br/>
                          Coupon Used : -${item.fcCoupon!} KRW<br/>
                          Point Used : -${item.fcPoint!} KRW<br/>
                          <hr style="margin: 2px 0">
                          <strong>Total : ${item.totalPrice!} KRW</strong><br/>

                    </td>
                    <td class="text-left">

                      <ul style="padding-left: 13px; margin-bottom: 0px">
                        <li>Order Status : <div class="label ${item.orderStatus.labelClass!}">${item.orderStatus.value!}</div>
                          <#if item.orderStatus == 'FAILED'>
                            <br/>
                            <span>Failed Reason:&nbsp;
                              <#if item.failedReasonByPayDollar?has_content>
                                ${item.failedReasonByPayDollar}
                              <#else>Unknown</#if>
                            </span>
                          </#if>
                        </li>
<#--                        <li>PayMethod : ${item.payMethod.value!}</li>-->
<#--                        <li>Payment Status : <#if item.paymentComplete>-->
<#--                            <div class="label label-success">Complete</div><#else>-->
<#--                            <div class="label label-warning">No Payment</div></#if></li>-->
                        <#if item.paymentComplete>
                          <li>Payment Date : ${item.paymentDate.format('yyyy.MM.dd HH:mm:ss')!}</li>
                        </#if>
                      </ul>
                    </td>

                    <td class="text-center">
                      <ul class="list-unstyled">
                        <li data-toggle="tooltip" data-placement="top"
                            title="Date">${item.relativeOrderMachine.startTime.format('yyyy.MM.dd')}</li>
                        <li data-toggle="tooltip" data-placement="top"
                            title="Time">${item.relativeOrderMachine.startTime.format('HH:mm')} - ${item.relativeOrderMachine.endTime.format('HH:mm')}</li>
                      </ul>
                    </td>
                    <td class="text-center">
                      <a href="<@spring.url header.url + "/view/${item.id?c}"/>"
                         class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top"
                         title="View and Edit">
                        <em class="fa fa-pencil"></em>
                      </a>
                    </td>
                  </tr>
                </#list>
            </#if>
            </tbody>
          </table>
        </div>
          <#-- END table-responsive-->

          <#-- TABLE FOOTER -->
        <div class="panel-footer">
          <div class="row">
            <div class="col-lg-12 text-center">
                <#include "../../common/list/pagination.ftl"/>
            </div>
          </div>
        </div>
          <#-- END : TABLE FOOTER -->
      </div>
    </div>
  </div>
</div>
