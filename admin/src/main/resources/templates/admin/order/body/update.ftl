<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div id="update-contact" class="container-fluid contact-create-update">
<#-- FORM -->
    <form id="form-update-contact" action="<@spring.url header.url + "/update"/>" method="post" data-parsley-validate="" novalidate="" data-parsley-international="true">
    <#-- Modify Complete -->
    <#include "../../common/modify-success.ftl"/>
    <#-- END : Modify Complete -->
        <div class="row">
            <div class="col-md-12">

                <div class="pull-left mb-lg">
                    <button type="submit" class="btn btn-primary btn-lg">
                        Modify
                    </button>
                <@spring.formHiddenInput "contact.id"/>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </div>

                <div class="pull-right mb-lg">
                    <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top" title="List">
                        <span class="icon-list"></span>
                    </a>
                    <button data-type="btn-delete"  type="button" class="btn btn-danger btn-lg" data-toggle="tooltip" data-placement="top" title="Delete"
                            data-action="<@spring.url header.url + "/delete"/>"
                            data-id="${contact.id?c}">
                        <span class="icon-trash"></span>
                    </button>
                </div>

            </div>
        </div>
        <div class="row">
        <#-- CONTENT-->
            <div class="col-lg-7">

                <div class="panel panel-default">

                    <div class="panel-body">

                        <#--<!-- Title &ndash;&gt;-->
                        <#--<div class="form-group">-->
                            <#--<label class="control-label">Title <span class="text-require">*</span></label>-->
                        <#--<@spring.formInput "contact.title",-->
                        <#--"placeholder=\"Please enter a title.\"-->
                            <#--class=\"form-control\"-->
                            <#--maxlength=\"100\"-->
                            <#--required"/>-->
                        <#--&lt;#&ndash;<span class="help-block m-b-none"> </span>&ndash;&gt;-->
                        <#--</div>-->

                        <#--<hr/>-->

                    <#-- WYSISWYG EDITOR -->
                    <@ui.wysiswygEdior "contact.content" "Content" false/>
                    <#-- END : WYSISWYG EDITOR -->

                        <hr/>
                    <#-- WYSISWYG EDITOR -->
                    <@ui.wysiswygEdior "contact.memo" "관리자 메모" false/>
                    <#-- END : WYSISWYG EDITOR -->
                    </div>
                </div>

            </div>
        <#-- END : CONTENT-->
        <#-- SIDBAR -->
            <div class="col-lg-5 pl0-lg">

                <div class="panel panel-default">
                    <div class="panel-body">
                        <!-- Fullname -->
                        <div class="form-group">
                            <label class="control-label">Fullname <span class="text-require">*</span></label>
                        <@spring.formInput "contact.fullName",
                        "placeholder=\"Fullname을 입력하세요.\"
                            class=\"form-control\"
                            maxlength=\"100\"
                            required"/>
                        <#--<span class="help-block m-b-none"> </span>-->
                        </div>

                        <hr/>

                        <!-- 휴대폰 번호 -->
                        <div class="form-group">
                            <label class="control-label">휴대폰 번호 <span class="text-require">*</span></label>
                        <@spring.formInput "contact.mobile",
                        "placeholder=\"휴대폰 번호를 입력하세요.\"
                            class=\"form-control\"
                            maxlength=\"100\"
                            required"/>
                        <#--<span class="help-block m-b-none"> </span>-->
                        </div>

                        <hr/>

                        <!-- Email -->
                        <div class="form-group">
                            <label class="control-label">이메일 <span class="text-require">*</span></label>
                        <@spring.formInput "contact.email",
                        "placeholder=\"Email을 입력하세요.\"
                            class=\"form-control\"
                            maxlength=\"255\"
                            required" "email"/>
                        <#--<span class="help-block m-b-none"> </span>-->
                        </div>

                        <hr/>

                    <@spring.bind "contact.complete"/>
                        <div class="form-group">
                            <label class="control-label">답변</label>
                            <div>
                                <label class="radio-inline c-radio">
                                    <input data-type="mode" id="inline-radio-active-1" type="radio" name="${spring.status.expression}" value="true" <#if spring.stringStatusValue == 'true'>checked</#if>>
                                    <span class="fa fa-circle"></span>Complete
                                </label>
                                <label class="radio-inline c-radio">
                                    <input data-type="mode" id="inline-radio-active-2" type="radio" name="${spring.status.expression}" value="false" <#if spring.stringStatusValue == 'false'>checked</#if>>
                                    <span class="fa fa-circle"></span>미Complete
                                </label>
                            </div>
                        </div>


                    </div>
                </div>

            <#-- Meta Information (Modify 페이지 필수) -->
            <@ui.panelMetaInfo contact.updatedDate contact.createdDate/>
            <#-- END : Meta Information (Modify 페이지 필수) -->
            </div>
        <#-- END : SIDBAR -->
        </div>
    </form>
</div>