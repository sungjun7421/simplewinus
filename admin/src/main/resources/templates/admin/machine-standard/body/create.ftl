<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div class="container-fluid es-standard-machine-create-update" xmlns="http://www.w3.org/1999/html">
    <#-- FORM -->
    <form id="form-create-standard-machine" action="<@spring.url header.url + "/create"/>" method="post"
          data-parsley-validate=""
          novalidate="" data-parsley-international="true">
        <div class="row">
            <div class="col-md-12">

                <div class="pull-left mb-lg">
                    <button type="submit" class="btn btn-primary btn-lg">
                        Save
                    </button>

                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </div>

                <div class="pull-right mb-lg">
                    <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip"
                       data-placement="top"
                       title="List">
                        <span class="icon-list"></span>
                    </a>
                </div>

            </div>
        </div>
        <div class="row">
            <#-- CONTENT-->
            <div class="col-lg-7">

                <#-- TAB Panel or Panel Wrapper -->
                <div <#if international>role="tabpanel" class="panel panel-transparent" data-type="tabpanel-language"
                     <#else>class="panel panel-default"</#if>>

                    <@spring.bind "standardMachine.name"/>
                    <#assign tabPath = spring.status.expression/>
                    <#-- TAB LANGUAGE HEADER -->
                    <@ui.tabListByLanguage tabPath/>
                    <#-- END : TAB LANGUAGE HEADER -->

                    <#-- TAB LANGUAGE BODY -->
                    <div <#if international>class="tab-content bg-white" <#else>class="panel-body"</#if>>

                        <#-- 국문 -->
                        <#if im.koKr>
                            <div id="${tabPath}-ko" <#if international>role="tabpanel"
                                 class="tab-pane ${im.koKr}"</#if>>

                                <#-- Title -->
                                <@ui.formInputTextByLanguage "standardMachine.name.textKoKr" "Name" true 255 "Enter the standard machine name."/>
                                <hr/>

                                <#-- WYSISWYG EDITOR -->
                                <@ui.wysiswygEdior "standardMachine.description.textKoKr" "Content" false/>

                                <@ui.formActiveByLanguage "standardMachine.internationalMode.koKr" />
                            </div>
                        <#else>
                            <@spring.formHiddenInput "standardMachine.name.textKoKr"/>
                            <@spring.formHiddenInput "standardMachine.description.textKoKr"/>
                        </#if>
                        <#-- END : 국문 -->
                        <#-- 영문 -->
                        <#if im.enUs>
                            <div id="${tabPath}-en" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- Title -->
                                <@ui.formInputTextByLanguage "standardMachine.name.textEnUs" "Name" true 255 "Enter the standard machine name."/>

                                <hr/>
                                <#-- WYSISWYG EDITOR -->
                                <@ui.wysiswygEdior "standardMachine.description.textEnUs" "Content" false/>
                                <@ui.formActiveByLanguage "standardMachine.internationalMode.enUs"/>
                            </div>
                        <#else>
                            <@spring.formHiddenInput "standardMachine.name.textEnUs"/>
                            <@spring.formHiddenInput "standardMachine.description.textEnUs"/>
                        </#if>
                        <#-- END : 영문 -->
                        <#-- 중문 (간체) -->
                        <#if im.zhCn>
                            <div id="${tabPath}-zh-cn" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- Title -->
                                <@ui.formInputTextByLanguage "standardMachine.name.textZhCn" "Name" true 255 "Enter the standard machine name."/>
                                <hr/>

                                <#-- WYSISWYG EDITOR -->
                                <@ui.wysiswygEdior "standardMachine.description.textZhCn" "Content" false/>
                                <@ui.formActiveByLanguage "standardMachine.internationalMode.zhCn"/>
                            </div>
                        <#else>
                            <@spring.formHiddenInput "standardMachine.name.textZhCn"/>
                            <@spring.formHiddenInput "standardMachine.description.textZhCn"/>
                        </#if>
                        <#-- END : 중문 (간체) -->
                        <#-- 중문 (번체) -->
                        <#if im.zhTw>
                            <div id="${tabPath}-zh-tw" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- Title -->
                                <@ui.formInputTextByLanguage "standardMachine.name.textZhTw" "Name" true 255 "Enter the standard machine name."/>

                                <hr/>

                                <#-- WYSISWYG EDITOR -->
                                <@ui.wysiswygEdior "standardMachine.description.textZhTw" "Content" false/>
                                <@ui.formActiveByLanguage "standardMachine.internationalMode.zhTw"/>
                            </div>
                        <#else>
                            <@spring.formHiddenInput "standardMachine.name.textZhTw"/>
                            <@spring.formHiddenInput "standardMachine.description.textZhTw"/>
                        </#if>
                        <#-- END : 중문 (번체) -->
                        <#-- 일문 -->
                        <#if im.jaJp>
                            <div id="${tabPath}-ja" <#if international>role="tabpanel" class="tab-pane"</#if>>

                                <#-- Title -->
                                <@ui.formInputTextByLanguage "standardMachine.name.textJaJp" "Name" true 255 "Enter the standard machine name."/>
                                <hr/>
                                <#-- WYSISWYG EDITOR -->
                                <@ui.wysiswygEdior "standardMachine.description.textJaJp" "Content" false/>
                                <@ui.formActiveByLanguage "standardMachine.internationalMode.jaJp"/>
                            </div>
                        <#else>
                            <@spring.formHiddenInput "standardMachine.name.textJaJp"/>
                            <@spring.formHiddenInput "standardMachine.description.textJaJp"/>
                        </#if>
                        <#-- END : 일문 -->
                    </div>
                </div>
                <#-- END : TAB LANGUAGE BODY -->
            </div>
            <#-- END : CONTENT-->
            <#-- SIDBAR -->
            <div class="col-lg-5 pl0-lg">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Type</h4>
                    </div>
                    <div class="panel-body">
                        <@spring.bind path="standardMachine.type"/>
                        <div class="form-group">
                            <label class="control-label">Machine Type <span class="text-require">*</span></label>

                            <select name="${spring.status.expression}" class="form-control">
                                <#list types as type>
                                    <option value="${type}"
                                            <#if standardMachine.type == type>selected</#if>>${type.value}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Image</h4></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <@ui.uploadImage "Thumbnail" "thumbnail" "" "JPEG 800 X 800"/>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                    </div>
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Detail Standard Machine</h4></div>
                    <div class="panel-body">
                        <@ui.formInputText "standardMachine.brand" "Brand" false 255 "" "text" ""/>
                        <@ui.formInputText "standardMachine.weight" "Weight" true 255 "" "number" ""/>
                    </div>
                </div>
            </div>
        </div>
        <#-- END : SIDBAR -->
    </form>
</div>
