<#--<#assign pageparam = "&key=value"/>-->
<#assign pageparam = "&type=${type!}&weight=${weight!}"/>
<div id="list-standard-machine" class="container-fluid">
    <#-- 필터 -->
  <div class="row">
    <div class="col-md-12">
      <div id="" class="panel panel-default panel-list-filter">
        <div class="panel-heading">검색 필터

          <a href="#" data-tool="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
            <em class="fa fa-minus"></em>
          </a>

          <a id="initial-list-filter" href="javascript:void(0);" data-tool="panel-refresh" data-toggle="tooltip"
             title="" class="pull-right">
            <em class="fa fa-refresh"></em>
          </a>

        </div>
        <div class="panel-wrapper collapse in">
          <div class="panel-body">
            <form id="form-list-filter" action="" method="get">
                <#include "../../common/list/fieldset-common.ftl"/>
              <fieldset>
                <div class="row">
                  <div class="col-lg-8">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">Reg. Date</label>

                      <div class="col-xs-5 ">
                        <div class="date list-filter-date date-wave" data-type="startDate">
                          <input name="startDate" type="text" class="form-control" placeholder="Start" value="${startDate!}" autocomplete="off">
                        </div>
                      </div>
                      <div class="col-xs-5">
                        <div class="date list-filter-date" data-type="endDate">
                          <input name="endDate" type="text" class="form-control" placeholder="End" value="${endDate!}" autocomplete="off">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <div class="row">
                  <div class="col-lg-8">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">Search Name</label>
                      <div class="col-xs-10">

                        <input name="query" type="text" class="form-control" placeholder="Please enter Machine name."
                               value="${data.query!}">
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <div class="row">
                  <div class="col-lg-8">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">Weight</label>

                      <div class="col-xs-10">
                        <input name="weight" type="number" class="form-control" placeholder="Please enter Weight."
                               value="${weight!}">
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <div class="row">
                  <div class="col-lg-8">
                    <div class="form-group">
                      <label class="col-xs-2 control-label">Type</label>

                      <div class="col-xs-10">
                        <select name="type" data-type="chosen-select" data-placeholder="Please select type."
                                class="form-control">
                          <option value="">All</option>
<#--                          <#list machineType as item>-->
<#--                            <option value="${item}"-->
<#--                                    <#if type?has_content && type == item>selected</#if>>-->
<#--                              ${item.value}</option>-->
<#--                          </#list>-->
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
            <#-- 필터 실행 -->
          <div class="panel-footer">

            <button id="submit-list-filter" type="button" class="btn btn-primary">검색</button>

            <div class="btn-group">
              <button type="button" data-toggle="dropdown" class="btn dropdown-toggle btn-default">${data.pageSize}개씩 보기 <span class="caret"></span></button>
              <ul role="menu" class="dropdown-menu">
                <li><a href="<@spring.url "?query=${data.query!}&size=10&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 10</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=20&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 20</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=50&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 50</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=100&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 100</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=300&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 300</a></li>
                <li><a href="<@spring.url "?query=${data.query!}&size=500&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">View 500</a></li>
              </ul>
            </div>
          </div>
            <#-- END : 필터 실행 -->
        </div>
      </div>
    </div>
  </div>
    <#-- END : 필터 -->
    <#-- HEADER -->
    <#assign createparam = header.url +"/create"/>
  <div class="row">
    <div class="col-md-12">
      <header class="clearfix pb-lg mb-sm">
          <#-- 목록 Status -->
        <div class="pull-left">
          <div class="list-total">total ${data.page.totalElements!},&nbsp;&nbsp;${data.currentIndex}
            /${data.page.totalPages} page
          </div>
        </div>
          <#-- END : 목록 Status -->
<#--          <#if createparam?has_content>-->
          <#-- 등록 버튼 -->
            <div class="pull-right">
              <a href="<@spring.url createparam/>" class="btn btn-primary btn-lg">Create</a>
            </div>
          <#-- 등록 버튼 -->
<#--          </#if>-->
      </header>
    </div>
  </div>
    <#-- END : HEADER -->

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

          <#-- START table-responsive-->
        <div class="table-responsive">
          <table id="table-ext-1" class="table table-bordered table-hover">
            <colgroup>
              <col width="10%">
              <col width="15%">
              <col>
              <col width="15%">
              <col width="10%">
<#--              <col width="10%">-->
              <col width="10%">
            </colgroup>
            <thead>
            <tr>
              <th class="text-center">#</th>
              <th class="text-center">Thumbnail</th>
              <th class="text-center">Name</th>
              <th class="text-center">Brand</th>
              <th class="text-center">Weight</th>
<#--              <th class="text-center">Type</th>-->
              <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>

            <#if data?has_content>
                <#list data.page.content as item>
                  <tr>
                    <td class="text-center">no.${data.firstNo- (item_index + 1)}<br/>id.${item.id?c}</td>

                    <td class="text-center">
                        <#if item.thumbnail?has_content>
                          <img src="${item.thumbnail}" class="img-responsive text-center" style="max-height: 60px; display: inline-block;"/>
                        <#else>-</#if>
                    </td>

                    <td class="text-left">
                      <p>기기 종류: ${item.name!}</p>
                    </td>

                    <td class="text-center"><p>${item.brand!}</p></td>
                    <td class="text-center"><p>${item.weight!}kg</p></td>
<#--                    <td class="text-center"><p>${item.type.value}</p></td>-->

                    <td class="text-center">
                      <a href="<@spring.url header.url + "/update/${item.id?c}"/><#if springMacroRequestContext.queryString?has_content>?${springMacroRequestContext.queryString}</#if>"
                         class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top"
                         title="View and edit">
                        <em class="fa fa-pencil"></em>
                      </a>
                    </td>
                  </tr>
                </#list>
            </#if>
            </tbody>
          </table>
        </div>
          <#-- END table-responsive-->

          <#-- TABLE FOOTER -->
        <div class="panel-footer">
          <div class="row">
            <div class="col-lg-12 text-center">
                <#include "../../common/list/pagination.ftl"/>
            </div>
          </div>
        </div>
          <#-- END : TABLE FOOTER -->
      </div>
    </div>
  </div>
</div>
