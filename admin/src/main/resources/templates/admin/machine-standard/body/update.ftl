<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div id="update-standard-machine" class="container-fluid es-standard-machine-create-update">
  <#-- FORM -->
  <form id="form-update-standard-machine" action="<@spring.url header.url + "/update"/><#if springMacroRequestContext.queryString?has_content>?${springMacroRequestContext.queryString?replace("success&", "")}</#if>" method="post"
        data-parsley-validate=""
        novalidate="" data-parsley-international="true">
    <#-- Modify Complete -->
    <#include "../../common/modify-success.ftl"/>
    <#-- END : Modify Complete -->
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            Modify
          </button>
          <@spring.formHiddenInput "machine.id"/>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/><#if springMacroRequestContext.queryString?has_content>?${springMacroRequestContext.queryString?replace("success&", "")}</#if>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top"
             title="List">
            <span class="icon-list"></span>
          </a>
          <button data-type="btn-delete" type="button" class="btn btn-danger btn-lg" data-toggle="tooltip"
                  data-placement="top" title="Delete"
                  data-action="<@spring.url header.url + "/delete"/>"
                  data-id="${machine.id?c}">
            <span class="icon-trash"></span>
          </button>
        </div>

      </div>
    </div>
    <div class="row">
      <#-- CONTENT-->
      <div class="col-lg-7">

        <#-- TAB Panel or Panel Wrapper -->
        <div <#if international>role="tabpanel" class="panel panel-transparent" data-type="tabpanel-language"
             <#else>class="panel panel-default"</#if>>

          <@spring.bind "machine.name"/>
          <#assign tabPath = spring.status.expression/>
          <#-- TAB LANGUAGE HEADER -->
          <@ui.tabListByLanguage tabPath/>
          <#-- END : TAB LANGUAGE HEADER -->

          <#-- TAB LANGUAGE BODY -->
          <div <#if international>class="tab-content bg-white" <#else>class="panel-body"</#if>>

            <#-- 국문 -->
            <#if im.koKr>
              <div id="${tabPath}-ko" <#if international>role="tabpanel"
                   class="tab-pane ${im.koKr}"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "machine.name.textKoKr" "Name" true 255 "Enter the standard machine name."/>
                <hr/>

                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "machine.description.textKoKr" "Content" false/>

                <@ui.formActiveByLanguage "machine.internationalMode.koKr" />
              </div>
            <#else>
              <@spring.formHiddenInput "machine.name.textKoKr"/>
              <@spring.formHiddenInput "machine.description.textKoKr"/>
            </#if>
            <#-- END : 국문 -->
            <#-- 영문 -->
            <#if im.enUs>
              <div id="${tabPath}-en" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "machine.name.textEnUs" "Name" true 255 "Enter the standard machine name."/>

                <hr/>
                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "machine.description.textEnUs" "Content" false/>
                <@ui.formActiveByLanguage "machine.internationalMode.enUs"/>
              </div>
            <#else>
              <@spring.formHiddenInput "machine.name.textEnUs"/>
              <@spring.formHiddenInput "machine.description.textEnUs"/>
            </#if>
            <#-- END : 영문 -->
            <#-- 중문 (간체) -->
            <#if im.zhCn>
              <div id="${tabPath}-zh-cn" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "machine.name.textZhCn" "Name" true 255 "Enter the standard machine name."/>
                <hr/>

                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "machine.description.textZhCn" "Content" false/>
                <@ui.formActiveByLanguage "machine.internationalMode.zhCn"/>
              </div>
            <#else>
              <@spring.formHiddenInput "machine.name.textZhCn"/>
              <@spring.formHiddenInput "machine.description.textZhCn"/>
            </#if>
            <#-- END : 중문 (간체) -->
            <#-- 중문 (번체) -->
            <#if im.zhTw>
              <div id="${tabPath}-zh-tw" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "machine.name.textZhTw" "Name" true 255 "Enter the standard machine name."/>

                <hr/>

                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "machine.description.textZhTw" "Content" false/>
                <@ui.formActiveByLanguage "machine.internationalMode.zhTw"/>
              </div>
            <#else>
              <@spring.formHiddenInput "machine.name.textZhTw"/>
              <@spring.formHiddenInput "machine.description.textZhTw"/>
            </#if>
            <#-- END : 중문 (번체) -->
            <#-- 일문 -->
            <#if im.jaJp>
              <div id="${tabPath}-ja" <#if international>role="tabpanel" class="tab-pane"</#if>>

                <#-- Title -->
                <@ui.formInputTextByLanguage "machine.name.textJaJp" "Name" true 255 "Enter the standard machine name."/>
                <hr/>
                <#-- WYSISWYG EDITOR -->
                <@ui.wysiswygEdior "machine.description.textJaJp" "Content" false/>
                <@ui.formActiveByLanguage "machine.internationalMode.jaJp"/>
              </div>
            <#else>
              <@spring.formHiddenInput "machine.name.textJaJp"/>
              <@spring.formHiddenInput "machine.description.textJaJp"/>
            </#if>
            <#-- END : 일문 -->
          </div>
        </div>
        <#-- END : TAB LANGUAGE BODY -->

        <#-- End : 옵션 -->
      </div>
      <#-- END : CONTENT-->
      <#-- SIDBAR -->
      <div class="col-lg-5 pl0-lg">

        <div class="panel panel-default">
          <div class="panel-heading">
            <h4>Type</h4>
          </div>
          <div class="panel-body">
            <@spring.bind path="machine.type"/>
            <div class="form-group">
              <label class="control-label">기기 종류. Type <span class="text-require">*</span></label>
              <select name="${spring.status.expression}" class="form-control">
                <#list types as type>
                  <option value="${type}"
                          <#if machine.type == type>selected</#if>>${type.value}</option>
                </#list>
              </select>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Image</h4></div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-6">
                <@ui.uploadImage "Thumbnail Image" "thumbnail" "${machine.thumbnail!}" "JPEG 800 X 800"/>
              </div>
              <div class="col-md-6"></div>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading"><h4>Detail standard machine</h4></div>
          <div class="panel-body">
            <@ui.formInputText "machine.brand" "Brand" false 255 "" "text" ""/>
            <@ui.formInputText "machine.weight" "Weight" false 255 "" "number" ""/>
          </div>
        </div>

        <@ui.panelMetaInfo machine.updatedDate machine.createdDate/>
      </div>
      <#-- END : SIDBAR -->
    </div>
  </form>
</div>
