<#--<#assign pageparam = "&key=value"/>-->
<#assign pageparam = ""/>
<div class="container-fluid">

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

          <#-- START table-responsive-->
        <div class="table-responsive">
          <table id="table-ext-1" class="table table-bordered table-hover">
            <colgroup>
              <col width="5%">

              <col width="10%">
              <col width="30%">

              <col width="10%">
              <col width="10%">
              <col width="10%">
              <col width="10%">
              <col width="5%">
            </colgroup>
            <thead>
            <tr>
              <th class="text-center">#</th>

              <th class="text-center">썸네일</th>
              <th class="text-center">제목</th>

              <th class="text-center">릴리즈</th>
              <th class="text-center">노출여부</th>
              <th class="text-center">저장시간</th>
              <th class="text-center">등록일</th>
              <th class="text-center">본문</th>
            </tr>
            </thead>
            <tbody>

            <#if list?has_content>
                <#list list as item>
                  <tr>
                    <td class="text-center" data-toggle="tooltip" data-placement="top"
                        title="G.${item.relativeVersionContent.id?c}">no.${list?size - (item_index)}
                      <br/>id.${item.id?c}</td>
                    <td class="text-center">
                      <img class="img-responsive" src="${item.thumbnail!}" style="height: 60px;"/>
                    </td>
                    <td class="text-left">
                        <#if international>
                          <ul style="padding-left: 13px; margin-bottom: 0px">
                              <#if im.koKr>
                              <li <#if !item.internationalMode.koKr>class="inactive"</#if>>국문
                                : <#if item.internationalMode.koKr>${item.title.textKoKr!}<#else>비활성</#if></li></#if>
                              <#if im.enUs>
                              <li <#if !item.internationalMode.enUs>class="inactive"</#if>>영문
                                : <#if item.internationalMode.enUs>${item.title.textEnUs!}<#else>비활성</#if></li></#if>
                              <#if im.zhCn>
                              <li <#if !item.internationalMode.zhCn>class="inactive"</#if>>간체(중)
                                : <#if item.internationalMode.zhCn>${item.title.textZhCn!}<#else>비활성</#if></li></#if>
                              <#if im.zhTw>
                              <li <#if !item.internationalMode.zhTw>class="inactive"</#if>>번체(중)
                                : <#if item.internationalMode.zhTw>${item.title.textZhTw!}<#else>비활성</#if></li></#if>
                              <#if im.jaJp>
                              <li <#if !item.internationalMode.jaJp>class="inactive"</#if>>일문
                                : <#if item.internationalMode.jaJp>${item.title.textJaJp!}<#else>비활성</#if></li></#if>
                          </ul>
                        <#else>
                            ${item.title.value!}
                        </#if>
                    </td>


                    <td class="text-center"><#if item.releaseVersion>
                        <div class="label label-success">활성</div><#else>
                        <div class="label label-warning">비활성</div></#if></td>

                    <td class="text-center"><#if item.active>
                        <div class="label label-success">활성</div><#else>
                        <div class="label label-warning">비활성</div></#if></td>

                    <td class="text-center">
                      <#if item.saveTime?has_content>
                        ${item.saveTime.format('yyyy.MM.dd HH:mm:ss')}
                      </#if></td>
                    <td class="text-center">
                        <#if item.regDate?has_content>
                            ${item.regDate.format('yyyy.MM.dd HH:mm:ss')}
                        </#if></td>
                    <td class="text-center">

                      <a href="#" class="btn btn-sm btn-default"
                         data-toggle="tooltip" data-placement="top" title="본문 보기">
                        본문
                      </a>
                    </td>
                  </tr>
                </#list>
            </#if>
            </tbody>
          </table>
        </div>
          <#-- END table-responsive-->


      </div>
    </div>
  </div>
</div>