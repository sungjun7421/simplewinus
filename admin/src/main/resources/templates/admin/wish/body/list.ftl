<#--<#assign pageparam = "&key=value"/>-->
<#assign pageparam = "&type=${type!}"/>
<div class="container-fluid">
    <#-- Search Option -->
    <div class="row">
        <div class="col-md-12">
            <div id="" class="panel panel-default panel-list-filter">
                <div class="panel-heading">검색 필터

                    <a href="#" data-tool="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
                        <em class="fa fa-minus"></em>
                    </a>

                    <a id="initial-list-filter" href="javascript:void(0);" data-tool="panel-refresh" data-toggle="tooltip"
                       title="" class="pull-right">
                        <em class="fa fa-refresh"></em>
                    </a>

                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <form id="form-list-filter" action="" method="get"  onsubmit="return beforeClickSubmit()"   >
                            <#include "/admin/common/list/fieldset-date.ftl"/>
                            <#include "/admin/common/list/fieldset-search2.ftl"/>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="col-xs-2 control-label">Type </label>

                                        <div class="col-xs-10">
                                            <a></a>
                                            <input type="radio" name="byProduct" value="1" onchange="goTo('?byProduct=1')" <#if byProduct=='1'>checked</#if>  >
                                            <label>Product</label>
                                            <input type="radio" name="byProduct" value="0" onchange="goTo('?byProduct=0')" <#if byProduct=='0'>checked</#if> >

                                            <label>User</label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <#if byProduct=='1'>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-xs-2 control-label">Product </label>

                                            <div class="col-xs-10">
                                                <select name="productSelected" data-type="chosen-select" data-placeholder="Please select product"
                                                        class="form-control">
                                                    <option value="">--All--</option>
                                                    <#list products as item>
                                                        <option value="${item.id}"
                                                                <#if products?has_content && productSelected == item.id>selected</#if>>${item.information.name.value}</option>
                                                    </#list>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <#else >
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" id="mydiv">
                                            <label class="col-xs-2 control-label">User </label>


                                            <div class="col-xs-10">
                                                <input type="text" placeholder="Member fullname, email, Search using cell phone number"
                                                       class="form-control "
                                                       name="query"
                                                       data-type="autocomplete-ajax"
                                                       data-url="/admin/api/user"
                                                       data-param-name="query"
                                                       value="${query}"
                                                       autofocus
                                                       data-fn-name="callbackByUser">
                                                <input name="user" data-displayInput="false" type="hidden" min="0" class="form-control" placeholder="User Id" value="${user!}">
                                                <input name="oldQuery" data-displayInput="false" type="hidden" min="0" class="form-control" disabled value="${query!}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </#if>

                        </form>
                    </div>
                    <div class="panel-footer">

                        <button id="submit-list-filter"  type="button" class="btn btn-primary">검색</button>

                        <div class="btn-group">
                            <button type="button" data-toggle="dropdown" class="btn dropdown-toggle btn-default">${data.pageSize} 개씩
                                보기<span class="caret"></span></button>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="<@spring.url "?size=10&byProduct=${byProduct!}&search=${search!}&productSelected=${productSelected!}&user=${user!}&query=${query!}&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">10
                                        개씩 보기</a></li>
                                <li>
                                    <a href="<@spring.url "?size=20&byProduct=${byProduct!}&search=${search!}&productSelected=${productSelected!}&user=${user!}&query=${query!}&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">20
                                        개씩 보기</a></li>
                                <li>
                                    <a href="<@spring.url "?size=50&byProduct=${byProduct!}&search=${search!}&productSelected=${productSelected!}&user=${user!}&query=${query!}&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">50
                                        개씩 보기</a></li>
                                <li>
                                    <a href="<@spring.url "?size=100&byProduct=${byProduct!}&search=${search!}&productSelected=${productSelected!}&user=${user!}&query=${query!}&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">100
                                        개씩 보기</a></li>
                                <li>
                                    <a href="<@spring.url "?size=500&byProduct=${byProduct!}&search=${search!}&productSelected=${productSelected!}&user=${user!}&query=${query!}&startDate=${startDate!}&endDate=${endDate!}${pageparam}"/>">500
                                        개씩 보기</a></li>
                            </ul>
                        </div>


                        <#--<button id="excel-list-filter" type="button" data-action="<@spring.url "/admin/board/blogCategory/excel"/>" class="btn btn-labeled btn-default">-->
                        <#--<span class="text">엑셀 다운로드</span>-->
                        <#--<span class="btn-label btn-label-right"><i class="fa fa-file-excel-o"></i></span>-->
                        <#--</button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <#-- END : 필터 -->
    <#-- HEADER -->
    <#include "/admin/common/list/header.ftl"/>
    <#-- END : HEADER -->
    <#if byProduct=='1'>
<#--        table wish for product-->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="table-responsive">
                        <table id="table-ext-1" class="table table-bordered table-hover">
<#--                            table header-->
                            <thead>
                            <tr>
                                <th style="width:10%" class="text-center">No#</th>
                                <th style="width:10%" class="text-center">User id</th>
                                <th style="width:auto" class="text-center">User email</th>
                                <th style="width: auto" class="text-center">Product Id</th>
                                <th style="width: auto" class="text-center">Product Name</th>
                                <th style="width: auto" class="text-center">Wish date</th>
                            </tr>
                            </thead>
<#--                            table body-->
                            <tbody>
                                <#if data?has_content>
                                    <#list data.page.content as item>
                                        <tr>
                                            <td class="text-center">no.${data.firstNo- (item_index + 1)}</td>
                                            <td class="text-center">${item.id.idBuyer!}</td>
                                            <td class="text-center">${item.relativeBuyer.relativeUser.email!}</td>
                                            <td class="text-center">${item.id.idProduct!}</td>
                                            <td class="text-center">${item.relativeProduct.information.name.value!}</td>
                                            <td class="text-center">${item.createdDate!}</td>
                                        </tr>
                                    </#list>
                                </#if>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </#if>
<#--        table wish for user-->
    <#if byProduct=='0'>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="table-responsive">
                        <table id="table-ext-1" class="table table-bordered table-hover">
                            <#--                            table header-->
                            <thead>
                            <tr>
                                <th style="width:10%" class="text-center">No#</th>
                                <th style="width: auto" class="text-center">Product Id</th>
                                <th style="width: auto" class="text-center">Product Name</th>
                                <th style="width: auto" class="text-center">Wish date</th>
                            </tr>
                            </thead>
                            <#--                            table body-->
                            <tbody>
                            <#if data?has_content>
                                <#list data.page.content as item>
                                    <tr>
                                        <td class="text-center">no.${data.firstNo- (item_index + 1)}</td>
                                        <td class="text-center">${item.id.idProduct!}</td>
                                        <td class="text-center">${item.relativeProduct.information.name.value!}</td>
                                        <td class="text-center">${item.createdDate!}</td>
                                    </tr>
                                </#list>
                            </#if>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </#if>
    <#-- TABLE FOOTER -->
    <div class="panel-footer">
        <div class="row">
            <div class="col-lg-12 text-center">
                <#include "/admin/common/list/pagination.ftl"/>
            </div>
        </div>
    </div>
    <#-- END : TABLE FOOTER -->
    <script>
        function goTo( ur) {
             window.location.href = window.location.origin+window.location.pathname+ur;
        }
    </script>
</div>
