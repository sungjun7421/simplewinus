# MyBatis 
## MyBatis 구조

1. config
 - {package}.core.config.database.MyBatisConfig
2. XML mapper
 - /resources/mappers/{Domain}Mapper.xml
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="{package}.core.domain.user.UserDAO">

    <select id="selectUsers"parameterType="{package}.core.domain.{Domain}Parameter" resultType="{package}.core.model.resbody.user.MeResBody">
        SELECT `id`, `fullName`, `image`
        FROM User
    </select>

</mapper>
```

3. DAO
- {Domain}DAO.java