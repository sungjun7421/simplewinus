package com.logisall.anyware.core.model.reqbody.sample;

import com.logisall.anyware.core.model.BaseResponseBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class SampleReqBody extends BaseResponseBody {
  private static final long serialVersionUID = -6540830096786917910L;

  @Schema(description = "TEST 1", example = "TEST")
  private String test1;

  @Schema(description = "TEST 2", example = "10")
  private Long test2;

  @Schema(description = "TEST 3", example = "true")
  private boolean test3;
}
