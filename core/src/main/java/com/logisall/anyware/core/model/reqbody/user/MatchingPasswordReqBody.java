package com.logisall.anyware.core.model.reqbody.user;

import com.logisall.anyware.core.model.BaseRequestBody;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Pattern;

@Setter
@Getter
@ToString
public class MatchingPasswordReqBody extends BaseRequestBody {

  private static final long serialVersionUID = -967274294480844947L;

  @Pattern(regexp = ValidUtils.PATTERN_PASSWORD_NEW)
  @Schema(description = "password", example = "abcd1234", minLength = 6, maxLength = 30)
  private String password;
}
