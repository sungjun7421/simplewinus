package com.logisall.anyware.core.model.resbody.commerce.point;

import com.logisall.anyware.core.config.serializer.JsonLocalDateDeserializer;
import com.logisall.anyware.core.config.serializer.JsonLocalDateSerializer;
import com.logisall.anyware.core.config.serializer.JsonLocalDateTimeDeserializer;
import com.logisall.anyware.core.config.serializer.JsonLocalDateTimeSerializer;
import com.logisall.anyware.core.domain.commerce.point.Point;
import com.logisall.anyware.core.model.BaseResponseBody;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Relation(value = "point", collectionRelation = "points")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PointResBody extends BaseResponseBody {

  private static final long serialVersionUID = 8680650065260534267L;

  private Long id;
  private int point;
  private int used;
  private Point.Status status;
  private String message;
  private int total;

  @JsonSerialize(using = JsonLocalDateSerializer.class)
  @JsonDeserialize(using = JsonLocalDateDeserializer.class)
  private LocalDate expirationDate;

  @JsonSerialize(using = JsonLocalDateTimeSerializer.class)
  @JsonDeserialize(using = JsonLocalDateTimeDeserializer.class)
  private LocalDateTime createdDate;

}
