package com.logisall.anyware.core.service.send.sms;


import com.logisall.anyware.core.vo.SMS;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

public interface SendSMSService {


    Future<Map> send(SMS sms) throws Exception;

    /**
     * SMS 발송
     *
     * @param sms 메세지 정보
     */
    void sendSMS(SMS sms) throws Exception;

    /**
     * LMS 발송
     *
     * @param sms 메세지 정보
     */
    void sendLMS(SMS sms) throws Exception;

    /**
     * MMS 발송
     *
     * @param sms 메세지 정보
     */
    void sendMMS(SMS sms) throws Exception;

    /**
     * REPORT 조회 서비스
     *
     * @param cmid cmid
     * @return 결과값
     */
    Map report(String cmid);

    /**
     * 잔액 조회 서비스
     *
     * @param balanceDate 잔액 조회 날짜 (201612)
     * @return 결과값
     */
    Map balance(String balanceDate);

    Map balance();

    /**
     * 충전금액 조회 서비스
     *
     * @param depositDate 충전금액 조회 날짜 (201612)
     * @return 결과값
     */
    Map deposit(String depositDate);

    Map deposit();

    /**
     * 발신번호리스트 조회
     */
    List<Map<String, Object>> listSendNumber();

    /**
     * 발신번호 등록/인증
     * 1차 등록하고, 2차 인증해야된다. (2번 날려야함)
     *
     * @param sendnumber 발신번호
     * @param comment    코맨트
     * @param pintype    SMS/VMS
     * @param pincode    인증번호 (2번째)
     * @return 결과값
     */
    Map insertSendNumber(String sendnumber, String comment, String pintype, String pincode);
}
