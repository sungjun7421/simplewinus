package com.logisall.anyware.core.domain.commerce.coupon;

import com.logisall.anyware.core.domain.commerce.coupon.Coupon;
import com.logisall.anyware.core.domain.commerce.coupon.CouponRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface CouponRepository extends
    JpaRepository<Coupon, Long>,
    QuerydslPredicateExecutor<Coupon>,
        CouponRepositoryCustom {

    boolean existsByCode(String code);
    Coupon findByCode(String code);
//    List<Coupon> findAllByRelativeBuyer_Id(Long idBuyer);
}
