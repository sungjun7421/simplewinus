package com.logisall.anyware.core.service.send.sms;


import com.logisall.anyware.core.utils.StringUtils;
import com.logisall.anyware.core.vo.SMS;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

@Slf4j
@Service
public class SendSMSServiceImpl implements SendSMSService {

  public static final String EVENT_BUS_COMMAND = "SMS";
  private static final Integer API_VERSION = 1;

  @Value("${apistore.id}")
  private String apiStoreId;

  @Value("${apistore.key}")
  private String apiStoreKey;

  @Value("${apistore.sms.send-phone}")
  private String sendPhone;

  @Value("${apistore.sms.send-name}")
  private String sendName;


  @Async
  @Override
  public Future<Map> send(final com.logisall.anyware.core.vo.SMS sms) throws Exception {
    log.debug("> send sms : {}", sms);

    if (com.logisall.anyware.core.utils.StringUtils.isNotEmpty(sms.getDestPhone()) && !sms.getDestPhone().contains("*")) {

      String url = String.format("http://api.apistore.co.kr/ppurio/%s/message/%s/%s"
          , API_VERSION
          , sms.getType().getValue()
          , apiStoreId
      );

      log.debug("api store sms url : {}", url);

      String send_phone = com.logisall.anyware.core.utils.StringUtils.isEmpty(sms.getSendPhone()) ? this.sendPhone : sms.getSendPhone();
      String send_name = StringUtils.isEmpty(sms.getSendName()) ? this.sendName : sms.getSendName();

      MultiValueMap<String, String> multiValueMap = sms.getMultiValueMap();
      multiValueMap.add("send_phone", send_phone);
      multiValueMap.add("send_name", send_name);

      RestTemplate restTemplate = new RestTemplate();
      HttpEntity request = new HttpEntity<>(multiValueMap, this.headers());
      Map result = restTemplate.postForObject(url, request, Map.class);
      log.info("> send result ::: {}", result);
      return new AsyncResult<>(result);
    }
    return null;
  }


  @Override
  @Async
  public void sendSMS(com.logisall.anyware.core.vo.SMS sms) throws Exception {
    sms.setType(com.logisall.anyware.core.vo.SMS.Type.SMS);
    this.send(sms);
  }

  @Override
  @Async
  public void sendLMS(com.logisall.anyware.core.vo.SMS sms) throws Exception {
    sms.setType(com.logisall.anyware.core.vo.SMS.Type.LMS);
    this.send(sms);
  }

  @Override
  @Async
  public void sendMMS(com.logisall.anyware.core.vo.SMS sms) throws Exception {
    // TODO multipart/form-data 형식으로 추가
//        sms.setType(SMS.Type.MMS);
//        eventBus.notify(EVENT_BUS_COMMAND, Event.wrap(sms));
  }

  @Override
  public Map report(String cmid) {

    String url = String.format("http://api.apistore.co.kr/ppurio/%s/message/report/%s"
        , API_VERSION
        , apiStoreId
    );

    URI uri = UriComponentsBuilder.fromHttpUrl(url)

        .queryParam("cmid", cmid)
        .queryParam("client_id", apiStoreId)
        .build().toUri();


    RestTemplate restTemplate = new RestTemplate();
    HttpEntity<?> entity = new HttpEntity<>(this.headers());

    HttpEntity<Map> response = restTemplate.exchange(
        uri,
        HttpMethod.GET,
        entity,
        Map.class);

    Map result = response.getBody();

    if (!result.get("dest_phone").equals("result is null")) {
      try {
        int status = result.get("call_status") == null ? 0 : Integer.valueOf(result.get("call_status").toString());
        if (status != 0) {
          result.put("status_msg", SMS.statusMsg(status));
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    } else {
      result.put("status_msg", "응답대기중");
      result.put("call_status", 0);
      result.put("dest_phone", "응답대기중");
    }
    return result;
  }

  @Override
  public Map balance(String balanceDate) {

    String url = String.format("http://api.apistore.co.kr/ppurio/%s/message/balance/%s/%s"
        , API_VERSION
        , apiStoreId
        , balanceDate
    );

    URI uri = UriComponentsBuilder.fromHttpUrl(url)
        .build().toUri();


    RestTemplate restTemplate = new RestTemplate();
    HttpEntity<?> entity = new HttpEntity<>(this.headers());

    HttpEntity<Map> response = restTemplate.exchange(
        uri,
        HttpMethod.GET,
        entity,
        Map.class);

    return response.getBody();
  }

  @Override
  public Map balance() {
    LocalDateTime now = LocalDateTime.now();
    int monthValue = now.getMonthValue();
    String monthValueStr = String.valueOf(monthValue);
    if (monthValue < 10) {
      monthValueStr = "0" + monthValueStr;
    }
    return this.balance(String.format("%s%s", now.getYear(), monthValueStr));
  }

  @Override
  public Map deposit(String depositDate) {

    String url = String.format("http://api.apistore.co.kr/ppurio/%s/message/deposit/%s/%s"
        , API_VERSION
        , apiStoreId
        , depositDate
    );

    URI uri = UriComponentsBuilder.fromHttpUrl(url)
        .build().toUri();

    RestTemplate restTemplate = new RestTemplate();
    HttpEntity<?> entity = new HttpEntity<>(this.headers());

    HttpEntity<Map> response = restTemplate.exchange(
        uri,
        HttpMethod.GET,
        entity,
        Map.class);

    return response.getBody();
  }

  @Override
  public Map deposit() {
    LocalDateTime now = LocalDateTime.now();
    int monthValue = now.getMonthValue();
    String monthValueStr = String.valueOf(monthValue);
    if (monthValue < 10) {
      monthValueStr = "0" + monthValueStr;
    }
    return this.deposit(String.format("%s%s", now.getYear(), monthValueStr));
  }

  @Override
  public List<Map<String, Object>> listSendNumber() {

    String url = String.format("http://api.apistore.co.kr/ppurio/%s/sendnumber/list/%s"
        , API_VERSION
        , apiStoreId
    );

    RestTemplate restTemplate = new RestTemplate();
    HttpEntity request = new HttpEntity<>(null, this.headers());
    Map result = restTemplate.postForObject(url, request, Map.class);

    List<Map<String, Object>> list = (List<Map<String, Object>>) result.get("numberList");
    Collections.reverse(list);
    return list;
  }


  @Override
  public Map insertSendNumber(String sendnumber, String comment, String pintype, String pincode) {

    String url = String.format("http://api.apistore.co.kr/ppurio/%s/sendnumber/save/%s"
        , 2
        , apiStoreId
    );

    MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();
    multiValueMap.add("sendnumber", sendnumber);
    multiValueMap.add("comment", comment);
    multiValueMap.add("pintype", pintype);
    multiValueMap.add("pincode", pincode);

    RestTemplate restTemplate = new RestTemplate();
    HttpEntity request = new HttpEntity<>(multiValueMap, this.headers());
    return restTemplate.postForObject(url, request, Map.class);
  }

  private HttpHeaders headers() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    headers.set("x-waple-authorization", apiStoreKey);
    return headers;
  }


}
