package com.logisall.anyware.core.domain.commerce.order;

import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.domain.commerce.order.OrderRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends
    JpaRepository<Order, Long>,
    QuerydslPredicateExecutor<Order>,
    OrderRepositoryCustom {

    Optional<Order> findByOid(String oid);
//    Order findByOid(String oid);
    boolean existsByOid(String oid);
    List<Order> findByTransactionInfo_TxId(String txId);



    List<Order> findAllByRelativeBuyerAndOrderStatus(Buyer buyer, OrderStatus orderStatus);
}
