package com.logisall.anyware.core.domain.commerce.buyer;

import com.logisall.anyware.core.domain.commerce.buyer.BuyerRepositoryCustom;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;

@CommonsLog
public class BuyerRepositoryImpl implements BuyerRepositoryCustom {

    @Autowired
    private JPAQueryFactory jpaQueryFactory;
}
