package com.logisall.anyware.core.model.reqbody.commerce.product;

import com.logisall.anyware.core.model.BaseRequestBody;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@Slf4j
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductReqBody extends BaseRequestBody {
  private static final long serialVersionUID = 1822519911426718242L;
  public static int GROUP = 10; // 10인이상
  public static String SALE = "0.0"; // 10%

  private Long id; // 상품 ID
  private String name; // 구매 상품내역 {ProductName}-{OptionsName}

  private int price; // 판매가
  private int qty; // 수량 (옵션이 있을 경우 : 옵션의 재고, 옵션이 없을 경우 : 상품의 재고)
  private int dc; // ESSale에 대한 제품 1개당 할인
  private int hwdc; // 수기할인
  private int amount; // 금액

  private Long idCart; // 장바구니 ID

  // 합계가 일치하는지 검사
  public boolean isMatched() {

    BigDecimal priceBig = BigDecimal.valueOf(this.price);
    BigDecimal dcBig = BigDecimal.valueOf(this.dc);
    BigDecimal hwdcBig = BigDecimal.valueOf(this.hwdc);

    BigDecimal a = priceBig.subtract(dcBig);
    BigDecimal b = a.multiply(BigDecimal.valueOf(this.qty));
    BigDecimal c = b.subtract(hwdcBig);

    log.info("c.intValue() ::: {}", c.intValue());
    log.info("this.amount ::: {}", this.amount);

    return c.intValue() == this.amount;
  }
}
