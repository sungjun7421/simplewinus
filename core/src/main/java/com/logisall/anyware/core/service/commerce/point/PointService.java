package com.logisall.anyware.core.service.commerce.point;

import com.logisall.anyware.core.domain.International.InterText;
import com.logisall.anyware.core.domain.commerce.point.Point;
import com.logisall.anyware.core.model.Filter;
import org.springframework.data.domain.Page;

import java.math.BigDecimal;
import java.util.Locale;

public interface PointService {

    // CD
    Point create(Point point);
    void delete(Long id);

    // R
    Point get(Locale locale, Long id);
    Page<Point> page(Locale locale, Filter filter, Long idBuyer, Point.Status status);

    // 포인트 지급
    void increase(BigDecimal point, InterText message, String email, Long idOrder);
    void increase(BigDecimal point, InterText message, String email);
    // 포인트 차감
    void decrease(BigDecimal point, InterText message, String email, Long idOrder);
    // 포인트 만료
    void expire(BigDecimal point, String email, Long idOriginPoint);

    // 보유포인트
    int total(String email);
    int total(Long idUser);
    // 만료 예정포인트
    int expireScheduledTotal(String email);
    int expireScheduledTotal(Long idUser);
}
