package com.logisall.anyware.core.service.commerce.point;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.point.Point;
import com.logisall.anyware.core.domain.commerce.point.PointPredicate;
import com.logisall.anyware.core.domain.commerce.point.PointRepository;
import com.logisall.anyware.core.domain.commerce.point.PointSetting;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.querydsl.commerce.QDPoint;
import com.logisall.anyware.core.service.commerce.buyer.BuyerService;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

@Slf4j
@Service
public class PointAdServiceImpl implements PointAdService {

    @Autowired
    private PointRepository pointRepository;

    @Autowired
    private BuyerService buyerService;

    @Autowired
    private PointSettingService pointSettingService;

    @Override
    @Transactional
    public com.logisall.anyware.core.domain.commerce.point.Point create(com.logisall.anyware.core.domain.commerce.point.Point point) {

        PointSetting setting = pointSettingService.get();
        log.debug("point setting ::: {}", setting);
        if (setting.isEnabled()) {

            if (point.getStatus() == null) {
                throw new com.logisall.anyware.core.config.exception.BadRequestException();
            }

            if (point.getRelativeBuyer() == null || point.getRelativeBuyer().getId() == null || point.getRelativeBuyer().getId() == 0L) {
                throw new com.logisall.anyware.core.config.exception.BadRequestException();
            }

            if (Objects.equals(point.getStatus(), com.logisall.anyware.core.domain.commerce.point.Point.Status.DECREASE)) {

                List<com.logisall.anyware.core.model.querydsl.commerce.QDPoint> list = pointRepository.listByAvailable(point.getRelativeBuyer().getId());

                int decreasePoint = point.getPoint().intValue();

                for (QDPoint qdPoint : list) {
                    log.debug("qdPoint ::: {}", qdPoint);
                    Long idPoint = qdPoint.getId();
                    int rest = qdPoint.getRest();

                    decreasePoint += rest;
                    if (decreasePoint >= 0) {
                        // 사용포인트 - 사용하고 남은포인트 = 양수 (사용포인트가 모두 차감되어 반복문에서 벗어나야 함)
                        if (decreasePoint > 0) {
                            int usedPoint = decreasePoint - rest;
                            log.debug("decreasePoint - rest ::: {} - {} = {}", decreasePoint, rest, usedPoint);
                            pointRepository.findById(idPoint)
                                    .ifPresent(updatePoint -> {
                                        updatePoint.setUsed(updatePoint.getUsed().add(BigDecimal.valueOf(usedPoint)));
                                        pointRepository.save(updatePoint);
                                    });
                        } else {
                            log.debug("decreasePoint, rest ::: {} {}", decreasePoint, rest);
                            pointRepository.findById(idPoint)
                                    .ifPresent(updatePoint -> {
                                        updatePoint.setUsed(updatePoint.getUsed().add(BigDecimal.valueOf(-1 * rest)));
                                        pointRepository.save(updatePoint);
                                    });
                        }
                        break;
                    } else {
                        // 사용포인트 - 사용하고 남은포인트 = 음수 (아직 사용포인트가 남아있는 경우)
                        log.debug("decreasePoint, rest ::: {} {}", decreasePoint, rest);
                        pointRepository.findById(idPoint)
                                .ifPresent(updatePoint -> {
                                    updatePoint.setUsed(updatePoint.getUsed().add(BigDecimal.valueOf(-1 * rest)));
                                    pointRepository.save(updatePoint);
                                });
                    }
                }

                log.debug("decreasePoint ::: {}", decreasePoint);
            } else if (Objects.equals(point.getStatus(), com.logisall.anyware.core.domain.commerce.point.Point.Status.INCREASE)) {

                log.debug("buyer ::: {}", point.getRelativeBuyer());
                log.debug("user ::: {}", point.getRelativeBuyer().getRelativeUser());
                point.setExpirationDate(LocalDate.now().plusYears(setting.getExpiration())); // 만료일 입력

                //save ExpirationDate of User
//                Buyer esBuyer = point.getRelativeBuyer();
//                if(esBuyer.getExpirationDate() == null){
//                    esBuyer.setExpirationDate(LocalDate.now().plusMonths(setting.getExpiration()));
//                }else{
//                    if(esBuyer.getExpirationDate().isBefore(LocalDate.now())){
//                        esBuyer.setExpirationDate(LocalDate.now().plusMonths(setting.getExpiration()));
//                    }else{
//                        esBuyer.setExpirationDate(esBuyer.getExpirationDate().plusMonths(setting.getExpiration()));
//                    }
//                }
//                esBuyerRepository.save(esBuyer);

                // 최대 적립 가능 포인트 계산
                int maxSavingPoint = setting.getMaxSavingPoint();
                if (maxSavingPoint < point.getPoint().intValue()) {
                    point.setPoint(BigDecimal.valueOf(maxSavingPoint));
                }
            }

            com.logisall.anyware.core.domain.commerce.point.Point createdPoint = pointRepository.save(point);
            log.debug("createdPoint ::: {}", createdPoint);
            return createdPoint;

        } else {
            return point;
        }
    }

    @Override
    @Transactional
    public void delete(Long id) {
        pointRepository.deleteById(id);
    }

    @Override
    @Transactional
    public com.logisall.anyware.core.domain.commerce.point.Point get(Locale locale, Long id) {
        return pointRepository.findById(id)
                .map(point -> {
                    point.lazy();
                    point.setLocale(locale);
                    return point;
                }).orElseThrow(BadRequestException::new);
    }

    @Override
    @Transactional
    public Page<com.logisall.anyware.core.domain.commerce.point.Point> page(Locale locale, Filter filter, Long idBuyer, com.logisall.anyware.core.domain.commerce.point.Point.Status status) {
        Page<Point> page = pointRepository.findAll(
                PointPredicate.getInstance()
                    .search(filter.getQuery())
                    .startDate(filter.getStartDate())
                    .endDate(filter.getEndDate())
                    .buyer(idBuyer)
                    .status(status)
                    .values(),
                filter.getPageable()
        );
        page.forEach(point -> {
            point.setLocale(locale);
        });
        return page;
    }

    @Override
    @Transactional
    public int total(String email) {

        if (com.logisall.anyware.core.utils.StringUtils.isEmpty(email)) {
            return 0;
        }

        com.logisall.anyware.core.domain.commerce.buyer.Buyer buyer = buyerService.get(email);
        if (buyer != null) {
            return pointRepository.total(buyer.getId());
        }

        return 0;
    }

    @Override
    @Transactional
    public int total(Long idUser) {
        return pointRepository.total(idUser);
    }

    @Override
    @Transactional
    public int expireScheduledTotal(String email) {

        if (StringUtils.isEmpty(email)) {
            return 0;
        }

        Buyer buyer = buyerService.get(email);
        if (buyer != null) {
            pointRepository.expireScheduledTotal(buyer.getId());
        }

        return 0;
    }

    @Override
    @Transactional
    public int expireScheduledTotal(Long idUser) {
        return pointRepository.expireScheduledTotal(idUser);
    }
}
