package com.logisall.anyware.core.domain.board.contact;


import com.logisall.anyware.core.config.security.CryptoAESConverter;
import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.RestEntityBody;
import com.logisall.anyware.core.domain.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Locale;

@Entity(name = "Contact")
@Getter
@Setter
@ToString
public class Contact extends AbstractEntity<Long>
        implements RestEntityBody<com.logisall.anyware.core.domain.board.contact.Contact> {

  private static final long serialVersionUID = 445831443233566901L;

  public static String[] IGNORE_PROPERTIES = {
      "id",
      "privacyAgree",
      "relativeUser"
  };

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @Lob
  @Column(columnDefinition = "TEXT", length = 65535)
  private String content;

  @Column(columnDefinition = "BIT(1) default 0")
  private boolean privacyAgree; // 개인정보 수집 동의

  // == 비로그인 데이터
  @Column
  @Convert(converter = CryptoAESConverter.class)
  private String fullName; // 성명 (비로그인)

  @Column
  @Convert(converter = CryptoAESConverter.class)
  private String mobile; // 휴대전화번호 (비로그인)

//  @Column(length = 5)
//  private String locale;

//  @Transient
//  private String countryCode;

//  public void setCountryCode(String countryCode) {
//    this.countryCode = countryCode;
//
//    AtomicReference<String> locale = new AtomicReference<>("");
//    if (StringUtils.isNotEmpty(countryCode)) {
//      PhoneNumberUtils.phoneNumbers().forEach(phoneNumber -> {
//        if (("" + phoneNumber.getCode()).equals(countryCode))
//          LocaleUtils.countries().forEach(country -> {
//            if (country.getDisplayName().equals(phoneNumber.getDisplayName())) {
//              locale.set(country.getCode().toUpperCase());
//            }
//          });
//      });
//
//    }
//
//    if (StringUtils.isNotEmpty(this.getCountryCode())) {
//
//      String _locale = locale.get();
//      if (StringUtils.isNotEmpty(_locale)) {
//
//        Locale lc = new Locale("", locale.get());
//
//        if (StringUtils.isNotEmpty(lc.getCountry())) {
//          this.locale = lc.getCountry().toUpperCase();
//        }
//      }
//    }
//  }
//
//  public String getMobileE164() {
//    if (StringUtils.isNotEmpty(locale) && StringUtils.isNotEmpty(mobile)) {
//      int code = PhoneNumberUtils.phoneNumber(locale);
//      if (code != 0) {
//        return PhoneNumberUtils.convertByFormatE164("" + code, mobile);
//      }
//    }
//    if (StringUtils.isEmpty(locale)) {
//      return "국가코드가 없습니다.";
//    }
//    return "휴대폰번호가 없습니디.";
//  }


  @Column
  @Convert(converter = CryptoAESConverter.class)
  private String email; // 이메일 (비로그인)

  // == 로그인 데이터
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "idUser", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_User_For_Contact"))
  // Column name, 참조하는 ID(pk) name
  private User relativeUser; // 작성자 (로그인)

  // == 관리자
  @Column(columnDefinition = "BIT(1) default 1")
  private boolean complete; // 답변완료/답변미완료

  @Lob
  @Column(columnDefinition = "TEXT", length = 65535)
  private String memo;

  @Column(length = 20)
  private String ipAddress;

  @Override
  public void delete() {

  }

  @Override
  public void lazy() {
    if (this.relativeUser != null) {
      this.relativeUser.getId();
    }
  }

  @Override
  public com.logisall.anyware.core.domain.board.contact.Contact toBody(Locale locale) {
    return null;
  }
}

