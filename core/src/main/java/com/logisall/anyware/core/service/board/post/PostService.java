package com.logisall.anyware.core.service.board.post;

import com.logisall.anyware.core.domain.board.post.Post;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.admin.PostInfoResBody;
import com.logisall.anyware.core.service.DomainService;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Locale;

public interface PostService extends DomainService<com.logisall.anyware.core.domain.board.post.Post, Long> {

    Page<com.logisall.anyware.core.domain.board.post.Post> page(Locale locale, com.logisall.anyware.core.model.Filter filter, com.logisall.anyware.core.domain.board.post.Post.Type type);
    Page<com.logisall.anyware.core.domain.board.post.Post> page(Locale locale, com.logisall.anyware.core.model.Filter filter, com.logisall.anyware.core.domain.board.post.Post.Type type, Boolean isActive);
    Page<com.logisall.anyware.core.domain.board.post.Post> page(Locale locale, Filter filter, Post.Type type, Boolean isActive, Long idCategory);

    List<PostInfoResBody> list(String query);
}
