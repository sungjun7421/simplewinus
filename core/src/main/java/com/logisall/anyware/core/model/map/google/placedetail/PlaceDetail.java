package com.logisall.anyware.core.model.map.google.placedetail;

import com.logisall.anyware.core.model.map.google.placedetail.result.Result;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Slf4j
@Getter
@Setter
@ToString
public class PlaceDetail implements Serializable {

    private static final long serialVersionUID = -7007138481993069154L;

    public enum Status {
        OK("오류가 발생되지 않았음을 나타냅니다. 장소가 성공적으로 감지되었고 최소 한 개 이상의 결과가 반환되었습니다."),
        UNKNOWN_ERROR("서버 측 오류를 나타냅니다. 다시 시도하면 성공할 수도 있습니다."),
        ZERO_RESULTS("참조가 올바르지만, 더 이상 올바른 결과를 참조하지 않음을 나타냅니다. 사업체가 더 이상 운영되지 않는 경우에 발생할 수 있습니다."),
        OVER_QUERY_LIMIT("할당량이 초과되었음을 나타냅니다."),
        REQUEST_DENIED("요청이 거부되었음을 나타냅니다. 일반적인 원인은 key 매개변수가 잘못되었기 때문입니다."),
        INVALID_REQUEST("일반적으로 쿼리(reference)가 누락되었음을 나타냅니다."),
        NOT_FOUND("참조된 위치를 장소 데이터베이스에서 찾지 못했음을 나타냅니다.");

        @Getter
        final String value;

        Status(final String value) {
            this.value = value;
        }
    }

    private Long id;
    private Status status;
    private Result result;

}
