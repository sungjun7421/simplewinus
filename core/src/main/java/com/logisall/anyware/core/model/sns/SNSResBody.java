package com.logisall.anyware.core.model.sns;//package com.logisall.anyware.core.model.sns;
//
//import com.logisall.anyware.core.config.serializer.JsonLocalDateDeserializer;
//import com.logisall.anyware.core.config.serializer.JsonLocalDateSerializer;
//import com.logisall.anyware.core.model.BaseResponseBody;
//import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
//import com.fasterxml.jackson.databind.annotation.JsonSerialize;
//import lombok.Getter;
//import lombok.Setter;
//import lombok.ToString;
//
//import java.time.LocalDate;
//
//@Getter
//@Setter
//@ToString
//public class SNSResBody extends BaseResponseBody {
//
//    private static final long serialVersionUID = 5181952956419948965L;
//    public static final String SUCCESS = "success";
//    public static final String FAIL = "fail";
//
//    private String access_token;
//    private String redirectUri;
//    private String result = SUCCESS;
//
//    // SNS 정보
//    private SNSType snsType;
//    private String id;
//    private String name;
//    private String email;
//    private boolean verifiedEmail;
//
//    @JsonSerialize(using = JsonLocalDateSerializer.class)
//    @JsonDeserialize(using = JsonLocalDateDeserializer.class)
//    private LocalDate birth; // 생년월일
//
//    public static SNSResBody of(boolean result) {
//        SNSResBody body = new SNSResBody();
//        if (result) {
//            body.setResult(SNSResBody.SUCCESS);
//        } else {
//            body.setResult(SNSResBody.FAIL);
//        }
//        return body;
//    }
//}
