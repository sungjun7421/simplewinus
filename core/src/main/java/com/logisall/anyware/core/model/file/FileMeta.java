package com.logisall.anyware.core.model.file;

import com.logisall.anyware.core.domain.file.FileEntity;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.net.URL;

@Setter
@Getter
@ToString
public class FileMeta implements Serializable {

  private static final long serialVersionUID = -3770830091024680811L;

  private Long id;
  private URL url;
  private String originalFilename;
  private String filename;
  private String mimeType;
  private long size;

  public com.logisall.anyware.core.domain.file.FileEntity toFileEntity() {
    com.logisall.anyware.core.domain.file.FileEntity fileEntity = new FileEntity();
    fileEntity.setUrl(StringUtils.isNotEmpty(this.url) ? this.url.toString() : null);
    fileEntity.setFilename(this.filename);
    fileEntity.setOriginalFilename(this.originalFilename);
    fileEntity.setMimeType(this.mimeType);
    fileEntity.setSize(this.size);
    return fileEntity;
  }
}

