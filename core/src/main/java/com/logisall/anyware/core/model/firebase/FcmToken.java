package com.logisall.anyware.core.model.firebase;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class FcmToken implements Serializable {

  public enum Code {
    AND,
    IOS
  }

  private String token;
  private Code code;
}
