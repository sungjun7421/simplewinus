package com.logisall.anyware.core.model.sns.apple;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class AppsResponse implements Serializable {

  private static final long serialVersionUID = -5036745645897780884L;
  private String payload;
}
