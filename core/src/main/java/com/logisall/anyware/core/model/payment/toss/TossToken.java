package com.logisall.anyware.core.model.payment.toss;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class TossToken implements Serializable {

    private static final long serialVersionUID = 4942935274119263226L;

    private String apiKey;
    private String payToken;
}
