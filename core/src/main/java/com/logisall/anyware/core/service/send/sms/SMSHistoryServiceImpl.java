package com.logisall.anyware.core.service.send.sms;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.sms.SMSHistory;
import com.logisall.anyware.core.domain.sms.SMSHistoryPredicate;
import com.logisall.anyware.core.domain.sms.SMSHistoryRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.account.user.UserService;
import com.logisall.anyware.core.service.send.sms.SMSHistoryService;
import com.logisall.anyware.core.service.setting.AppSettingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

@Slf4j
@Service
public class SMSHistoryServiceImpl implements SMSHistoryService {

  @Autowired
  private SMSHistoryRepository smsHistoryRepository;

  @Autowired
  private AppSettingService appSettingService;

  @Autowired
  private PagedResourcesAssembler pagedResourcesAssembler;

  @Autowired
  private UserService userService;

  @Override
  @Transactional
  public SMSHistory create(SMSHistory history) {
    return smsHistoryRepository.save(history);
  }

  @Override
  @Transactional
  public SMSHistory update(SMSHistory history) {
    if (history.getId() == null) {
      throw new BadRequestException();
    }

    return smsHistoryRepository.findById(history.getId())
      .map(ori -> {
        BeanUtils.copyProperties(history, ori, SMSHistory.IGNORE_PROPERTIES);
        return smsHistoryRepository.save(ori);
      }).orElseThrow(BadRequestException::new);
  }

  @Override
  @Transactional(readOnly = true)
  public SMSHistory get(Locale locale, Long id) {
    return smsHistoryRepository.findById(id)
      .map(history -> {
        history.lazy();
        return history;
      }).orElseThrow(BadRequestException::new);
  }

  @Override
  @Transactional
  public void delete(Long id) {
    smsHistoryRepository.deleteById(id);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<SMSHistory> page(Locale locale, Filter filter) {

    Page<SMSHistory> page = smsHistoryRepository.findAll(
        SMSHistoryPredicate.getInstance()
            .search(filter.getQuery())
            .startDate(filter.getStartDate())
            .endDate(filter.getEndDate())
            .values(),
        filter.getPageable());

    page.forEach(SMSHistory::lazy);
    return page;
  }

  @Override
  @Transactional(readOnly = true)
  public Page<SMSHistory> page(Filter filter, SMSHistory.Code code) {
    Page<SMSHistory> page = smsHistoryRepository.findAll(
            SMSHistoryPredicate.getInstance()
                    .search(filter.getQuery())
                    .startDate(filter.getStartDate())
                    .endDate(filter.getEndDate())
                    .code(code)
                    .values(),
      filter.getPageable());

    page.forEach(SMSHistory::lazy);
    return page;
  }
}
