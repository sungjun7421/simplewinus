package com.logisall.anyware.core.service.account.user;

import com.logisall.anyware.core.config.database.PwdEncConfig;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.buyer.BuyerRepository;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevelRepository;
import com.logisall.anyware.core.domain.user.*;
import com.logisall.anyware.core.model.Filter;

import com.logisall.anyware.core.service.account.user.UserService;
import com.logisall.anyware.core.utils.StringUtils;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

  @Value("${core.privacy.expired.password}")
  private Integer pwdExpiredPeriod;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private AuthorityRepository authorityRepository;

  @Autowired
  private PwdEncConfig pwdEncConfig;

  @Autowired
  private BuyerLevelRepository buyerLevelRepository;

  @Autowired
  private BuyerRepository buyerRepository;

  @Override
  @Transactional
  public User create(User user) {

    if (user == null) {
      throw new BadRequestException("User가 입력되지 않았습니다.");
    }

    if (StringUtils.isEmpty(user.getPassword())) {
      throw new BadRequestException("비밀번호가 입력되지 않았습니다.");
    }

    if (StringUtils.isEmpty(user.getImage())) {
      user.setImage(User.DEFAULT_PROFILE_IMAGE);
    }
    log.debug(">>> {}", pwdEncConfig.getPasswordEncoder());
    user.setPassword(pwdEncConfig.getPasswordEncoder().encode(user.getPassword()));

    // verification
    user.setVerification(new Verification());
    user.getVerification().setEmail(true);
    user.getVerification().setMobile(true);

    // add role
    user.setUserAuthorities(authorityRepository); // 사용자 권한 주기

    Buyer buyer = new Buyer();
    BuyerLevel buyerLevel = buyerLevelRepository.findByLevel(1);
    buyer.setRelativeBuyerLevel(buyerLevel);
    buyer.setRelativeUser(user);
    user.setRelativeBuyer(buyer);
    buyerRepository.save(buyer);

    return userRepository.save(user);
  }

  @Override
  @Transactional
  public User update(User user) {

    if (user.getId() == null) {
      throw new BadRequestException();
    }

    return userRepository.findById(user.getId())
        .map(ori -> {
          user.setUserAuthorities(authorityRepository); // 사용자 권한 주기
          user.keepEmbeddedInformation(ori);

          BeanUtils.copyProperties(user, ori, User.IGNORE_PROPERTIES);
          return userRepository.save(ori);
        }).orElseThrow(BadRequestException::new);
  }

  @Override
  @Transactional(readOnly = true)
  public User get(Locale locale, Long id) {
    return this.get(id);
  }

  @Override
  @Transactional(readOnly = true)
  public User get(Long id) {
    return userRepository.findById(id)
        .map(user -> {
          user.lazy();
          return user;
        }).orElse(null);
  }

  @Override
  @Transactional(readOnly = true)
  public User get(String email) {
    return userRepository.findByEmail(email)
        .map(user -> {
          user.lazy();
          return user;
        }).orElse(null);
  }

  @Override
  @Transactional
  @PreAuthorize("hasRole('ROLE_SUPER')")
  public void delete(Long id) {
    userRepository.findById(id)
        .ifPresent(user -> userRepository.delete(user));
  }

  @Override
  @Transactional(readOnly = true)
  public List<User> list(Filter filter) {

    return Lists.newArrayList(userRepository.findAll(
        UserPredicate.getInstance()
            .startDate(filter.getStartDate())
            .endDate(filter.getEndDate())
            .values()
    ));
  }

  @Override
  @Transactional(readOnly = true)
  public List<User> list(Locale locale) {
    List<User> list = Lists.newArrayList(userRepository.findAll(
        UserPredicate.getInstance()
            .role(Authority.Role.MANAGER)
            .values()));
    list.forEach(user -> {
//      user.setLocale(locale);
    });
    return list;
  }

  @Override
  @Transactional(readOnly = true)
  public Page<User> page(Filter filter, Authority.Role role) {

    return userRepository.findAll(
        UserPredicate.getInstance()
            .search(filter.getQuery())
            .startDate(filter.getStartDate())
            .endDate(filter.getEndDate())
            .role(role)
            .roles(role == null ? Lists.newArrayList(Authority.Role.USER) : null)
            .values()
        ,
        filter.getPageable());
  }

  @Override
  @Transactional(readOnly = true)
  public Page<User> page(Locale locale, Filter filter) {
    return this.page(filter, Authority.Role.USER);
  }

  @Override
  @Transactional(readOnly = true)
  public Authority authority(Authority.Role role) {
    return authorityRepository.findByRole(role);
  }

  @Override
  @Transactional
  public void resetPassword(Long id, String password) {
    userRepository.findById(id)
        .ifPresent(user -> {
          user.setPassword(pwdEncConfig.getPasswordEncoder().encode(password));
          user.getUserDetailsMeta().setUpdatedPasswordDateTime(LocalDateTime.now());
        });
  }


  @Override
  @Transactional
  public void leave(Long id, String reason) {
    userRepository.findById(id)
        .ifPresent(user -> {
          user.getUserDetailsMeta().setEnabled(false);
          user.getLeaveMeta().setLeave(true);
          user.getLeaveMeta().setLeaveTime(LocalDateTime.now());
          user.getLeaveMeta().setLeaveReason(reason);

          user.setSocialId(null); // SNS 연동끊기
        });
  }

  @Override
  @Transactional
  public void removePrivacy(Long id) {
    userRepository.findById(id)
        .ifPresent(user -> {
          if (!user.getUserDetailsMeta().isEnabled()
              && user.getLeaveMeta().isLeave()) {

            String disableKeyword = "leave" + id;
            user.setEmail(disableKeyword);
            user.setMobile(disableKeyword);
            user.setFullName(disableKeyword);
            user.setImage(null);
            user.setPassword(null);
//            user.setBirth(null);
            user.setSocialId(null);
            user.setAuthorities(null);
            user.getUserDetailsMeta().setEnabled(false);
            user.getLeaveMeta().setLeave(true);
            user.getLeaveMeta().setRemovePrivacyTime(LocalDateTime.now());
          }
        });
  }


}
