package com.logisall.anyware.core.model.resbody.document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.logisall.anyware.core.config.serializer.JsonLocalDateDeserializer;
import com.logisall.anyware.core.config.serializer.JsonLocalDateSerializer;
import com.logisall.anyware.core.domain.document.Document;
import com.logisall.anyware.core.model.BaseResponseBody;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.time.LocalDate;


@Relation(value = "document", collectionRelation = "documents")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DocumentResBody extends BaseResponseBody {

  private static final long serialVersionUID = 6623011747570416708L;

  private Long id;
  private Document.Type type;
  private String title; // 제목
  private String content;

  @JsonSerialize(using = JsonLocalDateSerializer.class)
  @JsonDeserialize(using = JsonLocalDateDeserializer.class)
  private LocalDate executeDate; // 시행일
}
