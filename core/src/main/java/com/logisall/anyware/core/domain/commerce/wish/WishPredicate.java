package com.logisall.anyware.core.domain.commerce.wish;

import com.logisall.anyware.core.utils.StringUtils;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor(staticName = "getInstance")
public class WishPredicate {
  private static final QWish Q_WISH = QWish.wish;
  private BooleanBuilder builder = new BooleanBuilder();

  public Predicate values() {
    return builder.getValue() == null ? builder.and(Q_WISH.id.isNotNull()) : builder.getValue();
  }

  public com.logisall.anyware.core.domain.commerce.wish.WishPredicate userId(Long userId) {
    if (userId != null) {
      builder.and(Q_WISH.id.idBuyer.eq(userId));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.wish.WishPredicate productId(Long productId) {
    if (productId != null) {
      builder.and(Q_WISH.id.idProduct.eq(productId));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.wish.WishPredicate query(String query) {
    if (query != null && StringUtils.isNotEmpty(query.trim())) {
      builder.and(
          Q_WISH.relativeProduct.information.name.textEnUs.containsIgnoreCase(query)
              .or(Q_WISH.relativeProduct.information.name.textJaJp.containsIgnoreCase(query))
              .or(Q_WISH.relativeProduct.information.name.textKoKr.containsIgnoreCase(query))
              .or(Q_WISH.relativeProduct.information.name.textZhCn.containsIgnoreCase(query))
              .or(Q_WISH.relativeProduct.information.name.textZhTw.containsIgnoreCase(query))
              .or(Q_WISH.relativeBuyer.relativeUser.fullName.containsIgnoreCase(query))
              .or(Q_WISH.relativeBuyer.relativeUser.email.containsIgnoreCase(query))
      );
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.wish.WishPredicate startDate(final LocalDateTime startDate) {

    if (startDate != null) {
      builder.and(Q_WISH.createdDate.goe(startDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.wish.WishPredicate endDate(final LocalDateTime endDate) {

    if (endDate != null) {
      builder.and(Q_WISH.createdDate.loe(endDate));
    }
    return this;
  }
}
