package com.logisall.anyware.core.domain.board.comment;

import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.RestEntityBody;
import com.logisall.anyware.core.domain.board.event.Event;
import com.logisall.anyware.core.domain.board.post.Post;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.model.resbody.board.CommentResBody;
import com.logisall.anyware.core.utils.HtmlUtils;
import com.logisall.anyware.core.utils.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * 댓글 테이블
 */
@Entity
@Getter
@Setter
@ToString(exclude = {"relativeUser", "relativePost", "relativeEvent", "relativeParent"})
public class Comment extends AbstractEntity<Long>
        implements RestEntityBody<CommentResBody> {

    private static final long serialVersionUID = -4955881323202878008L;

    public static String[] IGNORE_PROPERTIES = {
            "id",
            "relativeUser",
            "relativePost",
            "relativeEvent",
            "relativeParent"
    };

    @Getter
    public enum Type {
        EVENT("Event"),
        POST("Post");

        private final String value;

        Type(final String value) {
            this.value = value;
        }
    }


    @PrePersist
    public void prePersist() {
        if (StringUtils.isNotEmpty(this.content))
            this.content = HtmlUtils.convertLineSeparatorToBrTag(this.content);
    }

    @PreUpdate
    public void preUpdate() {
        if (StringUtils.isNotEmpty(this.content))
            this.content = HtmlUtils.convertLineSeparatorToBrTag(this.content);
    }

    @Id
    @GeneratedValue
    @Column(unique = true)
    private Long id;

    @Lob
    @Column(columnDefinition = "TEXT", length = 65535)
    private String content;

    @Enumerated
    @Column(columnDefinition = "TINYINT(1) default 0")
    private Type type;

    @Column(columnDefinition = "BIT(1) default 1")
    private boolean active; // 활성/비활성

    @Column(name = "deleteRow", columnDefinition = "BIT(1) default 0")
    private boolean delete; // 댓글삭제

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "idParent", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Reply_For_Comment"))
    private com.logisall.anyware.core.domain.board.comment.Comment relativeParent;

    @JsonIgnore
    @OneToMany(mappedBy = "relativeParent", cascade = CascadeType.REMOVE)
    private List<com.logisall.anyware.core.domain.board.comment.Comment> comments = new ArrayList<>();

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "idUser", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_User_For_Comment"))
    private User relativeUser;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "idPost", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Post_For_Comment"))
    private Post relativePost;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "idEvent", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Event_For_Comment"))
    private Event relativeEvent;

    @Override
    public void delete() {
        if (this.relativeUser != null)
            this.relativeUser = null;
        if (this.relativePost != null)
            this.relativePost = null;
        if (this.relativeEvent != null)
            this.relativeEvent = null;
    }

    @Override
    public void lazy() {
        if (this.relativeUser != null)
            this.relativeUser.getId();
        if (this.relativePost != null)
            this.relativePost.getId();
        if (this.relativeEvent != null)
            this.relativeEvent.getId();
    }

    @Override
    public CommentResBody toBody(Locale locale) {

        return CommentResBody.builder()
                .id(this.getId())
                .content(this.isDelete() ? "삭제된 댓글입니다." : this.getContent())
                .delete(this.isDelete())
                .profileImage(this.getRelativeUser() != null ? this.getRelativeUser().getImage() : "")
                .locale(locale)
                .build();
    }
}
