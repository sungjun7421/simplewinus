package com.logisall.anyware.core.config.security;

import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
public class LoginUserDetailsService implements UserDetailsService {

  private final com.logisall.anyware.core.domain.user.UserRepository userRepository;

  public LoginUserDetailsService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

    try {
      log.debug(">>>> loadUserByUsername ::: {}", email);

      return userRepository.findByEmail(email).map(User::getUserDetails).orElseThrow(RuntimeException::new);
    } catch (NullPointerException e) {
      throw new UsernameNotFoundException(null);
    }
  }
}
