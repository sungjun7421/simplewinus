package com.logisall.anyware.core.service.map.juso;

import com.google.gson.Gson;
import com.logisall.anyware.core.model.map.juso.Juso;
import com.logisall.anyware.core.model.map.juso.JusoEntity;
import com.logisall.anyware.core.model.map.juso.JusoEntityCommon;
import com.logisall.anyware.core.model.property.MetaPlugin;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

@Slf4j
@Service
public class JusoServiceImpl implements JusoService {

  @Autowired
  private MetaPlugin metaPlugin;

  @Autowired
  private OkHttpClient client;

  @Override
  public Future<com.logisall.anyware.core.model.map.juso.JusoEntity> geJusoInfo(String keyword, int currentPage) throws InterruptedException {
    return this.geJusoInfo(keyword, currentPage, 10);
  }

  @Async
  @Override
  public Future<com.logisall.anyware.core.model.map.juso.JusoEntity> geJusoInfo(String keyword, int currentPage, int countPerPage) throws InterruptedException {

    log.debug("meta key ::: {}", metaPlugin.getJusoKey());

    if (StringUtils.isEmpty(keyword)) {
      throw new NullPointerException("키워드를 입력하세요.");
    }

    URI uri = UriComponentsBuilder.newInstance().scheme("http").host("www.juso.go.kr")
        .path("/addrlink/addrLinkApi.do")
        .queryParam("currentPage", currentPage)
        .queryParam("countPerPage", countPerPage)
        .queryParam("keyword", keyword)
        .queryParam("confmKey", metaPlugin.getJusoKey())
        .queryParam("resultType", "json")
        .build()
        .toUri();

    try {
      Request request = new Request.Builder()
          .url(uri.toURL())
          .get()
          .build();

      Response response = null;
      String result = null;

      response = client.newCall(request).execute();

      if(response.code() == 200) {

        result = response.body().string();
        Map<String, Object> responseMap =  new Gson().fromJson(result, Map.class);

        com.logisall.anyware.core.model.map.juso.JusoEntity jusoEntity = new JusoEntity();

        Map<String, Object> results = (Map<String, Object>) responseMap.get("results");
        Map<String, Object> common = (Map<String, Object>) results.get("common");

        jusoEntity.setCommon(new com.logisall.anyware.core.model.map.juso.JusoEntityCommon(common));

        if (common.get("errorCode").toString().equals(JusoEntityCommon.ERROR_CODE_SUCCESS)) {

          List<Map<String, String>> jusoMapList = (List<Map<String, String>>) results.get("juso");
          List<com.logisall.anyware.core.model.map.juso.Juso> jusos = new ArrayList<>();
          jusoMapList.forEach(jusoMap -> {

            com.logisall.anyware.core.model.map.juso.Juso juso = new Juso();
            juso.setRoadAddrPart1(jusoMap.get("roadAddrPart1"));
            juso.setRoadAddrPart2(jusoMap.get("roadAddrPart2"));
            juso.setJibunAddr(jusoMap.get("jibunAddr"));
            juso.setEngAddr(jusoMap.get("engAddr"));
            juso.setZipNo(jusoMap.get("zipNo"));
            juso.setAdmCd(jusoMap.get("admCd"));
            juso.setDetBdNmList(jusoMap.get("detBdNmList"));
            juso.setBdNm(jusoMap.get("bdNm"));
            juso.setSiNm(jusoMap.get("siNm"));
            juso.setSggNm(jusoMap.get("sggNm"));
            juso.setEmdNm(jusoMap.get("emdNm"));
            juso.setLiNm(jusoMap.get("liNm"));
            juso.setRn(jusoMap.get("rn"));
            juso.setBuldMnnm(jusoMap.get("buldMnnm"));
            juso.setBuldSlno(jusoMap.get("buldSlno"));

            jusos.add(juso);
          });
          jusoEntity.setJusos(jusos);
        }
        return new AsyncResult<>(jusoEntity);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    throw new RuntimeException("API 문제 발생");
  }
}
