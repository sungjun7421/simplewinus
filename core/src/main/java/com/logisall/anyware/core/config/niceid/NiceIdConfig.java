package com.logisall.anyware.core.config.niceid;

import NiceID.Check.CPClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NiceIdConfig {

  @Bean
  public CPClient getCPClient() {
    return new CPClient();
  }
}
