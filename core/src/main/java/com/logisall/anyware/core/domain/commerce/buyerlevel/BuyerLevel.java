package com.logisall.anyware.core.domain.commerce.buyerlevel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logisall.anyware.core.domain.AbstractEntityInternational;
import com.logisall.anyware.core.domain.International.InterText;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.coupon.Coupon;
import com.logisall.anyware.core.model.resbody.commerce.level.BuyerLevelResBody;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * 회원등급
 */
@Slf4j
@Entity(name = "BuyerLevel")
@Getter
@Setter
@ToString(exclude = {"buyers", "coupons"})
public class BuyerLevel extends AbstractEntityInternational<Long> {

  private static final long serialVersionUID = 2393686980157468618L;

  public static final String[] IGNORE_PROPERTIES = {
      "id",
      "buyers",
      "coupons"
  };

  @PrePersist
  public void prePersist() {
  }

  @PreUpdate
  public void PreUpdate() {
  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @Embedded
  @AttributeOverrides({
          @AttributeOverride(name = "textKoKr", column = @Column(name = "nameKoKr", length = 100)),
          @AttributeOverride(name = "textEnUs", column = @Column(name = "nameEnUs", length = 100)),
          @AttributeOverride(name = "textJaJp", column = @Column(name = "nameJaJp", length = 100)),
          @AttributeOverride(name = "textZhCn", column = @Column(name = "nameZhCn", length = 100)),
          @AttributeOverride(name = "textZhTw", column = @Column(name = "nameZhTw", length = 100))
  })
  private com.logisall.anyware.core.domain.International.InterText name; // 명칭

  @Embedded
  @AttributeOverrides({
          @AttributeOverride(name = "textKoKr", column = @Column(name = "descriptionKoKr", columnDefinition = "TEXT")),
          @AttributeOverride(name = "textEnUs", column = @Column(name = "descriptionEnUs", columnDefinition = "TEXT")),
          @AttributeOverride(name = "textJaJp", column = @Column(name = "descriptionJaJp", columnDefinition = "TEXT")),
          @AttributeOverride(name = "textZhCn", column = @Column(name = "descriptionZhCn", columnDefinition = "TEXT")),
          @AttributeOverride(name = "textZhTw", column = @Column(name = "descriptionZhTw", columnDefinition = "TEXT"))
  })
  private InterText description; // 설명

  @Column
  private String image;

  @Column(columnDefinition = "INT(2) default 0", unique = true)
  private int level; // 등급우선순위 (내림차순, descending) 높을수록 혜택이 많음

  @Column(columnDefinition = "DECIMAL(19,2) default 0")
  private BigDecimal saveRate;

  @Column
  private String discountExpression; // 할인 수식

  @Column
  private String birthSavingExpression; // 생일월 적립 수식

  @JsonIgnore
  @Transient
  private boolean checked;

  // === CUSTOM RELATIVE ===
  @JsonIgnore
  @OrderBy("createdDate DESC")
  @OneToMany(mappedBy = "relativeBuyerLevel", cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, orphanRemoval = true)
  private List<Buyer> buyers = new ArrayList<>();

  @JsonIgnore
  @OrderBy("createdDate desc")
  @ManyToMany(mappedBy = "buyerLevels")
  private List<Coupon> coupons = new ArrayList<>();

  @Override
  public void setLocale(Locale locale) {
    super.setLocale(locale);
    if (this.name != null)
      this.name.setLocale(this.locale);
    if (this.description != null)
      this.description.setLocale(this.locale);
  }

  @Override
  public void delete() {
    if (this.buyers != null) {
      this.buyers.forEach(buyer -> buyer.setRelativeBuyerLevel(null));
    }
  }

  @Override
  public void lazy() {

  }

  public com.logisall.anyware.core.model.resbody.commerce.level.BuyerLevelResBody toBuyerLevelResBody(Locale locale) {
    this.setLocale(locale);
    return BuyerLevelResBody.builder()
        .level(this.level)
        .name(this.name.getValue())
        .description(this.description.getValue())
        .pointRate(this.saveRate)
        .build();
  }


}
