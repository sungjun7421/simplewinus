/*
 * Copyright (c) 2015-2015 Amedia Utvikling AS.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.logisall.anyware.core.config.freemarker;

import com.logisall.anyware.core.config.freemarker.time.ZoneOffsetAdapter;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.Version;

import java.time.*;

/**
 * Freemarker ObjectWrapper that extends the DefaultObjectWrapper with support for all classes in the new java.time api.
 */
public class Java8ObjectWrapper extends DefaultObjectWrapper {

    public Java8ObjectWrapper(Version incompatibleImprovements) {
        super(incompatibleImprovements);
    }

    @Override
    protected TemplateModel handleUnknownType(Object obj) throws TemplateModelException {
        if (obj instanceof Clock) {
            return new com.logisall.anyware.core.config.freemarker.time.ClockAdapter((Clock) obj);
        } else if (obj instanceof Duration) {
            return new com.logisall.anyware.core.config.freemarker.time.DurationAdapter((Duration) obj);
        } else if (obj instanceof Instant) {
            return new com.logisall.anyware.core.config.freemarker.time.InstantAdapter((Instant) obj);
        } else if (obj instanceof LocalDate) {
            return new com.logisall.anyware.core.config.freemarker.time.LocalDateAdapter((LocalDate) obj);
        } else if (obj instanceof LocalDateTime) {
            return new com.logisall.anyware.core.config.freemarker.time.LocalDateTimeAdapter((LocalDateTime) obj);
        } else if (obj instanceof LocalTime) {
            return new com.logisall.anyware.core.config.freemarker.time.LocalTimeAdapter((LocalTime) obj);
        } else if (obj instanceof MonthDay) {
            return new com.logisall.anyware.core.config.freemarker.time.MonthDayAdapter((MonthDay) obj);
        } else if (obj instanceof OffsetDateTime) {
            return new com.logisall.anyware.core.config.freemarker.time.OffsetDateTimeAdapter((OffsetDateTime) obj);
        } else if (obj instanceof OffsetTime) {
            return new com.logisall.anyware.core.config.freemarker.time.OffsetTimeAdapter((OffsetTime) obj);
        } else if (obj instanceof Period) {
            return new com.logisall.anyware.core.config.freemarker.time.PeriodAdapter((Period) obj);
        } else if (obj instanceof Year) {
            return new com.logisall.anyware.core.config.freemarker.time.YearAdapter((Year) obj);
        } else if (obj instanceof YearMonth) {
            return new com.logisall.anyware.core.config.freemarker.time.YearMonthAdapter((YearMonth) obj);
        } else if (obj instanceof ZonedDateTime) {
            return new com.logisall.anyware.core.config.freemarker.time.ZonedDateTimeAdapter((ZonedDateTime) obj);
        } else if (obj instanceof ZoneId) {
            return new com.logisall.anyware.core.config.freemarker.time.ZoneIdAdapter((ZoneId) obj);
        } else if (obj instanceof ZoneOffset) {
            return new ZoneOffsetAdapter((ZoneOffset) obj);
        }
        return super.handleUnknownType(obj);
    }
}
