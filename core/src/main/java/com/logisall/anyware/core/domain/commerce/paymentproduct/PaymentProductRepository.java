package com.logisall.anyware.core.domain.commerce.paymentproduct;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface PaymentProductRepository extends
    JpaRepository<com.logisall.anyware.core.domain.commerce.paymentproduct.PaymentProduct, Long>,
    QuerydslPredicateExecutor<PaymentProduct> {

}
