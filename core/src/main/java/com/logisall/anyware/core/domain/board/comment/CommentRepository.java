package com.logisall.anyware.core.domain.board.comment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface CommentRepository extends
        JpaRepository<com.logisall.anyware.core.domain.board.comment.Comment, Long>,
        QuerydslPredicateExecutor<Comment>,
    CommentRepositoryCustom {
}

