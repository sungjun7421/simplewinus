package com.logisall.anyware.core.model.firebase;

import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class NotificationRequest implements Serializable {

  private static final long serialVersionUID = -8739766424900588075L;

  private String token;
  private String title;
  private String message;
}
