package com.logisall.anyware.core.model.reqbody.account;

import com.logisall.anyware.core.model.BaseRequestBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;

@Schema(description = "이메일 인증")
@Setter
@Getter
@ToString
public class CertEmailReqBody extends BaseRequestBody {

    private static final long serialVersionUID = 5984533067166825135L;

    @Email
    @Schema(description = "이메일 주소", example = "test@logisall.com", maxLength = 255)
    private String email;
}
