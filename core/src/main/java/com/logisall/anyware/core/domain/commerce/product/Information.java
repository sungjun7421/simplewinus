package com.logisall.anyware.core.domain.commerce.product;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logisall.anyware.core.domain.International.InterText;
import com.logisall.anyware.core.domain.file.FileEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;

@Embeddable
@Setter
@Getter
@ToString
public class Information implements Serializable {

  private static final long serialVersionUID = 1303593186538631471L;

  @Column
  private String thumbnail; // 썸네일 이미지

  @Embedded
  @AttributeOverrides({
      @AttributeOverride(name = "textKoKr", column = @Column(name = "nameKoKr", length = 30)),
      @AttributeOverride(name = "textEnUs", column = @Column(name = "nameEnUs", length = 30)),
      @AttributeOverride(name = "textJaJp", column = @Column(name = "nameJaJp", length = 30)),
      @AttributeOverride(name = "textZhCn", column = @Column(name = "nameZhCn", length = 30)),
      @AttributeOverride(name = "textZhTw", column = @Column(name = "nameZhTw", length = 30))
  })
  private com.logisall.anyware.core.domain.International.InterText name; // 상품

  @Embedded
  @AttributeOverrides({
      @AttributeOverride(name = "textKoKr", column = @Column(name = "subtitleKoKr")),
      @AttributeOverride(name = "textEnUs", column = @Column(name = "subtitleEnUs")),
      @AttributeOverride(name = "textJaJp", column = @Column(name = "subtitleJaJp")),
      @AttributeOverride(name = "textZhCn", column = @Column(name = "subtitleZhCn")),
      @AttributeOverride(name = "textZhTw", column = @Column(name = "subtitleZhTw"))
  })
  private com.logisall.anyware.core.domain.International.InterText subtitle; // 서브 타이틀

  @Embedded
  @AttributeOverrides({
      @AttributeOverride(name = "textKoKr", column = @Column(name = "summaryKoKr", length = 500)),
      @AttributeOverride(name = "textEnUs", column = @Column(name = "summaryEnUs", length = 500)),
      @AttributeOverride(name = "textJaJp", column = @Column(name = "summaryJaJp", length = 500)),
      @AttributeOverride(name = "textZhCn", column = @Column(name = "summaryZhCn", length = 500)),
      @AttributeOverride(name = "textZhTw", column = @Column(name = "summaryZhTw", length = 500))
  })
  private com.logisall.anyware.core.domain.International.InterText summary; // 간략 설명

  @Embedded
  @AttributeOverrides({
      @AttributeOverride(name = "textKoKr", column = @Column(name = "contentKoKr", columnDefinition = "TEXT", length = 65535)),
      @AttributeOverride(name = "textEnUs", column = @Column(name = "contentEnUs", columnDefinition = "TEXT", length = 65535)),
      @AttributeOverride(name = "textJaJp", column = @Column(name = "contentJaJp", columnDefinition = "TEXT", length = 65535)),
      @AttributeOverride(name = "textZhCn", column = @Column(name = "contentZhCn", columnDefinition = "TEXT", length = 65535)),
      @AttributeOverride(name = "textZhTw", column = @Column(name = "contentZhTw", columnDefinition = "TEXT", length = 65535))
  })
  private InterText content; // 상세 설명

  // 이미지
  @JsonIgnore
  @OrderBy("createdDate ASC")
  @ManyToMany
  @JoinTable(name = "ProductToFile",
      joinColumns = @JoinColumn(name = "idProduct", nullable = false, foreignKey = @ForeignKey(name = "FK_Product_For_Product_To_File")),
      inverseJoinColumns = @JoinColumn(name = "idFile", nullable = false, foreignKey = @ForeignKey(name = "FK_File_For_Product_To_File"))
  )
  private List<com.logisall.anyware.core.domain.file.FileEntity> images ;

  public void setLocale(Locale locale) {
    if (this.name != null)
      this.name.setLocale(locale);
    if (this.subtitle != null)
      this.subtitle.setLocale(locale);
    if (this.summary != null)
      this.summary.setLocale(locale);
    if (this.content != null)
      this.content.setLocale(locale);
  }

  public void lazy() {
    if(this.images != null) {
      this.images.size();
      for(FileEntity fe:images){
        fe.lazy();
      }
    }
  }
}
