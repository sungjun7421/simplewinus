package com.logisall.anyware.core.service;

import java.util.List;
import java.util.Locale;

public interface CategoryService<T, R> {

  // CREATE
  T create(T entity);

  // UPDATE
  T update(T entity);

  // DELETE
  void delete(R id);

  // READ
  T get(Locale locale, R id);
  List<T> list(Locale locale);

  boolean isDuplicate(Locale locale, String name);
  void changeOrder(Long id, String mode);

}

