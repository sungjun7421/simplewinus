package com.logisall.anyware.core.model.resbody.commerce.product;

import com.logisall.anyware.core.model.BaseResponseThumbBody;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.server.core.Relation;

@Relation(value = "product", collectionRelation = "products")
@Slf4j
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductThumbResBody extends BaseResponseThumbBody {

  private static final long serialVersionUID = 8940953336713059501L;

  private long id;
  private String name;
  private int price;
}
