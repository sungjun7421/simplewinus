package com.logisall.anyware.core.service.commerce.product;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.commerce.product.Product;
import com.logisall.anyware.core.domain.commerce.product.ProductPredicate;
import com.logisall.anyware.core.domain.commerce.product.ProductRepository;
import com.logisall.anyware.core.domain.commerce.wish.WishRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.commerce.cart.CartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

  @Autowired
  private ProductRepository productRepository;
  @Autowired
  private CartService cartService;
  @Autowired
  private WishRepository wishRepository;

  @Override
  @Transactional
  public com.logisall.anyware.core.domain.commerce.product.Product create(com.logisall.anyware.core.domain.commerce.product.Product product) {

//    product.uploadFiles();

    return productRepository.save(product);
  }

  @Override
  @Transactional
  public com.logisall.anyware.core.domain.commerce.product.Product update(com.logisall.anyware.core.domain.commerce.product.Product product) {
    if (product.getId() == null) {
      throw new com.logisall.anyware.core.config.exception.BadRequestException();
    }

    return productRepository.findById(product.getId())
            .map(ori -> {
//              product.uploadFiles();
              BeanUtils.copyProperties(product, ori, com.logisall.anyware.core.domain.commerce.product.Product.IGNORE_PROPERTIES);
              return productRepository.save(ori);
            }).orElseThrow(BadRequestException::new);
  }

  @Override
  @Transactional
  public void delete(Long id) {
    productRepository.findById(id).ifPresent(product -> {
      product.delete();
      // TODO 장바구니 삭제
      cartService.deleteAllByProduct(id);
      wishRepository.deleteCustomByProductId(id);
//      });
      productRepository.delete(product);
    });
  }

  @Override
  @Transactional
  public com.logisall.anyware.core.domain.commerce.product.Product get(Locale locale, Long id) {
    return productRepository.findById(id)
            .map(product -> {
              product.setLocale(locale);
              product.lazy();
              return product;
            }).orElse(null);
  }

  @Override
  @Transactional
  public Page<com.logisall.anyware.core.domain.commerce.product.Product> page(Locale locale, com.logisall.anyware.core.model.Filter filter, Boolean isActive) {
    return null;
  }

  @Override
  @Transactional
  public List<com.logisall.anyware.core.domain.commerce.product.Product> listByAvailableForPurchase(long qty) {
    return null;
  }

  @Override
  @Transactional
  public List<com.logisall.anyware.core.domain.commerce.product.Product> listByOnSaleAndOwner(Long idUser) {
    return null;
  }

  @Override
  @Transactional
  public List<com.logisall.anyware.core.domain.commerce.product.Product> listByAvailableAndOwner(Long idUser) {
    return null;
  }

  @Override
  @Transactional
  public Page<com.logisall.anyware.core.domain.commerce.product.Product> page(Locale locale, Filter filter, Long idCategory, Boolean isActive) {
    Page<com.logisall.anyware.core.domain.commerce.product.Product> page = productRepository.findAll(
            ProductPredicate.getInstance()
                    .search(filter.getQuery())
                    .startDate(filter.getStartDate())
                    .endDate(filter.getEndDate())
                    .category(idCategory)
                    .active(isActive)
                    .values(),
            filter.getPageable());

    page.forEach(Product::lazy);
    page.forEach(product -> {
      product.setLocale(locale);
    });
    return page;
  }
  @Override
  @Transactional
  public void changeOrder(Long id, String mode) {
    return ;
  }
}
