package com.logisall.anyware.core.vo;

import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class BaseEntityInfo implements Serializable {

  private static final long serialVersionUID = -4559737981171974488L;

  private Long id;
  private String name;
}
