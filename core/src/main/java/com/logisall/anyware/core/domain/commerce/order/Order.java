package com.logisall.anyware.core.domain.commerce.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.util.concurrent.AtomicDouble;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.commerce.PayMethod;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistory;
import com.logisall.anyware.core.model.reqbody.commerce.order.PaymentPriceInfo;
import com.logisall.anyware.core.model.reqbody.commerce.product.ProductReqBody;
import com.logisall.anyware.core.model.resbody.commerce.order.OrderDetailResBody;
import com.logisall.anyware.core.utils.DateUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 주문
 */
@Slf4j
@Entity(name = "CommerceOrder")
@Getter
@Setter
@ToString(exclude = {"relativeBuyer", "usedCoupons"})
@NoArgsConstructor
public class Order extends AbstractEntity<Long> {

    private static final long serialVersionUID = 7286974552754757364L;

    public static final String[] IGNORE_PROPERTIES = {
            "id",
            "oid",
            "relativeBuyer",
            "relativeOrderMachine",
            "usedCoupons",
            "orderMachines"
    };

    @PrePersist
    public void prePersist() {
        if (this.orderStatus == null) {
            this.orderStatus = com.logisall.anyware.core.domain.commerce.OrderStatus.INIT;
        }

    }

    @Id
    @Column(unique = true)
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String oid; // 주문 ID

    @Column
    private String title; // 주문 명 (ex A삼품 외 3개)

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "fullName", column = @Column(name = "buyerFullName")),
            @AttributeOverride(name = "mobile", column = @Column(name = "buyerMobile")),
            @AttributeOverride(name = "email", column = @Column(name = "buyerEmail"))
    })
    private BuyerInfo buyerInfo; // 구매자 정보


    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "fullName", column = @Column(name = "shipFullName")),
            @AttributeOverride(name = "mobile1", column = @Column(name = "shipMobile1")),
            @AttributeOverride(name = "mobile2", column = @Column(name = "shipMobile2")),
            @AttributeOverride(name = "postalCode", column = @Column(name = "shipPostalCode")),
            @AttributeOverride(name = "address1", column = @Column(name = "shipAddress1")),
            @AttributeOverride(name = "address2", column = @Column(name = "shipAddress2")),
            @AttributeOverride(name = "message", column = @Column(name = "shipMessage"))
    })
    private ShippingInfo shippingInfo; // 배송 정보

    //== 상태
    @Enumerated(EnumType.STRING)
    @Column(length = 12)
    private OrderStatus orderStatus; // 주문 상태

    @Enumerated(EnumType.STRING)
    @Column(length = 12)
    private PayMethod payMethod; // 결제 수단

    @Column(columnDefinition = "BIT(1) default 0")
    private boolean paymentComplete; // 결제 모듈 완료

    @Column
    @DateTimeFormat(pattern = DateUtils.FORMAT_DATE_TIME_UNIT_BAR)
    private LocalDateTime paymentDate; // 결제 완료(확정)한 시간

    @Column
    private String failedMsg; // 결제 실패 메시지

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "idBuyer", referencedColumnName = "id", nullable = true, foreignKey = @ForeignKey(name = "FK_Buyer_For_Order"))
    private Buyer relativeBuyer; // 주문한 사용자

    @Embedded
    private TransactionInfo transactionInfo; // PG사 Transaction 정보

    @Column(columnDefinition = "DECIMAL(19,0) default 0")
    private BigDecimal amount; // 원가


//    @Column(columnDefinition = "DECIMAL(19,2) default 0")
//    private BigDecimal amountUSD; // 결제 금액 <부가세 포함>
//    @Column(columnDefinition = "DECIMAL(19,2) default 0")
//    private BigDecimal exchange; // 환율


    @Column(columnDefinition = "DECIMAL(19,2) default 0")
    private BigDecimal fcCoupon; // 할인된 쿠폰 금액 (-)

    @Column(columnDefinition = "DECIMAL(19,2) default 0")
    private BigDecimal fcPoint; // 사용한 포인트 금액 (-)

    @Column(columnDefinition = "DECIMAL(19,2) default 0")
    private BigDecimal totalPrice;// 총결제 금액

    @Column(columnDefinition = "BIT(1) default 0")
    private boolean isMember; // 비회원 결제

    @Column
    private String mobileNonMember; // 비회원 주문정보

    @Transient
    private List<ProductReqBody> products;

    // Discounted product amount for coupon discount (for coupon discount calculation)
    @JsonIgnore
    public BigDecimal getDcPriceForCouponDc() {
        BigDecimal amount = this.amount.subtract(this.fcCoupon).subtract(this.fcPoint); // A = (price - discount - dcByMembership)
        log.debug("DcPriceForCouponDc amount ::: {}", amount);
        return amount;
    }

    @JsonIgnore
    @OrderBy("createdDate DESC")
    @ManyToMany
    @JoinTable(name = "UsedCoupon",
        joinColumns = @JoinColumn(name = "idOrder", nullable = false, foreignKey = @ForeignKey(name = "FK_Order_For_Order_To_CouponHistory")),
        inverseJoinColumns = @JoinColumn(name = "idCouponHistory", nullable = false, foreignKey = @ForeignKey(name = "FK_CouponHistory_For_Order_To_CouponHistory"))
    )
    //private List<CouponHistory> usedCoupons = new ArrayList<>(); // 사용 쿠폰 목록
    private List<com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistory> usedCoupons ;


    // 쿠폰할인을 적용하여 결제금액 재 계산
    public void resetPaymentAmountByCouponDc() {
        this.totalPrice = this.amount.subtract(this.fcCoupon).subtract(this.fcPoint);
    }


    public void delete() {

    }

    public void lazy() {
        if (this.getRelativeBuyer() != null) {
            if (this.getRelativeBuyer().getOrders() != null) {
                this.getRelativeBuyer().getOrders().size();
            }
        }
        if (this.usedCoupons != null) {
            this.usedCoupons.size();
            for(CouponHistory c:this.usedCoupons){
                c.lazy();
            }

        }
    }

    public com.logisall.anyware.core.model.resbody.commerce.order.OrderDetailResBody toBodyOrderDetail() {

        com.logisall.anyware.core.model.resbody.commerce.order.OrderDetailResBody orderDetailResBody = new OrderDetailResBody();
        orderDetailResBody.setId(this.id);
        orderDetailResBody.setOid(this.oid);
        orderDetailResBody.setTitle(this.title);
        orderDetailResBody.setBuyerInfo(this.buyerInfo);
        orderDetailResBody.setOrderStatus(this.orderStatus);
        orderDetailResBody.setPayMethod(this.payMethod);
        orderDetailResBody.setPaymentComplete(this.paymentComplete);
        orderDetailResBody.setPaymentDate(this.paymentDate);
        orderDetailResBody.setFailedMsg(this.failedMsg);
        //orderDetailResBody.setRelativeBuyer(this.relativeBuyer);
        orderDetailResBody.setAmount(this.amount);
        orderDetailResBody.setFcCoupon(this.fcCoupon);
        orderDetailResBody.setFcPoint(this.fcPoint);
        orderDetailResBody.setTotalPrice(this.totalPrice);
        orderDetailResBody.setMember(this.isMember);
        orderDetailResBody.setMobileNonMember(this.mobileNonMember);

        return orderDetailResBody;
    }

    public com.logisall.anyware.core.model.reqbody.commerce.order.PaymentPriceInfo toPaymentPriceInfo() {

        AtomicDouble price = new AtomicDouble(0);
        AtomicDouble dc = new AtomicDouble(0);
        AtomicDouble dcByCoupon = new AtomicDouble(0);
        AtomicDouble hwdc = new AtomicDouble(0);
        AtomicDouble amount = new AtomicDouble(0);

        if (this.totalPrice == null || this.totalPrice.doubleValue() != amount.doubleValue()) {
            log.info("fcTotal => {}, amount => {}", this.totalPrice, amount.doubleValue());
            throw new com.logisall.anyware.core.config.exception.BadRequestException("상품 총액 불일치!");
        }

        if (this.amount == null) {
            log.info(".thisamount ::: {}", this.amount);
            throw new BadRequestException("최종 지불금액(결제금액) 이 존재하지 않음!");
        }

        return PaymentPriceInfo.builder()
                .totalByHwdc(BigDecimal.valueOf(hwdc.doubleValue()))

                .totalByPrice(this.amount != null ? this.amount : BigDecimal.valueOf(amount.doubleValue()))
                .totalByDiscountCoupon(this.fcCoupon != null ? this.fcCoupon : BigDecimal.valueOf(dcByCoupon.doubleValue()))
                .point(this.fcPoint != null ? this.fcPoint.intValue() : 0)
                .totalByPaymentPrice(this.totalPrice)
                .build();
    }
}
