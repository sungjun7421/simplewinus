package com.logisall.anyware.core.model.resbody.commerce.cart;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
@ToString
public class CartResTotalBody {
    Double totalOriginPrice;
    Double totalSalePrice;
    Integer totalProduct;
    Integer totalQty;
}
