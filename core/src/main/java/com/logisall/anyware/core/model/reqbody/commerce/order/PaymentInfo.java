package com.logisall.anyware.core.model.reqbody.commerce.order;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 결제금액정보
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@Builder
public class PaymentInfo implements Serializable {

    private static final long serialVersionUID = 8041881660897449511L;
    private BigDecimal totalPriceOrder;
    private BigDecimal totalPriceCoupon;
    private BigDecimal totalPricePoint;
    private BigDecimal totalPaymentPrice;
    private BigDecimal totalPriceSave;

    public PaymentInfo() {
        this.totalPriceOrder = BigDecimal.ZERO;
        this.totalPriceCoupon = BigDecimal.ZERO;
        this.totalPricePoint = BigDecimal.ZERO;
        this.totalPaymentPrice = BigDecimal.ZERO;
        this.totalPriceSave = BigDecimal.ZERO;
    }


}
