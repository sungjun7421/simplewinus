package com.logisall.anyware.core.model.property;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "app.email")
public class MetaEmail implements Serializable {

  private static final long serialVersionUID = -8973013042851311932L;

  private String siteName;
  private String frontHost;
  private String logoUri;
  private String unsubscribeUri;
  private String primaryColor;
}
