package com.logisall.anyware.core.model.resbody;

import com.logisall.anyware.core.model.BaseResponseBody;
import lombok.*;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BaseStatusResBody extends BaseResponseBody {

  private static final long serialVersionUID = 2355295882821534501L;

  private String key;
  private String value;
}
