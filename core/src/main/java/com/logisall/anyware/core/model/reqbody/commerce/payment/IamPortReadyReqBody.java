package com.logisall.anyware.core.model.reqbody.commerce.payment;

import com.logisall.anyware.core.domain.commerce.PayMethod;
import com.logisall.anyware.core.domain.commerce.order.Order;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
@ToString
public class IamPortReadyReqBody {
    private static final long serialVersionUID = 4954462426316770227L;

    private String oid;
    private PayMethod payMethod;

    public boolean isBadRequest() {
        return  this.payMethod == null
                || this.oid == null
                ;
    }

    public com.logisall.anyware.core.domain.commerce.order.Order toOrder() {
        com.logisall.anyware.core.domain.commerce.order.Order order = new Order();
        order.setOid(this.oid);
        order.setPayMethod(this.getPayMethod());

        return order;
    }
}
