package com.logisall.anyware.core.service.send.email;

import java.util.List;
import java.util.Map;

public interface SendEmailService {

  void send(String to, String subject, String body) throws Exception;

  void send(String to, String subject, String body, String[] ccList) throws Exception;

  void send(String to, String subject, Map<String, Object> model, String templatePathName) throws Exception;

  void send(List<String> to, String subject, String body) throws Exception;
}
