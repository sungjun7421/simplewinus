package com.logisall.anyware.core.model.resbody.commerce.point;

import com.logisall.anyware.core.model.BaseResponseBody;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Relation(value = "point", collectionRelation = "points")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PointUsedHistoryResBody extends BaseResponseBody {

    private static final long serialVersionUID = -2587873964217247277L;

    private Long id;
    private String storeName;
    private LocalDateTime createdDate;
    private BigDecimal point;
    private String status;
}
