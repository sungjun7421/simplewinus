package com.logisall.anyware.core.domain.board.contact;

import com.logisall.anyware.core.domain.board.contact.Contact;
import com.logisall.anyware.core.domain.board.contact.ContactRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface ContactRepository extends
        JpaRepository<Contact, Long>,
        QuerydslPredicateExecutor<Contact>,
        ContactRepositoryCustom {
}
