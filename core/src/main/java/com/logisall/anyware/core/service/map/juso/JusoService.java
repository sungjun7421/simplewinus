package com.logisall.anyware.core.service.map.juso;


import com.logisall.anyware.core.model.map.juso.JusoEntity;

import java.util.concurrent.Future;

public interface JusoService {

    Future<com.logisall.anyware.core.model.map.juso.JusoEntity> geJusoInfo(String keyword, int currentPage, int countPerPage) throws InterruptedException;
    Future<JusoEntity> geJusoInfo(String keyword, int currentPage) throws InterruptedException;
}
