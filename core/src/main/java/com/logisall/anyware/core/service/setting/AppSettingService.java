package com.logisall.anyware.core.service.setting;

import com.logisall.anyware.core.domain.International.InternationalMode;
import com.logisall.anyware.core.domain.setting.AppSetting;

import java.util.List;
import java.util.Locale;
import java.util.Map;

public interface AppSettingService {

    com.logisall.anyware.core.domain.setting.AppSetting setting(com.logisall.anyware.core.domain.setting.AppSetting appSetting);

    AppSetting getSetting();

    InternationalMode getInternationalMode();

//    ComponentMode getComponentMode();
//
    Locale getDefaultLocale();
    List<Map<String, String>> languages();
}
