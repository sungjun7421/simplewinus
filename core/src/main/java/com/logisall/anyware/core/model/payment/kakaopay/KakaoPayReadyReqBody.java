package com.logisall.anyware.core.model.payment.kakaopay;

import lombok.*;
import okhttp3.FormBody;

import java.io.Serializable;
import java.nio.charset.Charset;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class KakaoPayReadyReqBody implements Serializable {

  private static final long serialVersionUID = 6213501720096061357L;

  private String cid; // 가맹점 코드. 10자.
  private String partner_order_id; // 가맹점 주문번호. 최대 100자
  private String partner_user_id; // 가맹점 회원 id. 최대 100자
  private String item_name; // 상품명. 최대 100자
  private int quantity; // 상품 수량
  private int total_amount; // 상품 총액
  private int tax_free_amount; // 상품 비과세 금액
  private String approval_url; // 결제 성공시 redirect url. 최대 255자
  private String cancel_url; // 결제 취소시 redirect url. 최대 255자
  private String fail_url; // 결제 실패시 redirect url. 최대 255자

  public FormBody toFormBody() {
    return new FormBody.Builder(Charset.forName("UTF-8"))
      .add("cid", cid)
      .add("partner_order_id", partner_order_id)
      .add("partner_user_id", partner_user_id)
      .add("item_name", item_name)
      .add("quantity", String.valueOf(quantity))
      .add("total_amount", String.valueOf(total_amount))
      .add("tax_free_amount", String.valueOf(tax_free_amount))
      .add("approval_url", approval_url)
      .add("cancel_url", cancel_url)
      .add("fail_url", fail_url)
      .build();
  }
}
