package com.logisall.anyware.core.service.account.user;

import com.logisall.anyware.core.domain.user.Authority;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.DomainService;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Locale;

public interface UserService extends DomainService<User, Long> {

    // CUD
    User create(User user);
    User update(User user);
    void delete(Long id);

    // R
    User get(Long id);
    User get(String email);
    List<User> list(Filter filter);
    List<User> list(Locale locale);
    Page<User> page(Filter filter, Authority.Role role);
    Authority authority(Authority.Role role);  // 권한 불러오기

    // 비밀번호 재설정 (아이디, 병견할 비밀번호)
    void resetPassword(Long id, String password);

    // 회원탈퇴
    void leave(Long id, String reason);

    // 개인정보 삭제
    void removePrivacy(Long id);
}
