package com.logisall.anyware.core.service.device;

import com.logisall.anyware.core.domain.device.Device;
import com.logisall.anyware.core.domain.device.DevicePredicate;
import com.logisall.anyware.core.domain.device.DeviceRepository;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.reqbody.etc.device.DeviceReqBody;
import com.logisall.anyware.core.service.account.user.UserService;
import com.logisall.anyware.core.service.send.push.PushService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PushService pushService;

    @Override
    @Transactional
    public com.logisall.anyware.core.domain.device.Device create(DeviceReqBody deviceReqBody) {
        com.logisall.anyware.core.domain.device.Device device = new com.logisall.anyware.core.domain.device.Device();
        device.setDeviceToken(deviceReqBody.getDeviceToken());
        if(deviceReqBody.getEmail() != null && !deviceReqBody.getEmail().isEmpty() && userRepository.existsByEmail(deviceReqBody.getEmail())) {
            User user = userService.get(deviceReqBody.getEmail());
            device.setRelativeUser(user);
        }
//        pushService.subscribeToTopic(deviceReqBody.getDeviceToken());
        device.setSubscribe(false);

        return deviceRepository.save(device);
    }

    @Override
    @Transactional(readOnly = true)
    public com.logisall.anyware.core.domain.device.Device get(Long id) {

        return deviceRepository.findById(id)
                .map(device -> {
                    device.lazy();
                    return device;
                }).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public com.logisall.anyware.core.domain.device.Device get(String deviceToken) {

        return deviceRepository.findByDeviceToken(deviceToken)
                .map(device -> {
                    device.lazy();
                    return device;
                }).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<com.logisall.anyware.core.domain.device.Device> page(Filter filter) {

        Page<Device> page = deviceRepository.findAll(
                DevicePredicate.getInstance()
                        .search(filter.getQuery())
                        .startDate(filter.getStartDate())
                        .endDate(filter.getEndDate())
                        .values(),
                filter.getPageable()
    );

        page.forEach(device -> {
            device.lazy();
        });

        return page;
    }
}
