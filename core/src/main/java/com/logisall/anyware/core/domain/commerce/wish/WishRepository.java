package com.logisall.anyware.core.domain.commerce.wish;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface WishRepository extends
        JpaRepository<com.logisall.anyware.core.domain.commerce.wish.Wish, com.logisall.anyware.core.domain.commerce.wish.Wish.Id>,
        QuerydslPredicateExecutor<com.logisall.anyware.core.domain.commerce.wish.Wish>,
    WishRepositoryCustom {

    public long countByRelativeProduct_IdAndRelativeBuyer_Id(Long idProduct, Long idBuyer);
    public List<com.logisall.anyware.core.domain.commerce.wish.Wish> findAllById_IdBuyer(Long idBuyer);
    public Wish findById_IdBuyerAndId_IdProduct(Long idBuyer, Long idProduct);
    public boolean existsById_IdBuyerAndId_IdProduct(Long idBuyer,Long idProduct);
    public void deleteById_IdBuyerAndId_IdProduct(Long idBuyer,Long idProduct);

    @Modifying
    @Query("delete from Wish w where w.id.idBuyer=:idBuyer and w.id.idProduct=:idProduct")
    public void deleteCustom(Long idBuyer,Long idProduct);

    @Modifying
    @Query("delete from Wish w where w.id.idProduct=:idProduct")
    public void deleteCustomByProductId(Long idProduct);

}
