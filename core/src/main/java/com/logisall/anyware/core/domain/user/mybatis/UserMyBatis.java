package com.logisall.anyware.core.domain.user.mybatis;

import com.logisall.anyware.core.model.resbody.account.MeResBody;

import java.util.List;

/**
 * MyBatis
 */
public interface UserMyBatis {

    List<MeResBody> selectUsers() throws Exception;
}
