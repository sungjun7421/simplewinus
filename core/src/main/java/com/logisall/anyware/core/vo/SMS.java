package com.logisall.anyware.core.vo;

import com.logisall.anyware.core.domain.sms.SMSHistory;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import java.io.Serializable;

// TODO SMS 모듈형식에 맞춰야함
@Getter
@Setter
@ToString
public class SMS implements Serializable {

    private static final long serialVersionUID = 3943107289461998729L;

    @Getter
    public enum Type {
        SMS("sms"),
        LMS("lms"),
        MMS("mms");

        private final String value;

        Type(final String value) {
            this.value = value;
        }
    }

    public SMS() {
        this.type = Type.LMS;
    }

    public SMS(String destPhone, String msgBody) {
        this.type = Type.LMS;
        this.destPhone = destPhone;
        this.msgBody = msgBody;
    }

    public SMS(Type type, String destPhone, String msgBody) {
        this.type = type;
        this.destPhone = destPhone;
        this.msgBody = msgBody;
    }

    private Type type;

    private String sendTime;

    private String destPhone;

    private String destName;

    private String sendPhone;

    private String sendName;

    private String subject;

    private String msgBody;

    public MultiValueMap<String, String> getMultiValueMap() {
        MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();

        multiValueMap.add("dest_phone", this.getDestPhone());
        multiValueMap.add("dest_name", this.getDestName());
        multiValueMap.add("subject", this.getSubject());
        multiValueMap.add("msg_body", this.getMsgBody());
        if (!StringUtils.isEmpty(this.getSendTime()))
            multiValueMap.add("send_time", this.getSendTime().toString());

        return multiValueMap;
    }


    public static final String STATUS_4100 = "SMS_전달 (성공)";
    public static final String STATUS_6600 = "LMS/MMS_전달 (성공)";

    public static final String STATUS_4421 = "SMS_타임아웃";
    public static final String STATUS_4426 = "SMS_재시도한도초과";
    public static final String STATUS_4425 = "SMS_단말기 호 처리중";
    public static final String STATUS_4400 = "SMS_음영 지역";
    public static final String STATUS_4401 = "SMS_단말기 전원꺼짐";
    public static final String STATUS_4402 = "SMS_단말기 메시지 저장 초과";
    public static final String STATUS_4410 = "SMS_잘못된 번호";
    public static final String STATUS_4422 = "SMS_단말기 일시정지";
    public static final String STATUS_4427 = "SMS_기타 단말기 문제";
    public static final String STATUS_4405 = "SMS_단말기 busy";
    public static final String STATUS_4423 = "SMS_단말기 착신거부";
    public static final String STATUS_4412 = "SMS_착신거절";
    public static final String STATUS_4411 = "SMS_EPDB에러";
    public static final String STATUS_4428 = "SMS_시스템에러";
    public static final String STATUS_4404 = "SMS_가입자 위치정보 없음";
    public static final String STATUS_4413 = "SMS_SMSC형식오류";
    public static final String STATUS_4414 = "SMS_비가입자,결번,서비스정지";
    public static final String STATUS_4424 = "SMS_URL SMS 미지원폰";
    public static final String STATUS_4403 = "SMS_메시지 삭제됨";
    public static final String STATUS_4430 = "SMS_스팸";
    public static final String STATUS_4420 = "SMS_기타에러";

    public static final String STATUS_6601 = "LMS/MMS_타임아웃";
    public static final String STATUS_6602 = "LMS/MMS_핸드폰 호 처리 중";
    public static final String STATUS_6603 = "LMS/MMS_음영 지역";
    public static final String STATUS_6604 = "LMS/MMS_전원이 꺼져있음";
    public static final String STATUS_6605 = "LMS/MMS_메시지 저장개수 초과";
    public static final String STATUS_6606 = "LMS/MMS_잘못된 번호";
    public static final String STATUS_6607 = "LMS/MMS_서비스 일시정지";
    public static final String STATUS_6608 = "LMS/MMS_기타 단말기 문제";
    public static final String STATUS_6609 = "LMS/MMS_착신 거절";
    public static final String STATUS_6610 = "LMS/MMS_기타 에러";
    public static final String STATUS_6611 = "LMS/MMS_통신사의 SMC 형식 오류";
    public static final String STATUS_6612 = "LMS/MMS_게이트웨이의 형식 오류";
    public static final String STATUS_6613 = "LMS/MMS_서비스 불가 단말기";
    public static final String STATUS_6614 = "LMS/MMS_핸드폰 호 불가 상태";
    public static final String STATUS_6615 = "LMS/MMS_SMC 운영자에 의해 삭제";
    public static final String STATUS_6616 = "LMS/MMS_통신사의 메시지 큐 초과";
    public static final String STATUS_6617 = "LMS/MMS_통신사의 스팸처리";
    public static final String STATUS_6618 = "LMS/MMS_공정위의 스팸처리";
    public static final String STATUS_6619 = "LMS/MMS_게이트웨이의 스팸처리";
    public static final String STATUS_6620 = "LMS/MMS_발송 건수 초과";
    public static final String STATUS_6621 = "LMS/MMS_메시지의 길이 초과";
    public static final String STATUS_6622 = "LMS/MMS_잘못된 번호 형식";
    public static final String STATUS_6623 = "LMS/MMS_잘못된 데이터 형식";
    public static final String STATUS_6624 = "LMS/MMS_MMS 정보를 찾을 수 없음";
    public static final String STATUS_6670 = "LMS/MMS_첨부파일 사이즈 초과(60K)";
    public static final String STATUS_9903 = "공통_LMS/MMS_선불사용자 사용금지";
    public static final String STATUS_9904 = "공통_Block time (날짜제한)";
    public static final String STATUS_9082 = "공통_발송해제)";
    public static final String STATUS_9083 = "공통_IP차단)";
    public static final String STATUS_9023 = "공통_Callback error)";
    public static final String STATUS_9905 = "공통_Block time(요일제한)";
    public static final String STATUS_9010 = "공통_아이디 틀림";
    public static final String STATUS_9011 = "공통_비밀번호 틀림";
    public static final String STATUS_9012 = "공통_중복접속량 많음";
    public static final String STATUS_9013 = "공통_발송시간 지난 데이터";
    public static final String STATUS_9014 = "공통_시간제한(리포트 수신대기timeout)";
    public static final String STATUS_9020 = "공통_Wrong Data Format";
    public static final String STATUS_9021 = "공통_";
    public static final String STATUS_9022 = "공통_Wrong Data Format (cinfo가 특수 문자 / , 공백 을 포함하고 있다)";
    public static final String STATUS_9080 = "공통_Deny User Ack";
    public static final String STATUS_9214 = "공통_Wrong Phone Num";
    public static final String STATUS_9311 = "공통_Fax File Not Found";
    public static final String STATUS_9908 = "공통_PHONE,FAX선불사용자 제한기능";
    public static final String STATUS_9090 = "공통_기타에러";
    public static final String STATUS_M1 = "공통_잘못된 데이터 형식 발송 오류";

    public static String statusMsg(int status) {
        switch (status) {
            case 4100:
                return com.logisall.anyware.core.vo.SMS.STATUS_4100;
            case 6600:
                return com.logisall.anyware.core.vo.SMS.STATUS_6600;

            case 4421:
                return com.logisall.anyware.core.vo.SMS.STATUS_4421;
            case 4426:
                return com.logisall.anyware.core.vo.SMS.STATUS_4426;
            case 4425:
                return com.logisall.anyware.core.vo.SMS.STATUS_4425;
            case 4400:
                return com.logisall.anyware.core.vo.SMS.STATUS_4400;
            case 4401:
                return com.logisall.anyware.core.vo.SMS.STATUS_4401;
            case 4402:
                return com.logisall.anyware.core.vo.SMS.STATUS_4402;
            case 4410:
                return com.logisall.anyware.core.vo.SMS.STATUS_4410;
            case 4422:
                return com.logisall.anyware.core.vo.SMS.STATUS_4422;
            case 4427:
                return com.logisall.anyware.core.vo.SMS.STATUS_4427;
            case 4405:
                return com.logisall.anyware.core.vo.SMS.STATUS_4405;
            case 4423:
                return com.logisall.anyware.core.vo.SMS.STATUS_4423;
            case 4412:
                return com.logisall.anyware.core.vo.SMS.STATUS_4412;
            case 4411:
                return com.logisall.anyware.core.vo.SMS.STATUS_4411;
            case 4428:
                return com.logisall.anyware.core.vo.SMS.STATUS_4428;
            case 4404:
                return com.logisall.anyware.core.vo.SMS.STATUS_4404;
            case 4413:
                return com.logisall.anyware.core.vo.SMS.STATUS_4413;
            case 4414:
                return com.logisall.anyware.core.vo.SMS.STATUS_4414;
            case 4424:
                return com.logisall.anyware.core.vo.SMS.STATUS_4424;
            case 4403:
                return com.logisall.anyware.core.vo.SMS.STATUS_4403;
            case 4430:
                return com.logisall.anyware.core.vo.SMS.STATUS_4430;
            case 4420:
                return com.logisall.anyware.core.vo.SMS.STATUS_4420;

            case 6601:
                return com.logisall.anyware.core.vo.SMS.STATUS_6601;
            case 6602:
                return com.logisall.anyware.core.vo.SMS.STATUS_6602;
            case 6603:
                return com.logisall.anyware.core.vo.SMS.STATUS_6603;
            case 6604:
                return com.logisall.anyware.core.vo.SMS.STATUS_6604;
            case 6605:
                return com.logisall.anyware.core.vo.SMS.STATUS_6605;
            case 6606:
                return com.logisall.anyware.core.vo.SMS.STATUS_6606;
            case 6607:
                return com.logisall.anyware.core.vo.SMS.STATUS_6607;
            case 6608:
                return com.logisall.anyware.core.vo.SMS.STATUS_6608;
            case 6609:
                return com.logisall.anyware.core.vo.SMS.STATUS_6609;
            case 6610:
                return com.logisall.anyware.core.vo.SMS.STATUS_6610;
            case 6611:
                return com.logisall.anyware.core.vo.SMS.STATUS_6611;
            case 6612:
                return com.logisall.anyware.core.vo.SMS.STATUS_6612;
            case 6613:
                return com.logisall.anyware.core.vo.SMS.STATUS_6613;
            case 6614:
                return com.logisall.anyware.core.vo.SMS.STATUS_6614;
            case 6615:
                return com.logisall.anyware.core.vo.SMS.STATUS_6615;
            case 6616:
                return com.logisall.anyware.core.vo.SMS.STATUS_6616;
            case 6617:
                return com.logisall.anyware.core.vo.SMS.STATUS_6617;
            case 6618:
                return com.logisall.anyware.core.vo.SMS.STATUS_6618;
            case 6619:
                return com.logisall.anyware.core.vo.SMS.STATUS_6619;
            case 6620:
                return com.logisall.anyware.core.vo.SMS.STATUS_6620;
            case 6621:
                return com.logisall.anyware.core.vo.SMS.STATUS_6621;
            case 6622:
                return com.logisall.anyware.core.vo.SMS.STATUS_6622;
            case 6623:
                return com.logisall.anyware.core.vo.SMS.STATUS_6623;
            case 6624:
                return com.logisall.anyware.core.vo.SMS.STATUS_6624;
            case 6670:
                return com.logisall.anyware.core.vo.SMS.STATUS_6670;

            case 9903:
                return com.logisall.anyware.core.vo.SMS.STATUS_9903;
            case 9904:
                return com.logisall.anyware.core.vo.SMS.STATUS_9904;
            case 9082:
                return com.logisall.anyware.core.vo.SMS.STATUS_9082;
            case 9083:
                return com.logisall.anyware.core.vo.SMS.STATUS_9083;
            case 9023:
                return com.logisall.anyware.core.vo.SMS.STATUS_9023;
            case 9905:
                return com.logisall.anyware.core.vo.SMS.STATUS_9905;
            case 9010:
                return com.logisall.anyware.core.vo.SMS.STATUS_9010;
            case 9011:
                return com.logisall.anyware.core.vo.SMS.STATUS_9011;
            case 9012:
                return com.logisall.anyware.core.vo.SMS.STATUS_9012;
            case 9013:
                return com.logisall.anyware.core.vo.SMS.STATUS_9013;
            case 9014:
                return com.logisall.anyware.core.vo.SMS.STATUS_9014;
            case 9020:
                return com.logisall.anyware.core.vo.SMS.STATUS_9020;
            case 9021:
                return com.logisall.anyware.core.vo.SMS.STATUS_9021;
            case 9022:
                return com.logisall.anyware.core.vo.SMS.STATUS_9022;
            case 9080:
                return com.logisall.anyware.core.vo.SMS.STATUS_9080;
            case 9214:
                return com.logisall.anyware.core.vo.SMS.STATUS_9214;
            case 9908:
                return com.logisall.anyware.core.vo.SMS.STATUS_9908;
            case 9090:
                return com.logisall.anyware.core.vo.SMS.STATUS_9090;
            case -1:
                return com.logisall.anyware.core.vo.SMS.STATUS_M1;
            default:
                return "NOT FOUND ERROR CODE";
        }
    }

    public SMSHistory toHistory(){
        SMSHistory history = new SMSHistory();
        history.setType(type);
        history.setSendTime(sendTime);
        history.setDestPhone(destPhone);
        history.setDestName(destName);
        history.setSubject(subject);
        history.setMsgBody(msgBody);
        return history;
    }
}
