package com.logisall.anyware.core.model.reqbody.user;

import com.logisall.anyware.core.model.BaseRequestBody;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

@Schema(description = "휴대폰/이메일 인증")
@Setter
@Getter
@ToString
public class CertReqBody extends BaseRequestBody {

    private static final long serialVersionUID = 5843409952798725635L;

    @Getter
    public enum Mode {
        CERT_MOBILE("휴대폰 인증 및 수정")// 회원가입, 수정
        , CERT_EMAIL("이메일 존재 유무") // 회원가입,
        , FIND_PASSWORD_EMAIL("비밀번호 찾기") // 이메일
        , FIND_PASSWORD_MOBILE("비밀번호 찾기") // 모바일
        , FIND_ACCOUNT_MOBILE("계졍 찾기") // 모바일
        ;

        private final String value;

        Mode(final String value) {
            this.value = value;
        }
    }

    @Pattern(regexp = ValidUtils.PATTERN_MOBILE)
    @Schema(description = "휴대폰 번호", example = "01011112222", minLength = 10, maxLength = 11)
    private String mobile;

    @Schema(description = "모드 [ CERT_MOBILE(\"휴대폰 인증 및 수정\")" +
            ", CERT_EMAIL(\"이메일 존재 유무\")" +
            ", FIND_PASSWORD_EMAIL(\"비밀번호 찾기\")" +
            ", FIND_PASSWORD_MOBILE(\"비밀번호 찾기\")" +
            ", FIND_ACCOUNT_MOBILE(\"계졍 찾기\") ]")
    private Mode mode;

    @Email
    @Schema(description = "이메일 주소", example = "test@logisall.com", maxLength = 255)
    private String email;
}
