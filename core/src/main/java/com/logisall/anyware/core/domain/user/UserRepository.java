package com.logisall.anyware.core.domain.user;

import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepositoryCustom;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends
    PagingAndSortingRepository<User, Long>,
    QuerydslPredicateExecutor<User>,
    UserRepositoryCustom {

  Optional<User> findByEmail(String email);
  User findFirstByMobile(String mobile);

  List<User> findByMobileAndUserDetailsMetaEnabledTrue(String mobile);
  List<User> findByMobileAndFullNameAndEmailAndUserDetailsMetaEnabledTrue(String mobile, String fullName, String email);
  List<User> findByUserDetailsMetaEnabledTrueAndLeaveMetaLeaveFalse();
  List<User> findByUserDetailsMetaEnabledTrueAndLeaveMetaLeaveFalseAndTermsAgreeEmailRcvTrue();

  boolean existsByEmail(String email);
  boolean existsByMobile(String mobile);

  Optional<User> findBySocialIdFacebookId(String facebookId);
  Optional<User> findBySocialIdGoogleId(String googleId);
  Optional<User> findBySocialIdNaverId(String naverId);
  Optional<User> findBySocialIdKakaoId(String kakaoId);
  Optional<User> findBySocialIdAppleId(String appleId);
}
