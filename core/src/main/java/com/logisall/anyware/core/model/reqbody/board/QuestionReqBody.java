package com.logisall.anyware.core.model.reqbody.board;

import com.logisall.anyware.core.domain.board.qna.Qna;
import com.logisall.anyware.core.domain.board.qna.QnaNoMember;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.model.BaseRequestBody;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.util.List;

@Setter
@Getter
@ToString
public class QuestionReqBody extends BaseRequestBody {

    private static final long serialVersionUID = -147872989563697266L;

    @Schema(description = "회원")
    private boolean member;

    @Schema(description = "Category Id")
    private Long idCategory;

    @Schema(description = "질문")
    private String title;

    @Schema(description = "내용")
    private String content;

    @Schema(description = "첨부파일")
    private List<String> images;

    // 비회원
    @Pattern(regexp = ValidUtils.PATTERN_FULLNAME)
    @Schema(description = "성명", example = "홍길동", minLength = 2, maxLength = 10)
    private String fullName;

    @Email
    @Schema(description = "이메일 주소", example = "test@logisall.com", minLength = 3, maxLength = 255)
    private String email;

    public Qna toQna(User user) {
        Qna qna = new Qna();
        qna.setTitle(this.title);
        qna.setContent(this.content);
        qna.setIdCategory(this.idCategory);
        qna.setRelativeUser(user);
        qna.setMember(true);
        qna.setActive(true);
        return qna;
    }


    public Qna toQnaNoMember() {
        Qna qna = new Qna();
        qna.setTitle(this.title);
        qna.setContent(this.content);
        qna.setIdCategory(this.idCategory);
        qna.setMember(false);
        qna.setActive(true);

        QnaNoMember qnaNoMember = new QnaNoMember();
        qnaNoMember.setFullName(this.fullName);
        qnaNoMember.setEmail(this.email);
        qna.setQnaNoMember(qnaNoMember);

        return qna;
    }

}
