package com.logisall.anyware.core.model.resbody.board;

import com.logisall.anyware.core.config.serializer.JsonLocalDateTimeDeserializer;
import com.logisall.anyware.core.config.serializer.JsonLocalDateTimeSerializer;
import com.logisall.anyware.core.domain.board.post.Post;
import com.logisall.anyware.core.model.BaseResponseThumbBody;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "게시물(게시판)")
@Relation(value = "post", collectionRelation = "posts")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PostThumbResBody extends BaseResponseThumbBody {

  private static final long serialVersionUID = -7322479157527620703L;

  @Schema(description = "ID")
  private Long id;

  @Schema(description = "게시물(게시판) 유형")
  private Post.Type type; // 게시판 유형

  @Schema(description = "제목")
  private String title; // 제목

  @Schema(description = "썸네일 이미지")
  private String thumbnail; // 썸네일

  @Schema(description = "조회수")
  private long pageView; // 조회수

  @Schema(description = "작성일")
  @JsonSerialize(using = JsonLocalDateTimeSerializer.class)
  @JsonDeserialize(using = JsonLocalDateTimeDeserializer.class)
  private LocalDateTime regDate; // 등록일 , 내림차순

  @Schema(description = "카테고리")
  private List<String> categories;

  @Schema(description = "상단고정(핀고정)")
  private boolean top;
}
