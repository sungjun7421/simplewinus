package com.logisall.anyware.core.model.payment.toss;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class TossStatusResBody implements Serializable {

  private static final long serialVersionUID = -2690785398099916132L;

  private String code;
  private String payToken;
  private String orderNo;
  private String payMethod;
  private String payStatus;
  private String[] availableActions;
  private String metadata;
}
