package com.logisall.anyware.core.model.reqbody.commerce.coupon;

import com.logisall.anyware.core.config.serializer.JsonLocalDateDeserializer;
import com.logisall.anyware.core.config.serializer.JsonLocalDateSerializer;
import com.logisall.anyware.core.domain.commerce.coupon.CouponType;
import com.logisall.anyware.core.model.BaseResponseThumbBody;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.time.LocalDate;


@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CouponThumbBody extends BaseResponseThumbBody {
    private static final long serialVersionUID = -6911193137775175604L;

    private Long id;
    private String name; // 메세지
    private String requite;
    private CouponType type; // 쿠폰 발급 타입
    private String code;
    private BigDecimal discountFixed; // 정액 의 입력 필드
    //private BigDecimal discountPercent;
    private BigDecimal minimumPayment;

    @JsonSerialize(using = JsonLocalDateSerializer.class)
    @JsonDeserialize(using = JsonLocalDateDeserializer.class)
    private LocalDate startDate;   // 쿠폰을 사용할 수 있는 시작 날짜

    @JsonSerialize(using = JsonLocalDateSerializer.class)
    @JsonDeserialize(using = JsonLocalDateDeserializer.class)
    private LocalDate endDate;   // 쿠폰을 사용할 수 있는 마감 날짜
}
