package com.logisall.anyware.core.domain.commerce.category;

public interface CategoryRepositoryCustom {
    boolean existsByNameAndKoKr(String name);

    boolean existsByNameAndEnUs(String name);

    boolean existsByNameAndZhCn(String name);

    boolean existsByNameAndZhTw(String name);

    boolean existsByNameAndJaJp(String name);
}
