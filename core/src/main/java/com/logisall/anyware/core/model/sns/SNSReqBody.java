package com.logisall.anyware.core.model.sns;

import com.logisall.anyware.core.model.BaseRequestBody;
import com.logisall.anyware.core.model.sns.SNSType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SNSReqBody extends BaseRequestBody {

  private static final long serialVersionUID = 5738785088672460354L;

  @Schema(description = "snsType", example = " FACEBOOK")
  private SNSType snsType; // sns 유형

  @Schema(description = "code", example = "String")
  private String code;

  @Schema(description = "redirectUri", example = "String")
  private String redirectUri;

  @Schema(description = "state", example = "String")
  private String state;

  @Schema(description = "idToken", example = "String")
  private String idToken;
}
