package com.logisall.anyware.core.model.sns;

import com.logisall.anyware.core.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Map;

@Getter
@Setter
@ToString
public class Kakao implements Serializable {

  private static final long serialVersionUID = -111117863280239714L;

  private String id;
  private String email;
  private String nickName;
  private String profileImage;
  private boolean kaccountEmailVerified;

  public static Kakao of(String id, String email, String nickName, String image, boolean emailVerified, Map<String, String> properties) {

    Kakao kakao = new Kakao();
    kakao.setId(id);
    kakao.setEmail(email);
    kakao.setNickName(nickName);
    kakao.setProfileImage(image);
    kakao.setKaccountEmailVerified(emailVerified);

    if (com.logisall.anyware.core.utils.StringUtils.isEmpty(kakao.getNickName())) {
      String _email = kakao.getEmail();
      if (StringUtils.isNotEmpty(_email)) {
        kakao.setNickName(_email.substring(0, _email.indexOf("@")));
      }
    }
    return kakao;
  }
}
