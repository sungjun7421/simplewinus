package com.logisall.anyware.core.domain.commerce.review;

import com.logisall.anyware.core.domain.commerce.point.QPoint;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

public class ReviewRepositoryImpl implements ReviewRepositoryCustom {

    @Autowired
    private JPAQueryFactory jpaQueryFactory;

    @Override
    public BigDecimal cacculateAvgStar() {

        BigDecimal temp = BigDecimal.valueOf(2);
        QReview qPReview = QReview.review;
        NumberExpression<?> expression = qPReview.star.add(qPReview.star.intValue());
        BigDecimal total = BigDecimal.valueOf(jpaQueryFactory
                .select(
                        expression.avg()
                )
                .from(qPReview)
                .fetchFirst());
        total = total.divide(temp,2);
        return total;
    }
}
