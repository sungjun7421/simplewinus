package com.logisall.anyware.core.domain.commerce.review;

import com.logisall.anyware.core.domain.commerce.paymentproduct.PaymentProduct;
import com.logisall.anyware.core.model.resbody.commerce.review.ReviewResBody;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.logisall.anyware.core.config.serializer.JsonLocalDateTimeSerializer;
import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.RestEntityBody;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.product.Product;
import com.logisall.anyware.core.domain.file.FileEntity;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.utils.DateUtils;
import com.logisall.anyware.core.utils.HtmlUtils;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Entity(name = "PReview")
@Getter
@Setter
@ToString(exclude = {"relativeWriter", "relativePaymentProduct"})
public class Review extends AbstractEntity<Long> implements RestEntityBody<ReviewResBody> {

  private static final long serialVersionUID = -8011138766880080512L;

  public static String[] IGNORE_PROPERTIES = {
      "id",
      "pageView",
      "relativeBuyer",
      "relativeOrderProduct"};

  @PrePersist
  public void prePersist() {
    if (StringUtils.isNotEmpty(this.content)) {
      this.content = HtmlUtils.convertLineSeparatorToBrTag(this.content);
    }
  }

  @PreUpdate
  public void preUpdate() {
    if (StringUtils.isNotEmpty(this.content)) {
      this.content = HtmlUtils.convertLineSeparatorToBrTag(this.content);
    }
  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @Column
  private String title; // 제목

  @Lob
  @Column(columnDefinition = "TEXT", length = 65535)
  private String content; // 내용

  @Lob
  @Column(columnDefinition = "TEXT", length = 65535)
  private String reply; // 사장님, 답변

  @DateTimeFormat(pattern = DateUtils.FORMAT_DATE_TIME_UNIT_BAR)
  @JsonSerialize(using = JsonLocalDateTimeSerializer.class)
  private LocalDateTime replyTime;

  @Column(columnDefinition = "DECIMAL(19,0) default 0")
  private BigDecimal star; // 별점

  @Column(columnDefinition = "BIT(1) default 1")
  private boolean active; // 활성/비활성

  @JsonIgnore
  @OrderBy("orderAscending ASC")
  @ManyToMany(cascade = {
      CascadeType.PERSIST,
      CascadeType.MERGE
  })
  @JoinTable(name = "ReviewToFile",
      joinColumns = @JoinColumn(name = "idReview", nullable = false, foreignKey = @ForeignKey(name = "FK_Review_For_Review_To_Image")),
      inverseJoinColumns = @JoinColumn(name = "idFile", nullable = false, foreignKey = @ForeignKey(name = "FK_Image_For_Review_To_Image"))
  )
  private List<FileEntity> images = new ArrayList<>();
  @Column(columnDefinition = "BIGINT(20) default 0")
  @Getter
  @Setter
  private long orderAscending; // 순서, 오름차순

  // == 로그인 데이터
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "idBuyer", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Writer_For_Review")) // Column name, 참조하는 ID(pk) name
  private Buyer relativeWriter; // 작성자 (로그인)

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "replyBy", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_WriterReply_For_Review"))
  private User relativeWriterReply;

  @JsonIgnore
  @OneToOne(mappedBy = "relativeReview",cascade = CascadeType.ALL, orphanRemoval = true)
  private PaymentProduct relativePaymentProduct;

  public Product getProduct() {
    if(this.relativePaymentProduct != null) {
      return this.relativePaymentProduct.getRelativeProduct();
    }
    return null;
  }

  @Override
  public void delete() {
    if (this.relativePaymentProduct != null) {
      this.relativePaymentProduct.setRelativeReview(null);
    }
  }

  @Override
  public void lazy() {
//    if (this.relativePaymentProduct != null) {
//      this.relativePaymentProduct.getId();
//    }
    if (this.relativeWriter != null) {
      this.relativeWriter.getId();
    }
  }

  @Override
  public ReviewResBody toBody(Locale locale) {
    return ReviewResBody.builder()
            .id(id)
            .title(this.title)
            .content(this.content)
            .star(this.star)
            .active(this.active)
            .images(images)
            .buyerName(this.relativePaymentProduct.getRelativeOrder().getBuyerInfo().getFullName())
            .buyerThumbnail(this.relativeWriter.getRelativeUser().getImage())
            .build();
  }

}
