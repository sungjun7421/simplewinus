package com.logisall.anyware.core.model.reqbody.commerce.point;

import com.logisall.anyware.core.model.BaseRequestBody;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GiftPointReqBody extends BaseRequestBody {

  private static final long serialVersionUID = 933071517684465443L;

  private String code;
}
