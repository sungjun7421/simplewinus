package com.logisall.anyware.core.model.resbody.commerce.review;

import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.file.FileEntity;
import com.logisall.anyware.core.model.BaseResponseBody;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.math.BigDecimal;
import java.util.List;

@Relation(value = "review", collectionRelation = "reviews")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReviewInfoResBody extends BaseResponseBody {

  private static final long serialVersionUID = 1079027401021595086L;

  private Long id;
  private String title;
  private String content;
  private BigDecimal star;
  private boolean active;
  private List<FileEntity> images;
  private Buyer relativeWriter;
}
