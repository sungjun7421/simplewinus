package com.logisall.anyware.core.service.board.faq;


import com.logisall.anyware.core.domain.board.faq.Faq;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.DomainService;
import org.springframework.data.domain.Page;

import java.util.Locale;

public interface FaqService extends DomainService<com.logisall.anyware.core.domain.board.faq.Faq, Long> {

    void changeOrder(Long id, String mode);
    com.logisall.anyware.core.domain.board.faq.Faq get(Locale locale, Long id, Boolean isActive);
    Page<Faq> page(Locale locale, Filter filter, Boolean isActive, Long idCategory);
}
