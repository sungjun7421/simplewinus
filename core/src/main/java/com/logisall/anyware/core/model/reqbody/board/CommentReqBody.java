package com.logisall.anyware.core.model.reqbody.board;

import com.logisall.anyware.core.domain.board.comment.Comment;
import com.logisall.anyware.core.model.BaseRequestBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@ToString
public class CommentReqBody extends BaseRequestBody {

  private static final long serialVersionUID = -4176441885291821448L;

  @NotNull
  @Schema(description = "댓글 유형")
  private Comment.Type type;

  @NotNull
  @Schema(description = "댓글 내용")
  private String Content;

  @NotNull
  @Schema(description = "게시물 ID")
  private Long idContent;

  @Schema(description = "부모 댓글 ID")
  private Long idParent;

}
