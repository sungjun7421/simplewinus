package com.logisall.anyware.core.service.map.naver;

import com.google.gson.Gson;
import com.logisall.anyware.core.model.map.naver.NaverGeoInfo;
import com.logisall.anyware.core.model.map.naver.NaverMapAddressInfo;
import com.logisall.anyware.core.model.property.MetaPlugin;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.concurrent.Future;

@Slf4j
@Service
public class MapNaverServiceImpl implements MapNaverService {

  @Autowired
  private MetaPlugin metaPlugin;

  @Autowired
  private OkHttpClient client;

  @Override
  public Future<com.logisall.anyware.core.model.map.naver.NaverGeoInfo> getNaverJusoInfo(String query) throws IOException {

    if (StringUtils.isEmpty(query)) {
      throw new NullPointerException("키워드를 입력하세요.");
    }

    String apiURL = "https://naveropenapi.apigw.ntruss.com/map-geocode/v2/geocode?query=" + query; //json


    String clientId = "3rqu6tfbjv";//애플리케이션 클라이언트 아이디값";
    String clientSecret = "axOrdfxVXlmlvOFxL8qoRh2S0kyPUVEdRzZ6kpaK";//애플리케이션 클라이언트 시크릿값";

    Request request = new Request.Builder()
        .addHeader("X-NCP-APIGW-API-KEY-ID", clientId)
        .addHeader("X-NCP-APIGW-API-KEY", clientSecret)
        .url(apiURL)
        .get()
        .build();

    Response response = client.newCall(request).execute();
    String result = response.body().string();

    Gson gson = new Gson();
    com.logisall.anyware.core.model.map.naver.NaverGeoInfo info = gson.fromJson(result, NaverGeoInfo.class);
    return new AsyncResult<>(info);
  }


  @Override
  public Future<com.logisall.anyware.core.model.map.naver.NaverMapAddressInfo> getNaverMapAddressInfo(String keyword) throws IOException {

    if (StringUtils.isEmpty(keyword)) {
      throw new NullPointerException("키워드를 입력하세요.");
    }

    String apiURL = "https://naveropenapi.apigw.ntruss.com/map-place/v1/search?query=" + keyword; //json


    String clientId = "3rqu6tfbjv";//애플리케이션 클라이언트 아이디값";
    String clientSecret = "axOrdfxVXlmlvOFxL8qoRh2S0kyPUVEdRzZ6kpaK";//애플리케이션 클라이언트 시크릿값" TODO 키값 생성 및 변경;

    Request request = new Request.Builder()
        .addHeader("X-NCP-APIGW-API-KEY-ID", clientId)
        .addHeader("X-NCP-APIGW-API-KEY", clientSecret)
        .url(apiURL)
        .get()
        .build();

    Response response = client.newCall(request).execute();
    String result = response.body().string();

    Gson gson = new Gson();
    com.logisall.anyware.core.model.map.naver.NaverMapAddressInfo info = gson.fromJson(result, NaverMapAddressInfo.class);
    return new AsyncResult<>(info);
  }
}
