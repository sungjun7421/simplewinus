package com.logisall.anyware.core.service.user;

import com.logisall.anyware.core.domain.user.Authority;

import java.util.List;

public interface AuthorityService {

  List<Authority> list();

  List<Authority> list(Authority.Role... roles);
}
