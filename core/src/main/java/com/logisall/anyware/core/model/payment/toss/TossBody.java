package com.logisall.anyware.core.model.payment.toss;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class TossBody implements Serializable {

    private static final long serialVersionUID = -8362134332311955585L;

    private String orderNo; // 토스몰 고유의 주문번호 (필수)
    private int amount; // 결제 금액 (필수)
    private int amountTaxFree = 0; // 비과세 금액 (필수)
    private String productDesc; // 상품 정보 (필수)
    private String apiKey; // 상점의 API Key (필수)
    private String retUrl; // 결제 완료 후 연결할 웹 URL (필수)
    private String retCancelUrl = "https://api.fooddeuk.com/toss/cancel"; // 결제 취소 시 연결할 웹 URL (필수)
    private boolean autoExecute = true; // 자동 승인 설정
    private String resultCallback = "https://api.fooddeuk.com/api/v1/toss/result"; // # 결제 결과 callback 웹 URL (필수-자동승인설정 true의 경우)
    private String retAppScheme;
//    private int amountTaxable; // # 결제 금액 중 과세금액
//    private int amountVat; // # 결제 금액 중 부가세
//    private int amountServiceFee; // # 결제 금액 중 봉사료
//    private String expiredTime; // # 결제 만료 예정 시각
//    private boolean cashReceipt; // # 현금영수증 발급 가능 여부
    // "metadata":"{'size':'XL','color':'Red'}"
}
