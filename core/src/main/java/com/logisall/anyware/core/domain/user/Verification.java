package com.logisall.anyware.core.domain.user;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

/**
 * TODO 실지적인 모바일/이메일 인증 로직 필요.
 */
@Schema(description = "이메일/모바일 인증")
@Setter
@Getter
@ToString
@Embeddable
public class Verification implements java.io.Serializable {

    private static final long serialVersionUID = 9109898567003441029L;

    @Schema(description = "이메일 인증", nullable = true)
    @Column(name = "vEmail", columnDefinition = "BIT(1) default 0")
    private boolean email; // 이메일 인증

    @Schema(description = "모바일 인증", nullable = true)
    @Column(name = "vMobile", columnDefinition = "BIT(1) default 0")
    private boolean mobile; // 모바일 인증

    @Transient
    @Schema(description = "모바일 인증 코드", nullable = true)
    private String token;
}

