package com.logisall.anyware.core.domain.commerce.buyerlevel;

import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel;

public interface BuyerLevelRepositoryCustom {
  BuyerLevel findByBuyerId(Long buyerId);
}
