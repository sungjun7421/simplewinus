package com.logisall.anyware.core.domain.document;


import com.logisall.anyware.core.domain.document.Document;
import com.logisall.anyware.core.domain.document.DocumentRepositoryCustom;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DocumentRepository extends
    PagingAndSortingRepository<Document, Long>,
    QuerydslPredicateExecutor<Document>,
    DocumentRepositoryCustom {

  @Query("from Document d where d.type = ?1")
  java.util.List<Document> lastType(Document.Type type, Pageable pageable);

}
