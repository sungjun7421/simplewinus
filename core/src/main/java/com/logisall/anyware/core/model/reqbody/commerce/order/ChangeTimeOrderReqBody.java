package com.logisall.anyware.core.model.reqbody.commerce.order;

import com.logisall.anyware.core.model.BaseRequestBody;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Setter
@Getter
@ToString
public class ChangeTimeOrderReqBody extends BaseRequestBody {

    private static final long serialVersionUID = 869782985129492595L;

    private String oid;
    private LocalDateTime time;
}
