package com.logisall.anyware.core.domain.commerce.stock;

import com.logisall.anyware.core.domain.commerce.deposit.QDeposit;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@NoArgsConstructor(staticName = "getInstance")
public class StockPredicate {

    private static final QDeposit Q_ENTITY = QDeposit.deposit;

    private BooleanBuilder builder = new BooleanBuilder();

    public Predicate values() {
        return builder.getValue() == null ? builder.and(Q_ENTITY.id.isNotNull()) : builder.getValue();
    }


    public com.logisall.anyware.core.domain.commerce.stock.StockPredicate search(String value) {

        if (!StringUtils.isEmpty(value)) {
            value = value.trim();
//            builder.and(Q_ENTITY.name.textEnUs.containsIgnoreCase(value)
//                    .or(Q_ENTITY.name.textJaJp.containsIgnoreCase(value))
//                    .or(Q_ENTITY.name.textKoKr.containsIgnoreCase(value))
//                    .or(Q_ENTITY.name.textZhCn.containsIgnoreCase(value))
//                    .or(Q_ENTITY.name.textZhTw.containsIgnoreCase(value)));
        }

        return this;

    }


    public com.logisall.anyware.core.domain.commerce.stock.StockPredicate startDate(final LocalDateTime startDate) {

        if (!StringUtils.isEmpty(startDate)) {
            builder.and(Q_ENTITY.createdDate.goe(startDate));
        }
        return this;
    }

    public com.logisall.anyware.core.domain.commerce.stock.StockPredicate endDate(final LocalDateTime endDate) {

        if (!StringUtils.isEmpty(endDate)) {
            builder.and(Q_ENTITY.createdDate.loe(endDate));
        }
        return this;
    }
}
