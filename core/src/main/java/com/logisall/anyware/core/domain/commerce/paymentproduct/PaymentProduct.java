package com.logisall.anyware.core.domain.commerce.paymentproduct;

import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.RestEntityThumbBody;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.domain.commerce.product.Product;
import com.logisall.anyware.core.domain.commerce.review.Review;
import com.logisall.anyware.core.model.resbody.commerce.paymentproduct.PaymentProductResBody;
import com.logisall.anyware.core.model.resbody.commerce.paymentproduct.PaymentProductThumbResBody;
import com.logisall.anyware.core.utils.DateUtils;
import com.google.common.collect.ImmutableMap;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
@ToString(exclude = {"relativeOrder"})
@NoArgsConstructor
public class PaymentProduct extends AbstractEntity<Long> implements RestEntityThumbBody<PaymentProductResBody, PaymentProductThumbResBody> {

    private static final long serialVersionUID = -7100737143933956178L;

    public static String[] IGNORE_PROPERTIES = {
            "id"
    };

    // 부분상품 상태
    @Getter
    public enum Status {
        CONFIRM("구매확정"), // default
        WAITING("상태대기중"),

        REFUND("부분 환불 신청"),
        REFUND_COMPLETE("부분 환불 완료"),

        CANCEL("취소"),
        CANCEL_COMPLETE("부분 취소 완료"),
        ;

        private final String value;

        Status(final String value) {
            this.value = value;
        }

        public static List<Map<String, String>> toListByMap() {
            return Arrays.stream(Status.values())
                .map(s -> ImmutableMap.of(s.getValue(), s.name()))
                .collect(Collectors.toList());
        }
    }

    public PaymentProduct(Order order, Product product) {
        this.relativeOrder = order;
        this.relativeProduct = product;
        this.status = Status.WAITING;
    }

    @PrePersist
    public void prePersist() {
        if (this.status == null) {
            this.status = Status.WAITING;
        }
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column
    private String productName; // 상품명 ({Product Name} - {Option Name})

    @Column(columnDefinition = "DECIMAL(19,0) default 0")
    private BigDecimal price; // 원가

    @Column(name = "addPrice", columnDefinition = "DECIMAL(19,0) default 0")
    private BigDecimal add; // 추가금액

    @Column(columnDefinition = "DECIMAL(19,0) default 0")
    private BigDecimal dc; // 판매 할인 금액

    @Column(columnDefinition = "DECIMAL(19,0) default 0")
    private BigDecimal hwdc; // 수기 할인

    @Column
    private Integer qty; // 주문 수량

    // (price + add - discount) X qty - hwdc = amount
    public BigDecimal getAmount() {
        if(this.price != null) {
            BigDecimal amount = this.price;

            // 추가금액
            if (this.add != null) {
                amount = this.price.add(this.add);
            }
            // (price - discount)
            if (this.dc != null) {
                amount = this.price.subtract(this.dc);
            }
            // (price - discount) X qty
            if (this.qty != null) {
                amount = amount.multiply(new BigDecimal(this.qty));
            } else {
                amount = BigDecimal.valueOf(0);
            }
            // (price - discount) X qty - hwdc
            if (this.hwdc != null) {
                amount = amount.subtract(this.hwdc);
            }
            return amount;
        }
        return BigDecimal.valueOf(0);
    }

    @Column(columnDefinition = "INT(11) default 0")
    private int savingPoint; // 적립 포인트

    // 취소 사유
    @Column
    private String cancelMessage;

    // 취소 신청 날짜
    @DateTimeFormat(pattern = DateUtils.FORMAT_DATE_TIME_UNIT_BAR)
    private LocalDateTime cancelDate;

    // 구매확정 날짜
    @DateTimeFormat(pattern = DateUtils.FORMAT_DATE_TIME_UNIT_BAR)
    private LocalDateTime completeDate;

    // 주문
    @ManyToOne
    @JoinColumn(name = "idOrder", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Order_For_Payment_Product"))
    private Order relativeOrder; // 주문/결제 내역 + 주문자 정보

    // 상품
    @ManyToOne
    @JoinColumn(name = "idProduct", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Product_For_Payment_Product")) // Column name
    private Product relativeProduct;

    // 리뷰
    @OneToOne
    @JoinColumn(name = "idReview", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Review_For_Payment_Product"))
    private Review relativeReview;

    public boolean hasReview() {
        return this.relativeReview != null
            && this.relativeReview.getId() != null
            && this.relativeReview.getId() != 0L;
    }

    @Override
    public void delete() {

    }

    @Override
    public void lazy() {

    }

    @Override
    public PaymentProductResBody toBody(Locale locale) {
        return PaymentProductResBody.builder()
            .id(this.id)
            .status(this.status.getValue())
            .productName(this.productName)
            .price(this.price)
            .dc(this.dc)
            .hwdc(this.hwdc)
            .amount(this.getAmount())
            .savingPoint(this.savingPoint)
            .cancelMessage(this.cancelMessage)
            .cancelDate(this.cancelDate)
            .build();
    }

    @Override
    public PaymentProductThumbResBody toThumbBody(Locale locale) {
        return PaymentProductThumbResBody.builder().build();
    }
}
