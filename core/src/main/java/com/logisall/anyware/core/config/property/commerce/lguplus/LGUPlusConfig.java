package com.logisall.anyware.core.config.property.commerce.lguplus;

import lgdacom.XPayClient.XPayClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@Slf4j
@Configuration
public class LGUPlusConfig {

  private static final String CERTIFICATE_PATH_WINDOWS = System.getProperty("user.home") + File.separator + "Documents" + File.separator + "myKey" + File.separator + ".cacerts"; // 감덕인이 패스 맞게 설정
  private static final String CERTIFICATE_PATH_MAC = System.getProperty("user.home") + File.separator + "Documents" + File.separator + "myKey" + File.separator + ".cacerts"; // 장종례/김진영 패스 맞게 설정

  @Autowired
  private LGUPlus lguPlus;

  @Bean
  public XPayClient xPayClient() {

    log.debug("> lguPlus : {}", lguPlus);

    XPayClient xPayClient = new XPayClient();
    xPayClient.SetConfig("url", lguPlus.getUrl().getService());
    xPayClient.SetConfig("test_url", lguPlus.getUrl().getTest());
    xPayClient.SetConfig("aux_url", lguPlus.getUrl().getAux());

    xPayClient.SetConfig(lguPlus.getMall().getId(), lguPlus.getMall().getMertKey());
    xPayClient.SetConfig("t" + lguPlus.getMall().getId(), lguPlus.getMall().getMertKey());
    xPayClient.SetConfig("server_id", lguPlus.getMall().getServerId());
    xPayClient.SetConfig("timeout", lguPlus.getMall().getTimeout());
    xPayClient.SetConfig("log_level", lguPlus.getMall().getLogLevel());
    xPayClient.SetConfig("verify_cert", lguPlus.getMall().getVerifyCert());
    xPayClient.SetConfig("verify_host", lguPlus.getMall().getVerifyHost());

    xPayClient.SetConfig("report_error", lguPlus.getMall().getReportError());
    xPayClient.SetConfig("output_UTF8", lguPlus.getMall().getOutputUtf8());
    xPayClient.SetConfig("auto_rollback", lguPlus.getMall().getAutoRollback());

//        if (OSUtils.isMac()) {
//            xPayClient.SetConfig("keystore_cacerts_dir", CERTIFICATE_PATH_MAC);
//        } else if (OSUtils.isWindows()) {
//            xPayClient.SetConfig("keystore_cacerts_dir", CERTIFICATE_PATH_WINDOWS);
//        } else if (OSUtils.isLinux()) {
//            xPayClient.SetConfig("keystore_cacerts_dir", lguPlus.getMall().getKeystoreCacertsDir());
//        }

    return xPayClient;
  }
}
