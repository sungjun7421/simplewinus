package com.logisall.anyware.core.service.map.google;

import com.logisall.anyware.core.service.map.google.MapGoogleService;
import com.google.gson.Gson;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.embedd.GPS;
import com.logisall.anyware.core.model.map.google.autocomplete.AutoCompleteByPlace;
import com.logisall.anyware.core.model.map.google.placedetail.PlaceDetail;
import com.logisall.anyware.core.model.map.google.placedetail.result.components.Address;
import com.logisall.anyware.core.model.map.google.textsearch.TextSearch;
import com.logisall.anyware.core.model.property.MetaPlugin;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MapGoogleServiceImpl implements MapGoogleService {


  @Autowired
  private OkHttpClient client;

  @Autowired
  private MetaPlugin metaPlugin;

  @Async
  @Override
  public Future<GPS> getLocationInfo(String address) throws InterruptedException, IOException {
    if (StringUtils.isEmpty(address)) {
      throw new BadRequestException();
    }

    URI uri = UriComponentsBuilder.newInstance().scheme("https").host("maps.googleapis.com")
        .path("/maps/api/geocode/json")
        .queryParam("sensor", false)
        .queryParam("language", "ko")
        .queryParam("key", metaPlugin.getGoogleMap())
        .queryParam("address", address.replaceAll(" ", "+"))
        .build()
        .toUri();

    log.debug("> uri : {}", uri);

//    ResponseEntity<Map> entity = new RestTemplate().getForEntity(uri, Map.class);
//    if (entity.getStatusCode() != HttpStatus.OK) {
//      throw new RuntimeException("API 문제 발생");
//    }
//
//    Map<String, Object> map = entity.getBody();

    Request request = new Request.Builder()
        .url(uri.toURL())
        .get()
        .build();

    Response response = client.newCall(request).execute();
    String result = response.body().string();

    Gson gson = new Gson();
    Map<String, Object> map = gson.fromJson(result, Map.class);

    String status = map.get("status").toString();

    log.debug("map : {}", map);
    log.debug(status);
    if (status.equals("ZERO_RESULTS")) {
//      throw new NullPointerException("존재하지 않은 주소입니다.");
    } else if (!status.equals("OK")) {
      throw new RuntimeException("API 문제 발생");
    }

    GPS gps = new GPS();

    List<Map<String, Object>> list = (List<Map<String, Object>>) map.get("results");
    list.forEach(item -> {

      Map geometry = (Map) item.get("geometry");
      Map location = (Map) geometry.get("location");

      double latitude = Double.valueOf(location.get("lat").toString());
      double longitude = Double.valueOf(location.get("lng").toString());

      gps.setLatitude(latitude);
      gps.setLongitude(longitude);
    });
    return new AsyncResult<>(gps);
  }

  @Async
  @Override
  public Future<TextSearch> getPlaceSearch(String query, String language) throws InterruptedException {
    if (StringUtils.isEmpty(query)) {
      throw new BadRequestException();
    }

    URI uri = UriComponentsBuilder.newInstance().scheme("https").host("maps.googleapis.com")
        .path("/maps/api/place/textsearch/json")
        .queryParam("key", metaPlugin.getGooglePlace())
        .queryParam("query", query)
        .queryParam("language", language)
        .build()
        .toUri();

    try {
      log.debug("uri ::: {}", uri.toURL().toString());
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }

    ResponseEntity<TextSearch> entity = new RestTemplate().getForEntity(uri, TextSearch.class);

    if (entity.getStatusCode() != HttpStatus.OK) {
      throw new RuntimeException("API 문제 발생");
    }

    TextSearch textSearch = entity.getBody();

    if (textSearch.getStatus() == TextSearch.Status.ZERO_RESULTS) {
      log.error("getPlaceSearch ::: getStatusCode() ::: {}", entity.getStatusCode());
//      throw new NullPointerException("존재하지 않은 주소입니다.");
    } else if (textSearch.getStatus() != TextSearch.Status.OK) {
      log.error("textSearch.getStatus() ::: {} , getStatusCode() ::: {}", textSearch.getStatus(), entity.getStatusCode());
      throw new RuntimeException("API 문제 발생");
    }

    log.debug("entity.getStatusCode() ::: {}", entity.getStatusCode());
    log.debug("entity.toString() ::: {}", textSearch);

    return new AsyncResult<>(textSearch);
  }

  @Override
  public Future<AutoCompleteByPlace> getAutoComplete(String keyword, String language) throws InterruptedException {
    if (StringUtils.isEmpty(keyword)) {
      throw new BadRequestException();
    }

    URI uri = UriComponentsBuilder.newInstance().scheme("https").host("maps.googleapis.com")
        .path("/maps/api/place/autocomplete/json")
        .queryParam("key", metaPlugin.getGooglePlace())
        .queryParam("input", keyword)
        .queryParam("language", language)
        .build()
        .toUri();

    try {
      log.debug("uri ::: {}", uri.toURL().toString());
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }

    ResponseEntity<AutoCompleteByPlace> entity = new RestTemplate().getForEntity(uri, AutoCompleteByPlace.class);

    if (entity.getStatusCode() != HttpStatus.OK) {
      throw new RuntimeException("API 문제 발생");
    }

    AutoCompleteByPlace autoComplete = entity.getBody();

    if (autoComplete.getStatus() == AutoCompleteByPlace.Status.ZERO_RESULTS) {
      log.error("getAutoComplete ::: getStatusCode() ::: {}", entity.getStatusCode());
//      throw new NullPointerException("존재하지 않은 주소입니다.");
    } else if (autoComplete.getStatus() != AutoCompleteByPlace.Status.OK) {
      log.error("getAutoComplete status ::: {} getStatusCode() ::: {}", autoComplete.getStatus(), entity.getStatusCode());
      throw new RuntimeException("API 문제 발생");
    }

    log.debug("entity.getStatusCode() ::: {}", entity.getStatusCode());
    log.debug("entity.toString() ::: {}", autoComplete);

    return new AsyncResult<>(autoComplete);
  }

  @Async
  @Override
  public Future<PlaceDetail> getPlaceDetail(String place_id, String language) throws InterruptedException {
    if (StringUtils.isEmpty(place_id)) {
      throw new BadRequestException();
    }

    URI uri = UriComponentsBuilder.newInstance().scheme("https").host("maps.googleapis.com")
        .path("/maps/api/place/details/json")
        .queryParam("key", metaPlugin.getGooglePlace())
        .queryParam("placeid", place_id)
        .queryParam("language", language)
        .build()
        .toUri();

    try {
      log.debug("uri ::: {}", uri.toURL().toString());
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }

    ResponseEntity<PlaceDetail> entity = new RestTemplate().getForEntity(uri, PlaceDetail.class);

    if (entity.getStatusCode() != HttpStatus.OK) {
      throw new RuntimeException("API 문제 발생");
    }

    PlaceDetail placeDetail = entity.getBody();

//        if (placeDetail.getStatus() == PlaceDetail.Status.ZERO_RESULTS) {
//            throw new NullPointerException("존재하지 않은 주소입니다.");
//        }
//
//        if (placeDetail.getStatus() != PlaceDetail.Status.OK) {
//            throw new RuntimeException("API 문제 발생");
//        }

    log.debug("entity.getStatusCode() ::: {}", entity.getStatusCode());
    log.debug("entity.toString() ::: {}", placeDetail);

    // Postal Code
    if (placeDetail.getResult() != null && placeDetail.getResult().getAddress_components() != null) {
      log.debug("1");
      List<String> postalCodes = placeDetail.getResult().getAddress_components().stream()
          .filter(address -> {
            log.debug("2");
            if (address.getTypes() != null) {
              return address.getTypes().stream().anyMatch(type -> type.equals("postal_code"));
            }
            return false;
          }).map(Address::getLong_name)
          .collect(Collectors.toList());

      log.debug("3");
      if (postalCodes != null && postalCodes.size() > 0) {
        log.debug("4 ::: {}", postalCodes.get(0));
        placeDetail.getResult().setPostalCode(postalCodes.get(0));
      }
    }

    log.debug("### entity.toString() ::: {}", placeDetail);

    return new AsyncResult<>(placeDetail);
  }
}
