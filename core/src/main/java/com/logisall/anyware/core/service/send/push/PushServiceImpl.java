package com.logisall.anyware.core.service.send.push;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.device.Device;
import com.logisall.anyware.core.domain.device.DeviceRepository;
import com.logisall.anyware.core.domain.push.Push;
import com.logisall.anyware.core.domain.push.PushPredicate;
import com.logisall.anyware.core.domain.push.PushRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.fcm.FCMReqBody;
import com.logisall.anyware.core.model.reqbody.etc.push.PushReqBody;
import com.logisall.anyware.core.service.device.DeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class PushServiceImpl implements PushService {

    public static String TOPIC_NAME = "Boilerplate";

    @Autowired
    private PushRepository pushRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DeviceService deviceService;

    @Override
    @Transactional
    public com.logisall.anyware.core.domain.push.Push create(com.logisall.anyware.core.model.reqbody.etc.push.PushReqBody pushReqBody, String email) {
        String response = this.sendMessageToDevice(pushReqBody);
        if (!response.equals("200")) {
            throw new com.logisall.anyware.core.config.exception.BadRequestException();
        }

        com.logisall.anyware.core.domain.device.Device device = deviceService.get(pushReqBody.getDeviceToken());

        if (device == null) {
            throw new com.logisall.anyware.core.config.exception.BadRequestException("device null");
        }

        com.logisall.anyware.core.domain.push.Push push = new com.logisall.anyware.core.domain.push.Push();

        push.setTitle(pushReqBody.getTitle());
        push.setContent(pushReqBody.getContent());
        push.setRelativeDevice(device);

        return pushRepository.save(push);
    }

    @Override
    @Transactional(readOnly = true)
    public com.logisall.anyware.core.domain.push.Push get(Long id) {

        return pushRepository.findById(id)
            .map(push -> {
                push.lazy();
                return push;
            }).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<com.logisall.anyware.core.domain.push.Push> page(Filter filter) {

        Page<Push> page = pushRepository.findAll(
            PushPredicate.getInstance()
                .search(filter.getQuery())
                .startDate(filter.getStartDate())
                .endDate(filter.getEndDate())
                .values(),
            filter.getPageable()
        );

        page.forEach(push -> {
            push.lazy();
        });
        return page;
    }

    @Override
    @Transactional
    public String sendMessageToDevice(PushReqBody pushReqBody) {
        Message message = Message.builder()
            .putData("title", pushReqBody.getTitle())
            .putData("body", pushReqBody.getContent())
            .setToken(pushReqBody.getDeviceToken())
            .build();

        String response = null;
        try {
            response = FirebaseMessaging.getInstance().send(message);
        } catch (FirebaseMessagingException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    @Transactional
    public void subscribeToTopic(String deviceToken) {
        try {
            FirebaseMessaging.getInstance().subscribeToTopic(Arrays.asList(deviceToken), TOPIC_NAME);
            com.logisall.anyware.core.domain.device.Device device = deviceService.get(deviceToken);
            if (device != null) {
                device.setSubscribe(true);
                deviceRepository.save(device);
            }
        } catch (FirebaseMessagingException e) {
            throw new com.logisall.anyware.core.config.exception.BadRequestException("Firebase subscribe to topic fail");
        }
    }

    @Override
    @Transactional
    public void unsubscribeFromTopic(String deviceToken) {
        try {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(Arrays.asList(deviceToken), TOPIC_NAME);
            com.logisall.anyware.core.domain.device.Device device = deviceService.get(deviceToken);
            if (device != null) {
                device.setSubscribe(false);
                deviceRepository.save(device);
            }
        } catch (FirebaseMessagingException e) {
            throw new BadRequestException("Firebase unsubscribe from topic fail");
        }
    }

    @Override
    @Transactional
    public String sendMessageToAllDevice(FCMReqBody fcmReqBody) {
        List<Device> devices = deviceRepository.findAllBySubscribe(true);
        List<String> deviceTokens = new ArrayList<>();
        devices.forEach(device -> {
            deviceTokens.add(device.getDeviceToken());
        });

        try {
//            TopicManagementResponse responseTopic = FirebaseMessaging.getInstance().subscribeToTopic(
//                    deviceTokens, TOPIC_NAME);
//
//            log.info("PushServiceImpl > responseTopic ::: {}", responseTopic);
//            System.out.println(responseTopic.getSuccessCount() + " tokens were subscribed successfully");

//      Message message = Message.builder()
//          .setTopic(TOPIC_NAME)
////                    .setNotification(new com.google.firebase.messaging.Notification(fcmReqBody.getTitle()))
//          .putData("title", fcmReqBody.getTitle())
//          .putData("body", fcmReqBody.getContent())
//          .build();
//
//      String response = null;
//      response = FirebaseMessaging.getInstance().send(message);
//      log.info("PushServiceImpl > response ::: {}", response);
//
//      Push push = new Push();
//      push.setContent(fcmReqBody.getContent());
//      push.setRelativeDevice(device);
//      push.setPushTime(LocalDateTime.now());
//      pushRepository.save(push);

            return "200";
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("PushServiceImpl > error ::: {}", ex.toString());
            return "500";
        }
    }
}
