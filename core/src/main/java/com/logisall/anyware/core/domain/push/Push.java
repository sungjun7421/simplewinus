package com.logisall.anyware.core.domain.push;


import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.device.Device;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;

@Slf4j
@Entity(name = "PushHistory")
@Getter
@Setter
@ToString(exclude = {"relativeDevice"})
@NoArgsConstructor
public class Push extends AbstractEntity<Long> {

    private static final long serialVersionUID = 5319856894503393640L;

    public static String[] IGNORE_PROPERTIES = {
        "id",
        "relativeDevice"
    };

    @PrePersist
    public void prePersist() {

    }

    @PreUpdate
    public void PreUpdate() {

    }

    @Id
    @GeneratedValue
    @Column(unique = true)
    private Long id;

    @Column(columnDefinition = "LONGTEXT")
    private String title;

    @Column(columnDefinition = "LONGTEXT")
    private String content;

    @ManyToOne
    @JoinColumn(name = "idDevice", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Device_For_PushHistory"))
    private Device relativeDevice;

    @Override
    public void delete() {

    }

    @Override
    public void lazy() {

    }
}
