package com.logisall.anyware.core.service.send.email;

import com.logisall.anyware.core.domain.email.EmailHistory;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.DomainService;
import org.springframework.data.domain.Page;

public interface EmailHistoryService extends DomainService<EmailHistory, Long> {

    // Page
    Page<EmailHistory> page(Filter filter, EmailHistory.Type type);
}
