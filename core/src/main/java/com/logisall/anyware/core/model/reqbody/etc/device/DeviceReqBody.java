package com.logisall.anyware.core.model.reqbody.etc.device;

import com.logisall.anyware.core.model.BaseRequestBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;

@Setter
@Getter
@ToString
public class DeviceReqBody extends BaseRequestBody {

    private static final long serialVersionUID = 6086550971166053340L;

    @Schema(description = "Device Token")
    private String deviceToken;

    @Email
    @Schema(description = "이메일 주소", example = "test@logisall.com", minLength = 3, maxLength = 255)
    private String email;
}
