package com.logisall.anyware.core.model.resbody.account;

import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel;
import com.logisall.anyware.core.domain.user.Authority;
import com.logisall.anyware.core.domain.user.TermsAgree;
import com.logisall.anyware.core.domain.user.Verification;
import com.logisall.anyware.core.model.BaseResponseBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.server.core.Relation;

import java.util.List;

@Relation(value = "me")
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MeResBody extends BaseResponseBody {

  private static final long serialVersionUID = 4536454965313232901L;

  @Schema(description = "ID")
  private Long id;

  @Schema(description = "이메일")
  private String email;

  @Schema(description = "이름")
  private String fullName;

  @Schema(description = "휴대폰번호")
  private String mobile;

  @Schema(description = "권한")
  private List<Authority.Role> roles;

  @Schema(description = "이미지")
  private String image; // 이미지

  @Schema(description = "SMS/이메일/카카오톡/App Push 수신동의")
  private TermsAgree termsAgree; // SMS 수신동의 or 이메일 수신동의

  @Schema(description = "이메일/휴대폰번호 인증")
  private Verification verification; // 이메일 전화번호 인증 [인증 채크 별개]

  @Schema(description = "비밀번호 변경 만료 상태")
  private boolean expiredPassword; // 비밀번호 변경 만료 상태

  @Schema(description = "비밀번호가 설정되어 있는지 상태값")
  private boolean hasPassword; // 비밀번호가 설정되어 있는지 상태값

  @Schema(description = "휴면계정")
  private boolean dormancy; // 휴면계정

  @Schema(description = "총 보유 적립금")
  private int totalPoint;

  @Schema(description = "만료될 적립급 (이번달)")
  private int expiredPoint;

  @Schema(description = "회원등급")
  private BuyerLevel buyerLevel;
}
