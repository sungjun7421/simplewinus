package com.logisall.anyware.core.domain.setting;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AppSettingRepository extends
    PagingAndSortingRepository<com.logisall.anyware.core.domain.setting.AppSetting, Long>,
    QuerydslPredicateExecutor<AppSetting> {
}
