package com.logisall.anyware.core.domain.document;

import com.logisall.anyware.core.domain.document.Document;

public interface DocumentRepositoryCustom {

  Document latestDoc(Document.Type type);
}
