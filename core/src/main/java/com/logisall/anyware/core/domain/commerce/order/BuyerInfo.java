package com.logisall.anyware.core.domain.commerce.order;

import com.logisall.anyware.core.config.security.CryptoAESConverter;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@ToString
@Embeddable
public class BuyerInfo implements Serializable {
    private static final long serialVersionUID = 221766229592938438L;

    @PrePersist
    public void prePersist() {
    }

    @PreUpdate
    public void PreUpdate() {
    }

    // 주문자 정보
    @Column
    @Convert(converter = CryptoAESConverter.class)
    private String fullName; // 이름

    @Column
    @Convert(converter = CryptoAESConverter.class)
    private String mobile; // 연락처

    @Column
    @Convert(converter = CryptoAESConverter.class)
    private String email; // 이메일

    public boolean isEmpty() {

        return StringUtils.isEmpty(fullName)
                || StringUtils.isEmpty(mobile)
                || StringUtils.isEmpty(email);
    }
}
