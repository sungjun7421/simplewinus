package com.logisall.anyware.core.service.board.event;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.crud.UpdateErrorException;
import com.logisall.anyware.core.domain.board.event.Event;
import com.logisall.anyware.core.domain.board.event.EventPredicate;
import com.logisall.anyware.core.domain.board.event.EventRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.admin.EventInfoResBody;
import com.logisall.anyware.core.service.board.event.EventService;
import com.logisall.anyware.core.service.setting.AppSettingService;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class EventServiceImpl implements EventService {

  @Value("${app.client.host}")
  private String clientHost;

  @Autowired
  private EventRepository eventRepository;

  @Autowired
  private AppSettingService appSettingService;

  @Autowired
  private PagedResourcesAssembler pagedResourcesAssembler;

  private void updateByTopTrue(Event event) {
    if (event.isTop()) {
      Optional.ofNullable(eventRepository.findByTopTrue())
        .ifPresent(byTopTrue -> {
          byTopTrue.setTop(false);
          eventRepository.save(byTopTrue);
        });
    }
  }


  @Override
  @Transactional
  public Event create(Event event) {
    this.updateByTopTrue(event); // 상단고정 1개만 적용되게
    return eventRepository.save(event);
  }

  @Override
  @Transactional
  public Event update(Event event) {

    if (event.getId() == null) {
      throw new BadRequestException();
    }

    return eventRepository.findById(event.getId())
      .map(ori -> {
        this.updateByTopTrue(event); // 상단고정 1개만 적용되게
        BeanUtils.copyProperties(event, ori, Event.IGNORE_PROPERTIES);
        return eventRepository.save(ori);
      }).orElseThrow(() -> new UpdateErrorException(event.getId(), Event.class.getName()));
  }

  @Override
  @Transactional
  public void delete(Long id) {
    eventRepository.findById(id)
      .ifPresent(event -> {
        event.delete();
        eventRepository.delete(event);
      });
  }

  @Override
  @Transactional(readOnly = true)
  public Event get(Locale locale, Long id) {
    return eventRepository.findById(id)
      .map(event -> {
        event.lazy();
        event.setLocale(locale);
        return event;
      }).orElse(null);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<Event> page(Locale locale, Filter filter) {

    Page<Event> page = eventRepository.findAll(
        EventPredicate.getInstance()
            .search(filter.getQuery())
            .startDate(filter.getStartDate())
            .endDate(filter.getEndDate())
            .values(),
        filter.getPageable());

    page.forEach(event -> {
      event.lazy();
      event.setLocale(locale);
    });
    return page;
  }

  @Override
  @Transactional(readOnly = true)
  public Page<Event> page(Locale locale, Filter filter, Boolean active) {

    Page<Event> page = eventRepository.findAll(
            EventPredicate.getInstance()
              .search(filter.getQuery())
              .startDate(filter.getStartDate())
              .endDate(filter.getEndDate())
              .active(active)
              .values(),
      filter.getPageable());

    page.forEach(event -> {
      event.lazy();
      event.setLocale(locale);
    });
    return page;
  }

  @Override
  @Transactional
  public List<Event> list(Locale locale) {
    return Lists.newArrayList(eventRepository.findAll());
  }

  @Override
  @Transactional
  public List<EventInfoResBody> list(String query) {

    List<Event> list = Lists.newArrayList(eventRepository.findAll(
        EventPredicate.getInstance()
            .search(query)
            .values()));

    return list.stream().map(event -> {
      EventInfoResBody ev = new EventInfoResBody();
      ev.setId(event.getId());
      ev.setTitle(event.getTitle());
      ev.setContent(event.getContent());
      return ev;
    }).collect(Collectors.toList());
  }
//  @Override
//  @Transactional
//  public void countUpByPageView(Long id) {
//    eventRepository.findById(id)
//      .ifPresent(event -> {
//        event.setPageView(event.getPageView() + 1);
//      });
//  }

//  @Override
//  @Transactional
//  public List<EventInfo> list(Locale locale, String query) {
//    return eventRepository.list(locale, query);
//  }
//
//  @Override
//  @Transactional
//  public PagedResources<EventThumbBody> pagedResources(Locale locale, Filter filter, Long idMHC, Mall mall) {
//
//    Long idMall = mall.getId();
//    Locale defaultLocale = mall.getDefaultLocale();
//
//    Page<EBEvent> page = eventRepository.findAll(
//      ExpressionUtils.allOf(
//        EBEventPredicate.search(filter.getQuery()),
//        EBEventPredicate.startDate(filter.getStartDate()),
//        EBEventPredicate.endDate(filter.getEndDate()),
//        EBEventPredicate.home(idMHC),
//        EBEventPredicate.active(true),
//        EBEventPredicate.mall(idMall),
//        EBEventPredicate.locale(locale, defaultLocale)
//      ),
//      filter.getPageable());
//
//    List<EventThumbBody> list = page.getContent().stream()
//      .map(ebEvent -> ebEvent.toEventThumbBody(locale))
//      .collect(Collectors.toList());
//
//    return pagedResourcesAssembler.toResource(new PageImpl<>(list, filter.getPageable(), page.getTotalElements()));
//  }
//
//  @Override
//  @Transactional
//  public Resource<EventResBody> resource(Locale locale, Long id) {
//
//    EBEvent event = this.get(locale, id);
//
//    return new Resource<>(event.toEventResBody(locale));
//  }
//
//  @Override
//  @Transactional
//  public Resources<Resource<EventThumbBody>> resourcesByMobileHome(Locale locale, Long idHome, Mall mall) {
//
//    Long idMall = mall.getId();
//    Locale defaultLocale = mall.getDefaultLocale();
//
//    List<EBEvent> list = Lists.newArrayList(eventRepository.findAll(
//      ExpressionUtils.allOf(
//        EBEventPredicate.home(idHome),
//        EBEventPredicate.active(true),
//        EBEventPredicate.mall(idMall),
//        EBEventPredicate.locale(locale, defaultLocale)
//      ),
//      new Sort(Sort.Direction.DESC, "createdDate")
//    ));
//
//    return Resources.wrap(list.stream()
//      .map(ebEvent -> ebEvent.toEventThumbBody(locale))
//      .collect(Collectors.toList()));
//  }
}
