package com.logisall.anyware.core.domain.user;

import com.logisall.anyware.core.domain.user.Authority;
import com.logisall.anyware.core.domain.user.AuthorityRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface AuthorityRepository extends
    JpaRepository<Authority, Long>,
    QuerydslPredicateExecutor<Authority>,
    AuthorityRepositoryCustom {

  Authority findByRole(Authority.Role role);

}
