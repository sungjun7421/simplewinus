package com.logisall.anyware.core.config.property.commerce.paypal;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "pay-module.pay-pal")
public class PayPalConfig implements Serializable {

  private static final long serialVersionUID = 8592038183977851044L;

  private String clientId;
  private String secret;
  private String apiUrl;
  private String jsUrl;
}
