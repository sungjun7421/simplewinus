package com.logisall.anyware.core.model.reqbody.commerce.wish;

import javax.validation.constraints.NotNull;

public class ToggleWishRequestBody {
    @NotNull()
    private Long productId;

    public Long getProductId() {
        return productId;
    }
}
