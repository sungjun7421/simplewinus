package com.logisall.anyware.core.domain.setting;

import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.International.InternationalMode;
import com.logisall.anyware.core.domain.RestEntityBody;
import com.logisall.anyware.core.domain.setting.ComponentMode;
import com.logisall.anyware.core.model.resbody.setting.AppSettingResBody;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Locale;

@Entity
@Getter
@Setter
@ToString
public class AppSetting extends AbstractEntity<Long> implements RestEntityBody<AppSettingResBody> {

  private static final long serialVersionUID = 3667972003401550990L;

  public static String[] IGNORE_PROPERTIES = {"id"};

  @PrePersist
  public void prePersist() {
    if (this.defaultLocale == null) {
      this.setDefaultLocale(Locale.KOREA);
    }
  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  /**
   * 언어 설정 (국제화)
   */
  @Column
  private Locale defaultLocale;

  @Column
  private boolean international; // 국제화 모드

  @Embedded
  private InternationalMode internationalMode;

  @Embedded
  private ComponentMode componentMode;

  @Override
  public void delete() {

  }

  @Override
  public void lazy() {

  }

  @Override
  public AppSettingResBody toBody(Locale locale) {
    return AppSettingResBody.builder()
        .defaultLocale(defaultLocale)
        .international(international)
        .internationalMode(internationalMode)
        .build();
  }
}
