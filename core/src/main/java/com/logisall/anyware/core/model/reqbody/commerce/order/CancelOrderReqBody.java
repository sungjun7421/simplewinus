package com.logisall.anyware.core.model.reqbody.commerce.order;

import com.logisall.anyware.core.model.BaseRequestBody;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class CancelOrderReqBody extends BaseRequestBody {

    private static final long serialVersionUID = 3076832767580028718L;

    private String oid;

}
