package com.logisall.anyware.core.service.commerce.wish;

import com.logisall.anyware.core.domain.commerce.wish.Wish;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.resbody.ResultResBody;
import com.logisall.anyware.core.model.resbody.commerce.product.ProductResBody;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Locale;

public interface WishService {
    com.logisall.anyware.core.domain.commerce.wish.Wish create(com.logisall.anyware.core.domain.commerce.wish.Wish wish);
    void delete(com.logisall.anyware.core.domain.commerce.wish.Wish.Id id);

    // Rest
    CollectionModel<EntityModel<ProductResBody>> resources(Locale locale, Long idUser);
    ResultResBody toggle(Long idShow, Long idUser);
    void deleteList(List<Long> showIds, Long idUser);
    Page<Wish> page(Filter filter, Long productId, Long userIdCanNullAble, boolean byProduct);
}
