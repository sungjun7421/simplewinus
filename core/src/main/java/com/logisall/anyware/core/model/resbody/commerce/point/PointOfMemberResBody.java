package com.logisall.anyware.core.model.resbody.commerce.point;

import com.logisall.anyware.core.model.BaseResponseBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.math.BigDecimal;

@Schema(description = "적립금(Point)")
@Relation(value = "point", collectionRelation = "points")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PointOfMemberResBody extends BaseResponseBody {

    private static final long serialVersionUID = 1444072863161990463L;

    @Schema(description = "ID Buyer")
    private Long idBuyer;

    @Schema(description = "보유 총 적립금")
    private int totalPoint;

    @Schema(description = "적립 예정")
    private BigDecimal expected;

    @Schema(description = "적립 만료 포인트")
    private int expirePoint;
}
