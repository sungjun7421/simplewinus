package com.logisall.anyware.core.domain.board.qna;

import com.logisall.anyware.core.config.security.CryptoAESConverter;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

// 비회원
@Setter
@Getter
@ToString
@Embeddable
public class QnaNoMember implements Serializable {

  private static final long serialVersionUID = -7751720516339490382L;

  @PrePersist
  public void prePersist() {
  }

  @PreUpdate
  public void PreUpdate() {
  }

  @Schema(description = "이름")
  @Column(length = 255)
  @Convert(converter = CryptoAESConverter.class)
  private String fullName;

  @Pattern(regexp = ValidUtils.PATTERN_MOBILE)
  @Schema(description = "휴대폰 번호", example = "01011112222", minLength = 10, maxLength = 11)
  @Column(length = 255)
  @Convert(converter = CryptoAESConverter.class)
  private String mobile;

  @Email
  @Schema(description = "이메일 주소", example = "test@logisall.com", minLength = 3, maxLength = 255)
  @Convert(converter = CryptoAESConverter.class)
  private String email;

  @Schema(description = "비밀번호", example = "abcd1234", minLength = 6, maxLength = 30)
  @Column
  private String password;
}
