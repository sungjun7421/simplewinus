package com.logisall.anyware.core.domain.commerce.buyerlevel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface BuyerLevelRepository extends
    JpaRepository<com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel, Long>,
    QuerydslPredicateExecutor<com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel>,
    BuyerLevelRepositoryCustom {

    List<com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel> findByOrderByLevelAsc();
    BuyerLevel findByLevel(int level);
}
