package com.logisall.anyware.core.service.account;

import com.logisall.anyware.core.config.property.sns.FacebookApiKey;
import com.logisall.anyware.core.domain.user.SocialId;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.domain.user.sns.SNSStatus;
import com.logisall.anyware.core.model.sns.Facebook;
import com.logisall.anyware.core.service.restapi.RestAPIService;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
public class SNSFacebookAPIServiceImpl implements SNSFacebookAPIService {

  @Autowired
  private FacebookApiKey apiKey;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private RestAPIService restAPIService;

  @Override
  public Map getAccessToken(String code, String redirectUri) {

    URI uri = UriComponentsBuilder.fromHttpUrl(apiKey.getAccessTokenUri())
        .queryParam("client_id", apiKey.getClientId())
        .queryParam("client_secret", apiKey.getClientSecret())
        .queryParam("code", code)
        .queryParam("redirect_uri", redirectUri)
        .build().toUri();

    log.debug("uri ::: {}", uri.toString());

    try {
      ResponseEntity<Map> response = restAPIService.get(uri, null, Map.class);

      if (response.getStatusCode().equals(HttpStatus.OK))
        return response.getBody();
      else
        log.error("> response : {}", response.getBody());
    } catch (Exception e) {
      e.printStackTrace();
    }
    throw new RuntimeException();
  }

  @Override
  public Map getMe(String accessToken) {

    URI uri = UriComponentsBuilder.fromHttpUrl(apiKey.getMeUri())
        .queryParam("access_token", accessToken)
        .queryParam("fields", apiKey.getMeFields())
        .build().toUri();

    log.debug("uri ::: {}", uri.toString());

    try {
      ResponseEntity<Map> response = restAPIService.get(uri, null, Map.class);

      if (response.getStatusCode().equals(HttpStatus.OK))
        return response.getBody();
      else
        log.error("> response : {}", response.getBody());
    } catch (Exception e) {
      e.printStackTrace();
    }
    throw new RuntimeException();
  }

  @Override
  @Transactional
  public com.logisall.anyware.core.domain.user.sns.SNSStatus getStatus(Facebook facebook) {

    Optional<com.logisall.anyware.core.domain.user.User> userOpt = userRepository.findBySocialIdFacebookId(facebook.getId());

    if (userOpt.isPresent()) {
      return com.logisall.anyware.core.domain.user.sns.SNSStatus.LINKED; // 이미 연동되어었음
    } else if (com.logisall.anyware.core.utils.StringUtils.isNotEmpty(facebook.getEmail())) {

      // 페이스북에서 제공한 이메일 정보로 계정 조회
      userOpt = userRepository.findByEmail(facebook.getEmail());

      if (userOpt.isPresent()) {
        /* 페이스북에서 제공한 이메일 정보로 조회한 계정이 존재한다면 */

        User user = userOpt.get();

        if (user.isWithd()) {
          /* 탈퇴한 계졍이라면 */
          return com.logisall.anyware.core.domain.user.sns.SNSStatus.LEAVED_ACCOUNT;
        }

        if (user.getSocialId() == null || com.logisall.anyware.core.utils.StringUtils.isEmpty(user.getSocialId().getFacebookId())) {
          /* 계정에 페이스북 ID가 연동되어 있지 않으면 연동 시켜준다. */

          com.logisall.anyware.core.domain.user.SocialId socialId = user.getSocialId() == null ? new SocialId() : user.getSocialId();
          socialId.setFacebookId(facebook.getId());
          socialId.setFacebookName(facebook.getName());

          user.setSocialId(socialId);

          if (StringUtils.isNotEmpty(facebook.getPicture())) {
            user.setImage(facebook.getPicture());
          }

          userRepository.save(user);
          return com.logisall.anyware.core.domain.user.sns.SNSStatus.CONNECT;
        } else {
          /* 계정에 다른 페이스북 ID가 연동되어 있다면 */
          return com.logisall.anyware.core.domain.user.sns.SNSStatus.NOT_MATCH_SNS;
        }
      } else {
        /* 페이스북에서 제공한 정보와 동일한 이메일과 페이스북 ID 가 존재하지 않는다. -> 회원가입 */
        return com.logisall.anyware.core.domain.user.sns.SNSStatus.NOT_EXISTED_ACCOUNT;
      }
    } else {
      /* 페이스북에서 이메일을 제공하지 않았다면 -> 회원가입 */
      return SNSStatus.NOT_PROVIDED_EMAIL;
    }
  }
}
