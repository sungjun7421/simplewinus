package com.logisall.anyware.core.domain.commerce.product;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProductRepository extends
    PagingAndSortingRepository<com.logisall.anyware.core.domain.commerce.product.Product, Long>,
    QuerydslPredicateExecutor<com.logisall.anyware.core.domain.commerce.product.Product>,
    ProductRepositoryCustom {

    List<Product> findAllByActiveIsTrueOrderByOrderDescending();

}
