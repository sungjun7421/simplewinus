package com.logisall.anyware.core.model.resbody.board;

import com.logisall.anyware.core.model.BaseResponseBody;
import com.logisall.anyware.core.model.resbody.board.FaqResBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.util.List;

@Schema(description = "자주하는 질문 카테고리")
@Relation(value = "faqCategory", collectionRelation = "faqCategories")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FaqCategoryResBody extends BaseResponseBody {

  private static final long serialVersionUID = 8780396155649692018L;

  @Schema(description = "ID")
  private Long id;

  @Schema(description = "카테고리 명")
  private String name;

  @Schema(description = "아이콘 이미지")
  private String image;

  @Schema(description = "자주하는 질문")
  private List<FaqResBody> faqs;
}
