package com.logisall.anyware.core.service.authenticode;

import com.logisall.anyware.core.domain.authenticode.Authenticode;
import com.logisall.anyware.core.domain.authenticode.AuthenticodeRepository;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.model.AuthentiCodeResBody;
import com.logisall.anyware.core.service.authenticode.AuthenticodeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;

import static org.apache.commons.text.CharacterPredicates.DIGITS;
import static org.apache.commons.text.CharacterPredicates.LETTERS;

@Slf4j
@Service
public class AuthenticodeServiceImpl implements AuthenticodeService {

    @Autowired
    private AuthenticodeRepository tokenStorageRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public String tokenCode(Authenticode authenticode) {

        String token = this.generateToken();
        authenticode.setId(token);
        Authenticode created = tokenStorageRepository.save(authenticode);
        return created.getId();
    }

    @Override
    @Transactional
    public AuthentiCodeResBody confirmByMobile(String token, String mobile) {

        return tokenStorageRepository.findById(token).map(authenticode -> {

            log.debug("> authenticode ::: {}", authenticode);
            log.debug("> LocalDateTime.now().isAfter(authenticode.getExpireTime()) ::: {}", LocalDateTime.now().isAfter(authenticode.getExpireTime()));

            AuthentiCodeResBody result = new AuthentiCodeResBody();

            if (authenticode.isExpire()) {
                tokenStorageRepository.deleteById(token);
                return new AuthentiCodeResBody(Authenticode.RESULT_EXPIRE);
            }

            Map<String, String> value = authenticode.toMap();

            log.debug("> value.get(\"mobile\") : {}", value.get("mobile"));
            log.debug("> mobile : {}", mobile);
            log.debug("> Objects.equals(value.get(\"mobile\"), mobile) : {}", Objects.equals(value.get("mobile"), mobile));

            if (Objects.equals(value.get("mobile"), mobile)) {
                result.setResultCode(Authenticode.RESULT_SUCCESS);
                result.setMobile(value.get("mobile"));
            } else {
                result.setResultCode(Authenticode.RESULT_FAIL);
            }

            log.debug("> authenticode ::: {}", authenticode);
            log.debug("> result authenticode ::: {}", result.toString());
            log.debug("> token ::: {}", token);

//            tokenStorageRepository.deleteById(token);

            return result;
        }).orElseGet(() -> new AuthentiCodeResBody(Authenticode.RESULT_EXPIRE));
    }

    @Override
    public AuthentiCodeResBody getAuthCode(String token) {

        return tokenStorageRepository.findById(token).map(authenticode -> {

            if (authenticode.isExpire()) {
                tokenStorageRepository.deleteById(token);
                return new AuthentiCodeResBody(Authenticode.RESULT_EXPIRE);
            }

            AuthentiCodeResBody result = new AuthentiCodeResBody();
            Map<String, String> value = authenticode.toMap();

            if (value != null && value.get("email") != null) {

                final String email = value.get("email");
                User user = userRepository.findByEmail(email).orElse(null);

                if (user == null) {

                    result.setResultCode(Authenticode.RESULT_NOT_EXISTS);
                } else {

                    user.getVerification().setEmail(true);
                    result.setResultCode(Authenticode.RESULT_SUCCESS);
                    result.setEmail(email);
                }
            } else {

                result.setResultCode(Authenticode.RESULT_FAIL);
            }
            return result;
        }).orElseGet(() -> new AuthentiCodeResBody(Authenticode.RESULT_EXPIRE));
    }


    @Override
    @Transactional
    public AuthentiCodeResBody confirmByEmail(String token) {
        AuthentiCodeResBody result = this.getAuthCode(token);
        tokenStorageRepository.findById(token).ifPresent(authenticode -> {
            tokenStorageRepository.delete(authenticode);
        });
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public String generateToken() {
        RandomStringGenerator generator = new RandomStringGenerator.Builder().withinRange('0', '6').filteredBy(LETTERS, DIGITS).build();
        String token = generator.generate(6);
        if (tokenStorageRepository.existsById(token)) {
            token = this.generateToken();
        }
        return token;
    }

    @Override
    @Transactional
    public void deleteByToken(String token) {
        this.tokenStorageRepository.deleteById(token);
    }

    @Override
    @Transactional
    public void deletePreviousValue(String mobile) {
        this.tokenStorageRepository.deleteAllByMobile(mobile);
    }

}
