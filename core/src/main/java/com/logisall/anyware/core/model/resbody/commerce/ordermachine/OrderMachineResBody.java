package com.logisall.anyware.core.model.resbody.commerce.ordermachine;

import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Relation(value = "orderMachine", collectionRelation = "orderMachines")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderMachineResBody implements Serializable {

  private static final long serialVersionUID = -8474490626457425884L;

  private Long id;
  private Long machineId;
  private String machineSerial;
  private String machineNameStandard;
  private BigDecimal weight;
  private Long kioskId;
  private String kioskName;
  private String kioskSerial;
  private LocalDateTime startTime;
  private LocalDateTime endTime;
  private BigDecimal basePrice;
  private String washingMode;
  private String dryingTimes;
  private BigDecimal price;
}
