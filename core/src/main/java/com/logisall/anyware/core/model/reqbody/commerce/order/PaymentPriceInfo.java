package com.logisall.anyware.core.model.reqbody.commerce.order;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 결제금액정보
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@Builder
public class PaymentPriceInfo implements Serializable {

    private static final long serialVersionUID = -6047397567149991708L;

    public PaymentPriceInfo() {
        this.totalByPrice = BigDecimal.ZERO;
        this.totalByDiscountCoupon = BigDecimal.ZERO;
        this.point = 0;
        this.totalByHwdc = BigDecimal.ZERO;
        this.totalByPaymentPrice = BigDecimal.ZERO;
    }


    private BigDecimal totalByHwdc; // 수기할인 합계

    private BigDecimal totalByPrice; // 예상 결제금액 합계(상품금액)

    // == 이하 최종결제금액

    private BigDecimal totalByDiscountCoupon; // 쿠폰할인 합계

    private double point; // 포인트사용


    private BigDecimal totalByPaymentPrice; // 최종 지불금액(결제금액)
}
