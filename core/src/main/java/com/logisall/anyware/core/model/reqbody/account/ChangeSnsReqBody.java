package com.logisall.anyware.core.model.reqbody.account;

import com.logisall.anyware.core.model.BaseRequestBody;
import com.logisall.anyware.core.model.sns.SNSType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@ToString
public class ChangeSnsReqBody extends BaseRequestBody {

  private static final long serialVersionUID = 5738785088672460354L;

  @Schema(description = "SNS type", example = "FACEBOOK", minLength = 2, maxLength = 255)
  private SNSType snsType; // sns 유형

  @Schema(description = "code", example = "code", minLength = 2, maxLength = 255)
  private String code;

  @Schema(description = "redirectUri", example = "redirectUri", minLength = 2, maxLength = 255)
  private String redirectUri;

  @Schema(description = "state", example = "active", minLength = 2, maxLength = 255)
  private String state;
}
