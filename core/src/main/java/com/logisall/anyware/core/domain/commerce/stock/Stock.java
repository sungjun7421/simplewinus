package com.logisall.anyware.core.domain.commerce.stock;

import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.commerce.product.Product;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * 재고
 */
@Entity
@Getter
@Setter
@ToString(exclude = {"relativeProduct"})
public class Stock extends AbstractEntity<Long> {

  public static final String[] IGNORE_PROPERTIES = {"id"};
  private static final long serialVersionUID = 7631273608153828572L;

  public enum Status {
    IN("입고"),
    OUT("출고");

    @Getter
    private final String value;

    Status(final String value) {
      this.value = value;
    }
  }


  @PrePersist
  public void prePersist() {
  }

  @PreUpdate
  public void PreUpdate() {
  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @Column
  @Enumerated
  private Status status; // 입고/출고 상태

  @Column(columnDefinition = "DECIMAL(19,0) default 0")
  private BigDecimal qty; // 수량 (입고량/출고량)

  @Column(columnDefinition = "BIT(1) default 0")
  private boolean cancel; // 입고/출고 취소

  @ManyToOne
  @JoinColumn(name = "idProduct", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Product_For_Stock"))
  private Product relativeProduct;

  @Override
  public void delete() {

  }

  @Override
  public void lazy() {

  }
}
