package com.logisall.anyware.core.config.property.commerce.lguplus;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class URL implements Serializable {

    private static final long serialVersionUID = 1586771487960138147L;

    private String service;
    private String test;
    private String aux;
    private String returnedAuth;
    private String returnedPay;
    private String casnote;
}
