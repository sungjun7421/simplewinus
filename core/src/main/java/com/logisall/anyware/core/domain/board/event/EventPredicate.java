package com.logisall.anyware.core.domain.board.event;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Objects;

@NoArgsConstructor(staticName = "getInstance")
public class EventPredicate {

  private static final QEvent Q_ENTITY = QEvent.event;

  private final BooleanBuilder builder = new BooleanBuilder();

  public Predicate values() {
    return builder.getValue() == null ? builder.and(Q_ENTITY.id.isNotNull()) : builder.getValue();
  }
  public com.logisall.anyware.core.domain.board.event.EventPredicate search(String value) {

    if (!StringUtils.isEmpty(value)) {
      value = value.trim();
      builder.and(Q_ENTITY.title.textEnUs.containsIgnoreCase(value)
              .or(Q_ENTITY.title.textJaJp.containsIgnoreCase(value))
              .or(Q_ENTITY.title.textKoKr.containsIgnoreCase(value))
              .or(Q_ENTITY.title.textZhCn.containsIgnoreCase(value))
              .or(Q_ENTITY.title.textZhTw.containsIgnoreCase(value)));
    }
    return this;
  }


  public com.logisall.anyware.core.domain.board.event.EventPredicate startDate(final LocalDateTime startDate) {

    if (!StringUtils.isEmpty(startDate)) {
      builder.and(Q_ENTITY.createdDate.goe(startDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.event.EventPredicate endDate(final LocalDateTime endDate) {

    if (!StringUtils.isEmpty(endDate)) {
      builder.and(Q_ENTITY.createdDate.loe(endDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.event.EventPredicate active(final Boolean isActive) {

    if (isActive != null) {
      builder.and(Q_ENTITY.active.eq(isActive));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.event.EventPredicate end(final Boolean end) {

    if (end != null) {
      final LocalDate now = LocalDate.now();
      builder.and(end ? Q_ENTITY.endDate.lt(now) : Q_ENTITY.endDate.isNull().or(Q_ENTITY.endDate.goe(now)));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.event.EventPredicate top(final Boolean top) {

    if (top != null) {
      builder.and(Q_ENTITY.top.eq(top));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.event.EventPredicate locale(Locale locale, final Locale defaultLocale) {

    Locale aLocale = new Locale.Builder().setLanguage("ko").setRegion("Kr").build();

    if (!Objects.equals(locale, Locale.KOREA)
            && !Objects.equals(locale, Locale.US)
            && !Objects.equals(locale, Locale.CHINA)
            && !Objects.equals(locale, Locale.TAIWAN)
            && !Objects.equals(locale, Locale.JAPAN)
            && !Objects.equals(locale, aLocale)) {
      locale = defaultLocale;
    }

    if (Objects.equals(locale, Locale.KOREA)) {
      builder.and(Q_ENTITY.internationalMode.koKr.eq(true));
    } else if (Objects.equals(locale, Locale.US)) {
      builder.and(Q_ENTITY.internationalMode.enUs.eq(true));
    } else if (Objects.equals(locale, Locale.CHINA)) {
      builder.and(Q_ENTITY.internationalMode.zhCn.eq(true));
    } else if (Objects.equals(locale, Locale.TAIWAN)) {
      builder.and(Q_ENTITY.internationalMode.zhTw.eq(true));
    } else if (Objects.equals(locale, Locale.JAPAN)) {
      builder.and(Q_ENTITY.internationalMode.jaJp.eq(true));
    }

    return this;
  }

}
