package com.logisall.anyware.core.model.resbody.commerce.order;

import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.commerce.PayMethod;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.order.BuyerInfo;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Relation(value = "order", collectionRelation = "orders")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderInfoResBody {
    private static final long serialVersionUID = -1345092282502665892L;

    private Long id;
    private String oid;
    private String title;
    private BuyerInfo buyerInfo;
    private OrderStatus orderStatus;
    private PayMethod payMethod;
    private BigDecimal amount;
    private boolean paymentComplete;
    private String failedMsg;
    private LocalDateTime paymentDate;
    private Buyer relativeBuyer;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private BigDecimal fcCoupon;
    private BigDecimal fcPoint;
    private BigDecimal totalPrice;
}
