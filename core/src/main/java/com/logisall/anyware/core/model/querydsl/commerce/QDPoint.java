package com.logisall.anyware.core.model.querydsl.commerce;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class QDPoint implements Serializable {

  private static final long serialVersionUID = -1368507903217033112L;

  @QueryProjection
  public QDPoint(Long id, int point, int used, int rest, Long idBuyer, String buyerEmail) {
    this.id = id;
    this.point = point;
    this.used = used;
    this.rest = rest;
    this.idBuyer = idBuyer;
    this.buyerEmail = buyerEmail;
  }

  private Long id;
  private int point;
  private int used;
  private int rest; // 사용하고 남은 포인트

  private Long idBuyer;
  private String buyerEmail;
}
