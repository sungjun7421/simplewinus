package com.logisall.anyware.core.model.property.niceid;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Setter
@Getter
@ToString
@Component
@ConfigurationProperties(prefix = "niceid.key")
public class NiceIdKey implements Serializable {

  private static final long serialVersionUID = -6431790588314059662L;

  private String siteCode;
  private String sitePassword;
}
