package com.logisall.anyware.core.domain.sms;


import com.logisall.anyware.core.domain.sms.SMSHistory;
import com.logisall.anyware.core.domain.sms.SMSHistoryRepositoryCustom;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SMSHistoryRepository extends
    PagingAndSortingRepository<SMSHistory, Long>,
    QuerydslPredicateExecutor<SMSHistory>,
    SMSHistoryRepositoryCustom {

//    List<SMSHistory> findAllByTypeSMSAndDestPhoneOrderBySendTimeDesc(SMSHistory.TypeSMS typeSMS, String destPhone);
//    List<SMSHistory> findAllByTypeSMSAndDestPhoneAndVerifySMSOrderBySendTimeDesc(SMSHistory.TypeSMS typeSMS, String destPhone, boolean verifySMS);
}
