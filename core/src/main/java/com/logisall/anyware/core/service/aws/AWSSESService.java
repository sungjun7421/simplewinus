package com.logisall.anyware.core.service.aws;


import com.logisall.anyware.core.model.aws.SESSender;

import java.util.concurrent.Future;

public interface AWSSESService {

    Future<String> send(SESSender sesSender);
}
