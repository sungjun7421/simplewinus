package com.logisall.anyware.core.domain.commerce.paymentproduct;

import com.logisall.anyware.core.domain.commerce.paymentproduct.PaymentProductRepositoryCustom;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class PaymentProductRepositoryImpl implements PaymentProductRepositoryCustom {

  @Autowired
  private JPAQueryFactory jpaQueryFactory;
}
