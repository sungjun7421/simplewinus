package com.logisall.anyware.core.domain.board.post.category;

import com.logisall.anyware.core.domain.board.post.Post;

public interface PCategoryRepositoryCustom {

    boolean existsByNameAndTypeAndKoKr(String name, com.logisall.anyware.core.domain.board.post.Post.Type type);

    boolean existsByNameAndTypeAndEnUs(String name, com.logisall.anyware.core.domain.board.post.Post.Type type);

    boolean existsByNameAndTypeAndZhCn(String name, com.logisall.anyware.core.domain.board.post.Post.Type type);

    boolean existsByNameAndTypeAndZhTw(String name, com.logisall.anyware.core.domain.board.post.Post.Type type);

    boolean existsByNameAndTypeAndJaJp(String name, Post.Type type);
}
