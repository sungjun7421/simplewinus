package com.logisall.anyware.core.domain.commerce.buyerlevel.enumerate;

import lombok.Getter;

/**
 * 회원등급 부여 방법
 */
@Getter
public enum BuyerLevelGrantMethod {

  DAY("Daily", "매일 오전 0시에 자동으로 확정되어, 확정된 내용은 그 즉시 반영됩니다."),
  WEEK("Weekly", "매주 월요일 오전 0시에 자동으로 확정되어, 확정된 내용은 그 즉시 반영됩니다."),
  MONTH("Monthly", "매월 1일 오전 0시에 자동으로 확정되어, 확정된 내용은 그 즉시 반영됩니다.");

  private final String value;
  private final String description;

  BuyerLevelGrantMethod(final String value, final String description) {
    this.value = value;
    this.description = description;
  }
}
