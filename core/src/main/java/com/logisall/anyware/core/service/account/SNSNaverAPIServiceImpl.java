package com.logisall.anyware.core.service.account;

import com.logisall.anyware.core.config.property.sns.NaverApiKey;
import com.logisall.anyware.core.domain.user.SocialId;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.domain.user.sns.SNSStatus;
import com.logisall.anyware.core.model.sns.Naver;
import com.logisall.anyware.core.service.restapi.RestAPIService;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Headers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
public class SNSNaverAPIServiceImpl implements SNSNaverAPIService {

  @Autowired
  private NaverApiKey apiKey;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private RestAPIService restAPIService;

  @Override
  public Map getAccessToken(String code, String redirectUri, String state) {

    URI uri = UriComponentsBuilder.fromHttpUrl(apiKey.getAccessTokenUri())
        .queryParam("client_id", apiKey.getClientId())
        .queryParam("client_secret", apiKey.getClientSecret())
        .queryParam("grant_type", "authorization_code")
        .queryParam("code", code)
        .queryParam("redirect_uri", redirectUri)
        .queryParam("state", state)
        .build().toUri();

    log.debug("uri ::: {}", uri.toString());

    try {
      ResponseEntity<Map> response = restAPIService.get(uri, null, Map.class);

      if (response.getStatusCode().equals(HttpStatus.OK))
        return response.getBody();
      else
        log.error("> response : {}", response.getBody());
    } catch (Exception e) {
      e.printStackTrace();
    }
    throw new RuntimeException();
  }

  @Override
  public Map getMe(String accessToken) {

    URI uri = UriComponentsBuilder.fromHttpUrl(apiKey.getMeUri()).build().toUri();

    log.debug("uri ::: {}", uri.toString());

    try {
      Headers.Builder hdBuilder = new Headers.Builder();
      hdBuilder.add("Authorization", "Bearer " + accessToken);

      ResponseEntity<Map> response = restAPIService.get(uri, hdBuilder.build(), Map.class);

      if (response.getStatusCode().equals(HttpStatus.OK))
        return response.getBody();
      else
        log.error("> response : {}", response.getBody());
    } catch (Exception e) {
      e.printStackTrace();
    }
    throw new RuntimeException();
  }

  @Override
  @Transactional
  public com.logisall.anyware.core.domain.user.sns.SNSStatus getStatus(Naver naver) {
    Optional<com.logisall.anyware.core.domain.user.User> userOpt = userRepository.findBySocialIdNaverId(naver.getId());

    if (userOpt.isPresent()) {
      return com.logisall.anyware.core.domain.user.sns.SNSStatus.LINKED; // 이미 연동되어었음
    } else if (com.logisall.anyware.core.utils.StringUtils.isNotEmpty(naver.getEmail())) {

      // 페이스북에서 제공한 이메일 정보로 계정 조회
      userOpt = userRepository.findByEmail(naver.getEmail());

      if (userOpt.isPresent()) {
        /* 페이스북에서 제공한 이메일 정보로 조회한 계정이 존재한다면 */

        User user = userOpt.get();

        if (user.isWithd()) {
          /* 탈퇴한 계졍이라면 */
          return com.logisall.anyware.core.domain.user.sns.SNSStatus.LEAVED_ACCOUNT;
        }

        if (user.getSocialId() == null || com.logisall.anyware.core.utils.StringUtils.isEmpty(user.getSocialId().getNaverId())) {
          /* 계정에 네이버\ ID가 연동되어 있지 않으면 연동 시켜준다. */

          com.logisall.anyware.core.domain.user.SocialId socialId = user.getSocialId() == null ? new SocialId() : user.getSocialId();
          socialId.setNaverId(naver.getId());
          socialId.setNaverName(naver.getName());

          user.setSocialId(socialId);

          if (StringUtils.isNotEmpty(naver.getProfile_image())) {
            user.setImage(naver.getProfile_image());
          }

          userRepository.save(user);
          return com.logisall.anyware.core.domain.user.sns.SNSStatus.CONNECT;
        } else {
          /* 계정에 다른 페이스북 ID가 연동되어 있다면 */
          return com.logisall.anyware.core.domain.user.sns.SNSStatus.NOT_MATCH_SNS;
        }
      } else {
        /* 페이스북에서 제공한 정보와 동일한 이메일과 페이스북 ID 가 존재하지 않는다. -> 회원가입 */
        return com.logisall.anyware.core.domain.user.sns.SNSStatus.NOT_EXISTED_ACCOUNT;
      }
    } else {
      /* 페이스북에서 이메일을 제공하지 않았다면 -> 회원가입 */
      return SNSStatus.NOT_PROVIDED_EMAIL;
    }
  }
}
