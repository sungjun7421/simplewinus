package com.logisall.anyware.core.model.admin;

import com.logisall.anyware.core.domain.International.InterText;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.server.core.Relation;

import java.io.Serializable;

@Relation(value = "postInfo")
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PostInfoResBody implements Serializable {

  private static final long serialVersionUID = 2527352512829967846L;

  private Long id;
  private InterText content;
  private InterText title;
}
