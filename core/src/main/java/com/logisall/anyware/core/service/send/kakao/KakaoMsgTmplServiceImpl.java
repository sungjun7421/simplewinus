package com.logisall.anyware.core.service.send.kakao;

import com.logisall.anyware.core.model.property.kko.KKOTemplate;
import com.logisall.anyware.core.service.send.kakao.KakaoMsgTmplService;
import com.logisall.anyware.core.service.send.kakao.SendKakaoMsgService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KakaoMsgTmplServiceImpl implements KakaoMsgTmplService {

    @Autowired
    private SendKakaoMsgService sendKakaoMsgService;

    @Autowired
    private KKOTemplate kkoTemplate;


    @Override
    public void sendMsg1(String mobile, String code) {
        sendKakaoMsgService.send(kkoTemplate.toTemplate001(mobile, code));
    }

    @Override
    public void sendMsg2(String mobile, String oid, String productName) {
        sendKakaoMsgService.send(kkoTemplate.toTemplate002(mobile, oid, productName));
    }

    @Override
    public void sendMsg3(String mobile, String oid, String productName) {
        sendKakaoMsgService.send(kkoTemplate.toTemplate003(mobile, oid, productName));
    }

    @Override
    public void sendMsg4(String mobile, String oid, String productName) {
        sendKakaoMsgService.send(kkoTemplate.toTemplate004(mobile, oid, productName));
    }

    @Override
    public void sendMsg5(String mobile, String oid, String productName, String vbankNum, String vbankName, String vbankHolder, String amount, String expirationDate) {
        sendKakaoMsgService.send(kkoTemplate.toTemplate005(mobile, oid, productName, vbankNum, vbankName, vbankHolder, amount, expirationDate));
    }

    @Override
    public void sendMsg6(String mobile, String fullName, String type, String function, String option, String starttime, String endtime, String address) {
        sendKakaoMsgService.send(kkoTemplate.toTemplate006(mobile, fullName, type, function, option, starttime, endtime, address));
    }

    @Override
    public void sendMsg7(String mobile, String fullName, String type, String function, String option, String starttime, String endtime, String address, String fee) {
        sendKakaoMsgService.send(kkoTemplate.toTemplate007(mobile, fullName, type, function, option, starttime, endtime, address, fee));
    }
}
