package com.logisall.anyware.core.domain.board.comment;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@NoArgsConstructor(staticName = "getInstance")
public class CommentPredicate {

  private static final QComment Q_ENTITY = QComment.comment;

  private BooleanBuilder builder = new BooleanBuilder();

  public Predicate values() {
    return builder.getValue() == null ? builder.and(Q_ENTITY.id.isNotNull()) : builder.getValue();
  }

  public CommentPredicate startDate(final LocalDateTime startDate) {

    if (startDate != null) {
      builder.and(Q_ENTITY.createdDate.goe(startDate));
    }
    return this;
  }

  public CommentPredicate endDate(final LocalDateTime endDate) {

    if (endDate != null) {
      builder.and(Q_ENTITY.createdDate.loe(endDate));
    }
    return this;
  }

  public CommentPredicate search(String value) {

    if (!StringUtils.isEmpty(value)) {
      value = value.trim();

      builder.and(Q_ENTITY.content.containsIgnoreCase(value));
    }
    return this;
  }

  public CommentPredicate user(final Long idUser) {
    if (idUser != null && idUser != 0L) {
      builder.and(Q_ENTITY.relativeUser.id.eq(idUser));
    }
    return this;
  }

  public CommentPredicate parent(final Long idParent) {
    if (idParent != null && idParent != 0L) {
      builder.and(Q_ENTITY.relativeParent.id.eq(idParent));
    }
    return this;
  }

  public CommentPredicate active(final Boolean active) {
    if (active != null) {
      builder.and(Q_ENTITY.active.eq(active));
    }
    return this;
  }

  public CommentPredicate relative(final com.logisall.anyware.core.domain.board.comment.Comment.Type type, final Long idContent) {
    if (idContent != null && idContent != 0L) {
      if(type.equals(com.logisall.anyware.core.domain.board.comment.Comment.Type.POST)) {
        builder.and(Q_ENTITY.relativePost.id.eq(idContent));
      } else if(type.equals(com.logisall.anyware.core.domain.board.comment.Comment.Type.EVENT)) {
        builder.and(Q_ENTITY.relativeEvent.id.eq(idContent));
      }
    }
    return this;
  }

  public CommentPredicate relativePost(final Long idContent) {
    if (idContent != null && idContent != 0L) {
      builder.and(Q_ENTITY.relativePost.id.eq(idContent));
    }
    return this;
  }

  public CommentPredicate relativeEvent(final Long idContent) {
    if (idContent != null && idContent != 0L) {
      builder.and(Q_ENTITY.relativeEvent.id.eq(idContent));
    }
    return this;
  }

  public CommentPredicate type(final Comment.Type type) {
    if (type != null) {
      builder.and(Q_ENTITY.type.eq(type));
    }
    return this;
  }

//  public CommentPredicate hasEvent() {
//    builder.and(Q_ENTITY.relativeEvent.isNotNull());
//    return this;
//  }
//
//  public CommentPredicate hasPost() {
//    builder.and(Q_ENTITY.relativePost.isNotNull());
//    return this;
//  }
}
