package com.logisall.anyware.core.model.sns.apple;

import com.logisall.anyware.core.model.sns.apple.Key;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@ToString
public class Keys implements Serializable {

    private static final long serialVersionUID = -269701500900262110L;
    private List<Key> keys;
}
