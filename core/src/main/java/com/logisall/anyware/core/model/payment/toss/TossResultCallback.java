package com.logisall.anyware.core.model.payment.toss;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class TossResultCallback implements Serializable {

  private static final long serialVersionUID = -6703787438227050270L;

  private TossStatus status;
  private String payToken;
  private String orderNo;
  private TossPayMethod payMethod;
  private int discountedAmount;
  private String paidTs;
  private String metadata;
}
