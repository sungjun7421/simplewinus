package com.logisall.anyware.core.domain.commerce.point;

import com.logisall.anyware.core.domain.commerce.point.PointSetting;
import com.logisall.anyware.core.domain.commerce.point.PointSettingRepositoryCustom;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class PointSettingRepositoryImpl implements PointSettingRepositoryCustom {
    @Autowired
    private JPAQueryFactory jpaQueryFactory;

    @Override
    public PointSetting findFist() {
        QPointSetting qPointSetting = QPointSetting.pointSetting;
        return jpaQueryFactory.selectFrom(qPointSetting)
                .orderBy(qPointSetting.createdDate.desc())
                .fetchFirst();
    }
}
