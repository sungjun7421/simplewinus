package com.logisall.anyware.core.domain.board.contact;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class ContactRepositoryImpl implements ContactRepositoryCustom {

  @Autowired
  private JPAQueryFactory jpaQueryFactory;
}
