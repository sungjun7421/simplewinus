package com.logisall.anyware.core.model.resbody.board;

import com.logisall.anyware.core.config.serializer.JsonLocalDateDeserializer;
import com.logisall.anyware.core.config.serializer.JsonLocalDateSerializer;
import com.logisall.anyware.core.model.BaseResponseThumbBody;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.time.LocalDate;

@Schema(description = "이벤트")
@Relation(value = "event", collectionRelation = "events")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EventThumbBody extends BaseResponseThumbBody {

  private static final long serialVersionUID = 1297080571183814169L;

  @Schema(description = "ID")
  private Long id;

  @Schema(description = "제목")
  private String title;

  @Schema(description = "요약글")
  private String summary;

  @Schema(description = "썸네일 이미지")
  private String thumbnail;

  @Schema(description = "이벤트 시작일")
  @JsonSerialize(using = JsonLocalDateSerializer.class)
  @JsonDeserialize(using = JsonLocalDateDeserializer.class)
  private LocalDate startDate; // 전시 기간

  @Schema(description = "이벤트 마감일")
  @JsonSerialize(using = JsonLocalDateSerializer.class)
  @JsonDeserialize(using = JsonLocalDateDeserializer.class)
  private LocalDate endDate; // 전시 기간

  @Schema(description = "이벤트 종료", example = "false")
  private boolean end;

  @Schema(description = "조회수")
  private long pageView; // 조회수
}
