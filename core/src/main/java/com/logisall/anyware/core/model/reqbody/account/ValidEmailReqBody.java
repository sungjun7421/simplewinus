package com.logisall.anyware.core.model.reqbody.account;

import com.logisall.anyware.core.model.BaseRequestBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;

@Setter
@Getter
@ToString
public class ValidEmailReqBody extends BaseRequestBody {
  private static final long serialVersionUID = 3624673557127238712L;

  @Email
  @Schema(description = "이메일 주소", example = "test@logisall.com", minLength = 3, maxLength = 255)
  private String email;
}
