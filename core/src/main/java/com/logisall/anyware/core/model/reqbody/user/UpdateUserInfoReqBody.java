package com.logisall.anyware.core.model.reqbody.user;

import com.logisall.anyware.core.model.BaseRequestBody;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Pattern;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateUserInfoReqBody extends BaseRequestBody {

  private static final long serialVersionUID = -1567352085715794401L;

  @Pattern(regexp = com.logisall.anyware.core.utils.ValidUtils.PATTERN_FULLNAME)
  @Schema(description = "fullName", example = "홍길동", minLength = 2, maxLength = 10)
  private String fullName;
  @Pattern(regexp = com.logisall.anyware.core.utils.ValidUtils.PATTERN_PASSWORD_NEW)
  @Schema(description = "password", example = "abcd1234", minLength = 6, maxLength = 30)
  private String password;
  @Pattern(regexp = ValidUtils.PATTERN_MOBILE)
  @Schema(description = "mobile", example = "01011112222", minLength = 10, maxLength = 11)
  private String mobile;

  @Schema(description = "Email receive", example = "test@logisall.com", maxLength = 255)
  private boolean emailRcv;
  @Schema(description = "sms receive", example = "sms", maxLength = 255)
  private boolean smsRcv;
}
