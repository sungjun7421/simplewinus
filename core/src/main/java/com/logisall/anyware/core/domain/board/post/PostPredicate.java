package com.logisall.anyware.core.domain.board.post;

import com.logisall.anyware.core.domain.board.post.Post;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Objects;

@NoArgsConstructor(staticName = "getInstance")
public class PostPredicate {

  private static final QPost Q_ENTITY = QPost.post;

  private final BooleanBuilder builder = new BooleanBuilder();

  public Predicate values() {
    return builder.getValue() == null ? builder.and(Q_ENTITY.id.isNotNull()) : builder.getValue();
  }

  public com.logisall.anyware.core.domain.board.post.PostPredicate startDate(final LocalDateTime startDate) {

    if (startDate != null) {
      builder.and(Q_ENTITY.createdDate.goe(startDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.post.PostPredicate endDate(final LocalDateTime endDate) {

    if (endDate != null) {
      builder.and(Q_ENTITY.createdDate.loe(endDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.post.PostPredicate search(String value) {

    if (!StringUtils.isEmpty(value)) {
      value = value.trim();

      builder.and(Q_ENTITY.title.textEnUs.containsIgnoreCase(value)
          .or(Q_ENTITY.title.textJaJp.containsIgnoreCase(value))
          .or(Q_ENTITY.title.textKoKr.containsIgnoreCase(value))
          .or(Q_ENTITY.title.textZhCn.containsIgnoreCase(value))
          .or(Q_ENTITY.title.textZhTw.containsIgnoreCase(value))
          .or(Q_ENTITY.content.textEnUs.containsIgnoreCase(value))
          .or(Q_ENTITY.content.textJaJp.containsIgnoreCase(value))
          .or(Q_ENTITY.content.textKoKr.containsIgnoreCase(value))
          .or(Q_ENTITY.content.textZhCn.containsIgnoreCase(value))
          .or(Q_ENTITY.content.textZhTw.containsIgnoreCase(value)));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.post.PostPredicate type(final Post.Type type) {

    if (type != null) {
      builder.and(Q_ENTITY.type.eq(type));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.post.PostPredicate category(final Long idCategory) {

    if (idCategory != null && idCategory != 0L) {
      builder.and(Q_ENTITY.categories.any().id.eq(idCategory));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.post.PostPredicate categories(final Long[] idCategories) {

    if (idCategories != null && idCategories.length > 0) {
      builder.and(Q_ENTITY.categories.any().id.in(idCategories));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.post.PostPredicate top(final Boolean top) {

    if (top != null) {
      builder.and(Q_ENTITY.top.eq(top));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.post.PostPredicate active(final Boolean isActive) {

    if (isActive != null) {
      builder.and(Q_ENTITY.active.eq(isActive));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.post.PostPredicate locale(Locale locale, final Locale defaultLocale) {

    if (!Objects.equals(locale, Locale.KOREA)
            && !Objects.equals(locale, Locale.US)
            && !Objects.equals(locale, Locale.CHINA)
            && !Objects.equals(locale, Locale.TAIWAN)
            && !Objects.equals(locale, Locale.JAPAN)) {
      locale = defaultLocale;
    }

    if (Objects.equals(locale, Locale.KOREA)) {
      builder.and(Q_ENTITY.internationalMode.koKr.eq(true));
    } else if (Objects.equals(locale, Locale.US)) {
      builder.and(Q_ENTITY.internationalMode.enUs.eq(true));
    } else if (Objects.equals(locale, Locale.CHINA)) {
      builder.and(Q_ENTITY.internationalMode.zhCn.eq(true));
    } else if (Objects.equals(locale, Locale.TAIWAN)) {
      builder.and(Q_ENTITY.internationalMode.zhTw.eq(true));
    } else if (Objects.equals(locale, Locale.JAPAN)) {
      builder.and(Q_ENTITY.internationalMode.jaJp.eq(true));
    }
    return this;
  }
}
