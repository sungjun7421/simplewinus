package com.logisall.anyware.core.model.map.google.textsearch;

import com.logisall.anyware.core.model.map.google.placedetail.result.Result;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.List;

@Slf4j
@Setter
@Getter
@ToString
public class TextSearch implements Serializable {

    private static final long serialVersionUID = -3081450093951857077L;

    public enum Status {
        OK("오류가 발생되지 않았음을 나타냅니다. 장소가 성공적으로 감지되었고 최소 한 개 이상의 결과가 반환됩니다."),
        ZERO_RESULTS("검색에 성공했지만 반환된 결과가 없음을 나타냅니다. 이는 멀리 떨어진 위치의 latlng가 검색에 전달된 경우 발생할 수 있습니다."),
        OVER_QUERY_LIMIT("할당량이 초과되었음을 나타냅니다."),
        REQUEST_DENIED("요청이 거부되었음을 나타냅니다. 일반적인 원인은 key 매개변수가 잘못되었기 때문입니다."),
        INVALID_REQUEST("일반적으로 필수 검색 매개변수(location 또는 radius)가 누락되어 있음을 나타냅니다.");

        @Getter
        final String value;

        Status(final String value) {
            this.value = value;
        }
    }

    private Status status;
    private List<Result> results;
}
