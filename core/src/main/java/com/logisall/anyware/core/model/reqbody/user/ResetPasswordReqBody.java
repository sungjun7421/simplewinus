package com.logisall.anyware.core.model.reqbody.user;

import com.logisall.anyware.core.model.BaseRequestBody;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Pattern;

@Setter
@Getter
@ToString
public class ResetPasswordReqBody extends BaseRequestBody {

  private static final long serialVersionUID = -5745026670782801228L;

  @Schema(description = "Auth code")
  private String code;

  @Pattern(regexp = com.logisall.anyware.core.utils.ValidUtils.PATTERN_PASSWORD_NEW)
  @Schema(description = "비밀번호", example = "abcd1234", minLength = 6, maxLength = 30)
  private String password;

  @Pattern(regexp = ValidUtils.PATTERN_PASSWORD_NEW)
  @Schema(description = "confirm password", example = "abcd1234", minLength = 6, maxLength = 30)
  private String passwordConfirm;
}
