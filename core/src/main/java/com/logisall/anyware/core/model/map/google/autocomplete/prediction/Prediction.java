package com.logisall.anyware.core.model.map.google.autocomplete.prediction;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Slf4j
@Setter
@Getter
@ToString
public class Prediction implements Serializable {

    private static final long serialVersionUID = -6843713542184464742L;

    private String id;
    private String description;
    private String place_id;
}

