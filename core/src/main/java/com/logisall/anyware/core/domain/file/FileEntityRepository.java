package com.logisall.anyware.core.domain.file;

import com.logisall.anyware.core.domain.file.FileEntity;
import com.logisall.anyware.core.domain.file.FileEntityRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface FileEntityRepository extends
    JpaRepository<FileEntity, Long>,
    QuerydslPredicateExecutor<FileEntity>,
    FileEntityRepositoryCustom {

  FileEntity findOneByFilename(String filename);

  FileEntity findOneByUrl(String url);
}
