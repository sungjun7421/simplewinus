package com.logisall.anyware.core.model.reqbody.commerce.order;

import com.logisall.anyware.core.domain.commerce.PayMethod;
import com.logisall.anyware.core.model.BaseRequestBody;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderReqBody extends BaseRequestBody {

    private static final long serialVersionUID = 984794516826293736L;

//    private BuyerInfo buyerInfo;

//    private OrderStatus orderStatus;

    private PayMethod payMethod;
//    private BigDecimal amount;
//    private List<ProductReqBody> products;


}
