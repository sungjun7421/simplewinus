package com.logisall.anyware.core.domain.commerce.coupon;

import com.logisall.anyware.core.domain.commerce.coupon.Coupon;

import java.util.List;

public interface CouponRepositoryCustom {
    List<Coupon> findAllByUser(Long idUser, Boolean isUsed);

}
