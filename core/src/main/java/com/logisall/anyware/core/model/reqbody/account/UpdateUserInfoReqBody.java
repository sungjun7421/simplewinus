package com.logisall.anyware.core.model.reqbody.account;

import com.logisall.anyware.core.model.BaseRequestBody;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Pattern;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateUserInfoReqBody extends BaseRequestBody {

  private static final long serialVersionUID = -1567352085715794401L;

  @Pattern(regexp = ValidUtils.PATTERN_FULLNAME)
  @Schema(description = "fullName", example = "홍길동", minLength = 2, maxLength = 10)
  private String fullName;

//  @Pattern(regexp = ValidUtils.PATTERN_MOBILE)
//  @Schema(description = "mobile", example = "01011112222", minLength = 10, maxLength = 11)
//  private String mobile;

  @Schema(description = "프로필 이미지")
  private String image;

  @Schema(description = "Email 수신동의 여부")
  private boolean emailRcv;

  @Schema(description = "SMS 수신동의 여부")
  private boolean smsRcv;

  @Schema(description = "kakao 수신동의 여부")
  private boolean kakaoRcv;

  @Schema(description = "Push 수신동의 여부")
  private boolean pushRcv;
}
