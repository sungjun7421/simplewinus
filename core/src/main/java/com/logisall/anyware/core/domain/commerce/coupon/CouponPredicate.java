package com.logisall.anyware.core.domain.commerce.coupon;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;

@NoArgsConstructor(staticName = "getInstance")
public class CouponPredicate {

    private static final QCoupon Q_COUPON = QCoupon.coupon;

    private BooleanBuilder builder = new BooleanBuilder();

    public Predicate values() {
        return builder.getValue() == null ? builder.and(Q_COUPON.id.isNotNull()) : builder.getValue();
    }


    public CouponPredicate search(String value) {

        if (!StringUtils.isEmpty(value)) {
            value = value.trim();
            builder.and(Q_COUPON.name.textEnUs.containsIgnoreCase(value)
                    .or(Q_COUPON.name.textJaJp.containsIgnoreCase(value))
                    .or(Q_COUPON.name.textKoKr.containsIgnoreCase(value))
                    .or(Q_COUPON.name.textZhCn.containsIgnoreCase(value))
                    .or(Q_COUPON.name.textZhTw.containsIgnoreCase(value)));
        }

        return this;

    }

    public CouponPredicate time(final LocalDate time) {

        if (!StringUtils.isEmpty(time)) {
            builder.and(Q_COUPON.startDate.loe(time))
                    .and(Q_COUPON.endDate.goe(time));
        }
        return this;
    }

    public CouponPredicate startDate(final LocalDateTime startDate) {

        if (!StringUtils.isEmpty(startDate)) {
            builder.and(Q_COUPON.createdDate.goe(startDate));
        }
        return this;
    }

    public CouponPredicate endDate(final LocalDateTime endDate) {

        if (!StringUtils.isEmpty(endDate)) {
            builder.and(Q_COUPON.createdDate.loe(endDate));
        }
        return this;
    }

    public CouponPredicate type(final CouponType type) {
        if (type != null) {
            builder.and(Q_COUPON.type.eq(type));
        }
        return this;
    }



    public CouponPredicate active(final Boolean active) {
        if (active == null) {
            builder.and(Q_COUPON.active.eq(active));
        }
        return this;
    }


    public CouponPredicate expireUsed() {
        builder.and(Q_COUPON.endDate.goe(LocalDate.now()));
        return this;
    }



    //  public CouponPredicate status(final Point.Status status) {
//    if (status != null) {
//      builder.and(Q_COUPON.status.eq(status));
//    }
//    return this;
//  }
//
//    public CouponPredicate buyer(final Long idBuyer) {
//        if (idBuyer != null) {
//            builder.and(Q_COUPON.relativeBuyer.id.eq(idBuyer));
//        }
//        return this;
//    }
//
//    public CouponPredicate buyer(final String email) {
//        if (!StringUtils.isEmpty(email)) {
//            builder.and(Q_COUPON.relativeBuyer.relativeUser.email.eq(email));
//        }
//        return this;
//    }
}
