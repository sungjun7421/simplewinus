package com.logisall.anyware.core.model.reqbody.account;

import com.logisall.anyware.core.model.BaseRequestBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ChangeFullNameReqBody extends BaseRequestBody {

  private static final long serialVersionUID = -4086252111150897191L;

  @Schema(description = "fullName", example = "홍길동", minLength = 2, maxLength = 30)
  private String fullName;
}
