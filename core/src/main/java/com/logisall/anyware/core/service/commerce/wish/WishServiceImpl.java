package com.logisall.anyware.core.service.commerce.wish;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.buyer.BuyerRepository;
import com.logisall.anyware.core.domain.commerce.product.Product;
import com.logisall.anyware.core.domain.commerce.product.ProductRepository;
import com.logisall.anyware.core.domain.commerce.wish.Wish;
import com.logisall.anyware.core.domain.commerce.wish.WishPredicate;
import com.logisall.anyware.core.domain.commerce.wish.WishRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.resbody.ResultResBody;
import com.logisall.anyware.core.model.resbody.commerce.product.ProductResBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@Service
public class WishServiceImpl implements WishService {

    @Autowired
    private WishRepository wishRepository;

    @Autowired
    private BuyerRepository buyerRepository;

    @Autowired
    private ProductRepository showRepository;

    @Override
    @Transactional
    public com.logisall.anyware.core.domain.commerce.wish.Wish create(com.logisall.anyware.core.domain.commerce.wish.Wish wish) {
        return wishRepository.save(wish);
    }

    @Override
    @Transactional
    public void delete(com.logisall.anyware.core.domain.commerce.wish.Wish.Id id) {
        wishRepository.deleteById(id);
    }

    @Override
    @Transactional
    public CollectionModel<EntityModel<ProductResBody>> resources(Locale locale, Long idUser) {

        com.logisall.anyware.core.domain.commerce.buyer.Buyer buyer = buyerRepository.findByRelativeUser_Id(idUser);

        if (buyer == null) {
            throw new com.logisall.anyware.core.config.exception.BadRequestException();
        }

        return CollectionModel.wrap(buyer.getWishList().stream()
                .filter(wish -> wish.getRelativeProduct().isActive())
                .map(wish -> {
                    com.logisall.anyware.core.domain.commerce.product.Product show = wish.getRelativeProduct();
                    return show.toBody(locale);
                })
                .collect(Collectors.toList()));
    }

    @Override
    @Transactional
    public com.logisall.anyware.core.model.resbody.ResultResBody toggle(Long idProduct, Long idUser) {

        Product show = showRepository.findById(idProduct).orElseThrow(com.logisall.anyware.core.config.exception.BadRequestException::new);
        com.logisall.anyware.core.domain.commerce.buyer.Buyer buyer = buyerRepository.findByRelativeUser_Id(idUser);

        if (buyer == null) {
            throw new com.logisall.anyware.core.config.exception.BadRequestException();
        }

        com.logisall.anyware.core.domain.commerce.wish.Wish.Id id = new com.logisall.anyware.core.domain.commerce.wish.Wish.Id(buyer.getId(), show.getId());

        if (wishRepository.existsById(id)) {
            this.delete(id);
            return com.logisall.anyware.core.model.resbody.ResultResBody.of(true, com.logisall.anyware.core.domain.commerce.wish.Wish.RESULT_DELETED);
        } else {
            com.logisall.anyware.core.domain.commerce.wish.Wish created = this.create(new com.logisall.anyware.core.domain.commerce.wish.Wish(buyer, show));
            if (created == null) {
                throw new RuntimeException();
            }
            return ResultResBody.of(true, com.logisall.anyware.core.domain.commerce.wish.Wish.RESULT_ADDED);
        }
    }

    @Override
    @Transactional
    public void deleteList(List<Long> showIds, Long idUser) {

        Buyer buyer = buyerRepository.findByRelativeUser_Id(idUser);

        if (buyer == null) {
            throw new BadRequestException();
        }

        showIds.forEach(idProduct -> {
            this.delete(new com.logisall.anyware.core.domain.commerce.wish.Wish.Id(buyer.getId(), idProduct));
        });
    }

    @Override
    @Transactional
    public Page<com.logisall.anyware.core.domain.commerce.wish.Wish> page(Filter filter, Long productId, Long userIdCanNullAble, boolean byProduct) {
        if(!byProduct && userIdCanNullAble==null){
            return wishRepository.findAll(
                    com.logisall.anyware.core.domain.commerce.wish.WishPredicate.getInstance()
                            .userId(-9999L).values()
                    ,filter.getPageable());
        }
        Page<Wish> page=wishRepository.findAll(
                WishPredicate.getInstance()
                        .query(filter.getQuery())
                        .productId(productId)
                        .userId(userIdCanNullAble)
                        .startDate(filter.getStartDate())
                        .endDate(filter.getEndDate())
                        .values()
                ,filter.getPageable())
//                .map(wish->{
//                    wish.lazy();
//                    return wish;
//                })
                ;
        return page;
    }

}
