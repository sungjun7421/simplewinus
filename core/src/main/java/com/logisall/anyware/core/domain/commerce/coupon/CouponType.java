package com.logisall.anyware.core.domain.commerce.coupon;

import lombok.Getter;

@Getter
public enum CouponType {
    DISCOUNT_FIXED("Fixed discounts"),
    DISCOUNT_PERCENT("Percent discount"),
    GIFT_POINT("Gift point");

    private final String value;

    CouponType(final String value) {
        this.value = value;
    }
}
