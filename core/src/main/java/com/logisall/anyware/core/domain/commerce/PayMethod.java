package com.logisall.anyware.core.domain.commerce;

import lombok.Getter;

import java.math.BigDecimal;

public enum PayMethod {

  // == IAMPORT
  CARD("신용카드 (올엣페이)", "card", "CARD", "2.7"),// (신용카드)
  TRANS("실시간계좌이체 (올엣페이)", "trans", "NONE", "2.7"), // (실시간계좌이체)
  VBANK("가상계좌 (올엣페이)", "vbank", "VBANK", "2.7"), // (가상계좌)
  NOR("일반결제 (올엣페이)", "nor", "NOR", "2.7"), // (일반결제|올렛페이전용)

  PAY_UP("페이업", "Pay up", "", "4"),
  KAKAO_PAY("카카오 페이", "Kakao Pay", "", "4"),
  TOSS("토스 페이", "Toss Pay", "", "3.2"),
  PAY_PAL("페이팔", "PayPal", "", "3.2"),
  PAYCO("페이코", "Payco", "", "3.2"),
  TEST_PAY("테스트 결제", "Test Pay", "", "4"),
  ;

  @Getter
  private final String value;

  @Getter
  private final String text;

  @Getter
  private final String allAtPay; // 올엣 페이용, 상태값

  @Getter
  private final BigDecimal fee; // 수수료

  PayMethod(final String value, final String text, final String allAtPay, String fee) {
    this.value = value;
    this.text = text;
    this.allAtPay = allAtPay;
    this.fee = new BigDecimal(fee);
  }
}
