package com.logisall.anyware.core.config.property.commerce.kakaopay;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "pay-module.kakao-pay")
public class KakaoPayConfig implements Serializable {

    private static final long serialVersionUID = -8047838349351176056L;

    private String cid;
    private String readyUri;
    private String approveUri;
    private String cancelUri;
}
