package com.logisall.anyware.core.domain.user;

import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepositoryCustom;
import com.logisall.anyware.core.domain.user.mybatis.UserMyBatis;
import com.logisall.anyware.core.model.resbody.account.MeResBody;
import com.logisall.anyware.core.model.resbody.account.UserInfoResBody;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserRepositoryImpl implements UserRepositoryCustom {

  @Autowired
  private UserMyBatis userMyBatis;

  @Autowired
  private JPAQueryFactory jpaQueryFactory;

  @Override
  public long countAll() {
    QUser qUser = QUser.user;
    return jpaQueryFactory.select(qUser.id)
            .from(qUser)
            .fetchCount();
  }

  @Override
  public long countUserLeave() {
    QUser qUser = QUser.user;
    return jpaQueryFactory.select(qUser.id)
            .from(qUser)
            .where(
                    qUser.leaveMeta.leave.eq(true)
            )
            .fetchCount();
  }

  @Override
  public List<UserInfoResBody> findByUserInfo(String query) {
    QUser qUser = QUser.user;
    List<User> list = jpaQueryFactory.selectFrom(qUser)
        .where(qUser.fullName.eq(query)
          .or(qUser.mobile.eq(query))
          .or(qUser.email.eq(query))
        )
        .orderBy(qUser.fullName.asc()).fetch();

    return list.stream().map(user -> {
      UserInfoResBody ui = new UserInfoResBody();
      ui.setId(user.getId());
      ui.setName(user.getFullName());
      ui.setEmail(user.getEmail());
      ui.setMobile(ui.getMobile());
      return ui;
    }).collect(Collectors.toList());
  }

  @Override
  public List<MeResBody> selectUsers() throws Exception {
    return userMyBatis.selectUsers();
  }
}
