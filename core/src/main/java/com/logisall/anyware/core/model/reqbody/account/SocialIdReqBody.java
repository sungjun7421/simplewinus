package com.logisall.anyware.core.model.reqbody.account;

import com.logisall.anyware.core.domain.user.SocialId;
import com.logisall.anyware.core.model.BaseRequestBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class SocialIdReqBody extends BaseRequestBody {

  private static final long serialVersionUID = 8949141828540470595L;

  public SocialIdReqBody() {
    this.facebookId = "";
    this.facebookName = "";

    this.googleId = "";
    this.googleName = "";

    this.kakaoId = "";
    this.kakaoName = "";

    this.naverId = "";
    this.naverName = "";

    this.appleId = "";
    this.appleName = "";
  }

  @Schema(description = "페이스북 ID", nullable = true)
  private String facebookId;

  @Schema(description = "페이스북 Username", nullable = true)
  private String facebookName;


  @Schema(description = "구글 ID", nullable = true)
  private String googleId;

  @Schema(description = "구글 Username", nullable = true)
  private String googleName;


  @Schema(description = "카카오톡 ID", nullable = true)
  private String kakaoId;

  @Schema(description = "카카오톡 Username", nullable = true)
  private String kakaoName;


  @Schema(description = "애플 ID", nullable = true)
  private String appleId;

  @Schema(description = "애플 Username", nullable = true)
  private String appleName;


  @Schema(description = "Naver ID", nullable = true)
  private String naverId;

  @Schema(description = "Naver Username", nullable = true)
  private String naverName;


  public SocialId toSocialId() {

    return SocialId.builder()
        .appleId(this.appleId)
        .appleName(this.appleName)
        .facebookId(this.facebookId)
        .facebookName(this.facebookName)
        .googleId(this.googleId)
        .googleName(this.googleName)
        .kakaoId(this.kakaoId)
        .kakaoName(this.kakaoName)
        .build();
  }
}
