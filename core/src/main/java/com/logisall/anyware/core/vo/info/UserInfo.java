package com.logisall.anyware.core.vo.info;

import com.logisall.anyware.core.vo.BaseEntityInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserInfo extends BaseEntityInfo {

  private static final long serialVersionUID = -5511509450884992633L;

  private String email;
  private String mobile;
}
