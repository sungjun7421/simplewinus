package com.logisall.anyware.core.model.property.niceid;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Setter
@Getter
@ToString
@Component
@ConfigurationProperties(prefix = "niceid.config")
public class NiceIdConfigProperty implements Serializable {

  private static final long serialVersionUID = 1522656197116649543L;

  private String authType;
  private String popgubun;
  private String customize;
  private String gender;
  private String returnUrl;
  private String errorUrl;
}
