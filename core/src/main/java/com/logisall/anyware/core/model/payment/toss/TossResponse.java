package com.logisall.anyware.core.model.payment.toss;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@ToString
@Setter
@Getter
public class TossResponse implements Serializable {


    private static final long serialVersionUID = -322023099071455318L;

    private String oid;
    private int code; // 결제 생성에 '성공'했음을 나타냅니다.
    private String checkoutPage; // 생성된 결제를 진행할 웹페이지 URL입니다. (구매자를 이 URL로 보내주세요)
    private String payToken; // 생성된 결제건의 고유번호입니다. 결제를 진행할 때, 결제를 취소하거나 환불할 때, 결제의 현재 상태를 파악할 때 이 고유번호를 통해 해당 결제 건에 접근하게 되니 잘 보관해주세요!

    private String msg;
    private String errorCode;
}
