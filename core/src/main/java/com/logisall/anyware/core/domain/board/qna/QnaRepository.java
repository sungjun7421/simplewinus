package com.logisall.anyware.core.domain.board.qna;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface QnaRepository extends
        JpaRepository<com.logisall.anyware.core.domain.board.qna.Qna, Long>,
        QuerydslPredicateExecutor<com.logisall.anyware.core.domain.board.qna.Qna> {

    @Query("from QuestionsAndAnswers q where q.id = ?1 and q.relativeUser.email = ?2")
    Qna getByUser(Long id, String email);
}
