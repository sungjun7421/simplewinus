package com.logisall.anyware.core.service.account;

import com.logisall.anyware.core.domain.user.sns.SNSStatus;
import com.logisall.anyware.core.model.sns.GooglePojo;

import java.util.Map;

public interface SNSGoogleAPIService {

  Map getAccessToken(String code, String redirectUri);

  com.logisall.anyware.core.model.sns.GooglePojo getMe(String accessToken);

  SNSStatus getStatus(GooglePojo google);
}
