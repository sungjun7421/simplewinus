package com.logisall.anyware.core.model.map.juso;

import lombok.Data;

import java.util.Map;

@Data
public class JusoEntityCommon {

    public static final String ERROR_CODE_SUCCESS = "0"; // 정상
    public static final String ERROR_CODE_SYSTEM = "-999"; // 시스템 에러
    public static final String ERROR_CODE_E0001 = "E0001"; // 승인되지 않은 KEY 입니다.
    public static final String ERROR_CODE_E0002 = "E0002"; // 승인되지 않은 사이트 입니다.
    public static final String ERROR_CODE_E0003 = "E0003"; // 정상적인 경로로 접속하시기 바랍니다.
    public static final String ERROR_CODE_E0005 = "E0005"; // 검색어가 입력되지 않았습니다.
    public static final String ERROR_CODE_E0006 = "E0006"; // 주소를 상세히 입력해 주시기 바랍니다.

    private String errorMessage;

    private int countPerPage;

    private int totalCount;

    private int currentPage;

    private String errorCode;

    public JusoEntityCommon(Map<String, Object> common) {

        if (common == null) {
            new NullPointerException();
        }
        this.errorMessage = common.get("errorMessage").toString();
        this.countPerPage = Integer.valueOf(common.get("countPerPage").toString());
        this.totalCount = Integer.valueOf(common.get("totalCount").toString());
        this.currentPage = Integer.valueOf(common.get("currentPage").toString());
        this.errorCode = common.get("errorCode").toString();
    }

    public String getErrorMessageText() {

        if (this.errorMessage.equals(JusoEntityCommon.ERROR_CODE_SYSTEM)) {
            return "시스템 에러";
        } else if (this.errorMessage.equals(JusoEntityCommon.ERROR_CODE_E0001)) {
            return "승인되지 않은 KEY 입니다.";
        } else if (this.errorMessage.equals(JusoEntityCommon.ERROR_CODE_E0002)) {
            return "승인되지 않은 사이트 입니다.";
        } else if (this.errorMessage.equals(JusoEntityCommon.ERROR_CODE_E0003)) {
            return "정상적인 경로로 접속하시기 바랍니다.";
        } else if (this.errorMessage.equals(JusoEntityCommon.ERROR_CODE_E0005)) {
            return "검색어가 입력되지 않았습니다.";
        } else if (this.errorMessage.equals(JusoEntityCommon.ERROR_CODE_E0006)) {
            return "주소를 상세히 입력해 주시기 바랍니다.";
        }
        return "";
    }
}
