package com.logisall.anyware.core.model.reqbody.commerce.point;

import com.logisall.anyware.core.model.BaseRequestBody;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserSettingPointPasswordReqBody extends BaseRequestBody {

  private static final long serialVersionUID = -4001982184786556054L;
  private String pointPassword;
}
