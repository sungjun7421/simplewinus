package com.logisall.anyware.core.domain.commerce.product;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Objects;

@NoArgsConstructor(staticName = "getInstance")
public class ProductPredicate {
    public static final QProduct Q_PRODUCT = QProduct.product;

    private BooleanBuilder builder = new BooleanBuilder();

    public Predicate values() {
        return builder.getValue() == null ? builder.and(Q_PRODUCT.id.isNotNull()) : builder.getValue();
    }

    public com.logisall.anyware.core.domain.commerce.product.ProductPredicate startDate(final LocalDateTime startDate) {

        if (startDate != null) {
            builder.and(Q_PRODUCT.createdDate.goe(startDate));
        }
        return this;
    }

    public com.logisall.anyware.core.domain.commerce.product.ProductPredicate endDate(final LocalDateTime endDate) {

        if (endDate != null) {
            builder.and(Q_PRODUCT.createdDate.loe(endDate));
        }
        return this;
    }

    public com.logisall.anyware.core.domain.commerce.product.ProductPredicate search(String value) {

        if (!StringUtils.isEmpty(value)) {
            value = value.trim();

            builder.and(Q_PRODUCT.information.name.textKoKr.containsIgnoreCase(value)
                    .or(Q_PRODUCT.information.name.textEnUs.containsIgnoreCase(value))
                    .or(Q_PRODUCT.information.name.textJaJp.containsIgnoreCase(value))
                    .or(Q_PRODUCT.information.name.textZhCn.containsIgnoreCase(value))
                    .or(Q_PRODUCT.information.name.textZhTw.containsIgnoreCase(value))
            );
        }
        return this;
    }

    public com.logisall.anyware.core.domain.commerce.product.ProductPredicate category(final Long idCategory) {

        if (idCategory != null) {
            builder.and(Q_PRODUCT.categories.any().id.eq(idCategory));
        }
        return this;
    }

    public com.logisall.anyware.core.domain.commerce.product.ProductPredicate active(final Boolean isActive) {

        if (isActive != null) {
            builder.and(Q_PRODUCT.active.eq(isActive));
        }
        return this;
    }

    public com.logisall.anyware.core.domain.commerce.product.ProductPredicate locale(Locale locale) {

        if (!Objects.equals(locale, Locale.KOREA)
                && !Objects.equals(locale, Locale.US)
                && !Objects.equals(locale, Locale.CHINA)
                && !Objects.equals(locale, Locale.TAIWAN)
                && !Objects.equals(locale, Locale.JAPAN)) {
            locale = Locale.KOREA;
        }

        if (Objects.equals(locale, Locale.KOREA)) {
            builder.and(Q_PRODUCT.internationalMode.koKr.eq(true));
        } else if (Objects.equals(locale, Locale.US)) {
            builder.and(Q_PRODUCT.internationalMode.enUs.eq(true));
        } else if (Objects.equals(locale, Locale.CHINA)) {
            builder.and(Q_PRODUCT.internationalMode.zhCn.eq(true));
        } else if (Objects.equals(locale, Locale.TAIWAN)) {
            builder.and(Q_PRODUCT.internationalMode.zhTw.eq(true));
        } else if (Objects.equals(locale, Locale.JAPAN)) {
            builder.and(Q_PRODUCT.internationalMode.jaJp.eq(true));
        }
        return this;
    }
}
