package com.logisall.anyware.core.domain.commerce.stock;

import java.math.BigDecimal;

public interface StockRepositoryCustom {

    BigDecimal getQtyAvailable(Long idProduct); // 보유 재고
}
