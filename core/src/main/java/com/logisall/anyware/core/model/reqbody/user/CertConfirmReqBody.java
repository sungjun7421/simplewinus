package com.logisall.anyware.core.model.reqbody.user;

import com.logisall.anyware.core.model.BaseResponseBody;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.Pattern;

@Schema(description = "휴대폰/이메일 인증 확인")
@Slf4j
@Setter
@Getter
@ToString
public class CertConfirmReqBody extends BaseResponseBody {

    private static final long serialVersionUID = -707347076146318685L;

    @Schema(description = "인증코드")
    private String code;

    @Pattern(regexp = ValidUtils.PATTERN_MOBILE)
    @Schema(description = "휴대폰 번호", example = "01011112222", minLength = 10, maxLength = 11)
    private String mobile;

    @Schema(description = "모드 [ CERT_MOBILE(\"휴대폰 인증 및 수정\")" +
            ", CERT_EMAIL(\"이메일 존재 유무\")" +
            ", FIND_PASSWORD_EMAIL(\"비밀번호 찾기\")" +
            ", FIND_PASSWORD_MOBILE(\"비밀번호 찾기\")" +
            ", FIND_ACCOUNT_MOBILE(\"계졍 찾기\") ]")
    private CertReqBody.Mode mode;
}
