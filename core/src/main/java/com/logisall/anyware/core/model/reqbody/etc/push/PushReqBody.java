package com.logisall.anyware.core.model.reqbody.etc.push;

import com.logisall.anyware.core.model.BaseRequestBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class PushReqBody extends BaseRequestBody {

    private static final long serialVersionUID = 9157717506990016730L;

    @Schema(description = "Device Token")
    private String deviceToken;

    @Schema(description = "Title push")
    private String title;

    @Schema(description = "Content push")
    private String content;
}
