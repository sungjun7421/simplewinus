package com.logisall.anyware.core.service.commerce.cart;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.NotFoundException;
import com.logisall.anyware.core.config.exception.UnAuthorizedException;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.buyer.BuyerRepository;
import com.logisall.anyware.core.domain.commerce.cart.Cart;
import com.logisall.anyware.core.domain.commerce.cart.CartRepository;
import com.logisall.anyware.core.domain.commerce.order.OrderRepository;
import com.logisall.anyware.core.domain.commerce.product.Product;
import com.logisall.anyware.core.domain.commerce.product.ProductRepository;
import com.logisall.anyware.core.model.reqbody.commerce.cart.CartRequestBody;
import com.logisall.anyware.core.model.reqbody.commerce.cart.CartUpdateReqBody;
import com.logisall.anyware.core.service.commerce.buyer.BuyerService;
import com.logisall.anyware.core.service.commerce.cart.CartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private BuyerRepository buyerRepository;

    @Autowired
    private BuyerService buyerService;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Override
    @Transactional
    public Cart create(Cart cart) {

        return cartRepository.save(cart);
    }


    @Override
    @Transactional
    public Cart add(CartRequestBody cartReq, String emailBuyer) {

        Buyer buyer = buyerRepository.findByRelativeUser_Email(emailBuyer);
        Product product=productRepository.findById(cartReq.getIdProduct()).get();
        if(product==null || buyer==null) return null;
        Cart _cart = new Cart(buyer, product);
        _cart.setBuyCount(cartReq.getQty());
        return cartRepository.save(_cart);
    }

    @Override
    @Transactional
    public Cart get(Long cartId) {
        return cartRepository.findById(cartId).orElse(null);
    }

    @Override
    @Transactional
    public Cart get(String email, Long productId) {
        Buyer buyer=buyerRepository.findByRelativeUser_Email(email);
        if(buyer==null) return null;
        return Optional.ofNullable(cartRepository.findByRelativeBuyer_IdAndRelativeProduct_Id(buyer.getId(),productId))
                .map(cart -> {
                    cart.lazy();
                    return cart;
                }).orElse(null);
    }

    @Override
    @Transactional
    public List<Cart> list(Long idBuyer) {
        List<Cart> list = cartRepository.findByRelativeBuyer_Id(idBuyer, Sort.by(Sort.Direction.DESC, "createdDate"));
        list.forEach(cart -> {
            cart.lazy();
        });
        return list;
    }


    @Override
    @Transactional
    public Cart update(CartUpdateReqBody body, String email) {

        Buyer buyer = buyerRepository.findByRelativeUser_Email(email);
        List<Cart> carts = this.list(buyer.getId());
        if (carts.stream().anyMatch(cart -> Objects.equals(body.getIdCart(), cart.getId()))) {
            Cart cart = this.get(body.getIdCart());
            cart.setBuyCount(body.getQty());
            return cartRepository.save(cart);
        }
        throw new NotFoundException("not exist cart ::: " + body.getIdCart());
    }
    @Override
    @Transactional
    public Cart update(Cart cart) {

        return cartRepository.findById(cart.getId())
                .map(ori -> {
                    BeanUtils.copyProperties(cart, ori, Cart.IGNORE_PROPERTIES);
                    return cartRepository.save(ori);
                }).orElseThrow(BadRequestException::new);
    }

    @Override
    @Modifying
    @Transactional
    public void delete(Long cartId) {
        cartRepository.findById(cartId)
                .ifPresent(cart -> {
                    cartRepository.customDeleteByIdCard(cart.getId());
                });
    }

    @Override
    @Modifying
    @Transactional
    public void delete(Long cartId, String email) {
        Optional.ofNullable(buyerRepository.findByRelativeUser_Email(email))
                .ifPresent(buyer -> {

                    if (buyer.getCartList() != null &&
                            buyer.getCartList().stream().anyMatch(cart -> Objects.equals(cart.getId(), cartId))
                    ) {
                        this.delete(cartId);
                    } else {
                        throw new UnAuthorizedException();
                    }

                });
    }

    @Override
    @Modifying
    @Transactional
    public void deleteAllByProduct(Long productId){
        cartRepository.customDeleteAllByIdProduct(productId);
    }



    @Override
    @Modifying
    @Transactional
    public void deleteAll(String email) {
        Buyer buyer = buyerService.get(email);
        cartRepository.customDeleteAllByIdBuyer(buyer.getId());
    }

    @Override
    @Modifying
    @Transactional
    public void deleteByCarts(List<Long> idCarts, String email) {

        if(idCarts == null) {
            throw new BadRequestException();
        }

        idCarts.forEach(id->{
            this.delete(id, email);
        });
    }
}
