package com.logisall.anyware.core.model.resbody.commerce.paymentproduct;

import com.logisall.anyware.core.model.BaseResponseThumbBody;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.server.core.Relation;

@Relation(value = "paymentProduct", collectionRelation = "paymentProducts")
@Slf4j
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PaymentProductThumbResBody extends BaseResponseThumbBody {

  private static final long serialVersionUID = -7831683802576985306L;

  private long id;
  private String name;
  private int price;
}
