package com.logisall.anyware.core.service.send.kakao;

import com.logisall.anyware.core.model.property.kko.KKO;

import java.util.List;
import java.util.Map;

public interface SendKakaoMsgService {

    /**
     * 카카오 알림톡 발송
     *
     * @param kko 메세지 정보
     * @return 결과메세지
     */
    Map send(KKO kko);

    /**
     * REPORT 조회
     *
     * @param cmid cmid 서버에서 생성한 request 를 식별할 수 있는 유일한 키
     * @return 결과메세지
     */
    Map report(String cmid);

    /**
     * TEMPLATE 조회
     *
     * @return 결과메세지
     */
    List<Map<String, Object>> listTemplate();

    /**
     * 발신번호 인증/등록
     * 1차 등록 후, 2차 인증해야 함. (2번 요청해야함)
     *
     * @param sendnumber 발신번호("-" 제외)
     * @param comment    코멘트(200자)
     * @param pintype    인증방법(SMS/VMS)
     * @param pincode    인증번호 (1차 등록후, 인증번호 받은 후 2차 인증시 사용)
     * @return 결과메세지
     */
    Map saveSendNumber(String sendnumber, String comment, String pintype, String pincode);

    /**
     * 발신번호 리스트 조회
     *
     * @return 결과메세지
     */
    List<Map<String, Object>> listSendNumber();
}
