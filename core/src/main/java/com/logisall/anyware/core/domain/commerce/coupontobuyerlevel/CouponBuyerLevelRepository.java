package com.logisall.anyware.core.domain.commerce.coupontobuyerlevel;

import com.logisall.anyware.core.domain.commerce.coupontobuyerlevel.CouponBuyerLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface CouponBuyerLevelRepository extends
    JpaRepository<CouponBuyerLevel, CouponBuyerLevel.Id>,
    QuerydslPredicateExecutor<CouponBuyerLevel> {

  List<CouponBuyerLevel> findAllByRelativeCoupon_Id(Long idCoupon);
}
