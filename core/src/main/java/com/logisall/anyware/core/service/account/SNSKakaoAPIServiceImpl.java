package com.logisall.anyware.core.service.account;

import com.logisall.anyware.core.config.property.sns.KakaoApiKey;
import com.logisall.anyware.core.domain.user.SocialId;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.domain.user.sns.SNSStatus;
import com.logisall.anyware.core.model.sns.Kakao;
import com.logisall.anyware.core.service.restapi.RestAPIService;
import com.logisall.anyware.core.service.restapi.RestAPIServiceImpl;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Headers;
import okhttp3.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
public class SNSKakaoAPIServiceImpl implements SNSKakaoAPIService {

  @Autowired
  private KakaoApiKey apiKey;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private RestAPIService restAPIService;


  @Override
  public Map getAccessToken(String code, String redirectUri) {

    URI uri = UriComponentsBuilder.fromHttpUrl(apiKey.getAccessTokenUri()).build().toUri();

    log.debug("uri ::: {}", uri.toString());

    ResponseEntity<Map> response = null;
    try {
      response = restAPIService.post(
          uri,
          new Headers.Builder()
              .add("Content-Type", com.logisall.anyware.core.service.restapi.RestAPIServiceImpl.FORM.toString())
              .build(),
          RequestBody.create(
              com.logisall.anyware.core.service.restapi.RestAPIServiceImpl.FORM,
              "grant_type=authorization_code"
                  + "&client_id=" + apiKey.getClientId()
                  + "&client_secret=" + apiKey.getClientSecret()
                  + "&code=" + code
                  + "&redirect_uri=" + redirectUri
          ),
          Map.class
      );

      return response.getBody();
    } catch (Exception e) {
      log.error("SNS Kakao Login ERROR", e);
    }
    throw new RuntimeException("카카오 토큰정보를 가져오지 못했습니다.");
  }

  @Override
  public Map getMe(String accessToken) {

    URI uri = UriComponentsBuilder.fromHttpUrl(apiKey.getMeUri()).build().toUri();

    log.debug("uri ::: {}", uri.toString());

    ResponseEntity<Map> response = null;
    try {
      response = restAPIService.post(
          uri,
          new Headers.Builder()
              .add("Content-Type", com.logisall.anyware.core.service.restapi.RestAPIServiceImpl.FORM.toString())
              .add("Authorization", "Bearer " + accessToken)
              .build(),
          RequestBody.create(RestAPIServiceImpl.FORM, "secure_resource=true"),
          Map.class
      );

      return response.getBody();
    } catch (Exception e) {
      log.error("SNS Kakao Login ERROR", e);
    }
    throw new RuntimeException("카카오 회원정보를 가져오지 못했습니다.");
  }

  @Override
  @Transactional
  public com.logisall.anyware.core.domain.user.sns.SNSStatus getStatus(Kakao kakao) {

    Optional<com.logisall.anyware.core.domain.user.User> userOpt = userRepository.findBySocialIdKakaoId(kakao.getId());

    if (userOpt.isPresent()) {
      return com.logisall.anyware.core.domain.user.sns.SNSStatus.LINKED; // 이미 연동되어었음
    } else if (com.logisall.anyware.core.utils.StringUtils.isNotEmpty(kakao.getEmail())) {

      // 카카오에서 제공한 이메일 정보로 계정 조회
      userOpt = userRepository.findByEmail(kakao.getEmail());

      if (userOpt.isPresent()) {
        /* 카카오에서 제공한 이메일 정보로 조회한 계정이 존재한다면 */

        User user = userOpt.get();

        if (user.isWithd()) {
          /* 탈퇴한 계졍이라면 */
          return com.logisall.anyware.core.domain.user.sns.SNSStatus.LEAVED_ACCOUNT;
        }

        if (user.getSocialId() == null || com.logisall.anyware.core.utils.StringUtils.isEmpty(user.getSocialId().getKakaoId())) {
          /* 계정에 카카오 ID가 연동되어 있지 않으면 연동 시켜준다. */

          com.logisall.anyware.core.domain.user.SocialId socialId = user.getSocialId() == null ? new SocialId() : user.getSocialId();
          socialId.setKakaoId(kakao.getId());
          socialId.setKakaoName(kakao.getNickName());

          user.setSocialId(socialId);
          if (StringUtils.isNotEmpty(kakao.getProfileImage())) {
            user.setImage(kakao.getProfileImage());
          }

          userRepository.save(user);
          return com.logisall.anyware.core.domain.user.sns.SNSStatus.CONNECT;
        } else {
          /* 계정에 다른 페이스북 ID가 연동되어 있다면 */
          return com.logisall.anyware.core.domain.user.sns.SNSStatus.NOT_MATCH_SNS;
        }
      } else {
        /* 페이스북에서 제공한 정보와 동일한 이메일과 페이스북 ID 가 존재하지 않는다. -> 회원가입 */
        return com.logisall.anyware.core.domain.user.sns.SNSStatus.NOT_EXISTED_ACCOUNT;
      }
    } else {
      /* 페이스북에서 이메일을 제공하지 않았다면 -> 회원가입 */
      return SNSStatus.NOT_PROVIDED_EMAIL;
    }
  }
}
