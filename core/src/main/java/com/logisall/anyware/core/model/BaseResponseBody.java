package com.logisall.anyware.core.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class BaseResponseBody implements Serializable {

  private static final long serialVersionUID = -1023974204236974960L;

//  public static final String SUCCESS = "success";
//  public static final String FAIL = "fail";
//
//  private String result = SUCCESS;
//  private String message;
//
//  public boolean isSuccess() {
//    return Objects.equals(this.result, SUCCESS);
//  }
//
//  public static BaseResponseBody of(boolean result) {
//    BaseResponseBody baseResponseBody = new BaseResponseBody();
//    if (result) {
//      baseResponseBody.setResult(BaseResponseBody.SUCCESS);
//    } else {
//      baseResponseBody.setResult(BaseResponseBody.FAIL);
//    }
//    return baseResponseBody;
//  }
//
//  public static BaseResponseBody of(boolean result, String message) {
//    BaseResponseBody baseResponseBody = new BaseResponseBody();
//    if (result) {
//      baseResponseBody.setResult(BaseResponseBody.SUCCESS);
//    } else {
//      baseResponseBody.setResult(BaseResponseBody.FAIL);
//    }
//    baseResponseBody.setMessage(message);
//    return baseResponseBody;
//  }
}
