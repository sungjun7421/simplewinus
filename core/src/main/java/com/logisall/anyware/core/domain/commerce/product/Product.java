package com.logisall.anyware.core.domain.commerce.product;

import com.logisall.anyware.core.domain.AbstractEntityInternational;
import com.logisall.anyware.core.domain.RestEntityThumbBody;
import com.logisall.anyware.core.domain.commerce.category.Category;
import com.logisall.anyware.core.domain.commerce.paymentproduct.PaymentProduct;
import com.logisall.anyware.core.domain.commerce.product.Information;
import com.logisall.anyware.core.model.reqbody.commerce.product.ProductReqBody;
import com.logisall.anyware.core.model.resbody.commerce.product.ProductResBody;
import com.logisall.anyware.core.model.resbody.commerce.product.ProductThumbResBody;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * 직접원가(직접재료비+직접노무비+직접경비) (Direct Cost) + 제조간접비 (Overhead Cost) = 제조원가 (Manufacturing Cost)
 * 제조원가 + 판매비 + 관리비 = 판매원가 (Cost of Good Sold, COGS)
 * 판매원가 (COGS) + 손익 = 판매가격 (price)
 */
@Entity
@Getter
@Setter
@ToString(exclude = {"paymentProducts", "categories"})
public class Product extends AbstractEntityInternational<Long> implements RestEntityThumbBody<ProductResBody, ProductThumbResBody> {

  private static final long serialVersionUID = 4078988878951963970L;

  public static String[] IGNORE_PROPERTIES = {
      "id",
      "productHistories",
      "paymentProducts"
  };

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @Embedded
  private Information information; // 상품 명, 서브 타이틀, 설명 등

  // == 가격
  @Column(columnDefinition = "DECIMAL(19,0) default 0")
  private BigDecimal cogs; // 매출원가 (Cost of good sold) (통계용 Option)

  @Column(columnDefinition = "DECIMAL(19,0) default 0")
  private BigDecimal price; // 판매가 (Price) (required)

  @Column(columnDefinition = "DECIMAL(19,0) default 0")
  private BigDecimal dcRate; // 할인율 (Discount)

  // 할인 가격
  public BigDecimal getDiscount() {
    if (this.price == null || this.price.intValue() == 0) {
      return BigDecimal.valueOf(0);
    }
    if (this.price.intValue() > 0 && this.dcRate.intValue() > 0) {
      return this.price.multiply(this.dcRate.divide(BigDecimal.valueOf(100), 4, RoundingMode.HALF_UP));
    }
    return BigDecimal.valueOf(0);
  }

  // 판매가격 (판매액 - (판매액×할인율/100))
  public BigDecimal getSalePrice() {
    if (this.price == null || this.price.intValue() == 0) {
      return BigDecimal.valueOf(0);
    }
    if (this.dcRate != null && this.dcRate.intValue() > 0) {
      return this.price.subtract(this.getDiscount());
    }
    return BigDecimal.valueOf(0);
  }

  // ==== 구매 내역
  @JsonIgnore
  @OrderBy("createdDate DESC")
  @OneToMany(mappedBy = "relativeProduct", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<PaymentProduct> paymentProducts = new ArrayList<>();

  // == CATEGORY
  @OrderBy("orderAscending ASC")
  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "ProductToCategory",
      joinColumns = @JoinColumn(name = "idProduct", nullable = false, foreignKey = @ForeignKey(name = "FK_Product_For_Product_To_Cate")),
      inverseJoinColumns = @JoinColumn(name = "idCategory", nullable = false, foreignKey = @ForeignKey(name = "FK_Category_For_Product_To_Cate"))
  )
  private List<Category> categories;

  @Column(columnDefinition = "BIT(1) default 1")
  private boolean active; // 활성/비활성

  @Column(columnDefinition = "BIGINT(20) default 0")
  private long orderDescending; // 순서, 내림차순

  public void changeOrder(com.logisall.anyware.core.domain.commerce.product.Product target) {

    com.logisall.anyware.core.domain.commerce.product.Product source = this;

    long sourceOrderDescending = source.getOrderDescending();
    long targetOrderDescending = target.getOrderDescending();

    target.setOrderDescending(sourceOrderDescending);
    source.setOrderDescending(targetOrderDescending);
  }

  @Override
  public void setLocale(Locale locale) {
    super.setLocale(locale);

    this.information.setLocale(locale);
  }

  @Override
  public void delete() {

  }

  @Override
  public void lazy() {
    if (this.paymentProducts != null) {
      this.paymentProducts.size();
    }
    if (this.categories != null) {
      this.categories.size();
      for (Category cat : categories) {
        cat.lazy();
      }
    }
  }

  @Override
  public ProductResBody toBody(Locale locale) {
    this.setLocale(locale);
    return ProductResBody.builder().build();
  }

  @Override
  public ProductThumbResBody toThumbBody(Locale locale) {
    this.setLocale(locale);
    return ProductThumbResBody.builder().build();
  }

  @Transient
  private ProductReqBody productReqBody;
}
