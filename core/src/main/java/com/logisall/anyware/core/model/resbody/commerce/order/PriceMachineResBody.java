package com.logisall.anyware.core.model.resbody.commerce.order;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PriceMachineResBody {

    private static final long serialVersionUID = 2220963509007629378L;

    private Long idMachine;
    private BigDecimal priceMachine;
    private Long timeWash;
}
