package com.logisall.anyware.core.model.payment.toss;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class TossResult implements Serializable {

    private static final long serialVersionUID = -8974011024006929625L;

    private int code;
    private String approvalTime;
    private int amount;
    private String orderNo;
    private String msg;
    private String errorCode;
    private Long idOrder;
}
