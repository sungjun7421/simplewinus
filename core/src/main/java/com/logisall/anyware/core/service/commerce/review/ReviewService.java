package com.logisall.anyware.core.service.commerce.review;

import com.logisall.anyware.core.domain.commerce.review.Review;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.reqbody.commerce.review.ReviewReqBody;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.PagedModel;

import java.math.BigDecimal;
import java.util.Locale;

public interface ReviewService {

    Boolean hasPayment(Long idRelativePaymentProduct);

    com.logisall.anyware.core.domain.commerce.review.Review setValue(ReviewReqBody reviewReqBody, String email);
    BigDecimal averageStar();

    PagedModel<?> pagedResources(Locale locale,Long id, com.logisall.anyware.core.model.Filter filter);

    Page<com.logisall.anyware.core.domain.commerce.review.Review> page(Filter filter, Long idUser);

    com.logisall.anyware.core.domain.commerce.review.Review get(Long id);

    com.logisall.anyware.core.domain.commerce.review.Review update(Review review);

    void delete(Long id);
}
