package com.logisall.anyware.core.model.resbody.commerce.order;

import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.commerce.PayMethod;
import com.logisall.anyware.core.domain.commerce.order.BuyerInfo;
import com.logisall.anyware.core.model.resbody.commerce.ordermachine.OrderMachineResBody;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Relation(value = "order", collectionRelation = "orders")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderDetailResBody implements Serializable {

  private static final long serialVersionUID = 3930505083408808248L;

  private Long id;
  private String oid;
  private String title;
  private BuyerInfo buyerInfo;
  private OrderStatus orderStatus;
  private PayMethod payMethod;
  private boolean paymentComplete;
  private LocalDateTime paymentDate;
  private String failedMsg;
//  private Buyer relativeBuyer;
  private OrderMachineResBody relativeOrderMachine;
  private BigDecimal amount;
  private BigDecimal fcCoupon;
  private BigDecimal fcPoint;
  private BigDecimal totalPrice;
  private boolean isMember;
  private String mobileNonMember;
  private BigDecimal cumulativePoint;
}
