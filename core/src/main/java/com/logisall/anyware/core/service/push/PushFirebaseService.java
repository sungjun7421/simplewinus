package com.logisall.anyware.core.service.push;

import com.logisall.anyware.core.model.firebase.NotificationRequest;

import java.util.concurrent.ExecutionException;

public interface PushFirebaseService {

  void send(final NotificationRequest notificationRequest) throws InterruptedException, ExecutionException;
}
