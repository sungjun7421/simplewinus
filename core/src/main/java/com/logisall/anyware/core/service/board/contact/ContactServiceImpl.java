package com.logisall.anyware.core.service.board.contact;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.board.contact.Contact;
import com.logisall.anyware.core.domain.board.contact.ContactPredicate;
import com.logisall.anyware.core.domain.board.contact.ContactRepository;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.board.contact.ContactService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

@Slf4j
@Service
public class ContactServiceImpl implements ContactService {

  @Autowired
  private ContactRepository contactRepository;

  @Autowired
  private UserRepository userRepository;

  @Override
  @Transactional
  public Contact create(Contact contact) {
    return contactRepository.save(contact);
  }

  @Override
  @Transactional
  public Contact update(Contact contact) {

    if (contact.getId() == null) {
      throw new BadRequestException();
    }

    return contactRepository.findById(contact.getId())
        .map(ori -> {
          BeanUtils.copyProperties(contact, ori, Contact.IGNORE_PROPERTIES);
          return contactRepository.save(ori);
        }).orElseThrow(RuntimeException::new);
  }

  @Override
  @Transactional(readOnly = true)
  public Contact get(Locale locale, Long id) {

    return contactRepository.findById(id)
        .map(contact -> {
          contact.lazy();
          return contact;
        }).orElse(null);
  }

  @Override
  @Transactional
  public void delete(Long id) {

    contactRepository.findById(id).ifPresent(contact -> {
      contact.delete();
      contactRepository.delete(contact);
    });
  }

  @Override
  @Transactional(readOnly = true)
  public Page<Contact> page(Locale locale, Filter filter) {

    Page<Contact> page = contactRepository.findAll(
        ContactPredicate.getInstance()
            .search(filter.getQuery())
            .startDate(filter.getStartDate())
            .endDate(filter.getEndDate())
            .values(),
        filter.getPageable());

    page.forEach(Contact::lazy);
    return page;
  }

}
