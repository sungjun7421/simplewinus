package com.logisall.anyware.core.service.setting;

import com.google.common.collect.ImmutableMap;
import com.logisall.anyware.core.domain.International.InternationalMode;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.domain.setting.AppSettingRepository;
import com.logisall.anyware.core.domain.setting.ComponentMode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Slf4j
@Service
@Transactional
public class AppSettingServiceImpl implements AppSettingService {

  @Autowired
  private AppSettingRepository settingRepository;

  @Override
  public com.logisall.anyware.core.domain.setting.AppSetting setting(com.logisall.anyware.core.domain.setting.AppSetting appSetting) {
    log.debug("## appSetting ::: {}", appSetting);
    if (appSetting.getId() == null) {
      return settingRepository.save(appSetting);
    } else {
      Optional<com.logisall.anyware.core.domain.setting.AppSetting> optAppSetting = settingRepository.findById(appSetting.getId());

      if (optAppSetting.isPresent()) {

        com.logisall.anyware.core.domain.International.InternationalMode im = appSetting.getInternationalMode();
        if (im != null && appSetting.isInternational()) {
          final Locale defaultLocale = appSetting.getDefaultLocale();

          if (defaultLocale.equals(Locale.KOREA)) {
            if (!im.isKoKr()) {
              im.setKoKr(true);
            }
          } else if (defaultLocale.equals(Locale.US)) {
            if (!im.isEnUs()) {
              im.setEnUs(true);
            }
          } else if (defaultLocale.equals(Locale.CHINA)) {
            if (!im.isZhCn()) {
              im.setZhCn(true);
            }
          } else if (defaultLocale.equals(Locale.TAIWAN)) {
            if (!im.isZhTw()) {
              im.setZhTw(true);
            }
          } else if (defaultLocale.equals(Locale.JAPAN)) {
            if (!im.isJaJp()) {
              im.setJaJp(true);
            }
          }
        } else {
          im = new com.logisall.anyware.core.domain.International.InternationalMode();
          appSetting.setInternationalMode(im);
        }

        com.logisall.anyware.core.domain.setting.AppSetting oriAppSetting = optAppSetting.get();
        BeanUtils.copyProperties(appSetting, oriAppSetting, com.logisall.anyware.core.domain.setting.AppSetting.IGNORE_PROPERTIES);
        return settingRepository.save(oriAppSetting);
      } else {
        return settingRepository.save(appSetting);
      }
    }
  }

  @Override
  public com.logisall.anyware.core.domain.setting.AppSetting getSetting() {
    Page<com.logisall.anyware.core.domain.setting.AppSetting> page = settingRepository.findAll(PageRequest.of(0, 1, Sort.Direction.DESC, "createdDate"));

    if (page.getContent().size() > 0) {
      return page.getContent().get(0);
    }

    com.logisall.anyware.core.domain.setting.AppSetting appSetting = new AppSetting();
    com.logisall.anyware.core.domain.International.InternationalMode im = new com.logisall.anyware.core.domain.International.InternationalMode();
    im.setKoKr(true);
    appSetting.setInternationalMode(im);
    com.logisall.anyware.core.domain.setting.ComponentMode cm = new ComponentMode();
    appSetting.setComponentMode(cm);
    return this.setting(appSetting);
  }

  @Override
  public com.logisall.anyware.core.domain.International.InternationalMode getInternationalMode() {
    return this.getSetting().getInternationalMode();
  }

//  @Override
//  public ComponentMode getComponentMode() {
//    return this.getSetting().getComponentMode();
//  }
//
  @Override
  public Locale getDefaultLocale() {
    return this.getSetting().getDefaultLocale();
  }

  @Override
  public List<Map<String, String>> languages() {
    InternationalMode im = this.getInternationalMode();
    List<Map<String, String>> locales = new ArrayList<>();
    if (im.isKoKr()) {
      locales.add(ImmutableMap.of("code", "ko-KR", "name", "한국어"));
    }
    if (im.isEnUs()) {
      locales.add(ImmutableMap.of("code", "en-US", "name", "English"));
    }
    if (im.isZhCn()) {
      locales.add(ImmutableMap.of("code", "zh-CN", "name", "中国"));
    }
    if (im.isZhTw()) {
      locales.add(ImmutableMap.of("code", "zh-TW", "name", "中國傳統"));
    }
    if (im.isJaJp()) {
      locales.add(ImmutableMap.of("code", "ja-JP", "name", "日本"));
    }
    return locales;
  }
}
