package com.logisall.anyware.core.domain.board.post.category;

import com.logisall.anyware.core.domain.AbstractEntityCategory;
import com.logisall.anyware.core.domain.RestEntityBody;
import com.logisall.anyware.core.domain.board.post.Post;
import com.logisall.anyware.core.model.resbody.board.CategoryResBody;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

@Entity(name = "PostCategory")
@Getter
@Setter
@ToString()
@NoArgsConstructor
public class PCategory extends AbstractEntityCategory<Long> implements RestEntityBody<com.logisall.anyware.core.model.resbody.board.CategoryResBody> {

  private static final long serialVersionUID = -714603746092485437L;

  public static String[] IGNORE_PROPERTIES = {
      "id",
      "type",
      "orderAscending",
      "posts"
  };

  @PrePersist
  public void prePersist() {
  }

  @PreUpdate
  public void PreUpdate() {

  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @NonNull
  @Column(columnDefinition = "TINYINT(1) default 0", updatable = false)
  private com.logisall.anyware.core.domain.board.post.Post.Type type; // 수정 못함

  @OrderBy("regDate DESC")
  @ManyToMany(mappedBy = "categories")
  private List<Post> posts = new ArrayList<>();

  @Override
  public void delete() {

    if (this.posts != null) {
      this.posts.forEach(post -> {
        post.getCategories().removeIf(category -> Objects.equals(category.getId(), this.id));
      });
    }
  }

  @Override
  public void lazy() {
    if (this.posts != null) {
      this.posts.size();
    }
  }

  @Override
  public com.logisall.anyware.core.model.resbody.board.CategoryResBody toBody(Locale locale) {
    this.setLocale(locale);

    return CategoryResBody.builder()
        .id(this.id)
        .name(this.name.getValue())
        .build();
  }
}
