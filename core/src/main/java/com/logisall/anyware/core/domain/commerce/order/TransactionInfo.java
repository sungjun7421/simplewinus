package com.logisall.anyware.core.domain.commerce.order;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

@Setter
@Getter
@ToString
@Embeddable
public class TransactionInfo implements Serializable {

    @Getter
    public enum PG {
        WELCOME,
        TOSS,
        KAKAO,
    }

    private static final long serialVersionUID = -2222517294750106416L;

    private final static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    @Column(length = 5)
    @Enumerated(EnumType.STRING)
    private PG txPG; // PG Code

    @Column(length = 20)
    private String txStatus; // 결제 모듈의 상태값

    @Column(length = 20)
    private String txId; // 결제모듈의 주문아이디(트랜잭션 아이디)

    @Column
    private String txReceiptUrl; // 결제모듈의 신용카드 매출전표 URL(영수증)

    @Column
    private String txFailReason; // 결제모듈의 실패 사유

    @Column
    private String txCancelReason; // 결제모듈의 취소 사유

    @Lob
    @Column(columnDefinition = "TEXT", length = 65535)
    private String txJson; // 응답값 JSON

    @Column
    private String txCaptureId; // 페이팔 캡쳐 아이디 (환불할때 사용)

    @Column(length = 20)
    private String txVbankNum; // 가상계좌 입금계좌번호

    @Column
    private Long txVbankDate; // 가상계좌 입금기한

    @Column(length = 20)
    private String txVbankName; // 가상계좌 은행명

    @Column(length = 10)
    private String txVbankHolder; // 가상계좌 예금주

    public LocalDateTime getVbankDate() {
        if (this.txVbankDate == null) {
            return null;
        }
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(txVbankDate), TimeZone.getDefault().toZoneId());
    }

    public String getVbankDateString() {
        if (this.txVbankDate == null) {
            return null;
        }
        return FORMATTER.format(this.getVbankDate());
    }
}
