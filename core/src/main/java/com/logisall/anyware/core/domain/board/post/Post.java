package com.logisall.anyware.core.domain.board.post;

import com.logisall.anyware.core.config.serializer.JsonLocalDateTimeSerializer;
import com.logisall.anyware.core.domain.AbstractEntityInternational;
import com.logisall.anyware.core.domain.International.InterText;
import com.logisall.anyware.core.domain.RestEntityThumbBody;
import com.logisall.anyware.core.domain.board.comment.Comment;
import com.logisall.anyware.core.domain.board.post.category.PCategory;
import com.logisall.anyware.core.domain.embedd.SEO;
import com.logisall.anyware.core.domain.file.FileEntity;
import com.logisall.anyware.core.model.resbody.board.PostResBody;
import com.logisall.anyware.core.model.resbody.board.PostThumbResBody;
import com.logisall.anyware.core.utils.DateUtils;
import com.logisall.anyware.core.utils.HtmlUtils;
import com.logisall.anyware.core.utils.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * 게시판
 */
@Slf4j
@Entity(name = "Post")
@Getter
@Setter
@ToString(exclude = {"comments", "files"})
@NoArgsConstructor
public class Post extends AbstractEntityInternational<Long>
        implements RestEntityThumbBody<PostResBody, PostThumbResBody> {

    private static final long serialVersionUID = 950674970843219381L;

    public static String[] IGNORE_PROPERTIES = {
            "id",
            "type",
            "pageView",
            "comments"
    };

    @Getter
    public enum Type {
        NOTICE("공지사항"),
        CONTENTS("콘텐츠"),
        ;
        final String value;

        Type(final String value) {
            this.value = value;
        }
    }

    @PrePersist
    public void prePersist() {
        if (this.type == null)
            this.type = Type.NOTICE;
        if (this.regDate == null)
            this.regDate = LocalDateTime.now();
        if (StringUtils.isNotEmpty(this.content.getTextEnUs()))
            this.content.setTextEnUs(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextEnUs()));
        if (StringUtils.isNotEmpty(this.content.getTextJaJp()))
            this.content.setTextJaJp(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextJaJp()));
        if (StringUtils.isNotEmpty(this.content.getTextKoKr()))
            this.content.setTextKoKr(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextKoKr()));
        if (StringUtils.isNotEmpty(this.content.getTextZhCn()))
            this.content.setTextZhCn(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextZhCn()));
        if (StringUtils.isNotEmpty(this.content.getTextZhTw()))
            this.content.setTextZhTw(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextZhTw()));
    }

    @PreUpdate
    public void PreUpdate() {
        if (this.type == null)
            this.type = Type.NOTICE;
        if (this.regDate == null)
            this.regDate = LocalDateTime.now();
        if (StringUtils.isNotEmpty(this.content.getTextEnUs()))
            this.content.setTextEnUs(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextEnUs()));
        if (StringUtils.isNotEmpty(this.content.getTextJaJp()))
            this.content.setTextJaJp(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextJaJp()));
        if (StringUtils.isNotEmpty(this.content.getTextKoKr()))
            this.content.setTextKoKr(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextKoKr()));
        if (StringUtils.isNotEmpty(this.content.getTextZhCn()))
            this.content.setTextZhCn(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextZhCn()));
        if (StringUtils.isNotEmpty(this.content.getTextZhTw()))
            this.content.setTextZhTw(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextZhTw()));
    }

    public Post(Type type) {
        this.type = type;
    }

    @Id
    @GeneratedValue
    @Column(unique = true)
    private Long id;

    @Enumerated
    @Column(columnDefinition = "TINYINT(1) default 0")
    private Type type; // 게시판 유형

    @Column
    private String thumbnail; // 썸네일 이미지

    @Embedded
    @AttributeOverrides({        // 임베디드 타입에 정의한 매핑정보를 재정의
            @AttributeOverride(name = "textKoKr", column = @Column(name = "titleKoKr", length = 200)),
            @AttributeOverride(name = "textEnUs", column = @Column(name = "titleEnUs", length = 200)),
            @AttributeOverride(name = "textJaJp", column = @Column(name = "titleJaJp", length = 200)),
            @AttributeOverride(name = "textZhCn", column = @Column(name = "titleZhCn", length = 200)),
            @AttributeOverride(name = "textZhTw", column = @Column(name = "titleZhTw", length = 200))
    })
    private InterText title; // 제목

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "textKoKr", column = @Column(name = "contentKoKr", columnDefinition = "LONGTEXT")),
            @AttributeOverride(name = "textEnUs", column = @Column(name = "contentEnUs", columnDefinition = "LONGTEXT")),
            @AttributeOverride(name = "textJaJp", column = @Column(name = "contentJaJp", columnDefinition = "LONGTEXT")),
            @AttributeOverride(name = "textZhCn", column = @Column(name = "contentZhCn", columnDefinition = "LONGTEXT")),
            @AttributeOverride(name = "textZhTw", column = @Column(name = "contentZhTw", columnDefinition = "LONGTEXT"))
    })
    private InterText content; // 내용

    @Column(columnDefinition = "BIGINT(20) default 0")
    private long pageView; // 조회수

    @Column(columnDefinition = "BIT(1) default 0")
    private boolean top; // 상단고정

    @Column(columnDefinition = "BIT(1) default 1")
    private boolean active; // 활성/비활성

    @DateTimeFormat(pattern = DateUtils.FORMAT_DATE_TIME_UNIT_BAR)
    @JsonSerialize(using = JsonLocalDateTimeSerializer.class)
    private LocalDateTime regDate; // 등록일 , 내림차순

    @Embedded
    private SEO seo; // 검색 엔진 최적화

    @JsonIgnore
    @OrderBy("orderAscending ASC")
    @ManyToMany
    @JoinTable(name = "PostToCategory",
            joinColumns = @JoinColumn(name = "idPost", nullable = false, foreignKey = @ForeignKey(name = "FK_Post_For_Post_To_Cate")),
            inverseJoinColumns = @JoinColumn(name = "idCategory", nullable = false, foreignKey = @ForeignKey(name = "FK_Category_For_Post_To_Cate"))
    )
    private List<PCategory> categories = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "relativePost", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Comment> comments = new ArrayList<>();

    @JsonIgnore
    @OrderBy("createdDate ASC")
    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "PostToFile",
            joinColumns = @JoinColumn(name = "idPost", nullable = false, foreignKey = @ForeignKey(name = "FK_Post_For_Post_To_File")),
            inverseJoinColumns = @JoinColumn(name = "idFile", nullable = false, foreignKey = @ForeignKey(name = "FK_File_For_Post_To_File"))
    )
    private List<FileEntity> files = new ArrayList<>();

    public void uploadFiles() {
        if (this.files != null) {
            this.files.removeIf(file -> StringUtils.isEmpty(file.getUrl()));
        }
    }

    @Override
    public void setLocale(Locale locale) {
        super.setLocale(locale);
        if (this.title != null)
            this.title.setLocale(this.locale);
        if (this.content != null)
            this.content.setLocale(this.locale);
        if (this.categories != null) {
            this.categories.forEach(category -> {
                category.setLocale(locale);
            });
        }
    }

    @Override
    public void delete() {

    }

    @Override
    public void lazy() {
        if (this.categories != null)
            this.categories.size();
        if (this.comments != null)
            this.comments.size();
        if (this.files != null)
            this.files.size();
    }

    @Override
    public PostResBody toBody(Locale locale) {
        this.setLocale(locale);

        return PostResBody.builder()
                .id(this.id)
                .type(this.type)
                .title(this.title.getValue())
                .content(this.content.getValue())
                .thumbnail(this.thumbnail)
                .pageView(this.pageView)
                .regDate(this.regDate)
                .seo(this.seo)
                .top(this.top)
                .build();
    }

    @Override
    public PostThumbResBody toThumbBody(Locale locale) {
        this.setLocale(locale);

        return PostThumbResBody.builder()
                .id(this.id)
                .type(this.type)
                .title(this.title.getValue())
                .thumbnail(this.thumbnail)
                .pageView(this.pageView)
                .regDate(this.regDate)
                .top(this.top)
                .build();
    }
}
