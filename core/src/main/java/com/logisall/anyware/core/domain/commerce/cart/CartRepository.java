package com.logisall.anyware.core.domain.commerce.cart;

import com.logisall.anyware.core.domain.commerce.cart.Cart;
import com.logisall.anyware.core.domain.commerce.cart.CartRepositoryCustom;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface CartRepository extends
        JpaRepository<Cart, Long>,
        QuerydslPredicateExecutor<Cart> ,
        CartRepositoryCustom {

    List<Cart> findByRelativeBuyer_Id(Long idBuyer, Sort sort);
    List<Cart> findByRelativeProduct_Id(Long idProduct);
    Cart findByRelativeBuyer_IdAndRelativeProduct_Id(Long idBuyer,Long productId);

    @Modifying
    @Query("delete from Cart c where c.id=:cartId ")
    public void customDeleteByIdCard(Long cartId);

    @Modifying
    @Query("delete from Cart c where c.relativeBuyer.id=:idBuyer ")
    public void customDeleteAllByIdBuyer(Long idBuyer);

    @Modifying
    @Query("delete from Cart c where c.relativeProduct.id=:idProduct ")
    public void customDeleteAllByIdProduct(Long idProduct);

}
