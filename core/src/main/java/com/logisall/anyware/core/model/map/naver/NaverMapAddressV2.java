package com.logisall.anyware.core.model.map.naver;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class NaverMapAddressV2 implements Serializable {

  private static final long serialVersionUID = -6021515937539606518L;

  private String name;
  private String roadAddress;
  private String jibunAddress;
  private String phoneNumber;
  private double x;
  private double y;
  private int distance;
  private String sessionId;
}
