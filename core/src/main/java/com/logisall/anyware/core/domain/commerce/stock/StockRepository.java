package com.logisall.anyware.core.domain.commerce.stock;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface StockRepository extends
    JpaRepository<com.logisall.anyware.core.domain.commerce.stock.Stock, Long>,
    QuerydslPredicateExecutor<Stock>,
    StockRepositoryCustom {
}
