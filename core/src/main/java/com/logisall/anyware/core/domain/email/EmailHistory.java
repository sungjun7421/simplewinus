package com.logisall.anyware.core.domain.email;

import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.utils.DateUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.time.LocalDateTime;

@Slf4j
@Entity
@Getter
@Setter
@ToString(exclude = {"relativeUser"})
public class EmailHistory extends AbstractEntity<Long> {

  private static final long serialVersionUID = 7019167884689868297L;

  public static String[] IGNORE_PROPERTIES = {
    "id",
    "relativeUser"
  };

  @PrePersist
  public void prePersist() {
    if (this.sendTime == null) {
      this.sendTime = LocalDateTime.now();
    }
  }

  @PreUpdate
  public void PreUpdate() {
  }

  @Getter
  public enum Type {
    LOGIN("Login"),
    SEND_CODE("Send code");

    private final String value;

    Type(final String value) {
      this.value = value;
    }
  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;  // PK

  @Column
  @Email
  private String sendAddress; // 보낸 이메일 주소

  @Column
  @Email
  private String destAddress; // 받는 이메일 주소

  @Enumerated
  @Column(columnDefinition = "TINYINT(1) default 0")
  private Type type;

  @Column
  private String code;

  @Column
  @DateTimeFormat(pattern = DateUtils.FORMAT_DATE_TIME_UNIT_BAR)
  private LocalDateTime sendTime; // 보낸시간

  @Column
  private String subject;

  @Column(columnDefinition = "LONGTEXT")
  private String msgBody;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "idUser", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_User_For_EmailHistory")) // Column name, 참조하는 ID(pk) name
  private User relativeUser; // 보낸사람


  @Override
  public void delete() {

  }

  @Override
  public void lazy() {

  }
}
