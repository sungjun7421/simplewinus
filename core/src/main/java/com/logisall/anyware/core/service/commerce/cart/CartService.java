package com.logisall.anyware.core.service.commerce.cart;

import com.logisall.anyware.core.domain.commerce.cart.Cart;
import com.logisall.anyware.core.model.reqbody.commerce.cart.CartRequestBody;
import com.logisall.anyware.core.model.reqbody.commerce.cart.CartUpdateReqBody;

import java.util.List;

public interface CartService {
    // CRU
    Cart create(Cart cart);

    // 장바구니 담기
    Cart add(CartRequestBody cartReq, String email);

    // 장바구니 목록
    Cart get(Long cartId);
    Cart get(String email,Long productId);
    List<Cart> list(Long idBuyer);
    Cart update(CartUpdateReqBody body, String email);
    Cart update(Cart cart);


    // 장바구니 삭제
    void delete(Long cartId);
    void delete(Long cartId, String email);
    void deleteAllByProduct(Long productId);

    // 장바구니 전체 삭제
    void deleteAll(String email);
    void deleteByCarts(List<Long> idCarts, String email);
}
