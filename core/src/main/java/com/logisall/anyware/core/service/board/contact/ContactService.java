package com.logisall.anyware.core.service.board.contact;

import com.logisall.anyware.core.domain.board.contact.Contact;
import com.logisall.anyware.core.service.DomainService;

public interface ContactService extends DomainService<Contact, Long> {

}
