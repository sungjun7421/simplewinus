package com.logisall.anyware.core.config.exception;

import lombok.Getter;

import java.io.Serializable;

@Getter
public enum ResponseErrorCode implements Serializable {

    // 400
    NOT_ENTERED_USER(400, "회원정보가 입력되지 않았습니다."),
    NOT_ENTERED_PASSWORD(400, "비밀번호가 입력되지 않았습니다."),
    NOT_ENTERED_MOBILE(400, "휴대폰번호가 입력되지 않았습니다."),
    NOT_ENTERED_EMAIL(400, "이메일주소가 입력되지 않았습니다."),
    NOT_EXIST_EMAIL(400, "존재하지 않는 이메일주소 입니다."),
    NOT_EXIST_MOBILE(400, "존재하지 않는 휴대폰번호 입니다."),
    NOT_EXIST_USER(400, "존재하지 않는 회원 입니다."),
    ENTERED_INVALID(400, "입력하신 정보가 유효하지 않습니다."),
    ENTERED_INVALID_EMAIL(400, "입력하신 이메일주소의 형식이 유효하지 않습니다."),
    EXPIRED_VERIFICATION_CODE(400, "만료된 인증번호 입니다."),
    INVALID_VERIFICATION_CODE(400, "인증번호가 유효하지 않습니다."),
    INVALID_ENTERED_PASSWORD(400, "입력한 비밀번호가 일치 하지 않습니다."),
    INVALID_ENTERED_NUMBER_PATTERN(400, "입력하신 정보가 숫자형식이 아닙니다."),
    LEAVED_USER(400, "탈퇴한 회원입니다."),
    NOT_CERTIFIED(400, "인증되지 않았습니다."),
    DORMANCY_USER(400, "휴면계정입니다."),
    //- 결제
    LIMIT_POINT(400, "최소단위 포인트를 사용하여야 합니다."),
    MORE_THAN_MY_POINTS(400, "사용자의 포인트보다 많습니다."),
    PAYMENT_NON_MEMBER_NE_PASSWORD(400, "비회원 결제 패스워드가 일치하지 않습니다."),
    COUPON_PRODUCT_LIMIT_AMOUNT(400, "쿠폰사용 최소 제품 구매 한도 미만"),
    NON_EXIST_ORDER_TEMP(400, "임시 주문제품정보가 존재하지 않습니다."),
    BAD_ORDER_TEMP(400, "잘못된 임시 주문 정보입니다."),
    PAYMENT_AMOUNT_DISCORD(400, "결제할 금액이 일치하지 않습니다."),
    //== 몰
    NOT_EXIST_MALL(400, "존재하지 않는 몰 입니다."),
    NOT_ENTERED_MALL_ID(400, "몰 ID 가 입력되지 않았습니다."),
    BAD_MALL(400, "잘못된 몰 정보입니다."),
    //== 쿠폰
    NOT_COUPON_REGISTRATION_PERIOD(400, "쿠폰 등록기간이 아닙니다."),
    ALREADY_REGISTERED_COUPON(400, "이미 등록된 쿠폰입니다."),
    LIMITED_NUMBER_EXCEEDED(400, "제한인원을 초과하였습니다."),
    INVALID_COUPON(400, "유효한 쿠폰이 아닙니다."),

    NOT_AN_AVAILABLE_PERIOD(400, "사용 가능기간이 아닙니다."),
    ALREADY_USED_COUPON(400, "이미 사용된 쿠폰입니다."),
    NOT_EXIST_COUPON_OWNER(400, "소유자가 존재하지 않습니다."),
    NOT_MATCH_COUPON_OWNER(400, "쿠폰 소유자와 일치하지 않습니다."),
    NOT_PRODUCT_COUPON(400, "상품 할인 쿠폰이 아닙니다."),
    NOT_ORDER_COUPON(400, "주문 할인 쿠폰이 아닙니다."),
    NOT_DELIVERY_COUPON(400, "배송비 할인 쿠폰이 아닙니다."),
    NOT_EXIST_COUPON_START_DATE(400, "쿠폰 시작일이 없습니다."),
    NOT_EXIST_COUPON_END_DATE(400, "쿠폰 종료일이 없습니다."),
    INCORRECT_DISCOUNT_METHOD_COUPON(400, "할인방법이 잘못된 쿠폰 입니다."),
    LOWER_THAN_MINIMUM_PURCHASE_AMOUNT_COUPON(400, "주문금액이 최소 구매금액보다 낮습니다."),

    // 404

    // 409
    ALREADY_EXIST_EMAIL(409, "이미 존재하는 이메일입니다."),
    ALREADY_EXIST_MOBILE(409, "이미 존재하는 전화번호입니다."),
    ALREADY_EXIST_USER(409, "이미 존재하는 회원입니다."),
    ALREADY_EXIST_BAIDU(409, "이미 존재하는 바이두 계정입니다."),
    ALREADY_EXIST_WECHAT(409, "이미 존재하는 위챗 계정입니다."),
    ALREADY_EXIST_WEIBO(409, "이미 존재하는 웨이보 계정입니다."),
    ALREADY_EXIST_FACEBOOK(409, "이미 존재하는 페이스북 계정입니다."),
    ALREADY_EXIST_INSTAGRAM(409, "이미 존재하는 인스타그램 계정입니다."),
    FAILED_GENERATE_VERIFICATION_CODE(409, "인증번호생성에 실패하였습니다. 재시도 해주시기 바랍니다."),
    ALREADY_EXIST_ACCOUNT(409, "이미 존재하는 이메일 계정입니다."),
    ALREADY_EXIST_PHONE(409, "이미 존재하는 휴대폰번호입니다."),
    PHONE_HAS_NOT_VERIFIED(409, "인증되지 않은 휴대폰번호입니다."),

    // 500
    PAYMENT_ERROR(500, "결제 에러"),
    NOT_EXIST_AMOUNT(500, "결제금액이 존재하지 않습니다."),
    NOT_EXIST_CURRENCY(500, "통화단위가 존재하지 않습니다."),
    NOT_EXIST_TAX_RATE(500, "세율이 존재하지 않습니다."),
    //- 결제
    I_AM_PORT_NOT_COMPLETE(500, "아임포트 검증 실패"),
    ;

    final private int value;
    final private String message;

    ResponseErrorCode(final int value, final String message) {
        this.value = value;
        this.message = message;
    }

}
