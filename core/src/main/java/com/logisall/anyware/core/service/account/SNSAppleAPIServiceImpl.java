package com.logisall.anyware.core.service.account;

import com.logisall.anyware.core.domain.user.SocialId;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.domain.user.sns.SNSStatus;
import com.logisall.anyware.core.model.sns.Apple;
import com.logisall.anyware.core.model.sns.apple.AppleUtils;
import com.logisall.anyware.core.model.sns.apple.Payload;
import com.logisall.anyware.core.model.sns.apple.TokenResponse;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
public class SNSAppleAPIServiceImpl implements com.logisall.anyware.core.service.account.SNSAppleAPIService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private SNSAppleAPIService snsAppleAPIService;

  @Override
  public com.logisall.anyware.core.model.sns.apple.TokenResponse getAccessToken(String code, String idToken, String redirectUri) {

    String client_secret = snsAppleAPIService.getAppleClientSecret(idToken);

    log.debug("================================");
    log.debug("id_token ‣ " + idToken);
    log.debug("payload ‣ " + snsAppleAPIService.getPayload(idToken));
    log.debug("client_secret ‣ " + client_secret);
    log.debug("================================");

    com.logisall.anyware.core.model.sns.apple.TokenResponse tokenResponse = snsAppleAPIService.requestCodeValidations(client_secret, code, null);
    log.debug("tokenResponse : {}", tokenResponse);
    return tokenResponse;
  }


  @Override
  public com.logisall.anyware.core.domain.user.sns.SNSStatus getStatus(Apple apple) {

    Optional<com.logisall.anyware.core.domain.user.User> userOpt = userRepository.findBySocialIdFacebookId(apple.getId());

    if (userOpt.isPresent()) {
      return com.logisall.anyware.core.domain.user.sns.SNSStatus.LINKED; // 이미 연동되어었음
    } else if (com.logisall.anyware.core.utils.StringUtils.isNotEmpty(apple.getEmail())) {

      // Apple에서 제공한 이메일 정보로 계정 조회
      userOpt = userRepository.findByEmail(apple.getEmail());

      if (userOpt.isPresent()) {
        /* Apple에서 제공한 이메일 정보로 조회한 계정이 존재한다면 */

        User user = userOpt.get();

        if (user.isWithd()) {
          /* 탈퇴한 계졍이라면 */
          return com.logisall.anyware.core.domain.user.sns.SNSStatus.LEAVED_ACCOUNT;
        }

        if (user.getSocialId() == null || StringUtils.isEmpty(user.getSocialId().getFacebookId())) {
          /* 계정에 Apple ID가 연동되어 있지 않으면 연동 시켜준다. */

          com.logisall.anyware.core.domain.user.SocialId socialId = user.getSocialId() == null ? new SocialId() : user.getSocialId();
          socialId.setAppleId(apple.getId());
          socialId.setAppleName(apple.getEmail());

          user.setSocialId(socialId);

          userRepository.save(user);
          return com.logisall.anyware.core.domain.user.sns.SNSStatus.CONNECT;
        } else {
          /* 계정에 다른 Apple ID가 연동되어 있다면 */
          return com.logisall.anyware.core.domain.user.sns.SNSStatus.NOT_MATCH_SNS;
        }
      } else {
        /* Apple에서 제공한 정보와 동일한 이메일과 Apple ID 가 존재하지 않는다. -> 회원가입 */
        return com.logisall.anyware.core.domain.user.sns.SNSStatus.NOT_EXISTED_ACCOUNT;
      }
    } else {
      /* Apple에서 이메일을 제공하지 않았다면 -> 회원가입 */
      return SNSStatus.NOT_PROVIDED_EMAIL;
    }
  }

  /**
   * iss : TEAM ID
   * The issuer registered claim identifies the principal that issued the client secret. Since the client secret belongs to your developer team, use your 10-character Team ID associated with your developer account.
   * 발급자 등록 클레임은 클라이언트 암호를 발급 한 주체를 식별합니다. 클라이언트 암호는 개발자 팀에 속하므로 개발자 계정과 연결된 10 자로 된 팀 ID를 사용하십시오.
   * <p>
   * iat : 발행 시간
   * The issued at registered claim indicates the time at which you generated the client secret, in terms of the number of seconds since Epoch, in UTC.
   * 발행 된 at registered 클레임은 UTC로 Epoch 이후 초 단위로 클라이언트 암호를 생성 한 시간을 나타냅니다.
   * <p>
   * exp : 만료 시간
   * The expiration time registered claim identifies the time on or after which the client secret will expire. The value must not be greater than 15777000 (6 months in seconds) from the Current Unix Time on the server.
   * 등록 된 만료 시간 클레임은 클라이언트 암호가 만료되는 시간을 식별합니다. 값은 서버의 현재 Unix 시간에서 15777000 (6 개월)보다 크지 않아야합니다.
   * <p>
   * aud : https://appleid.apple.com
   * The audience registered claim identifies the intended recipient of the client secret. Since the client secret is sent to the validation server, use https://appleid.apple.com.
   * 청중 등록 클레임은 클라이언트 암호의 의도 된 수신자를 식별합니다. 클라이언트 시크릿이 유효성 검사 서버로 전송되므로 https://appleid.apple.com을 사용하십시오.
   * <p>
   * sub : client_id
   * The subject registered claim identifies the principal that is the subject of the client secret. Since this client secret is meant for your application, use the same value as client_id. The value is case-sensitive.
   * 주체 등록 클레임은 클라이언트 시크릿의 주체 인 주체를 식별합니다. 이 클라이언트 시크릿은 애플리케이션 용이므로 client_id와 동일한 값을 사용하십시오. 값은 대소 문자를 구분합니다.
   *
   * @param apiKey
   * @return
   */
  @Autowired
  private AppleUtils appleUtils;

  /**
   * 유효한 id_token인 경우 client_secret 생성
   *
   * @param id_token
   * @return
   */
  @Override
  public String getAppleClientSecret(String id_token) {

    if (appleUtils.verifyIdentityToken(id_token)) {
      return appleUtils.createClientSecret();
    }

    return null;
  }

  /**
   * code 또는 refresh_token가 유효한지 Apple Server에 검증 요청
   *
   * @param client_secret
   * @param code
   * @param refresh_token
   * @return
   */
  @Override
  public com.logisall.anyware.core.model.sns.apple.TokenResponse requestCodeValidations(String client_secret, String code, String refresh_token) {

    com.logisall.anyware.core.model.sns.apple.TokenResponse tokenResponse = new TokenResponse();

    if (client_secret != null && code != null && refresh_token == null) {
      tokenResponse = appleUtils.validateAuthorizationGrantCode(client_secret, code);
    } else if (client_secret != null && code == null && refresh_token != null) {
      tokenResponse = appleUtils.validateAnExistingRefreshToken(client_secret, refresh_token);
    }

    return tokenResponse;
  }

  /**
   * Apple login page 호출을 위한 Meta 정보 가져오기
   *
   * @return
   */
  @Override
  public Map<String, String> getLoginMetaInfo() {
    return appleUtils.getMetaInfo();
  }

  /**
   * id_token에서 payload 데이터 가져오기
   *
   * @return
   */
  @Override
  public Payload getPayload(String id_token) {
    return appleUtils.decodeFromIdToken(id_token);
  }

}
