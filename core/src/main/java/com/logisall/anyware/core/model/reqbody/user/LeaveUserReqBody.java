package com.logisall.anyware.core.model.reqbody.user;

import com.logisall.anyware.core.model.BaseRequestBody;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Pattern;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LeaveUserReqBody extends BaseRequestBody {

    private static final long serialVersionUID = -3422607201168752231L;

    @Pattern(regexp = ValidUtils.PATTERN_PASSWORD_NEW)
    @Schema(description = "password", example = "abcd1234", minLength = 6, maxLength = 30)
    private String password;
//    private boolean leaveTerms; // 탈퇴약관 동의 유/무

    @Schema(description = "leave reason", example = "reason", minLength = 2, maxLength = 255)
    private String leaveReason; // 탈퇴사유
}
