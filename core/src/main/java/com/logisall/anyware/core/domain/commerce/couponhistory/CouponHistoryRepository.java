package com.logisall.anyware.core.domain.commerce.couponhistory;

import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistory;
import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistoryRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.time.LocalDate;
import java.util.List;

public interface CouponHistoryRepository extends
    JpaRepository<CouponHistory, Long>,
    QuerydslPredicateExecutor<CouponHistory>,
        CouponHistoryRepositoryCustom {

    boolean existsByRelativeBuyer_IdAndRelativeCoupon_Id(Long idBuyer, Long idCoupon);
    long countByRelativeCoupon_Id(Long idCoupon);
    List<CouponHistory> findAllByRelativeBuyer_RelativeUser_EmailAndUsedAndRelativeCoupon_StartDateLessThanEqualAndRelativeCoupon_EndDateGreaterThanEqual(String email,Boolean used ,LocalDate now, LocalDate now1);
}
