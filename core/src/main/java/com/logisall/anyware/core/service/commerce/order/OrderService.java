package com.logisall.anyware.core.service.commerce.order;

import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.resbody.commerce.order.OrderDetailResBody;
import org.springframework.data.domain.Page;

import java.util.Locale;


public interface OrderService {

    //R
    Page<com.logisall.anyware.core.domain.commerce.order.Order> page(Filter filter,
                                                                     com.logisall.anyware.core.domain.commerce.OrderStatus orderStatuses,
                                                                     String productName,
                                                                     String memberQuery);

    com.logisall.anyware.core.domain.commerce.order.Order get(Long id);
    com.logisall.anyware.core.domain.commerce.order.Order get(String oid);


    String generateOid();

    com.logisall.anyware.core.domain.commerce.order.Order success(com.logisall.anyware.core.domain.commerce.order.Order order);

    String oid(Long idBuyer);
    // U
    Order updateByOrderStatus(Long id, OrderStatus orderStatus);

    OrderDetailResBody getDetailOrder(Long id, Locale locale);
}
