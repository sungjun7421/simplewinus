package com.logisall.anyware.core.service.account;

import com.logisall.anyware.core.domain.user.sns.SNSStatus;
import com.logisall.anyware.core.model.sns.Facebook;

import java.util.Map;

public interface SNSFacebookAPIService {

  Map getAccessToken(String code, String redirectUri);

  Map getMe(String accessToken);

  SNSStatus getStatus(Facebook facebook);
}
