package com.logisall.anyware.core.model.map.naver;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class NaverMapMeta implements Serializable {

  private static final long serialVersionUID = 3991477615013065320L;

  private Integer totalCount;
  private Integer page;
  private Integer count;
}
