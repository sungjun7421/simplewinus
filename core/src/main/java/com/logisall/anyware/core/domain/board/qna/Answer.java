package com.logisall.anyware.core.domain.board.qna;

import com.logisall.anyware.core.config.serializer.JsonLocalDateTimeDeserializer;
import com.logisall.anyware.core.config.serializer.JsonLocalDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDateTime;

@Setter
@Getter
@ToString
@Embeddable
public class Answer implements java.io.Serializable {

  private static final long serialVersionUID = -1794195323338176770L;

  @Schema(description = "답변")
  @Column(name = "answer", columnDefinition = "TEXT")
  private String content; //

  @Schema(description = "답변시간")
  @JsonSerialize(using = JsonLocalDateTimeSerializer.class)
  @JsonDeserialize(using = JsonLocalDateTimeDeserializer.class)
  private LocalDateTime regTime;
}
