package com.logisall.anyware.core.domain.commerce.deposit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface DepositRepository extends
    JpaRepository<com.logisall.anyware.core.domain.commerce.deposit.Deposit, Long>,
    QuerydslPredicateExecutor<Deposit>,
    DepositRepositoryCustom {
}
