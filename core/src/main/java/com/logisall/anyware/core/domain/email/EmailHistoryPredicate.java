package com.logisall.anyware.core.domain.email;

import com.logisall.anyware.core.domain.user.User;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@NoArgsConstructor(staticName = "getInstance")
public class EmailHistoryPredicate {

  public static final QEmailHistory Q_EMAIL_HISTORY = QEmailHistory.emailHistory;

  private BooleanBuilder builder = new BooleanBuilder();

  public EmailHistoryPredicate search(String value) {

    if (!StringUtils.isEmpty(value)) {
      value = value.trim();

      builder.and(Q_EMAIL_HISTORY.sendAddress.containsIgnoreCase(value)
              .or(Q_EMAIL_HISTORY.destAddress.containsIgnoreCase(value))
              .or(Q_EMAIL_HISTORY.subject.containsIgnoreCase(value))
              .or(Q_EMAIL_HISTORY.msgBody.containsIgnoreCase(value)));
    }
    return this;
  }

  public EmailHistoryPredicate type(final EmailHistory.Type type) {

    if (type != null) {
      builder.and(Q_EMAIL_HISTORY.type.eq(type));
    }
    return this;
  }

  public EmailHistoryPredicate startDate(final LocalDateTime startDate) {

    if (startDate != null) {
      builder.and(Q_EMAIL_HISTORY.createdDate.goe(startDate));
    }
    return this;
  }

  public EmailHistoryPredicate endDate(final LocalDateTime endDate) {

    if (endDate != null) {
      builder.and(Q_EMAIL_HISTORY.createdDate.loe(endDate));
    }
    return this;
  }

  public EmailHistoryPredicate user(final User user) {

    if (user != null) {
      builder.and(Q_EMAIL_HISTORY.relativeUser.eq(user));
    }
    return this;
  }

  public Predicate values() {

    return builder.getValue() == null ? builder.and(Q_EMAIL_HISTORY.id.isNotNull()) : builder.getValue();
  }
}
