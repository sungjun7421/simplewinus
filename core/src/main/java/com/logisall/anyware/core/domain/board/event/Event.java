package com.logisall.anyware.core.domain.board.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logisall.anyware.core.domain.AbstractEntityInternational;
import com.logisall.anyware.core.domain.International.InterText;
import com.logisall.anyware.core.domain.RestEntityThumbBody;
import com.logisall.anyware.core.domain.board.comment.Comment;
import com.logisall.anyware.core.domain.embedd.SEO;
import com.logisall.anyware.core.model.resbody.board.EventResBody;
import com.logisall.anyware.core.model.resbody.board.EventThumbBody;
import com.logisall.anyware.core.utils.DateUtils;
import com.logisall.anyware.core.utils.HtmlUtils;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * 이벤트 게시판
 */
@Slf4j
@Entity(name = "Event")
@Getter
@Setter
@ToString(exclude = "comments")
public class Event extends AbstractEntityInternational<Long> implements RestEntityThumbBody<com.logisall.anyware.core.model.resbody.board.EventResBody, com.logisall.anyware.core.model.resbody.board.EventThumbBody> {

  private static final long serialVersionUID = -5505613305187983558L;
  public static String[] IGNORE_PROPERTIES = {
      "id",
      "pageView",
      "comments"
  };

  @PrePersist
  public void prePersist() {
    if (com.logisall.anyware.core.utils.StringUtils.isNotEmpty(this.content.getTextEnUs()))
      this.content.setTextEnUs(com.logisall.anyware.core.utils.HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextEnUs()));
    if (com.logisall.anyware.core.utils.StringUtils.isNotEmpty(this.content.getTextJaJp()))
      this.content.setTextJaJp(com.logisall.anyware.core.utils.HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextJaJp()));
    if (com.logisall.anyware.core.utils.StringUtils.isNotEmpty(this.content.getTextKoKr()))
      this.content.setTextKoKr(com.logisall.anyware.core.utils.HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextKoKr()));
    if (com.logisall.anyware.core.utils.StringUtils.isNotEmpty(this.content.getTextZhCn()))
      this.content.setTextZhCn(com.logisall.anyware.core.utils.HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextZhCn()));
    if (com.logisall.anyware.core.utils.StringUtils.isNotEmpty(this.content.getTextZhTw()))
      this.content.setTextZhTw(com.logisall.anyware.core.utils.HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextZhTw()));
  }

  @PreUpdate
  public void PreUpdate() {
    if (com.logisall.anyware.core.utils.StringUtils.isNotEmpty(this.content.getTextEnUs()))
      this.content.setTextEnUs(com.logisall.anyware.core.utils.HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextEnUs()));
    if (com.logisall.anyware.core.utils.StringUtils.isNotEmpty(this.content.getTextJaJp()))
      this.content.setTextJaJp(com.logisall.anyware.core.utils.HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextJaJp()));
    if (com.logisall.anyware.core.utils.StringUtils.isNotEmpty(this.content.getTextKoKr()))
      this.content.setTextKoKr(com.logisall.anyware.core.utils.HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextKoKr()));
    if (com.logisall.anyware.core.utils.StringUtils.isNotEmpty(this.content.getTextZhCn()))
      this.content.setTextZhCn(com.logisall.anyware.core.utils.HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextZhCn()));
    if (StringUtils.isNotEmpty(this.content.getTextZhTw()))
      this.content.setTextZhTw(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextZhTw()));
  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @Embedded
  @AttributeOverrides({        // 임베디드 타입에 정의한 매핑정보를 재정의
      @AttributeOverride(name = "textKoKr", column = @Column(name = "titleKoKr", length = 200)),
      @AttributeOverride(name = "textEnUs", column = @Column(name = "titleEnUs", length = 200)),
      @AttributeOverride(name = "textJaJp", column = @Column(name = "titleJaJp", length = 200)),
      @AttributeOverride(name = "textZhCn", column = @Column(name = "titleZhCn", length = 200)),
      @AttributeOverride(name = "textZhTw", column = @Column(name = "titleZhTw", length = 200))
  })
  private com.logisall.anyware.core.domain.International.InterText title; // 제목

  @Embedded
  @AttributeOverrides({
      @AttributeOverride(name = "textKoKr", column = @Column(name = "contentKoKr", columnDefinition = "LONGTEXT")),
      @AttributeOverride(name = "textEnUs", column = @Column(name = "contentEnUs", columnDefinition = "LONGTEXT")),
      @AttributeOverride(name = "textJaJp", column = @Column(name = "contentJaJp", columnDefinition = "LONGTEXT")),
      @AttributeOverride(name = "textZhCn", column = @Column(name = "contentZhCn", columnDefinition = "LONGTEXT")),
      @AttributeOverride(name = "textZhTw", column = @Column(name = "contentZhTw", columnDefinition = "LONGTEXT"))
  })
  private InterText content; // 내용

  @Column
  private String thumbnail; // 썸네일

  @Column
  private String image; // 메인 이미지

  /* 전시기간 */
  @DateTimeFormat(pattern = com.logisall.anyware.core.utils.DateUtils.FORMAT_DATE_UNIT_BAR)
  private LocalDate startDate;

  @DateTimeFormat(pattern = com.logisall.anyware.core.utils.DateUtils.FORMAT_DATE_UNIT_BAR)
  private LocalDate endDate;

  public boolean isWithinRange() {
    return DateUtils.isWithinRange(LocalDate.now(), this.startDate, this.endDate);
  }
  /* END : 전시기간 */

  @Embedded
  @AttributeOverrides({
      @AttributeOverride(name = "description", column = @Column(name = "seoDescription")),
      @AttributeOverride(name = "keywords", column = @Column(name = "seoKeywords")),
      @AttributeOverride(name = "ogImage", column = @Column(name = "seoOgImage"))
  })
  private SEO seo; // 검색 엔진 최적화

  @Column(columnDefinition = "BIGINT(20) default 0")
  private long pageView; // 조회수

  @Column(columnDefinition = "BIT(1) default 0")
  private boolean top; // 상단고정

  @JsonIgnore
  @OneToMany(mappedBy = "relativeEvent", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Comment> comments = new ArrayList<>();

  @Column(columnDefinition = "BIT(1) default 1")
  private boolean active; // 활성/비활성

  @Transient
  private boolean checked;

  @Override
  public void setLocale(Locale locale) {
    super.setLocale(locale);
    if (this.title != null)
      this.title.setLocale(this.locale);
    if (this.content != null)
      this.content.setLocale(this.locale);
  }

  @Override
  public void delete() {

  }

  @Override
  public void lazy() {

  }

  @Override
  public com.logisall.anyware.core.model.resbody.board.EventResBody toBody(Locale locale) {
    this.setLocale(locale);
    return EventResBody.builder()
        .id(this.id)
        .title(this.title != null ? this.title.getValue() : null)
        .content(this.content != null ? this.content.getValue() : null)
        .startDate(this.startDate)
        .endDate(this.endDate)
        .seo(this.seo)
        .end(!this.isWithinRange())
        .pageView(this.pageView)
        .build();
  }

  @Override
  public com.logisall.anyware.core.model.resbody.board.EventThumbBody toThumbBody(Locale locale) {
    this.setLocale(locale);
    return EventThumbBody.builder()
        .id(this.id)
        .title(this.title != null ? this.title.getValue() : null)
        .summary("요약글 (작업중)") // TODO 요약글 처리
        .thumbnail(this.thumbnail)
        .pageView(this.pageView)
        .startDate(this.startDate)
        .endDate(this.endDate)
        .end(!this.isWithinRange())
        .build();
  }
}
