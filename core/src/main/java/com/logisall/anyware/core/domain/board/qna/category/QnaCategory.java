package com.logisall.anyware.core.domain.board.qna.category;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logisall.anyware.core.domain.AbstractEntityCategory;
import com.logisall.anyware.core.domain.RestEntityBody;
import com.logisall.anyware.core.domain.board.qna.Qna;
import com.logisall.anyware.core.model.resbody.board.CategoryResBody;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

@Entity(name = "QnaCategory")
@Getter
@Setter
@ToString(exclude = {"qnas"})
@NoArgsConstructor
public class QnaCategory extends AbstractEntityCategory<Long>
        implements RestEntityBody<com.logisall.anyware.core.model.resbody.board.CategoryResBody> {

  private static final long serialVersionUID = -7827384715707862993L;

  public static String[] IGNORE_PROPERTIES = {
      "id",
      "qnas"
  };

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @JsonIgnore
  @OrderBy("createdDate ASC")
  @ManyToMany(mappedBy = "categories")
  private List<Qna> qnas = new ArrayList<>();

  @Override
  public void delete() {
    if (this.qnas != null) {
      this.qnas.forEach(qna -> {
        qna.getCategories().removeIf(category -> Objects.equals(category.getId(), this.id));
      });
    }
  }

  @Override
  public void lazy() {
    if (this.qnas != null)
      this.qnas.size();
  }

  @Override
  public com.logisall.anyware.core.model.resbody.board.CategoryResBody toBody(Locale locale) {
    this.setLocale(locale);

    return CategoryResBody.builder()
        .id(this.id)
        .name(this.name.getValue())
        .build();
  }
}
