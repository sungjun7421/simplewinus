package com.logisall.anyware.core.domain.commerce.category;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class CategoryRepositoryImpl implements CategoryRepositoryCustom {

    @Autowired
    private JPAQueryFactory jpaQueryFactory;

    @Override
    public boolean existsByNameAndKoKr(String name) {
        QCategory qCategory = QCategory.category;

        return jpaQueryFactory.from(qCategory)
                .where(qCategory.name.textKoKr.eq(name)).fetchCount() > 0;
    }

    @Override
    public boolean existsByNameAndEnUs(String name) {
        QCategory qCategory = QCategory.category;

        return jpaQueryFactory.from(qCategory)
                .where(qCategory.name.textEnUs.eq(name)).fetchCount() > 0;
    }

    @Override
    public boolean existsByNameAndZhCn(String name) {
        QCategory qCategory = QCategory.category;

        return jpaQueryFactory.from(qCategory)
                .where(qCategory.name.textZhCn.eq(name)).fetchCount() > 0;
    }

    @Override
    public boolean existsByNameAndZhTw(String name) {
        QCategory qCategory = QCategory.category;

        return jpaQueryFactory.from(qCategory)
                .where(qCategory.name.textZhTw.eq(name)).fetchCount() > 0;
    }

    @Override
    public boolean existsByNameAndJaJp(String name) {
        QCategory qCategory = QCategory.category;

        return jpaQueryFactory.from(qCategory)
                .where(qCategory.name.textJaJp.eq(name)).fetchCount() > 0;
    }
}
