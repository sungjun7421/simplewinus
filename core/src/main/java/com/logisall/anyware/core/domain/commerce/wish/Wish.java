package com.logisall.anyware.core.domain.commerce.wish;

import com.logisall.anyware.core.domain.AbstractEntityId;
import com.logisall.anyware.core.domain.AbstractEntityMtoM;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.product.Product;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Entity(name = "Wish")
@Getter
@Setter
@ToString(exclude = {"relativeBuyer", "relativeProduct"})
@NoArgsConstructor
public class Wish extends AbstractEntityMtoM {

    private static final long serialVersionUID = 1277799539621469481L;

    // Result Message
    public static final String RESULT_ADDED = "ADDED"; // 추가됨
    public static final String RESULT_DELETED = "DELETED"; // 삭제됨

    public Wish(Buyer buyer, Product product) {
        this.id.idBuyer = buyer.getId();
        this.id.idProduct = product.getId();
        this.relativeBuyer = buyer;
        this.relativeProduct = product;
    }

    @EmbeddedId
    private Id id = new Id();

    @JsonIgnore
    @ManyToOne
    @MapsId("idBuyer")
    @JoinColumn(name = "idBuyer", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Buyer_For_BTOP")) // Column name
    private Buyer relativeBuyer;

    @ManyToOne
    @JsonIgnore
    @MapsId("idProduct")
    @JoinColumn(name = "idProduct", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Product_For_BTOP")) // Column name
    private Product relativeProduct;


    public void lazy(){
        if(relativeBuyer!=null){
            this.relativeBuyer.lazy();
        }
        if(relativeProduct!=null){
            this.relativeProduct.lazy();
        }
    }

    @Embeddable
    @Data
    @NoArgsConstructor
    @EqualsAndHashCode(callSuper = false, of = {"idBuyer", "idProduct"})
    public static class Id extends AbstractEntityId {

        private static final long serialVersionUID = 5947803610780117354L;

        public Id(Long idBuyer, Long idProduct) {
            this.idBuyer = idBuyer;
            this.idProduct = idProduct;
        }

        @Column
        private Long idBuyer;

        @Column
        private Long idProduct;
    }
}
