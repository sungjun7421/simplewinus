package com.logisall.anyware.core.domain.user;

import com.logisall.anyware.core.domain.user.Authority;

import java.util.List;

public interface AuthorityRepositoryCustom {

  List<Authority> listByRoles(Authority.Role... roles);
}
