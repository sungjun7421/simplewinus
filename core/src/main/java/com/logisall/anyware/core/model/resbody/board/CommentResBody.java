package com.logisall.anyware.core.model.resbody.board;

import com.logisall.anyware.core.config.serializer.JsonLocalDateTimeSerializer;
import com.logisall.anyware.core.model.BaseResponseBody;
import com.logisall.anyware.core.utils.TimeAgoUtils;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.server.core.Relation;

import javax.persistence.Transient;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Locale;

@Slf4j
@Schema(description = "댓글")
@Relation(value = "comment", collectionRelation = "comments")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CommentResBody extends BaseResponseBody {
    private static final long serialVersionUID = 4928024495184674123L;

    public CommentResBody(Locale locale) {
        this.locale = locale;
    }

    @Transient
    private Locale locale;

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "댓글 내용")
    private String content;

    @Schema(description = "삭제된 글")
    private boolean delete;

    @Schema(description = "작성자")
    private String writer;

    @Schema(description = "프로필 이미지")
    private String profileImage;

    @Schema(description = "등록시간")
    @JsonSerialize(using = JsonLocalDateTimeSerializer.class)
    private LocalDateTime regTime;

    public String getAgoString() {

        if (regTime != null) {
            long now = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
            long seconds = regTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
            return TimeAgoUtils.toDuration(now - seconds, locale);
        }
        return null;
    }
}
