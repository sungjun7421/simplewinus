package com.logisall.anyware.core.model.map.juso;


import lombok.*;
import org.springframework.util.StringUtils;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Juso implements Serializable {

  private static final long serialVersionUID = -7525389243292320067L;

  private String roadAddrPart1; // 도로명주소 (참고항목 제외)

  private String roadAddrPart2; // 도로명주소 참고항목

  // 도로명 주소
  public String roadAddr() {

    String address1 = StringUtils.isEmpty(this.getRoadAddrPart1()) ? "" : this.getRoadAddrPart1();
    String address2 = StringUtils.isEmpty(this.getRoadAddrPart2()) ? "" : this.getRoadAddrPart2();

    StringBuilder sb = new StringBuilder();
    sb.append(address1);
    sb.append(address2);
    return sb.toString();
  }

  private String jibunAddr; // 지번 정보

  private String engAddr; // 도로명주소(영문)

  private String zipNo; // 우편번호

  private String admCd; // 행정구역코드

//    @Column
//    private String rnMgtSn; // 도로명코드

//    @Column
//    private String bdMgtSn; // 건물관리번호

  private String detBdNmList; // 상세건물명

  private String bdNm; // 건물명

//    @Column
//    private String bdKdcd; // 공동주택여부 (1:공동주택, 0: 비공동주택)

  private String siNm; // 시도명

  private String sggNm; // 시군구명

  private String emdNm; // 읍면동명

  private String liNm; // 법정리명

  private String rn; // 도로명

//    @Column
//    private String udrtYn; // 지하여부 (0:지상, 1:지하)

  private String buldMnnm; // 건물본번

  private String buldSlno; // 건물부번 (부번이 없는 경우 0)

//    @Column
//    private String mtYn; // 산여부 (0:대지, 1:산)

//    @Column
//    private String lnbrMnnm; // 지번본번(번지)

//    @Column
//    private String lnbrSlno; // 지번부번(호) (부번이 없는 경우 0)

//    @Column
//    private String emdNo; // 읍면동일련번호
}
