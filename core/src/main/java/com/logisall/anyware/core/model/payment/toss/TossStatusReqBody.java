package com.logisall.anyware.core.model.payment.toss;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
@Builder
public class TossStatusReqBody implements Serializable {

  private static final long serialVersionUID = -4568895098306978157L;

  private String apiKey;
  private String payToken;
}
