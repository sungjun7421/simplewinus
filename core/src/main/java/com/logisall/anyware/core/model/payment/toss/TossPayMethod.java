package com.logisall.anyware.core.model.payment.toss;

import lombok.Getter;

@Getter
public enum TossPayMethod {

  TOSS_MONEY("토스머니"),
  CARD("카드");

  @Getter
  private final String value;

  TossPayMethod(final String value) {
    this.value = value;
  }
}
