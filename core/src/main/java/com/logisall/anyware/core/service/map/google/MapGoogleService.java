package com.logisall.anyware.core.service.map.google;

import com.logisall.anyware.core.domain.embedd.GPS;
import com.logisall.anyware.core.model.map.google.autocomplete.AutoCompleteByPlace;
import com.logisall.anyware.core.model.map.google.placedetail.PlaceDetail;
import com.logisall.anyware.core.model.map.google.textsearch.TextSearch;

import java.io.IOException;
import java.util.concurrent.Future;

public interface MapGoogleService {

  Future<GPS> getLocationInfo(String address) throws InterruptedException, IOException; // 위치정보
  Future<AutoCompleteByPlace> getAutoComplete(String query, String language) throws InterruptedException; // 자동완성
  Future<TextSearch> getPlaceSearch(String query, String language) throws InterruptedException; // 장소검색
  Future<PlaceDetail> getPlaceDetail(String place_id, String language) throws InterruptedException; // 장소상세
}
