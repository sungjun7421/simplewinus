package com.logisall.anyware.core.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

@Slf4j
public class TimeAgoUtils {

    public static final List<Long> times = Arrays.asList(
            TimeUnit.DAYS.toMillis(365),
            TimeUnit.DAYS.toMillis(30),
            TimeUnit.DAYS.toMillis(1),
            TimeUnit.HOURS.toMillis(1),
            TimeUnit.MINUTES.toMillis(1),
            TimeUnit.SECONDS.toMillis(1));

    public static final List<String> timesStringEn = Arrays.asList("year", "month", "day", "hour", "minute", "second");
    public static final List<String> timesStringKo = Arrays.asList("년", "개월", "일", "시", "분", "초");
    public static final List<String> timesStringJa = Arrays.asList("年", "ヶ月", "日", "時", "分", "秒");
    public static final List<String> timesStringZh = Arrays.asList("年", "月", "日", "小时", "分钟", "秒");
    public static final List<String> timesStringTw = Arrays.asList("年", "月", "日", "小時", "分鐘", "秒");

    public static final String agoStringEn = "ago";
    public static final String agoStringKo = "전";
    public static final String agoStringJa = "前";
    public static final String agoStringZh = "前";
    public static final String agoStringTw = "前";

    public static final String secondsStringEn = "seconds";
    public static final String secondsStringKo = "초";
    public static final String secondsStringJa = "秒";
    public static final String secondsStringZh = "秒";
    public static final String secondsStringTw = "秒";

    public static String toDuration(long duration, Locale locale) {

        StringBuffer res = new StringBuffer();
        for (int i = 0; i < com.logisall.anyware.core.utils.TimeAgoUtils.times.size(); i++) {

//            log.debug("--------------");
//            log.debug("i : {}", i);
            Long current = com.logisall.anyware.core.utils.TimeAgoUtils.times.get(i);
            long temp = duration / current;
//            log.debug("duration / current : {} / {}", duration, current);
//            log.debug("temp : {}", temp);

            if (temp > 0) {
                log.debug("# : {}", com.logisall.anyware.core.utils.TimeAgoUtils.timeString(locale, i));
                //                        .append(" ")
                res.append(temp)
                        .append(locale.equals(Locale.ENGLISH) || locale.equals(Locale.US) || locale.equals(Locale.UK) ? " " : "")
                        .append(com.logisall.anyware.core.utils.TimeAgoUtils.timeString(locale, i))
                        .append((temp != 1 && locale.equals(Locale.ENGLISH) || locale.equals(Locale.US) || locale.equals(Locale.UK)) ? "s" : "")
                        .append(" ")
                        .append(com.logisall.anyware.core.utils.TimeAgoUtils.ago(locale))
                ;
                break;
            }
        }
        return res.toString();
    }

    public static String timeString(Locale locale, int i) {
//        log.debug("timeString({},{})", locale, i);
        if (locale.equals(Locale.KOREA) || locale.equals(Locale.KOREAN)) {
            return com.logisall.anyware.core.utils.TimeAgoUtils.timesStringKo.get(i);
        } else if (locale.equals(Locale.JAPAN) || locale.equals(Locale.JAPANESE)) {
            return com.logisall.anyware.core.utils.TimeAgoUtils.timesStringJa.get(i);
        } else if (locale.equals(Locale.TAIWAN) || locale.equals(Locale.TRADITIONAL_CHINESE)) {
            return com.logisall.anyware.core.utils.TimeAgoUtils.timesStringTw.get(i);
        } else if (locale.equals(Locale.CHINA) || locale.equals(Locale.CHINESE) || locale.equals(Locale.SIMPLIFIED_CHINESE)) {
            return com.logisall.anyware.core.utils.TimeAgoUtils.timesStringZh.get(i);
        } else if (locale.equals(Locale.ENGLISH) || locale.equals(Locale.US) || locale.equals(Locale.UK)) {
            return com.logisall.anyware.core.utils.TimeAgoUtils.timesStringEn.get(i);
        }
        return com.logisall.anyware.core.utils.TimeAgoUtils.timesStringKo.get(i);
    }

    public static String ago(Locale locale) {
//        log.debug("ago({})", locale);
        if (locale.equals(Locale.KOREA) || locale.equals(Locale.KOREAN)) {
            return com.logisall.anyware.core.utils.TimeAgoUtils.agoStringKo;
        } else if (locale.equals(Locale.JAPAN) || locale.equals(Locale.JAPANESE)) {
            return com.logisall.anyware.core.utils.TimeAgoUtils.agoStringJa;
        } else if (locale.equals(Locale.TAIWAN) || locale.equals(Locale.TRADITIONAL_CHINESE)) {
            return com.logisall.anyware.core.utils.TimeAgoUtils.agoStringTw;
        } else if (locale.equals(Locale.CHINA) || locale.equals(Locale.CHINESE) || locale.equals(Locale.SIMPLIFIED_CHINESE)) {
            return com.logisall.anyware.core.utils.TimeAgoUtils.agoStringZh;
        } else if (locale.equals(Locale.ENGLISH) || locale.equals(Locale.US) || locale.equals(Locale.UK)) {
            return com.logisall.anyware.core.utils.TimeAgoUtils.agoStringEn;
        }
        return com.logisall.anyware.core.utils.TimeAgoUtils.agoStringKo;
    }

    public static String seconds(Locale locale) {
//        log.debug("seconds({})", locale);
        if (locale.equals(Locale.KOREA) || locale.equals(Locale.KOREAN)) {
            return com.logisall.anyware.core.utils.TimeAgoUtils.secondsStringKo;
        } else if (locale.equals(Locale.JAPAN) || locale.equals(Locale.JAPANESE)) {
            return com.logisall.anyware.core.utils.TimeAgoUtils.secondsStringJa;
        } else if (locale.equals(Locale.TAIWAN) || locale.equals(Locale.TRADITIONAL_CHINESE)) {
            return com.logisall.anyware.core.utils.TimeAgoUtils.secondsStringTw;
        } else if (locale.equals(Locale.CHINA) || locale.equals(Locale.CHINESE) || locale.equals(Locale.SIMPLIFIED_CHINESE)) {
            return com.logisall.anyware.core.utils.TimeAgoUtils.secondsStringZh;
        } else if (locale.equals(Locale.ENGLISH) || locale.equals(Locale.US) || locale.equals(Locale.UK)) {
            return com.logisall.anyware.core.utils.TimeAgoUtils.secondsStringEn;
        }
        return com.logisall.anyware.core.utils.TimeAgoUtils.secondsStringKo;
    }

    public static String agoDefault(Locale locale) {
        log.debug("agoDefault({})", locale);
        if (locale.equals(Locale.KOREA) || locale.equals(Locale.KOREAN)) {
            return "전";
//            return "초 전";
        } else if (locale.equals(Locale.JAPAN) || locale.equals(Locale.JAPANESE)) {
            return "前";
//            return "秒前";
        } else if (locale.equals(Locale.TAIWAN) || locale.equals(Locale.TRADITIONAL_CHINESE)) {
            return "前";
//            return "秒前";
        } else if (locale.equals(Locale.CHINA) || locale.equals(Locale.CHINESE) || locale.equals(Locale.SIMPLIFIED_CHINESE)) {
            return "前";
//            return "秒前";
        } else if (locale.equals(Locale.ENGLISH) || locale.equals(Locale.US) || locale.equals(Locale.UK)) {
            return "ago";
//            return "seconds ago";
        }
        return "전";
    }
}
