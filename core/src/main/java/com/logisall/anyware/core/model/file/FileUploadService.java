package com.logisall.anyware.core.model.file;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileUploadService {

    List<com.logisall.anyware.core.model.file.FileMeta> uploadImages(MultipartFile[] files);
    com.logisall.anyware.core.model.file.FileMeta uploadImage(MultipartFile file);
    com.logisall.anyware.core.model.file.FileMeta uploadFile(MultipartFile file);
    FileMeta uploadVideo(MultipartFile file);
    boolean delete(String url);
}
