package com.logisall.anyware.core.domain.commerce.point;

import com.logisall.anyware.core.domain.AbstractEntity;
import lombok.*;

import javax.persistence.*;

@Entity(name = "PointSetting")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PointSetting extends AbstractEntity<Long> {

  private static final long serialVersionUID = -1508390869347289469L;

  public static final String[] IGNORE_PROPERTIES = {"id"};

  @PrePersist
  public void prePersist() {
    if (this.unit > 100) {
      this.unit = 100;
    } else if (this.unit < 1) {
      this.unit = 1;
    }
    if (this.expiration > 10) {
      this.expiration = 10;
    } else if (this.expiration < 1) {
      this.expiration = 1;
    }
    if (this.maxSavingPoint < 1) {
      this.maxSavingPoint = 1000000;
    }
    if (this.minPoint < 1) {
      this.minPoint = 1000;
    }
  }

  @PreUpdate
  public void preUpdate() {
    if (this.unit > 100) {
      this.unit = 100;
    } else if (this.unit < 1) {
      this.unit = 1;
    }
    if (this.expiration > 10) {
      this.expiration = 10;
    } else if (this.expiration < 1) {
      this.expiration = 1;
    }
    if (this.maxSavingPoint < 1) {
      this.maxSavingPoint = 1000000;
    }
    if (this.minPoint < 1) {
      this.minPoint = 1000;
    }
  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @Column(columnDefinition = "BIT(1) default 0")
  private boolean enabled; // 활성모드

  @Column(columnDefinition = "INT(11) default 0")
  private int minPoint; // 최소 결제포인트

  @Column(columnDefinition = "INT(11) default 1")
  private int unit; // 결제 포인트 단위 (ex: 1p, 10p, 100p)

  @Column(columnDefinition = "INT(11) default 0")
  private int maxSavingPoint; // 최대 적립 가능한 포인트

  @Column(columnDefinition = "INT(11) default 1")
  private int expiration; // 적립포인트 유효기간 (연단위, ex: 1년, 2년, 3년)


  @Override
  public void delete() {

  }

  @Override
  public void lazy() {

  }

  //== Utils

  public static PointSetting initialData(boolean enabled) {
    return PointSetting.builder()
      .enabled(enabled)
      .minPoint(1000)
      .unit(1)
      .maxSavingPoint(1000000)
      .expiration(1)
      .build();
  }
}
