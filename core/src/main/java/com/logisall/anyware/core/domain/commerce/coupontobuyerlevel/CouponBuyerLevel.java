package com.logisall.anyware.core.domain.commerce.coupontobuyerlevel;

import com.logisall.anyware.core.domain.AbstractEntityId;
import com.logisall.anyware.core.domain.AbstractEntityMtoM;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel;
import com.logisall.anyware.core.domain.commerce.coupon.Coupon;
import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString(exclude = {"relativeBuyerLevel", "relativeCoupon"})
@NoArgsConstructor
public class CouponBuyerLevel extends AbstractEntityMtoM implements Cloneable {

    private static final long serialVersionUID = -6505800054580660603L;

    public CouponBuyerLevel(com.logisall.anyware.core.domain.commerce.coupon.Coupon coupon, com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel buyerLevel) {
        this.id.idCoupon = coupon.getId();
        this.id.idBuyerLevel = buyerLevel.getId();
        this.relativeCoupon = coupon;
        this.relativeBuyerLevel = buyerLevel;
    }

    @EmbeddedId
    private Id id = new Id();

    @ManyToOne
    @MapsId("idCoupon")
    @JoinColumn(name = "idCoupon", referencedColumnName = "id") // Column name
    private Coupon relativeCoupon;

    @ManyToOne
    @MapsId("idBuyerLevel")
    @JoinColumn(name = "idBuyerLevel", referencedColumnName = "id") // Column name
    private BuyerLevel relativeBuyerLevel;

    @Embeddable
    @Data
    @NoArgsConstructor
    @EqualsAndHashCode(callSuper = false, of = {"idCoupon", "idBuyerLevel"})
    public static class Id extends AbstractEntityId {

        private static final long serialVersionUID = 5947803610780117354L;

        public Id(Long idCoupon, Long idBuyerLevel) {
            this.idCoupon = idCoupon;
            this.idBuyerLevel = idBuyerLevel;
        }

        @Column
        private Long idCoupon;

        @Column
        private Long idBuyerLevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CouponBuyerLevel that = (CouponBuyerLevel) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(relativeCoupon.getId(), that.relativeCoupon.getId()) &&
                Objects.equals(relativeBuyerLevel.getId(), that.relativeBuyerLevel.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, relativeCoupon, relativeBuyerLevel);
    }
}
