package com.logisall.anyware.core.domain.commerce.point;

import com.logisall.anyware.core.domain.commerce.buyer.QBuyer;
import com.logisall.anyware.core.domain.commerce.point.PointRepositoryCustom;
import com.logisall.anyware.core.model.querydsl.commerce.QDPoint;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
public class PointRepositoryImpl implements PointRepositoryCustom {

  @Autowired
  private JPAQueryFactory jpaQueryFactory;

  @Override
  public List<QDPoint> listByAvailable(Long idBuyer) {

    if (idBuyer == null || idBuyer == 0L) {
      return null;
    }

    QPoint qPoint = QPoint.point1;
    QBuyer qBuyer = QBuyer.buyer;

    NumberExpression<Integer> expression = qPoint.point.add(qPoint.used).intValue();

    return jpaQueryFactory
      .select(Projections.constructor(
        QDPoint.class,
        qPoint.id,
        qPoint.point.intValue(),
        qPoint.used.intValue(),
        expression,
        qBuyer.id,
        qBuyer.relativeUser.email
      ))
      .from(qPoint)
      .innerJoin(qPoint.relativeBuyer, qBuyer)
      .where(
        ExpressionUtils.allOf(
          qBuyer.id.eq(idBuyer),
          qPoint.expirationDate.gt(LocalDate.now()),
          ExpressionUtils.ne(expression, Expressions.ZERO)
        )
      )
      .orderBy(qPoint.expirationDate.asc())
      .fetch();
  }

  @Override
  public int total(Long idBuyer) {

    if (idBuyer == null || idBuyer == 0L) {
      return 0;
    }

    QPoint qPoint = QPoint.point1;
    QBuyer qBuyer = QBuyer.buyer;

    Integer total = jpaQueryFactory
      .select(qPoint.point.sum().intValue())
      .from(qPoint)
      .innerJoin(qPoint.relativeBuyer, qBuyer)
      .where(qBuyer.id.eq(idBuyer))
      .fetchFirst();

    log.debug("total ::: {}", total);

    return total == null ? 0 : total;
  }

  @Override
  public int totalFollowDate(Long idBuyer, LocalDateTime date) {

    if (idBuyer == null || idBuyer == 0L) {
      return 0;
    }

    QPoint qPoint = QPoint.point1;
    QBuyer qBuyer = QBuyer.buyer;

    Integer total = jpaQueryFactory
            .select(qPoint.point.sum().intValue())
            .from(qPoint)
            .innerJoin(qPoint.relativeBuyer, qBuyer)
            .where(qBuyer.id.eq(idBuyer))
            .where(qPoint.createdDate.loe(date))
            .fetchFirst();

    log.debug("total ::: {}", total);

    return total == null ? 0 : total;
  }

  @Override
  public int expireScheduledTotal(Long idBuyer) {

    if (idBuyer == null || idBuyer == 0L) {
      return 0;
    }

    QPoint qPoint = QPoint.point1;
    QBuyer qBuyer = QBuyer.buyer;

    NumberExpression<?> expression = qPoint.point.add(qPoint.used);

    final LocalDate now = LocalDate.now();
    final LocalDate start = now.withDayOfMonth(1);
    final LocalDate end = now.withDayOfMonth(now.lengthOfMonth());

    Integer expireScheduledTotal = jpaQueryFactory
      .select(
        expression.sum().intValue()
      )
      .from(qPoint)
      .innerJoin(qPoint.relativeBuyer, qBuyer)
      .where(
        Expressions.allOf(
          qBuyer.id.eq(idBuyer),
          qPoint.expirationDate.ne(now),
          qPoint.expirationDate.between(start, end)
        )
      )
      .where(ExpressionUtils.ne(expression.intValue(), Expressions.ZERO))
      .fetchFirst();

    log.debug("expireScheduledTotal ::: {}", expireScheduledTotal);

    return expireScheduledTotal == null ? 0 : expireScheduledTotal;
  }

  @Override
  public List<QDPoint> todayExpire() {

    QPoint qPoint = QPoint.point1;
    QBuyer qBuyer = QBuyer.buyer;

    NumberExpression<Integer> expression = qPoint.point.add(qPoint.used).intValue();

    return jpaQueryFactory
      .select(Projections.constructor(
        QDPoint.class,
        qPoint.id,
        qPoint.point.intValue(),
        qPoint.used.intValue(),
        expression,
        qBuyer.id,
        qBuyer.relativeUser.email
      ))
      .from(qPoint)

      .innerJoin(qPoint.relativeBuyer, qBuyer)
      .where(
        ExpressionUtils.allOf(
          qPoint.expirationDate.loe(LocalDate.now()),
          ExpressionUtils.ne(expression, Expressions.ZERO)
        )
      )
      .fetch();
  }
}
