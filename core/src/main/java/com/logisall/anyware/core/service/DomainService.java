package com.logisall.anyware.core.service;

import com.logisall.anyware.core.model.Filter;
import org.springframework.data.domain.Page;

import java.util.Locale;

public interface DomainService<T, R> {

  // C REATE
  T create(T entity);

  // U PDATE
  T update(T entity);

  // D ELETE
  void delete(R id);

  // R EAD
  T get(Locale locale, R id);
  Page<T> page(Locale locale, Filter filter);

}

