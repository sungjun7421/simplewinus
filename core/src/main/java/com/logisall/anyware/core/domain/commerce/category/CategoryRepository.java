package com.logisall.anyware.core.domain.commerce.category;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface CategoryRepository extends
        JpaRepository<com.logisall.anyware.core.domain.commerce.category.Category, Long>,
        QuerydslPredicateExecutor<com.logisall.anyware.core.domain.commerce.category.Category>,
    CategoryRepositoryCustom {

    @Query("select max(c.orderAscending) from ProductCategory c")
    Long highestOrder();

    @Query("from ProductCategory c where c.orderAscending < ?1 order by c.orderAscending desc")
    List<com.logisall.anyware.core.domain.commerce.category.Category> previous(long orderIndex, Pageable pageable);

    @Query("from ProductCategory c where c.orderAscending > ?1 order by c.orderAscending asc")
    List<Category> next(long orderAscending, Pageable pageable);
}
