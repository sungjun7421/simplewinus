package com.logisall.anyware.core.model.map.naver;

import com.logisall.anyware.core.model.map.naver.NaverMapAddressElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@ToString
public class NaverMapAddress implements Serializable {

  private static final long serialVersionUID = -1803112776711560686L;

  private String roadAddress;
  private String jibunAddress;
  private String englishAddress;
  private List<NaverMapAddressElement> addressElements;

  private Double x;
  private Double y;
  private Integer distance;
}
