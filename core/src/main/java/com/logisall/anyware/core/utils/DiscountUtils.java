package com.logisall.anyware.core.utils;

import com.google.common.collect.ImmutableMap;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DiscountUtils {

  public static Double getDiscountRateOld(String expression) {
    if (com.logisall.anyware.core.utils.StringUtils.isEmpty(expression)) {
      return 0.0;
    }
    expression = expression.replace("%", ""); // 표현식에 '%' 를 넣는 경우가 있어서 강제로 제거해야 함
    expression = expression.replace("\"", "");
    Expression e = new ExpressionBuilder(expression)
        .variables("price")
        .build()
        .setVariables(ImmutableMap.of("price", 100D));
    double result = e.evaluate();
    if (result == 0.0) {
      // 표현식에 0 을 넣는 경우가 있어서 강제로 0.0으로 리턴해야함.
      return 0.0;
    }
    return 100 - BigDecimal.valueOf(result).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
  }

  public static Double getDiscountRate(String expression, BigDecimal price) {
    if (StringUtils.isEmpty(expression)) {
      return 0.0;
    }
    if (price == null || price.intValue() == 0) {
      price = BigDecimal.valueOf(100);
    }
    expression = expression.replace("%", ""); // 표현식에 '%' 를 넣는 경우가 있어서 강제로 제거해야 함
    expression = expression.replace("\"", "");
    Expression e = new ExpressionBuilder(expression)
        .variables("price")
        .build()
        .setVariables(ImmutableMap.of("price", price.doubleValue()));
    double result = e.evaluate();
    if (result == 0.0) {
      // 표현식에 0 을 넣는 경우가 있어서 강제로 0.0으로 리턴해야함.
      return 0.0;
    }
    // (정가-할인가)/정가
    BigDecimal discount = BigDecimal.valueOf(result).setScale(2, BigDecimal.ROUND_HALF_UP);
    return price.subtract(discount).divide(price, 2, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100)).doubleValue();
  }
}
