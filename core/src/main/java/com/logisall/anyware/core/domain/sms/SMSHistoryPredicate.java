package com.logisall.anyware.core.domain.sms;

import com.logisall.anyware.core.domain.sms.SMSHistory;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.vo.SMS;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@NoArgsConstructor(staticName = "getInstance")
public class SMSHistoryPredicate {

  public static final QSMSHistory Q_SMS_HISTORY = QSMSHistory.sMSHistory;

  private BooleanBuilder builder = new BooleanBuilder();

  public com.logisall.anyware.core.domain.sms.SMSHistoryPredicate search(String value) {

    if (!StringUtils.isEmpty(value)) {
      value = value.trim();

      builder.and(Q_SMS_HISTORY.destPhone.containsIgnoreCase(value)
              .or(Q_SMS_HISTORY.destName.containsIgnoreCase(value))
              .or(Q_SMS_HISTORY.subject.containsIgnoreCase(value))
              .or(Q_SMS_HISTORY.msgBody.containsIgnoreCase(value)));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.sms.SMSHistoryPredicate type(final SMS.Type type) {

    if (type != null) {
      builder.and(Q_SMS_HISTORY.type.eq(type));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.sms.SMSHistoryPredicate code(final SMSHistory.Code code) {

    if (code != null) {
      builder.and(Q_SMS_HISTORY.code.eq(code));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.sms.SMSHistoryPredicate startDate(final LocalDateTime startDate) {

    if (startDate != null) {
      builder.and(Q_SMS_HISTORY.createdDate.goe(startDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.sms.SMSHistoryPredicate endDate(final LocalDateTime endDate) {

    if (endDate != null) {
      builder.and(Q_SMS_HISTORY.createdDate.loe(endDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.sms.SMSHistoryPredicate user(final User user) {

    if (user != null) {
      builder.and(Q_SMS_HISTORY.relativeUser.eq(user));
    }
    return this;
  }

  public Predicate values() {

    return builder.getValue() == null ? builder.and(Q_SMS_HISTORY.id.isNotNull()) : builder.getValue();
  }
}
