package com.logisall.anyware.core.model.resbody.board;

import com.logisall.anyware.core.config.serializer.JsonLocalDateTimeDeserializer;
import com.logisall.anyware.core.config.serializer.JsonLocalDateTimeSerializer;
import com.logisall.anyware.core.domain.board.post.Post;
import com.logisall.anyware.core.domain.embedd.SEO;
import com.logisall.anyware.core.model.BaseResponseBody;
import com.logisall.anyware.core.model.resbody.board.CategoryResBody;
import com.logisall.anyware.core.model.resbody.common.FileResBody;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "게시물(게시판)")
@Relation(value = "post", collectionRelation = "posts")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PostResBody extends BaseResponseBody {

  private static final long serialVersionUID = -7676420592670217510L;

  @Schema(description = "ID")
  private Long id;

  @Schema(description = "게시물(게시판) 유형")
  private Post.Type type; // 게시판 유형

  @Schema(description = "제목")
  private String title; // 제목

  @Schema(description = "내용(HTML)")
  private String content;

  @Schema(description = "썸네일 이미지")
  private String thumbnail; // 썸네일

  @Schema(description = "조회수")
  private long pageView; // 조회수

  @Schema(description = "작성일")
  @JsonSerialize(using = JsonLocalDateTimeSerializer.class)
  @JsonDeserialize(using = JsonLocalDateTimeDeserializer.class)
  private LocalDateTime regDate; // 등록일 , 내림차순

  @Schema(description = "검색 엔진 최적화(SEO)")
  private SEO seo;

  @Schema(description = "카테고리")
  private List<CategoryResBody> categories;

  @Schema(description = "이전글 유/무")
  private boolean hasPrevious;

  @Schema(description = "이전글 ID")
  private Long idPrevious;

  @Schema(description = "다음글 유/무")
  private boolean hasNext;

  @Schema(description = "다음글 ID")
  private Long idNext;

  @Schema(description = "첨부 파일")
  private List<FileResBody> files;

  @Schema(description = "상단고정(핀고정)")
  private boolean top;
}
