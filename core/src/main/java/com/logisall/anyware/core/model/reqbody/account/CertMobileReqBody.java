package com.logisall.anyware.core.model.reqbody.account;

import com.logisall.anyware.core.model.BaseRequestBody;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Pattern;

@Schema(description = "휴대폰 인증")
@Setter
@Getter
@ToString
public class CertMobileReqBody extends BaseRequestBody {

    private static final long serialVersionUID = 6902477733761207014L;

    @Pattern(regexp = ValidUtils.PATTERN_MOBILE)
    @Schema(description = "휴대폰 번호", example = "01011112222", minLength = 10, maxLength = 11)
    private String mobile;
}
