package com.logisall.anyware.core.service.map.kakao;

import com.logisall.anyware.core.service.map.kakao.MapKakaoService;
import com.logisall.anyware.core.service.restapi.RestAPIService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Headers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

@Slf4j
@Service
public class MapKakaoServiceImpl implements MapKakaoService {

  @Value("${kakao.api.adminKey}")
  private String adminKey;

  @Value("${kakao.api.devHost}")
  private String host;

  @Autowired
  private RestAPIService restAPIService;

  @Async
  @Override
  public AsyncResult address(String query, Integer page, Integer size) {

    if (StringUtils.isEmpty(query)) {
      throw new BadRequestException();
    }

    UriComponentsBuilder builder = UriComponentsBuilder.newInstance().scheme("https").host(host)
        .path("/v2/local/search/address.json")
        .queryParam("query", query);

    if (page != null) {
      builder.queryParam("page", page);
    }

    if (size != null) {
      builder.queryParam("size", size);
    }

    URI uri = builder.build().toUri();
    Headers headers = Headers.of("Authorization", "KakaoAK " + adminKey);

    ResponseEntity<Map> responseEntity = null;
    try {
      responseEntity = restAPIService.get(uri, headers, Map.class);
    } catch (Exception e) {
      log.error("> KAKAO MAP API ERROR", e);
    }
    if(responseEntity.getStatusCode().equals(HttpStatus.OK)) {
      return new AsyncResult<>(responseEntity.getBody());
    }
    return new AsyncResult<>(new HashMap<>());
  }

  @Async
  @Override
  public Future<Map<String,Object>> coord2regioncode(Double x, Double y, String inputCoord, String outputCoord) {

    UriComponentsBuilder builder = UriComponentsBuilder.newInstance().scheme("https").host(host)
        .path("/v2/local/geo/coord2regioncode.json")
        .queryParam("x", x)
        .queryParam("y", y)
        ;

    if (inputCoord != null) {
      builder.queryParam("input_coord", inputCoord);
    }

    if (outputCoord != null) {
      builder.queryParam("output_coord", outputCoord);
    }

    URI uri = builder.build().toUri();
    Headers headers = Headers.of("Authorization", "KakaoAK " + adminKey);

    ResponseEntity<Map> responseEntity = null;
    try {
      responseEntity = restAPIService.get(uri, headers, Map.class);
    } catch (Exception e) {
      log.error("> KAKAO MAP API ERROR", e);
    }
    if(responseEntity.getStatusCode().equals(HttpStatus.OK)) {
      return new AsyncResult<>(responseEntity.getBody());
    }
    return new AsyncResult<>(new HashMap<>());
  }

  @Async
  @Override
  public Future<Map<String,Object>> coord2address(Double x, Double y, String inputCoord) {

    UriComponentsBuilder builder = UriComponentsBuilder.newInstance().scheme("https").host(host)
        .path("/v2/local/geo/coord2address.json")
        .queryParam("x", x)
        .queryParam("y", y)
        ;

    if (inputCoord != null) {
      builder.queryParam("input_coord", inputCoord);
    }

    URI uri = builder.build().toUri();
    Headers headers = Headers.of("Authorization", "KakaoAK " + adminKey);

    ResponseEntity<Map> responseEntity = null;
    try {
      responseEntity = restAPIService.get(uri, headers, Map.class);
    } catch (Exception e) {
      log.error("> KAKAO MAP API ERROR", e);
    }
    if(responseEntity.getStatusCode().equals(HttpStatus.OK)) {
      return new AsyncResult<>(responseEntity.getBody());
    }
    return new AsyncResult<>(new HashMap<>());
  }

  @Async
  @Override
  public Future<Map<String,Object>> transcoord(Double x, Double y, String inputCoord, String outputCoord) {

    UriComponentsBuilder builder = UriComponentsBuilder.newInstance().scheme("https").host(host)
        .path("/v2/local/geo/transcoord.json")
        .queryParam("x", x)
        .queryParam("y", y)
        ;

    if (inputCoord != null) {
      builder.queryParam("input_coord", inputCoord);
    }

    if (outputCoord != null) {
      builder.queryParam("output_coord", outputCoord);
    }

    URI uri = builder.build().toUri();
    Headers headers = Headers.of("Authorization", "KakaoAK " + adminKey);

    ResponseEntity<Map> responseEntity = null;
    try {
      responseEntity = restAPIService.get(uri, headers, Map.class);
    } catch (Exception e) {
      log.error("> KAKAO MAP API ERROR", e);
    }
    if(responseEntity.getStatusCode().equals(HttpStatus.OK)) {
      return new AsyncResult<>(responseEntity.getBody());
    }
    return new AsyncResult<>(new HashMap<>());
  }

  @Async
  @Override
  public Future<Map<String,Object>> keyword(String query, CategoryType categoryGroupCode, Double x, Double y, Integer radius, String rect, Integer page, Integer size, String sort) {

    UriComponentsBuilder builder = UriComponentsBuilder.newInstance().scheme("https").host(host)
        .path("/v2/local/search/keyword.json")
        .queryParam("query", query)

        ;

    if (StringUtils.isNotEmpty(categoryGroupCode)) {
      builder.queryParam("category_group_code", categoryGroupCode);
    }
    if (x != null) {
      builder.queryParam("x", x);
    }
    if (y != null) {
      builder.queryParam("y", y);
    }
    if (radius != null) {
      builder.queryParam("radius", radius);
    }
    if (rect != null) {
      builder.queryParam("rect", rect);
    }
    if (page != null) {
      builder.queryParam("page", page);
    }
    if (size != null) {
      builder.queryParam("size", size);
    }
    if (sort != null) {
      builder.queryParam("sort", sort);
    }

    URI uri = builder.build().toUri();
    Headers headers = Headers.of("Authorization", "KakaoAK " + adminKey);

    ResponseEntity<Map> responseEntity = null;
    try {
      responseEntity = restAPIService.get(uri, headers, Map.class);
    } catch (Exception e) {
      log.error("> KAKAO MAP API ERROR", e);
    }
    if(responseEntity.getStatusCode().equals(HttpStatus.OK)) {
      return new AsyncResult<>(responseEntity.getBody());
    }
    return new AsyncResult<>(new HashMap<>());
  }

  @Async
  @Override
  public Future<Map<String,Object>> category(CategoryType categoryGroupCode, Double x, Double y, Integer radius, String rect, Integer page, Integer size, String sort) {

    UriComponentsBuilder builder = UriComponentsBuilder.newInstance().scheme("https").host(host)
        .path("/v2/local/search/category.json")
        .queryParam("category_group_code", categoryGroupCode)
        ;

    if (x != null) {
      builder.queryParam("x", x);
    }
    if (y != null) {
      builder.queryParam("y", y);
    }
    if (radius != null) {
      builder.queryParam("radius", radius);
    }
    if (rect != null) {
      builder.queryParam("rect", rect);
    }
    if (page != null) {
      builder.queryParam("page", page);
    }
    if (size != null) {
      builder.queryParam("size", size);
    }
    if (sort != null) {
      builder.queryParam("sort", sort);
    }

    URI uri = builder.build().toUri();
    Headers headers = Headers.of("Authorization", "KakaoAK " + adminKey);

    ResponseEntity<Map> responseEntity = null;
    try {
      responseEntity = restAPIService.get(uri, headers, Map.class);
    } catch (Exception e) {
      log.error("> KAKAO MAP API ERROR", e);
    }
    if(responseEntity.getStatusCode().equals(HttpStatus.OK)) {
      return new AsyncResult<>(responseEntity.getBody());
    }
    return new AsyncResult<>(new HashMap<>());
  }
}
