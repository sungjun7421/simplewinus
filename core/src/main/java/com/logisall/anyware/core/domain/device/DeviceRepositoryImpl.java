package com.logisall.anyware.core.domain.device;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class DeviceRepositoryImpl implements DeviceRepositoryCustom {

  @Autowired
  private JPAQueryFactory jpaQueryFactory;
}
