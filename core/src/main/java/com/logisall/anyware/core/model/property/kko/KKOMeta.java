package com.logisall.anyware.core.model.property.kko;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class KKOMeta implements Serializable {

    private static final long serialVersionUID = 1461272394817998308L;

    private String code;
    private String name;
    private String btnTypes;
    private String btnTxts;
    private String btnUrls1;
    private String btnUrls2;
    private String message;
}
