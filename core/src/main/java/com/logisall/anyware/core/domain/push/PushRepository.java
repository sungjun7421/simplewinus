package com.logisall.anyware.core.domain.push;


import com.logisall.anyware.core.domain.push.Push;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PushRepository extends
        PagingAndSortingRepository<Push, Long>,
        QuerydslPredicateExecutor<Push> {
}
