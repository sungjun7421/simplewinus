package com.logisall.anyware.core.service.send.email;

import com.logisall.anyware.core.config.security.SecurityUtils;
import com.logisall.anyware.core.domain.email.EmailHistory;
import com.logisall.anyware.core.domain.email.EmailHistoryRepository;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.model.aws.SESSender;
import com.logisall.anyware.core.model.property.MetaEmail;
import com.logisall.anyware.core.service.aws.AWSSESService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

@Slf4j
@Service
public class SendEmailServiceImpl implements SendEmailService {

  public static final String EVENT_BUS_COMMAND = "ES_MAIL";

  @Value("${aws.ses.sender.name}")
  private String appName;

  @Value("${aws.ses.sender.email}")
  private String sendEmailAddress;

  @Autowired
  private AWSSESService awssesService;

  @Autowired
  private Configuration cfg;


  @Autowired
  private UserRepository userRepository;

  @Autowired
  private EmailHistoryRepository emailHistoryRepository;

  @Autowired
  private MetaEmail metaEmail;


  @Override
  public void send(String to, String subject, String body) {

    try {
      com.logisall.anyware.core.model.aws.SESSender sesSender = new com.logisall.anyware.core.model.aws.SESSender(Arrays.asList(to), subject, body);
      Future<String> future = awssesService.send(sesSender);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void send(String to, String subject, String body, String[] ccList) {
    try {
      com.logisall.anyware.core.model.aws.SESSender sesSender = new com.logisall.anyware.core.model.aws.SESSender(Arrays.asList(to), subject, body);
      awssesService.send(sesSender);
    } catch (Exception e) {
      log.error("> email error", e);
      e.printStackTrace();
    }
  }

  @Override
  public void send(String to, String subject, Map<String, Object> model, String templatePathName) {
    try {

      this.addMetaEmail(model);
      Template template = cfg.getTemplate(templatePathName);
      String body = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);

      com.logisall.anyware.core.model.aws.SESSender sesSender = new com.logisall.anyware.core.model.aws.SESSender(Arrays.asList(to), subject, body);
      awssesService.send(sesSender);
    } catch (IOException | TemplateException e) {
      log.error("> email error", e);
      e.printStackTrace();
    }
  }

  @Override
  public void send(List<String> to, String subject, String body) {
    try {
      com.logisall.anyware.core.model.aws.SESSender sesSender = new SESSender(to, subject, body);
      awssesService.send(sesSender);

      to.forEach(destAddress -> {
        this.createHistory(destAddress, subject, body);
      });
    } catch (Exception e) {
      log.error("> email error", e);
      e.printStackTrace();
    }
  }

  private void addMetaEmail(Map<String, Object> model) {
    model.put("metaEmail", metaEmail);
  }

  private void createHistory(String to, String subject, String body) {

    com.logisall.anyware.core.domain.email.EmailHistory emailHistory = new EmailHistory();
    emailHistory.setSendAddress(sendEmailAddress);
    emailHistory.setDestAddress(to);
    emailHistory.setSubject(subject);
    emailHistory.setMsgBody(body);

    Long idUser = SecurityUtils.getCurrentUserId();
    if (idUser != null) {
      userRepository.findById(idUser)
          .ifPresent(emailHistory::setRelativeUser);
    }

    emailHistoryRepository.save(emailHistory);
  }
}
