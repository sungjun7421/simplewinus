package com.logisall.anyware.core.service.file;

import com.logisall.anyware.core.domain.file.FileEntity;
import com.logisall.anyware.core.service.DomainService;

public interface FileEntityService extends DomainService<FileEntity, Long> {
}
