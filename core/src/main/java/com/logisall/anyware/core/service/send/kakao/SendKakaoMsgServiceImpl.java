package com.logisall.anyware.core.service.send.kakao;

import com.logisall.anyware.core.model.property.kko.KKO;
import com.logisall.anyware.core.service.restapi.RestAPIService;
import com.logisall.anyware.core.service.restapi.RestAPIServiceImpl;
import com.logisall.anyware.core.service.send.kakao.SendKakaoMsgService;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Headers;
import okhttp3.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SendKakaoMsgServiceImpl implements SendKakaoMsgService {

  public static final String EVENT_BUS_COMMAND = "KKO";
  private static final int API_VERSION = 1;

  @Value("${apistore.id}")
  private String apiStoreId;

  @Value("${apistore.key}")
  private String apiStoreKey;

  @Value("${apistore.sms.send-phone}")
  private String sendPhone;

  @Value("${apistore.sms.send-name}")
  private String sendName;

  @Autowired
  private RestAPIService restAPIService;


  private Headers headers() {

    return new Headers.Builder()
        .add("Content-Type", "application/x-www-form-urlencoded")
        .add("x-waple-authorization", apiStoreKey)
        .build();
  }

  @Override
  public Map send(KKO kko) {
    String url = String.format("http://api.apistore.co.kr/kko/%s/msg/%s"
        , API_VERSION
        , apiStoreId
    );

    URI uri = UriComponentsBuilder.fromHttpUrl(url).build().toUri();

    ResponseEntity<Map> response = null;
    try {
      response = restAPIService.post(
          uri,
          headers(),
          RequestBody.create(
              RestAPIServiceImpl.FORM,
              "CALLBACK=" + this.sendPhone
          ),
          Map.class);
      if (response.getStatusCode().equals(HttpStatus.OK)) {
        Map result = response.getBody();

        log.debug("result ::: {}", result);
        return result;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new HashMap<String, Object>();
  }

  @Override
  public Map report(String cmid) {

    String url = String.format("http://api.apistore.co.kr/kko/%s/report/%s"
        , API_VERSION
        , apiStoreId
    );

    URI uri = UriComponentsBuilder.fromHttpUrl(url)
        .queryParam("cmid", cmid)
        .build().encode().toUri();

    ResponseEntity<Map> response = null;
    try {
      response = restAPIService.get(uri, headers(), Map.class);
      if (response.getStatusCode().equals(HttpStatus.OK)) {
        Map result = response.getBody();

        log.debug("result ::: {}", result);
        return result;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new HashMap<String, Object>();
  }

  @Override
  public List<Map<String, Object>> listTemplate() {

    String url = String.format("http://api.apistore.co.kr/kko/%s/template/list/%s"
        , API_VERSION
        , apiStoreId
    );

    URI uri = UriComponentsBuilder.fromHttpUrl(url).build().toUri();
    ResponseEntity<Map> response = null;

    try {
      response = restAPIService.get(uri, headers(), Map.class);
      if (response.getStatusCode().equals(HttpStatus.OK)) {
        Map result = response.getBody();
        List<Map<String, Object>> list = (List<Map<String, Object>>) result.get("templateList");

        log.debug("result ::: {}", result);
        log.debug("list ::: {}", list);
        return list;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new ArrayList<>();
  }

  @Override
  public Map saveSendNumber(String sendnumber, String comment, String pintype, String pincode) {

    String url = String.format("http://api.apistore.co.kr/kko/%s/sendnumber/save/%s"
        , 2
        , apiStoreId
    );

    URI uri = UriComponentsBuilder.fromHttpUrl(url).build().toUri();

    ResponseEntity<Map> response = null;
    try {
      response = restAPIService.post(
          uri,
          headers(),
          RequestBody.create(
              RestAPIServiceImpl.FORM,
              "sendnumber=" + sendnumber
              + "&comment=" + comment
              + "&pintype=" + pintype
              + "&pincode=" + pincode
          ),
          Map.class);
      if (response.getStatusCode().equals(HttpStatus.OK)) {
        Map result = response.getBody();

        log.debug("result ::: {}", result);
        return result;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new HashMap<String, Object>();
  }

  @Override
  public List<Map<String, Object>> listSendNumber() {

    String url = String.format("http://api.apistore.co.kr/kko/%s/sendnumber/list/%s"
        , API_VERSION
        , apiStoreId
    );

    URI uri = UriComponentsBuilder.fromHttpUrl(url).build().toUri();

    ResponseEntity<Map> response = null;
    try {
      response = restAPIService.get(
          uri,
          headers(),
          Map.class);
      if (response.getStatusCode().equals(HttpStatus.OK)) {
        Map result = response.getBody();
        List<Map<String, Object>> list = (List<Map<String, Object>>) result.get("numberList");

        log.debug("result ::: {}", result);
        log.debug("list ::: {}", list);
        return list;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new ArrayList<>();
  }
}
