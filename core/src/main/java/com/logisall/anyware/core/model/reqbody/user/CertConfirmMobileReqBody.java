package com.logisall.anyware.core.model.reqbody.user;

import com.logisall.anyware.core.model.BaseResponseBody;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.Pattern;

@Schema(description = "휴대폰 인증 확인")
@Slf4j
@Setter
@Getter
@ToString
public class CertConfirmMobileReqBody extends BaseResponseBody {

    private static final long serialVersionUID = 2818125420857476725L;

    @Schema(description = "인증코드")
    private String code;

    @Pattern(regexp = ValidUtils.PATTERN_MOBILE)
    @Schema(description = "휴대폰 번호", example = "01011112222", minLength = 10, maxLength = 11)
    private String mobile;
}
