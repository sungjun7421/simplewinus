package com.logisall.anyware.core.domain.commerce.point;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logisall.anyware.core.config.language.Messages;
import com.logisall.anyware.core.domain.AbstractEntityInternational;
import com.logisall.anyware.core.domain.International.InterText;
import com.logisall.anyware.core.domain.International.InternationalMode;
import com.logisall.anyware.core.domain.RestEntityBody;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.model.resbody.commerce.point.PointResBody;
import com.logisall.anyware.core.utils.DateUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Locale;
import java.util.Objects;

/**
 * 매월 1일 최근 6개월 간 누적 실결제금액을 기준으로 등급이 변경됩니다.
 * 멤버십 등급 산정 기준 및 등급별 혜택은 회사 사정에 따라 변경/대체 될 수 있습니다.
 * 포인트 사용 단위는 5,000p 부터 사용가능하며 사용 최소 단위는 100p입니다.
 */
@Entity(name = "Point")
@Getter
@Setter
@ToString(exclude = {"relativeBuyer", "relativeOrder"})
public class Point extends AbstractEntityInternational<Long> implements RestEntityBody<com.logisall.anyware.core.model.resbody.commerce.point.PointResBody> {

  private static final long serialVersionUID = 1870785277842571018L;

  public static final String[] IGNORE_PROPERTIES = {
      "id",
      "relativeBuyer",
      "relativeOrder"
  };

  @PrePersist
  public void prePersist() {
//        if (StringUtils.isEmpty(this.message)) {
//            if (Objects.equals(this.status, Status.INCREASE)) {
//                this.message = getMessageBySAVING();
//            } else if (Objects.equals(this.status, Status.DECREASE)) {
//                this.message = getMessageByUSE();
//            }
//        }

    if (Objects.equals(this.status, Status.INCREASE) && this.expirationDate == null) {
      this.expirationDate = LocalDate.now().plusYears(1);
    }

    if (this.point == null) {
      this.point = BigDecimal.ZERO;
    }

    if (this.used == null) {
      this.used = BigDecimal.ZERO;
    }
  }

  @PreUpdate
  public void preUpdate() {
  }

  public enum Status {
    INCREASE("+"),
    DECREASE("-");

    @Getter
    private final String value;

    Status(final String value) {
      this.value = value;
    }
  }

  public Point() {
    this.message = new com.logisall.anyware.core.domain.International.InterText();
    this.internationalMode = new InternationalMode();
  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @Column
  @Enumerated
  private Status status;

  @Embedded
  @AttributeOverrides({        // 임베디드 타입에 정의한 매핑정보를 재정의
      @AttributeOverride(name = "textKoKr", column = @Column(name = "messageKoKr", length = 255)),
      @AttributeOverride(name = "textEnUs", column = @Column(name = "messageEnUs", length = 255)),
      @AttributeOverride(name = "textJaJp", column = @Column(name = "messageJaJp", length = 255)),
      @AttributeOverride(name = "textZhCn", column = @Column(name = "messageZhCn", length = 255)),
      @AttributeOverride(name = "textZhTw", column = @Column(name = "messageZhTw", length = 255))
  })
  private com.logisall.anyware.core.domain.International.InterText message; // 메세지

  @Column(columnDefinition = "DECIMAL(19,2) default 0")
  private BigDecimal point;

  @Column(columnDefinition = "DECIMAL(19,2) default 0")
  private BigDecimal used; // 사용량(사용한 포인트)

  @Column
  @DateTimeFormat(pattern = DateUtils.FORMAT_DATE_UNIT_BAR)
  private LocalDate expirationDate; // 만료일


  @JsonIgnore
  @ManyToOne(targetEntity = com.logisall.anyware.core.domain.commerce.buyer.Buyer.class, optional = false)
  @JoinColumn(name = "idBuyer", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Buyer_For_Point"))
  private Buyer relativeBuyer; // 적립 대상

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "idOrder", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Order_For_Point"))
  private Order relativeOrder; // 포인트 사용취소의 주문 (주문을 취소하였을때 생성되는 감소포인트의 주문건) or 포인트 사용의 주문(주문시 사용한 포인트가 생성되는 증가포인트의 주문건)

  @Override
  public void setLocale(Locale locale) {
    super.setLocale(locale);
    if (this.message != null) {
      this.message.setLocale(locale);
    }
  }

  @Override
  public void delete() {

  }

  @Override
  public void lazy() {

  }

  //== Utils
  public static com.logisall.anyware.core.domain.International.InterText getInterTextByMessage(Messages messages, String code) {
    try {
      com.logisall.anyware.core.domain.International.InterText interText = new InterText();
      interText.setTextKoKr(messages.getMessage(code, null, null, Locale.KOREA));
      interText.setTextEnUs(messages.getMessage(code, null, null, Locale.ENGLISH));
      interText.setTextZhCn(messages.getMessage(code, null, null, Locale.CHINA));
      interText.setTextZhTw(messages.getMessage(code, null, null, Locale.TRADITIONAL_CHINESE));
      interText.setTextJaJp(messages.getMessage(code, null, null, Locale.JAPAN));
      return interText;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public com.logisall.anyware.core.model.resbody.commerce.point.PointResBody toBody(Locale locale) {
    this.setLocale(locale);

    return PointResBody
        .builder()
        .id(this.id)
        .createdDate(this.createdDate)
        .message(this.message.getValue())
        .status(this.status)
        .point(this.point != null ? this.point.intValue() : 0)
        .used(this.used != null ? this.used.intValue() : 0)
        .build();
  }
}
