package com.logisall.anyware.core.domain.board.comment;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class CommentRepositoryImpl implements CommentRepositoryCustom {

  @Autowired
  private JPAQueryFactory jpaQueryFactory;
}
