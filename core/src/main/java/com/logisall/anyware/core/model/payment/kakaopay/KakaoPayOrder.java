package com.logisall.anyware.core.model.payment.kakaopay;

import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.commerce.PayMethod;
import com.logisall.anyware.core.domain.commerce.order.AbstractOrder;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.model.payment.kakaopay.KakaoPayReadyReqBody;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.util.StringUtils;

@Slf4j
@Setter
@Getter
@ToString(callSuper = true)
@NoArgsConstructor
public class KakaoPayOrder extends AbstractOrder {

  private static final long serialVersionUID = -5371201095976246436L;

  private String approval_url; // 결제 성공시 redirect url. 최대 255자
  private String cancel_url; // 결제 취소시 redirect url. 최대 255자
  private String fail_url; // 결제 실패시 redirect url. 최대 255자

  @Override
  public Order toOrder() {
    Order order = new Order();
    order.setOrderStatus(OrderStatus.INIT);

    // 상품 정보
    order.setProducts(this.getProducts());

    // 주문자 정보
    order.setBuyerInfo(this.getBuyer());
    order.setShippingInfo(this.getShipping());
    order.setPayMethod(this.getPayMethod());

    // 혜택 사용
    // 결제 정보

    return order;
  }

  @JsonIgnore
  @Override
  public boolean isBadRequest() {
    return super.isBadRequest()
      || !this.payMethod.equals(PayMethod.KAKAO_PAY)
      || StringUtils.isEmpty(this.approval_url)
      || StringUtils.isEmpty(this.cancel_url)
      || StringUtils.isEmpty(this.fail_url);
  }

  public KakaoPayReadyReqBody toKakaoPayReadyReqBody(String cid, String oid, String email, String title) {
    return KakaoPayReadyReqBody.builder()
      .cid(cid)
      .partner_order_id(oid)
      .partner_user_id(email)
      .item_name(title)
      .quantity(this.getQty())
      .total_amount(this.amount)
      .approval_url(this.approval_url)
      .cancel_url(this.cancel_url)
      .fail_url(this.fail_url)
      .build();
  }
}
