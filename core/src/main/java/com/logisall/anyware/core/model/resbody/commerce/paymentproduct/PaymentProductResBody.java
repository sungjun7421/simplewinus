package com.logisall.anyware.core.model.resbody.commerce.paymentproduct;

import com.logisall.anyware.core.model.BaseResponseBody;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.server.core.Relation;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Relation(value = "paymentProduct", collectionRelation = "paymentProducts")
@Slf4j
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PaymentProductResBody extends BaseResponseBody {

  private static final long serialVersionUID = 2840263557823070940L;

  private Long id;
  private String status;
  private String productName;

  private BigDecimal price;
  private BigDecimal dc;
  private BigDecimal hwdc;
  private int qty;

  private BigDecimal amount;
  private int savingPoint;
  private String cancelMessage;
  private LocalDateTime cancelDate;
}
