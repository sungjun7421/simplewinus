package com.logisall.anyware.core.domain.commerce.buyerlevel;

import com.logisall.anyware.core.domain.commerce.buyer.QBuyer;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevelRepositoryCustom;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class BuyerLevelRepositoryImpl implements BuyerLevelRepositoryCustom {

  @Autowired
  private JPAQueryFactory jpaQueryFactory;

  @Override
  public BuyerLevel findByBuyerId(Long buyerId) {
    QBuyerLevel qBuyerLevel = QBuyerLevel.buyerLevel;
    QBuyer qBuyer = QBuyer.buyer;

    return jpaQueryFactory.selectFrom(qBuyerLevel)
        .innerJoin(qBuyerLevel.buyers, qBuyer)
        .where(qBuyer.id.eq(buyerId))
        .limit(1)
        .fetchFirst();
  }
}
