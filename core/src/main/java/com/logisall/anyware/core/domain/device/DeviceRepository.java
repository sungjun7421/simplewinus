package com.logisall.anyware.core.domain.device;

import com.logisall.anyware.core.domain.device.Device;
import com.logisall.anyware.core.domain.device.DeviceRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;
import java.util.Optional;

public interface DeviceRepository extends
        JpaRepository<Device, Long>,
        QuerydslPredicateExecutor<Device>,
        DeviceRepositoryCustom {

    Optional<Device> findByDeviceToken(String deviceToken);

    List<Device> findAllBySubscribe(boolean subscribe);
}
