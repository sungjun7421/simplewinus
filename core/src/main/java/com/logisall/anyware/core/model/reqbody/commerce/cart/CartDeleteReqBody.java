package com.logisall.anyware.core.model.reqbody.commerce.cart;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@ToString
public class CartDeleteReqBody implements Serializable {
    @NotNull
    private List<Long> idCarts;
}
