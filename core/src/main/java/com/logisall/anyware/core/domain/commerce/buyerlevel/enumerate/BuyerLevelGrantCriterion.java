package com.logisall.anyware.core.domain.commerce.buyerlevel.enumerate;

import lombok.Getter;

/**
 * 회원등급 부여 기준
 */
@Getter
public enum BuyerLevelGrantCriterion {

  ORDER_COUNT("Number of orders", "최근 주문건수 총합"),
  ORDER_AMOUNT("Purchase amount", "최근 구매금액 총합");

  private final String value;
  private final String description;

  BuyerLevelGrantCriterion(final String value, final String description) {
    this.value = value;
    this.description = description;
  }
}
