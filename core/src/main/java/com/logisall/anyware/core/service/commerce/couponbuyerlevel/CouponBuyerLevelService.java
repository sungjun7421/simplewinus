package com.logisall.anyware.core.service.commerce.couponbuyerlevel;

import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel;
import com.logisall.anyware.core.domain.commerce.coupon.Coupon;
import com.logisall.anyware.core.domain.commerce.coupontobuyerlevel.CouponBuyerLevel;

import java.util.List;

public interface CouponBuyerLevelService {

    CouponBuyerLevel create(Coupon coupon, BuyerLevel buyerLevel);
    List<CouponBuyerLevel> list(Long idCoupon);
}
