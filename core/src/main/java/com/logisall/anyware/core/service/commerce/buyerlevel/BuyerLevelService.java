package com.logisall.anyware.core.service.commerce.buyerlevel;

import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel;

import java.util.List;
import java.util.Locale;

public interface BuyerLevelService {
    List<BuyerLevel> list(Locale locale);
}
