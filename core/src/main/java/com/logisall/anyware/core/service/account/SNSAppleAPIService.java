package com.logisall.anyware.core.service.account;

import com.logisall.anyware.core.domain.user.sns.SNSStatus;
import com.logisall.anyware.core.model.sns.Apple;
import com.logisall.anyware.core.model.sns.apple.Payload;
import com.logisall.anyware.core.model.sns.apple.TokenResponse;

import java.util.Map;

public interface SNSAppleAPIService {

  com.logisall.anyware.core.model.sns.apple.TokenResponse getAccessToken(String code, String idToken, String redirectUri);

  SNSStatus getStatus(Apple apple);

  String getAppleClientSecret(String id_token);

  TokenResponse requestCodeValidations(String client_secret, String code, String refresh_token);

  Map<String, String> getLoginMetaInfo();

  Payload getPayload(String id_token);
}
