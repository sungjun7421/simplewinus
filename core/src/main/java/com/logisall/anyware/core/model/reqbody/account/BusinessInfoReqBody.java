package com.logisall.anyware.core.model.reqbody.account;

import com.logisall.anyware.core.model.BaseRequestBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

@Schema(description = "Business Info Request Body")
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BusinessInfoReqBody extends BaseRequestBody {

    private static final long serialVersionUID = -7858521704183857648L;

    @Schema(description = "회사명")
    private String companyName;

    @Schema(description = "사업자 번호")
    private String businessNumber;

    @Schema(description = "사업자 번호")
    private String businessEmail;

}
