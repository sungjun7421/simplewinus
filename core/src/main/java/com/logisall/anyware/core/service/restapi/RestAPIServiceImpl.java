package com.logisall.anyware.core.service.restapi;

import com.logisall.anyware.core.service.restapi.RestAPIService;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;

/**
 * OK HTTP Version
 */
@Slf4j
@Service
public class RestAPIServiceImpl implements RestAPIService {

  public static final MediaType APPLICATION_JSON = MediaType.parse("application/json;charset=utf-8");
  public static final MediaType FORM = MediaType.parse("application/x-www-form-urlencoded;charset=utf-8");

  @Autowired
  private OkHttpClient client;

  @Override
  public <T> ResponseEntity<T> get(URI uri, Headers headers, Class<T> responseType) throws Exception {

    try {
      Request.Builder reqBuilder = new Request.Builder()
          .url(uri.toURL())
          .get();

      if(headers != null) {
        reqBuilder.headers(headers);
      }

      final Response response = client.newCall(reqBuilder.build()).execute();
      final int httpStatusCode = response.code();
      final String result = response.body().string();

      return new ResponseEntity<T>(new Gson().fromJson(result, responseType), this.getHttpStatus(httpStatusCode));
    } catch (IOException e) {
      log.error("> OK HTTP IO ERROR", e);
    }
    throw new RuntimeException("API 수신이 완료되지 않았습니다.");
  }

  @Override
  public <T> ResponseEntity<T> post(URI uri, Headers headers, RequestBody requestBody, Class<T> responseType) throws Exception {

    try {
      Request.Builder reqBuilder = new Request.Builder()
          .url(uri.toURL());

      if(headers != null) {
        reqBuilder.headers(headers);
      }

      if (requestBody != null) {
        reqBuilder.post(requestBody);
      }
      return this.response(reqBuilder, responseType);
    } catch (IOException e) {
      log.error("> OK HTTP IO ERROR", e);
    }
    throw new RuntimeException("API 수신이 완료되지 않았습니다.");
  }

  @Override
  public <T> ResponseEntity<T> put(URI uri, Headers headers, RequestBody requestBody, Class<T> responseType) throws Exception {

    try {
      Request.Builder reqBuilder = new Request.Builder()
          .url(uri.toURL());

      if(headers != null) {
        reqBuilder.headers(headers);
      }

      if (requestBody != null) {
        reqBuilder.put(requestBody);
      } else {
        reqBuilder.put(RequestBody.create(APPLICATION_JSON, ""));
      }
      return this.response(reqBuilder, responseType);
    } catch (IOException e) {
      log.error("> OK HTTP IO ERROR", e);
    }
    throw new RuntimeException("API 수신이 완료되지 않았습니다.");
  }

  @Override
  public <T> ResponseEntity<T> patch(URI uri, Headers headers, RequestBody requestBody, Class<T> responseType) throws Exception {
    try {
      Request.Builder reqBuilder = new Request.Builder()
          .url(uri.toURL());

      if(headers != null) {
        reqBuilder.headers(headers);
      }

      if (requestBody != null) {
        reqBuilder.patch(requestBody);
      } else {
        reqBuilder.patch(RequestBody.create(APPLICATION_JSON, ""));
      }
      return this.response(reqBuilder, responseType);
    } catch (IOException e) {
      log.error("> OK HTTP IO ERROR", e);
    }
    throw new RuntimeException("API 수신이 완료되지 않았습니다.");
  }

  @Override
  public <T> ResponseEntity<T> delete(URI uri, Headers headers, RequestBody requestBody, Class<T> responseType) throws Exception {
    try {
      Request.Builder reqBuilder = new Request.Builder()
          .url(uri.toURL());

      if(headers != null) {
        reqBuilder.headers(headers);
      }

      if (requestBody != null) {
        reqBuilder.delete(requestBody);
      } else {
        reqBuilder.delete();
      }
      return this.response(reqBuilder, responseType);
    } catch (IOException e) {
      log.error("> OK HTTP IO ERROR", e);
    }
    throw new RuntimeException("API 수신이 완료되지 않았습니다.");
  }

  private HttpStatus getHttpStatus(int httpStatus) {
    try {
      return HttpStatus.valueOf(httpStatus);
    } catch (Exception e) {
      log.error("HTTP STATUS ERROR", e);
    }
    return HttpStatus.INTERNAL_SERVER_ERROR;
  }

  private <T> ResponseEntity<T> response(Request.Builder reqBuilder, Class<T> responseType) throws IOException {

    Request request = reqBuilder.build();

    final Response response = client.newCall(request).execute();
    final int httpStatusCode = response.code();
    final String result = response.body().string();

    return new ResponseEntity<T>(new Gson().fromJson(result, responseType), this.getHttpStatus(httpStatusCode));
  }
}
