package com.logisall.anyware.core.service.send.aligo;

public interface AligoService {
    String sendPhoneCodeWithAligo(String phoneNumber, String msg);
}
