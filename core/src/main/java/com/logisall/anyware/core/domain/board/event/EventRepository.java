package com.logisall.anyware.core.domain.board.event;


import com.logisall.anyware.core.domain.board.event.Event;
import com.logisall.anyware.core.domain.board.event.EventRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface EventRepository extends
        JpaRepository<Event, Long>,
        QuerydslPredicateExecutor<Event>,
        EventRepositoryCustom {

    Event findByTopTrue();
}
