package com.logisall.anyware.core.utils;

import com.logisall.anyware.core.utils.StringUtils;

import java.net.URLEncoder;
import java.util.Random;

public class Utils {

  public static String getRandomNumber(int length) {
    Random randomNum = new Random();
    String numTemp[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    String auth = "";

    for (int i = 0; i < length; i++) {
      auth = auth + numTemp[randomNum.nextInt(10)];
    }
    return auth;
  }

  private String getDisposition(String filename, String browser) throws Exception {
    String dispositionPrefix = "attachment;filename=";
    String encodedFilename = null;

    if (browser.equals("MSIE")) {

      encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
    } else if (browser.equals("Firefox")) {

      encodedFilename = new String(filename.getBytes("UTF-8"), "8859_1");

    } else if (browser.equals("Opera")) {

      encodedFilename = new String(filename.getBytes("UTF-8"), "8859_1");
    } else if (browser.equals("Chrome")) {

      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < filename.length(); i++) {
        char c = filename.charAt(i);
        if (c > '~') {
          sb.append(URLEncoder.encode("" + c, "UTF-8"));
        } else {
          sb.append(c);
        }
      }
      encodedFilename = sb.toString();
    } else {
      throw new RuntimeException("Not supported browser");
    }

    return dispositionPrefix + encodedFilename;
  }

  public static String getUrlEmbedVideo(String linkUrl) {
    final String startWithVimeo = "https://vimeo.com/channels/staffpicks/";
    final String startWithYouTube = "https://youtu.be/";
    StringBuffer result = new StringBuffer();


    if (linkUrl.startsWith(startWithVimeo)) {
      result.append("https://player.vimeo.com/video/");
      result.append(linkUrl.substring(startWithVimeo.length(), linkUrl.length()));
    } else if (linkUrl.startsWith(startWithYouTube)) {
      result.append("https://www.youtube.com/embed/");
      result.append(linkUrl.substring(startWithYouTube.length(), linkUrl.length()));
    } else {
      return linkUrl;
    }

    return result.toString();

  }

  public static String convertLineSeparatorToBrTag(String content) {
    if (content == null) {
      return null;
    }
    return content
      .replaceAll("\r\n", "<br/>")
      .replaceAll(System.getProperty("line.separator"), "<br/>")
      .replaceAll("\n", "<br/>");
  }

  public static String convertBrTagToLineSeparator(String content) {
    if (content == null) {
      return null;
    }
    return content
      .replaceAll("<br/>", System.getProperty("line.separator"))
      .replaceAll("<br>", System.getProperty("line.separator"));
  }

  public static String convertNameToXXX(String name) {

    if (name == null) {
      return null;
    }

    int length = name.length() - 1;

    String result = "";

    for (int i = 0; i < length; i++) {
      result += "*";
    }

    return name.substring(0, 2) + result;
  }

  public static String replaceLink(String link) {
    if (StringUtils.isNotEmpty(link)
      && !link.startsWith("http://")
      && !link.startsWith("https://")) {
      return link = "http://" + link;
    }
    return link;
  }

//    public static String compressHtml(String content) {
//        return new HtmlCompressor().compress(content);  // html minify
//    }
}
