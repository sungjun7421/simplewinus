package com.logisall.anyware.core.model.payment.toss;

import lombok.Getter;

@Getter
public enum TossStatus {

  PAY_STANDBY("결제 대기 중"),
  PAY_APPROVED("매자 인증 완료"),
  PAY_CANCEL("결제 취소"),
  PAY_PROGRESS("결제 진행 중"),
  PAY_COMPLETE("결제 완료"),
  REFUND_PROGRESS("환불 진행 중"),
  REFUND_SUCCESS("환불 성공"),
  SETTLEMENT_COMPLETE("정산 완료"),
  SETTLEMENT_REFUND_COMPLETE("환불 정산 완료");

  @Getter
  private final String value;

  TossStatus(final String value) {
    this.value = value;
  }
}
