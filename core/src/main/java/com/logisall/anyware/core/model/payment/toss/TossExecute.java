package com.logisall.anyware.core.model.payment.toss;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class TossExecute implements Serializable {

    private static final long serialVersionUID = -7866976315262002857L;

    private String apiKey;
    private String payToken;
}
