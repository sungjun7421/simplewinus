package com.logisall.anyware.core.model.sns.apple;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class ServicesResponse implements Serializable {

    private static final long serialVersionUID = 3237345860761442143L;
    private String state;
    private String code;
    private String id_token;
    private String user;


}
