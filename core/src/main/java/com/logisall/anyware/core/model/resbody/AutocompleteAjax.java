package com.logisall.anyware.core.model.resbody;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class AutocompleteAjax<T> implements Serializable {
  private static final long serialVersionUID = -2141299514219141182L;

  private String value;
  private T data;
}
