package com.logisall.anyware.core.model.resbody.user;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.logisall.anyware.core.config.serializer.JsonLocalDateTimeDeserializer;
import com.logisall.anyware.core.config.serializer.JsonLocalDateTimeSerializer;
import com.logisall.anyware.core.model.BaseResponseBody;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.server.core.Relation;

import java.time.LocalDateTime;

@Relation(value = "changeSubscribe")
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChangeSubscribeResBody extends BaseResponseBody {

  private boolean agree;

  @JsonSerialize(using = JsonLocalDateTimeSerializer.class)
  @JsonDeserialize(using = JsonLocalDateTimeDeserializer.class)
  private LocalDateTime time;
}
