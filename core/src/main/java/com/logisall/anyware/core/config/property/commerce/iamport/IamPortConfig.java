package com.logisall.anyware.core.config.property.commerce.iamport;

import com.logisall.anyware.core.config.property.commerce.iamport.IPRest;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Map;

@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "es.iamport")
public class IamPortConfig implements Serializable {

  private static final long serialVersionUID = 3511757761705505363L;

  private String jsurl;
  private String code;
  private IPRest rest;
  private String pg;
  private boolean escrow;
  private String mredirecurl;
  private Map<String, String> payMethod;
  private String vbankHolder;
}
