package com.logisall.anyware.core.model.sns;

import lombok.Getter;

@Getter
public enum SNSType {

    FACEBOOK("페이스북"),
    GOOGLE("구글"),
    NAVER("네이버"),
    KAKAO("카카오"),
    APPLE("애플"),
    INSTAGRAM("Instagram");

    final private String value;

    SNSType(final String value) {
        this.value = value;
    }
}
