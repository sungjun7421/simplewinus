package com.logisall.anyware.core.model.payment.toss;

import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.commerce.order.AbstractOrder;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.model.payment.toss.TossBody;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
@ToString
public class Toss extends AbstractOrder {

  private static final long serialVersionUID = 1085801704605410775L;

  private String name;
  private String retAppScheme;

  public boolean isValid() {
    return this.amount == 0;
  }

  public TossBody toBody(String orderId, String apiKey) {
    TossBody tb = new TossBody();
    tb.setOrderNo(orderId);
    tb.setAmount(this.amount);
    tb.setAmountTaxFree(0);
    tb.setApiKey(apiKey);
    tb.setProductDesc(this.name);
    tb.setRetUrl("http://");
    tb.setRetAppScheme(retAppScheme);
    tb.setAutoExecute(true);
    return tb;
  }

  @Override
  public Order toOrder() {
    Order order = new Order();
    order.setOrderStatus(OrderStatus.INIT);

    // 상품 정보
    order.setProducts(this.getProducts());

    // 주문자 정보
    order.setBuyerInfo(this.getBuyer());
    order.setShippingInfo(this.getShipping());
    order.setPayMethod(this.getPayMethod());

    // 혜택 사용
    // 결제 정보

    return order;
  }

}
