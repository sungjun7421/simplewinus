package com.logisall.anyware.core.config.property.commerce.lguplus;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class Mall implements Serializable{

    private static final long serialVersionUID = 1990128988506612931L;

    private String id;
    private String mertKey;
    private String serverId;
    private String timeout;
    private String logLevel;
    private String verifyCert;
    private String verifyHost;
    private String reportError;
    private String outputUtf8;
    private String autoRollback;
    private String logDir;
    private String keystoreCacertsDir;
}
