package com.logisall.anyware.core.model.reqbody.account;

import com.logisall.anyware.core.model.BaseRequestBody;
import com.logisall.anyware.core.model.sns.SNSType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ChangeSnsNativeReqBody extends BaseRequestBody {

  private static final long serialVersionUID = 5738785088672460354L;

  @Schema(description = "sns type", example = "FACEBOOK", minLength = 2, maxLength = 30)
  private SNSType snsType; // sns 유형

  @Schema(description = "active", example = "true")
  private boolean active;

  @Schema(description = "id", example = "1")
  private String id;

  @Schema(description = "name", example = "홍길동", minLength = 2, maxLength = 30)
  private String name;
}
