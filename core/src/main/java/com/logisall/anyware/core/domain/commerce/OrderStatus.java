package com.logisall.anyware.core.domain.commerce;

import lombok.Getter;


@Getter
public enum OrderStatus {

  INIT("주문번호 생성"), // 결제모듈를 시작하기 전 상태,주문정보 등록
  V_READY("입금 대기"), // 입금 전 상태(3일 이내 미입금시 자동 취소)
  COMPLETE("결제완료"), // 결제모듈을 통해 결제가 완료된 상태
  SHIP1("상품준비중"), // 상품준비중,배송중,배송완료
  SHIP2("배송중"), // 상품준비중,배송중,배송완료
  SHIP3("배송완료"), // 상품준비중,배송중,배송완료
//  PICK("픽업"), // 예약완료,상품준비중,픽업가능,픽업완료
  CONFIRM("구매확정"), // 고객이 상품 수령을 완료하고 구매를 확정한 상태
  CANCEL1("반품 접수"), // 고객이 상품의 하자의 인하여 일부 제품의 반품을 요청한 상태 (접수 후 반품 가능 시 환불 요청)
  CANCEL2("상품회수 중"),
  CANCEL("상품회수 완료"),
  REFUND("환불"),
  FAILED("결제실패"),
  ;

  private final String value;

  public String getLabelClass() {
    if (this.value != null) {
      switch (this) {
        case COMPLETE:
          return "label-success";
        case CANCEL1:
        case CANCEL2:
        case CANCEL:
          return "label-warning";
        case FAILED:
          return "label-danger";
        default:
          return "label-info";
      }
    }
    return "label-danger";
  }

  OrderStatus(final String value) {
    this.value = value;
  }
}
