package com.logisall.anyware.core.service.board.post;

import com.google.common.collect.Lists;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.board.post.Post;
import com.logisall.anyware.core.domain.board.post.PostPredicate;
import com.logisall.anyware.core.domain.board.post.PostRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.admin.PostInfoResBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PostServiceImpl implements PostService {

  @Autowired
  private PostRepository postRepository;

  @Override
  @Transactional
  public com.logisall.anyware.core.domain.board.post.Post create(com.logisall.anyware.core.domain.board.post.Post post) {
    post.uploadFiles();
    return postRepository.save(post);
  }

  @Override
  @Transactional
  public com.logisall.anyware.core.domain.board.post.Post update(com.logisall.anyware.core.domain.board.post.Post post) {

    if (post.getId() == null) {
      throw new com.logisall.anyware.core.config.exception.BadRequestException();
    }
    return postRepository.findById(post.getId())
            .map(ori -> {
              post.uploadFiles();
              BeanUtils.copyProperties(post, ori, com.logisall.anyware.core.domain.board.post.Post.IGNORE_PROPERTIES);
              return postRepository.save(ori);
            }).orElseThrow(com.logisall.anyware.core.config.exception.BadRequestException::new);
  }

  @Override
  @Transactional(readOnly = true)
  public com.logisall.anyware.core.domain.board.post.Post get(Locale locale, Long id) {
    return postRepository.findById(id)
            .map(post -> {
              post.lazy();
              post.setLocale(locale);
              return post;
            }).orElseThrow(BadRequestException::new);
  }

  @Override
  @Transactional(readOnly = true)
  public void delete(Long id) {
    postRepository.deleteById(id);
  }

  @Override
  public Page<com.logisall.anyware.core.domain.board.post.Post> page(Locale locale, com.logisall.anyware.core.model.Filter filter) {
   return this.page(locale, filter, null, null, null);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<com.logisall.anyware.core.domain.board.post.Post> page(Locale locale, com.logisall.anyware.core.model.Filter filter, com.logisall.anyware.core.domain.board.post.Post.Type type) {
    return this.page(locale, filter, type, null, null);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<com.logisall.anyware.core.domain.board.post.Post> page(Locale locale, com.logisall.anyware.core.model.Filter filter, com.logisall.anyware.core.domain.board.post.Post.Type type, Boolean isActive) {
    return this.page(locale, filter, type, isActive, null);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<com.logisall.anyware.core.domain.board.post.Post> page(Locale locale, Filter filter, com.logisall.anyware.core.domain.board.post.Post.Type type, Boolean isActive, Long idCategory) {

    Page<com.logisall.anyware.core.domain.board.post.Post> page = postRepository.findAll(
            com.logisall.anyware.core.domain.board.post.PostPredicate.getInstance()
                    .search(filter.getQuery())
                    .startDate(filter.getStartDate())
                    .endDate(filter.getEndDate())
                    .type(type)
                    .active(isActive)
                    .category(idCategory)
                    .values(),
            filter.getPageable());

    page.forEach(post -> {
      post.lazy();
      post.setLocale(locale);
    });
    return page;
  }

  @Override
  @Transactional(readOnly = true)
  public List<com.logisall.anyware.core.model.admin.PostInfoResBody> list(String query) {

    List<Post> list = Lists.newArrayList(postRepository.findAll(
        PostPredicate.getInstance()
            .search(query)
            .values()));

    return list.stream().map(post -> {
      com.logisall.anyware.core.model.admin.PostInfoResBody po = new PostInfoResBody();
      po.setId(post.getId());
      po.setTitle(post.getTitle());
      po.setContent(post.getContent());
      return po;
    }).collect(Collectors.toList());
  }
}
