package com.logisall.anyware.core.domain.email;


import com.logisall.anyware.core.domain.user.User;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmailHistoryRepository extends
    PagingAndSortingRepository<com.logisall.anyware.core.domain.email.EmailHistory, Long>,
    QuerydslPredicateExecutor<com.logisall.anyware.core.domain.email.EmailHistory>,
    EmailHistoryRepositoryCustom {

    List<com.logisall.anyware.core.domain.email.EmailHistory> findAllByTypeAndSendAddressAndRelativeUserOrderBySendTimeDesc(EmailHistory.Type type, String sendAddress, User user);
}
