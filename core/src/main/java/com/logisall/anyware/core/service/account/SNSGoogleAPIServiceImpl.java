package com.logisall.anyware.core.service.account;

import com.logisall.anyware.core.config.exception.NotFoundException;
import com.logisall.anyware.core.config.property.sns.GoogleApiKey;
import com.logisall.anyware.core.domain.user.SocialId;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.domain.user.sns.SNSStatus;
import com.logisall.anyware.core.model.sns.GooglePojo;
import com.logisall.anyware.core.service.account.SNSGoogleAPIService;
import com.logisall.anyware.core.service.restapi.RestAPIService;
import com.logisall.anyware.core.service.restapi.RestAPIServiceImpl;
import com.logisall.anyware.core.utils.FileUtils;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import okhttp3.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Map;
import java.util.Optional;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

@Slf4j
@Service
public class SNSGoogleAPIServiceImpl implements SNSGoogleAPIService {

  @Autowired
  private GoogleApiKey apiKey;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private RestAPIService restAPIService;

  @Override
  public Map getAccessToken(String code, String redirectUri) {
    URI uri = UriComponentsBuilder.fromHttpUrl(apiKey.getAccessTokenUri()).build().toUri();
    log.debug("uri ::: {}", uri.toString());
    try {

      ResponseEntity<Map> response = restAPIService.post(
          uri,
          null,
          RequestBody.create(RestAPIServiceImpl.FORM
              , "grant_type=authorization_code"
                  + "&client_id=" + apiKey.getClientId()
                  + "&client_secret=" + apiKey.getClientSecret()
                  + "&code=" + code
                  + "&redirect_uri=" + redirectUri
          ),
          Map.class);

      if (response.getStatusCode().equals(HttpStatus.OK)) {
        return response.getBody();
      } else {
        log.error("> Google Login error : {}", response.getBody());
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    throw new RuntimeException();
  }

  @Override
  public GooglePojo getMe(String accessToken) {

    URI uri = UriComponentsBuilder.fromHttpUrl(apiKey.getMeUri())
        .queryParam("access_token", accessToken)
        .build().toUri();

    log.debug("uri ::: {}", uri.toString());

//        RestTemplate restTemplate = new RestTemplate();
//    RestTemplate restTemplate = new RestTemplateBuilder()
//        .errorHandler(new RestTemplateResponseErrorHandler())
//        .build();
//    ResponseEntity<GooglePojo> response = restTemplate.getForEntity(uri, GooglePojo.class);
//
//    return response.getBody();
//
//    log.debug("uri ::: {}", uri.toString());

    try {
      ResponseEntity<GooglePojo> response = restAPIService.get(uri, null, GooglePojo.class);

      if (response.getStatusCode().equals(HttpStatus.OK))
        return response.getBody();
      else
        log.error("> response : {}", response.getBody());
    } catch (Exception e) {
      e.printStackTrace();
    }
    throw new RuntimeException();
  }

  static class RestTemplateResponseErrorHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
      return (response.getStatusCode().series() == CLIENT_ERROR || response.getStatusCode().series() == SERVER_ERROR);
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
      if (response.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR) {
        // handle SERVER_ERROR
        this.log(response);
      } else if (response.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR) {
        // handle CLIENT_ERROR
        if (response.getStatusCode() == HttpStatus.NOT_FOUND) {
          throw new NotFoundException();
        }
        this.log(response);
      }
    }

    private void log(ClientHttpResponse response) throws IOException {
      log.error("error status ({}) ::: {}", response.getRawStatusCode(), response.getStatusText());
      try (InputStream stream = response.getBody()) {
        log.error("error body ::: {}", FileUtils.getStreamString(stream));
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  @Transactional
  public SNSStatus getStatus(GooglePojo google) {
    Optional<User> userOpt = userRepository.findBySocialIdGoogleId(google.getId());

    if (userOpt.isPresent()) {
      return SNSStatus.LINKED; // 이미 연동되어었음
    } else if (StringUtils.isNotEmpty(google.getEmail())) {

      // 구굴에서 제공한 이메일 정보로 계정 조회
      userOpt = userRepository.findByEmail(google.getEmail());

      if (userOpt.isPresent()) {
        /* 구굴에서 제공한 이메일 정보로 조회한 계정이 존재한다면 */

        User user = userOpt.get();

        if (user.isWithd()) {
          /* 탈퇴한 계졍이라면 */
          return SNSStatus.LEAVED_ACCOUNT;
        }

        if (user.getSocialId() == null || StringUtils.isEmpty(user.getSocialId().getGoogleId())) {
          /* 계정에 구굴 ID가 연동되어 있지 않으면 연동 시켜준다. */

          SocialId socialId = user.getSocialId() == null ? new SocialId() : user.getSocialId();
          socialId.setGoogleId(google.getId());
          socialId.setGoogleName(google.getName());

          user.setSocialId(socialId);

          if (StringUtils.isNotEmpty(google.getPicture())) {
            user.setImage(google.getPicture());
          }

          userRepository.save(user);
          return SNSStatus.CONNECT;
        } else {
          /* 계정에 다른 구굴 ID가 연동되어 있다면 */
          return SNSStatus.NOT_MATCH_SNS;
        }
      } else {
        /* 구굴에서 제공한 정보와 동일한 이메일과 구굴 ID 가 존재하지 않는다. -> 회원가입 */
        return SNSStatus.NOT_EXISTED_ACCOUNT;
      }
    } else {
      /* 구굴에서 이메일을 제공하지 않았다면 -> 회원가입 */
      return SNSStatus.NOT_PROVIDED_EMAIL;
    }
  }
}
