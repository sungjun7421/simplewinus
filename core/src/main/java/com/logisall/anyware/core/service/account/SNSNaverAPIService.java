package com.logisall.anyware.core.service.account;

import com.logisall.anyware.core.domain.user.sns.SNSStatus;
import com.logisall.anyware.core.model.sns.Naver;

import java.util.Map;

public interface SNSNaverAPIService {

  Map getAccessToken(String code, String redirectUri, String state);

  Map getMe(String accessToken);

  SNSStatus getStatus(Naver naver);
}
