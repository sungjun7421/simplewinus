package com.logisall.anyware.core.service.board.comment;

import com.logisall.anyware.core.domain.board.comment.Comment;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.DomainService;
import org.springframework.data.domain.Page;

public interface CommentService extends DomainService<Comment, Long> {

    Page<Comment> page(Filter filter,
                       Long idUser,
                       Comment.Type type,
                       Long idContent,
                       Long idParent,
                       Boolean active);
}
