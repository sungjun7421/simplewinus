package com.logisall.anyware.core.service.document;

import com.logisall.anyware.core.domain.document.Document;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.DomainService;
import org.springframework.data.domain.Page;

import java.util.Locale;

public interface DocumentService extends DomainService<Document, Long> {

    Page<Document> page(Locale locale, Filter filter, Document.Type type);

}
