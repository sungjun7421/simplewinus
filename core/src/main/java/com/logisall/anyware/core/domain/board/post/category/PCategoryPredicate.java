package com.logisall.anyware.core.domain.board.post.category;

import com.logisall.anyware.core.domain.board.post.Post;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Objects;

@NoArgsConstructor(staticName = "getInstance")
public class PCategoryPredicate {

  private static final QPCategory Q_ENTITY = QPCategory.pCategory;

  private final BooleanBuilder builder = new BooleanBuilder();

  public Predicate values() {
    return builder.getValue() == null ? builder.and(Q_ENTITY.id.isNotNull()) : builder.getValue();
  }

  public PCategoryPredicate startDate(final LocalDateTime startDate) {

    if (startDate != null) {
      builder.and(Q_ENTITY.createdDate.goe(startDate));
    }
    return this;
  }

  public PCategoryPredicate endDate(final LocalDateTime endDate) {

    if (endDate != null) {
      builder.and(Q_ENTITY.createdDate.loe(endDate));
    }
    return this;
  }

  public PCategoryPredicate search(String value) {

    if (!StringUtils.isEmpty(value)) {
      value = value.trim();

      builder.and(Q_ENTITY.name.textZhTw.containsIgnoreCase(value)
          .or(Q_ENTITY.name.textZhCn.containsIgnoreCase(value))
          .or(Q_ENTITY.name.textKoKr.containsIgnoreCase(value))
          .or(Q_ENTITY.name.textJaJp.containsIgnoreCase(value))
          .or(Q_ENTITY.name.textEnUs.containsIgnoreCase(value)));
    }
    return this;
  }

  public PCategoryPredicate name(String name) {

    if (!StringUtils.isEmpty(name)) {
      name = name.trim();
      builder.and(Q_ENTITY.name.textZhTw.eq(name)
          .or(Q_ENTITY.name.textZhCn.eq(name))
          .or(Q_ENTITY.name.textKoKr.eq(name))
          .or(Q_ENTITY.name.textJaJp.eq(name))
          .or(Q_ENTITY.name.textEnUs.eq(name)));
    }
    return this;
  }

  public PCategoryPredicate type(final Post.Type type) {

    if (type != null) {
      builder.and(Q_ENTITY.type.eq(type));
    }
    return this;
  }

  public PCategoryPredicate active(final Boolean isActive) {

    if (isActive != null) {
      builder.and(Q_ENTITY.active.eq(isActive));
    }
    return this;
  }

  public PCategoryPredicate locale(Locale locale, final Locale defaultLocale) {

    if (!Objects.equals(locale, Locale.KOREA)
        && !Objects.equals(locale, Locale.US)
        && !Objects.equals(locale, Locale.CHINA)
        && !Objects.equals(locale, Locale.TAIWAN)
        && !Objects.equals(locale, Locale.JAPAN)) {
      locale = defaultLocale;
    }

    if (Objects.equals(locale, Locale.KOREA)) {
      builder.and(Q_ENTITY.internationalMode.koKr.eq(true));
    } else if (Objects.equals(locale, Locale.US)) {
      builder.and(Q_ENTITY.internationalMode.enUs.eq(true));
    } else if (Objects.equals(locale, Locale.CHINA)) {
      builder.and(Q_ENTITY.internationalMode.zhCn.eq(true));
    } else if (Objects.equals(locale, Locale.TAIWAN)) {
      builder.and(Q_ENTITY.internationalMode.zhTw.eq(true));
    } else if (Objects.equals(locale, Locale.JAPAN)) {
      builder.and(Q_ENTITY.internationalMode.jaJp.eq(true));
    }
    return this;
  }
}
