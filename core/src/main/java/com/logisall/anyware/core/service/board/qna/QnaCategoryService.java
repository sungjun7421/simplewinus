package com.logisall.anyware.core.service.board.qna;

import com.logisall.anyware.core.domain.board.qna.category.QnaCategory;
import com.logisall.anyware.core.service.CategoryService;

public interface QnaCategoryService extends CategoryService<QnaCategory, Long> {

}
