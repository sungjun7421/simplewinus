package com.logisall.anyware.core.domain.board.contact;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@NoArgsConstructor(staticName = "getInstance")
public class ContactPredicate {

    public static final QContact Q_ENTITY = QContact.contact;

    private BooleanBuilder builder = new BooleanBuilder();

    public Predicate values() {
        return builder.getValue() == null ? builder.and(Q_ENTITY.id.isNotNull()) : builder.getValue();
    }

    public com.logisall.anyware.core.domain.board.contact.ContactPredicate startDate(final LocalDateTime startDate) {

        if (startDate != null) {
            builder.and(Q_ENTITY.createdDate.goe(startDate));
        }
        return this;
    }

    public com.logisall.anyware.core.domain.board.contact.ContactPredicate endDate(final LocalDateTime endDate) {

        if (endDate != null) {
            builder.and(Q_ENTITY.createdDate.loe(endDate));
        }
        return this;
    }


    public com.logisall.anyware.core.domain.board.contact.ContactPredicate search(String value) {

        if (!StringUtils.isEmpty(value)) {
            value = value.trim();

            builder.and(Q_ENTITY.content.containsIgnoreCase(value)
                    .or(Q_ENTITY.fullName.eq(value))
                    .or(Q_ENTITY.email.eq(value))
                    .or(Q_ENTITY.mobile.eq(value))
            );
        }
        return this;
    }
}
