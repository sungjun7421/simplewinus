package com.logisall.anyware.core.model.reqbody.commerce.cart;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Slf4j
@Setter
@Getter
@ToString
public class CartRequestBody implements Serializable {
    @NotNull
    private Long idProduct;
    @Min(1)
    private Integer qty;
}
