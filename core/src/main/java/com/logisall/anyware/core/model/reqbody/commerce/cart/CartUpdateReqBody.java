package com.logisall.anyware.core.model.reqbody.commerce.cart;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Setter
@Getter
@ToString
public class CartUpdateReqBody implements Serializable {
    @NotNull
    private Long idCart;
    @NotNull(message = "QTY not null and only number")
    @Min(1)
    private Integer qty;
}
