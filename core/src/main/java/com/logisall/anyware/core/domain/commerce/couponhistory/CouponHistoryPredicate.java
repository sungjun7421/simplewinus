package com.logisall.anyware.core.domain.commerce.couponhistory;

import com.logisall.anyware.core.domain.commerce.coupon.CouponType;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@NoArgsConstructor(staticName = "getInstance")
public class CouponHistoryPredicate {

    private static final QCouponHistory Q_COUPON_HISTORY = QCouponHistory.couponHistory;

    private BooleanBuilder builder = new BooleanBuilder();

    public Predicate values() {
        return builder.getValue() == null ? builder.and(Q_COUPON_HISTORY.id.isNotNull()) : builder.getValue();
    }

    public CouponHistoryPredicate search(String value) {

        if (!StringUtils.isEmpty(value)) {
            value = value.trim();
            builder.and(Q_COUPON_HISTORY.relativeCoupon.name.textEnUs.containsIgnoreCase(value)
                    .or(Q_COUPON_HISTORY.relativeCoupon.name.textJaJp.containsIgnoreCase(value))
                    .or(Q_COUPON_HISTORY.relativeCoupon.name.textKoKr.containsIgnoreCase(value))
                    .or(Q_COUPON_HISTORY.relativeCoupon.name.textZhCn.containsIgnoreCase(value))
                    .or(Q_COUPON_HISTORY.relativeCoupon.name.textZhTw.containsIgnoreCase(value)));
        }

        return this;
    }

    public CouponHistoryPredicate used(final Boolean used) {

        if (used != null) {
            builder.and(Q_COUPON_HISTORY.used.eq(used));
        }

        return this;
    }

    public CouponHistoryPredicate type(final CouponType type) {

        if (type != null) {
            builder.and(Q_COUPON_HISTORY.relativeCoupon.type.eq(type));
        }
        return this;
    }


    public CouponHistoryPredicate buyer(final String email) {

        if (StringUtils.isEmpty(email)) {
            builder.and(Q_COUPON_HISTORY.relativeBuyer.relativeUser.email.eq(email));
        }

        return this;
    }

    public CouponHistoryPredicate startDate(final LocalDateTime startDate) {
        if (!StringUtils.isEmpty(startDate)) {
            builder.and(Q_COUPON_HISTORY.createdDate.goe(startDate));
        }
        return this;
    }
    public CouponHistoryPredicate endDate(final LocalDateTime endDate) {
        if (!StringUtils.isEmpty(endDate)) {
            builder.and(Q_COUPON_HISTORY.createdDate.loe(endDate));
        }
        return this;
    }


//    public static Predicate locale(Locale locale, final Locale defaultLocale) {
//        Locale aLocale = new Locale.Builder().setLanguage("us").setRegion("EN").build();
//
//        if (!Objects.equals(locale, Locale.KOREA)
//                && !Objects.equals(locale, Locale.US)
//                && !Objects.equals(locale, Locale.CHINA)
//                && !Objects.equals(locale, Locale.TAIWAN)
//                && !Objects.equals(locale, Locale.JAPAN)
//                && !Objects.equals(locale, aLocale)) {
//            locale = defaultLocale;
//        }
//
//        return null;
//    }
}
