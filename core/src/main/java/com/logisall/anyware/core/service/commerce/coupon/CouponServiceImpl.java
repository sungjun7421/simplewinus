package com.logisall.anyware.core.service.commerce.coupon;

import com.logisall.anyware.core.service.commerce.coupon.CouponService;
import com.logisall.anyware.core.service.commerce.couponbuyerlevel.CouponBuyerLevelService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.crud.UpdateErrorException;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel;
import com.logisall.anyware.core.domain.commerce.coupon.Coupon;
import com.logisall.anyware.core.domain.commerce.coupon.CouponPredicate;
import com.logisall.anyware.core.domain.commerce.coupon.CouponRepository;
import com.logisall.anyware.core.domain.commerce.coupon.CouponType;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.setting.AppSettingService;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.RandomStringGenerator;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static org.apache.commons.text.CharacterPredicates.DIGITS;
import static org.apache.commons.text.CharacterPredicates.LETTERS;

@Slf4j
@Service
@Transactional
public class CouponServiceImpl implements CouponService {

  @Autowired
  private CouponRepository couponRepository;

  @Autowired
  private CouponBuyerLevelService couponBuyerLevelService;


  @Autowired
  private AppSettingService settingService;

  private void addCouponBuyerLevel(Coupon coupon, List<BuyerLevel> buyerLevelList) {
    if (buyerLevelList != null) {
      buyerLevelList.stream().filter(Objects::nonNull).forEach(buyerLevel -> {
        couponBuyerLevelService.create(coupon, buyerLevel);
      });
    }
  }

  @Override
  @Transactional
  public Coupon create(Coupon coupon) {
    Coupon created = couponRepository.save(coupon);
    this.addCouponBuyerLevel(coupon, coupon.getBuyerLevels());
    return created;
  }

  @Override
  @Transactional
  public Coupon update(Coupon coupon) {

    if (coupon.getId() == null) {
      throw new BadRequestException();
    }

    return couponRepository.findById(coupon.getId())
      .map(ori -> {
        BeanUtils.copyProperties(coupon, ori, Coupon.IGNORE_PROPERTIES);
        return couponRepository.save(ori);
      }).orElseThrow(() -> new UpdateErrorException(coupon.getId(), Coupon.class.getName()));
  }

  @Override
  @Transactional
  public void delete(Long id) {
    couponRepository.findById(id)
      .ifPresent(coupon -> {
        coupon.delete();
        couponRepository.delete(coupon);
      });
  }

  @Override
  @Transactional
  public Coupon get(Locale locale, Long id) {
    return couponRepository.findById(id)
      .map(coupon -> {
        coupon.setLocale(locale);
        coupon.lazy();
        return coupon;
      }).orElse(null);
  }

  @Override
  @Transactional
  public Page<Coupon> page(Locale locale, Filter filter, CouponType type) {
    Page<Coupon> page = couponRepository.findAll(
            CouponPredicate.getInstance()
              .search(filter.getQuery())
              .startDate(filter.getStartDate())
              .endDate(filter.getEndDate())
              .type(type)
              .values(), filter.getPageable());

    page.forEach(coupon -> {
      coupon.setLocale(locale);
      coupon.lazy();
    });

    return page;
  }

  @Override
  @Transactional
  public List<Coupon> list(Locale locale, CouponType type) {
    List<Coupon> list = Lists.newArrayList(couponRepository.findAll(
            CouponPredicate.getInstance()
              .type(type)
              .active(true)
              .values()
    ));
    list.forEach(coupon -> {
      coupon.setLocale(locale);
      coupon.lazy();
    });
    return list;
  }

  @Override
  @Transactional
  public String generateCode() {
    return this.generateCode(12);
  }

  @Override
  @Transactional
  public String generateCode(int length) {
    if (length > 20) {
      throw new RuntimeException("최대 20자리까지 생성 가능합니다.");
    }

    RandomStringGenerator generator = new RandomStringGenerator.Builder().withinRange('0', 'Z').filteredBy(LETTERS, DIGITS).build();
    String code = generator.generate(length);
    if (couponRepository.existsByCode(code)) {
      return this.generateCode(length);
    } else {
      return code;
    }
  }
}
