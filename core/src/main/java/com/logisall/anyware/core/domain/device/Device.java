package com.logisall.anyware.core.domain.device;

import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.push.Push;
import com.logisall.anyware.core.domain.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Entity(name = "Device")
@Getter
@Setter
@ToString()
@NoArgsConstructor
public class Device extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1246152228563394910L;

    public enum Type {
        IOS, AOS
    }

    public static String[] IGNORE_PROPERTIES = {
            "id",
            "relativeUser"
    };

    @PrePersist
    public void prePersist() {

    }

    @PreUpdate
    public void PreUpdate() {

    }


    @Id
    @GeneratedValue
    @Column(unique = true)
    private Long id;

    @Enumerated(EnumType.STRING)
    private Type type;

    @Column(unique = true)
    private String deviceToken; // FCM 토큰

    @Column(columnDefinition = "BIT(1) default 0")
    private boolean subscribe;

    @Column(columnDefinition = "BIT(1) default 0")
    private boolean logout; // 로그아웃 상태

    @ManyToOne
    @JoinColumn(name = "idUser", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_User_For_Device"))
    private User relativeUser; // 회원 계정

    @OneToMany(mappedBy = "relativeDevice", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Push> pushHistories = new ArrayList<>(); // 푸시 알림 내역

    @Override
    public void delete() {

    }

    @Override
    public void lazy() {

    }
}
