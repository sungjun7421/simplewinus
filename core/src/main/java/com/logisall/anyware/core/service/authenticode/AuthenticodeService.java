package com.logisall.anyware.core.service.authenticode;

import com.logisall.anyware.core.domain.authenticode.Authenticode;
import com.logisall.anyware.core.model.AuthentiCodeResBody;

/**
 * 인증 토큰
 * (문자 인증, 이메일 인증 등 사용)
 */
public interface AuthenticodeService {

    String tokenCode(Authenticode tokenStorage);

    com.logisall.anyware.core.model.AuthentiCodeResBody confirmByMobile(String token, String mobile);

    com.logisall.anyware.core.model.AuthentiCodeResBody getAuthCode(String token);
    AuthentiCodeResBody confirmByEmail(String token);

    String generateToken();

    void deleteByToken(String token);
    void deletePreviousValue(String mobile);

}
