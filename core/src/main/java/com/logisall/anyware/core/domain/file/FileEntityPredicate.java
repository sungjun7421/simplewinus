package com.logisall.anyware.core.domain.file;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@NoArgsConstructor(staticName = "getInstance")
public class FileEntityPredicate {

  private static final QFileEntity Q_ENTITY = QFileEntity.fileEntity;

  private BooleanBuilder builder = new BooleanBuilder();

  public FileEntityPredicate search(String value) {

    if (!StringUtils.isEmpty(value)) {
      value = value.trim();

      builder.and(Q_ENTITY.filename.containsIgnoreCase(value)
          .or(Q_ENTITY.originalFilename.containsIgnoreCase(value))
          .or(Q_ENTITY.mimeType.containsIgnoreCase(value))
      );
    }
    return this;
  }

  public FileEntityPredicate mimeType(final FileEntity.MimeType mimeType) {

    if (mimeType != null) {
      switch (mimeType) {
        case VIDEO:
          builder.and(Q_ENTITY.mimeType.startsWith("video"));
          break;
        case IMAGE:
          builder.and(Q_ENTITY.mimeType.startsWith("image"));
          break;
        case AUDIO:
          builder.and(Q_ENTITY.mimeType.startsWith("audio"));
          break;
      }
    }
    return this;
  }

  public FileEntityPredicate startDate(final LocalDateTime startDate) {

    if (startDate != null) {
      builder.and(Q_ENTITY.createdDate.goe(startDate));
    }
    return this;
  }

  public FileEntityPredicate endDate(final LocalDateTime endDate) {

    if (endDate != null) {
      builder.and(Q_ENTITY.createdDate.loe(endDate));
    }
    return this;
  }

  public Predicate values() {

    return builder.getValue() == null ? builder.and(Q_ENTITY.id.isNotNull()) : builder.getValue();
  }
}
