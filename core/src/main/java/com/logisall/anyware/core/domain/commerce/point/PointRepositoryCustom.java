package com.logisall.anyware.core.domain.commerce.point;



import com.logisall.anyware.core.model.querydsl.commerce.QDPoint;

import java.time.LocalDateTime;
import java.util.List;

public interface PointRepositoryCustom {

  List<QDPoint> listByAvailable(Long idBuyer);

  // 보유포인트
  int total(Long idBuyer);

  // 보유포인트 > date
  int totalFollowDate(Long idBuyer, LocalDateTime date);

  // 만료 예정포인트 (1달전)
  int expireScheduledTotal(Long idBuyer);

  // 오늘 만료 예정인 포인트 리스트
  List<QDPoint> todayExpire();
}
