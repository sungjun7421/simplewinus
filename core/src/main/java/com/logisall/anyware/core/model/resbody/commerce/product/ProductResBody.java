package com.logisall.anyware.core.model.resbody.commerce.product;

import com.logisall.anyware.core.model.BaseResponseBody;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.server.core.Relation;

@Relation(value = "product", collectionRelation = "products")
@Slf4j
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductResBody extends BaseResponseBody {

  private static final long serialVersionUID = 2840263557823070940L;

  private long id;
  private String name;
  private int price;
}
