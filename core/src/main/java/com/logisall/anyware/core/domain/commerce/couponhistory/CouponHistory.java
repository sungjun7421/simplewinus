package com.logisall.anyware.core.domain.commerce.couponhistory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.coupon.Coupon;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.model.reqbody.commerce.coupon.CouponThumbBody;
import com.logisall.anyware.core.utils.DateUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Entity
@Getter
@Setter
@ToString(exclude = {
        "relativeCoupon",
        "relativeBuyer",
})
@NoArgsConstructor
public class CouponHistory extends AbstractEntity<Long> {

  private static final long serialVersionUID = -8947853398992219602L;
  public static String[] IGNORE_PROPERTIES = {
          "id",
          "relativeBuyer",
          "relativeOrderProduct"};

//  @PrePersist
//  public void prePersist() {
//    if (this.relativeBuyer == null
////            || this.relativeCoupon == null
////    ) {
////      throw new CreateErrorException(this.getClass().getName());
////    }
//  }

//  public void CouponHistory(Buyer relativeBuyer, Coupon relativeCoupon) {
//    this.relativeBuyer = relativeBuyer;
//    this.relativeCoupon = relativeCoupon;
//  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @Column(columnDefinition = "BIT(1) default 0")
  private boolean used;

  @DateTimeFormat(pattern = DateUtils.FORMAT_DATE_UNIT_BAR)
  private LocalDate usedDate;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "idBuyer", referencedColumnName = "id") //
  private Buyer relativeBuyer;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "idCoupon", referencedColumnName = "id") //
  private com.logisall.anyware.core.domain.commerce.coupon.Coupon relativeCoupon;

  @JsonIgnore
  @OrderBy("createdDate DESC")
  @ManyToMany(mappedBy = "usedCoupons")
  private List<Order> orders = new ArrayList<>();

  @Override
  public void delete() {

  }

  @Override
  public void lazy() {
    if(orders!=null){
      orders.size();

    }
  }

  public com.logisall.anyware.core.model.reqbody.commerce.coupon.CouponThumbBody toCouponThumbBody(Locale locale) {
    Coupon coupon = this.relativeCoupon;
    coupon.setLocale(locale);

    return CouponThumbBody.builder()
            .id(this.id)
            .name(coupon.getName().getValue())
            .requite(coupon.getRequite() != null ? coupon.getRequite().getValue() : "")
            .type(coupon.getType())
            .code(coupon.getCode())
            .discountFixed(coupon.getDiscountFixed())
            //.discountPercent(coupon.getDiscountPercent())
            .minimumPayment(coupon.getMinimumPayment())
            .startDate(coupon.getStartDate())
            .endDate(coupon.getEndDate())
            .build();
  }
}
