package com.logisall.anyware.core.domain.commerce.order;


import com.logisall.anyware.core.domain.commerce.PayMethod;
import com.logisall.anyware.core.domain.commerce.order.BuyerInfo;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.domain.commerce.order.ShippingInfo;
import com.logisall.anyware.core.model.reqbody.commerce.product.ProductReqBody;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@ToString
@NoArgsConstructor
public abstract class AbstractOrder implements Serializable {

  private static final long serialVersionUID = 5408197639732262815L;

  // 상품 정보
  protected List<ProductReqBody> products; // 결제할 제품군

  // 주문자 정보
  protected BuyerInfo buyer; // 구매자 정보
  protected ShippingInfo shipping; // 배송지 정보

  // 혜택 사용
  protected int point; // 사용 포인트
  protected List<Long> idCoupons; // 사용 쿠폰 ID 목록

  // 결제 정보
  protected int amount; // 결제할 금액
  protected PayMethod payMethod; // 결제 방법

  // 처리
  // protected List<Long> idCarts; // 결제 완료후, 장바구니 삭제할 장바구니 ID

  // 총 수량
  public int getQty() {

    if (this.products != null) {
      return this.products.stream().mapToInt(ProductReqBody::getQty).sum();
    }
    return 0;
  }

  public boolean isBadRequest() {

    return this.payMethod == null
        || this.amount == 0
        || this.products == null
        || products.size() == 0
        ;
  }

  protected abstract Order toOrder();
}
