package com.logisall.anyware.core.domain.board.faq;

import com.logisall.anyware.core.domain.board.faq.Faq;
import com.logisall.anyware.core.domain.board.faq.FaqRepositoryCustom;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface FaqRepository extends
        JpaRepository<Faq, Long>,
        QuerydslPredicateExecutor<Faq>,
        FaqRepositoryCustom {

    @Query("select max(f.orderAscending) from Faq f")
    Long highestOrder();

    @Query("from Faq f where f.orderAscending < ?1 order by f.orderAscending desc")
    List<Faq> previous(long orderAscending, Pageable pageable);

    @Query("from Faq f where f.orderAscending > ?1 order by f.orderAscending asc")
    List<Faq> next(long orderAscending, Pageable pageable);
}
