package com.logisall.anyware.core.service.commerce.point;

import com.logisall.anyware.core.domain.commerce.point.PointSetting;
import com.logisall.anyware.core.domain.commerce.point.PointSettingRepository;
import com.logisall.anyware.core.service.commerce.point.PointSettingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class PointSettingServiceImpl implements PointSettingService {

  @Autowired
  private PointSettingRepository pointSettingRepository;


  @Override
  @Transactional
  public PointSetting setting(PointSetting setting) {

    if (setting.getId() == null) {
      return pointSettingRepository.save(setting);
    } else {
      PointSetting ori = pointSettingRepository.getById(setting.getId());

      if (ori != null) {
        BeanUtils.copyProperties(setting, ori, PointSetting.IGNORE_PROPERTIES);
        return pointSettingRepository.save(ori);
      } else {
        return pointSettingRepository.save(setting);
      }
    }
  }

  @Override
  @Transactional
  public PointSetting get() {
    PointSetting first = pointSettingRepository.findFist();
    if (first != null) {
      return first;
    }
    PointSetting setting = PointSetting.initialData(true);
    return pointSettingRepository.save(setting);
  }
}
