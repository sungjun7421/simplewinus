package com.logisall.anyware.core.domain.document;

import com.logisall.anyware.core.domain.document.Document;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@NoArgsConstructor(staticName = "getInstance")
public class DocumentPredicate {

  private static final QDocument Q_ENTITY = QDocument.document;

  private BooleanBuilder builder = new BooleanBuilder();

  public com.logisall.anyware.core.domain.document.DocumentPredicate search(String value) {

    if (!StringUtils.isEmpty(value)) {
      value = value.trim();

      builder.and(Q_ENTITY.title.textEnUs.containsIgnoreCase(value)
          .or(Q_ENTITY.title.textJaJp.containsIgnoreCase(value))
          .or(Q_ENTITY.title.textKoKr.containsIgnoreCase(value))
          .or(Q_ENTITY.title.textZhCn.containsIgnoreCase(value))
          .or(Q_ENTITY.title.textZhTw.containsIgnoreCase(value))

          .or(Q_ENTITY.content.textEnUs.containsIgnoreCase(value))
          .or(Q_ENTITY.content.textJaJp.containsIgnoreCase(value))
          .or(Q_ENTITY.content.textKoKr.containsIgnoreCase(value))
          .or(Q_ENTITY.content.textZhCn.containsIgnoreCase(value))
          .or(Q_ENTITY.content.textZhTw.containsIgnoreCase(value))
      );
    }
    return this;
  }

  public com.logisall.anyware.core.domain.document.DocumentPredicate type(final Document.Type type) {

    if (type != null) {
      builder.and(Q_ENTITY.type.eq(type));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.document.DocumentPredicate startDate(final LocalDateTime startDate) {

    if (startDate != null) {
      builder.and(Q_ENTITY.createdDate.goe(startDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.document.DocumentPredicate endDate(final LocalDateTime endDate) {

    if (endDate != null) {
      builder.and(Q_ENTITY.createdDate.loe(endDate));
    }
    return this;
  }

  public Predicate values() {

    return builder.getValue() == null ? builder.and(Q_ENTITY.id.isNotNull()) : builder.getValue();
  }
}
