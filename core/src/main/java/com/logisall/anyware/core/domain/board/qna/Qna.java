package com.logisall.anyware.core.domain.board.qna;

import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.RestEntityBody;
import com.logisall.anyware.core.domain.board.qna.Answer;
import com.logisall.anyware.core.domain.board.qna.QnaNoMember;
import com.logisall.anyware.core.domain.board.qna.category.QnaCategory;
import com.logisall.anyware.core.domain.file.FileEntity;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.model.resbody.board.QnaResBody;
import com.logisall.anyware.core.utils.HtmlUtils;
import com.logisall.anyware.core.utils.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 1:1 문의하기
 */
@Slf4j
@Entity(name = "QuestionsAndAnswers")
@Getter
@Setter
@ToString(exclude = {
    "relativeUser", "files", "categories"
})
@NoArgsConstructor
public class Qna extends AbstractEntity<Long> implements RestEntityBody<QnaResBody> {

  private static final long serialVersionUID = 6328325247234411688L;

  public static String[] IGNORE_PROPERTIES = {
      "id",
      "relativeUser",
      "qnaNoMember"};

  @PrePersist
  public void prePersist() {

    if (StringUtils.isNotEmpty(this.content)) {
      this.content = HtmlUtils.convertLineSeparatorToBrTag(this.content);
    }
  }

  @PreUpdate
  public void preUpdate() {

    if (StringUtils.isNotEmpty(this.content)) {
      this.content = HtmlUtils.convertLineSeparatorToBrTag(this.content);
    }
  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @Column(length = 100)
  private String title;

  @Lob
  @Column(columnDefinition = "TEXT", length = 65535)
  private String content;

  // == IMAGE
  @JsonIgnore
  @ManyToMany()
  @JoinTable(name = "QnaToFile",
      joinColumns = @JoinColumn(name = "idQna", nullable = false, foreignKey = @ForeignKey(name = "FK_Qna_For_File_To_Cate")),
      inverseJoinColumns = @JoinColumn(name = "idFile", nullable = false, foreignKey = @ForeignKey(name = "FK_File_For_File_To_Cate"))
  )
  private List<FileEntity> files = new ArrayList<>();

  public void uploadFiles() {
    if (this.files != null) {
      this.files.removeIf(file -> StringUtils.isEmpty(file.getUrl()));
    }
  }

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "idUser", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_User_For_Qna")) // Column name, 참조하는 ID(pk) name
  private User relativeUser; // 작성자

  @Column(name = "qMember", columnDefinition = "BIT(1) default 0")
  private boolean member; // 회원 여부

  @Column(columnDefinition = "BIT(1) default 1")
  private boolean active; // 활성/비활성

  @Column(name = "deleteRow", columnDefinition = "BIT(1) default 0")
  private boolean delete; // 활성/비활성

  @Column(length = 20)
  private String ipAddress;

  @Embedded
  private Answer answer;

  @Embedded
  private QnaNoMember qnaNoMember;

  @JsonIgnore
  @OrderBy("createdDate ASC")
  @ManyToMany
  @JoinTable(name = "QnaToCategory",
      joinColumns = @JoinColumn(name = "idQna", nullable = false, foreignKey = @ForeignKey(name = "FK_Qna_For_Qna_To_Cate")),
      inverseJoinColumns = @JoinColumn(name = "idCategory", nullable = false, foreignKey = @ForeignKey(name = "FK_Category_For_Qna_To_Cate"))
  )
  private List<QnaCategory> categories = new ArrayList<>(); // 카테고리

  @Transient
  private Long idCategory;

  public void setLocale(Locale locale) {
    if (this.categories != null) {
      this.categories.forEach(category -> {
        category.setLocale(locale);
      });
    }
  }

  public boolean getHasAnswer() {
    return this.answer != null && StringUtils.isNotEmpty(this.answer.getContent());
  }

  @Override
  public void delete() {

  }

  @Override
  public void lazy() {
    if (this.relativeUser != null) {
      this.relativeUser.getId();
    }
    if (this.categories != null) {
      this.categories.size();
    }
    if (this.files != null) {
      this.files.size();
    }
  }

  @Override
  public QnaResBody toBody(Locale locale) {
    this.setLocale(locale);
    return QnaResBody.builder()
        .id(this.id)
        .title(this.title)
        .content(this.content)
        .answer(this.answer)
        .files(this.files != null ? this.files.stream().map(fileEntity -> {
          try {
            return fileEntity.toFileMeta();
          } catch (MalformedURLException e) {
            e.printStackTrace();
          }
          return null;
        }).filter(Objects::nonNull).collect(Collectors.toList()) : null)
        .regTime(this.createdDate)
        .idUser(this.member && this.relativeUser != null ? this.relativeUser.getId() : null)
        .fullName(this.member && this.relativeUser != null ? this.relativeUser.getFullName() : (this.qnaNoMember != null ? this.qnaNoMember.getFullName() : null))
        .categories(this.categories != null ? this.categories.stream().map(category -> category.toBody(locale)).collect(Collectors.toList()) : null)
        .build();
  }
}
