package com.logisall.anyware.core.service.niceid;

import NiceID.Check.CPClient;
import com.logisall.anyware.core.model.property.niceid.*;
import com.logisall.anyware.core.service.niceid.NiceIdService;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Slf4j
@Service
public class NiceIdServiceImpl implements NiceIdService {

  @Autowired
  private CPClient niceCheck;

  @Autowired
  private NiceIdKey key;

  @Autowired
  private NiceIdConfigProperty property;


  @Override
  public NiceIdReturn check() {
    log.debug("niceCheck : {}", niceCheck);

    final String sSiteCode = key.getSiteCode();      // NICE로부터 부여받은 사이트 코드
    final String sSitePassword = key.getSitePassword();    // NICE로부터 부여받은 사이트 패스워드

    log.debug("sSiteCode : {}", sSiteCode);
    log.debug("sSitePassword : {}", sSitePassword);

    String sRequestNumber = StringUtils.getRandomStr();          // 요청 번호, 이는 성공/실패후에 같은 값으로 되돌려주게 되므로
    // 업체에서 적절하게 변경하여 쓰거나, 아래와 같이 생성한다.
    sRequestNumber = niceCheck.getRequestNO(sSiteCode);
    // session.setAttribute("REQ_SEQ" , sRequestNumber);	// 해킹등의 방지를 위하여 세션을 쓴다면, 세션에 요청번호를 넣는다.

    String sAuthType = property.getAuthType();        // 없으면 기본 선택화면, M: 핸드폰, C: 신용카드, X: 공인인증서

    String popgubun = property.getPopgubun();    //Y : 취소버튼 있음 / N : 취소버튼 없음
    String customize = property.getCustomize();    //없으면 기본 웹페이지 / Mobile : 모바일페이지

    String sGender = property.getGender();      //없으면 기본 선택 값, 0 : 여자, 1 : 남자

    // CheckPlus(본인인증) 처리 후, 결과 데이타를 리턴 받기위해 다음예제와 같이 http부터 입력합니다.
    //리턴url은 인증 전 인증페이지를 호출하기 전 url과 동일해야 합니다. ex) 인증 전 url : http://www.~ 리턴 url : http://www.~
    String sReturnUrl = property.getReturnUrl();      // 성공시 이동될 URL
    String sErrorUrl = property.getErrorUrl();          // 실패시 이동될 URL

    // 입력될 plain 데이타를 만든다.
    String sPlainData = "7:REQ_SEQ" + sRequestNumber.getBytes().length + ":" + sRequestNumber +
        "8:SITECODE" + sSiteCode.getBytes().length + ":" + sSiteCode +
        "9:AUTH_TYPE" + sAuthType.getBytes().length + ":" + sAuthType +
        "7:RTN_URL" + sReturnUrl.getBytes().length + ":" + sReturnUrl +
        "7:ERR_URL" + sErrorUrl.getBytes().length + ":" + sErrorUrl +
        "11:POPUP_GUBUN" + popgubun.getBytes().length + ":" + popgubun +
        "9:CUSTOMIZE" + customize.getBytes().length + ":" + customize +
        "6:GENDER" + sGender.getBytes().length + ":" + sGender;

    String sMessage = "";
    String sEncData = "";

    int iReturn = niceCheck.fnEncode(sSiteCode, sSitePassword, sPlainData);
    if (iReturn == 0) {
      sEncData = niceCheck.getCipherData();
    } else if (iReturn == -1) {
      sMessage = "암호화 시스템 에러입니다.";
    } else if (iReturn == -2) {
      sMessage = "암호화 처리오류입니다.";
    } else if (iReturn == -3) {
      sMessage = "암호화 데이터 오류입니다.";
    } else if (iReturn == -9) {
      sMessage = "입력 데이터 오류입니다.";
    } else {
      sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
    }

    return NiceIdReturn.builder()
        .message(sMessage)
        .encData(sEncData)
        .requestNumber(sRequestNumber)
        .build();

    /**
     * <html>
     * <head>
     * 	<title>NICE평가정보 - CheckPlus 안심본인인증 테스트</title>
     *
     * 	<script language='javascript'>
     * 	window.name ="Parent_window";
     *
     * 	function fnPopup(){
     * 		window.open('', 'popupChk', 'width=500, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
     * 		document.form_chk.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
     * 		document.form_chk.target = "popupChk";
     * 		document.form_chk.submit();
     *    }
     * 	</script>
     * </head>
     * <body>
     * 	<%= sMessage %><br><br>
     * 	업체정보 암호화 데이타 : [<%= sEncData %>]<br><br>
     *
     * 	<!-- 본인인증 서비스 팝업을 호출하기 위해서는 다음과 같은 form이 필요합니다. -->
     * 	<form name="form_chk" method="post">
     * 		<input type="hidden" name="m" value="checkplusService">						<!-- 필수 데이타로, 누락하시면 안됩니다. -->
     * 		<input type="hidden" name="EncodeData" value="<%= sEncData %>">		<!-- 위에서 업체정보를 암호화 한 데이타입니다. -->
     *
     * 		<a href="javascript:fnPopup();"> CheckPlus 안심본인인증 Click</a>
     * 	</form>
     * </body>
     * </html>
     */
  }

  @Override
  public NiceIdSuccess success(String encodeData, String requestNumber) {

    encodeData = this.requestReplace(encodeData, "encodeData");

    NiceIdSuccess result = new NiceIdSuccess();

    String sSiteCode = "";        // NICE로부터 부여받은 사이트 코드
    String sSitePassword = "";      // NICE로부터 부여받은 사이트 패스워드

    String sCipherTime = "";      // 복호화한 시간
    String sRequestNumber = "";      // 요청 번호
    String sResponseNumber = "";    // 인증 고유번호
    String sAuthType = "";        // 인증 수단
    String sName = "";          // 성명
    String sDupInfo = "";        // 중복가입 확인값 (DI_64 byte)
    String sConnInfo = "";        // 연계정보 확인값 (CI_88 byte)
    String sBirthDate = "";        // 생년월일(YYYYMMDD)
    String sGender = "";        // 성별
    String sNationalInfo = "";      // 내/외국인정보 (개발가이드 참조)
    String sMobileNo = "";        // 휴대폰번호
    String sMobileCo = "";        // 통신사
    String sMessage = "";
    String sPlainData = "";

    int iReturn = niceCheck.fnDecode(sSiteCode, sSitePassword, encodeData);

    if (iReturn == 0) {
      result.setSuccess(true);
      sPlainData = niceCheck.getPlainData();
      sCipherTime = niceCheck.getCipherDateTime();

      // 데이타를 추출합니다.
      HashMap mapresult = niceCheck.fnParse(sPlainData);

      sRequestNumber = (String) mapresult.get("REQ_SEQ");
      sResponseNumber = (String) mapresult.get("RES_SEQ");
      sAuthType = (String) mapresult.get("AUTH_TYPE");
      sName = (String) mapresult.get("NAME");
      //sName			= (String)mapresult.get("UTF8_NAME"); //charset utf8 사용시 주석 해제 후 사용
      sBirthDate = (String) mapresult.get("BIRTHDATE");
      sGender = (String) mapresult.get("GENDER");
      sNationalInfo = (String) mapresult.get("NATIONALINFO");
      sDupInfo = (String) mapresult.get("DI");
      sConnInfo = (String) mapresult.get("CI");
      sMobileNo = (String) mapresult.get("MOBILE_NO");
      sMobileCo = (String) mapresult.get("MOBILE_CO");

      if (!sRequestNumber.equals(requestNumber)) {
        sMessage = "세션값 불일치 오류입니다.";
        sResponseNumber = "";
        sAuthType = "";
      }

      result.setCipherTime(sCipherTime);
      result.setRequestNumber(sRequestNumber);
      result.setResponseNumber(sResponseNumber);
      result.setAuthType(sAuthType);
      result.setName(sName);
      result.setBirthDate(sBirthDate);
      result.setGender(sGender);
      result.setNationalInfo(sNationalInfo);
      result.setDupInfo(sDupInfo);
      result.setConnInfo(sConnInfo);
      result.setMobileCo(sMobileCo);
      result.setMobileNo(sMobileNo);

    } else if (iReturn == -1) {
      sMessage = "복호화 시스템 오류입니다.";
    } else if (iReturn == -4) {
      sMessage = "복호화 처리 오류입니다.";
    } else if (iReturn == -5) {
      sMessage = "복호화 해쉬 오류입니다.";
    } else if (iReturn == -6) {
      sMessage = "복호화 데이터 오류입니다.";
    } else if (iReturn == -9) {
      sMessage = "입력 데이터 오류입니다.";
    } else if (iReturn == -12) {
      sMessage = "사이트 패스워드 오류입니다.";
    } else {
      sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
    }

    result.setMessage(sMessage);
    return result;
  }

  @Override
  public NiceIdFail fail(String encodeData) {

    encodeData = this.requestReplace(encodeData, "encodeData");

    String sSiteCode = "";        // NICE로부터 부여받은 사이트 코드
    String sSitePassword = "";      // NICE로부터 부여받은 사이트 패스워드

    String sCipherTime = "";      // 복호화한 시간
    String sRequestNumber = "";      // 요청 번호
    String sErrorCode = "";        // 인증 결과코드
    String sAuthType = "";        // 인증 수단
    String sMessage = "";
    String sPlainData = "";

    int iReturn = niceCheck.fnDecode(sSiteCode, sSitePassword, encodeData);

    if (iReturn == 0) {
      sPlainData = niceCheck.getPlainData();
      sCipherTime = niceCheck.getCipherDateTime();

      // 데이타를 추출합니다.
      HashMap mapresult = niceCheck.fnParse(sPlainData);

      sRequestNumber = (String) mapresult.get("REQ_SEQ");
      sErrorCode = (String) mapresult.get("ERR_CODE");
      sAuthType = (String) mapresult.get("AUTH_TYPE");
    } else if (iReturn == -1) {
      sMessage = "복호화 시스템 에러입니다.";
    } else if (iReturn == -4) {
      sMessage = "복호화 처리오류입니다.";
    } else if (iReturn == -5) {
      sMessage = "복호화 해쉬 오류입니다.";
    } else if (iReturn == -6) {
      sMessage = "복호화 데이터 오류입니다.";
    } else if (iReturn == -9) {
      sMessage = "입력 데이터 오류입니다.";
    } else if (iReturn == -12) {
      sMessage = "사이트 패스워드 오류입니다.";
    } else {
      sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
    }
    return NiceIdFail.builder()
        .cipherTime(sCipherTime)
        .message(sMessage)
        .errorCode(sErrorCode)
        .authType(sAuthType)
        .requestNumber(sRequestNumber)
        .build();
  }

  @Override
  public String requestReplace(String paramValue, String gubun) {

    String result = "";

    if (paramValue != null) {

      paramValue = paramValue.replaceAll("<", "&lt;").replaceAll(">", "&gt;");

      paramValue = paramValue.replaceAll("\\*", "");
      paramValue = paramValue.replaceAll("\\?", "");
      paramValue = paramValue.replaceAll("\\[", "");
      paramValue = paramValue.replaceAll("\\{", "");
      paramValue = paramValue.replaceAll("\\(", "");
      paramValue = paramValue.replaceAll("\\)", "");
      paramValue = paramValue.replaceAll("\\^", "");
      paramValue = paramValue.replaceAll("\\$", "");
      paramValue = paramValue.replaceAll("'", "");
      paramValue = paramValue.replaceAll("@", "");
      paramValue = paramValue.replaceAll("%", "");
      paramValue = paramValue.replaceAll(";", "");
      paramValue = paramValue.replaceAll(":", "");
      paramValue = paramValue.replaceAll("-", "");
      paramValue = paramValue.replaceAll("#", "");
      paramValue = paramValue.replaceAll("--", "");
      paramValue = paramValue.replaceAll("-", "");
      paramValue = paramValue.replaceAll(",", "");

      if (gubun != "encodeData") {
        paramValue = paramValue.replaceAll("\\+", "");
        paramValue = paramValue.replaceAll("/", "");
        paramValue = paramValue.replaceAll("=", "");
      }

      result = paramValue;

    }
    return result;
  }
}
