package com.logisall.anyware.core.service.niceid;

import com.logisall.anyware.core.model.property.niceid.NiceIdFail;
import com.logisall.anyware.core.model.property.niceid.NiceIdSuccess;
import com.logisall.anyware.core.model.property.niceid.NiceIdReturn;

public interface NiceIdService {

  NiceIdReturn check();

  NiceIdSuccess success(String encodeData, String requestNumber);
  NiceIdFail fail(String encodeData);
  String requestReplace(String paramValue, String gubun);
}
