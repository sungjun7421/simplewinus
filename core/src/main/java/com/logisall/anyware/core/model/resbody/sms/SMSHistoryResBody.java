package com.logisall.anyware.core.model.resbody.sms;

import com.logisall.anyware.core.domain.sms.SMSHistory;
import com.logisall.anyware.core.model.BaseResponseBody;
import com.logisall.anyware.core.vo.SMS;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;


@Relation(value = "sms", collectionRelation = "smses")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SMSHistoryResBody extends BaseResponseBody {

  private static final long serialVersionUID = -5811053401443310437L;

  private Long id;
  private SMS.Type type;
  private SMSHistory.Code code;
//  private boolean verifySMS;
  private String sendTime;
  private String destPhone;
  private String destName;
  private String subject;
  private String msgBody;
  private String fullName;
  private String email;
  private String mobile;
}
