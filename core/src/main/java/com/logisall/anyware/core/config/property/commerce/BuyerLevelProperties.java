package com.logisall.anyware.core.config.property.commerce;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Component
@ConfigurationProperties(prefix = "app.shop.buyer-level")
public class BuyerLevelProperties implements Serializable {

  private static final long serialVersionUID = 1358602600424372017L;

  private List<Map<String, Object>> samples;
}
