package com.logisall.anyware.core.domain.file;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.RestEntityBody;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.model.file.FileMeta;
import com.logisall.anyware.core.model.resbody.common.FileResBody;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

@Entity(name = "File")
@Getter
@Setter
@ToString(exclude = {"relativeUser"})
public class FileEntity extends AbstractEntity<Long> implements RestEntityBody<com.logisall.anyware.core.model.resbody.common.FileResBody> {

  private static final long serialVersionUID = -5683577710431281569L;

  public static String[] IGNORE_PROPERTIES = {"id"};

  public enum MimeType {
    IMAGE,
    VIDEO,
    AUDIO,
    UNkNOWN
  }

  @PrePersist
  public void prePersist() {
  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @Column
  private String url; // /images/a.jpg

  @Column
  private String filename;

  @Column
  private String originalFilename;

  @Column(length = 50)
  private String mimeType;

  public MimeType getMimeTypeEnum() {

    if (isVideo()) {
      return MimeType.VIDEO;
    } else if (isAudio()) {
      return MimeType.AUDIO;
    } else if (isImage()) {
      return MimeType.IMAGE;
    }
    return MimeType.UNkNOWN;
  }

  public String getMimeType() {
    if (StringUtils.isEmpty(this.mimeType)) {
      return "application/octet-stream";
    }
    return mimeType;
  }

  public boolean isVideo() {
    if (!StringUtils.isEmpty(mimeType)) {
      return this.mimeType.startsWith("video");
    }
    return false;
  }

  public boolean isImage() {
    if (!StringUtils.isEmpty(mimeType)) {
      return this.mimeType.startsWith("image");
    }
    return false;
  }

  public boolean isAudio() {
    if (!StringUtils.isEmpty(mimeType)) {
      return this.mimeType.startsWith("audio");
    }
    return false;
  }

  @Column(columnDefinition = "BIGINT(20) default 0")
  private long size;


  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "idUser", referencedColumnName = "id") // Column name, 참조하는 ID(pk) name
  private User relativeUser; // 보낸사람

  @Override
  public void delete() {

  }

  @Override
  public void lazy() {

  }

  @Override
  public com.logisall.anyware.core.model.resbody.common.FileResBody toBody(Locale locale) {
    com.logisall.anyware.core.model.resbody.common.FileResBody fileResBody = new FileResBody();
    fileResBody.setId(this.id);
    fileResBody.setUrl(this.url);
    fileResBody.setFilename(this.filename);
    fileResBody.setOriginalFilename(this.originalFilename);
    fileResBody.setMimeType(this.mimeType);
    fileResBody.setSize(this.size);
    return fileResBody;
  }

  public com.logisall.anyware.core.model.file.FileMeta toFileMeta() throws MalformedURLException {
    com.logisall.anyware.core.model.file.FileMeta fileMeta = new FileMeta();
    fileMeta.setId(this.id);
    fileMeta.setUrl(StringUtils.isEmpty(url) ? null : new URL(url));
    fileMeta.setOriginalFilename(this.originalFilename);
    fileMeta.setFilename(this.filename);
    fileMeta.setMimeType(this.mimeType);
    fileMeta.setSize(this.size);
    return fileMeta;
  }
}
