package com.logisall.anyware.core.domain.board.faq;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Objects;

@NoArgsConstructor(staticName = "getInstance")
public class FaqPredicate {

  private static final QFaq Q_ENTITY = QFaq.faq;

  private BooleanBuilder builder = new BooleanBuilder();

  public Predicate values() {
    return builder.getValue() == null ? builder.and(Q_ENTITY.id.isNotNull()) : builder.getValue();
  }

  public com.logisall.anyware.core.domain.board.faq.FaqPredicate startDate(final LocalDateTime startDate) {

    if (startDate != null) {
      builder.and(Q_ENTITY.createdDate.goe(startDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.faq.FaqPredicate endDate(final LocalDateTime endDate) {

    if (endDate != null) {
      builder.and(Q_ENTITY.createdDate.loe(endDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.faq.FaqPredicate search(String value) {

    if (!StringUtils.isEmpty(value)) {
      value = value.trim();

      builder.and(Q_ENTITY.question.textEnUs.containsIgnoreCase(value)
          .or(Q_ENTITY.question.textJaJp.containsIgnoreCase(value))
          .or(Q_ENTITY.question.textKoKr.containsIgnoreCase(value))
          .or(Q_ENTITY.question.textZhCn.containsIgnoreCase(value))
          .or(Q_ENTITY.question.textZhTw.containsIgnoreCase(value))
          .or(Q_ENTITY.answer.textEnUs.containsIgnoreCase(value))
          .or(Q_ENTITY.answer.textJaJp.containsIgnoreCase(value))
          .or(Q_ENTITY.answer.textKoKr.containsIgnoreCase(value))
          .or(Q_ENTITY.answer.textZhCn.containsIgnoreCase(value))
          .or(Q_ENTITY.answer.textZhTw.containsIgnoreCase(value)));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.faq.FaqPredicate id(final Long id) {

    if (id != null && id != 0L) {
      builder.and(Q_ENTITY.id.eq(id));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.faq.FaqPredicate active(final Boolean isActive) {

    if (isActive != null) {
      builder.and(Q_ENTITY.active.eq(isActive));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.faq.FaqPredicate category(final Long idCategory) {

    if (idCategory != null && idCategory != 0L) {
      builder.and(Q_ENTITY.categories.any().id.eq(idCategory));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.faq.FaqPredicate categories(final Long[] idCategories) {

    if (idCategories != null && idCategories.length > 0) {
      builder.and(Q_ENTITY.categories.any().id.in(idCategories));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.board.faq.FaqPredicate locale(Locale locale, final Locale defaultLocale) {

    if (!Objects.equals(locale, Locale.KOREA)
        && !Objects.equals(locale, Locale.US)
        && !Objects.equals(locale, Locale.CHINA)
        && !Objects.equals(locale, Locale.TAIWAN)
        && !Objects.equals(locale, Locale.JAPAN)) {
      locale = defaultLocale;
    }

    if (Objects.equals(locale, Locale.KOREA)) {
      builder.and(Q_ENTITY.internationalMode.koKr.eq(true));
    } else if (Objects.equals(locale, Locale.US)) {
      builder.and(Q_ENTITY.internationalMode.enUs.eq(true));
    } else if (Objects.equals(locale, Locale.CHINA)) {
      builder.and(Q_ENTITY.internationalMode.zhCn.eq(true));
    } else if (Objects.equals(locale, Locale.TAIWAN)) {
      builder.and(Q_ENTITY.internationalMode.zhTw.eq(true));
    } else if (Objects.equals(locale, Locale.JAPAN)) {
      builder.and(Q_ENTITY.internationalMode.jaJp.eq(true));
    }
    return this;
  }
}
