package com.logisall.anyware.core.model.sns;

import com.logisall.anyware.core.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class Apple implements Serializable {
  private static final long serialVersionUID = -228901395473542280L;

  private String id; // sub
  private String email; // email

  public static Apple of(String id, String email) {

    Apple apple = new Apple();
    apple.setId(id);

    if (StringUtils.isNotEmpty(email)) {
      apple.setEmail(email);
    }

    return apple;
  }
}
