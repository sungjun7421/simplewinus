package com.logisall.anyware.core.service.board.comment;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.crud.UpdateErrorException;
import com.logisall.anyware.core.domain.board.comment.Comment;
import com.logisall.anyware.core.domain.board.comment.CommentPredicate;
import com.logisall.anyware.core.domain.board.comment.CommentRepository;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.account.user.UserService;
import com.logisall.anyware.core.service.setting.AppSettingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

@Slf4j
@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private AppSettingService appSettingService;

    @Override
    @Transactional
    public com.logisall.anyware.core.domain.board.comment.Comment create(com.logisall.anyware.core.domain.board.comment.Comment comment) {
        User user = userService.get("super@super.com");
        comment.setRelativeUser(user);
        return commentRepository.save(comment);
    }

    @Override
    @Transactional
    public com.logisall.anyware.core.domain.board.comment.Comment update(com.logisall.anyware.core.domain.board.comment.Comment comment) {

        if (comment.getId() == null) {
            throw new BadRequestException();
        }

        return commentRepository.findById(comment.getId())
                .map(ori -> {
                    BeanUtils.copyProperties(comment, ori, com.logisall.anyware.core.domain.board.comment.Comment.IGNORE_PROPERTIES);
                    return commentRepository.save(ori);
                }).orElseThrow(() -> new UpdateErrorException(comment.getId(), com.logisall.anyware.core.domain.board.comment.Comment.class.getName()));
    }

    @Override
    @Transactional
    public void delete(Long id) {
        commentRepository.findById(id)
                .ifPresent(comment -> {
                    comment.delete();
                    commentRepository.delete(comment);
                });
    }

    @Override
    @Transactional(readOnly = true)
    public com.logisall.anyware.core.domain.board.comment.Comment get(Locale locale, Long id) {
        return commentRepository.findById(id)
                .map(comment -> {
                    comment.lazy();
                    return comment;
                }).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<com.logisall.anyware.core.domain.board.comment.Comment> page(Locale locale, com.logisall.anyware.core.model.Filter filter) {
        Page<com.logisall.anyware.core.domain.board.comment.Comment> page = commentRepository.findAll(
                com.logisall.anyware.core.domain.board.comment.CommentPredicate.getInstance()
                        .search(filter.getQuery())
                        .startDate(filter.getStartDate())
                        .endDate(filter.getEndDate())
                        .values(),
                filter.getPageable());

        page.forEach(comment -> {
            comment.lazy();
//      comment.setLocale(locale);
        });
        return page;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<com.logisall.anyware.core.domain.board.comment.Comment> page(Filter filter,
                                                                             Long idUser,
                                                                             com.logisall.anyware.core.domain.board.comment.Comment.Type type,
                                                                             Long idContent,
                                                                             Long idParent,
                                                                             Boolean active) {

        Page<Comment> page = commentRepository.findAll(
                CommentPredicate.getInstance()

                        .search(filter.getQuery())
                        .startDate(filter.getStartDate())
                        .endDate(filter.getEndDate())

                        .relative(type, idContent)
                        .user(idUser)
                        .parent(idParent)

                        .active(active)
                        .type(type)
                        .values(),
                filter.getPageable());

        page.forEach(comment -> {
            comment.lazy();
        });
        return page;
    }

}
