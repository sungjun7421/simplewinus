package com.logisall.anyware.core.model.property.kko;

import com.logisall.anyware.core.utils.StringUtils;
import lombok.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class KKO implements Serializable {

    private static final long serialVersionUID = -7899844023293714302L;

    @Builder
    public KKO(String phone, String message, String templateCode, FailedType failedType, String failedSubject, String failedMsg, String btnTypes, String btnTxts, String btnUrls1, String btnUrls2) {
        this.phone = phone;
        this.message = message;
        this.templateCode = templateCode;
        this.failedType = failedType;
        this.failedSubject = failedSubject;
        this.failedMsg = failedMsg;
        this.btnTypes = btnTypes;
        this.btnTxts = btnTxts;
        this.btnUrls1 = btnUrls1;
        this.btnUrls2 = btnUrls2;
    }

    @Getter
    public enum FailedType {
        SMS("SMS"),
        LMS("LMS"),
        N("전송안함");

        private final String value;

        FailedType(final String value) {
            this.value = value;
        }
    }

    @Getter
    public enum TemplateStatus {
        ADD("등록"),
        REQUEST("검수요청"),
        PASSED("승인"),
        RETURN("반려"),
        STOP("승인중단");

        private final String value;

        TemplateStatus(final String value) {
            this.value = value;
        }
    }

    private String phone; // 수신할 핸드폰번호
    private String reqDate; // 발송시간 (없을 경우 즉시발송 )
    private String message; // 전송할 메시지 (승인된 템플릿의 내용과 동일하게 작성)
    private String templateCode; // 카카오 알림톡 템플릿코드

    private FailedType failedType; // 카카오알림톡 전송 실패 시 전송할 메시지 타입
    private String failedSubject; // 카카오알림톡 전송 실패 시 전송할 제목 (SMS 미사용)
    private String failedMsg; // 카카오알림톡 전송 실패 시 전송할 내용

    private String btnTypes; // 카카오 알림톡 버튼타입 (웹링크, 앱링크, 봇키워드, 메시지전달, 배송조회), 최대 5개까지 추가 가능하며 타입별 콤마로 구분함. (승인된 템플릿과 불일치시 전송실패)
    private String btnTxts; // 카카오 알림톡 버튼이름 (BNT_TYPES 입력 순으로 버튼이름 TEXT 입력, 승인된 템플릿과 불일치시 전송실패), TEXT별 콤마로 구분함. (승인된 템플릿과 불일치시 전송실패)
    private String btnUrls1; // 알림톡 버튼링크주소 URL. 버튼 URL 버튼이2개 이상일 경우 콤마로 구분함. 웹링크, 앱링크는 URL이 필수이며, 기타(배송조회, 봇키워드, 메시지전달)은 null 값임. (승인된 템플릿과 불일치시 전송실패)
    private String btnUrls2; // 알림톡 버튼링크주소 URL. 버튼 1개에 링크가2개인 경우 BTN_URLS2 파라미터 사용. 버튼이 2개 이상일 경우 콤마로 구분함. (승인된 템플릿과 불일치시 전송실패)

    public MultiValueMap<String, String> getMultiValueMap() {
        MultiValueMap<String, String> result = new LinkedMultiValueMap<>();

        result.add("PHONE", this.phone);
        result.add("MSG", this.message);
        result.add("TEMPLATE_CODE", this.templateCode);
        result.add("FAILED_TYPE", this.failedType.name());
        result.add("BTN_TYPES", this.btnTypes);
        result.add("BTN_TXTS", this.btnTxts);
        result.add("BTN_URLS1", this.btnUrls1);
        result.add("BTN_URLS2", this.btnUrls2);

        if (!Objects.equals(this.failedType, FailedType.N)) {
            if (Objects.equals(this.failedType, FailedType.LMS)) {
                result.add("FAILED_SUBJECT", this.failedSubject);
            }
            result.add("FAILED_MSG", this.failedMsg);
        }

        if (StringUtils.isNotEmpty(this.reqDate)) {
            result.add("REQDATE", this.reqDate);
        }

        return result;
    }

    public static final String RESULT_0 = "카카오알림톡_전달 (성공)";
    public static final String RESULT_1 = "카카오알림톡_발신 프로필 키가 유효하지않음";
    public static final String RESULT_2 = "카카오알림톡_서버와 연결되어있지않은 사용자";
    public static final String RESULT_5 = "카카오알림톡_메시지 발송 후 수신여부 불투명";
    public static final String RESULT_6 = "카카오알림톡_메시지 전송결과를 찾을 수 없음";
    public static final String RESULT_8 = "카카오알림톡_메시지를 전송할 수 없는 상태";
    public static final String RESULT_9 = "카카오알림톡_최근 카카오톡을 미사용자";
    public static final String RESULT_a = "카카오알림톡_건수 부족";
    public static final String RESULT_b = "카카오알림톡_전송 권한 없음";
    public static final String RESULT_c = "카카오알림톡_중복된 키 접수 차단";
    public static final String RESULT_d = "카카오알림톡_중복된 수신번호 접수 차단";
    public static final String RESULT_e = "카카오알림톡_서버 내부 에러";
    public static final String RESULT_f = "카카오알림톡_메시지 포맷 에러";
    public static final String RESULT_k = "카카오알림톡_메시지가존재하지않음";
    public static final String RESULT_o = "카카오알림톡_TIME OUT 처리(Agent 내부)";
    public static final String RESULT_p = "카카오알림톡_메시지본문 중복 차단(Agent 내부)";
    public static final String RESULT_t = "카카오알림톡_메시지가비어있음";
    public static final String RESULT_A = "카카오알림톡_카카오톡을 미사용자";
    public static final String RESULT_B = "카카오알림톡_알림톡 차단을 선택한 사용자";
    public static final String RESULT_C = "카카오알림톡_메시지 일련번호 중복";
    public static final String RESULT_D = "카카오알림톡_5초 이내 메시지 중복 발송";
    public static final String RESULT_E = "카카오알림톡_미지원 클라이언트 버전";
    public static final String RESULT_F = "카카오알림톡_기타 오류";
    public static final String RESULT_H = "카카오알림톡_카카오 시스템 오류";
    public static final String RESULT_I = "카카오알림톡_전화번호 오류";
    public static final String RESULT_J = "카카오알림톡_050안심번호 발송불가";
    public static final String RESULT_L = "카카오알림톡_메시지 길이 제한 오류";
    public static final String RESULT_M = "카카오알림톡_템플릿을 찾을 수 없음";
    public static final String RESULT_S = "카카오알림톡_발신번호 검증 오류";
    public static final String RESULT_U = "카카오알림톡_메시지가 템플릿과 일치하지않음";
    public static final String RESULT_V = "카카오알림톡_메시지가 템플릿과 비교 실패";

    public static final String MSG_RESULT_0 = "문자메세지_전달 (성공)";
    public static final String MSG_RESULT_1 = "문자메세지_전송시간 초과";
    public static final String MSG_RESULT_2 = "문자메세지_잘못된 전화번호/비가입자";
    public static final String MSG_RESULT_5 = "문자메세지_통신사 결과 미수신";
    public static final String MSG_RESULT_8 = "문자메세지_단말기 BUSY";
    public static final String MSG_RESULT_9 = "문자메세지_음영지역";
    public static final String MSG_RESULT_a = "문자메세지_건수 부족";
    public static final String MSG_RESULT_b = "문자메세지_전송 권한 없음";
    public static final String MSG_RESULT_c = "문자메세지_중복된 키 접수 차단";
    public static final String MSG_RESULT_d = "문자메세지_중복된 수신번호 접수 차단";
    public static final String MSG_RESULT_e = "문자메세지_서버 내부 에러";
    public static final String MSG_RESULT_o = "문자메세지_TIME OUT 처리(Agent 내부) 메시지본문 중복 차단(Agent 내부)";
    public static final String MSG_RESULT_p = "문자메세지_TIME OUT 처리(Agent 내부) 메시지본문 중복 차단(Agent 내부)";
    public static final String MSG_RESULT_q = "문자메세지_메시지 중복키 체크(Agent 내부)";
    public static final String MSG_RESULT_t = "문자메세지_잘못된 동보 전송 수신번호 리스트 카운트(Agent 내부)";
    public static final String MSG_RESULT_A = "문자메세지_단말기 메시지 저장개수 초과";
    public static final String MSG_RESULT_B = "문자메세지_단말기 일시 서비스 정지";
    public static final String MSG_RESULT_C = "문자메세지_기타 단말기 문제";
    public static final String MSG_RESULT_D = "문자메세지_착신 거절";
    public static final String MSG_RESULT_E = "문자메세지_전원 꺼짐";
    public static final String MSG_RESULT_F = "문자메세지_기타";
    public static final String MSG_RESULT_G = "문자메세지_내부 포맷 에러";
    public static final String MSG_RESULT_H = "문자메세지_통신사 포맷 에러";
    public static final String MSG_RESULT_I = "문자메세지_SMS/MMS 서비스 불가 단말기";
    public static final String MSG_RESULT_J = "문자메세지_착신 측 호 불가 상태";
    public static final String MSG_RESULT_K = "문자메세지_통신사에서 메시지 삭제 처리";
    public static final String MSG_RESULT_L = "문자메세지_통신사 메시지 처리 불가 상태";
    public static final String MSG_RESULT_M = "문자메세지_무선망단 전송 실패";
    public static final String MSG_RESULT_S = "문자메세지_스팸";
    public static final String MSG_RESULT_V = "문자메세지_컨텐츠 사이즈 초과";
    public static final String MSG_RESULT_U = "문자메세지_잘못된 컨텐츠";

    public static String resultMsg(String result) {
        switch (result) {
            case "0":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_0;
            case "1":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_1;
            case "2":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_2;
            case "5":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_5;
            case "6":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_6;
            case "8":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_8;
            case "9":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_9;
            case "a":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_a;
            case "b":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_b;
            case "c":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_c;
            case "d":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_d;
            case "e":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_e;
            case "f":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_f;
            case "k":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_k;
            case "o":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_o;
            case "p":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_p;
            case "t":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_t;
            case "A":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_A;
            case "B":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_B;
            case "C":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_C;
            case "D":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_D;
            case "E":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_E;
            case "F":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_F;
            case "H":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_H;
            case "I":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_I;
            case "J":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_J;
            case "L":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_L;
            case "M":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_M;
            case "S":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_S;
            case "U":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_U;
            case "V":
                return com.logisall.anyware.core.model.property.kko.KKO.RESULT_V;
            default:
                return "NOT FOUND ERROR CODE";
        }
    }

    public static String msgResultMsg(String msgResult) {
        switch (msgResult) {
            case "0":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_0;
            case "1":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_1;
            case "2":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_2;
            case "5":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_5;
            case "8":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_8;
            case "9":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_9;
            case "a":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_a;
            case "b":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_b;
            case "c":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_c;
            case "d":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_d;
            case "e":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_e;
            case "o":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_o;
            case "p":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_p;
            case "t":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_t;
            case "A":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_A;
            case "B":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_B;
            case "C":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_C;
            case "D":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_D;
            case "E":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_E;
            case "F":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_F;
            case "G":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_G;
            case "H":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_H;
            case "I":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_I;
            case "J":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_J;
            case "K":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_K;
            case "L":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_L;
            case "M":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_M;
            case "S":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_S;
            case "V":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_V;
            case "U":
                return com.logisall.anyware.core.model.property.kko.KKO.MSG_RESULT_U;
            default:
                return "NOT FOUND ERROR CODE";
        }
    }
}
