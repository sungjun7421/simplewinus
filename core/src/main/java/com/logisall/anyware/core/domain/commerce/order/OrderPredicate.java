package com.logisall.anyware.core.domain.commerce.order;

import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.commerce.PayMethod;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor(staticName = "getInstance")
public class OrderPredicate {

  private static final QOrder Q_ORDER = QOrder.order;

  private BooleanBuilder builder = new BooleanBuilder();

  public Predicate values() {
    return builder.getValue() == null ? builder.and(Q_ORDER.id.isNotNull()) : builder.getValue();
  }

  public com.logisall.anyware.core.domain.commerce.order.OrderPredicate startDate(final LocalDateTime startDate) {

    if (startDate != null) {
      builder.and(Q_ORDER.createdDate.goe(startDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.order.OrderPredicate endDate(final LocalDateTime endDate) {

    if (endDate != null) {
      builder.and(Q_ORDER.createdDate.loe(endDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.order.OrderPredicate startTime(final LocalDateTime startDate) {

    if (startDate != null) {
//      builder.and(Q_ORDER.relativeOrderMachine.startTime.goe(startDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.order.OrderPredicate endTime(final LocalDateTime endDate) {

    if (endDate != null) {
//      builder.and(Q_ORDER.relativeOrderMachine.endTime.loe(endDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.order.OrderPredicate search(String value) {

    if (!StringUtils.isEmpty(value)) {
      value = value.trim();

      builder.and(Q_ORDER.buyerInfo.fullName.eq(value)
          .or(Q_ORDER.buyerInfo.mobile.eq(value))
          .or(Q_ORDER.buyerInfo.email.eq(value))
          .or(Q_ORDER.title.containsIgnoreCase(value))
          .or(Q_ORDER.oid.containsIgnoreCase(value)));
    }
    return this;
  }


  public com.logisall.anyware.core.domain.commerce.order.OrderPredicate status(final OrderStatus orderStatus) {
    if (orderStatus != null) {
      builder.and(Q_ORDER.orderStatus.eq(orderStatus));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.order.OrderPredicate statuses(final List<OrderStatus> orderStatuses) {
    if (orderStatuses != null && orderStatuses.size() > 0) {
      builder.and(Q_ORDER.orderStatus.in(orderStatuses));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.order.OrderPredicate paymentComplete(final Boolean paymentComplete) {

    if (paymentComplete != null) {
      builder.and(Q_ORDER.paymentComplete.eq(paymentComplete));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.order.OrderPredicate payMethod(final PayMethod payMethod) {

    if (payMethod != null) {
      builder.and(Q_ORDER.payMethod.eq(payMethod));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.order.OrderPredicate buyerEmail(final String email) {
    if (!StringUtils.isEmpty(email)) {
      builder.and(Q_ORDER.relativeBuyer.relativeUser.email.eq(email));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.order.OrderPredicate buyerId(final Long idBuyer) {

    if (idBuyer != null) {
      builder.and(Q_ORDER.relativeBuyer.id.eq(idBuyer));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.order.OrderPredicate memberSearch(final String value) {

    if (!StringUtils.isEmpty(value)) {
      builder.and(Q_ORDER.relativeBuyer.relativeUser.fullName.eq(value)
            .or(Q_ORDER.relativeBuyer.relativeUser.mobile.eq(value))
            .or(Q_ORDER.relativeBuyer.relativeUser.email.eq(value)));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.order.OrderPredicate oid(final String oid) {
    if (oid != null) {
      builder.and(Q_ORDER.oid.eq(oid));
    }
    return this;
  }
}
