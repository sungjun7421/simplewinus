package com.logisall.anyware.core.domain.commerce.cart;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;


@NoArgsConstructor(staticName = "getInstance")
public class CartPredicate {

    private static final QCart Q_ENTITY = QCart.cart;


    private BooleanBuilder builder = new BooleanBuilder();

    public Predicate values() {
        return builder.getValue() == null ? builder.and(Q_ENTITY.id.isNotNull()) : builder.getValue();
    }

    public com.logisall.anyware.core.domain.commerce.cart.CartPredicate startDate(final LocalDateTime startDate) {

        if (startDate != null) {
            builder.and(Q_ENTITY.createdDate.goe(startDate));
        }
        return this;
    }

    public com.logisall.anyware.core.domain.commerce.cart.CartPredicate endDate(final LocalDateTime endDate) {

        if (endDate != null) {
            builder.and(Q_ENTITY.createdDate.loe(endDate));
        }
        return this;
    }

    public com.logisall.anyware.core.domain.commerce.cart.CartPredicate search(String value) {

        if (!StringUtils.isEmpty(value)) {
            value = value.trim();

            builder.and(Q_ENTITY.relativeProduct.information.name.textKoKr.containsIgnoreCase(value)
                .or(Q_ENTITY.relativeProduct.information.name.textEnUs.containsIgnoreCase(value))
                .or(Q_ENTITY.relativeProduct.information.name.textJaJp.containsIgnoreCase(value))
                .or(Q_ENTITY.relativeProduct.information.name.textZhCn.containsIgnoreCase(value))
                .or(Q_ENTITY.relativeProduct.information.name.textZhTw.containsIgnoreCase(value))
            );
        }
        return this;
    }

    public com.logisall.anyware.core.domain.commerce.cart.CartPredicate buyer(final Long idBuyer) {

        if (idBuyer != null && idBuyer != 0L) {
            builder.and(Q_ENTITY.relativeBuyer.id.eq(idBuyer));
        }
        return this;
    }

    public com.logisall.anyware.core.domain.commerce.cart.CartPredicate product(final Long idProduct) {

        if (idProduct != null && idProduct != 0L) {
            builder.and(Q_ENTITY.relativeProduct.id.eq(idProduct));
        }
        return this;
    }

}
