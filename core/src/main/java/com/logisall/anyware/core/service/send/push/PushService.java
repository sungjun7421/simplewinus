package com.logisall.anyware.core.service.send.push;

import com.logisall.anyware.core.domain.push.Push;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.fcm.FCMReqBody;
import com.logisall.anyware.core.model.reqbody.etc.push.PushReqBody;
import org.springframework.data.domain.Page;

public interface PushService {

    // CUD
    com.logisall.anyware.core.domain.push.Push create(com.logisall.anyware.core.model.reqbody.etc.push.PushReqBody pushReqBody, String email);
    // R
    com.logisall.anyware.core.domain.push.Push get(Long id);
    Page<Push> page(Filter filter);

    void subscribeToTopic(String deviceToken);
    void unsubscribeFromTopic(String deviceToken);
    String sendMessageToDevice(PushReqBody pushReqBody);
    String sendMessageToAllDevice(FCMReqBody fcmReqBody);
}
