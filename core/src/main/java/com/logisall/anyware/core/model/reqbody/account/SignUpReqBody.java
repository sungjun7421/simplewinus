package com.logisall.anyware.core.model.reqbody.account;

import com.logisall.anyware.core.domain.user.Authority;
import com.logisall.anyware.core.domain.user.SocialId;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.model.BaseRequestBody;
import com.logisall.anyware.core.model.reqbody.account.SocialIdReqBody;
import com.logisall.anyware.core.model.reqbody.account.TermsAgreeReqBody;
import com.logisall.anyware.core.utils.ValidUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

@Schema(description = "회워가입")
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SignUpReqBody extends BaseRequestBody {

  private static final long serialVersionUID = -2212393886807589144L;

  @Email
  @Schema(description = "이메일 주소", example = "test@logisall.com", minLength = 3, maxLength = 255)
  private String email; // ID

  @Pattern(regexp = ValidUtils.PATTERN_PASSWORD_NEW)
  @Schema(description = "비밀번호", example = "abcd1234", minLength = 6, maxLength = 30)
  private String password; // 비밀번호

  @Pattern(regexp = ValidUtils.PATTERN_FULLNAME)
  @Schema(description = "성명", example = "홍길동", minLength = 2, maxLength = 10)
  private String fullName; // 성명

  @Pattern(regexp = ValidUtils.PATTERN_MOBILE)
  @Schema(description = "휴대폰 번호", example = "01011112222", minLength = 10, maxLength = 11)
  private String mobile; // 휴대전화

  @Schema(description = "프로필 이미지")
  private String image;

  @Schema(description = "약관 동의")
  private TermsAgreeReqBody termsAgree; // 동의

  @Schema(description = "SNS ID")
  private SocialIdReqBody socialId; // SNS 아이디

//  @Schema(description = "이메일/모바일 인증")
//  private Verification verification; // 인증

  @JsonIgnore
  public boolean isValid() {
    return StringUtils.isEmpty(this.getEmail())
        || StringUtils.isEmpty(this.getPassword())
        || StringUtils.isEmpty(this.getFullName())
        || StringUtils.isEmpty(this.getMobile())
        || this.getTermsAgree() == null;
  }

  public User toUser() {

    User user = new User();

    user.setEmail(this.getEmail().trim());
    user.setPassword(this.getPassword().trim());
    user.setFullName(this.getFullName().trim());
    user.setMobile(this.getMobile().trim());
    user.setImage(this.image);

    user.setRole(Authority.Role.USER);
    user.setTermsAgree(this.getTermsAgree() != null ? this.getTermsAgree().toTermsAgree() : null);
    user.setSocialId(this.getSocialId() != null ? this.getSocialId().toSocialId() : new SocialId());

    return user;
  }
}
