package com.logisall.anyware.core.model.payment.toss;

import com.logisall.anyware.core.model.BaseRequestBody;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TossComplete extends BaseRequestBody {
  private static final long serialVersionUID = -1481717752186300663L;

  private String code;
  private String oid;
}
