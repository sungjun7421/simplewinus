package com.logisall.anyware.core.model.resbody.commerce.coupon;

import com.logisall.anyware.core.domain.commerce.coupon.CouponType;
import com.logisall.anyware.core.model.BaseResponseBody;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.math.BigDecimal;
import java.time.LocalDate;

@Relation(value = "coupon", collectionRelation = "coupons")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CouponResBody extends BaseResponseBody {

  private static final long serialVersionUID = 8274248531932045028L;

  private Long id;
  private String name;
  private String requite;
  private CouponType type;
  private String code;
  private BigDecimal discountFixed;
 // private BigDecimal discountPercent;
  private LocalDate startDate;
  private LocalDate endDate;
}
