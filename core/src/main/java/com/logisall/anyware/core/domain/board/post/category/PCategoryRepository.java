package com.logisall.anyware.core.domain.board.post.category;

import com.logisall.anyware.core.domain.board.post.Post;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PCategoryRepository extends
    PagingAndSortingRepository<com.logisall.anyware.core.domain.board.post.category.PCategory, Long>,
    QuerydslPredicateExecutor<com.logisall.anyware.core.domain.board.post.category.PCategory>,
    PCategoryRepositoryCustom {

  List<com.logisall.anyware.core.domain.board.post.category.PCategory> findAllByType(com.logisall.anyware.core.domain.board.post.Post.Type type, Sort sort);

  @Query("select max(c.orderAscending) from PostCategory c where c.type = ?1")
  Long highestOrder(com.logisall.anyware.core.domain.board.post.Post.Type type);

  @Query("from PostCategory c where c.orderAscending < ?1 and c.type = ?2 order by c.orderAscending desc")
  List<com.logisall.anyware.core.domain.board.post.category.PCategory> previous(long orderAscending, com.logisall.anyware.core.domain.board.post.Post.Type type, Pageable pageable);

  @Query("from PostCategory c where c.orderAscending > ?1 and c.type = ?2 order by c.orderAscending asc")
  List<com.logisall.anyware.core.domain.board.post.category.PCategory> next(long orderAscending, com.logisall.anyware.core.domain.board.post.Post.Type type, Pageable pageable);

  List<PCategory> findByTypeOrderByOrderAscendingAsc(Post.Type type);
}

