package com.logisall.anyware.core.service.commerce.buyer;

import com.logisall.anyware.core.domain.commerce.buyer.Buyer;

public interface BuyerService {

  Buyer get(Long id);
  Buyer get(String email);
}
