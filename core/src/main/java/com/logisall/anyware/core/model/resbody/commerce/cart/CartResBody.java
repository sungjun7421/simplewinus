package com.logisall.anyware.core.model.resbody.commerce.cart;

import com.logisall.anyware.core.domain.commerce.product.Product;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
@ToString
public class CartResBody {
    private Long idCart;
    private Product product;
    private Integer qty;
}
