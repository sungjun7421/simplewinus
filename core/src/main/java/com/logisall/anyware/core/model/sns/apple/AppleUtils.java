package com.logisall.anyware.core.model.sns.apple;

import com.logisall.anyware.core.config.property.sns.AppleApiKey;
import com.logisall.anyware.core.model.sns.apple.Key;
import com.logisall.anyware.core.model.sns.apple.Keys;
import com.logisall.anyware.core.model.sns.apple.Payload;
import com.logisall.anyware.core.model.sns.apple.TokenResponse;
import com.logisall.anyware.core.utils.OkHttpUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.ECDSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.ReadOnlyJWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import okhttp3.*;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class AppleUtils {


  private final static String APPLE_PUBLIC_KEYS_URL = "https://appleid.apple.com/auth/keys";
  private final static String ISS = "https://appleid.apple.com";
  private final static String AUTH_TOKEN_URL = "https://appleid.apple.com/auth/token";

  @Autowired
  private AppleApiKey apiKey;

  @Autowired
  private OkHttpClient client;

  /**
   * User가 Sign in with Apple 요청(https://appleid.apple.com/auth/authorize)으로 전달받은 id_token을 이용한 최초 검증
   * Apple Document URL ‣ https://developer.apple.com/documentation/sign_in_with_apple/sign_in_with_apple_rest_api/verifying_a_user
   *
   * @param id_token
   * @return boolean
   */
  public boolean verifyIdentityToken(String id_token) {

    try {
      SignedJWT signedJWT = SignedJWT.parse(id_token);
      ReadOnlyJWTClaimsSet payload = signedJWT.getJWTClaimsSet();

      // EXP
      Date currentTime = new Date(System.currentTimeMillis());
      if (!currentTime.before(payload.getExpirationTime())) {
        return false;
      }

      // NONCE(Test value), ISS, AUD
      if (!apiKey.getNonce().equals(payload.getClaim("nonce")) || !ISS.equals(payload.getIssuer()) || !apiKey.getClientId().equals(payload.getAudience().get(0))) {
        return false;
      }

      // RSA
      if (verifyPublicKey(signedJWT)) {
        return true;
      }
    } catch (ParseException e) {
      e.printStackTrace();
    }

    return false;
  }

  /**
   * Apple Server에서 공개 키를 받아서 서명 확인
   *
   * @param signedJWT
   * @return
   */
  private boolean verifyPublicKey(SignedJWT signedJWT) {

    try {
      Request request = new Request.Builder()
          .url(APPLE_PUBLIC_KEYS_URL)
          .method("GET", null)
          .build();
      Response response = client.newCall(request).execute();
      String publicKeys = response.body().string();
//            String publicKeys = HttpClientUtils.doGet(APPLE_PUBLIC_KEYS_URL);
      ObjectMapper objectMapper = new ObjectMapper();
      Keys keys = objectMapper.readValue(publicKeys, Keys.class);
      for (Key key : keys.getKeys()) {
        RSAKey rsaKey = (RSAKey) JWK.parse(objectMapper.writeValueAsString(key));
        RSAPublicKey publicKey = rsaKey.toRSAPublicKey();
        JWSVerifier verifier = new RSASSAVerifier(publicKey);

        if (signedJWT.verify(verifier)) {
          return true;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return false;
  }

  /**
   * client_secret 생성
   * Apple Document URL ‣ https://developer.apple.com/documentation/sign_in_with_apple/generate_and_validate_tokens
   *
   * @return client_secret(jwt)
   */
  public String createClientSecret() {

    JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.ES256).keyID(apiKey.getKeyId()).build();
    JWTClaimsSet claimsSet = new JWTClaimsSet();
    Date now = new Date();

    claimsSet.setIssuer(apiKey.getTeamId());
    claimsSet.setIssueTime(now);
    claimsSet.setExpirationTime(new Date(now.getTime() + 3600000));
    claimsSet.setAudience(ISS);
    claimsSet.setSubject(apiKey.getClientId());

    SignedJWT jwt = new SignedJWT(header, claimsSet);

    // TODO new ECPrivateKeyImpl java 11 변경
//    try {
//
//      ECPrivateKey ecPrivateKey = new ECPrivateKeyImpl(readPrivateKey());
//      JWSSigner jwsSigner = new ECDSASigner(ecPrivateKey.getS());
//
//      jwt.sign(jwsSigner);
//
//    } catch (InvalidKeyException e) {
//      e.printStackTrace();
//    } catch (JOSEException e) {
//      e.printStackTrace();
//    }

    try {
      final KeyFactory keyPairGenerator = KeyFactory.getInstance("EC"); // EC is ECDSA in Java
      ECPrivateKey ecPrivateKey = (ECPrivateKey) keyPairGenerator.generatePrivate(new PKCS8EncodedKeySpec(Base64.decodeBase64(readPrivateKey())));
      JWSSigner jwsSigner = new ECDSASigner(ecPrivateKey.getS());

      jwt.sign(jwsSigner);

    } catch (JOSEException e) {
      e.printStackTrace();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (InvalidKeySpecException e) {
      e.printStackTrace();
    }

    return jwt.serialize();
  }

  /**
   * 파일에서 private key 획득
   *
   * @return Private Key
   */
  private byte[] readPrivateKey() {

    Resource resource = new ClassPathResource(apiKey.getPath());
    byte[] content = null;

    try (FileReader keyReader = new FileReader(resource.getURI().getPath());
         PemReader pemReader = new PemReader(keyReader)) {
      {
        PemObject pemObject = pemReader.readPemObject();
        content = pemObject.getContent();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    return content;
  }

  /**
   * 유효한 code 인지 Apple Server에 확인 요청
   * Apple Document URL ‣ https://developer.apple.com/documentation/sign_in_with_apple/generate_and_validate_tokens
   *
   * @return
   */
  public TokenResponse validateAuthorizationGrantCode(String client_secret, String code) {

    StringBuffer sb = new StringBuffer();

    sb.append("client_id=" + apiKey.getClientId());
    sb.append("&client_secret=" + client_secret);
    sb.append("&code=" + code);
    sb.append("&grant_type=" + "authorization_code");
    sb.append("&redirect_uri=" + apiKey.getReturnUri());

    return getTokenResponse(sb.toString());
  }

  /**
   * 유효한 refresh_token 인지 Apple Server에 확인 요청
   * Apple Document URL ‣ https://developer.apple.com/documentation/sign_in_with_apple/generate_and_validate_tokens
   *
   * @param client_secret
   * @param refresh_token
   * @return
   */
  public TokenResponse validateAnExistingRefreshToken(String client_secret, String refresh_token) {

    StringBuffer sb = new StringBuffer();

    sb.append("client_id=" + apiKey.getClientId());
    sb.append("&client_secret=" + client_secret);
    sb.append("&grant_type=" + "refresh_token");
    sb.append("&refresh_token=" + refresh_token);

    return getTokenResponse(sb.toString());
  }

  /**
   * POST https://appleid.apple.com/auth/token
   *
   * @param tokenRequest
   * @return
   */
  private TokenResponse getTokenResponse(String tokenRequest) {

    try {
//            String response = HttpClientUtils.doPost(AUTH_TOKEN_URL, tokenRequest);
      MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
      RequestBody body = RequestBody.create(mediaType, tokenRequest);
      Request request = new Request.Builder()
          .url(AUTH_TOKEN_URL)
          .method("POST", body)
          .addHeader("Content-Type", "application/x-www-form-urlencoded")
          .build();

      Response response = client.newCall(request).execute();
      TokenResponse tokenResponse = OkHttpUtils.toObject(response.body(), TokenResponse.class);

      if (tokenRequest != null) {
        return tokenResponse;
      }
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Apple Meta Value
   *
   * @return
   */
  public Map<String, String> getMetaInfo() {

    Map<String, String> metaInfo = new HashMap<>();

    metaInfo.put("CLIENT_ID", apiKey.getClientId());
    metaInfo.put("REDIRECT_URI", apiKey.getReturnUri());
    metaInfo.put("NONCE", apiKey.getNonce()); // Test value

    return metaInfo;
  }

  /**
   * id_token을 decode해서 payload 값 가져오기
   *
   * @param id_token
   * @return
   */
  public Payload decodeFromIdToken(String id_token) {

    try {
      SignedJWT signedJWT = SignedJWT.parse(id_token);
      ReadOnlyJWTClaimsSet getPayload = signedJWT.getJWTClaimsSet();
      ObjectMapper objectMapper = new ObjectMapper();
      Payload payload = objectMapper.readValue(getPayload.toJSONObject().toJSONString(), Payload.class);

      if (payload != null) {
        return payload;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

}
