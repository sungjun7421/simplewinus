package com.logisall.anyware.core.model.reqbody.commerce.coupon;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RegisterCouponReqBody {

    private static final long serialVersionUID = -2751631354582726867L;

    private String code;
}
