package com.logisall.anyware.core.model.resbody.common;

import com.logisall.anyware.core.model.BaseResponseBody;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

@Relation(value = "file", collectionRelation = "files")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FileResBody extends BaseResponseBody {

    private static final long serialVersionUID = -7676420592670217510L;

    private Long id;
    private String url;
    private String originalFilename;
    private String filename;
    private String mimeType;
    private long size;
}
