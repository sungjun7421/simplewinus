package com.logisall.anyware.core.service.map.naver;

import com.logisall.anyware.core.model.map.naver.NaverGeoInfo;
import com.logisall.anyware.core.model.map.naver.NaverMapAddressInfo;

import java.io.IOException;
import java.util.concurrent.Future;

public interface MapNaverService {

  Future<NaverGeoInfo> getNaverJusoInfo(String keyword) throws IOException;
  Future<NaverMapAddressInfo> getNaverMapAddressInfo(String keyword) throws IOException;

}
