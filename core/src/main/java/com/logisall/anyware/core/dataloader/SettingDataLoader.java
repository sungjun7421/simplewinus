package com.logisall.anyware.core.dataloader;

import com.google.common.collect.Lists;
import com.logisall.anyware.core.domain.International.InterText;
import com.logisall.anyware.core.domain.International.InternationalMode;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevelRepository;
import com.logisall.anyware.core.domain.setting.AppSetting;
import com.logisall.anyware.core.domain.setting.AppSettingRepository;
import com.logisall.anyware.core.utils.LocaleUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 초기 환결 데이터 값 설정
 */
@Slf4j
@Component
@Order(0) // 낮을수록 먼저
public class SettingDataLoader implements CommandLineRunner {

    @Value("${app.international.mode}")
    private boolean internationalMode;

    @Value("${app.international.locale}")
    private String internationalLocale;

    @Autowired
    private AppSettingRepository appSettingRepository;

    @Autowired
    private BuyerLevelRepository buyerLevelRepository;

//    @Autowired
//    private ServiceRepository serviceRepository;

    @Override
    public void run(String... args) {

        // init
        if (Lists.newArrayList(appSettingRepository.findAll()).size() == 0) {
            com.logisall.anyware.core.domain.setting.AppSetting setting = new AppSetting();
            setting.setInternational(internationalMode);
            setting.setDefaultLocale(LocaleUtils.toLocale(internationalLocale));

            com.logisall.anyware.core.domain.International.InternationalMode im = new InternationalMode();
            im.setKoKr(true);
            im.setZhCn(true);
            im.setZhTw(true);
            im.setJaJp(true);
            im.setEnUs(true);
            setting.setInternationalMode(im);
            appSettingRepository.save(setting);
        }

        if (Lists.newArrayList(buyerLevelRepository.findAll()).size() == 0){
            com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel buyerLevel1 = new com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel();
            buyerLevel1.setName(getName("Level 1"));
            buyerLevel1.setDescription(getName("15만원 미만"));
            buyerLevel1.setLevel(1);
            buyerLevel1.setSaveRate(BigDecimal.valueOf(0.5));
            buyerLevelRepository.save(buyerLevel1);

            com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel buyerLevel2 = new com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel();
            buyerLevel2.setName(getName("Level 2"));
            buyerLevel2.setDescription(getName("15만원 이상"));
            buyerLevel2.setLevel(2);
            buyerLevel2.setSaveRate(BigDecimal.valueOf(1));
            buyerLevelRepository.save(buyerLevel2);

            com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel buyerLevel3 = new com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel();
            buyerLevel3.setName(getName("Level 3"));
            buyerLevel3.setDescription(getName("30만원 이상"));
            buyerLevel3.setLevel(3);
            buyerLevel3.setSaveRate(BigDecimal.valueOf(3));
            buyerLevelRepository.save(buyerLevel3);

            com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel buyerLevel4 = new com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel();
            buyerLevel4.setName(getName("Level 4"));
            buyerLevel4.setDescription(getName("50만원 이상"));
            buyerLevel4.setLevel(4);
            buyerLevel4.setSaveRate(BigDecimal.valueOf(5));
            buyerLevelRepository.save(buyerLevel4);

            com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel buyerLevel5 = new com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel();
            buyerLevel5.setName(getName("Level 5"));
            buyerLevel5.setDescription(getName("100만원 이상"));
            buyerLevel5.setLevel(5);
            buyerLevel5.setSaveRate(BigDecimal.valueOf(7));
            buyerLevelRepository.save(buyerLevel5);

            com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel buyerLevel6 = new BuyerLevel();
            buyerLevel6.setName(getName("Level 6"));
            buyerLevel6.setDescription(getName("150만원 이상"));
            buyerLevel6.setLevel(6);
            buyerLevel6.setSaveRate(BigDecimal.valueOf(7));
            buyerLevelRepository.save(buyerLevel6);
        }
//
//        if (Lists.newArrayList(serviceRepository.findAll()).size() == 0){
//            Service service1 = new Service();
//            service1.setName(getName("세탁기"));
//            service1.setCode("WASHING");
//            serviceRepository.save(service1);
//
//            Service service2 = new Service();
//            service2.setName(getName("건조기"));
//            service2.setCode("DRYER");
//            serviceRepository.save(service2);
//
//            Service service3 = new Service();
//            service3.setName(getName("스타일러"));
//            service3.setCode("STYLER");
//            serviceRepository.save(service3);
//
//            Service service4 = new Service();
//            service4.setName(getName("드라이클리닝"));
//            service4.setCode("DRY_CLEANING");
//            serviceRepository.save(service4);
//        }
    }

    private com.logisall.anyware.core.domain.International.InterText getName(String name){
        com.logisall.anyware.core.domain.International.InterText interText = new InterText();
        interText.setTextKoKr(name);
        interText.setTextEnUs(name);
        interText.setTextJaJp(name);
        interText.setTextZhCn(name);
        interText.setTextZhTw(name);
        return interText;
    }
}
