package com.logisall.anyware.core.service.document;

import com.logisall.anyware.core.domain.document.Document;
import com.logisall.anyware.core.domain.document.DocumentPredicate;
import com.logisall.anyware.core.domain.document.DocumentRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.crud.UpdateErrorException;
import com.logisall.anyware.core.service.document.DocumentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

@Slf4j
@Service
public class DocumentServiceImpl implements DocumentService {

  @Autowired
  private DocumentRepository documentRepository;

  @Override
  @Transactional
  public Document create(Document document) {
    return documentRepository.save(document);
  }

  @Override
  @Transactional
  public Document update(Document document) {

    if (document.getId() == null) {
      throw new BadRequestException();
    }

    return documentRepository.findById(document.getId())
        .map(ori -> {
          BeanUtils.copyProperties(document, ori, Document.IGNORE_PROPERTIES);
          return documentRepository.save(ori);
        }).orElseThrow(() -> new UpdateErrorException(document.getId(), Document.class.getName()));
  }

  @Override
  @Transactional
  public void delete(Long id) {
    documentRepository.findById(id)
        .ifPresent(document -> {
          document.delete();
          documentRepository.delete(document);
        });
  }

  @Override
  @Transactional(readOnly = true)
  public Document get(Locale locale, Long id) {
    return documentRepository.findById(id)
        .map(document -> {
          document.setLocale(locale);
          return document;
        }).orElse(null);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<Document> page(Locale locale, Filter filter) {

    Page<Document> page = documentRepository.findAll(
        DocumentPredicate.getInstance()
            .search(filter.getQuery())
            .startDate(filter.getStartDate())
            .endDate(filter.getEndDate())
            .values(),
        filter.getPageable());

    page.forEach(document -> document.setLocale(locale));
    return page;
  }

  @Override
  @Transactional(readOnly = true)
  public Page<Document> page(Locale locale, Filter filter, Document.Type type) {

    log.debug("locale ::: {}", locale);
    log.debug("filter ::: {}", filter);
    log.debug("type ::: {}", type);

    Page<Document> page = documentRepository.findAll(
        DocumentPredicate.getInstance()
            .search(filter.getQuery())
            .startDate(filter.getStartDate())
            .endDate(filter.getEndDate())
            .type(type)
            .values(),
        filter.getPageable());

    page.forEach(document -> document.setLocale(locale));
    return page;
  }
}
