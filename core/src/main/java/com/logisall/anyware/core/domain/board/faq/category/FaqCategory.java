package com.logisall.anyware.core.domain.board.faq.category;

import com.logisall.anyware.core.domain.AbstractEntityCategory;
import com.logisall.anyware.core.domain.RestEntityBody;
import com.logisall.anyware.core.domain.board.faq.Faq;
import com.logisall.anyware.core.model.resbody.board.FaqCategoryResBody;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

@Entity(name = "FaqCategory")
@Getter
@Setter
@ToString(exclude = {"faqs"})
@NoArgsConstructor
public class FaqCategory extends AbstractEntityCategory<Long>
        implements RestEntityBody<FaqCategoryResBody> {

    private static final long serialVersionUID = 3863266271477643490L;
    public static String[] IGNORE_PROPERTIES = {
            "id",
            "orderAscending",
            "faqs"
    };

    @PrePersist
    public void prePersist() {
    }

    @PreUpdate
    public void preUpdate() {
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String image; // icon 이미지

    @JsonIgnore
    @OrderBy("orderAscending ASC")
    @ManyToMany(mappedBy = "categories")
    private List<Faq> faqs = new ArrayList<>();

    @Override
    public void delete() {

        if (this.faqs != null) {
            this.faqs.forEach(faq -> {
                faq.getCategories().removeIf(category -> Objects.equals(category.getId(), this.id));
            });
        }
    }

    @Override
    public void lazy() {
        if (this.faqs != null)
            this.faqs.size();
    }

    @Override
    public FaqCategoryResBody toBody(Locale locale) {
        this.setLocale(locale);

        return FaqCategoryResBody.builder()
                .id(this.id)
                .name(this.name.getValue())
                .image(this.image)
                .faqs(this.faqs.stream().map(faq -> faq.toBody(locale)).collect(Collectors.toList()))
                .build();
    }
}
