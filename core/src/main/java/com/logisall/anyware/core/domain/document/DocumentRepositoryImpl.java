package com.logisall.anyware.core.domain.document;

import com.logisall.anyware.core.domain.document.Document;
import com.logisall.anyware.core.domain.document.DocumentRepositoryCustom;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

public class DocumentRepositoryImpl implements DocumentRepositoryCustom {

  @Autowired
  private JPAQueryFactory jpaQueryFactory;

  @Override
  public Document latestDoc(Document.Type type) {

    QDocument qDocument = QDocument.document;

    return jpaQueryFactory.selectFrom(qDocument)
        .where(qDocument.executeDate.loe(LocalDate.now()))
        .where(qDocument.active.eq(true))
        .where(qDocument.type.eq(type))
        .orderBy(qDocument.executeDate.desc())
        .limit(1)
        .fetchOne();
  }
}
