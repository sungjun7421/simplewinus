package com.logisall.anyware.core.model.sns.apple;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class TokenResponse implements Serializable {

    private static final long serialVersionUID = -6018247967779085153L;

    private String access_token;
    private Long expires_in;
    private String id_token;
    private String refresh_token;
    private String token_type;
}
