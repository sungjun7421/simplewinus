package com.logisall.anyware.core.config.property.commerce.allatpay;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "pay-module.allatpay")
public class AllAtPayConfig implements Serializable {

  private static final long serialVersionUID = 8592038183977851044L;

  private String id;
  private String key;
}
