package com.logisall.anyware.core.config.language;

import com.logisall.anyware.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Slf4j
@Component
public class Messages {

  @Autowired
  private MessageSource messageSource;

  public String getMessage(String code, String defaultMessage, HttpServletRequest request) throws Exception {
    return this.getMessage(code, null, defaultMessage, null, request);
  }

  public String getMessage(String code, String defaultMessage, String language, HttpServletRequest request) throws Exception {
    return this.getMessage(code, null, defaultMessage, language, request);
  }

  public String getMessage(String code, Object[] args, String defaultMessage, HttpServletRequest request) throws Exception {
    return this.getMessage(code, args, defaultMessage, null, request);
  }

  public String getMessage(String code, Object[] args, String defaultMessage, String language, HttpServletRequest request) throws Exception {
    String message = messageSource.getMessage(code, args, request.getLocale());
    if (StringUtils.isEmpty(message)) {
      if (StringUtils.isNotEmpty(language)) {
        if (language.startsWith("ko")) {
          message = messageSource.getMessage(code, args, defaultMessage, Locale.KOREA);
        } else if (language.equals("zh") || language.equals("zh_CN")) {
          message = messageSource.getMessage(code, args, defaultMessage, Locale.CHINA);
        } else if (language.equals("zh_TW")) {
          message = messageSource.getMessage(code, args, defaultMessage, Locale.TRADITIONAL_CHINESE);
        } else if (language.startsWith("ja")) {
          message = messageSource.getMessage(code, args, defaultMessage, Locale.JAPAN);
        } else if (language.startsWith("en")) {
          message = messageSource.getMessage(code, args, defaultMessage, Locale.ENGLISH);
        } else {
          message = messageSource.getMessage(code, args, defaultMessage, Locale.KOREA);
        }
      } else {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
          for (Cookie cookie : cookies) {
            if (cookie.getName().equals("language")) {
              //do something
              //value can be retrieved using #cookie.getValue()
              if (cookie.getValue().startsWith("ko")) {
                message = messageSource.getMessage(code, args, defaultMessage, Locale.KOREA);
              } else if (cookie.getValue().startsWith("en")) {
                message = messageSource.getMessage(code, args, defaultMessage, Locale.ENGLISH);
              } else if (cookie.getValue().startsWith("ja")) {
                message = messageSource.getMessage(code, args, defaultMessage, Locale.JAPAN);
              } else if (cookie.getValue().equals("zh") || cookie.getValue().equals("zh_CH")) {
                message = messageSource.getMessage(code, args, defaultMessage, Locale.CHINA);
              } else if (cookie.getValue().equals("zh_TW")) {
                message = messageSource.getMessage(code, args, defaultMessage, Locale.TRADITIONAL_CHINESE);
              } else {
                message = messageSource.getMessage(code, args, defaultMessage, Locale.KOREA);
              }
            }
          }
        } else {
          message = messageSource.getMessage(code, args, defaultMessage, Locale.KOREA);
        }
      }
    }
    return message;
  }

  public String getMessage(String code, Object[] args, String defaultMessage, Locale locale) throws Exception {
    return messageSource.getMessage(code, args, defaultMessage, locale);
  }
}
