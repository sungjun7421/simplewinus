package com.logisall.anyware.core.config.property.commerce.lguplus;

import com.logisall.anyware.core.utils.DateUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "pay-module.lguplus")
public class LGUPlus implements Serializable{

    private static final long serialVersionUID = -1063231324200291075L;

    private String platform;
    private URL url;
    private Mall mall;

    /**
     * 타임스탬프 가져오기 (yyyyMMddHHmmss)
     */
    public String getTimestamp() {
        return DateUtils.getDateTimeString();
    }

    /**
     * ************************************************
     * 본인인증 요청시 MD5 해쉬암호화 (절대 수정하지 마세요. LG유플러스에서 제공해준 암호화 방식입니다.)
     * MD5 해쉬암호화는 거래 위변조를 막기위한 방법입니다.
     * ************************************************
     *
     * 해쉬 암호화 적용( LGD_MID + LGD_BUYERSSN + LGD_TIMESTAMP + LGD_MERTKEY )
     * LGD_MID          : 상점아이디
     * LGD_BUYERSSN     : 생년월일 / 사업자번호
     * LGD_TIMESTAMP    : 타임스탬프
     * LGD_MERTKEY      : 상점MertKey (mertkey는 상점관리자 -> 계약정보 -> 상점정보관리에서 확인하실수 있습니다)
     *
     * MD5 해쉬데이터 암호화 검증을 위해
     * LG유플러스에서 발급한 상점키(MertKey)를 환경설정 파일(lgdacom/conf/mall.conf)에 반드시 입력하여 주시기 바랍니다.
     */
    public String encodeToHashDataByMD5InAuthRequestCase(String LGD_MID, String LGD_BUYERSSN, String LGD_TIMESTAMP, String LGD_MERTKEY) throws NoSuchAlgorithmException {

        StringBuffer sb = new StringBuffer();
        sb.append(LGD_MID);
        sb.append(LGD_BUYERSSN);
        sb.append(LGD_TIMESTAMP);
        sb.append(LGD_MERTKEY);

        byte[] bNoti = sb.toString().getBytes();
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] digest = md.digest(bNoti);

        StringBuffer strBuf = new StringBuffer();
        for (int i=0 ; i < digest.length ; i++) {
            int c = digest[i] & 0xff;
            if (c <= 15){
                strBuf.append("0");
            }
            strBuf.append(Integer.toHexString(c));
        }

        return strBuf.toString();
    }
}
