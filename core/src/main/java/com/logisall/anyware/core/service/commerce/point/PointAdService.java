package com.logisall.anyware.core.service.commerce.point;

import com.logisall.anyware.core.domain.commerce.point.Point;
import com.logisall.anyware.core.model.Filter;
import org.springframework.data.domain.Page;

import java.util.Locale;

public interface PointAdService {
    // CD
    Point create(Point point);
    void delete(Long id);

    // R
    Point get(Locale locale, Long id);
    Page<Point> page(Locale locale, Filter filter, Long idBuyer, Point.Status status);

    // 보유포인트
    int total(String email);
    int total(Long idUser);

    // 만료 예정포인트
    int expireScheduledTotal(String email);
    int expireScheduledTotal(Long idUser);
}
