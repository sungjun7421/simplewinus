package com.logisall.anyware.core.model.map.naver;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@ToString
public class NaverGeoInfo implements Serializable {

  private static final long serialVersionUID = -7656389381008850804L;

  private String status;
  private NaverMapMeta meta;
  private List<NaverMapAddress> addresses;
  private String errorMessage;
}
