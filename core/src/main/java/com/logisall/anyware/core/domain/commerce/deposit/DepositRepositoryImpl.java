package com.logisall.anyware.core.domain.commerce.deposit;

import com.logisall.anyware.core.domain.commerce.deposit.DepositRepositoryCustom;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

@Slf4j
public class DepositRepositoryImpl implements DepositRepositoryCustom {

  @Autowired
  private JPAQueryFactory jpaQueryFactory;

  @Override
  public BigDecimal getAmountAvailable(String email) {
    return null;
  }
}
