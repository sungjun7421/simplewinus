package com.logisall.anyware.core.config.property.sns;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "apple.api")
public class AppleApiKey {

    private String clientId;
    private String keyId;
    private String teamId;

    private String returnUri;
    private String accessTokenUri;
    private String publicKeyUri;
    private String path;
    private String nonce;
}
