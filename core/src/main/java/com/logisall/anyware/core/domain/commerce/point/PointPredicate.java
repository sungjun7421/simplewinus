package com.logisall.anyware.core.domain.commerce.point;

import com.logisall.anyware.core.domain.commerce.point.Point;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@NoArgsConstructor(staticName = "getInstance")
public class PointPredicate {

  private static final QPoint Q_POINT = QPoint.point1;

  private BooleanBuilder builder = new BooleanBuilder();

  public Predicate values() {
    return builder.getValue() == null ? builder.and(Q_POINT.id.isNotNull()) : builder.getValue();
  }

  public com.logisall.anyware.core.domain.commerce.point.PointPredicate search(String value) {

    if (!StringUtils.isEmpty(value)) {
      value = value.trim();
      builder.and(Q_POINT.message.textEnUs.containsIgnoreCase(value)
              .or(Q_POINT.message.textJaJp.containsIgnoreCase(value))
              .or(Q_POINT.message.textKoKr.containsIgnoreCase(value))
              .or(Q_POINT.message.textZhCn.containsIgnoreCase(value))
              .or(Q_POINT.message.textZhTw.containsIgnoreCase(value))
              .or(Q_POINT.relativeBuyer.relativeUser.email.eq(value))
              .or(Q_POINT.relativeBuyer.relativeUser.mobile.eq(value))
              .or(Q_POINT.relativeBuyer.relativeUser.fullName.eq(value)));
    }

    return this;
  }

  public com.logisall.anyware.core.domain.commerce.point.PointPredicate startDate(final LocalDateTime startDate) {

    if (!StringUtils.isEmpty(startDate)) {
      builder.and(Q_POINT.createdDate.goe(startDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.point.PointPredicate endDate(final LocalDateTime endDate) {

    if (!StringUtils.isEmpty(endDate)) {
      builder.and(Q_POINT.createdDate.loe(endDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.point.PointPredicate status(final Point.Status status) {
    if (status != null) {
      builder.and(Q_POINT.status.eq(status));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.point.PointPredicate buyer(final Long idBuyer) {
    if (idBuyer != null && idBuyer != 0L) {
      builder.and(Q_POINT.relativeBuyer.id.eq(idBuyer));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.commerce.point.PointPredicate buyer(final String email) {
    if (StringUtils.isEmpty(email)) {
      builder.and(Q_POINT.relativeBuyer.relativeUser.email.eq(email));
    }
    return this;
  }


}
