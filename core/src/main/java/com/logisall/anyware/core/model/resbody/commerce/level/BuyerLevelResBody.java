package com.logisall.anyware.core.model.resbody.commerce.level;

import com.logisall.anyware.core.model.BaseResponseBody;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.math.BigDecimal;

@Relation(value = "level", collectionRelation = "level")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BuyerLevelResBody extends BaseResponseBody {
    private static final long serialVersionUID = -176751668681472158L;

    private Long idBuyer;
    private int level;
    private String name;
    private String description;
    private BigDecimal pointRate;
}
