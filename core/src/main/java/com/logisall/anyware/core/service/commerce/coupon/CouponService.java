package com.logisall.anyware.core.service.commerce.coupon;

import com.logisall.anyware.core.domain.commerce.coupon.Coupon;
import com.logisall.anyware.core.domain.commerce.coupon.CouponType;
import com.logisall.anyware.core.model.Filter;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Locale;

public interface CouponService {

    // CUD
    com.logisall.anyware.core.domain.commerce.coupon.Coupon create(com.logisall.anyware.core.domain.commerce.coupon.Coupon coupon);
    com.logisall.anyware.core.domain.commerce.coupon.Coupon update(com.logisall.anyware.core.domain.commerce.coupon.Coupon coupon);
    void delete(Long id);

    // R
    com.logisall.anyware.core.domain.commerce.coupon.Coupon get(Locale locale, Long id);
    Page<com.logisall.anyware.core.domain.commerce.coupon.Coupon> page(Locale locale, Filter filter, com.logisall.anyware.core.domain.commerce.coupon.CouponType type);
    List<Coupon> list(Locale locale, CouponType type);

    /**
     * 쿠폰 번호 생성
     *
     * @return 쿠폰번호
     */
    String generateCode();
    String generateCode(int length);
}
