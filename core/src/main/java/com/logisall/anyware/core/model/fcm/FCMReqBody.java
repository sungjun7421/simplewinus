package com.logisall.anyware.core.model.fcm;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class FCMReqBody {

    private String deviceToken;
    private String title;
    private String content;
}
