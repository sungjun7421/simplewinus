package com.logisall.anyware.core.domain.commerce.coupontobuyerlevel;

import com.querydsl.core.types.Predicate;

public class CouponBuyerLevelPredicate {

  private static final QCouponBuyerLevel Q_Coupon_BuyerLevel = QCouponBuyerLevel.couponBuyerLevel;

  public static Predicate coupon(final Long idCoupon) {
    if (idCoupon == null) {
      return null;
    }
    return Q_Coupon_BuyerLevel.relativeCoupon.id.eq(idCoupon);
  }
}
