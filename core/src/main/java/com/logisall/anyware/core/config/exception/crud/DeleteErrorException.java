package com.logisall.anyware.core.config.exception.crud;

import com.logisall.anyware.core.config.exception.crud.CRUDException;

public class DeleteErrorException extends CRUDException {

  private static final long serialVersionUID = 938368102383727294L;

  public DeleteErrorException(Long id, String entityName) {
    super(id, entityName, "DELETE ERROR");
  }
}

