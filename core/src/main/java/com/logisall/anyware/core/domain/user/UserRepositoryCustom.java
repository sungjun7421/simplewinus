package com.logisall.anyware.core.domain.user;

import com.logisall.anyware.core.model.resbody.account.MeResBody;
import com.logisall.anyware.core.model.resbody.account.UserInfoResBody;

import java.util.List;

public interface UserRepositoryCustom {

    long countAll();
    long countUserLeave();
    List<UserInfoResBody> findByUserInfo(String query);

    // Sample
    // MyBatis
    List<MeResBody> selectUsers() throws Exception;

    // 1. 테이블 관리, 간단한 select 문, 기본 CRUD : JPA ORM 사용
    // 2. 복잡한 쿼리 (통계쿼리), 프로시저 Procedure (고객사가 작성) : MyBatis 사용
}
