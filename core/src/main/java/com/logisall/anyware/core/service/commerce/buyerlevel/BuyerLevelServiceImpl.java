package com.logisall.anyware.core.service.commerce.buyerlevel;

import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevelRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;

@Slf4j
@Service
public class BuyerLevelServiceImpl implements BuyerLevelService {
    @Autowired
    private BuyerLevelRepository buyerLevelRepository;

    @Override
    @Transactional
    public List<com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel> list(Locale locale) {
        List<BuyerLevel> list = buyerLevelRepository.findByOrderByLevelAsc();
        list.forEach(level -> {
            level.setLocale(locale);
            level.lazy();
        });
        return list;
    }
}
