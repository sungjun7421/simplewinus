package com.logisall.anyware.core.domain.push;

import com.logisall.anyware.core.utils.StringUtils;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor(staticName = "getInstance")
public class PushPredicate {

  private static final QPush Q_PUSH = QPush.push;

  private BooleanBuilder builder = new BooleanBuilder();

  public com.logisall.anyware.core.domain.push.PushPredicate search(String value) {

    if (!StringUtils.isEmpty(value)) {
      value = value.trim();

      builder.and(Q_PUSH.relativeDevice.deviceToken.eq(value));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.push.PushPredicate startDate(final LocalDateTime startDate) {

    if (startDate != null) {
      builder.and(Q_PUSH.createdDate.goe(startDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.push.PushPredicate endDate(final LocalDateTime endDate) {

    if (endDate != null) {
      builder.and(Q_PUSH.createdDate.loe(endDate));
    }
    return this;
  }

  public Predicate values() {

    return builder.getValue() == null ? builder.and(Q_PUSH.id.isNotNull()) : builder.getValue();
  }
}
