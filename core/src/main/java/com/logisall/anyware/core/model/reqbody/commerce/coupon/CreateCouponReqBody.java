package com.logisall.anyware.core.model.reqbody.commerce.coupon;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateCouponReqBody {

    private static final long serialVersionUID = 7845774680850953940L;

    private Long idCoupon;
}
