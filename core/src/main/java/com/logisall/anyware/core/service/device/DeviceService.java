package com.logisall.anyware.core.service.device;

import com.logisall.anyware.core.domain.device.Device;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.reqbody.etc.device.DeviceReqBody;
import org.springframework.data.domain.Page;

public interface DeviceService {
    // CUD
    Device create(DeviceReqBody deviceReqBody);
    // R
    Device get(Long id);
    Device get(String deviceToken);
    Page<Device> page(Filter filter);
}
