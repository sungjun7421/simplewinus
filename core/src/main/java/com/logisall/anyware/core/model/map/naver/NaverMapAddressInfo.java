package com.logisall.anyware.core.model.map.naver;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@ToString
public class NaverMapAddressInfo implements Serializable {

  private static final long serialVersionUID = -6412829778021066233L;

  private String status;
  private NaverMapMeta meta;
  private List<NaverMapAddressV2> places;
  private String errorMessage;
}
