package com.logisall.anyware.core.model.reqbody.commerce.order;

import com.logisall.anyware.core.domain.commerce.PayMethod;
import com.logisall.anyware.core.model.BaseRequestBody;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Setter
@Getter
@ToString
public class PaymentInfoReqBody extends BaseRequestBody {

    private static final long serialVersionUID = 9210759284140608605L;

    private String oid; // 주문번호
    private Long idCouponHistoty; // 주문쿠폰// coupon has saved
    private BigDecimal point; // 사용 포인트
    private PayMethod payMethod; // 사용 포인트
    private BigDecimal finalPrice;
}
