package com.logisall.anyware.core.model.resbody.board;

import com.logisall.anyware.core.model.BaseResponseBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

@Schema(description = "문의하기")
@Relation(value = "contact", collectionRelation = "contacts")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContactResBody extends BaseResponseBody {

  private static final long serialVersionUID = 1165933905543623563L;

  @Schema(description = "ID")
  private Long id;
}
