package com.logisall.anyware.core.model.map.naver;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@ToString
public class NaverMapAddressElement implements Serializable {
  private static final long serialVersionUID = 1061028179781491209L;

  private List<String> types;
  private String longName;
  private String shortName;
  private String code;

  public String getType() {
    if (types != null && types.size() > 0) {
      return types.get(0);
    }
    return null;
  }

}
