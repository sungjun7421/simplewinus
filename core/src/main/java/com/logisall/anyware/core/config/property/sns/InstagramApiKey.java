package com.logisall.anyware.core.config.property.sns;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "instagram.api")
public class InstagramApiKey {

    private String clientId;
    private String clientSecret;

    private String accessTokenUri;
    private String meUri;
}
