package com.logisall.anyware.core.domain.sms;

import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.model.resbody.sms.SMSHistoryResBody;
import com.logisall.anyware.core.utils.DateUtils;
import com.logisall.anyware.core.vo.SMS;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;

@Slf4j
@Entity
@Getter
@Setter
@ToString(exclude = {"relativeUser"})
public class SMSHistory extends AbstractEntity<Long> {

  private static final long serialVersionUID = -9175128047536552613L;

  public static String[] IGNORE_PROPERTIES = {
    "id",
    "relativeUser"};

  @Getter
  public enum Code {
    OK("200", "성공"),

    USER_ERROR("100", "사용자 에러"),
    PARAM_ERROR("300", "매개변수 에러"),
    ETC_ERROR("400", "기타 에러"),
    UNREG_BLOCK("500", "발신번호 사전 등록제에 의한 미동록 차단"),
    LOW_RATES("600", "선불제 충전요금 부족");

    @Getter
    private final String code;

    @Getter
    private final String message;

    Code(final String code, final String message) {
      this.code = code;
      this.message = message;
    }
  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;  // PK

  @Column
  private Code code; // 코드

//  @Column
//  private String codeSMS;

//  @Column(columnDefinition = "BIT(1) default 0", nullable = true)
//  private boolean verifySMS;

  @Column
  private String cmid; // 결과 코드

  @Column
  private SMS.Type type; // 타입

  @Column
  @DateTimeFormat(pattern = DateUtils.FORMAT_DATE_TIME_UNIT_BAR)
  private String sendTime;

  @Column
  private String destPhone;

  @Column
  private String destName;

  @Column
  private String subject;

  @Column
  private String msgBody;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "idUser", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_User_For_SMSHistory")) // Column name, 참조하는 ID(pk) name
  private User relativeUser; // 보낸사람

  @Override
  public void delete() {

  }

  @Override
  public void lazy() {
    if (this.relativeUser != null) {
      this.relativeUser.getId();
    }
  }

  public SMSHistoryResBody toBody(){
    return SMSHistoryResBody.builder()
            .id(this.id)
            .type(this.type)
            .code(this.code)
//            .verifySMS(this.verifySMS)
            .sendTime(this.sendTime)
            .destPhone(this.destPhone)
            .destName(this.destName)
            .subject(this.subject)
            .msgBody(this.msgBody)
            .fullName(this.relativeUser != null ? this.relativeUser.getFullName() : "")
            .email(this.relativeUser != null ? this.relativeUser.getEmail() : "")
            .mobile(this.relativeUser != null ? this.relativeUser.getMobile() : "")
            .build();
  }
}
