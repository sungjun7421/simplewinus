package com.logisall.anyware.core.service.commerce.review;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.NotFoundException;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.domain.commerce.paymentproduct.PaymentProduct;
import com.logisall.anyware.core.domain.commerce.paymentproduct.PaymentProductRepository;
import com.logisall.anyware.core.domain.commerce.review.Review;
import com.logisall.anyware.core.domain.commerce.review.ReviewPredicate;
import com.logisall.anyware.core.domain.commerce.review.ReviewRepository;
import com.logisall.anyware.core.domain.file.FileEntity;
import com.logisall.anyware.core.domain.file.FileEntityRepository;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.reqbody.commerce.review.ReviewReqBody;
import com.logisall.anyware.core.model.resbody.commerce.review.ReviewResBody;
import com.logisall.anyware.core.service.setting.AppSettingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ReviewServiceImpl implements ReviewService {
    @Autowired
    private PaymentProductRepository paymentProductRepository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FileEntityRepository fileEntityRepository;
    @Autowired
    private ReviewRepository reviewRepository;
    @Autowired
    private AppSettingService appSettingService;


    @Autowired
    private PagedResourcesAssembler pagedResourcesAssembler;

    @Override
    public Boolean hasPayment(Long idRelativePaymentProduct) {
        com.logisall.anyware.core.domain.commerce.paymentproduct.PaymentProduct paymentProduct= paymentProductRepository.findById(idRelativePaymentProduct).get();
        if(paymentProduct==null){
            throw new com.logisall.anyware.core.config.exception.NotFoundException("Payment not Found");
        }
        Order relativeOrder = paymentProduct.getRelativeOrder();
        if(paymentProduct.hasReview()){
            throw new com.logisall.anyware.core.config.exception.BadRequestException("Buyer has already reviewed");
        }
        if(relativeOrder==null){
            throw new NotFoundException("Relative order not found");
        }else{

            return relativeOrder.isPaymentComplete();
        }

    }

    @Override
    @Transactional
    public com.logisall.anyware.core.domain.commerce.review.Review setValue(ReviewReqBody reviewReqBody, String email) {
        User relativeWriter = userRepository.findByEmail(email).get();

        com.logisall.anyware.core.domain.commerce.review.Review review = reviewReqBody.toReview();
        review.setRelativeWriter(relativeWriter.getRelativeBuyer());

        PaymentProduct paymentProduct = paymentProductRepository.findById(reviewReqBody.getIdRelativePaymentProduct()).get();
        review.setRelativePaymentProduct(paymentProduct);
        List<com.logisall.anyware.core.domain.file.FileEntity> fileEntities = new ArrayList<>();
        if(reviewReqBody.getImages().size() > 0) {
            for (String fileUrl : reviewReqBody.getImages()) {
                FileEntity fileEntitySave = fileEntityRepository.findOneByUrl(fileUrl);
                fileEntities.add(fileEntitySave);
            }
        }
        review.setImages(fileEntities);
        reviewRepository.save(review);
        paymentProduct.setRelativeReview(review);


        return review;
    }

    @Override
    public BigDecimal averageStar() {
        return reviewRepository.cacculateAvgStar();

    }

    @Override
    @Transactional

    public PagedModel<?> pagedResources(Locale locale, Long id, com.logisall.anyware.core.model.Filter filter) {
        //Locale defaultLocale = appSettingService.getDefaultLocale();
        Page<com.logisall.anyware.core.domain.commerce.review.Review> page = page(filter, id);
        List<com.logisall.anyware.core.model.resbody.commerce.review.ReviewResBody> list = page.getContent().stream()
                .map(review -> review.toBody(locale))
                .collect(Collectors.toList());
        Page<ReviewResBody> reviewThumbBodyPage = new PageImpl<>(list, filter.getPageable(), page.getTotalElements());

        return pagedResourcesAssembler.toModel(reviewThumbBodyPage);
    }

    @Transactional
    public Page<com.logisall.anyware.core.domain.commerce.review.Review> page(Filter filter, Long id){
        Page<com.logisall.anyware.core.domain.commerce.review.Review> page = reviewRepository.findAll(
                ReviewPredicate.getInstance()
                        .search(filter.getQuery())
                        .startDate(filter.getStartDate())
                        .endDate(filter.getEndDate())
                        .buyer(id)
                        .values(),
                filter.getPageable());

        page.forEach(review -> {
          //  review.lazy();
//      comment.setLocale(locale);
        });
        return page;
    }

    @Override
    @Transactional
    public com.logisall.anyware.core.domain.commerce.review.Review get(Long id) {
        return reviewRepository.findById(id).get();
    }


    @Override
    @Transactional
    public com.logisall.anyware.core.domain.commerce.review.Review update(com.logisall.anyware.core.domain.commerce.review.Review review) {

        if (review.getId() == null) {
            throw new BadRequestException();
        }


        return reviewRepository.findById(review.getId())
                .map(ori -> {
                    BeanUtils.copyProperties(review, ori, Review.IGNORE_PROPERTIES);
                    return reviewRepository.save(ori);
                }).orElseThrow(RuntimeException::new);
    }

    @Override
    public void delete(Long id) {

        reviewRepository.findById(id)
                .ifPresent(faq -> {
                    faq.delete();
                    reviewRepository.delete(faq);
                });
    }
}
