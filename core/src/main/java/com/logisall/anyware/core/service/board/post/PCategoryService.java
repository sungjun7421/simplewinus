package com.logisall.anyware.core.service.board.post;

import com.logisall.anyware.core.domain.board.post.Post;
import com.logisall.anyware.core.domain.board.post.category.PCategory;
import com.logisall.anyware.core.service.CategoryService;

import java.util.List;
import java.util.Locale;

public interface PCategoryService extends CategoryService<PCategory, Long> {

  List<PCategory> list(Locale locale, Post.Type type);

  boolean isDuplicate(Locale locale, String name, Post.Type type);
}
