package com.logisall.anyware.core.model.reqbody.board;

import com.logisall.anyware.core.domain.board.contact.Contact;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.model.BaseRequestBody;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

@Setter
@Getter
@ToString
public class ContactReqBody extends BaseRequestBody {

  private static final long serialVersionUID = -1915017244895496399L;

  @Schema(description = "성명", example = "홍길동", minLength = 2, maxLength = 10)
  private String fullName;

  @Pattern(regexp = ValidUtils.PATTERN_MOBILE)
  @Schema(description = "휴대폰 번호", example = "01011112222", minLength = 10, maxLength = 11)
  private String mobile;

  @Email
  @Schema(description = "이메일 주소", example = "test@logisall.com", minLength = 3, maxLength = 255)
  private String email;

  @Schema(description = "문의 내용", example = "this is the content", minLength = 10, maxLength = 500)
  private String content;

  @Schema(description = "개인정보 수집 동의",example = "true")
  private boolean privacyAgree;

  public com.logisall.anyware.core.domain.board.contact.Contact toContact(User user) {
    com.logisall.anyware.core.domain.board.contact.Contact contact = new Contact();
//    contact.setCountryCode(this.countryCode);
    contact.setFullName(this.fullName);
    contact.setMobile(this.mobile);
    contact.setEmail(this.email);
    contact.setContent(this.content);
    contact.setPrivacyAgree(this.privacyAgree);
    if(user != null) {
      contact.setRelativeUser(user);
    }
    return contact;
  }
}
