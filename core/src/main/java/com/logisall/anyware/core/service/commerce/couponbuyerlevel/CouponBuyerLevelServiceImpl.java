package com.logisall.anyware.core.service.commerce.couponbuyerlevel;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevelRepository;
import com.logisall.anyware.core.domain.commerce.coupon.Coupon;
import com.logisall.anyware.core.domain.commerce.coupontobuyerlevel.CouponBuyerLevel;
import com.logisall.anyware.core.domain.commerce.coupontobuyerlevel.CouponBuyerLevelRepository;
import com.logisall.anyware.core.service.commerce.couponbuyerlevel.CouponBuyerLevelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class CouponBuyerLevelServiceImpl implements CouponBuyerLevelService {

    @Autowired
    private CouponBuyerLevelRepository couponBuyerLevelRepository;

    @Autowired
    private BuyerLevelRepository buyerLevelRepository;

    @Override
    @Transactional
    public CouponBuyerLevel create(Coupon coupon, BuyerLevel buyerLevel) {
        if(buyerLevel == null) {
            throw new BadRequestException();
        }

        CouponBuyerLevel couponBuyerLevel = new CouponBuyerLevel(coupon, buyerLevel);
        return couponBuyerLevelRepository.save(couponBuyerLevel);
    }

    @Override
    @Transactional
    public List<CouponBuyerLevel> list(Long idCoupon) {
        return couponBuyerLevelRepository.findAllByRelativeCoupon_Id(idCoupon);
    }
}
