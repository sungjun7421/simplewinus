package com.logisall.anyware.core.domain.board.post;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.time.LocalDateTime;
import java.util.List;

public interface PostRepository extends
        JpaRepository<com.logisall.anyware.core.domain.board.post.Post, Long>,
        QuerydslPredicateExecutor<com.logisall.anyware.core.domain.board.post.Post>,
    PostRepositoryCustom {

    @Query("select p.id from Post p where p.regDate < ?1 and p.type = ?2 order by p.regDate desc")
    List<Long> previous(LocalDateTime regDate, com.logisall.anyware.core.domain.board.post.Post.Type type, Pageable pageable);

    @Query("select p.id from Post p where p.regDate > ?1 and p.type = ?2 order by p.regDate asc")
    List<Long> next(LocalDateTime regDate, Post.Type type, Pageable pageable);
}
