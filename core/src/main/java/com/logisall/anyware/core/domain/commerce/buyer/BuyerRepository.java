package com.logisall.anyware.core.domain.commerce.buyer;

import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface BuyerRepository extends
    JpaRepository<Buyer, Long>,
    QuerydslPredicateExecutor<Buyer> {

        Buyer findByRelativeUser_Email(String email);
        Buyer findByRelativeUser_Id(Long idUser);
}
