package com.logisall.anyware.core.domain.commerce.point;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface PointRepository extends
    JpaRepository<com.logisall.anyware.core.domain.commerce.point.Point, Long>,
    QuerydslPredicateExecutor<com.logisall.anyware.core.domain.commerce.point.Point>,
    PointRepositoryCustom {
    com.logisall.anyware.core.domain.commerce.point.Point findByStatusAndRelativeBuyer_IdAndRelativeOrder_Id(Point.Status status, Long idBuyer, Long idOrder);
}
