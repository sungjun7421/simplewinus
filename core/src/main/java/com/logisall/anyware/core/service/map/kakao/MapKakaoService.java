package com.logisall.anyware.core.service.map.kakao;

import java.util.Map;
import java.util.concurrent.Future;

public interface MapKakaoService {

  enum CategoryType {
    MT1, //	대형마트
    CS2, //	편의점
    PS3, //	어린이집, 유치원
    SC4, //	학교
    AC5, //	학원
    PK6, //	주차장ø
    OL7, //	주유소, 충전소
    SW8, //	지하철역
    BK9, //	은행
    CT1, //	문화시설
    AG2, //	중개업소
    PO3, //	공공기관
    AT4, //	관광명소
    AD5, //	숙박
    FD6, //	음식점
    CE7, //	카페
    HP8, //	병원
    PM9, //	약국
  }


  /**
   * 주소검색
   * @param query (필수) 검색을 원하는 질의어
   * @param page 결과 페이지 번호, 1-45 사이, 기본 값 1
   * @param size 한 페이지에 보여질 문서의 개수, 1-30 사이, 기본 값 10
   * @return
   */
  Future<Map<String,Object>> address(String query, Integer page, Integer size);

  /**
   * 좌표로 행정구역정보 받기
   * @param x (필수) x 좌표로 경위도인 경우 longitude
   * @param y (필수) y 좌표로 경위도인 경우 latitude
   * @param inputCoord x, y 로 입력되는 값에 대한 좌표 체계, 기본 값은 WGS84 지원 좌표계: WGS84, WCONGNAMUL, CONGNAMUL, WTM, TM
   * @param outputCoord 결과에 출력될 좌표 체계, 기본 값은 WGS84 지원 좌표계: WGS84, WCONGNAMUL, CONGNAMUL, WTM, TM
   * @return
   */
  Future<Map<String,Object>> coord2regioncode(Double x, Double y, String inputCoord, String outputCoord);

  /**
   * 좌표로 주소 변환하기
   * @param x (필수) x 좌표로 경위도인 경우 longitude
   * @param y (필수) y 좌표로 경위도인 경우 latitude
   * @param inputCoord x, y 로 입력되는 값에 대한 좌표 체계, 기본 값은 WGS84 지원 좌표계: WGS84, WCONGNAMUL, CONGNAMUL, WTM, TM
   * @return
   */
  Future<Map<String,Object>> coord2address(Double x, Double y, String inputCoord);

  /**
   * 좌표계 변환
   * @param x (필수) x 좌표로 경위도인 경우 longitude
   * @param y (필수) y 좌표로 경위도인 경우 latitude
   * @param inputCoord x, y 로 입력되는 값에 대한 좌표 체계, 기본 값은 WGS84 지원 좌표계: WGS84, WCONGNAMUL, CONGNAMUL, WTM, TM
   * @param outputCoord 결과에 출력될 좌표 체계, 기본 값은 WGS84 지원 좌표계: WGS84, WCONGNAMUL, CONGNAMUL, WTM, TM
   * @return
   */
  Future<Map<String,Object>> transcoord(Double x, Double y, String inputCoord, String outputCoord);

  /**
   * 키워드로 장소 검색
   * @param query (필수) 검색을 원하는 질의어
   * @param categoryGroupCode 카테고리 그룹 코드 결과를 카테고리로 필터링을 원하는 경우 사용
   * @param x 중심 좌표의 X값 혹은 longitude 특정 지역을 중심으로 검색하려고 할 경우 radius와 함께 사용 가능
   * @param y 중심 좌표의 Y값 혹은 latitude 특정 지역을 중심으로 검색하려고 할 경우 radius와 함께 사용 가능
   * @param radius 중심 좌표부터의 반경거리. 특정 지역을 중심으로 검색하려고 할 경우 중심좌표로 쓰일 x,y와 함께 사용 단위 meter, 0~20000 사이의 값
   * @param rect 사각형 범위내에서 제한 검색을 위한 좌표. 지도 화면 내 검색시 등 제한 검색에서 사용 가능 좌측 X 좌표,좌측 Y 좌표, 우측 X 좌표, 우측 Y 좌표 형식
   * @param page 결과 페이지 번호, 1~45 사이의 값, 기본 값 1
   * @param size 한 페이지에 보여질 문서의 개수, 1~15 사이의 값, 기본 값 15
   * @param sort 결과 정렬 순서, distance 정렬을 원할 때는 기준 좌표로 쓰일 x, y와 함께 사용 distance 또는 accuracy, 기본 accuracy
   * @return
   */
  Future<Map<String,Object>> keyword(String query, CategoryType categoryGroupCode, Double x, Double y, Integer radius, String rect, Integer page, Integer size, String sort);

  /**
   * 카테고리로 장소 검색
   * @param categoryGroupCode (필수) 카테고리 코드
   * @param x ((x,y,radius) 또는 rect 필수) 중심 좌표의 X값 혹은 longitude 특정 지역을 중심으로 검색하려고 할 경우 radius와 함께 사용 가능.
   * @param y ((x,y,radius) 또는 rect 필수) 중심 좌표의 Y값 혹은 latitude 특정 지역을 중심으로 검색하려고 할 경우 radius와 함께 사용 가능.
   * @param radius 	((x,y,radius) 또는 rect 필수) 중심 좌표부터의 반경거리. 특정 지역을 중심으로 검색하려고 할 경우 중심좌표로 쓰일 x,y와 함께 사용. 단위 meter, 0~20000 사이의 값
   * @param rect 사각형 범위내에서 제한 검색을 위한 좌표 지도 화면 내 검색시 등 제한 검색에서 사용 가능 좌측 X 좌표, 좌측 Y 좌표, 우측 X 좌표, 우측 Y 좌표 형식 x, y, radius 또는 rect 필수
   * @param page 결과 페이지 번호, 1-45 사이의 값, 기본 값 1
   * @param size 한 페이지에 보여질 문서의 개수, 1-15 사이의 값, 기본 값 15
   * @param sort 결과 정렬 순서, distance 정렬을 원할 때는 기준좌표로 쓰일 x, y 파라미터 필요 "distance" 또는 "accuracy", 기본 값 "accuracy"
   * @return
   */
  Future<Map<String,Object>> category(CategoryType categoryGroupCode, Double x, Double y, Integer radius, String rect, Integer page, Integer size, String sort);
}
