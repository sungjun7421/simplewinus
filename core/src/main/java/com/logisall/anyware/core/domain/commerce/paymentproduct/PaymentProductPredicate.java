package com.logisall.anyware.core.domain.commerce.paymentproduct;

import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class PaymentProductPredicate {

  private static final QPaymentProduct Q_PAYMENT_PRODUCT = QPaymentProduct.paymentProduct;

  public static Predicate search(String value) {

    if (StringUtils.isEmpty(value)) {
      return null;
    }

    value = value.trim();

    return Q_PAYMENT_PRODUCT.relativeOrder.buyerInfo.fullName.containsIgnoreCase(value)
      .or(Q_PAYMENT_PRODUCT.relativeOrder.buyerInfo.mobile.containsIgnoreCase(value))
      .or(Q_PAYMENT_PRODUCT.relativeOrder.buyerInfo.email.containsIgnoreCase(value));
  }

  public static Predicate startDate(final LocalDateTime startDate) {

    if (startDate == null) {
      return null;
    }
    return Q_PAYMENT_PRODUCT.createdDate.goe(startDate);
  }

  public static Predicate endDate(final LocalDateTime endDate) {

    if (endDate == null) {
      return null;
    }
    return Q_PAYMENT_PRODUCT.createdDate.loe(endDate);
  }

  public static Predicate statuses(final List<PaymentProduct.Status> statuses) {

    if (statuses != null) {
      return ExpressionUtils.anyOf(statuses.stream().filter(Objects::nonNull).map(Q_PAYMENT_PRODUCT.status::eq).collect(Collectors.toList()));
    }
    return null;
  }

  public static Predicate neOrderStatus(OrderStatus... orderStatuses) {
    if (orderStatuses == null) {
      return null;
    }
    return ExpressionUtils.allOf(
      Arrays.stream(orderStatuses).map(Q_PAYMENT_PRODUCT.relativeOrder.orderStatus::ne).collect(Collectors.toList())
    );
  }

}
