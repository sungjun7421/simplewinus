package com.logisall.anyware.core.service.send.sms;

import com.logisall.anyware.core.domain.sms.SMSHistory;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.DomainService;
import org.springframework.data.domain.Page;

public interface SMSHistoryService extends DomainService<SMSHistory, Long> {

    // Page
    Page<SMSHistory> page(Filter filter, SMSHistory.Code code);
}
