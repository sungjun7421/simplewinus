package com.logisall.anyware.core.model.resbody.setting;

import com.logisall.anyware.core.domain.International.InternationalMode;
import com.logisall.anyware.core.model.BaseResponseBody;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.util.Locale;

@Relation(value = "setting", collectionRelation = "settings")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AppSettingResBody extends BaseResponseBody {

  private static final long serialVersionUID = 2411225505181969676L;

  private Locale defaultLocale;
  private boolean international; // 국제화 모드
  private InternationalMode internationalMode;
}
