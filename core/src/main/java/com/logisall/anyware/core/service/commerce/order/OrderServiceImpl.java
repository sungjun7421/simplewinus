package com.logisall.anyware.core.service.commerce.order;


import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.NotFoundException;
import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.commerce.buyer.BuyerRepository;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.domain.commerce.order.OrderPredicate;
import com.logisall.anyware.core.domain.commerce.order.OrderRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.resbody.commerce.order.OrderDetailResBody;
import com.logisall.anyware.core.service.commerce.buyer.BuyerService;
import com.logisall.anyware.core.utils.DateUtils;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private BuyerRepository buyerRepository;

    @Autowired
    private BuyerService buyerService;





    @Override
    @Transactional
    public Page<com.logisall.anyware.core.domain.commerce.order.Order> page(Filter filter,
                                                                            com.logisall.anyware.core.domain.commerce.OrderStatus orderStatuses,
                                                                            String productName,
                                                                            String memberQuery) {

        Page<com.logisall.anyware.core.domain.commerce.order.Order> page = orderRepository.findAll(
                OrderPredicate.getInstance()
                    .search(filter.getQuery())
                    .startDate(filter.getStartDate())
                    .endDate(filter.getEndDate())
                    .status(orderStatuses)
                    .memberSearch(memberQuery)
                    .values(),
                filter.getPageable());

        page.forEach(
                com.logisall.anyware.core.domain.commerce.order.Order::lazy);
        return page;
    }

    @Override
    @Transactional
    public com.logisall.anyware.core.domain.commerce.order.Order get(Long id) {

        return orderRepository.findById(id)
                .map(order -> {
                    order.lazy();
                    return order;
                }).orElseThrow(com.logisall.anyware.core.config.exception.BadRequestException::new);
    }

    @Override
    @Transactional
    public com.logisall.anyware.core.domain.commerce.order.Order get(String oid) {
        return Optional.ofNullable(orderRepository.findByOid(oid).get())
                .map(order -> {
                    order.lazy();
                    return order;
                }).orElse(null);
    }


    @Override
    @Transactional
    public com.logisall.anyware.core.domain.commerce.order.Order updateByOrderStatus(Long id, com.logisall.anyware.core.domain.commerce.OrderStatus orderStatus) {
        if (orderStatus == null) {
            throw new com.logisall.anyware.core.config.exception.BadRequestException();
        }

        return orderRepository.findById(id)
                .map(order -> {
                    switch (orderStatus) {
                        case INIT:
                        case COMPLETE:
                            if (order.getPaymentDate() == null) {
                                order.setPaymentDate(LocalDateTime.now());
                            }
                            order.setPaymentComplete(true);
                            if (Objects.equals(orderStatus, com.logisall.anyware.core.domain.commerce.OrderStatus.V_READY)) {
                                if (order.getPaymentDate() == null) {
                                    order.setPaymentDate(LocalDateTime.now());
                                }
                            }
                            break;
                        case FAILED:
                            order.setPaymentComplete(false);
                            break;
                        default:
                            break;
                    }
                    order.setOrderStatus(orderStatus);
                    return orderRepository.save(order);
                }).orElseThrow(BadRequestException::new);
    }

    @Override
    public String generateOid() {
        String oid = String.format("X%s%s", DateUtils.getDateString(), StringUtils.generateRandomString(5));

        if (orderRepository.existsByOid(oid)) {
            return generateOid();
        }
        return oid;
    }
//
//    @Override
//    @Transactional
//    public Order order(Order order, String email) {
//
//        // 회원
//        Buyer buyer = org.apache.commons.lang3.StringUtils.isEmpty(email) ? null : buyerRepository.findByRelativeUser_Email(email);
//
//        // 1. 구매할 상품 총합
//        BigDecimal amount = orderAmountService.totalByProductAndQty(order.getProducts());
//
//        // 결제할 금액 일치 여부 확인
//        if (order.getAmount().intValue() != amount.intValue()) {
//            throw new BadRequestException("결제할 금액이 일치하지 않습니다.\n처음부터 다시 시도해주세요.");
//        }
//
//        // 5. 결제할 금액
//        log.debug("amount ::: {}", amount);
//        order.setAmount(amount);
//
//        order.setOid(this.oid(buyer != null ? buyer.getId() : null));
//        order.setOrderStatus(OrderStatus.INIT);
//
//        if (buyer != null) {
//            buyer.getOrders().add(order);
//            order.setRelativeBuyer(buyer);
//        }
//        Order savedOrder = orderRepository.save(order);
//
//        // 결제 상품명
//        AtomicInteger atomicInt = new AtomicInteger(0);
//        StringBuffer title = new StringBuffer();
//
//        // 재고/옵션/가격 Order Product 등록
//        order.getProducts().forEach(p -> {
//
//            if (p.getId() != null && p.getId() > 0) {
//
//                if (!p.isMatched()) {
//                    log.error("({}) p.isMatched() == {}", p.getId(), p.isMatched());
//                    throw new BadRequestException("Product price calculation is incorrect.");
//                }
//
//                Optional<Product> productOpt = productRepository.findById(p.getId());
//                if (!productOpt.isPresent()) {
//                    throw new BadRequestException("Product not exist.");
//                }
//                Product product = productOpt.get();
////                PurchaseHistory purchaseHistory = p.toPurchaseHistory(savedOrder, product);
////                purchaseHistory.setProductName(product.getName().getValue());
////                purchaseHistory.setComposition(product.getComposition().getValue());
////                purchaseHistory.setDate1(product.getDate1());
////                purchaseHistory.setDate2(product.getDate2());
////                PurchaseHistory savedPurchaseHistory = purchaseHistoryRepository.save(purchaseHistory);
//
////                log.debug("savedPurchaseHistory ::: {}", savedPurchaseHistory);
////                log.debug("savedPurchaseHistory.isMatched() ::: {}", savedPurchaseHistory.isMatched());
//
//                if (atomicInt.get() == 0) {
//                    title.append(product.getInformation().getName().getValue());
//                }
//                atomicInt.incrementAndGet();
//            }
//        });
//
//        if (atomicInt.get() > 1) {
//            title.append(" 외 ").append(atomicInt.get() - 1).append("개");
//        }
//
//        savedOrder.setTitle(title.toString());
//        return savedOrder;
//    }

    @Override
    public com.logisall.anyware.core.domain.commerce.order.Order success(com.logisall.anyware.core.domain.commerce.order.Order order) {
        // 결제 완료
        order.setOrderStatus(OrderStatus.COMPLETE); // 결제완료
        order.setPaymentDate(LocalDateTime.now()); // 결제 시간
        order.setPaymentComplete(true); // 결제 성공

//        order.getPurchaseHistories().forEach(purchaseHistory -> {
//            purchaseHistory.setStatus(PurchaseHistory.Status.CONFIRM);
//        });

//        if (order.getBuyerInfo().getMobile().startsWith("01")) {
//
//            //          SMS sms = new SMS();
////          String msgBody = String.format("[%s] %s 이 주문이 완료 되었습니다.\n", sendName, order.getTitle());
////          msgBody += String.format("주문번호: %s", order.getOid());
////          sms.setMsgBody(msgBody);
////          sms.setDestName(order.getBuyerInfo().getFullname());
////          sms.setDestPhone(order.getBuyerInfo().getPhone());
////          sms.setType(SMS.Type.LMS);
////          smsService.send(sms);
//
//            kkoService.send(kkoTemplate.toTemplate002(order.getBuyerInfo().getPhone(), order.getOid()));
//        }

        // 주문완료 메일 발송
//        if (!org.springframework.util.StringUtils.isEmpty(order.getBuyerInfo().getEmail())) {
//
//            int totalOrderAmount = order.getAmount().intValue();
//
//            String subject = "고객님께서 주문하신 상품이 결제완료 되었습니다.";
//            String fullname = order.getBuyerInfo().getFullName();
//            String email = order.getBuyerInfo().getEmail();
//
//            Map<String, Object> model = new HashMap<>();
//            model.put("subject", subject);
//            model.put("order", order);
//            model.put("fullname", fullname);
//            model.put("totalOrderAmount", totalOrderAmount);
////            model.put("discountAmount", totalOrderAmount - order.getTotal());
//            model.put("nowDate", LocalDate.now());
//            model.put("email", email);
//            //model.put("metaEmail", metaEmail);
//
//            //emailService.send(email, subject, model, "email/order-complete.ftl");
//        }

        log.info("success oid ::: {}", order.getOid());

        // Cart 삭제
       // cartService.deleteByOrder(order);
        //log.info("DELETE CART");
        orderRepository.save(order);
        return order;
    }

    @Override
    public String oid(Long idBuyer) {
        DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yMMddHHmmss");
        return "o" + "-" + LocalDateTime.now().format(FORMATTER) + (idBuyer != null ? idBuyer : "0");
    }

    @Override
    @Transactional
    public com.logisall.anyware.core.model.resbody.commerce.order.OrderDetailResBody getDetailOrder(Long id, Locale locale) {
        if(!orderRepository.existsById(id)){
            throw new NotFoundException("Order not found");
        }
        Order order = orderRepository.findById(id).get();
        OrderDetailResBody orderDetailResBody =order.toBodyOrderDetail();
        return orderDetailResBody;
    }
}
