package com.logisall.anyware.core.service.user;

import com.logisall.anyware.core.domain.user.Authority;
import com.logisall.anyware.core.domain.user.AuthorityRepository;
import com.logisall.anyware.core.service.user.AuthorityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AuthorityServiceImpl implements AuthorityService {

  @Autowired
  private AuthorityRepository authorityRepository;

  @Override
  public List<Authority> list() {
    List<Authority> authorities = authorityRepository.findAll();
    return authorities.stream().sorted(Authority::compareTo).collect(Collectors.toList());
  }

  @Override
  public List<Authority> list(Authority.Role... roles) {
    return authorityRepository.listByRoles(roles);
  }
}
