package com.logisall.anyware.core.model.reqbody.commerce.order;

import com.logisall.anyware.core.model.BaseRequestBody;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class OrderPriceReqBody extends BaseRequestBody {

    private static final long serialVersionUID = 4603157751700315175L;

    private String oid; // 주문번호

    private Long idCoupon; // 주문쿠폰
    private Double point; // 사용 포인트
}
