package com.logisall.anyware.core.service.aws;

import com.amazonaws.AmazonClientException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.logisall.anyware.core.model.aws.SESSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.internet.MimeUtility;
import java.util.concurrent.Future;

@Slf4j
@Service
@Transactional
public class AWSSESServiceImpl implements AWSSESService {

  @Autowired
  private AWSCredentialsService awsCredentialsService;

  @Value("${aws.ses.sender.email}")
  private String from;

  @Value("${aws.ses.sender.name}")
  private String name;

  private AmazonSimpleEmailService client() {

    return AmazonSimpleEmailServiceClientBuilder.standard()
        .withCredentials(awsCredentialsService.credentials())
        .withRegion(Regions.AP_NORTHEAST_2).build();
  }

  @Async
  @Override
  public Future<String> send(SESSender sesSender) {
    log.debug("> sesSender ::: {}", sesSender);

    try {
      final String fromName = MimeUtility.encodeText(name); // RFC 2047 - UTF-8 인코딩 방식
      sesSender.setFrom(String.format("\"%s\" <%s>", fromName, from));
      SendEmailResult result = this.client().sendEmail(sesSender.toSendRequestDto());
      return new AsyncResult<>(result.getMessageId());
    } catch (Exception ex) {
      log.error("AWS SES Error", ex);
      throw new AmazonClientException(ex.getMessage(), ex);
    }
  }
}
