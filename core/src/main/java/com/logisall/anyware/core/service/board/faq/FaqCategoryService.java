package com.logisall.anyware.core.service.board.faq;


import com.logisall.anyware.core.domain.board.faq.category.FaqCategory;
import com.logisall.anyware.core.service.CategoryService;

public interface FaqCategoryService extends CategoryService<FaqCategory, Long> {

}
