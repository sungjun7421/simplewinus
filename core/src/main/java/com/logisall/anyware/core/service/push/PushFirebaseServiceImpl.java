package com.logisall.anyware.core.service.push;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.*;
import com.logisall.anyware.core.model.firebase.NotificationRequest;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.concurrent.ExecutionException;


@Slf4j
@Service
public class PushFirebaseServiceImpl implements PushFirebaseService {



  @Value("${firebase.accountKey}")
  public String accountKey;

  @Value("${firebase.databaseUrl}")
  public String databaseUrl;

  @PostConstruct
  public void init() {

    try {
      log.info("# FCM Google Key Set");
      ClassPathResource classPathResource = new ClassPathResource(accountKey);
      if (!classPathResource.exists()) {
        log.error("# Invalid filePath : {}", classPathResource);
        throw new IllegalArgumentException();
      }
      FirebaseOptions options = FirebaseOptions.builder()
          .setCredentials(GoogleCredentials.fromStream(classPathResource.getInputStream()))
          .setDatabaseUrl(databaseUrl)
          .build();

      if (FirebaseApp.getApps().isEmpty()) {
        log.info("# Firebase application has been initialized");
        FirebaseApp.initializeApp(options);
      }
    } catch (IOException e) {
      log.error("# Firebase Init Error", e);
    }
  }

  public void send(final NotificationRequest notificationRequest) throws InterruptedException, ExecutionException {

    log.debug("# NotificationRequest : " + notificationRequest);
    final Message message = Message.builder()
        .setToken(notificationRequest.getToken())
        .setWebpushConfig(WebpushConfig.builder()
            .putHeader("ttl", "300")
            .build())
        .setNotification(Notification.builder()
            .setTitle(notificationRequest.getTitle())
            .setBody(notificationRequest.getMessage())
            .build())
        .build();

    String response = null;
    try {
      response = FirebaseMessaging.getInstance().send(message);
    } catch (FirebaseMessagingException e) {
      log.error("# FCM Sent Error", e);
    }
    log.debug("# FCM Sent token : " + notificationRequest.getToken());
    log.debug("# FCM Sent response: " + response);

    try {
      // TODO Log 파일 위치 변경
      Logger logger = LoggerFactory.getLogger(PushFirebaseServiceImpl.class);
      logger.info("# request : {}, response : {}", notificationRequest, response);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
