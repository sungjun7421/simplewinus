package com.logisall.anyware.core.domain.commerce.category;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logisall.anyware.core.domain.AbstractEntityCategory;
import com.logisall.anyware.core.domain.RestEntityBody;
import com.logisall.anyware.core.domain.commerce.product.Product;
import com.logisall.anyware.core.model.resbody.board.CategoryResBody;
import com.logisall.anyware.core.utils.DateUtils;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

@Entity(name = "ProductCategory")
@Getter
@Setter
@ToString(exclude = {"products"})
public class Category extends AbstractEntityCategory<Long> implements RestEntityBody<com.logisall.anyware.core.model.resbody.board.CategoryResBody> {

    private static final long serialVersionUID = 6299204074895941676L;

    public static String[] IGNORE_PROPERTIES = {
            "id",
            "orderAscending",
            "products"};

    @Id
    @GeneratedValue
    @Column(unique = true)
    private Long id;

    @Column
    private String image;

    @DateTimeFormat(pattern = com.logisall.anyware.core.utils.DateUtils.FORMAT_DATE_UNIT_BAR)
    private LocalDate startDate; // 시작일

    @DateTimeFormat(pattern = DateUtils.FORMAT_DATE_UNIT_BAR)
    private LocalDate endDate; // 종료일

    public String getRangeDate() {

        String range = "";
        if (com.logisall.anyware.core.utils.StringUtils.isNotEmpty(startDate)) {
            range = startDate.format(DateTimeFormatter.ofPattern("MM/dd"));
        }
        range += " ~ ";
        if (StringUtils.isNotEmpty(endDate)) {
            range += endDate.format(DateTimeFormatter.ofPattern("MM/dd"));
        }
        return range;
    }

    @JsonIgnore
    @OrderBy("orderDescending desc")
    @ManyToMany(mappedBy = "categories")
    private List<Product> products = new ArrayList<>();

    @Override
    public void delete() {
        if (this.products != null) {
            this.products.forEach(product -> {
                product.getCategories().removeIf(category -> Objects.equals(category.getId(), this.id));
            });
        }
    }

    @Override
    public void lazy() {
        if (this.products != null) {
            this.products.size();
        }
    }

    @Override
    public com.logisall.anyware.core.model.resbody.board.CategoryResBody toBody(Locale locale) {
        this.setLocale(locale);

        return CategoryResBody.builder()
                .id(this.id)
                .name(this.name.getValue())
                .image(this.image)
                .build();
    }
}
