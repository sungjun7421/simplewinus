package com.logisall.anyware.core.service.account;

import com.logisall.anyware.core.domain.user.sns.SNSStatus;
import com.logisall.anyware.core.model.sns.Kakao;

import java.util.Map;

public interface SNSKakaoAPIService {

  Map getAccessToken(String code, String redirectUri);

  Map getMe(String accessToken);

  SNSStatus getStatus(Kakao kakao);
}
