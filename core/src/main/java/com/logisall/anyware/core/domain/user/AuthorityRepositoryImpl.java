package com.logisall.anyware.core.domain.user;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AuthorityRepositoryImpl implements AuthorityRepositoryCustom {

  @Autowired
  private JPAQueryFactory jpaQueryFactory;

  @Override
  public List<com.logisall.anyware.core.domain.user.Authority> listByRoles(Authority.Role... roles) {
    QAuthority qAuthority = QAuthority.authority;

    return jpaQueryFactory.selectFrom(qAuthority)
        .where(qAuthority.role.in(roles))
        .fetch();
  }
}
