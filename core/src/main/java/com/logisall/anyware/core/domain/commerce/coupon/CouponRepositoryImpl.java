package com.logisall.anyware.core.domain.commerce.coupon;

import com.logisall.anyware.core.domain.commerce.buyer.QBuyer;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Slf4j
public class CouponRepositoryImpl implements CouponRepositoryCustom {

  @Autowired
  private JPAQueryFactory jpaQueryFactory;

  @Override
  public List<Coupon> findAllByUser(Long idUser, Boolean isUsed){
    QCoupon qCoupon = QCoupon.coupon;
    QBuyer qBuyer = QBuyer.buyer;
    if(isUsed == null){
      return jpaQueryFactory.selectFrom(qCoupon)
//              .innerJoin(qCoupon.relativeBuyer, qBuyer)
              .where(
                      com.logisall.anyware.core.domain.commerce.coupon.CouponPredicate.getInstance()
//                              .buyer(idUser)
                              .values()
              )
              .orderBy(qCoupon.createdDate.desc())
              .fetchResults()
              .getResults();
    }else {
      return jpaQueryFactory.selectFrom(qCoupon)
//              .innerJoin(qCoupon.relativeBuyer, qBuyer)
              .where(
                      CouponPredicate.getInstance()
//                              .buyer(idUser)
                              .values()
              )
              .orderBy(qCoupon.createdDate.desc())
              .fetchResults()
              .getResults();
    }
  }
}
