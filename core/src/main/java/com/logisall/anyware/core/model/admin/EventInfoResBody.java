package com.logisall.anyware.core.model.admin;

import com.logisall.anyware.core.domain.International.InterText;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.server.core.Relation;

@Relation(value = "eventInfo")
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EventInfoResBody {

  private Long id;
  private com.logisall.anyware.core.domain.International.InterText content;
  private InterText title;
}
