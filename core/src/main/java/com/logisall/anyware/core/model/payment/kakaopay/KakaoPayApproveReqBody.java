package com.logisall.anyware.core.model.payment.kakaopay;

import com.logisall.anyware.core.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import okhttp3.FormBody;

import java.io.Serializable;
import java.nio.charset.Charset;

@Getter
@Setter
@ToString
public class KakaoPayApproveReqBody implements Serializable {

  private static final long serialVersionUID = 6972699530776080929L;

  private String tid; // 결제 고유번호. 결제준비 API의 응답에서 얻을 수 있음
  private String pg_token; // 결제승인 요청을 인증하는 토큰. 사용자가 결제수단 선택 완료시 approval_url로 redirection해줄 때 pg_token을 query string으로 넘겨줌

  private int total_amount; // 상품총액. 결제준비 API에서 요청한 total_amount 값과 일치해야 함

  public boolean isBadRequest() {
    return StringUtils.isEmpty(this.tid)
      || StringUtils.isEmpty(this.pg_token)
      || this.total_amount == 0;
  }

  public FormBody toFormBody(String cid, String partner_order_id, String partner_user_id) {
    return new FormBody.Builder(Charset.forName("UTF-8"))
      .add("cid", cid)
      .add("tid", tid)
      .add("partner_order_id", partner_order_id)
      .add("partner_user_id", partner_user_id)
      .add("pg_token", pg_token)
      .build();
  }
}
