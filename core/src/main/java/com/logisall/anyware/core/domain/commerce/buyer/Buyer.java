package com.logisall.anyware.core.domain.commerce.buyer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel;
import com.logisall.anyware.core.domain.commerce.cart.Cart;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.domain.commerce.point.Point;
import com.logisall.anyware.core.domain.commerce.wish.Wish;
import com.logisall.anyware.core.domain.user.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Entity(name = "Buyer")
@Getter
@Setter
@ToString(exclude = {
        "relativeUser",
        "orders",
        "points",
        "relativeBuyerLevel",
})
public class Buyer extends AbstractEntity<Long> {

    private static final long serialVersionUID = 8745083741037568722L;

    @PrePersist
    public void prePersist() {
    }

    @Id
    private Long id; // PK

    @OneToOne(optional = false)
    @MapsId
    @JoinColumn(name = "id", foreignKey = @ForeignKey(name = "FK_User_For_Buyer"))
    private User relativeUser; // 사용자

    @OneToMany(mappedBy = "relativeBuyer", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("createdDate DESC")
    private List<Point> points = new ArrayList<>(); // 적립금 사용 내역

    @JsonIgnore
    @OneToMany(mappedBy = "relativeBuyer", cascade = CascadeType.REMOVE, orphanRemoval = true)
    @OrderBy("paymentDate DESC")
    private List<Order> orders = new ArrayList<>(); // 주문 내역

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "idBuyerLevel", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Buyer_Level_For_Buyer")) // Column name, 참조하는 ID(pk) name
    private BuyerLevel relativeBuyerLevel; // 회원등급

    @JsonIgnore
    @OrderBy("createdDate DESC")
    @OneToMany(mappedBy = "relativeBuyer", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Wish> wishList; // 위시리스트

    @JsonIgnore
    @OrderBy("createdDate DESC")
    @OneToMany(mappedBy = "relativeBuyer", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Cart> cartList; //== 장바구니

    @Override
    public void delete() {

    }

    @Override
    public void lazy() {
        if (this.cartList != null) {
            this.cartList.size();
        }
        if(this.wishList!=null){
            this.wishList.size();
        }
        if (this.relativeBuyerLevel != null) {
            this.relativeBuyerLevel.getId();
        }
        if (this.relativeUser != null) {
            this.relativeUser.getId();
        }
        if (this.orders != null) {
            this.orders.size();
        }
        if (this.points != null) {
            this.points.size();
        }
    }
}
