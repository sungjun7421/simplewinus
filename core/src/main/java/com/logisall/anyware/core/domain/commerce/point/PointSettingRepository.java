package com.logisall.anyware.core.domain.commerce.point;

import com.logisall.anyware.core.domain.commerce.point.PointSetting;
import com.logisall.anyware.core.domain.commerce.point.PointSettingRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface PointSettingRepository extends
    JpaRepository<PointSetting, Long>,
    QuerydslPredicateExecutor<PointSetting>,
        PointSettingRepositoryCustom {

  PointSetting getById(Long id);
}
