package com.logisall.anyware.core.config.property.commerce.iamport;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class IPRest implements Serializable {

    private static final long serialVersionUID = -8657789622819959587L;

    private String imp_key;
    private String imp_secret;
}
