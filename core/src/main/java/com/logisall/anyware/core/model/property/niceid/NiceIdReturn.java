package com.logisall.anyware.core.model.property.niceid;

import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NiceIdReturn implements Serializable {

  private static final long serialVersionUID = 6419246223673407570L;

  private String message;
  private String encData;
  private String requestNumber;
}
