package com.logisall.anyware.core.service.commerce.product;

import com.logisall.anyware.core.domain.commerce.product.Product;
import com.logisall.anyware.core.model.Filter;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Locale;

public interface ProductService {

  // CUD
  Product create(Product Product);
  Product update(Product Product);
  void delete(Long id);

  // R
  Product get(Locale locale, Long id);
  Page<Product> page(Locale locale, Filter filter, Boolean isActive);
  void changeOrder(Long id, String mode);

  // 구매 가능한 상품
  List<Product> listByAvailableForPurchase(long qty);

  // 소유자의 판매 중인 티켓
  List<Product> listByOnSaleAndOwner(Long idUser);

  // 소유자의 사용가능한 티켓
  List<Product> listByAvailableAndOwner(Long idUser);
  Page<Product> page(Locale locale, Filter filter, Long idCategory, Boolean isActive);
}
