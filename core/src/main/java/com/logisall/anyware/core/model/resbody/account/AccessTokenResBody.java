package com.logisall.anyware.core.model.resbody.account;

import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.io.Serializable;

@Relation(value = "accessToken")
@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AccessTokenResBody implements Serializable {
  private static final long serialVersionUID = -4251072459954904170L;

  private String access_token;
  private Long expires_in;
  private String jti;
  private String refresh_token;
  private String scope;
  private String token_type;
}
