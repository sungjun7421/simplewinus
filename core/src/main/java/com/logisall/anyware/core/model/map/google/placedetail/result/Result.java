package com.logisall.anyware.core.model.map.google.placedetail.result;



import com.logisall.anyware.core.model.map.google.placedetail.result.components.Address;
import com.logisall.anyware.core.model.map.google.placedetail.result.geometry.Geometry;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.List;

@Slf4j
@Setter
@Getter
@ToString
public class Result implements Serializable{

    private static final long serialVersionUID = 2814321484091079802L;

    private List<Address> address_components;
    private String postalCode;
    private String formatted_address;
    private String formatted_phone_number;
    private String website;
    private String vicinity;
    private String url;
    private String place_id;
    private Geometry geometry;
}
