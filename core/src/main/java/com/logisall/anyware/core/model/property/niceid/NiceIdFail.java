package com.logisall.anyware.core.model.property.niceid;

import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NiceIdFail implements Serializable {

  private static final long serialVersionUID = 5869584593435988307L;

  private String cipherTime;
  private String message;
  private String errorCode;
  private String authType;
  private String requestNumber;
}
