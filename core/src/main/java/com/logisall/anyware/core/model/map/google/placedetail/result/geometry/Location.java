package com.logisall.anyware.core.model.map.google.placedetail.result.geometry;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Slf4j
@Setter
@Getter
@ToString
public class Location implements Serializable{

    private static final long serialVersionUID = -4796397623622808461L;

    private String lat;
    private String lng;
}
