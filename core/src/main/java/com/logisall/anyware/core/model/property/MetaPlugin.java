package com.logisall.anyware.core.model.property;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "app.plugin")
public class MetaPlugin implements Serializable {

    private static final long serialVersionUID = -2078082333217913598L;

    private String gaTrackingId;
    private String channelId;
    private String froalaId;
    private String jusoKey;
    private String googleMap;
    private String googlePlace;
    private String googleReCaptchaSecret;
    private String exchangeId;
}
