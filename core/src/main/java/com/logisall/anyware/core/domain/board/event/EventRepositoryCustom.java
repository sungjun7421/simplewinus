package com.logisall.anyware.core.domain.board.event;

import com.logisall.anyware.core.vo.BaseEntityInfo;

import java.util.List;
import java.util.Locale;

public interface EventRepositoryCustom {

  List<BaseEntityInfo> list(Locale locale, String query, Long idMall);
}
