package com.logisall.anyware.core.model.resbody.commerce.order;

import com.logisall.anyware.core.config.serializer.JsonLocalDateTimeDeserializer;
import com.logisall.anyware.core.config.serializer.JsonLocalDateTimeSerializer;
import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.commerce.PayMethod;
import com.logisall.anyware.core.model.BaseResponseBody;
import com.logisall.anyware.core.model.reqbody.commerce.order.PaymentPriceInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString
public class OrderResBody extends BaseResponseBody {

  private static final long serialVersionUID = -6302822553109746735L;

  private Long id;
  private String oid;
  private String fullname;
  private int savingPoint;

  private OrderStatus orderStatus;
  private PayMethod payMethod; // 결제 수단

  private String cardCompany; // 카드사
  private String cardNumByLast4; // 카드번호 마지막 4자리

  @JsonSerialize(using = JsonLocalDateTimeSerializer.class)
  @JsonDeserialize(using = JsonLocalDateTimeDeserializer.class)
  private LocalDateTime paymentDate;

  @JsonSerialize(using = JsonLocalDateTimeSerializer.class)
  @JsonDeserialize(using = JsonLocalDateTimeDeserializer.class)
  private LocalDateTime createdDate;

  private PaymentPriceInfo priceInfo;

  private boolean isMember;
  private String mobileNonMember; // 비회원 주문정보


  // 환불
  private String refundMessage; // 환불 사유
  private LocalDateTime refundDate; // 환불 신청 날짜
}
