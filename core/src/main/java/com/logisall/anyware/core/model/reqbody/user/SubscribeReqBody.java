package com.logisall.anyware.core.model.reqbody.user;

import com.logisall.anyware.core.model.BaseRequestBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class SubscribeReqBody extends BaseRequestBody {
  private static final long serialVersionUID = -6369426921362956820L;

  public enum Type {
    EMAIL, SMS, KAKAO
  }
  @Schema(description = "active", example = "true")
  private Boolean active;
}
