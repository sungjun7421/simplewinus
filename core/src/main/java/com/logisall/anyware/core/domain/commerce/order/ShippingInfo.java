package com.logisall.anyware.core.domain.commerce.order;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Setter
@Getter
@ToString
@Embeddable
public class ShippingInfo implements Serializable {

  private static final long serialVersionUID = 6170439288489564947L;

  // 배송지 정보
  @Column
  private String fullName; // 이름

  @Column
  private String mobile1; // 연락처 1

  @Column
  private String mobile2; // 연락처 2

  @Column
  private String postalCode; // 우편번호

  @Column
  private String address1; // 주소1 - 기본주소, 도로명

  @Column
  private String address2; // 주소2 - 상세주소

  @Column
  private String message; // 택배배송메세지

  public boolean isEmpty() {

    return StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(mobile1)
        || StringUtils.isEmpty(address1)
        || StringUtils.isEmpty(address2)
        ;
  }
}
