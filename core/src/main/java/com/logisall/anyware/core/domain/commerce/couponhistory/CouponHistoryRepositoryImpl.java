package com.logisall.anyware.core.domain.commerce.couponhistory;

import com.logisall.anyware.core.domain.commerce.buyer.QBuyer;
import com.logisall.anyware.core.domain.commerce.coupon.QCoupon;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.List;

@Slf4j
public class CouponHistoryRepositoryImpl implements CouponHistoryRepositoryCustom {

  @Autowired
  private JPAQueryFactory jpaQueryFactory;

  @Override
  public List<CouponHistory> getAllByUser(Long idBuyer, Boolean isUsed){
    QCoupon qCoupon = QCoupon.coupon;
    QCouponHistory qCouponHistory = QCouponHistory.couponHistory;
    QBuyer qBuyer = QBuyer.buyer;
    return jpaQueryFactory.selectFrom(qCouponHistory)
            .innerJoin(qCouponHistory.relativeBuyer, qBuyer)
            .innerJoin(qCouponHistory.relativeCoupon, qCoupon)
            .where(qCouponHistory.relativeBuyer.id.eq(idBuyer))
            .where(qCouponHistory.used.eq(isUsed))
            .where(qCoupon.startDate.before(LocalDate.now()))
            .where(qCoupon.endDate.after(LocalDate.now()))
            .orderBy(qCouponHistory.createdDate.desc())
            .fetchResults()
            .getResults();
  }
}
