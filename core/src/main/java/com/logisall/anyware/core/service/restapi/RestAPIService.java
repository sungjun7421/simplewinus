package com.logisall.anyware.core.service.restapi;

import okhttp3.Headers;
import okhttp3.RequestBody;
import org.springframework.http.ResponseEntity;

import java.net.URI;

public interface RestAPIService {

    <T> ResponseEntity<T> get(URI uri, Headers headers, Class<T> responseType) throws Exception;
    <T> ResponseEntity<T> post(URI uri, Headers headers, RequestBody requestBody, Class<T> responseType) throws Exception;
    <T> ResponseEntity<T> put(URI uri, Headers headers, RequestBody requestBody, Class<T> responseType) throws Exception;
    <T> ResponseEntity<T> patch(URI uri, Headers headers, RequestBody requestBody, Class<T> responseType) throws Exception;
    <T> ResponseEntity<T> delete(URI uri, Headers headers, RequestBody requestBody, Class<T> responseType) throws Exception;
}
