package com.logisall.anyware.core.config.exception.crud;

import com.logisall.anyware.core.config.exception.crud.CRUDException;

public class GetErrorException extends CRUDException {

  private static final long serialVersionUID = -7840105682252209337L;

  public GetErrorException(Long id, String entityName) {
    super(id, entityName, "GET ERROR");
  }
}
