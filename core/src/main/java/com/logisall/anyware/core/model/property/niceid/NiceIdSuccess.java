package com.logisall.anyware.core.model.property.niceid;

import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NiceIdSuccess implements Serializable {
  private static final long serialVersionUID = -6294525886867413404L;

  private boolean success;
  private String message;

  private String cipherTime; // 복호화한 시간 (YYMMDDHHMMSS)
  private String requestNumber; // 요청 번호
  private String responseNumber; // NICE응답 번호
  private String authType; // 인증수단
  private String name; // 성명
  private String dupInfo; // 중복가입 확인값(DI)
  private String connInfo; // 연계정보 확인값(CI)
  private String birthDate; // 생년월일(YYYYMMDD)
  private String gender; // 성별
  private String nationalInfo; // 내/외국인정보
  private String mobileNo; // 휴대폰번호
  private String mobileCo; // 통신사
}
