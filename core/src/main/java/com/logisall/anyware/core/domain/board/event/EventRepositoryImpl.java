package com.logisall.anyware.core.domain.board.event;

import com.logisall.anyware.core.domain.board.event.Event;
import com.logisall.anyware.core.domain.board.event.EventRepositoryCustom;
import com.logisall.anyware.core.utils.StringUtils;
import com.logisall.anyware.core.vo.BaseEntityInfo;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
public class EventRepositoryImpl implements EventRepositoryCustom {

  @Autowired
  private JPAQueryFactory jpaQueryFactory;

  @Override
  public List<BaseEntityInfo> list(Locale locale, String query, Long idMall) {

    log.debug("locale ::: {}", locale);
    log.debug("query ::: {}", query);

    if (StringUtils.isEmpty(query) || idMall == null) {
      return new ArrayList<>();
    }

    QEvent qEvent = QEvent.event;

    List<Event> list = jpaQueryFactory
      .selectFrom(qEvent)
      .where(qEvent.title.textKoKr.containsIgnoreCase(query)
        .or(qEvent.title.textEnUs.containsIgnoreCase(query))
        .or(qEvent.title.textJaJp.containsIgnoreCase(query))
        .or(qEvent.title.textZhCn.containsIgnoreCase(query))
        .or(qEvent.title.textZhTw.containsIgnoreCase(query))
      )
      .orderBy(qEvent.title.textKoKr.asc())
      .fetch();

    return list.stream().map(event -> {
      event.setLocale(locale);

      return BaseEntityInfo.of(event.getId(), event.getTitle().getValue());
    }).collect(Collectors.toList());
  }
}
