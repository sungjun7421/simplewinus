package com.logisall.anyware.core.service.commerce.category;

import com.logisall.anyware.core.domain.commerce.category.Category;

import java.util.List;
import java.util.Locale;

public interface CategoryService {
    // CUD
    Category create(Category category);
    Category update(Category category);
    void delete(Long id);
    void changeOrder(Long id, String mode);

    // R
    Category get(Locale locale, Long id);
    List<Category> list(Locale locale);
    boolean isDuplicate(Locale locale, String name);
}
