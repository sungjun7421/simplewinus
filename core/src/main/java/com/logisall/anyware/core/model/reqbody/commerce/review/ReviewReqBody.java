package com.logisall.anyware.core.model.reqbody.commerce.review;

import com.logisall.anyware.core.domain.commerce.review.Review;
import com.logisall.anyware.core.model.BaseRequestBody;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.List;

@Slf4j
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReviewReqBody extends BaseRequestBody {

    private Long id;
    private String title; // 제목
    private BigDecimal star;
    private String content;
    private Long idRelativePaymentProduct;
    private List<String> images;

    public Review toReview(){
        Review review = new Review();

        review.setContent(this.content);
        review.setStar(this.star);
        review.setTitle(this.title);

        return review;
    }


}
