package com.logisall.anyware.core.model.map.juso;

import com.logisall.anyware.core.model.map.juso.Juso;
import com.logisall.anyware.core.model.map.juso.JusoEntityCommon;
import lombok.Data;

import java.util.List;

@Data
public class JusoEntity {

    private JusoEntityCommon common;

    private List<Juso> jusos;
}
