package com.logisall.anyware.core.service.send.kakao;

public interface KakaoMsgTmplService {

    /**
     * [HL001] Mobile Auth Code Message
     *
     * @param mobile Mobile Number
     * @param code   Auth Code
     */
    void sendMsg1(String mobile, String code);

    /**
     * [HL002] Complete Card Payment
     *
     * @param mobile      Mobile Number
     * @param oid         Order Id(Number)
     * @param productName Product Name
     */
    void sendMsg2(String mobile, String oid, String productName);

    /**
     * [HL003] Complete Payment
     *
     * @param mobile      Mobile Number
     * @param oid         Order Id(Number)
     * @param productName Product Name
     */
    void sendMsg3(String mobile, String oid, String productName);

    /**
     * [HL004] Cancel Payment
     *
     * @param mobile      Mobile Number
     * @param oid         Order Id(Number)
     * @param productName Product Name
     */
    void sendMsg4(String mobile, String oid, String productName);

    /**
     * [HL005] Virtual Bank
     *
     * @param mobile      Mobile Number
     * @param oid         Order Id(Number)
     * @param productName Product Name
     * @param vbankNum Account Number
     * @param vbankName name of bank
     * @param vbankHolder Account Holder
     * @param amount amount
     * @param expirationDate Expiration date
     */
    void sendMsg5(String mobile, String oid, String productName, String vbankNum, String vbankName, String vbankHolder, String amount, String expirationDate);

    /**
     * [HL006] Complete Booking
     *
     * @param mobile      Mobile Number
     *
     * @param fullName fullname
     * @param type type
     * @param function function
     * @param option option
     * @param starttime start time
     * @param endtime end time
     * @param address address
     */
    void sendMsg6(String mobile, String fullName, String type, String function, String option, String starttime, String endtime, String address);


    /**
     * [HL007] Cancel Booking
     *
     * @param mobile      Mobile Number
     *
     * @param fullName fullname
     * @param type type
     * @param function function
     * @param option option
     * @param starttime start time
     * @param endtime end time
     * @param address address
     */
    void sendMsg7(String mobile, String fullName, String type, String function, String option, String starttime, String endtime, String address, String fee);

}
