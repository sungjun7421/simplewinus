package com.logisall.anyware.core.domain.commerce.deposit;

import java.math.BigDecimal;

public interface DepositRepositoryCustom {

    BigDecimal getAmountAvailable(String email); // 사용가능한 금액 (보유 금액)
}
