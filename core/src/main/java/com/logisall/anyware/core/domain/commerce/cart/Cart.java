package com.logisall.anyware.core.domain.commerce.cart;

import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.product.Product;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;

@Slf4j
@Entity(name = "Cart")
@Getter
@Setter
@ToString(exclude = {"relativeBuyer", "relativeProduct"})
@NoArgsConstructor
public class Cart extends AbstractEntity<Long> {

  private static final long serialVersionUID = 8775536207531366919L;

  public static String[] IGNORE_PROPERTIES = {
      "id"};

  public Cart(Buyer buyer, Product product) {
    this.relativeBuyer = buyer;
    this.relativeProduct = product;
  }

  @PrePersist
  public void prePersist() {
    if (this.buyCount == null) {
      this.buyCount = 1;
    }
  }

  @Id
  @GeneratedValue
  private Long id;

  @JsonIgnore
  @ManyToOne()
  @JoinColumn(name = "idBuyer", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Relative_Buyer_For_Cart"))
  private Buyer relativeBuyer; // 구매자


  @ManyToOne()
  @JsonIgnore
  @JoinColumn(name = "idProduct", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Relative_Product_For_Cart"))
  private Product relativeProduct; // 제품

  @Column
  private Integer buyCount;  // 당은 수량


  @Override
  public void delete() {

  }

  @Override
  public void lazy() {
    if (this.relativeBuyer != null) {
      this.relativeBuyer.getId();
      this.relativeBuyer.lazy();
    }
    if (this.relativeProduct != null) {
      this.relativeProduct.getId();
      this.relativeProduct.lazy();
    }
  }
}
