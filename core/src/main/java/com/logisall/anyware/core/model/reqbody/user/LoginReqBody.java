package com.logisall.anyware.core.model.reqbody.user;

import com.logisall.anyware.core.model.BaseRequestBody;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginReqBody extends BaseRequestBody {

    private static final long serialVersionUID = -6594245922268862985L;

    @Email
    @Schema(description = "이메일 주소", example = "test@logisall.com", minLength = 3, maxLength = 255)
    private String email;

    @Pattern(regexp = ValidUtils.PATTERN_PASSWORD_NEW)
    @Schema(description = "비밀번호", example = "abcd1234", minLength = 6, maxLength = 30)
    private String password;
}
