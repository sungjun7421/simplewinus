package com.logisall.anyware.core.service.board.event;


import com.logisall.anyware.core.domain.board.event.Event;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.admin.EventInfoResBody;
import com.logisall.anyware.core.service.DomainService;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Locale;

public interface EventService extends DomainService<Event, Long> {

  List<EventInfoResBody> list(String query);

  // R
  Page<Event> page(Locale locale, Filter filter, Boolean active);
  List<Event> list(Locale locale);
}
