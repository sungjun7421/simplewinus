package com.logisall.anyware.core.model.resbody.account;

import com.logisall.anyware.core.model.BaseResponseBody;
import com.logisall.anyware.core.model.resbody.commerce.level.BuyerLevelResBody;
import com.logisall.anyware.core.model.resbody.commerce.point.PointOfMemberResBody;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.server.core.Relation;

@Relation(value = "myPointInfo")
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MyPointInfoResBody extends BaseResponseBody {
  private static final long serialVersionUID = 5166499494765304175L;

  private BuyerLevelResBody level;
  private PointOfMemberResBody point;
}
