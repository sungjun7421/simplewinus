package com.logisall.anyware.core.model.reqbody.account;

import com.logisall.anyware.core.domain.user.TermsAgree;
import com.logisall.anyware.core.model.BaseRequestBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TermsAgreeReqBody extends BaseRequestBody {
  private static final long serialVersionUID = 6693598322030138401L;

  @Schema(description = "서비스 이용약관 동의 (필수)", example = "true")
  private boolean taService; // 서비스 이용약관 동의

  @Schema(description = "개인정보 수집이용 동의 (필수)", example = "true")
  private boolean taPrivacy; // 개인정보 수집이용 동의 (필수)

  @Schema(description = "본인은 만 14세 이상입니다. (선택)", example = "true")
  private boolean taYouth; // 본인은 만 14세 이상입니다. (필수)

  @Schema(description = "전자금융거래 이용약관 동의 (필수)", example = "true")
  private boolean taEft; // (Electronic financial transactions) 전자금융거래 이용약관 동의 (필수)

  @Schema(description = "위치정보서비스 및 위치기반서비스 이용약관 (필수)", example = "true")
  private boolean taLocation; // 위치정보서비스 및 위치기반서비스 이용약관

  // ============== SMS
  @Schema(description = "문자 발송 수신 동의 (선택)", nullable = true, example = "true")
  private boolean smsRcv; // 문자 수신동의

  // ============== EMAIL

  @Schema(description = "이메일 발송 수신 동의 (선택)", nullable = true, example = "true")
  private boolean emailRcv; // 이메일 수신 동의

  // ============== Push
  @Schema(description = "카카오톡 발송 수신 동의 (선택)", nullable = true, example = "true")
  private boolean pushRcv; // 푸시 수신 동의

  // ============== Kakao
  @Schema(description = "카카오톡 발송 수신 동의 (선택)", nullable = true, example = "true")
  private boolean kakaoRcv; // 카카오 알림톡 수신 동의

  public TermsAgree toTermsAgree() {
    return TermsAgree.builder()

        .taService(this.taService)
        .taLocation(this.taLocation)
        .taEft(this.taEft)
        .taPrivacy(this.taPrivacy)
        .taYouth(this.taYouth)

        .smsRcv(this.smsRcv)
        .emailRcv(this.emailRcv)
        .pushRcv(this.pushRcv)
        .kakaoRcv(this.kakaoRcv)
        .build();
  }
}
