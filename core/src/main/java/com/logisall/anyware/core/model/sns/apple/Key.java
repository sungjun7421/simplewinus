package com.logisall.anyware.core.model.sns.apple;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class Key implements Serializable {

    private static final long serialVersionUID = -1170404007891790036L;

    private String kty;
    private String kid;
    private String use;
    private String alg;
    private String n;
    private String e;
}
