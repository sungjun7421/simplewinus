package com.logisall.anyware.core.domain.commerce.buyerlevel;

import com.querydsl.core.types.Predicate;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

public class BuyerLevelPredicate {

  private static final QBuyerLevel Q_BUYER_LEVEL = QBuyerLevel.buyerLevel;

  public static Predicate search(String value) {

    if (StringUtils.isEmpty(value)) {
      return null;
    }

    value = value.trim();

    return Q_BUYER_LEVEL.name.textEnUs.containsIgnoreCase(value)
            .or(Q_BUYER_LEVEL.name.textJaJp.containsIgnoreCase(value))
            .or(Q_BUYER_LEVEL.name.textKoKr.containsIgnoreCase(value))
            .or(Q_BUYER_LEVEL.name.textZhCn.containsIgnoreCase(value))
            .or(Q_BUYER_LEVEL.name.textZhTw.containsIgnoreCase(value))
            .or(Q_BUYER_LEVEL.description.textEnUs.containsIgnoreCase(value))
            .or(Q_BUYER_LEVEL.description.textJaJp.containsIgnoreCase(value))
            .or(Q_BUYER_LEVEL.description.textKoKr.containsIgnoreCase(value))
            .or(Q_BUYER_LEVEL.description.textZhCn.containsIgnoreCase(value))
            .or(Q_BUYER_LEVEL.description.textZhTw.containsIgnoreCase(value));
  }

  public static Predicate startDate(final LocalDateTime startDate) {

    if (StringUtils.isEmpty(startDate)) {
      return null;
    }
    return Q_BUYER_LEVEL.createdDate.goe(startDate);
  }

  public static Predicate endDate(final LocalDateTime endDate) {

    if (StringUtils.isEmpty(endDate)) {
      return null;
    }
    return Q_BUYER_LEVEL.createdDate.loe(endDate);
  }
}
