package com.logisall.anyware.core.service.board.qna;

import com.logisall.anyware.core.domain.board.qna.Qna;
import com.logisall.anyware.core.service.DomainService;

public interface QnaService extends DomainService<Qna, Long> {

}
