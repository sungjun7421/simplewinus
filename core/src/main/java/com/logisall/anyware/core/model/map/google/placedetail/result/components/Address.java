package com.logisall.anyware.core.model.map.google.placedetail.result.components;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.List;

@Slf4j
@Setter
@Getter
@ToString
public class Address implements Serializable {

    private static final long serialVersionUID = -6220636731895092165L;

    private String long_name;
    private String short_name;
    private List<String> types;
}
