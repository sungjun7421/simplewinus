package com.logisall.anyware.core.service.send.aligo;


import com.logisall.anyware.core.domain.sms.SMSHistoryRepository;
import com.logisall.anyware.core.service.send.aligo.AligoService;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
@Transactional
public class AligoServiceImpl implements AligoService {

    @Autowired
    SMSHistoryRepository smsHistoryRepository;

    @Value("${aligo.url}")
    private String url;

    @Value("${aligo.key}")
    private String key;

    @Value("${aligo.userId}")
    private String userId;

    @Value("${aligo.sender}")
    private String sender;

    @Value("${aligo.title}")
    private String title;

    @Override
    public String sendPhoneCodeWithAligo(String phoneNumber, String msg) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();
            multiValueMap.add("key", key);
            multiValueMap.add("user_id", userId);
            multiValueMap.add("sender", sender);
            multiValueMap.add("receiver", phoneNumber.trim());
            multiValueMap.add("msg", msg.trim());
            multiValueMap.add("title", title);
            HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(multiValueMap, headers);
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
            log.info("> response ::: {}", response);

            if(response.getBody() != null && response.getStatusCode().equals(HttpStatus.OK)) {
                String body = response.getBody();
                JsonObject jsonObject = new JsonParser().parse(body).getAsJsonObject();

                if(jsonObject.get("result_code").getAsInt() > 0) {
                    return "200";
                } else {
                    return "400";
                }
            } else {
                return "400";
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("> error ::: {}", ex.toString());
            return "500";
        }
    }
}
