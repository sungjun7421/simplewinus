package com.logisall.anyware.core.model.resbody;

import com.logisall.anyware.core.model.BaseResponseBody;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.server.core.Relation;

@Relation(value = "result", collectionRelation = "results")
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder(buildMethodName = "resultBodyBuild", builderMethodName = "resultBodyBuilder")
public class ResultResBody extends BaseResponseBody {

    private static final long serialVersionUID = 2813017186081236225L;

    private boolean success;
    private String message;

    public static com.logisall.anyware.core.model.resbody.ResultResBody of(boolean result) {
        com.logisall.anyware.core.model.resbody.ResultResBody resultResBody = new com.logisall.anyware.core.model.resbody.ResultResBody();
        if (result) {
            resultResBody.setSuccess(true);
        } else {
            resultResBody.setSuccess(false);
        }
        return resultResBody;
    }

    public static com.logisall.anyware.core.model.resbody.ResultResBody of(boolean result, String message) {
        com.logisall.anyware.core.model.resbody.ResultResBody resultResBody = new com.logisall.anyware.core.model.resbody.ResultResBody();
        if (result) {
            resultResBody.setSuccess(true);
        } else {
            resultResBody.setSuccess(false);
        }
        resultResBody.setMessage(message);
        return resultResBody;
    }
}
