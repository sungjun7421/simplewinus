package com.logisall.anyware.core.model.map.google.placedetail.result.geometry;

import com.logisall.anyware.core.model.map.google.placedetail.result.geometry.Location;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Slf4j
@Getter
@Setter
@ToString
public class Geometry implements Serializable {

    private static final long serialVersionUID = -5697576620972658346L;

    private Location location;
}
