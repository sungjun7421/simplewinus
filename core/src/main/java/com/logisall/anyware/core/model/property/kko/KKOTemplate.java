package com.logisall.anyware.core.model.property.kko;

import com.logisall.anyware.core.model.property.kko.KKO;
import com.logisall.anyware.core.model.property.kko.KKOMeta;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.Serializable;

@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "kko-template")
public class KKOTemplate implements Serializable {

  private static final long serialVersionUID = 3905654535118777647L;

  @Value("${spring.application.name}")
  private String appName;

  private KKOMeta tmpl001;
  private KKOMeta tmpl002;
  private KKOMeta tmpl003;
  private KKOMeta tmpl004;
  private KKOMeta tmpl005;
  private KKOMeta tmpl006;
  private KKOMeta tmpl007;

  /**
   * [HL001] Mobile Auth Code Message
   *
   * @param mobile Mobile Number
   * @param code   Auth Code
   * @return KOO
   */
  public KKO toTemplate001(String mobile, String code) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(code)) {
      throw new IllegalArgumentException("'mobile' and 'code' is required");
    }

    String message = this.tmpl001.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{code}", code);

    return KKO.builder()

        .phone(mobile)
        .message(message)
        .templateCode(this.tmpl001.getCode())

        .failedType(KKO.FailedType.LMS)
        .failedSubject(this.tmpl001.getName())
        .failedMsg(message)

        .build();
  }

  /**
   * [HL002] Complete Card Payment
   *
   * @param mobile      Mobile Number
   * @param oid         Order Id(Number)
   * @param productName Product Name
   * @return KOO
   */
  public KKO toTemplate002(String mobile, String oid, String productName) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(oid)
        || StringUtils.isEmpty(productName)
    ) {
      throw new IllegalArgumentException("'mobile' and 'code' and 'productName' is required");
    }

    String message = this.tmpl002.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{oid}", oid);
    message = message.replace("#{productName}", productName);

    return KKO.builder()

        .phone(mobile)
        .message(message)
        .templateCode(this.tmpl002.getCode())

        .failedType(KKO.FailedType.LMS)
        .failedSubject(this.tmpl002.getName())
        .failedMsg(message)

        .build();
  }

  /**
   * [HL003] Complete Payment
   *
   * @param mobile      Mobile Number
   * @param oid         Order Id(Number)
   * @param productName Product Name
   * @return KOO
   */
  public KKO toTemplate003(String mobile, String oid, String productName) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(oid)
        || StringUtils.isEmpty(productName)
    ) {
      throw new IllegalArgumentException("'mobile' and 'code' and 'productName' is required");
    }

    String message = this.tmpl003.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{oid}", oid);
    message = message.replace("#{productName}", productName);

    return KKO.builder()

        .phone(mobile)
        .message(message)
        .templateCode(this.tmpl003.getCode())

        .failedType(KKO.FailedType.LMS)
        .failedSubject(this.tmpl003.getName())
        .failedMsg(message)

        .build();
  }

  /**
   * [HL004] Cancel Payment
   *
   * @param mobile      Mobile Number
   * @param oid         Order Id(Number)
   * @param productName Product Name
   * @return KOO
   */
  public KKO toTemplate004(String mobile, String oid, String productName) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(oid)
        || StringUtils.isEmpty(productName)
    ) {
      throw new IllegalArgumentException("'mobile' and 'code' and 'productName' is required");
    }

    String message = this.tmpl004.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{oid}", oid);
    message = message.replace("#{productName}", productName);

    return KKO.builder()

        .phone(mobile)
        .message(message)
        .templateCode(this.tmpl004.getCode())

        .failedType(KKO.FailedType.LMS)
        .failedSubject(this.tmpl004.getName())
        .failedMsg(message)

        .build();
  }

  /**
   * [HL005] Virtual Bank
   *
   * @param mobile      Mobile Number
   * @param oid         Order Id(Number)
   * @param productName Product Name
   * @param vbankNum Account Number
   * @param vbankName name of bank
   * @param vbankHolder Account Holder
   * @param amount amount
   * @param expirationDate Expiration date
   * @return KOO
   */
  public KKO toTemplate005(String mobile, String oid, String productName, String vbankNum, String vbankName, String vbankHolder, String amount, String expirationDate) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(oid)
        || StringUtils.isEmpty(productName)
        || StringUtils.isEmpty(vbankNum)
        || StringUtils.isEmpty(vbankName)
        || StringUtils.isEmpty(vbankHolder)
        || StringUtils.isEmpty(amount)
        || StringUtils.isEmpty(expirationDate)
    ) {
      throw new IllegalArgumentException("'mobile' " +
          "and 'code' " +
          "and 'productName' " +
          "and 'vbankNum' " +
          "and 'vbankName' " +
          "and 'vbankHolder' " +
          "and 'amount' " +
          "and 'expirationDate' " +
          "is required");
    }

    String message = this.tmpl005.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{oid}", oid);
    message = message.replace("#{productName}", productName);
    message = message.replace("#{vbankNum}", vbankNum);
    message = message.replace("#{vbankName}", vbankName);
    message = message.replace("#{vbankHolder}", vbankHolder);
    message = message.replace("#{amount}", amount);
    message = message.replace("#{expirationDate}", expirationDate);

    return KKO.builder()

        .phone(mobile)
        .message(message)
        .templateCode(this.tmpl005.getCode())

        .failedType(KKO.FailedType.LMS)
        .failedSubject(this.tmpl005.getName())
        .failedMsg(message)

        .build();
  }

  /**
   * [HL006] Complete Booking
   *
   * @param mobile      Mobile Number
   *
   * @param fullName fullname
   * @param type type
   * @param function function
   * @param option option
   * @param starttime start time
   * @param endtime end time
   * @param address address
   * @return KOO
   */
  public KKO toTemplate006(String mobile, String fullName, String type, String function, String option, String starttime, String endtime, String address) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(type)
        || StringUtils.isEmpty(function)
        || StringUtils.isEmpty(option)
        || StringUtils.isEmpty(starttime)
        || StringUtils.isEmpty(endtime)
        || StringUtils.isEmpty(address)
    ) {
      throw new IllegalArgumentException("'mobile' " +
          "and 'fullName' " +
          "and 'type' " +
          "and 'function' " +
          "and 'option' " +
          "and 'starttime' " +
          "and 'endtime' " +
          "and 'address' " +
          "is required");
    }

    String message = this.tmpl006.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{type}", type);
    message = message.replace("#{function}", function);
    message = message.replace("#{option}", option);
    message = message.replace("#{starttime}", starttime);
    message = message.replace("#{endtime}", endtime);
    message = message.replace("#{address}", address);

    return KKO.builder()

        .phone(mobile)
        .message(message)
        .templateCode(this.tmpl006.getCode())

        .failedType(KKO.FailedType.LMS)
        .failedSubject(this.tmpl006.getName())
        .failedMsg(message)

        .build();
  }

  /**
   * [HL007] Cancel Booking
   *
   * @param mobile      Mobile Number
   *
   * @param fullName fullname
   * @param type type
   * @param function function
   * @param option option
   * @param starttime start time
   * @param endtime end time
   * @param address address
   * @return KOO
   */
  public KKO toTemplate007(String mobile, String fullName, String type, String function, String option, String starttime, String endtime, String address, String fee) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(type)
        || StringUtils.isEmpty(function)
        || StringUtils.isEmpty(option)
        || StringUtils.isEmpty(starttime)
        || StringUtils.isEmpty(endtime)
        || StringUtils.isEmpty(address)
        || StringUtils.isEmpty(fee)
    ) {
      throw new IllegalArgumentException("'mobile' " +
          "and 'oid' " +
          "and 'fullName' " +
          "and 'type' " +
          "and 'function' " +
          "and 'option' " +
          "and 'starttime' " +
          "and 'endtime' " +
          "and 'address' " +
          "and 'fee' " +
          "is required");
    }

    String message = this.tmpl007.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{type}", type);
    message = message.replace("#{function}", function);
    message = message.replace("#{option}", option);
    message = message.replace("#{starttime}", starttime);
    message = message.replace("#{endtime}", endtime);
    message = message.replace("#{address}", address);
    message = message.replace("#{fee}", fee);

    return KKO.builder()

        .phone(mobile)
        .message(message)
        .templateCode(this.tmpl007.getCode())

        .failedType(KKO.FailedType.LMS)
        .failedSubject(this.tmpl007.getName())
        .failedMsg(message)

        .build();
  }
}
