package com.logisall.anyware.core.domain.commerce.review;

import java.math.BigDecimal;

public interface ReviewRepositoryCustom {
    BigDecimal cacculateAvgStar();
}
