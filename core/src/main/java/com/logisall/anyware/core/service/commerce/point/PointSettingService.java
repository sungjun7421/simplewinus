package com.logisall.anyware.core.service.commerce.point;

import com.logisall.anyware.core.domain.commerce.point.PointSetting;

public interface PointSettingService {

  PointSetting setting(PointSetting setting);

  PointSetting get();
}
