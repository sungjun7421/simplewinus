package com.logisall.anyware.core.model.resbody.commerce.order;

import com.logisall.anyware.core.utils.DateUtils;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.server.core.Relation;

import java.time.LocalDateTime;

@Relation(value = "orderTime", collectionRelation = "orderTimes")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderTimeResBody {
    private static final long serialVersionUID = -9213647619513434494L;

    @DateTimeFormat(pattern = DateUtils.FORMAT_DATE_TIME_UNIT)
    @JsonSerialize
    @JsonDeserialize
    private LocalDateTime startTime;

    @DateTimeFormat(pattern = DateUtils.FORMAT_DATE_TIME_UNIT)
    @JsonSerialize
    @JsonDeserialize
    private LocalDateTime endTime;
}
