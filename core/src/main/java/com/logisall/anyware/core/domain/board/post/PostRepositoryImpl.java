package com.logisall.anyware.core.domain.board.post;

import com.logisall.anyware.core.domain.board.post.PostRepositoryCustom;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class PostRepositoryImpl implements PostRepositoryCustom {

  @Autowired
  private JPAQueryFactory jpaQueryFactory;
}
