package com.logisall.anyware.core.domain.commerce.review;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@NoArgsConstructor(staticName = "getInstance")
public class ReviewPredicate {

  private static final QReview Q_REVIEW = QReview.review;

  private BooleanBuilder builder = new BooleanBuilder();
  public ReviewPredicate buyer (final Long id){
    if(id !=null && id != 0L){
      builder.and(Q_REVIEW.relativeWriter.id.eq(id));

    }
    return this;

  }

  public Predicate values() {
    return builder.getValue() == null ? builder.and(Q_REVIEW.id.isNotNull()) : builder.getValue();
  }

  public ReviewPredicate search(String value) {

    if (!StringUtils.isEmpty(value)) {
      value = value.trim();

//      builder.and(Q_REVIEW.message.containsIgnoreCase(value)
//          .or(Q_REVIEW.relativeBuyer.relativeUser.email.eq(value))
//          .or(Q_REVIEW.relativeBuyer.relativeUser.mobile.eq(value))
//          .or(Q_REVIEW.relativeBuyer.relativeUser.fullName.eq(value))
//      );
    }
    return this;
  }


  public ReviewPredicate startDate(final LocalDateTime startDate) {

    if (startDate != null) {
      builder.and(Q_REVIEW.createdDate.goe(startDate));
    }
    return this;
  }

  public ReviewPredicate endDate(final LocalDateTime endDate) {

    if (endDate != null) {
      builder.and(Q_REVIEW.createdDate.loe(endDate));
    }
    return this;
  }

//  public ReviewPredicate
//
//
//
//  checkImages(Boolean isImages) {
//
//    if (isImages != null && isImages == true) {
//      builder.and(Q_REVIEW.images.isNotEmpty());
//    }
//    return this;
//  }

//  public CouponPredicate status(final Point.Status status) {
//    if (status != null) {
//      builder.and(Q_REVIEW.status.eq(status));
//    }
//    return this;
//  }
//
//  public CouponPredicate buyer(final Long idBuyer) {
//    if (idBuyer != null) {
//      builder.and(Q_REVIEW.relativeBuyer.id.eq(idBuyer));
//    }
//    return this;
//  }
//
//  public CouponPredicate buyer(final String email) {
//    if (!StringUtils.isEmpty(email)) {
//      builder.and(Q_REVIEW.relativeBuyer.relativeUser.email.eq(email));
//    }
//    return this;
//  }
}
