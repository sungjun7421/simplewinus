package com.logisall.anyware.core.domain.setting;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.PrePersist;
import java.io.Serializable;

@Setter
@Getter
@ToString
@Embeddable
public class ComponentMode implements Serializable {

  public ComponentMode() {
    this.event = true;
    this.faq = true;
    this.qna = true;
    this.contact = true;
  }

  public void toFalseMode() {
    this.event = false;
    this.faq = false;
    this.qna = false;
    this.contact = false;
  }

  @PrePersist
  public void prePersist() {
  }

  @Column(columnDefinition = "BIT(1) default 1")
  private boolean event;

  @Column(columnDefinition = "BIT(1) default 1")
  private boolean faq;

  @Column(columnDefinition = "BIT(1) default 1")
  private boolean qna;

  @Column(columnDefinition = "BIT(1) default 1")
  private boolean contact;

}
