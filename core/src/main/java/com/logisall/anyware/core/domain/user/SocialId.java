package com.logisall.anyware.core.domain.user;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.PrePersist;
import java.io.Serializable;

@Schema(description = "SNS ID")
@Slf4j
@Getter
@Setter
@ToString
@AllArgsConstructor
@Builder
@Embeddable
public class SocialId implements Serializable {

  private static final long serialVersionUID = -3142066120722816609L;

  public SocialId() {
    this.facebookId = "";
    this.facebookName = "";

    this.googleId = "";
    this.googleName = "";

    this.kakaoId = "";
    this.kakaoName = "";

    this.naverId = "";
    this.naverName = "";

    this.appleId = "";
    this.appleName = "";
  }

  @PrePersist
  public void prePersist() {
    if (StringUtils.isEmpty(facebookId)) {
      this.facebookId = "";
    }
    if (StringUtils.isEmpty(facebookName)) {
      this.facebookName = "";
    }

    if (StringUtils.isEmpty(googleId)) {
      this.googleId = "";
    }
    if (StringUtils.isEmpty(googleName)) {
      this.googleName = "";
    }

    if (StringUtils.isEmpty(kakaoId)) {
      this.kakaoId = "";
    }
    if (StringUtils.isEmpty(kakaoName)) {
      this.kakaoName = "";
    }

    if (StringUtils.isEmpty(naverId)) {
      this.naverId = "";
    }
    if (StringUtils.isEmpty(naverName)) {
      this.naverName = "";
    }

    if (StringUtils.isEmpty(appleId)) {
      this.appleId = "";
    }
    if (StringUtils.isEmpty(appleName)) {
      this.appleName = "";
    }

  }

  @Schema(description = "페이스북 ID", nullable = true)
  @Column(length = 50, columnDefinition = "VARCHAR(50) default ''")
  private String facebookId;

  @Schema(description = "페이스북 Username", nullable = true)
  @Column(length = 50, columnDefinition = "VARCHAR(50) default ''")
  private String facebookName;


  @Schema(description = "구글 ID", nullable = true)
  @Column(length = 50, columnDefinition = "VARCHAR(50) default ''")
  private String googleId;

  @Schema(description = "구글 Username", nullable = true)
  @Column(length = 50, columnDefinition = "VARCHAR(50) default ''")
  private String googleName;


  @Schema(description = "카카오톡 ID", nullable = true)
  @Column(length = 50, columnDefinition = "VARCHAR(50) default ''")
  private String kakaoId;

  @Schema(description = "카카오톡 Username", nullable = true)
  @Column(length = 50, columnDefinition = "VARCHAR(50) default ''")
  private String kakaoName;


  @Schema(description = "애플 ID", nullable = true)
  @Column(length = 50, columnDefinition = "VARCHAR(50) default ''")
  private String appleId;

  @Schema(description = "애플 Username", nullable = true)
  @Column(length = 50, columnDefinition = "VARCHAR(50) default ''")
  private String appleName;


  @Schema(description = "Naver ID", nullable = true)
  @Column(length = 50, columnDefinition = "VARCHAR(50) default ''")
  private String naverId;

  @Schema(description = "Naver Username", nullable = true)
  @Column(length = 50, columnDefinition = "VARCHAR(50) default ''")
  private String naverName;

  @Schema(title = "Empty", description = "회원가입 요청시 사용안함", nullable = true)
  public boolean isEmpty() {
    return StringUtils.isEmpty(facebookId)
        && StringUtils.isEmpty(googleId)
        && StringUtils.isEmpty(kakaoId)
        && StringUtils.isEmpty(naverId)
        && StringUtils.isEmpty(appleId)
        ;
  }

  @Schema(description = "Facebook ID 유/무 (회원가입 요청시 사용안함)", nullable = true)
  public boolean isFacebook() {
    return !StringUtils.isEmpty(this.facebookId);
  }

  @Schema(description = "Google ID 유/무 (회원가입 요청시 사용안함)", nullable = true)
  public boolean isGoogle() {
    return !StringUtils.isEmpty(this.googleId);
  }

  @Schema(description = "Kakao ID 유/무 (회원가입 요청시 사용안함)", nullable = true)
  public boolean isKakao() {
    return !StringUtils.isEmpty(this.kakaoId);
  }

  @Schema(description = "Naver ID 유/무 (회원가입 요청시 사용안함)", nullable = true)
  public boolean isNaver() {
    return !StringUtils.isEmpty(this.naverId);
  }

  @Schema(description = "Apple ID 유/무 (회원가입 요청시 사용안함)", nullable = true)
  public boolean isApple() {
    return !StringUtils.isEmpty(this.appleId);
  }


}
