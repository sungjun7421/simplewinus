package com.logisall.anyware.core.domain.commerce.deposit;

import com.logisall.anyware.core.domain.AbstractEntity;
import com.logisall.anyware.core.domain.commerce.point.Point;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@ToString
public class Deposit extends AbstractEntity<Long> {
  private static final long serialVersionUID = -6615836582150346995L;
  public static final String[] IGNORE_PROPERTIES = {"id"};

  @PrePersist
  public void prePersist() {
  }

  @PreUpdate
  public void PreUpdate() {
  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @Column
  @Enumerated
  private Point.Status status; // 입금/출금

  @Column(columnDefinition = "DECIMAL(19,0) default 0")
  private BigDecimal amount; // 금액

  @Override
  public void delete() {

  }

  @Override
  public void lazy() {

  }
}
