package com.logisall.anyware.core.domain.commerce.coupon;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logisall.anyware.core.domain.AbstractEntityInternational;
import com.logisall.anyware.core.domain.International.InterText;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel;
import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistory;
import com.logisall.anyware.core.model.reqbody.commerce.coupon.CouponThumbBody;
import com.logisall.anyware.core.model.resbody.commerce.coupon.CouponResBody;
import com.logisall.anyware.core.utils.DateUtils;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * 쿠폰정책
 * https://docs.google.com/document/d/1veFDw-itBXbb4owKh_wCG1RzWh5OUcwB4ji2op8FG_M/edit
 */
@Entity
@Getter
@Setter
@ToString(exclude = {
        "buyerLevels",
        "couponHistories"
})
public class Coupon extends AbstractEntityInternational<Long> {

  private static final long serialVersionUID = -2023214255837638724L;

  public static final String[] IGNORE_PROPERTIES = {
          "id",
          "code",
          "couponHistories"
  };

  @PrePersist
  public void prePersist() {
    if (StringUtils.isNotEmpty(this.code)) {
      this.code = this.code.trim().replace(" ", "");
    }

  }

  @PreUpdate
  public void PreUpdate() {

  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @Embedded
  @AttributeOverrides({
      @AttributeOverride(name = "textKoKr", column = @Column(name = "nameKoKr", length = 30)),
      @AttributeOverride(name = "textEnUs", column = @Column(name = "nameEnUs", length = 30)),
      @AttributeOverride(name = "textJaJp", column = @Column(name = "nameJaJp", length = 30)),
      @AttributeOverride(name = "textZhCn", column = @Column(name = "nameZhCn", length = 30)),
      @AttributeOverride(name = "textZhTw", column = @Column(name = "nameZhTw", length = 30))
  })
  private com.logisall.anyware.core.domain.International.InterText name; // 쿠폰명

  @Embedded
  @AttributeOverrides({
          @AttributeOverride(name = "textKoKr", column = @Column(name = "requiteKoKr", columnDefinition = "MEDIUMTEXT")),
          @AttributeOverride(name = "textEnUs", column = @Column(name = "requiteEnUs", columnDefinition = "MEDIUMTEXT")),
          @AttributeOverride(name = "textJaJp", column = @Column(name = "requiteJaJp", columnDefinition = "MEDIUMTEXT")),
          @AttributeOverride(name = "textZhCn", column = @Column(name = "requiteZhCn", columnDefinition = "MEDIUMTEXT")),
          @AttributeOverride(name = "textZhTw", column = @Column(name = "requiteZhTw", columnDefinition = "MEDIUMTEXT")),
  })
  private InterText requite;

  @Enumerated(EnumType.STRING)
  @Column(length = 20)
  private CouponType type;

  @Column(length = 30, unique = true, nullable = false, updatable = false)
  private String code; // 쿠폰코드

  @Column(columnDefinition = "DECIMAL(19,2) default 0")
  private BigDecimal discountFixed;

  @Column(columnDefinition = "DECIMAL(19,2) default 0")
  private BigDecimal discountPercent;

  @Column(columnDefinition = "DECIMAL(19,2) default 0")
  private BigDecimal minimumPayment; //requite minimum payment amount

  /**
   * 사용기간
   **/
  @DateTimeFormat(pattern = com.logisall.anyware.core.utils.DateUtils.FORMAT_DATE_UNIT_BAR)
  private LocalDate startDate; // 쿠폰을 사용할 수 있는 시작 날짜

  @DateTimeFormat(pattern = DateUtils.FORMAT_DATE_UNIT_BAR)
  private LocalDate endDate; // 쿠폰을 사용할 수 있는 마감 날짜

  @Column(columnDefinition = "BIT(1) default 1")
  private boolean active; // 활성/비활성

  @OrderBy("createdDate DESC")
  @ManyToMany
  @JoinTable(name = "CouponToBuyerLevel",
          joinColumns = @JoinColumn(name = "idCoupon", nullable = false),
          inverseJoinColumns = @JoinColumn(name = "idBuyerLevel", nullable = false)
  )
  private List<BuyerLevel> buyerLevels = new ArrayList<>();

  //= 쿠폰 내역
  @JsonIgnore
  @OneToMany(mappedBy = "relativeCoupon", cascade = CascadeType.ALL)
  @OrderBy("createdDate desc")
  private List<CouponHistory> couponHistories = new ArrayList<>();

  @Override
  public void delete() {

  }

  @Override
  public void lazy() {
    if (this.buyerLevels != null) {
      this.buyerLevels.size();
    }
  }

  @Override
  public void setLocale(Locale locale) {
    super.setLocale(locale);
    if (this.name != null) {
      this.name.setLocale(locale);
    }
    if (this.requite != null) {
      this.requite.setLocale(locale);
    }
  }

  // Rest
  public com.logisall.anyware.core.model.reqbody.commerce.coupon.CouponThumbBody toCouponThumbBody(Locale locale) {
    this.setLocale(locale);
    return CouponThumbBody.builder()
            .id(this.id)
            .name(this.name.getValue())
            .type(this.type)
            .discountFixed(this.discountFixed)
            .startDate(this.startDate)
            .endDate(this.endDate)
            .build();
  }

  public com.logisall.anyware.core.model.resbody.commerce.coupon.CouponResBody toBody(Locale locale) {
    this.setLocale(locale);
    return CouponResBody.builder()
            .id(this.id)
            .name(this.name.getValue())
            .requite(this.requite != null ? this.requite.getValue() : "")
            .type(this.type)
            .code(this.code)
            .discountFixed(this.discountFixed)
            //.discountPercent(this.discountPercent)
            .startDate(this.startDate)
            .endDate(this.endDate)
            .build();
  }
}
