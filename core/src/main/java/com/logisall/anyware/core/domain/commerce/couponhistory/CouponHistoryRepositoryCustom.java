package com.logisall.anyware.core.domain.commerce.couponhistory;

import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistory;

import java.util.List;

public interface CouponHistoryRepositoryCustom {
    List<CouponHistory> getAllByUser(Long idBuyer, Boolean isUsed);
}
