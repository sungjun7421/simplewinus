package com.logisall.anyware.core.domain.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;

@Slf4j
@Entity
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
public class Authority implements java.io.Serializable, Comparable<Authority> {

  private static final long serialVersionUID = -1863041203652704913L;

  @Getter
  public enum Role {
    SUPER("ROLE_SUPER", "최고 관리자", 10), // 슈퍼, 서버 관리자
    ADMIN("ROLE_ADMIN", "관리자", 9), // 관리자

    MANAGER("ROLE_MANAGER", "콘텐츠 관리자", 3), // 책임급 이상권한 (책임/수석/이사/경영진)
    USER("ROLE_USER", "회원", 1), // 사용회원 (인턴/사원/선임)
    ;

    private final String role;
    private final String text;
    private final int level; // desc 가장 높은 권한 보여주기

    Role(final String role, final String text, final int level) {
      this.role = role;
      this.text = text;
      this.level = level;
    }

    @Override
    public String toString() {
      return this.getText();
    }

    public static Role toRole(String role) {
      if (role != null) {
        if (role.equals(SUPER.role)) {
          return SUPER;
        } else if (role.equals(ADMIN.role)) {
          return ADMIN;
        } else if (role.equals(USER.role)) {
          return USER;
        }
      }
      return null;
    }
  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @NonNull
  @Column(length = 30, unique = true)
  @Enumerated(EnumType.STRING)
  private Role role;

  public int getLevel() {
    return role.getLevel();
  }

  @Override
  public String toString() {
    return this.role.getText();
  }

  @JsonIgnore
  @Transient
  @Getter
  @Setter
  private boolean checked;

  @Override
  public int compareTo(Authority authority) {
    return Integer.compare(authority.role.level, this.role.level);
  }
}
