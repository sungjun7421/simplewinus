package com.logisall.anyware.core.service.commerce.couponhistory;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistory;
import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class CouponHistoryServicreImpl implements CouponHistoryServicre {


    @Autowired
    private CouponHistoryRepository couponHistoryRepository;

    @Override
    @Transactional
    public com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistory update(com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistory couponHistory)  {

        if (couponHistory.getId() == null) {
            throw new BadRequestException();
        }


        return couponHistoryRepository.findById(couponHistory.getId())
                .map(ori -> {
                    BeanUtils.copyProperties(couponHistory, ori, CouponHistory.IGNORE_PROPERTIES);
                    return couponHistoryRepository.save(ori);
                }).orElseThrow(RuntimeException::new);
    }
}
