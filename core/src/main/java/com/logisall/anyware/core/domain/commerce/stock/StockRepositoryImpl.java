package com.logisall.anyware.core.domain.commerce.stock;

import com.logisall.anyware.core.domain.commerce.stock.StockRepositoryCustom;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

@Slf4j
public class StockRepositoryImpl implements StockRepositoryCustom {

  @Autowired
  private JPAQueryFactory jpaQueryFactory;

  @Override
  public BigDecimal getQtyAvailable(Long idProduct) {
    return null;
  }
}
