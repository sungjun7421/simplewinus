package com.logisall.anyware.core.model.reqbody.user;

import com.logisall.anyware.core.model.BaseRequestBody;
import lombok.*;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginQRReqBody extends BaseRequestBody {

    private static final long serialVersionUID = -1735375641108351363L;

    private String qrCode;
}
