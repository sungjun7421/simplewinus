package com.logisall.anyware.core.domain.device;

import com.logisall.anyware.core.utils.StringUtils;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor(staticName = "getInstance")
public class DevicePredicate {

  private static final QDevice Q_DEVICE = QDevice.device;

  private BooleanBuilder builder = new BooleanBuilder();

  public com.logisall.anyware.core.domain.device.DevicePredicate search(String value) {

    if (!StringUtils.isEmpty(value)) {
      value = value.trim();

      builder.and(Q_DEVICE.deviceToken.containsIgnoreCase(value));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.device.DevicePredicate startDate(final LocalDateTime startDate) {

    if (startDate != null) {
      builder.and(Q_DEVICE.createdDate.goe(startDate));
    }
    return this;
  }

  public com.logisall.anyware.core.domain.device.DevicePredicate endDate(final LocalDateTime endDate) {

    if (endDate != null) {
      builder.and(Q_DEVICE.createdDate.loe(endDate));
    }
    return this;
  }

  public Predicate values() {

    return builder.getValue() == null ? builder.and(Q_DEVICE.id.isNotNull()) : builder.getValue();
  }
}
