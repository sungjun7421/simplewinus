package com.logisall.anyware.core.domain.commerce.review;

import com.logisall.anyware.core.domain.commerce.review.Review;
import com.logisall.anyware.core.domain.commerce.review.ReviewRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface ReviewRepository extends
    JpaRepository<Review, Long>,
    QuerydslPredicateExecutor<Review>,
    ReviewRepositoryCustom {


}
