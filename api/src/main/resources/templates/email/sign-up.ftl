<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>${subject}</title>
</head>
<body>

<div style="width:640px;">
    <ul style="padding-left:0;list-style: none;">
        <li style="padding:47px 20px 12px;background-color:${metaEmail.primaryColor!}">
          <a href="${clientHost}" style="display: block; text-underline: none; outline: none;">
            <img src="${imageHost}/logo-wh.png" style="width:142px;">
          </a>
        </li>
        <li style="padding:20px;">
            <h1 style="font-size: 22px;font-weight: 500;color: #474747;margin:20px 0;">Welcome</h1>
            <h2 style="font-size: 22px;font-weight: 500;color: #474747;line-height: 1.32;margin:20px 0;">${appName} 회원이 되신 것을 환영합니다.</h2>
            <p style="font-size: 14px;line-height: 1.57;color: #555555;">${fullName} 고객님, ${appName} 회원가입을 축하드립니다.</p>

            <h3 style="font-size: 15px;font-weight: 700;color: #474747;margin:45px 0 10px;">회원가입정보</h3>
            <table style="width:100%;border-top:2px solid #000;margin-bottom: 30px;">
                <tbody>
                <tr>
                    <th style="font-size:13px;color:#555;width: 150px;vertical-align:top;padding:6px 20px 7px;background-color: #f7f7f7;border-bottom:1px solid #e4e4e4;">이름</th>
                    <td style="font-size:14px;color:#555;padding:6px 30px;border-bottom:1px solid #e4e4e4;">${fullName}</td>
                </tr>
                <tr>
                    <th style="font-size:13px;color:#555;width: 150px;vertical-align:top;padding:6px 20px 7px;background-color: #f7f7f7;border-bottom:1px solid #e4e4e4;">이메일</th>
                    <td style="font-size:14px;color:#555;padding:6px 30px;border-bottom:1px solid #e4e4e4;">${email}</td>
                </tr>
                <tr>
                    <th style="font-size:13px;color:#555;width: 150px;vertical-align:top;padding:6px 20px 7px;background-color: #f7f7f7;border-bottom:1px solid #e4e4e4;">가입일</th>
                    <td style="font-size:14px;color:#555;padding:6px 30px;border-bottom:1px solid #e4e4e4;">${createdDate.format('yyyy.MM.dd HH:mm:ss')}</td>
                </tr>
                </tbody>
            </table>

        </li>
        <li style="font-size:12px;color:#878787;padding:30px 20px;background-color:#efefef">
            <#include "footer-out.ftl"/>
        </li>
    </ul>
</div>

</body>
</html>