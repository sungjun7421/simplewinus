<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>${subject}</title>
</head>
<body>

<div style="width:640px;">
    <div style="padding:40px 20px 80px">
        <div style="text-align: center;">
            <img src="${imageHost}/galaxia-con/assets/images/birthday.jpg" style="width:457px;margin-left:24px">
            <img src="${imageHost}/galaxia-con/assets/images/galcon-logo-wh.png" style="width:142px;">
        </div>
        <div style="padding:40px 20px 80px">
            <div style="text-align: center;">
                <img src="${imageHost}/galaxia-con/assets/images/birthday.jpg" style="width:457px;margin-left:24px">
            </div>
            <p style="font-size: 15px;font-weight: 600;color: #474747;line-height: 2;text-align:center;margin-top:40px;">
                <b style="font-weight:900;color: #000;">${fullName}님</b>의 생일을 진심으로 축하합니다.<br>
                ${fullName}님의 행복한 생일, 맛있는 하루 보내시라고<br>
                스타벅스 아메리카노 쿠폰을 드렸어요.<br>
                갤콘과 함께 설레는 생일 계획을 세워보세요!
            </p>
            <div style="margin-top: 60px;text-align:center">
                <a href="${clientHost}" target="_blank"
                   style="display:inline-block;background-color:#e52488;text-decoration:none;padding:14px 20px;border-radius:5px;">
                    <span style="font-size:15px;font-weight: 600;color: #fff;">갤럭시아콘 바로가기</span>
                </a>
            </div>
        </div>
        <div style="font-size:12px;color:#878787;padding:30px 20px;background-color:#efefef">
            <#include "footer-out.ftl"/>
        </div>

    </div>

</body>
</html>