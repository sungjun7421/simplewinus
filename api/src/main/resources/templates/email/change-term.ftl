<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="ko">
<head>
  <meta charset="UTF-8">
  <title>${subject}</title>
</head>
<body>

<div style="width:640px;">
  <ul style="padding-left:0;list-style: none;">
    <li style="padding:47px 20px 12px;background-color:${metaEmail.primaryColor!}"><img src="${metaEmail.logoUri!}" style="width:142px;"></li>
    <li style="padding:20px;">

      <h1 style="font-size: 22px;font-weight: 500;color: #474747;margin:20px 0;">${subject}</h1>
      <p>${metaEmail.siteName}을 이용해주시는 여러분께 감사드리며 이용약관이 일부 개정되어 안내 드립니다.</p>

        <table style="width:100%;margin-bottom:30px;border-top:2px solid #000">
          <tbody>

          <tr>
            <th style="vertical-align:middle; font-size:13px;color:#555;width: 150px;padding:6px 20px 7px;background-color: #f7f7f7;border-bottom:1px solid #e4e4e4;">
              변경 사항
            </th>
            <td style="font-size:14px;color:#555;padding:6px 0px;border-bottom:1px solid #e4e4e4;">${content!}</td>
          </tr>

          </tbody>
        </table>
          <#-- 내용 -->

      <p>${metaEmail.siteName}은 더 나은 서비스 제공을 위해 항상 최선을 다하겠습니다.<br/>
        앞으로도 지속적인 서비스 이용과 관심 부탁드립니다.<br/>
        감사합니다.</p>
    </li>
    <li style="font-size:12px;color:#878787;padding:30px 20px;background-color:#efefef">
        <#include "footer.ftl"/>
    </li>
  </ul>
</div>

</body>
</html>