<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>${subject}</title>
</head>
<body>

<div style="width:640px;">
    <ul style="padding-left:0;list-style: none;">
        <li style="padding:47px 20px 12px;background-color:${metaEmail.primaryColor!}"><img src="${metaEmail.logoUri!}" style="width:142px;"></li>
        <li style="padding:20px;">
            <h1 style="font-size: 22px;font-weight: 500;color: #474747;margin:20px 0;">${subject}</h1>
            <p style="font-size: 15px;line-height: 1.57;color: #555555;">${fullName}고객님, 아래를 클릭하여 이메일을 인증해 주세요.</p>

            <div style="text-align:center;padding:20px 0;margin:30px 0 15px;background-color: #f7f7f7;">
                <a href="${url}" style="display:inline-block;font-size: 14px;color:#fff;padding: 10px 60px;background-color:${metaEmail.primaryColor!};border-radius:2px;">인증하기</a>
            </div>
        </li>
        <li style="font-size:12px;color:#878787;padding:30px 20px;background-color:#efefef">
           <#include "footer-out.ftl"/>
        </li>
    </ul>
</div>

</body>
</html>