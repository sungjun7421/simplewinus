<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="ko">
<head>
  <meta charset="UTF-8">
  <title>${subject}</title>
</head>
<body>

<div style="width:640px;">
  <ul style="padding-left:0;list-style: none;">
    <li style="padding:47px 20px 12px;background-color:${metaEmail.primaryColor!}"><img src="${metaEmail.logoUri!}"
                                                                                        style="width:142px;"></li>
    <li style="padding:20px;">

      <h1 style="font-size: 22px;font-weight: 500;color: #474747;margin:20px 0;">${subject}</h1>
      <p>${desciption}</p>

    </li>
    <li style="font-size:12px;color:#878787;padding:30px 20px;background-color:#efefef">
        <#include "footer.ftl"/>
    </li>
  </ul>
</div>

</body>
</html>