<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>이메일 템플릿</title>
</head>
<body>
  <ul>
    <li><a href="/galaxia-con/email-tmpl/sign-up">회원가입 완료</a></li>
    <li><a href="/galaxia-con/email-tmpl/estimate-0">견적 접수</a></li>
    <li><a href="/galaxia-con/email-tmpl/estimate-1">견적 확정</a></li>
    <li><a href="/galaxia-con/email-tmpl/estimate-2">견적 취소</a></li>
    <li><a href="/galaxia-con/email-tmpl/estimate-3">견적 만료</a></li>
    <li><a href="/galaxia-con/email-tmpl/birthday">생일축하</a></li>
<#--    <li><a href="/email-tmpl/cert-find-password-email">비밀번호 이메일 찾기 인증</a></li>-->
<#--    <li><a href="/email-tmpl/contact-notify">문의 알림</a></li>-->
<#--    <li><a href="/email-tmpl/order-complete">결제 완료 (작업중)</a></li>-->
<#--    <li><a href="/email-tmpl/order-vbank">무통장 입금</a></li>-->
<#--    <li><a href="/email-tmpl/qna-answer">Q&A 답변</a></li>-->
<#--    <li><a href="/email-tmpl/change-term">이용약관 변경</a></li>-->
  </ul>
</body>
</html>