<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>${subject}</title>
</head>
<body>


<div style="width:640px;">
    <ul style="padding-left:0;list-style: none;">
        <li style="padding:47px 20px 12px;background-color:${metaEmail.primaryColor!}"><img src="${metaEmail.logoUri!}" style="width:142px;"></li>
        <li style="padding:20px;">
            <h1 style="font-size: 22px;font-weight: 500;color: #474747;margin:20px 0;">${subject}</h1>

            <table style="width:100%;border-top:2px solid #000">
                <tbody>
                <tr>
                    <th style="font-size:13px;color:#555;width: 150px;vertical-align:top;padding:6px 20px 7px;background-color: #f7f7f7;border-bottom:1px solid #e4e4e4;">입금할 금액</th>
                    <td style="font-size:14px;color:#555;padding:6px 30px;border-bottom:1px solid #e4e4e4;"><b>${amount}원</b></td>
                </tr>
                <tr>
                    <th style="font-size:13px;color:#555;width: 150px;vertical-align:top;padding:6px 20px 7px;background-color: #f7f7f7;border-bottom:1px solid #e4e4e4;">주문상품</th>
                    <td style="font-size:14px;color:#555;padding:6px 30px;border-bottom:1px solid #e4e4e4;"><b>${title}</b></td>
                </tr>
                <tr>
                    <th style="font-size:13px;color:#555;width: 150px;vertical-align:top;padding:6px 20px 7px;background-color: #f7f7f7;border-bottom:1px solid #e4e4e4;">주문번호</th>
                    <td style="font-size:14px;color:#555;padding:6px 30px;border-bottom:1px solid #e4e4e4;">${oid}</td>
                </tr>
                <tr>
                    <th style="font-size:13px;color:#555;width: 150px;vertical-align:top;padding:6px 20px 7px;background-color: #f7f7f7;border-bottom:1px solid #e4e4e4;">주문자명</th>
                    <td style="font-size:14px;color:#555;padding:6px 30px;border-bottom:1px solid #e4e4e4;">${fullName}</td>
                </tr>
                <tr>
                    <th style="font-size:13px;color:#555;width: 150px;vertical-align:top;padding:6px 20px 7px;background-color: #f7f7f7;border-bottom:1px solid #e4e4e4;">입금은행</th>
                    <td style="font-size:14px;color:#555;padding:6px 30px;border-bottom:1px solid #e4e4e4;">${bank}</td>
                </tr>
                <tr>
                    <th style="font-size:13px;color:#555;width: 150px;vertical-align:top;padding:6px 20px 7px;background-color: #f7f7f7;border-bottom:1px solid #e4e4e4;">입금하실 계좌번호</th>
                    <td style="font-size:14px;color:#555;padding:6px 30px;border-bottom:1px solid #e4e4e4;">${number}</td>
                </tr>
                <tr>
                    <th style="font-size:13px;color:#555;width: 150px;vertical-align:top;padding:6px 20px 7px;background-color: #f7f7f7;border-bottom:1px solid #e4e4e4;">예금주</th>
                    <td style="font-size:14px;color:#555;padding:6px 30px;border-bottom:1px solid #e4e4e4;">${holder}</td>
                </tr>
                <tr>
                    <th style="font-size:13px;color:#555;width: 150px;vertical-align:top;padding:6px 20px 7px;background-color: #f7f7f7;border-bottom:1px solid #e4e4e4;">입금기한</th>
                    <td style="font-size:14px;color:#555;padding:6px 30px;border-bottom:1px solid #e4e4e4;">${expireDateTime.format('yyyy/MM/dd HH:mm')}까지</td>
                </tr>
                </tbody>
            </table>
        </li>
        <li style="font-size:12px;color:#878787;padding:30px 20px;background-color:#efefef">
            <#include "footer.ftl"/>
        </li>
    </ul>
</div>


</body>
</html>