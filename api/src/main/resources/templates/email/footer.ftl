<#import "/spring.ftl" as spring/>
<p>이 이메일은 ${nowDate.format('yyyy년 MM월 dd일')} 현재 고객님의 메일 수신옵션을 확인한 후 보내드리는 발신전용 메일입니다.<br>
  특정 메일만을 받아보시거나, 더 이상 받지 않으시려면 <a href="${metaEmail.unsubscribeUri}${email!}" style="color:#474747">[수신거부]</a>
  버튼을 눌러주세요.<br>
    ${metaEmail.siteName!}은(는) 방송통신위원회 권고사항에 따라 메일을 제작, 발송하고 있습니다.<br>
  If you don’t want receive this mail anymore, <a href="${metaEmail.unsubscribeUri}${email!}"
                                                  style="color:#474747">Click Here</a>
</p>
<p style="margin:0;">Copyright © 2020 ${metaEmail.siteName!} All rights reserved.</p>