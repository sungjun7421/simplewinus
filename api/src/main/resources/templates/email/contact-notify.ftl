<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="ko">
<head>
  <meta charset="UTF-8">
  <title>${subject!}</title>
</head>
<body>

<div style="width:640px;">
  <ul style="padding-left:0;list-style: none;">
    <li style="padding:47px 20px 12px;background-color:${metaEmail.primaryColor!}"><img src="${metaEmail.logoUri!}" style="width:142px;"></li>
    <li style="padding:20px;">
      <h1 style="font-size: 22px;font-weight: 500;color: #474747;margin:20px 0;">${subject!}</h1>

      <h3 style="font-size: 15px;font-weight: 700;color: #474747;margin:45px 0 10px;">문의 내용 안내</h3>
      <table style="width:100%;margin-bottom:30px;border-top:2px solid #000">
        <tbody>
        <tr>
          <th style="font-size:13px;color:#555;width: 150px;vertical-align:top;padding:6px 20px 7px;background-color: #f7f7f7;border-bottom:1px solid #e4e4e4;">문의한 시간</th>
          <td style="font-size:14px;color:#555;padding:6px 30px;border-bottom:1px solid #e4e4e4;">${contact.createdDate.format('yyyy/MM/dd HH:mm:ss')!}</td>
        </tr>
        <tr>
          <th style="font-size:13px;color:#555;width: 150px;vertical-align:top;padding:6px 20px 7px;background-color: #f7f7f7;border-bottom:1px solid #e4e4e4;">성명</th>
          <td style="font-size:14px;color:#555;padding:6px 30px;border-bottom:1px solid #e4e4e4;">${contact.fullname!}</td>
        </tr>
        <tr>
          <th style="font-size:13px;color:#555;width: 150px;vertical-align:top;padding:6px 20px 7px;background-color: #f7f7f7;border-bottom:1px solid #e4e4e4;">이메일</th>
          <td style="font-size:14px;color:#555;padding:6px 30px;border-bottom:1px solid #e4e4e4;">${contact.email!}</td>
        </tr>
        <tr>
          <th style="font-size:13px;color:#555;width: 150px;vertical-align:top;padding:6px 20px 7px;background-color: #f7f7f7;border-bottom:1px solid #e4e4e4;">내용</th>
          <td style="font-size:14px;color:#555;padding:6px 30px;border-bottom:1px solid #e4e4e4;">${contact.content!}</td>
        </tr>
        </tbody>
      </table>
    </li>
    <li style="font-size:12px;color:#878787;padding:30px 20px;background-color:#efefef">
        <#include "footer.ftl"/>
    </li>
  </ul>
</div>

</body>
</html>