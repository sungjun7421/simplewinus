<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>${subject}</title>
</head>
<body>

<div style="width:640px;">
    <ul style="padding-left:0;list-style: none;">
        <li style="padding:47px 20px 12px;background-color:${metaEmail.primaryColor!}"><img src="${metaEmail.logoUri!}" style="width:142px;"></li>
        <li style="padding:20px;">
            <h1 style="font-size: 22px;font-weight: 500;color: #474747;margin:20px 0;">고객님께서 주문하신 상품이<br>
                결제완료 되었습니다.</h1>
            <p style="font-size: 14px;line-height: 1.57;color: #555555;">${fullName}고객님,<br>
                저희 쇼핑몰에서 주문하신 내역입니다.<br>
                결제하신 주문내역은 <b>마이페이지 > 주문/배송조회</b> 에서 확인하실 수 있습니다.</p>

            <h3 style="font-size: 15px;font-weight: 700;color: #474747;margin:45px 0 10px;">주문 정보</h3>
            <table style="width:100%;border-top:2px solid #000">
                <tbody>
                <tr>
                    <td style="font-size:14px;color:#555;padding:10px 0;border-bottom:1px solid #e4e4e4;">
                        주문번호<br>
                        주문일자</td>
                    <td style="text-align:right;font-size:14px;color:#555;padding:10px 5px;border-bottom:1px solid #e4e4e4;">
                        ${order.oid}<br>
                        ${order.createdDate.format('yyyy/MM/dd HH:mm')}</td>
                </tr>
                </tbody>
            </table>

            <h3 style="font-size: 15px;font-weight: 700;color: #474747;margin:45px 0 10px;">주문상품내역</h3>
            <table style="width:100%;border-top:2px solid #000">
                <tbody>
                <#list order.orderProducts as orderProduct>
                <tr>
                    <td style="width:120px;font-size:14px;color:#555;padding:10px 0;border-bottom:1px solid #e4e4e4;">
                        <div class="" style="width:100px;height:100px;background-size:cover;background-position:50% 50%;background-image:url('http://api.puremay.co.kr/${orderProduct.relativeProduct.thumbnail}')"></div>
                    </td>
                    <td style="font-size:14px;color:#555;padding:10px 0;border-bottom:1px solid #e4e4e4;">
                        <span style="font-size: 14px;font-weight: 700;color: #555;">${orderProduct.relativeProduct.name.value}</span>
                    </td>
                </tr>
                <tr>
                    <td style="font-size:14px;color:#555;padding:10px 0;border-bottom:1px solid #e4e4e4;">결제금액<br>수량</td>
                    <td style="text-align:right;font-size:14px;color:#555;padding:10px 5px;border-bottom:1px solid #e4e4e4;">${orderProduct.amount}원<br>${orderProduct.orderCount}</td>
                </tr>
                </#list>
                </tbody>
            </table>

            <h3 style="font-size: 15px;font-weight: 700;color: #474747;margin:45px 0 10px;">결제 정보</h3>
            <table style="width:100%;border-top:2px solid #000">
                <tbody>
                <tr>
                    <td style="font-size:14px;color:#555;padding:10px 0;border-bottom:1px solid #e4e4e4;">
                    총 주문 금액<br>
                    총 할인 금액<br>
                        결제수단
                    </td>
                    <td style="text-align:right;font-size:14px;color:#555;padding:10px 5px;border-bottom:1px solid #e4e4e4;">
                    ${totalOrderAmount}원<br>
                    ${discountAmount}원<br>
                        ${order.payMethod.value}
                    </td>
                </tr>
                <tr>
                    <td style="font-size:14px;font-weight:700;color:#555;padding:10px 0;">총 결제 금액</td>
                    <td style="text-align:right;font-size:14px;font-weight:700;color:#555;padding:10px 5px;">${order.priceKRW.price}원</td>
                </tr>
                </tbody>
            </table>
            <p style="font-size:14px;color:#555;margin:0;">*적립금은 최종 결제금액에 따라 계산되며 구매확정 이후 받으실 수 있습니다.</p>

            <h3 style="font-size: 15px;font-weight: 700;color: #474747;margin:45px 0 10px;">배송지 정보</h3>
            <table style="width:100%;border-top:2px solid #000">
                <tbody>
                <tr>
                    <td style="font-size:14px;color:#555;padding:10px 0;border-bottom:1px solid #e4e4e4;">
                        받으시는 분<br>
                        연락처1(연락처2)<br>
                        주소<br><br>
                        배송 시 요청사항
                    </td>
                    <td style="text-align:right;font-size:14px;color:#555;padding:10px 5px;border-bottom:1px solid #e4e4e4;">
                    ${order.shippingInfo.fullname}<br>
                    ${order.shippingInfo.phone1}(<#if order.shippingInfo.phone2?has_content>${order.shippingInfo.phone2}<#else>-</#if>)<br>
                    ${order.shippingInfo.postalCode}<br>
                    ${order.shippingInfo.address1} <#if order.shippingInfo.address2?has_content>${order.shippingInfo.address2}</#if><br>
                    <#if order.shippingInfo.message?has_content>${order.shippingInfo.message}<#else>-</#if></td>
                </tr>
                </tbody>
            </table>

            <div style="text-align:right;margin-top:40px;">
                <a href="${metaEmail.frontHost!}" style="display:inline-block;font-size: 14px;color:#fff;padding: 10px 25px;background-color:${metaEmail.primaryColor!};border-radius:2px;">${metaEmail.siteName!} 바로가기</a>
            </div>
        </li>
        <li style="font-size:12px;color:#878787;padding:30px 20px;background-color:#efefef">
            <#include "footer.ftl"/>
        </li>
    </ul>
</div>


</body>
</html>