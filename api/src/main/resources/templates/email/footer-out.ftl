<#import "/spring.ftl" as spring/>
<#--<p style="margin:2px;">이 이메일은 ${nowDate.format('yyyy년 MM월 dd일')} 현재 고객님의 메일 수신옵션과 상관없이 공지 및 안내를 위한 발신전용 메일입니다.</p>-->
<p style="margin:0 0 15px 0;">본 메일은 발신전용으로 회신되지 않습니다. 궁금하신 사항은 갤럭시아콘 고객센터를 이용해 주세요.</p>
<p style="margin:2px 0;">갤럭시아커뮤니케이션즈(주) / 대표 : 김용광 / 사업자 번호 : 120-81-60844</p>
<p style="margin:2px 0;">주소 : 서울시 강남구 광평로 281 수서오피스빌딩 15층(수서동)
<p style="margin:2px 0;">Galaxiacommunications Co, Ltd. All Rights Reserved.</p>