<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>${subject}</title>
</head>
<body>

<div style="width:640px;">
    <ul style="padding-left:0;list-style: none;">
        <li style="padding:47px 20px 12px;background-color:${metaEmail.primaryColor!}"><img src="${metaEmail.logoUri!}"
                                                                                            style="width:142px;"></li>
        <li style="padding:20px;">

            <h3>${subject}</h3>
            <p>${fullName} 님,</p>
            <p>아래의 인증코드를 입력하여 비밀번호를 재생성해주세요.</p>
            <p>인증번호를 입력하지 않을 시 비밀번호 재생성이 불가능합니다.</p>
            <h5>[인증코드]</h5>
            <h4>
                ${code}
            </h4>

        </li>
        <li style="font-size:12px;color:#878787;padding:30px 20px;background-color:#efefef">
            <#include "footer-out.ftl"/>
        </li>
    </ul>
</div>

</body>
</html>