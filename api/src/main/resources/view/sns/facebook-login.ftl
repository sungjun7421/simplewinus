<!doctype html>
<html lang="ko">
<head>
  <meta charset="utf-8">
  <title>소셜 테스트</title>
</head>
<body>
<h1>Facebook Login</h1>

<fb:login-button
        scope="public_profile,email"
        onlogin="checkLoginState();">
</fb:login-button>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '572512206692749',
      cookie     : true,
      xfbml      : true,
      version    : 'v6.0'
    });

    FB.AppEvents.logPageView();

  };

  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/ko_KR/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      console.debug(response);
    });
  }

  checkLoginState();
</script>
</body>

</html>