<#import "/spring.ftl" as spring/>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>${appName!}</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row" style="padding: 40px 0">
        <div class="col-md-6 col-md-push-3">
            <form id="form-reset-password" method="post" action="/account/reset-password">
                <h2 style="margin-bottom: 20px">${appName} 비밀번호 재설정</h2>
                <div class="form-group">
                    <label for="input-password">새 비밀번호</label>
                    <@spring.formPasswordInput "reqBody.password",
                    "placeholder=\"새 비밀번호를 입력하세요.\"
                        class=\"form-control\"
                        maxlength=\"50\"
                        required=\"required\"
                        data-parsley-minlength=\"6\"
                        data-parsley-maxlength=\"50\"
                        data-parsley-lowercase=\"1\"
                        data-parsley-number=\"1\"
                        "/>
                </div>
                <div class="form-group">
                    <label for="input-password-confirm">새 비밀번호 확인</label>
                    <@spring.formPasswordInput "reqBody.passwordConfirm",
                    "placeholder=\"비밀번호를 확인해주세요.\"
                        class=\"form-control \"
                        maxlength=\"50\"
                        required=\"required\"
                        data-parsley-equalto=\"#password\"
                        "/>
                </div>
                <input type="hidden" name="code" value="${reqBody.code!}"/>
                <button type="submit" class="btn btn-default">비밀번호 재설정</button>
            </form>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

<#-- PARSLEY-->
<script src="<@spring.url "/ad/vendor/parsleyjs/dist/parsley.min.js"/>"></script>
<script src="<@spring.url"/ad/vendor/parsleyjs/dist/i18n/ko.js"/>"></script>

<script>
  $(function () {
    $('#form-reset-password').each(function () {

      $(this).parsley().on('form:submit', function () {

        return true;
      });
    });

  });
</script>
</body>
</html>