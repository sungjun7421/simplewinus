<!doctype html>
<html lang="ko">
<head>
  <meta charset="utf-8">
  <title>소셜 테스트</title>
</head>
<body>
<h1>Access Token</h1>
<ul>
  <li><a href="#" data-type="sns-login" data-sns="naver">네이버</a></li>
  <li><a href="#" data-type="sns-login" data-sns="kakaotalk">카카오톡</a></li>
  <li><a href="#" data-type="sns-login" data-sns="band">밴드</a></li>
  <li><a href="#" data-type="sns-login" data-sns="twitter">트위터</a></li>
  <li><a href="/sns/facebook/sample" target="_blank">페이스북</a></li>
  <li><a href="#" data-type="sns-login" data-sns="youtube">유튜브</a></li>
</ul>


<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>

  $(function () {

    $('[data-type="sns-login"]').on('click', function (e) {
      e.preventDefault();

      var sns = $(this).data('sns');

      if (sns === 'naver') {
        var win = window.open("/sns/naver/access-token", "PopupWin", "width=500,height=600");
      } else if (sns === 'kakaotalk') {
        var win = window.open("/sns/kakao/access-token", "PopupWin", "width=500,height=600");
      } else if (sns === 'band') {
        var win = window.open("/sns/band/access-token", "PopupWin", "width=500,height=800");
      } else if (sns === 'twitter') {
        var win = window.open("/sns/twitter/access-token", "PopupWin", "width=500,height=600");
      } else if (sns === 'youtube') {
        var win = window.open("/sns/youtube/access-token", "PopupWin", "width=500,height=600");
      }
    })

    var callbackAccessToken = function (data) {
      var _data = JSON.parse(data);
      console.debug(_data);
    };

    window.callbackAccessToken = callbackAccessToken;

  });
</script>
</body>

</html>