import * as _ from 'lodash';

// import printMe from './api.ts';

function component() {
  // printMe();

  const element = document.createElement('div');
  element.innerHTML = _.join(['Hello', 'Axios 2'], ' ');
  return element;
}

document.body.appendChild(component());
