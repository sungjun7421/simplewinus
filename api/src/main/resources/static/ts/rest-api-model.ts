/* tslint:disable */
/* eslint-disable */
// Generated using typescript-generator version 2.27.744 on 2020-11-30 10:09:27.

export namespace APIModel {

    export interface SampleReqBody extends BaseResponseBody {
        test1: string;
        test2: number;
        test3: boolean;
    }

    export interface ResultResBody extends BaseResponseBody {
        success: boolean;
        message: string;
    }

    export interface LoginReqBody extends BaseRequestBody {
        email: string;
        password: string;
    }

    export interface SignUpReqBody extends BaseRequestBody {
        email: string;
        password: string;
        fullName: string;
        mobile: string;
        image: string;
        termsAgree: TermsAgreeReqBody;
        socialId: SocialIdReqBody;
    }

    export interface ValidEmailReqBody extends BaseRequestBody {
        email: string;
    }

    export interface ValidMobileReqBody extends BaseRequestBody {
        mobile: string;
    }

    export interface CertMobileReqBody extends BaseRequestBody {
        mobile: string;
    }

    export interface CertEmailReqBody extends BaseRequestBody {
        email: string;
    }

    export interface CertConfirmMobileReqBody extends BaseResponseBody {
        code: string;
        mobile: string;
    }

    export interface FindAccountReqBody extends BaseRequestBody {
        mobile: string;
        code: string;
    }

    export interface FindPasswordReqBody extends BaseRequestBody {
        email: string;
        mobile: string;
        fullName: string;
        code: string;
    }

    export interface ResetPasswordReqBody extends BaseRequestBody {
        code: string;
        password: string;
        passwordConfirm: string;
    }

    export interface ChangePasswordReqBody extends BaseRequestBody {
        password: string;
        newPassword: string;
        confirmNewPassword: string;
        email: string;
        phone: string;
    }

    export interface CertConfirmReqBody extends BaseResponseBody {
        code: string;
        mobile: string;
        mode: Mode;
    }

    export interface ChangeFullNameReqBody extends BaseRequestBody {
        fullName: string;
    }

    export interface ChangeSnsReqBody extends BaseRequestBody {
        snsType: SNSType;
        code: string;
        redirectUri: string;
        state: string;
    }

    export interface ChangeSnsNativeReqBody extends BaseRequestBody {
        snsType: SNSType;
        active: boolean;
        id: string;
        name: string;
    }

    export interface LeaveUserReqBody extends BaseRequestBody {
        password: string;
        leaveReason: string;
    }

    export interface MatchingPasswordReqBody extends BaseRequestBody {
        password: string;
    }

    export interface CertReqBody extends BaseRequestBody {
        mobile: string;
        mode: Mode;
        email: string;
    }

    export interface SubscribeReqBody extends BaseRequestBody {
        active: boolean;
    }

    export interface UpdateUserInfoReqBody extends BaseRequestBody {
        fullName: string;
        image: string;
        emailRcv: boolean;
        smsRcv: boolean;
        kakaoRcv: boolean;
        pushRcv: boolean;
    }

    export interface SNSReqBody extends BaseRequestBody {
        snsType: SNSType;
        code: string;
        redirectUri: string;
        state: string;
    }

    export interface Facebook extends Serializable {
        id: string;
        name: string;
        email: string;
        picture: string;
        gender: Gender;
    }

    export interface Apple extends Serializable {
        id: string;
        name: string;
        email: string;
    }

    export interface Google extends Serializable {
        id: string;
        name: string;
        email: string;
        picture: string;
        gender: Gender;
    }

    export interface GooglePojo extends Serializable {
        id: string;
        email: string;
        verified_email: boolean;
        name: string;
        given_name: string;
        family_name: string;
        link: string;
        picture: string;
    }

    export interface Kakao {
        id: string;
        email: string;
        nickName: string;
        profileImage: string;
        kaccountEmailVerified: boolean;
    }

    export interface Naver {
        id: string;
        nickname: string;
        enc_id: string;
        profile_image: string;
        age: string;
        email: string;
        name: string;
        birthday: string;
    }

    export interface AccessTokenResBody extends Serializable {
        access_token: string;
        expires_in: number;
        jti: string;
        refresh_token: string;
        scope: string;
        token_type: string;
    }

    export interface FindUserResBody extends BaseResponseBody {
        email: string;
        signUpDate: Date;
    }

    export interface MeResBody extends BaseResponseBody {
        id: number;
        email: string;
        fullName: string;
        mobile: string;
        roles: Role[];
        image: string;
        termsAgree: TermsAgree;
        verification: Verification;
        expiredPassword: boolean;
        hasPassword: boolean;
        dormancy: boolean;
        totalPoint: number;
        expiredPoint: number;
        buyerLevel: BuyerLevel;
    }

    export interface ChangeSubscribeResBody extends BaseResponseBody {
        agree: boolean;
        time: Date;
    }

    export interface MyPointInfoResBody extends BaseResponseBody {
        level: BuyerLevelResBody;
        point: PointOfMemberResBody;
    }

    export interface UserResBody extends BaseResponseBody {
        id: number;
        fullName: string;
        email: string;
        mobile: string;
    }

    export interface SNSResBody extends BaseResponseBody {
        access_token: string;
        redirectUri: string;
        message: string;
        snsType: SNSType;
        id: string;
        name: string;
        email: string;
        image: string;
        verifiedEmail: boolean;
        birth: Date;
        result: boolean;
    }

    export interface BaseResponseBody extends Serializable {
    }

    export interface BaseRequestBody extends Serializable {
    }

    export interface TermsAgreeReqBody extends BaseRequestBody {
        taService: boolean;
        taPrivacy: boolean;
        taYouth: boolean;
        taEft: boolean;
        taLocation: boolean;
        smsRcv: boolean;
        emailRcv: boolean;
        pushRcv: boolean;
        kakaoRcv: boolean;
    }

    export interface SocialIdReqBody extends BaseRequestBody {
        facebookId: string;
        facebookName: string;
        googleId: string;
        googleName: string;
        kakaoId: string;
        kakaoName: string;
        appleId: string;
        appleName: string;
        naverId: string;
        naverName: string;
    }

    export interface Serializable {
    }

    export interface TermsAgree extends Serializable {
        taService: boolean;
        taPrivacy: boolean;
        taYouth: boolean;
        taEft: boolean;
        taLocation: boolean;
        smsRcv: boolean;
        smsRcvDate: Date;
        emailRcv: boolean;
        emailRcvDate: Date;
        pushRcv: boolean;
        pushRcvDate: Date;
        kakaoRcv: boolean;
        kakaoRcvDate: Date;
        taMarketing: boolean;
    }

    export interface Verification extends Serializable {
        email: boolean;
        mobile: boolean;
        token: string;
    }

    export interface BuyerLevel extends AbstractEntityInternational<number> {
        id: number;
        name: InterText;
        description: InterText;
        image: string;
        level: number;
        saveRate: number;
        discountExpression: string;
        birthSavingExpression: string;
    }

    export interface BuyerLevelResBody extends BaseResponseBody {
        idBuyer: number;
        level: number;
        name: string;
        description: string;
        pointRate: number;
    }

    export interface PointOfMemberResBody extends BaseResponseBody {
        idBuyer: number;
        totalPoint: number;
        expected: number;
        expirePoint: number;
    }

    export interface InterText extends AbstractInternational {
        empty: boolean;
    }

    export interface AbstractInternational extends Serializable {
        value: string;
        textKoKr: string;
        textEnUs: string;
        textZhCn: string;
        textZhTw: string;
        textJaJp: string;
    }

    export interface AbstractEntityInternational<K> extends AbstractEntity<K> {
    }

    export interface AbstractEntity<K> extends Serializable {
        createdDate: Date;
        id: K;
    }

    export type Mode = "CERT_MOBILE" | "CERT_EMAIL" | "FIND_PASSWORD_EMAIL" | "FIND_PASSWORD_MOBILE" | "FIND_ACCOUNT_MOBILE";

    export type SNSType = "FACEBOOK" | "GOOGLE" | "NAVER" | "KAKAO" | "APPLE" | "INSTAGRAM";

    export type Gender = "MALE" | "FEMALE";

    export type Role = "SUPER" | "ADMIN" | "MANAGER" | "USER";

}
