package com.logisall.anyware.api.web.account;

import com.logisall.anyware.api.service.account.cert.CertService;
import com.logisall.anyware.api.service.account.user.UserAPIService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.ResponseErrorCode;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.model.reqbody.account.*;
import com.logisall.anyware.core.model.resbody.ResultResBody;
import com.logisall.anyware.core.service.send.email.SendEmailService;
import com.logisall.anyware.core.utils.ValidUtils;
import com.google.common.collect.ImmutableMap;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.HashMap;

@Tag(name = "Account", description = "사용자 계정 API")
@Slf4j
@RestController
@RequestMapping("/api/v1")
public class APIAccountController {

  @Value("${spring.application.name}")
  private String appName;

  @Value("${app.client.host}")
  private String clientHost;

  @Value("${app.image.host}")
  private String imageHost;

  @Autowired
  private UserAPIService userAPIService;

  @Autowired
  private CertService certService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private SendEmailService sendEmailService;

  /**
   * [account-login-1] 로그인
   */
  @Operation(summary = "[account-login-1] Login (로그인)")
  @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> restLogin(HttpServletRequest request,
                                     @RequestBody LoginReqBody requestBody) {

    if (requestBody == null
        || StringUtils.isEmpty(requestBody.getEmail())
        || StringUtils.isEmpty(requestBody.getPassword())) {
      throw new BadRequestException();
    }
    return userAPIService.authAccessToken(requestBody, request);
  }

  @Operation(summary = "[account-sign-up-1] Sign Up (회원가입)")
  @PostMapping(value = "/signup", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> signup(@RequestBody SignUpReqBody requestBody,
                                  HttpServletRequest request) {

    if (requestBody.isValid()) {
      throw new BadRequestException("필수항목을 입력하세요.");
    }

    // 이용약관 및 개인정보취급방침에 동의하지 안함
    if (!requestBody.getTermsAgree().isTaService()
        || !requestBody.getTermsAgree().isTaPrivacy()
        || !requestBody.getTermsAgree().isTaLocation()
    ) {
      throw new BadRequestException("필수항목에 동의하여야 가입할 수 있습니다.");
    }

    if (requestBody.getSocialId() == null
        || (requestBody.getSocialId() != null && requestBody.getSocialId().toSocialId().isEmpty())) {

      if (!ValidUtils.isPasswordPattern(requestBody.getPassword())) {
        throw new BadRequestException(ValidUtils.MESSAGE_PASSWORD_NEW);
      }
    }

    User user = userAPIService.join(requestBody);

    // 가입 환영 이메일
    String subject = String.format("%s 회원이 되신걸 환영합니다.", appName);
    String email = user.getEmail();

    java.util.Map<String, Object> model = new HashMap<>();
    model.put("subject", subject);
    model.put("fullName", user.getFullName());
    model.put("email", email);
    model.put("createdDate", user.getCreatedDate());

    model.put("clientHost", clientHost);
    model.put("imageHost", imageHost);
    model.put("appName", appName);
    model.put("nowDate", LocalDate.now());

    try {
      sendEmailService.send(email, subject, model, "email/sign-up.ftl");
    } catch (Exception e) {
      e.printStackTrace();
      log.error("account send email", e);
    }

    try {
      CertReqBody cert = new CertReqBody();
      cert.setMode(CertReqBody.Mode.CERT_EMAIL);
      cert.setEmail(email);
      String code = certService.cert(cert, request);

      log.debug("> Email 인증 코드 : {}", code);

    } catch (Exception e) {
      e.printStackTrace();
      log.error("account cert", e);
    }

    return ResponseEntity.ok(
        ImmutableMap
            .builder()
            .put("access_token", userAPIService.getJWT(user))
            .build()
    );
  }

  /**
   * [account-sign-up-2] 이메일 유효성 검사 (탈퇴한 이메일인지, 이미 존재하는 이메일인지 체크)
   */
  @Operation(summary = "[account-sign-up-2] Email validation (이메일 유효성 검사)", description = "탈퇴한 이메일인지, 이미 존재하는 이메일인지 체크")
  @PostMapping(value = "valid-email", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ResultResBody> validEmail(@RequestBody ValidEmailReqBody requestBody) {

    if (requestBody == null
        || StringUtils.isEmpty(requestBody.getEmail())) {
      throw new BadRequestException();
    }

    ResultResBody resultResBody = userRepository.findByEmail(requestBody.getEmail()).map(user -> {
      if (!user.getUserDetailsMeta().isEnabled()
          || user.getLeaveMeta().isLeave()) {
        throw new BadRequestException(ResponseErrorCode.LEAVED_USER.getMessage());
      }
      return ResultResBody.of(false, ResponseErrorCode.ALREADY_EXIST_EMAIL.getMessage());
    })
        .orElse(ResultResBody.of(true, "가입정보가 없는 이메일주소입니다."));

    return ResponseEntity.ok(resultResBody);
  }

  /**
   * [account-sign-up-3] 휴대폰번호 유효성 검사 (탈퇴한 휴대폰번호인지, 이미 존재하는 휴대폰번호인지 체크)
   */
  @Operation(summary = "[account-sign-up-3] Mobile validation (휴대폰번호 유효성 검사)", description = "Check whether it is a withdrawn phone or an existing phone (탈퇴한 휴대폰번호인지, 이미 존재하는 휴대폰번호인지 체크)")
  @PostMapping(value = "valid-mobile", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ResultResBody> validPhone(@RequestBody ValidMobileReqBody requestBody) {

    if (requestBody == null
        || StringUtils.isEmpty(requestBody.getMobile())) {
      throw new BadRequestException();
    }

    User user = userRepository.findFirstByMobile(requestBody.getMobile());

    if (user != null) {
      if (!user.getUserDetailsMeta().isEnabled()
          || user.getLeaveMeta().isLeave()) {
        throw new BadRequestException(ResponseErrorCode.LEAVED_USER.getMessage());
      }
      return ResponseEntity.ok(ResultResBody.of(false, ResponseErrorCode.ALREADY_EXIST_PHONE.getMessage()));
    }
    return ResponseEntity.ok(ResultResBody.of(true, "가입정보가 없는 휴대폰번호입니다."));
  }
}
