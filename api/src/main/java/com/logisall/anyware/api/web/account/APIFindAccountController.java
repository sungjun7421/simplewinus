package com.logisall.anyware.api.web.account;

import com.logisall.anyware.api.service.account.user.UserAPIService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.authenticode.Authenticode;
import com.logisall.anyware.core.model.AuthentiCodeResBody;
import com.logisall.anyware.core.model.reqbody.account.FindAccountReqBody;
import com.logisall.anyware.core.model.reqbody.account.FindPasswordReqBody;
import com.logisall.anyware.core.model.reqbody.account.ResetPasswordReqBody;
import com.logisall.anyware.core.model.resbody.ResultResBody;
import com.logisall.anyware.core.service.authenticode.AuthenticodeService;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Tag(name = "Find Account", description = "사용자 찾기 API")
@Slf4j
@RestController
@RequestMapping("/api/v1/find")
public class APIFindAccountController {

  @Autowired
  private UserAPIService userAPIService;

  @Autowired
  private AuthenticodeService authenticodeService;


  @Operation(summary = "[find-1] Find Email - [1]Send SMS authentication number (이메일 찾기 - [1]모바일 인증번호 발송)", description = "[1]Send SMS authentication number, [2]Check authentication number, [3]Get a list of ID(email)s. ([1]SMS 인증번호 발송, [2]인증번호확인, [3]ID(이메일) 항목 가져오기)")
  @PostMapping(value = "/account/mobile", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> findAccountMobile(HttpServletRequest request,
                                             @RequestBody FindAccountReqBody requestBody) {

    if (requestBody == null
        || StringUtils.isEmpty(requestBody.getMobile())) {
      throw new BadRequestException();
    }

    final String mobile = requestBody.getMobile();
    userAPIService.findAccountByMobile(mobile, request);
    return ResponseEntity.ok().body(ResultResBody.of(true, "인증번호가 발송되었습니다."));
  }

  @Operation(summary = "[find-2] Find Email - [2]Check authentication number, [3]Get a list of ID(email)s (이메일 찾기 - [2]인증번호확인, [3]아이디 확인 페이지 이동)", description = "[1]Send SMS authentication number, [2]Check authentication number, [3]Get a list of ID(email)s. ([1]SMS 인증번호 발송, [2]인증번호확인, [3]ID(이메일) 항목 가져오기)")
  @PostMapping(value = "/account/mobile/confirm", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> findAccountMobileConfirm(@RequestBody FindAccountReqBody requestBody) {

    if (requestBody == null
        || StringUtils.isEmpty(requestBody.getCode())
        || StringUtils.isEmpty(requestBody.getMobile())) {
      throw new BadRequestException();
    }

    return ResponseEntity.ok(userAPIService.findAccountByMobileConfirm(requestBody));
  }

  @Operation(summary = "[find-3] Reset password - Send authentication email (비밀번호 재설정 - 인증 메일 발송)", description = "Go to password reset page (비밀번호 재설정 페이지로 이동)")
  @PostMapping(value = "/reset/password", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> resetPassword1(HttpServletRequest request,
                                          @RequestBody FindPasswordReqBody requestBody) {

    if (requestBody == null
        || StringUtils.isEmpty(requestBody.getEmail())
        || !ValidUtils.isEmailPattern(requestBody.getEmail())) {
      throw new BadRequestException();
    }


    final String email = requestBody.getEmail();
    userAPIService.findPasswordByEmail(email, request);
    return ResponseEntity.ok(ResultResBody.of(true, "인증 메일이 발송되었습니다."));
  }

  @Operation(summary = "[find-4] Reset password 2 - (비밀번호 재설정 - 코드+새비밀번호 입력)", description = "이메일의 전송된 코드와 새비밀번호를 전송")
  @PostMapping(value = "/reset/password/change", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> resetPassword2(@RequestBody ResetPasswordReqBody body) {

    if(body == null || StringUtils.isEmpty(body.getCode()) || StringUtils.isEmpty(body.getPassword()) || StringUtils.isEmpty(body.getPasswordConfirm())) {
      throw new BadRequestException("잘못된 값입니다.");
    }

    if(!body.getPassword().equals(body.getPasswordConfirm())) {
      throw new BadRequestException("비밀번호가 일치하지 않습니다.");
    }

    log.debug("> ResetPasswordReqBody : {}", body);

    AuthentiCodeResBody authentiCode = authenticodeService.confirmByEmail(body.getCode());

    log.debug("authentiCode : {}", authentiCode);

    switch (authentiCode.getResultCode()) {
      case Authenticode.RESULT_SUCCESS:
        userAPIService.resetPasswordByEmail(body.getPassword(), authentiCode.getEmail());
        return ResponseEntity.ok(ResultResBody.of(true, "비밀번호가 변경되었습니다."));
      case Authenticode.RESULT_EXPIRE:
        return ResponseEntity.ok(ResultResBody.of(false, "인증코드가 만료되었습니다."));
      case Authenticode.RESULT_NOT_EXISTS:
        return ResponseEntity.ok(ResultResBody.of(false, "존재하지 않는 계정입니다."));
      case Authenticode.RESULT_FAIL:
        return ResponseEntity.ok(ResultResBody.of(false, "인증에 실패하였습니다."));
    }
    throw new BadRequestException();
  }
}
