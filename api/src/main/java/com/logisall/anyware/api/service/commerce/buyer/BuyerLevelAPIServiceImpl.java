package com.logisall.anyware.api.service.commerce.buyer;

import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevelRepository;
import com.logisall.anyware.core.model.resbody.commerce.level.BuyerLevelResBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

@Slf4j
@Service
public class BuyerLevelAPIServiceImpl implements BuyerLevelAPIService {

  @Autowired
  private BuyerLevelRepository buyerLevelRepository;

  @Override
  @Transactional
  public BuyerLevelResBody findByBuyerId(Long buyerId, Locale locale) {

    BuyerLevel buyerLevel = buyerLevelRepository.findByBuyerId(buyerId);
    if (buyerLevel != null) {
      buyerLevel.setLocale(locale);
      buyerLevel.lazy();
      return buyerLevel.toBuyerLevelResBody(locale);
    } else {
      return null;
    }
  }
}
