package com.logisall.anyware.api.service.commerce.wish;

import com.logisall.anyware.core.domain.commerce.wish.Wish;
import org.springframework.http.ResponseEntity;

public interface WishAPIService {
    ResponseEntity<?> getListWishOf(String email);
    ResponseEntity<?> getListWishOf(Long userId);
    ResponseEntity<?> toggleProduct(Long idProduct,Long idUser);
    ResponseEntity<?> toggleProduct(Long idProduct,String emailUser);
    void delete(Wish.Id id);
}
