//package com.logisall.anyware.api.service.commerce.payment.danal;
//
//import com.logisall.anyware.core.config.exception.BadRequestException;
//import com.logisall.anyware.core.config.exception.NotFoundException;
//import com.logisall.anyware.core.domain.user.User;
//import com.logisall.anyware.core.model.reqbody.consultingroom.IamPortReadyReqBody;
//import com.logisall.anyware.core.service.account.user.UserService;
//import com.logisall.anyware.core.utils.DateUtils;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.data.jpa.repository.Modifying;
//import org.springframework.mobile.device.Device;
//import org.springframework.mobile.device.DevicePlatform;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//import com.logisall.anyware.core.model.reqbody.commerce.payment.IamPortReadyReqBody;
//
//import java.math.BigDecimal;
//import java.text.DecimalFormat;
//import java.util.HashMap;
//import java.util.Map;
//
//@Slf4j
//@Service
//public class DanalAPIServiceImpl implements DanalAPIService {
//
//  @Autowired
//  private UserService userService;
//
//  @Value("${app.payment.danal.returnurl}")
//  private String RETURNURL;
//
//  @Value("${app.payment.danal.cancelurl}")
//  private String CANCELURL;
//
//
//  @Override
//  @Transactional
//  @Modifying
//  public Map ready(IamPortReadyReqBody paymentReqBody, String email, Device device) {
//    User user = userService.get(email);
//    ;
//    if (user == null) {
//      throw new RuntimeException("User not found!");
//    }
//
//
//    String oid = paymentReqBody.getOid();
////    ConsultingRoom payment = consultingRoomRepository.getPaymentNotComplete(oid);
//
////    if (payment == null || payment.getStatus().name().equalsIgnoreCase(ConsultingRoom.PaymentStatus.AMOUNT_NOT_MATCH.name())) {
////      throw new NotFoundException("Consult Room not found Or Complete!");
////    }
////
////    //re-save payment method
////    payment.setPayMethod(paymentReqBody.getPayMethod());
////    payment.setTax(new BigDecimal(0));
////    payment.setStatus(ConsultingRoom.PaymentStatus.WAITING_FUND);
////    BigDecimal _amount = payment.getPrice();
////    payment.setTotalPrice(_amount);
////    payment.setTimeStart(DateUtils.getLocalDate());
////    payment.setTimeEnd(DateUtils.getLocalDate().plusMonths(1));
////    payment.setTotalPriceFloor(new BigDecimal(new DecimalFormat("#.##").format(_amount.doubleValue())));
////    payment = consultingRoomRepository.save(payment);
////    log.debug("payment : {}", payment);
////
////
////    String itemname = payment.getRelativeConsultingProduct().getName();
//    /**
//     * WP : PC Web
//     * WM : Mobile Web
//     * WA : Mobile App(Android)
//     * WI : Mobile App(IOS)
//     */
//
//    String useragent = "WP";
//    if (device.isMobile()) {
//      useragent = "WM";
//      if (device.getDevicePlatform().equals(DevicePlatform.ANDROID)) {
//
//      } else if (device.getDevicePlatform().equals(DevicePlatform.IOS)) {
//
//      }
//    }
//    log.debug("useragent : {}", useragent);
//    String orderid = payment.getOid();
//    String username = payment.getRelativeUser().getFullName();
//    String userid = payment.getRelativeUser().getId().toString();
//    String useremail = payment.getRelativeUser().getEmail();
//
//    Map REQ_DATA = new HashMap();
//    Map RES_DATA = null;
//
//    /******************************************************
//     *  RETURNURL 	: CPCGI페이지의 Full URL을 넣어주세요
//     *  CANCELURL 	: BackURL페이지의 Full URL을 넣어주세요
//     ******************************************************/
////    String RETURNURL = "https://pawsome.kr/payment/danal/cpcgi";
////    String CANCELURL = "https://pawsome.kr/payment/danal/cancel";
////    String RETURNURL = "https://pawsome.kr/api/v1/payment-consultingroom/danal/cpcgi";
////    String CANCELURL = "https://pawsome.kr/api/v1/payment-consultingroom/danal/cancel";
////    String RETURNURL = "http://localhost:3001/api/v1/payment-consultingroom/danal/cpcgi";
////    String CANCELURL = "http://localhost:3001/api/v1/payment-consultingroom/danal/cancel";
//
//    /**************************************************
//     * SubCP 정보
//     **************************************************/
//    REQ_DATA.put("SUBCPID", "9810030929");
//
//    /**************************************************
//     * 결제 정보
//     **************************************************/
//    REQ_DATA.put("AMOUNT", _amount.intValue());
//    REQ_DATA.put("CURRENCY", "410");
//    REQ_DATA.put("ITEMNAME", itemname);
//    REQ_DATA.put("USERAGENT", useragent);
//    REQ_DATA.put("ORDERID", orderid);
//    REQ_DATA.put("OFFERPERIOD", DateUtils.getDateString(payment.getTimeStart()) + DateUtils.getDateString(payment.getTimeEnd()));
//
//    /**************************************************
//     * 고객 정보
//     **************************************************/
//    REQ_DATA.put("USERNAME", username); // 구매자 이름
//    REQ_DATA.put("USERID", userid); // 사용자 ID
//    REQ_DATA.put("USEREMAIL", useremail); // 소보법 email수신처
//
//    /**************************************************
//     * URL 정보
//     **************************************************/
//    REQ_DATA.put("CANCELURL", CANCELURL);
//    REQ_DATA.put("RETURNURL", RETURNURL);
//
//    /**************************************************
//     * 기본 정보
//     **************************************************/
//    REQ_DATA.put("TXTYPE", "AUTH");
//    REQ_DATA.put("SERVICETYPE", "DANALCARD");
//    REQ_DATA.put("ISNOTI", "N");
//    REQ_DATA.put("BYPASSVALUE", "this=is;a=test;bypass=value"); // BILL응답 또는 Noti에서 돌려받을 값. '&'를 사용할 경우 값이 잘리게되므로 유의.
//
//    log.debug(">>>>  1  RES_DATA : {}", RES_DATA);
//    RES_DATA = new DanalUtils().CallCredit(REQ_DATA, false);
//    log.debug(">>>>  2  RES_DATA : {}", RES_DATA);
//    return RES_DATA;
//  }
//
//  @Override
//  @Transactional
//  @Modifying
//  public ConsultingRoom.PaymentStatus cpcgi(String RETURNPARAMS, String email) {
//
//
//    String RES_STR = new DanalUtils().toDecrypt((String) RETURNPARAMS);
//    Map retMap = new DanalUtils().str2data(RES_STR);
//
//    log.debug("retMap : {}", retMap);
//    // {ORDERID=oid_1609995273309-8-1-21, RETURNCODE=0000, RETURNMSG=성공, TID=202101071354359665984400}
//
//    String returnCode = (String) retMap.get("RETURNCODE");
//    String returnMsg = (String) retMap.get("RETURNMSG");
//
//
//    //*****  신용카드 인증결과 확인 *****************
//    if (returnCode == null || !"0000".equals(returnCode)) {
//      // returnCode가 없거나 또는 그 결과가 성공이 아니라면 실패 처리
//      System.out.println("Authentication failed. " + returnMsg + "[" + returnCode + "]");
//      throw new BadRequestException("인증에 실패하였습니다. (" + "Authentication failed. " + returnMsg + "[" + returnCode + "]" + ")");
//    }
//
//    ConsultingRoom consultingRoom = consultingRoomRepository.getPaymentNotComplete((String) retMap.get("ORDERID"));
//    consultingRoom.setTid((String) retMap.get("TID"));
//
//    /*[ 필수 데이터 ]***************************************/
//    Map REQ_DATA = new HashMap();
//    Map RES_DATA = new HashMap();
//
//    /**************************************************
//     * 결제 정보
//     **************************************************/
//    REQ_DATA.put("TID", (String) retMap.get("TID"));
//    REQ_DATA.put("AMOUNT", consultingRoom.getPrice().intValue()); // 최초 결제요청(AUTH)시에 보냈던 금액과 동일한 금액을 전송
//
//    /**************************************************
//     * 기본 정보
//     **************************************************/
//    REQ_DATA.put("TXTYPE", "BILL");
//    REQ_DATA.put("SERVICETYPE", "DANALCARD");
//
//    RES_DATA = new DanalUtils().CallCredit(REQ_DATA, false);
//
//    String RETURNCODE = (String) RES_DATA.get("RETURNCODE");
//    String RETURNMSG = (String) RES_DATA.get("RETURNMSG");
//
//    consultingRoom.setTmessage(RETURNMSG);
//
//    if ("0000".equals(RETURNCODE)) {
//      consultingRoom.setStatus(ConsultingRoom.PaymentStatus.COMPLETE);
//      consultingRoom.setPaymentTime(DateUtils.getLocalDateTime());
//      consultingRoom.setCurrency("KRW");
//      consultingRoom.setStatusConsulting(ConsultingRoom.StatusConsulting.PROGRESS);
//
//      return ConsultingRoom.PaymentStatus.COMPLETE;
//    } else {
//      consultingRoom.setStatus(ConsultingRoom.PaymentStatus.FAIL);
//      return ConsultingRoom.PaymentStatus.FAIL;
//    }
//  }
//}
