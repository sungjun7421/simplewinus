package com.logisall.anyware.api.service.commerce.buyer;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.buyer.BuyerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Service
public class BuyerAPIServiceImpl implements BuyerAPIService {

    @Autowired
    private BuyerRepository buyerRepository;

    @Override
    @Transactional
    public Buyer get(Long id) {
        return buyerRepository.findById(id)
                .map(buyer -> {
                    buyer.lazy();
                    return buyer;
                }).orElseThrow(BadRequestException::new);
    }

    @Override
    @Transactional
    public Buyer get(String email) {
        return Optional.ofNullable(buyerRepository.findByRelativeUser_Email(email))
                .map(buyer -> {
                    buyer.lazy();
                    return buyer;
                }).orElseThrow(BadRequestException::new);
    }
}
