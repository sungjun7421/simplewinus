package com.logisall.anyware.api.service.account.user;


import com.logisall.anyware.api.service.account.cert.CertService;
import com.logisall.anyware.core.config.database.PwdEncConfig;
import com.logisall.anyware.core.config.exception.*;
import com.logisall.anyware.core.domain.authenticode.Authenticode;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.buyer.BuyerRepository;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevel;
import com.logisall.anyware.core.domain.commerce.buyerlevel.BuyerLevelRepository;
import com.logisall.anyware.core.domain.email.EmailHistoryRepository;
import com.logisall.anyware.core.domain.sms.SMSHistoryRepository;
import com.logisall.anyware.core.domain.user.*;
import com.logisall.anyware.core.model.AuthentiCodeResBody;
import com.logisall.anyware.core.model.file.FileMeta;
import com.logisall.anyware.core.model.file.FileUploadService;
import com.logisall.anyware.core.model.property.PrivacyExpireTime;
import com.logisall.anyware.core.model.reqbody.account.*;
import com.logisall.anyware.core.model.resbody.account.AccessTokenResBody;
import com.logisall.anyware.core.model.resbody.account.FindUserResBody;
import com.logisall.anyware.core.model.resbody.account.MeResBody;
import com.logisall.anyware.core.model.resbody.account.UserInfoResBody;
import com.logisall.anyware.core.service.authenticode.AuthenticodeService;
import com.logisall.anyware.core.service.commerce.point.PointService;
import com.logisall.anyware.core.service.restapi.RestAPIService;
import com.logisall.anyware.core.service.restapi.RestAPIServiceImpl;
import com.logisall.anyware.core.service.send.email.SendEmailService;
import com.logisall.anyware.core.service.send.sms.SendSMSService;
import com.logisall.anyware.core.utils.DateUtils;
import com.logisall.anyware.core.utils.StringUtils;
import com.google.common.collect.Lists;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Headers;
import okhttp3.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class UserAPIServiceImpl implements UserAPIService {

  @Value("${server.port}")
  private String port;

  @Value("${oauth.password.client.id}")
  private String clientId;

  @Value("${jwt.token.expire-time}")
  private int tokenExpireTime;

  @Value("${oauth.password.client.secret}")
  private String secretKey;

  @Autowired
  private KeyPair keyPair;

  @Value("${app.api.host}")
  private String authHost;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private AuthorityRepository authorityRepository;

  @Autowired
  private CertService certService;

  @Autowired
  private PrivacyExpireTime privacyExpireTime;

  @Autowired
  private AuthenticodeService authenticodeService;

  @Autowired
  private FileUploadService fileUploadService;

  @Autowired
  private EmailHistoryRepository emailHistoryRepository;

  @Autowired
  private SendEmailService sendEmailService;

  @Autowired
  private SendSMSService sendSmsService;

  @Autowired
  private SMSHistoryRepository smsHistoryRepository;

  @Autowired
  private PointService pointService;

  @Autowired
  private BuyerLevelRepository buyerLevelRepository;

  @Autowired
  private BuyerRepository buyerRepository;

  @Autowired
  private JwtTokenStore jwtTokenStore;

  @Autowired
  private PwdEncConfig pwdEncConfig;

  @Autowired
  private RestAPIService restAPIService;


//  @Autowired
//  private DormancyUserRepository dormancyUserRepository;

  @Override
  @Transactional(readOnly = true)
  public User get(Long id) {
    return userRepository.findById(id)
        .map(user -> {
          user.lazy();
          return user;
        }).orElse(null);
  }

  @Override
  @Transactional(readOnly = true)
  public User get(String email) {
    return userRepository.findByEmail(email)
        .map(user -> {
          user.lazy();
          return user;
        }).orElse(null);
  }

  @Override
  @Transactional(readOnly = true)
  public Authority authority(Authority.Role role) {
    return authorityRepository.findByRole(role);
  }


  @Override
  @Transactional
  public User join(SignUpReqBody signUpReqBody) {

    // 이메일로 회원가입 할경우 비밀번호 입력 체크
    if (signUpReqBody.getSocialId() == null
        && StringUtils.isEmpty(signUpReqBody.getPassword())) {
      throw new BadRequestException(ResponseErrorCode.NOT_ENTERED_PASSWORD.getMessage());
    }

    // 이미 존재하는 이메일입니다.
    if (userRepository.existsByEmail(signUpReqBody.getEmail())) {
      throw new ConflictException(ResponseErrorCode.ALREADY_EXIST_ACCOUNT.getMessage());
    }

    if (userRepository.existsByMobile(signUpReqBody.getMobile())) {
      throw new ConflictException(ResponseErrorCode.ALREADY_EXIST_PHONE.getMessage());
    }

    // 모바일 인증 필수
//        if (signUpReqBody.getVerification() == null || StringUtils.isEmpty(signUpReqBody.getVerification().getToken())) {
//            throw new ConflictException(ResponseErrorCode.PHONE_HAS_NOT_VERIFIED.getMessage());
//        }
//
//        AuthentiCodeResBody result = authenticodeService.confirmByMobile(signUpReqBody.getVerification().getToken(), signUpReqBody.getMobile());
//
//        if (Objects.equals(result.getResultCode(), Authenticode.RESULT_SUCCESS)) {
//            authenticodeService.deleteByToken(signUpReqBody.getVerification().getToken());
//        } else {
//            throw new ConflictException(ResponseErrorCode.PHONE_HAS_NOT_VERIFIED.getMessage());
//        }

    User user = signUpReqBody.toUser();

    if (user == null) {
      throw new BadRequestException(ResponseErrorCode.NOT_ENTERED_USER.getMessage());
    }

    if (user.getId() != null) {
      throw new ConflictException(ResponseErrorCode.ALREADY_EXIST_USER.getMessage());
    }

    if (StringUtils.isNotEmpty(user.getPassword())) {
      user.setPassword(pwdEncConfig.getPasswordEncoder().encode(user.getPassword()));
    }

    user.setImage(StringUtils.isEmpty(user.getImage()) ? User.DEFAULT_PROFILE_IMAGE : user.getImage());

    if (user.getTermsAgree().isEmailRcv()) {
      user.getTermsAgree().setEmailRcvDate(LocalDateTime.now());
    }
    if (user.getTermsAgree().isSmsRcv()) {
      user.getTermsAgree().setSmsRcvDate(LocalDateTime.now());
    }

    User _user = UserAPIServiceImpl.setUserAuthorities(authorityRepository, user);
    _user.getAuthorities().add(authorityRepository.findByRole(Authority.Role.USER));

    // Add Commerce Buyer User
    Buyer buyer = new Buyer();
    BuyerLevel buyerLevel = buyerLevelRepository.findByLevel(1);
    buyer.setRelativeBuyerLevel(buyerLevel);
    buyer.setRelativeUser(_user);
    _user.setRelativeBuyer(buyer);
    buyerRepository.save(buyer);

    return userRepository.save(user);
  }

  @Override
  @Transactional(readOnly = true)
  public String getJWT(Long id) {
    return userRepository.findById(id).map(this::getJWT).orElseThrow(UnAuthorizedException::new);
  }

  @Override
  @Transactional(readOnly = true)
  public String getJWT(User user) {

    return Jwts.builder()
        .setHeaderParam("typ", "JWT")
        .setExpiration(new Date(LocalDateTime.now().plusSeconds(tokenExpireTime).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli())) // 만료일
        .setId(UUID.randomUUID().toString()) // 고유값

        .claim("client_id", clientId)
        .claim("user_name", user.getEmail())
        .claim("authorities", user.getRoles())
        .claim("scope", Collections.singletonList("read"))

        .signWith(SignatureAlgorithm.RS256, keyPair.getPrivate())
        .compact();
  }

  @Override
  @Transactional
  public void login(Long id) {
    userRepository.findById(id)
        .map(user -> {
          UserDetails userDetails = user.getUserDetails();
          Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, user.getPassword(), user.getGrantedAuthorities());
          SecurityContextHolder.getContext().setAuthentication(authentication);
          user.getDormancyMeta().setLastLoginDate(LocalDateTime.now());
          return user;
        }).orElseThrow(BadRequestException::new);
  }

  @Override
  @Transactional(readOnly = true)
  public MeResBody profile(String email, Locale locale) {

    if (StringUtils.isEmpty(email)) {
      throw new RuntimeException();
    }

    User user = userRepository.findByEmail(email).orElseThrow(() -> {
      return new BadRequestException(ResponseErrorCode.ENTERED_INVALID.getMessage());
    });

    // 탈퇴한 계정인지 체크
    this.isLeaved(user);

    MeResBody meResBody = user.toBody(locale);

    meResBody.setExpiredPassword(this.isExpiredPassword(user));
    meResBody.setTotalPoint(pointService.total(user.getId()));
    meResBody.setExpiredPoint(pointService.expireScheduledTotal(user.getId()));

    log.debug("me ::: {}", meResBody);
    return meResBody;
  }

//  @Override
//  @Transactional(readOnly = true)
//  public ResponseEntity<?> profileByQr(String qrCode) {
//    if (org.springframework.util.StringUtils.isEmpty(qrCode)) {
//      throw new BadRequestException();
//    }
//
//    User user = userRepository.findByQrCode(qrCode);
//
//    if (user == null) {
//      throw new BadRequestException("존재하지 않는 계정입니다.");
//    }
//
//    this.isLeaved(user);
//
//    if (!user.getRoleTopLevel().equals(Authority.Role.SUPER)
//            && !user.getRoleTopLevel().equals(Authority.Role.ADMIN)
//    ) {
////      throw new UnAuthorizedException("관리자에 의해 중지된 계정입니다.");
//    }
//
//    user.getDormancyMeta().setLastLoginDate(LocalDateTime.now());
//    userRepository.save(user);
//
//    return ResponseEntity.ok(
//            ImmutableMap
//                    .builder()
//                    .put("access_token", this.getJWT(user))
//                    .build()
//    );
//  }

  @Override
  @Transactional
  public void changePassword(ChangePasswordReqBody changePasswordReqBody, String email) {

    final String password = changePasswordReqBody.getPassword();
    final String newPassword = changePasswordReqBody.getNewPassword();

    User user = userRepository.findByEmail(email).orElse(null);
//    if (StringUtils.isEmpty(user.getPassword()) && !user.getSocialId().isEmpty()) {
//      // 패스워드 가 없고, 쇼셜 ID가 존재할 경우 패스워드 등록한다.
//      user.setPassword(pwdEncConfig.getPasswordEncoder().encode(newPassword));
//    } else
    if (pwdEncConfig.getPasswordEncoder().matches(password, user.getPassword())) {
      user.setPassword(pwdEncConfig.getPasswordEncoder().encode(newPassword));

    } else {
      throw new BadRequestException(ResponseErrorCode.INVALID_ENTERED_PASSWORD.getMessage());
    }
  }

//  @Override
//  @Transactional
//  public String updateQr(String email) {
//    User user = userRepository.findByEmail(email).orElse(null);
//    if (user == null) {
//      throw new BadRequestException("존재하지 않은 계정입니다.");
//    }
//    RandomStringGenerator generator = new RandomStringGenerator.Builder().withinRange('0', 'Z').filteredBy(LETTERS, DIGITS).build();
//    String date = new SimpleDateFormat("yymmddhhmmss").format(new Date());
//    user.setQrCode(generator.generate(8) + date);
//    return user.getQrCode();
//  }


  @Override
  @Transactional
  public void resetPasswordByEmail(String newPassword, String email) {

    User user = userRepository.findByEmail(email).orElse(null);
    if (user == null) {
      throw new BadRequestException("존재하지 않은 계정입니다.");
    }
    user.setPassword(pwdEncConfig.getPasswordEncoder().encode(newPassword));
    user.getUserDetailsMeta().setUpdatedPasswordDateTime(LocalDateTime.now());
  }

  @Override
  @Transactional
  public void resetPasswordByMobile(String newPassword, String mobile) {
    User user = userRepository.findFirstByMobile(mobile);
    if (user == null) {
      throw new BadRequestException("존재하지 않은 계정입니다.");
    }
    user.setPassword(pwdEncConfig.getPasswordEncoder().encode(newPassword));
    user.getUserDetailsMeta().setUpdatedPasswordDateTime(LocalDateTime.now());
  }

  @Override
  @Transactional
  public void changeUser(String email, UpdateUserReqBody updateUserReqBody) {
    userRepository.findByEmail(email)
        .ifPresent(user -> {
          user.setFullName(updateUserReqBody.getFullName());

          user.getTermsAgree().setSmsRcv(updateUserReqBody.isSmsRcv());
          user.getTermsAgree().setEmailRcv(updateUserReqBody.isEmailRcv());
//          user.getTermsAgree().setKakaoRcv(updateUserReqBody.isKakaoRcv());
        });
  }

  @Override
  @Transactional
  public void changeFullName(String fullName, String email) {
    userRepository.findByEmail(email)
        .ifPresent(user -> {
          user.setFullName(fullName);
        });
  }

  @Override
  @Transactional
  public void changeMobile(CertConfirmReqBody confirm, String email) {

    AuthentiCodeResBody result = authenticodeService.confirmByMobile(confirm.getCode(), confirm.getMobile());

    // 성공
    if (Objects.equals(result.getResultCode(), Authenticode.RESULT_SUCCESS)) {

      userRepository.findByEmail(email)
          .ifPresent(user -> {
            user.setMobile(confirm.getMobile());
            user.getVerification().setMobile(true);
          });
    }
    // 실패
    else if (Objects.equals(result.getResultCode(), Authenticode.RESULT_EXPIRE)) {
      throw new BadRequestException(ResponseErrorCode.EXPIRED_VERIFICATION_CODE.getMessage());
    } else {
      throw new BadRequestException(ResponseErrorCode.INVALID_VERIFICATION_CODE.getMessage());
    }
  }

  @Override
  @Transactional
  public void changeEmail(CertReqBody cert, String email, HttpServletRequest request) {

    User _user = userRepository.findByEmail(cert.getEmail()).orElse(null);
    if (_user != null && StringUtils.isNotEmpty(_user.getEmail())) {
      throw new BadRequestException(ResponseErrorCode.ALREADY_EXIST_EMAIL.getMessage());
    }

    userRepository.findByEmail(email)
        .ifPresent(user -> {
          user.setEmail(cert.getEmail());
          user.getVerification().setEmail(false);
        });
  }

  @Override
  @Transactional
  public boolean matchPassword(User user, String password) {

    if (StringUtils.isEmpty(user.getPassword())) {
      throw new BadRequestException("비밀번호가 존재하지 않습니다.");
    }

    return pwdEncConfig.getPasswordEncoder().matches(password, user.getPassword());
  }

  @Override
  @Transactional
  public boolean matchPassword(String email, String password) {
    User user = this.get(email);

    if (user == null) {
      throw new BadRequestException("존재하지 않은 사용자입니다.");
    }

    return this.matchPassword(user, password);
  }

  @Override
  @Transactional
  public FileMeta updateProfileImage(MultipartFile file, String email) {

    return userRepository.findByEmail(email)
        .map(user -> {
          FileMeta result = fileUploadService.uploadImage(file);
          user.setImage(result.getUrl() != null ? result.getUrl().toString() : null);
          return result;
        }).orElseThrow(BadRequestException::new);
  }

  @Override
  @Transactional
  public void leave(Long id, String reason) {
    userRepository.findById(id)
        .ifPresent(user -> {
          user.getUserDetailsMeta().setEnabled(false);
          user.getLeaveMeta().setLeave(true);
          user.getLeaveMeta().setLeaveTime(LocalDateTime.now());
          user.getLeaveMeta().setLeaveReason(reason);

          user.setSocialId(null); // SNS 연동끊기
        });

  }

  @Override
  @Transactional
  public void removePrivacy(Long id) {
    userRepository.findById(id)
        .ifPresent(user -> {
          if (!user.getUserDetailsMeta().isEnabled()
              && user.getLeaveMeta().isLeave()) {

            String disableKeyword = "leave" + id;
            user.setEmail(disableKeyword);
            user.setMobile(disableKeyword);
            user.setFullName(disableKeyword);
            user.setImage(null);
            user.setPassword(null);
//            user.setBirth(null);
            user.setSocialId(null);
            user.setAuthorities(null);
            user.getUserDetailsMeta().setEnabled(false);
            user.getLeaveMeta().setLeave(true);
            user.getLeaveMeta().setRemovePrivacyTime(LocalDateTime.now());
          }
        });
  }

  @Override
  @Transactional
  public void findAccountByMobile(String mobile, HttpServletRequest request) {

    List<User> users = userRepository.findByMobileAndUserDetailsMetaEnabledTrue(mobile);

    if (users.size() > 0) {
      CertReqBody certReqBody = new CertReqBody();
      certReqBody.setMobile(mobile);
      certReqBody.setMode(CertReqBody.Mode.FIND_ACCOUNT_MOBILE);
      String code = certService.cert(certReqBody, request);
      log.debug("# find account mobile ::: {}", code);
    } else {
      throw new BadRequestException(ResponseErrorCode.ENTERED_INVALID.getMessage());
    }
  }

  @Override
  public void findPasswordByMobile(String mobile, String email, String fullName, HttpServletRequest request) {
    List<User> users = userRepository.findByMobileAndFullNameAndEmailAndUserDetailsMetaEnabledTrue(mobile, fullName, email);

    if (users.size() > 0) {
      CertReqBody certReqBody = new CertReqBody();
      certReqBody.setMobile(mobile);
      certReqBody.setMode(CertReqBody.Mode.FIND_ACCOUNT_MOBILE);
      String code = certService.cert(certReqBody, request);
      log.debug("# find account mobile ::: {}", code);
    } else {
      throw new BadRequestException(ResponseErrorCode.ENTERED_INVALID.getMessage());
    }
  }

  @Override
  @Transactional
  public List<FindUserResBody> findAccountByMobileConfirm(FindAccountReqBody findAccountReqBody) {

    CertConfirmReqBody confirm = new CertConfirmReqBody();
    confirm.setMode(CertReqBody.Mode.FIND_ACCOUNT_MOBILE);
    confirm.setCode(findAccountReqBody.getCode());
    confirm.setMobile(findAccountReqBody.getMobile());
    AuthentiCodeResBody result = certService.confirm(confirm);

    // 성공
    if (Objects.equals(result.getResultCode(), Authenticode.RESULT_SUCCESS)) {

      List<User> users = userRepository.findByMobileAndUserDetailsMetaEnabledTrue(result.getMobile());

      return users.stream().map(user -> {
        FindUserResBody findUserResBody = new FindUserResBody();
        findUserResBody.setEmail(user.getEmail());
        findUserResBody.setSignUpDate(user.getCreatedDate());
        return findUserResBody;
      }).collect(toList());
    }

    // 실패
    if (Objects.equals(result.getResultCode(), Authenticode.RESULT_EXPIRE)) {
      throw new BadRequestException(ResponseErrorCode.EXPIRED_VERIFICATION_CODE.getMessage());
    } else {
      throw new BadRequestException(ResponseErrorCode.NOT_CERTIFIED.getMessage());
    }
  }

  @Override
  public ResponseEntity<?> findPasswordByMobileConfirm(FindPasswordReqBody findPasswordReqBody) {

    CertConfirmReqBody confirm = new CertConfirmReqBody();
    confirm.setMode(CertReqBody.Mode.FIND_ACCOUNT_MOBILE);
    confirm.setCode(findPasswordReqBody.getCode());
    confirm.setMobile(findPasswordReqBody.getMobile());
    AuthentiCodeResBody result = certService.confirm(confirm);

    if (Objects.equals(result.getResultCode(), Authenticode.RESULT_SUCCESS)) {

      return ResponseEntity.ok().body("인증번호가 일치합니다.");
    }

    if (Objects.equals(result.getResultCode(), Authenticode.RESULT_EXPIRE)) {
      throw new BadRequestException(ResponseErrorCode.EXPIRED_VERIFICATION_CODE.getMessage());
    } else {
      throw new BadRequestException(ResponseErrorCode.NOT_CERTIFIED.getMessage());
    }
  }

  @Override
  @Transactional(readOnly = true)
  public ResponseEntity<AccessTokenResBody> authAccessToken(LoginReqBody reqBody, HttpServletRequest request) {

    log.debug("> restLoginReqBody : {}", reqBody);

    User user = userRepository.findByEmail(reqBody.getEmail()).orElse(null);
//    DormancyUser dormancyUser = dormancyUserRepository.findByEmail(restLoginReqBody.getEmail());
    // 계정이 존재하는지 체크 (휴면테이블도 확인)
    if (user == null
//        && dormancyUser == null
    ) {
      throw new BadRequestException("존재하지 않는 계정입니다.");
    }

    log.debug("> reqBody.getPassword() : {}", reqBody.getPassword());
    log.debug("> user.getPassword() : {}", user.getPassword());
    log.debug("> pwdEncConfig.getPasswordEncoder().matches(restLoginReqBody.getPassword(), user.getPassword()) : {}", pwdEncConfig.getPasswordEncoder().matches(reqBody.getPassword(), user.getPassword()));
    // 휴면이 아닐 경우
    if (user != null
        && !pwdEncConfig.getPasswordEncoder().matches(reqBody.getPassword().trim(), user.getPassword().trim())
    ) {
      throw new UnAuthorizedException("아이디 혹은 비밀번호가 일치하지 않습니다.");
    }

    // 휴면일 경우
//    if(dormancyUser != null && !pwdEncConfig.getPasswordEncoder().matches(restLoginReqBody.getPassword(), dormancyUser.getPassword())){
//      throw new UnAuthorizedException("아이디 혹은 비밀번호가 일치하지 않습니다.");
//    }
//    else if(dormancyUser != null && pwdEncConfig.getPasswordEncoder().matches(restLoginReqBody.getPassword(), dormancyUser.getPassword())){
//      throw new BadRequestException(ResponseErrorCode.DORMANCY_USER.getMessage());
//    }

    // 탈퇴한 계정인지 체크
    this.isLeaved(user);
    //

    if (!user.getRoleTopLevel().equals(Authority.Role.SUPER)
        && !user.getRoleTopLevel().equals(Authority.Role.ADMIN)
    ) {
//      throw new UnAuthorizedException("관리자에 의해 중지된 계정입니다.");
    }

    user.getDormancyMeta().setLastLoginDate(LocalDateTime.now());
    userRepository.save(user);

    ResponseEntity<AccessTokenResBody> responseEntity = null;
    URI uri;

    try {

      final String password = reqBody.getPassword();

      uri = UriComponentsBuilder.fromHttpUrl(String.format("%s/oauth/token", "http://127.0.0.1:" + port))
          .queryParam("grant_type", "password")
          .queryParam("username", reqBody.getEmail())
          .queryParam("password", password)
          .build()
          .encode()
          .toUri()
      ;

      String target = String.format("%s:%s", clientId, secretKey);
      byte[] targetBytes = new byte[0];
      targetBytes = target.getBytes(StandardCharsets.UTF_8);
      Base64.Encoder encoder = Base64.getEncoder();
      byte[] encodedBytes = encoder.encode(targetBytes);

      Map<String, String> headerMap = new HashMap<>();
      headerMap.put("Content-Type", "application/json");
      headerMap.put("Authorization", String.format("Basic %s", new String(encodedBytes)));
      Headers headers = Headers.of(headerMap);

      try {

        responseEntity = restAPIService.post(uri, headers, RequestBody.create(RestAPIServiceImpl.APPLICATION_JSON, ""), AccessTokenResBody.class);
      } catch (Exception e) {
        log.error("> Sign in ERROR", e);
        throw new UnAuthorizedException(e.getLocalizedMessage());
      }
      if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
        log.debug("body : {}", responseEntity.getBody());
      } else {
        throw new UnAuthorizedException("Sign in Error : Http Status : " + responseEntity.getStatusCode());
      }

    } catch (Exception e) {
      log.error("login error", e);
      e.printStackTrace();
    }
    return responseEntity;

//    return ResponseEntity.ok(
//        ImmutableMap
//            .builder()
//            .put("access_token", this.getJWT(user))
//            .build()
//    );
  }

  @Override
  @Transactional
  public ResponseEntity<AccessTokenResBody> authAccessTokenMobile(String mobile) {

    User user = userRepository.findFirstByMobile(mobile);

    user.getDormancyMeta().setLastLoginDate(LocalDateTime.now());
    userRepository.save(user);

    return ResponseEntity.ok(
        AccessTokenResBody.builder()
            .access_token(this.getJWT(user))
            .build()
    );
  }

  @Override
  @Transactional(readOnly = true)
  public ResponseEntity<AccessTokenResBody> getAccessToken(LoginReqBody restLoginReqBody) {

    User user = userRepository.findByEmail(restLoginReqBody.getEmail()).orElse(null);

    // 계정이 존재하는지 체크
    if (user == null) {
      throw new BadRequestException(ResponseErrorCode.ENTERED_INVALID.getMessage());
    }
    // 탈퇴한 계정인지 체크
    this.isLeaved(user);
    // 비밀번호가 일치하는지 체크
    if (!this.matchPassword(user, restLoginReqBody.getPassword())) {
      throw new BadRequestException(ResponseErrorCode.ENTERED_INVALID.getMessage());
    }

    return ResponseEntity.ok(
        AccessTokenResBody.builder()
            .access_token(this.getJWT(user))
            .build()
    );
  }

  @Override
  @Transactional
  public String findPasswordByEmail(String email, HttpServletRequest request) {

    if (userRepository.existsByEmail(email)) {
      CertReqBody certReqBody = new CertReqBody();
      certReqBody.setEmail(email);
      certReqBody.setMode(CertReqBody.Mode.FIND_PASSWORD_EMAIL);
      String code = certService.cert(certReqBody, request);
      log.debug("# find password email ::: {}", code);
      return code;
    } else {
      throw new BadRequestException(ResponseErrorCode.NOT_EXIST_EMAIL.getMessage());
    }
  }

  @Override
  @Transactional(readOnly = true)
  public boolean isExpiredPassword(User user) {
    LocalDateTime upd = user.getUserDetailsMeta().getUpdatedPasswordDateTime();
    return DateUtils.after(LocalDateTime.now(), upd.plusDays(privacyExpireTime.getPassword()));
  }

  @Override
  @Transactional
  public TermsAgree subscribeByEmail(boolean subscribe, String email) {
    User user = userRepository.findByEmail(email).orElse(null);

    if (user == null) {
      throw new UnAuthorizedException();
    }

    user.getTermsAgree().setEmailRcv(subscribe);
    user.getTermsAgree().setEmailRcvDate(LocalDateTime.now());
    return user.getTermsAgree();
  }

  @Override
  @Transactional
  public TermsAgree subscribeBySMS(boolean subscribe, String email) {
    User user = userRepository.findByEmail(email).orElse(null);

    if (user == null) {
      throw new UnAuthorizedException();
    }

    user.getTermsAgree().setSmsRcv(subscribe);
    user.getTermsAgree().setSmsRcvDate(LocalDateTime.now());
    return user.getTermsAgree();
  }

  @Override
  @Transactional
  public TermsAgree subscribeByKakao(boolean subscribe, String email) {
    User user = userRepository.findByEmail(email).orElse(null);

    if (user == null) {
      throw new UnAuthorizedException();
    }

    user.getTermsAgree().setKakaoRcv(subscribe);
    user.getTermsAgree().setKakaoRcvDate(LocalDateTime.now());
    return user.getTermsAgree();
  }

  @Override
  @Transactional
  public TermsAgree subscribeByPush(boolean subscribe, String email) {
    User user = userRepository.findByEmail(email).orElse(null);

    if (user == null) {
      throw new UnAuthorizedException();
    }

    user.getTermsAgree().setPushRcv(subscribe);
    user.getTermsAgree().setPushRcvDate(LocalDateTime.now());
    return user.getTermsAgree();
  }

  @Override
  @Transactional
  public List<User> userByAdmin() {
    List<User> users = Lists.newArrayList(userRepository.findAll(
        UserPredicate.getInstance()
            .role(Authority.Role.ADMIN)
            .role(Authority.Role.SUPER)
            .values()));
    users.removeIf(user -> Objects.equals(user.getEmail(), "master@galaxia.co.kr"));
    return users;
  }

  private void isLeaved(User user) {
    if (user != null) {
      if (!user.getUserDetailsMeta().isEnabled()
          || user.getLeaveMeta().isLeave()) {
        throw new BadRequestException(ResponseErrorCode.LEAVED_USER.getMessage());
      }
    }
  }

  private void isDormant(User user) {
    if (user != null) {
      if (!user.getUserDetailsMeta().isEnabled()
          || user.getDormancyMeta().isDormancy()) {
        throw new BadRequestException(ResponseErrorCode.DORMANCY_USER.getMessage());
      }
    }
  }


  public static User setUserAuthorities(AuthorityRepository authorityRepository, User user) {

    Authority.Role role = user.getRole();
    if (role != null) {
      Set<Authority> authorities = new HashSet<>();
//      switch (role) {
//        case SUPER:
//          authorities.add(authorityRepository.findByRole(Authority.Role.SUPER));
//        case ADMIN:
//          authorities.add(authorityRepository.findByRole(Authority.Role.ADMIN));
//        case OWNER:
//          authorities.add(authorityRepository.findByRole(Authority.Role.OWNER));
//        case USER:
//          authorities.add(authorityRepository.findByRole(Authority.Role.USER));
//      }
      authorities.add(authorityRepository.findByRole(role));
      user.setAuthorities(authorities);
    }
    return user;
  }

  @Override
  @Transactional
  public void dormantByMobile(String mobile, HttpServletRequest request, DormantReqBody reqBody) {
//    List<User> users = userRepository.findByMobileAndFullNameAndUserDetailsMetaEnabledTrue(mobile, reqBody.getFullName());
//
//
//    if (users.size() > 0) {
//
//      users.forEach(user -> {
//        if (!user.getRelativeBusinessUser().getName().equals(reqBody.getCompanyName())
//
//            || !user.getDormancyMeta().isDormancy()
//        ) {
//          throw new BadRequestException(ResponseErrorCode.ENTERED_INVALID.getMessage());
//        }
//      });
//
//      CertReqBody certReqBody = new CertReqBody();
//      certReqBody.setMobile(mobile);
//      certReqBody.setMode(CertReqBody.Mode.FIND_ACCOUNT_MOBILE);
//      String code = certService.cert(certReqBody, request);
//      log.debug("# dormant mobile ::: {}", code);
//    } else {
//      throw new BadRequestException(ResponseErrorCode.ENTERED_INVALID.getMessage());
//    }
  }

  @Override
  @Transactional
  public void dormantByMobileConfirm(DormantReqBody dormantReqBody) {

//    List<User> users = userRepository.findByMobileAndFullNameAndUserDetailsMetaEnabledTrue(dormantReqBody.getMobile(), dormantReqBody.getFullName());
//
//
//    if (users.size() > 0) {
//
//      users.forEach(user -> {
//        if (!user.getRelativeBusinessUser().getName().equals(dormantReqBody.getCompanyName())
//
//            || !user.getDormancyMeta().isDormancy()
//        ) {
//          throw new BadRequestException(ResponseErrorCode.ENTERED_INVALID.getMessage());
//        } else {
//          CertConfirmReqBody confirm = new CertConfirmReqBody();
//          confirm.setMode(CertReqBody.Mode.FIND_ACCOUNT_MOBILE);
//          confirm.setCode(dormantReqBody.getCode());
//          confirm.setMobile(dormantReqBody.getMobile());
//          AuthentiCodeResBody result = certService.confirm(confirm);
//
//          // 성공
//          if (Objects.equals(result.getResultCode(), Authenticode.RESULT_SUCCESS)) {
//            //휴면정보 갱신, 데이터 원복
//            user.setDormancyMeta(new DormancyMeta());
//            DormancyUser dormancyUser = dormancyUserRepository.findById(user.getId()).get();
//            user.setMobile(dormancyUser.getMobile());
//            user.setFullName(dormancyUser.getFullName());
//            user.setEmail(dormancyUser.getEmail());
//            user.setPassword(dormancyUser.getPassword());
//            dormancyUserRepository.deleteById(dormancyUser.getId());
//            return;
//          }
//
//          // 실패
//          if (Objects.equals(result.getResultCode(), Authenticode.RESULT_EXPIRE)) {
//            throw new BadRequestException(ResponseErrorCode.EXPIRED_VERIFICATION_CODE.getMessage());
//          } else {
//            throw new BadRequestException(ResponseErrorCode.NOT_CERTIFIED.getMessage());
//          }
//        }
//      });
//    }
  }

  @Override
  @Transactional
  public void setDormant() {
    // TODO 휴면 계정 처리
    List<UserInfoResBody> list = new ArrayList<>();

    list.forEach(userInfoResBody -> {
      User user = this.get(userInfoResBody.getId());
      //365일 이상 로그인하지 않았을 시 휴면테이블로 정보 이동(이메일, 비밀번호, 전화번호, 이름)
      if (!user.getDormancyMeta().isDormancy()
          && DateUtils.getNumberOfDays(user.getDormancyMeta().getLastLoginDate(), DateUtils.getLocalDateTime()) > 365) {
        DormancyMeta dormancyMeta = new DormancyMeta();
        dormancyMeta.setDormancy(true);
        dormancyMeta.setDormancyTime(LocalDateTime.now());
        user.setDormancyMeta(dormancyMeta);
        user.setEmail("dormant");
        user.setFullName("dormant");
        String password = user.getPassword();
        user.setPassword("dormant");
        user.setMobile("");
//        dormancyUserRepository.save(new DormancyUser(user.getId(), userInfoResBody.getEmail(), password, userInfoResBody.getName(), userInfoResBody.getMobile()));
      }
    });
  }

  @Override
  @Transactional
  public void updateUserInfo(String email, UpdateUserInfoReqBody updateUserInfoReqBody) {
    userRepository.findByEmail(email)
        .ifPresent(user -> {
          if (StringUtils.isNotEmpty(updateUserInfoReqBody.getFullName())) {
            user.setFullName(updateUserInfoReqBody.getFullName());
          }
          if (StringUtils.isNotEmpty(updateUserInfoReqBody.getImage())) {
            user.setImage(updateUserInfoReqBody.getImage());
          }

          user.getTermsAgree().setEmailRcv(updateUserInfoReqBody.isEmailRcv());
          if (user.getTermsAgree().isEmailRcv()) {
            user.getTermsAgree().setEmailRcvDate(LocalDateTime.now());
          } else {
            user.getTermsAgree().setEmailRcvDate(null);
          }

          user.getTermsAgree().setKakaoRcv(updateUserInfoReqBody.isKakaoRcv());
          if (user.getTermsAgree().isKakaoRcv()) {
            user.getTermsAgree().setKakaoRcvDate(LocalDateTime.now());
          } else {
            user.getTermsAgree().setKakaoRcvDate(null);
          }

          user.getTermsAgree().setSmsRcv(updateUserInfoReqBody.isSmsRcv());
          if (user.getTermsAgree().isSmsRcv()) {
            user.getTermsAgree().setSmsRcvDate(LocalDateTime.now());
          } else {
            user.getTermsAgree().setSmsRcvDate(null);
          }

          user.getTermsAgree().setPushRcv(updateUserInfoReqBody.isPushRcv());
          if (user.getTermsAgree().isPushRcv()) {
            user.getTermsAgree().setPushRcvDate(LocalDateTime.now());
          } else {
            user.getTermsAgree().setPushRcvDate(null);
          }
        });
  }

  //    @Override
//    @Transactional
//    public void changeUserSetting(String email) {
//        User user = userRepository.findByEmail(email).orElse(null);
//        TermsAgree termsAgree = user.getTermsAgree();
//        user.setTermsAgree(termsAgree);
//        userRepository.save(user);
//    }
//
//    @Override
//    @Transactional
//    public void sendCodeEmail(String email) {
//        User user = this.get(email);
//
//        String subject = "이메일 인증";
//        String code = RandomStringUtils.randomNumeric(6);
//        EmailHistory emailHistory = new EmailHistory();
//        emailHistory.setCode(code);
//        emailHistory.setType(EmailHistory.Type.SEND_CODE);
//        emailHistory.setSendAddress(email);
//        emailHistory.setSendTime(LocalDateTime.now());
//        emailHistory.setSubject(subject);
//        emailHistory.setRelativeUser(user);
//        emailHistoryRepository.save(emailHistory);
//
//        Map<String, Object> model = new HashMap<>();
//        model.put("subject", subject);
//        model.put("fullName", user.getFullName());
//        model.put("nowDate", LocalDate.now());
//        model.put("email", email);
//        model.put("code", code);
//
//        try {
//            emailService.send(email, subject, model, "email/send-code-email.ftl");
//        } catch (Exception e) {
//            e.printStackTrace();
//            log.debug("user api", e);
//        }
//    }
//
//    @Override
//    @Transactional
//    public boolean checkCodeEmail(VerifyReqBody verifyReqBody) {
//        String email = verifyReqBody.getEmail();
//        User user = this.get(email);
//        List<EmailHistory> listEmailHistory = emailHistoryRepository.findAllByTypeAndSendAddressAndRelativeUserOrderBySendTimeDesc(EmailHistory.Type.SEND_CODE, email, user);
//        if (listEmailHistory.size() > 0) {
//            if (listEmailHistory.get(0).getCode().equals(verifyReqBody.getCode())) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    @Override
//    @Transactional
//    public void sendCodeSMS(String mobile) {
//
//        String code = RandomStringUtils.randomNumeric(6);
//
//        SMSHistory smsHistory = new SMSHistory();
//        smsHistory.setCodeSMS(code);
//        smsHistory.setTypeSMS(SMSHistory.TypeSMS.FORGOT_EMAIL);
//        smsHistory.setSendTime(LocalDateTime.now());
//        smsHistory.setDestPhone(mobile);
//        smsHistoryRepository.save(smsHistory);
//
//        try {
//            smsService.send(new SMS(mobile, String.format("The verification code forgot your password is: [%s]", code)));
//        } catch (Exception e) {
//            log.error("send code sms", e);
//        }
//    }
//
//    @Override
//    @Transactional
//    public String checkCodeMobile(VerifyReqBody verifyReqBody) {
//        String mobile = verifyReqBody.getPhone();
//        User user = userRepository.findFirstByMobile(mobile);
//
//        if (user == null) {
//            throw new NotFoundException("User does not exist!");
//        }
//
//        List<SMSHistory> smsHistoryList = smsHistoryRepository.findAllByTypeSMSAndDestPhoneOrderBySendTimeDesc(SMSHistory.TypeSMS.FORGOT_EMAIL, mobile);
//        if (smsHistoryList.size() > 0) {
//            if (smsHistoryList.get(0).getCodeSMS().equals(verifyReqBody.getCode())) {
//                return user.getEmail();
//            }
//        }
//
//        throw new NotFoundException("Code is wrong!");
//    }
//
//    @Override
//    @Transactional
//    public void sendCodeRegister(String mobile) {
//        String code = RandomStringUtils.randomNumeric(6);
//        SMSHistory smsHistory = new SMSHistory();
//        smsHistory.setCodeSMS(code);
//        smsHistory.setTypeSMS(SMSHistory.TypeSMS.CODE_REGISTER);
//        smsHistory.setSendTime(LocalDateTime.now());
//        smsHistory.setDestPhone(mobile);
//        smsHistory.setVerifySMS(false);
//        smsHistoryRepository.save(smsHistory);
//
//        try {
//            smsService.send(new SMS(mobile, String.format("Your registration verification code is: [%s]", code)));
//        } catch (Exception e) {
//            log.error("send code sms", e);
//        }
//    }
//
//    @Override
//    @Transactional
//    public boolean checkCodeRegister(VerifyReqBody verifyReqBody) {
//        String mobile = verifyReqBody.getPhone();
//        List<SMSHistory> smsHistoryList = smsHistoryRepository.findAllByTypeSMSAndDestPhoneAndVerifySMSOrderBySendTimeDesc(SMSHistory.TypeSMS.CODE_REGISTER, mobile, false);
//        if (smsHistoryList.size() > 0) {
//            if (smsHistoryList.get(0).getCodeSMS().equals(verifyReqBody.getCode())) {
//                SMSHistory smsHistory = smsHistoryList.get(0);
//                smsHistory.setVerifySMS(true);
//                smsHistoryRepository.save(smsHistory);
//                return true;
//            }
//        }
//        return false;
//    }
//
//    @Override
//    @Transactional
//    public void sendCodeChangeMobile(String mobile) {
//        String code = RandomStringUtils.randomNumeric(6);
//        SMSHistory smsHistory = new SMSHistory();
//        smsHistory.setCodeSMS(code);
//        smsHistory.setTypeSMS(SMSHistory.TypeSMS.CODE_CHANGE_MOBILE);
//        smsHistory.setSendTime(LocalDateTime.now());
//        smsHistory.setDestPhone(mobile);
//        smsHistory.setVerifySMS(false);
//        smsHistoryRepository.save(smsHistory);
//
//        try {
//            smsService.send(new SMS(mobile, String.format("The verification code for changing your phone number is: [%s]", code)));
//        } catch (Exception e) {
//            log.error("send code sms", e);
//        }
//    }
//
//    @Override
//    @Transactional
//    public boolean verifyCodeChangeMobile(VerifyReqBody verifyReqBody, String email) {
//        String mobile = verifyReqBody.getPhone();
//        User user = this.get(email);
//        if (user != null) {
//            List<SMSHistory> smsHistoryList = smsHistoryRepository.findAllByTypeSMSAndDestPhoneAndVerifySMSOrderBySendTimeDesc(SMSHistory.TypeSMS.CODE_CHANGE_MOBILE, mobile, false);
//            if (smsHistoryList.size() > 0) {
//                if (smsHistoryList.get(0).getCodeSMS().equals(verifyReqBody.getCode())) {
//                    SMSHistory smsHistory = smsHistoryList.get(0);
//                    smsHistory.setVerifySMS(true);
//                    smsHistoryRepository.save(smsHistory);
//
//                    //change mobile
//                    user.setMobile(mobile);
//                    userRepository.save(user);
//
//                    return true;
//                }
//            }
//        }
//        return false;
//    }
//
//    @Override
//    @Transactional
//    public void sendCodeForgotPassword(String mobile) {
//        String code = RandomStringUtils.randomNumeric(6);
//        SMSHistory smsHistory = new SMSHistory();
//        smsHistory.setCodeSMS(code);
//        smsHistory.setTypeSMS(SMSHistory.TypeSMS.FORGOT_PASSWORD);
//        smsHistory.setSendTime(LocalDateTime.now());
//        smsHistory.setDestPhone(mobile);
//        smsHistory.setVerifySMS(false);
//        smsHistoryRepository.save(smsHistory);
//
//        try {
//            smsService.send(new SMS(mobile, String.format("The verification code forgot password your phone number is: [%s]", code)));
//        } catch (Exception e) {
//            log.error("send code sms", e);
//        }
//    }
//
//    @Override
//    @Transactional
//    public boolean verifyCodeForgotPassword(VerifyReqBody verifyReqBody) {
//        String mobile = verifyReqBody.getPhone();
//        List<SMSHistory> smsHistoryList = smsHistoryRepository.findAllByTypeSMSAndDestPhoneAndVerifySMSOrderBySendTimeDesc(SMSHistory.TypeSMS.FORGOT_PASSWORD, mobile, false);
//        if (smsHistoryList.size() > 0) {
//            if (smsHistoryList.get(0).getCodeSMS().equals(verifyReqBody.getCode())) {
//                SMSHistory smsHistory = smsHistoryList.get(0);
//                smsHistory.setVerifySMS(true);
//                smsHistoryRepository.save(smsHistory);
//
//                return true;
//            }
//        }
//        return false;
//    }
//
  @Override
  @Transactional
  public boolean checkAccount(String email, String mobile) {
    if (!userRepository.existsByEmail(email)) {
      throw new NotFoundException("User not exits!");
    }
    User user = this.get(email);
    if (!user.getMobile().equals(mobile)) {
      throw new NotFoundException("Email and phone numbers do not match!");
    }
    return true;
  }
}
