package com.logisall.anyware.api.service.board.qna;

import com.logisall.anyware.core.domain.board.qna.category.QnaCategory;
import com.logisall.anyware.core.domain.board.qna.category.QnaCategoryPredicate;
import com.logisall.anyware.core.domain.board.qna.category.QnaCategoryRepository;
import com.logisall.anyware.core.model.resbody.board.CategoryResBody;
import com.logisall.anyware.core.service.setting.AppSettingService;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;


@Slf4j
@Service
public class QnaCategoryAPIServiceImpl implements QnaCategoryAPIService {

    @Autowired
    private QnaCategoryRepository qnaCategoryRepository;

    @Autowired
    private AppSettingService appSettingService;

    @Override
    @Transactional(readOnly = true)
    public CollectionModel<EntityModel<CategoryResBody>> categories(Locale locale) {

        Locale defaultLocale = appSettingService.getDefaultLocale();

        List<QnaCategory> categories = Lists.newArrayList(qnaCategoryRepository.findAll(

                QnaCategoryPredicate.getInstance()
                        .active(true)
                        .locale(locale, defaultLocale)
                        .values(),
                Sort.by(Sort.Direction.ASC, "orderAscending")));

        return CollectionModel.wrap(categories.stream().map(category -> {
            category.lazy();
            return category.toBody(locale);
        }).collect(Collectors.toList()));
    }
}
