package com.logisall.anyware.api.web.account.sns;

import com.logisall.anyware.api.config.oauth.JwtUtils;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.UnAuthorizedException;
import com.logisall.anyware.core.config.property.sns.AppleApiKey;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.domain.user.sns.SNSStatus;
import com.logisall.anyware.core.model.resbody.account.SNSResBody;
import com.logisall.anyware.core.model.sns.Apple;
import com.logisall.anyware.core.model.sns.SNSReqBody;
import com.logisall.anyware.core.model.sns.SNSType;
import com.logisall.anyware.core.model.sns.apple.Payload;
import com.logisall.anyware.core.model.sns.apple.TokenResponse;
import com.logisall.anyware.core.service.account.SNSAppleAPIService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Slf4j
@Tag(name = "Apple Login", description = "애플 로그인 API")
@RestController
@RequestMapping("/api/v1/apple-login")
public class APIAppleLoginController {

  @Autowired
  private SNSAppleAPIService snsAppleAPIService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private JwtUtils jwtUtils;

  @Autowired
  private AppleApiKey apiKey;

  @GetMapping
  public ModelAndView login(Model model) {

    URI uri = UriComponentsBuilder.fromHttpUrl("https://appleid.apple.com/auth/authorize")
        .queryParam("client_id", apiKey.getClientId())
        .queryParam("redirect_uri", apiKey.getReturnUri())
        .queryParam("nonce", "UFLOW_APPLE_0001")
        .queryParam("response_type", "code id_token")
        .queryParam("scope", "name email")
        .queryParam("response_mode", "form_post")
        .build().toUri();

    return new ModelAndView("redirect:" + uri.toString());
  }

  @PostMapping
  public ResponseEntity<?> apple(@RequestBody SNSReqBody requestBody) {

    if (requestBody == null) {
      throw new BadRequestException("Body can not null");
    }

    String code = requestBody.getCode();
    String redirectUri = apiKey.getReturnUri();

    log.debug("===apple");
    log.debug("code ::: {}", code);
    log.debug("redirectUri ::: {}", redirectUri);
    log.debug("id_token ::: {}", requestBody.getIdToken());

    TokenResponse result = snsAppleAPIService.getAccessToken(code, requestBody.getIdToken(), redirectUri);

    log.debug("===apple result");
    log.debug("accessToken ::: {}", result.getAccess_token());
    log.debug("tokenType ::: {}", result.getToken_type());
    log.debug("expiresIn ::: {}", result.getExpires_in());
    log.debug("idToken ::: {}", result.getId_token());
    log.debug("refreshToken ::: {}", result.getRefresh_token());

    Payload payload = snsAppleAPIService.getPayload(result.getId_token());
    log.debug("payload ::: {}", payload);
    Apple apple = Apple.of(payload.getSub(), payload.getEmail());
    return appleAuth(apple);
  }

  @PostMapping("native")
  public ResponseEntity<?> appleNative(@RequestBody Apple apple) {
    return this.appleAuth(apple);
  }

  private ResponseEntity<?> appleAuth(Apple apple) {

    SNSStatus status = snsAppleAPIService.getStatus(apple);
    SNSResBody body = SNSResBody.of(true);

    log.debug("apple ::: {}", apple);
    log.debug("status ::: {} ", status);

    switch (status) {

      // AccessToken 발급
      case CONNECT:
      case LINKED:

        User user = userRepository.findBySocialIdAppleId(apple.getId()).orElseThrow(RuntimeException::new);
        if (user.isWithd()) {
          throw new UnAuthorizedException("이용할수 없는 계정입니다.");
        }
        body.setAccess_token(jwtUtils.generator(user));
        body.setSnsType(SNSType.APPLE);
        body.setId(apple.getId());
        body.setName(apple.getEmail());
        body.setEmail(apple.getEmail());
        return ResponseEntity.ok(body);

      case LEAVED_ACCOUNT:

        body = SNSResBody.of(false);
        body.setMessage(SNSStatus.LEAVED_ACCOUNT.name());
        body.setSnsType(SNSType.APPLE);
        body.setId(apple.getId());
        body.setName(apple.getEmail());
        body.setEmail(apple.getEmail());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);

      case NOT_MATCH_SNS:

        body = SNSResBody.of(false);
        body.setMessage(SNSStatus.NOT_MATCH_SNS.name());
        body.setSnsType(SNSType.APPLE);
        body.setId(apple.getId());
        body.setName(apple.getEmail());
        body.setEmail(apple.getEmail());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);

      case NOT_EXISTED_ACCOUNT:

        body = SNSResBody.of(false);
        body.setMessage(SNSStatus.NOT_EXISTED_ACCOUNT.name());
        body.setSnsType(SNSType.APPLE);
        body.setId(apple.getId());
        body.setName(apple.getEmail());
        body.setEmail(apple.getEmail());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);

      case NOT_PROVIDED_EMAIL:

        body = SNSResBody.of(false);
        body.setMessage(SNSStatus.NOT_PROVIDED_EMAIL.name());
        body.setSnsType(SNSType.APPLE);
        body.setId(apple.getId());
        body.setName(apple.getEmail());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);

      default:
        throw new BadRequestException("애플 로그인에서 나올수 없는 경우");
    }
  }
}
