package com.logisall.anyware.api.web.board;

import com.logisall.anyware.api.service.board.comment.CommentAPIService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.UnAuthorizedException;
import com.logisall.anyware.core.domain.board.comment.Comment;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.reqbody.board.CommentReqBody;
import com.logisall.anyware.core.model.resbody.ResultResBody;
import com.logisall.anyware.core.utils.StringUtils;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;

@Tag(name = "Comment", description = "댓글 API")
@Slf4j
@RestController
@RequestMapping("api/v1/comments")
public class APICommentController {

  @Autowired
  private CommentAPIService commentAPIService;

  @Operation(summary = "[comment-1] List comment (댓글 목록)",
      description = "List comment api"
  )
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code 언어코드", example = "ko-KR"),

      @Parameter(in = ParameterIn.QUERY, name = "size", description = "Page Size 페이지 크기 (default : 20)", example = "20"),
      @Parameter(in = ParameterIn.QUERY, name = "page", description = "Current Page 현재 페이지 (default : 0)", example = "0"),
      @Parameter(in = ParameterIn.QUERY, name = "sort", description = "Sort Page 정렬", example = "createdDate,desc"),
  })
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> page(@Parameter(hidden = true) @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                                @Parameter(description = "Search Word 검색어") @RequestParam(required = false, defaultValue = "") String query,
                                @Parameter(description = "Start Time 시작시간 (yyyy-MM-dd HH:mm:ss)") @RequestParam(required = false) String startDate,
                                @Parameter(description = "End Time 종료시간 (yyyy-MM-dd HH:mm:ss)") @RequestParam(required = false) String endDate,

                                @Parameter(description = "Type Comment 댓글 유형") @RequestParam(required = false) Comment.Type type,
                                @Parameter(description = "id user, not required 사용자") @RequestParam(required = false) Long idUser,
                                @Parameter(description = "id Content By Type 관련 게시물 ID (유형과 동일)") @RequestParam(required = false) Long idContent,
                                @Parameter(description = "id parent Comment 부모 댓글 ID") @RequestParam(required = false) Long idParent,
                                @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication,
                                Locale locale) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    return ResponseEntity.ok(commentAPIService.pagedResources(locale, filter, idUser, type, idContent, idParent));
  }

  @Operation(summary = "[comment-2] Get comment 댓글 상세",
      description = "Get comment detail by id")
  @GetMapping(value = "/{id}")
  public ResponseEntity<?> get(@Parameter(description = "Id Comment") @PathVariable Long id,
                               @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication,
                               Locale locale) {
    return ResponseEntity.ok(commentAPIService.resource(locale, id));
  }

  @Operation(summary = "[comment-3] Register comment (댓글 등록)",
      description = "create comment",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> create(@RequestBody CommentReqBody commentReqBody,
                                  @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication,
                                  Locale locale) {

    ValidUtils.isForbidden(oAuth2Authentication);

    final String email = oAuth2Authentication.getPrincipal().toString();
    if (email == null) {
      throw new UnAuthorizedException();
    }
    if (commentReqBody == null || StringUtils.isEmpty(commentReqBody.getContent())) {
      throw new BadRequestException();
    }

    Comment comment = commentAPIService.register(locale, commentReqBody, email);

    URI redirect = null;
    try {
      if (comment != null) {
        redirect = new URI("/api/v1/comments/" + comment.getId());
        return ResponseEntity.created(redirect).body(ResultResBody.of(true, "댓글이 등록되었습니다."));
      }
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
    return ResponseEntity.badRequest().build();
  }

  @Operation(summary = "[comment-4] Modify comment (댓글 수정)", description = "update comment",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> update(@PathVariable Long id,
                                  @RequestBody CommentReqBody commentReqBody,
                                  @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    ValidUtils.isForbidden(oAuth2Authentication);

    final String email = oAuth2Authentication.getPrincipal().toString();
    if (email == null) {
      throw new UnAuthorizedException();
    }

    if (commentReqBody == null || StringUtils.isEmpty(commentReqBody.getContent())) {
      throw new BadRequestException();
    }

    commentAPIService.modify(commentReqBody, id, email);
    return ResponseEntity.ok().build();
  }

  @Operation(summary = "[comment-5] Delete comment (댓글 삭제)",
      description = "Delete comment by id")
  @Parameters(value = {
      @Parameter(in = ParameterIn.QUERY, name = "Id Comment", description = "", example = "1")
  })
  @DeleteMapping(value = "/{id}")
  public ResponseEntity<?> delete(@Parameter(description = "Id Comment") @PathVariable Long id,
                                  @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    ValidUtils.isForbidden(oAuth2Authentication);

    final String email = oAuth2Authentication.getPrincipal().toString();
    if (email == null) {
      throw new UnAuthorizedException();
    }
    commentAPIService.delete(id, email);
    return ResponseEntity.ok().build();
  }

}
