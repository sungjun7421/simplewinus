package com.logisall.anyware.api.service.account.sns;

import com.logisall.anyware.core.domain.user.SocialId;
import com.logisall.anyware.core.model.reqbody.account.ChangeSnsNativeReqBody;
import com.logisall.anyware.core.model.reqbody.account.ChangeSnsReqBody;
import com.logisall.anyware.core.model.sns.*;

public interface SNSAPIService {

  Facebook connectFacebook(String code, String redirectUri);
  GooglePojo connectGoogle(String code, String redirectUri);
  Naver connectNaver(String code, String redirectUri, String state);
  Kakao connectKakao(String code, String redirectUri);
  Apple connectApple(String code, String redirectUri);

  void changeSns(ChangeSnsReqBody changeSnsReqBody, String email);
  void changeSnsNative(ChangeSnsNativeReqBody changeSnsReqBody, String email);
  void changeSns(SocialId socialId, String email);
}
