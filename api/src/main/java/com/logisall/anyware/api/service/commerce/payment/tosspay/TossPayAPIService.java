package com.logisall.anyware.api.service.commerce.payment.tosspay;

import com.logisall.anyware.core.model.payment.toss.Toss;
import com.logisall.anyware.core.model.payment.toss.TossComplete;
import com.logisall.anyware.core.model.payment.toss.TossResponse;
import com.logisall.anyware.core.model.payment.toss.TossResultCallback;

public interface TossPayAPIService {

  TossResponse payment(Toss toss, String email);

  void resultCallBack(TossResultCallback body);
  void complete(TossComplete body);
  void complete(String oid);
}
