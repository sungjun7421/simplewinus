package com.logisall.anyware.api.service.commerce.coupon;

import com.logisall.anyware.core.domain.commerce.coupon.Coupon;
import com.logisall.anyware.core.domain.commerce.coupon.CouponType;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.reqbody.commerce.coupon.CouponThumbBody;
import com.logisall.anyware.core.model.resbody.commerce.coupon.CouponResBody;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Locale;

public interface CouponAPIService {

    // CUD
    Coupon create(Coupon coupon);
    Coupon update(Coupon coupon);
    void delete(Long id);

    // R
    Coupon get(Locale locale, Long id);
    Page<Coupon> page(Locale locale, Filter filter, CouponType type);
    List<CouponResBody> list(Locale locale, CouponType type);

//    List<CouponThumbBody> listByBuyer(Long idBuyer, Locale locale);
    List<CouponThumbBody> listCouponsByUser(Long idUser, Boolean isUsed, Locale locale);
    CouponThumbBody getByCode(Locale locale, String code);

}
