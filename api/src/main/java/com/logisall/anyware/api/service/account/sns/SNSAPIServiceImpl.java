package com.logisall.anyware.api.service.account.sns;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.user.SocialId;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.model.reqbody.account.ChangeSnsNativeReqBody;
import com.logisall.anyware.core.model.reqbody.account.ChangeSnsReqBody;
import com.logisall.anyware.core.model.sns.*;
import com.logisall.anyware.core.service.account.*;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Slf4j
@Service
public class SNSAPIServiceImpl implements SNSAPIService {

  @Autowired
  private UserRepository userRepository;


  @Autowired
  private SNSFacebookAPIService facebookAPIService;

  @Autowired
  private SNSNaverAPIService naverAPIService;

  @Autowired
  private SNSGoogleAPIService googleAPIService;

  @Autowired
  private SNSAppleAPIService appleAPIService;

  @Autowired
  private SNSKakaoAPIService kakaoAPIService;

  @Override
  @Transactional
  public void changeSns(ChangeSnsReqBody changeSns, String email) {

    userRepository.findByEmail(email)
        .ifPresent(user -> {

          String code = changeSns.getCode();
          String redirectUri = changeSns.getRedirectUri();
          String state = changeSns.getState();

          switch (changeSns.getSnsType()) {
            case FACEBOOK:

              if (StringUtils.isNotEmpty(code)
                  && StringUtils.isNotEmpty(redirectUri)) {

                Facebook facebook = this.connectFacebook(code, redirectUri);

                SocialId socialId = user.getSocialId() == null ? new SocialId() : user.getSocialId();
                socialId.setFacebookId(facebook.getId());
                socialId.setFacebookName(facebook.getName());
              } else {

                SocialId socialId = user.getSocialId() == null ? new SocialId() : user.getSocialId();
                socialId.setFacebookId(null);
                socialId.setFacebookName(null);

                user.setSocialId(socialId);
              }
              break;
            case NAVER:

              if (StringUtils.isNotEmpty(code)
                  && StringUtils.isNotEmpty(redirectUri)) {

                Naver naver = this.connectNaver(code, redirectUri, state);

                SocialId socialId = user.getSocialId() == null ? new SocialId() : user.getSocialId();
                socialId.setNaverId(naver.getId());
                socialId.setNaverName(naver.getName());
              } else {

                SocialId socialId = user.getSocialId() == null ? new SocialId() : user.getSocialId();
                socialId.setNaverId(null);
                socialId.setNaverName(null);

                user.setSocialId(socialId);
              }
              break;
            case KAKAO:

              if (StringUtils.isNotEmpty(code)
                  && StringUtils.isNotEmpty(redirectUri)) {

                Kakao kakao = this.connectKakao(code, redirectUri);

                SocialId socialId = user.getSocialId() == null ? new SocialId() : user.getSocialId();
                socialId.setKakaoId(kakao.getId());
                socialId.setKakaoName(kakao.getNickName());
              } else {

                SocialId socialId = user.getSocialId() == null ? new SocialId() : user.getSocialId();
                socialId.setKakaoId(null);
                socialId.setKakaoName(null);

                user.setSocialId(socialId);
              }
              break;
            case GOOGLE:

              if (StringUtils.isNotEmpty(code)
                  && StringUtils.isNotEmpty(redirectUri)) {

                GooglePojo google = this.connectGoogle(code, redirectUri);

                SocialId socialId = user.getSocialId() == null ? new SocialId() : user.getSocialId();
                socialId.setGoogleId(google.getId());
                socialId.setGoogleName(google.getName());
              } else {

                SocialId socialId = user.getSocialId() == null ? new SocialId() : user.getSocialId();
                socialId.setGoogleId(null);
                socialId.setGoogleName(null);

                user.setSocialId(socialId);
              }
              break;
            case APPLE:
              break;
          }

        });
  }

  @Override
  @Transactional
  public void changeSnsNative(ChangeSnsNativeReqBody reqBody, String email) {

    log.debug("# changeSnsNative");

    userRepository.findByEmail(email)
        .ifPresent(user -> {
          SocialId socialId = user.getSocialId() == null ? new SocialId() : user.getSocialId();

          switch (reqBody.getSnsType()) {

            case FACEBOOK:
              if (reqBody.isActive()) {

                User _user = userRepository.findBySocialIdFacebookId(reqBody.getId()).orElse(null);
                if (_user == null || _user.getId().equals(user.getId())) {
                  socialId.setFacebookId(reqBody.getId());
                  socialId.setFacebookName(reqBody.getName());
                } else {
                  throw new BadRequestException("이미 다른계정과 연동되어있습니다.");
                }
              } else {
                socialId.setFacebookId(null);
                socialId.setFacebookName(null);
              }
              this.changeSns(socialId, email);

              break;

            case NAVER:

              if (reqBody.isActive()) {

                User _user = userRepository.findBySocialIdNaverId(reqBody.getId()).orElse(null);
                if (_user == null || _user.getId().equals(user.getId())) {
                  socialId.setNaverId(reqBody.getId());
                  socialId.setNaverName(reqBody.getName());
                } else {
                  throw new BadRequestException("이미 다른계정과 연동되어있습니다.");
                }
              } else {
                socialId.setNaverId(null);
                socialId.setNaverName(null);
              }
              this.changeSns(socialId, email);

              break;

            case KAKAO:

              if (reqBody.isActive()) {

                User _user = userRepository.findBySocialIdKakaoId(reqBody.getId()).orElse(null);
                if (_user == null || _user.getId().equals(user.getId())) {
                  socialId.setKakaoId(reqBody.getId());
                  socialId.setKakaoName(reqBody.getName());
                } else {
                  throw new BadRequestException("이미 다른계정과 연동되어있습니다.");
                }
              } else {
                socialId.setKakaoId(null);
                socialId.setKakaoName(null);
              }
              this.changeSns(socialId, email);

              break;

            case APPLE:

              if (reqBody.isActive()) {

                User _user = userRepository.findBySocialIdAppleId(reqBody.getId()).orElse(null);
                if (_user == null || _user.getId().equals(user.getId())) {
                  socialId.setAppleId(reqBody.getId());
                  socialId.setAppleName(reqBody.getName());
                } else {
                  throw new BadRequestException("이미 다른계정과 연동되어있습니다.");
                }
              } else {
                socialId.setAppleId(null);
                socialId.setAppleName(null);
              }
              this.changeSns(socialId, email);

              break;

            case GOOGLE:

              if (reqBody.isActive()) {

                User _user = userRepository.findBySocialIdGoogleId(reqBody.getId()).orElse(null);
                if (_user == null || _user.getId().equals(user.getId())) {
                  socialId.setGoogleId(reqBody.getId());
                  socialId.setGoogleName(reqBody.getName());
                } else {
                  throw new BadRequestException("이미 다른계정과 연동되어있습니다.");
                }
              } else {
                socialId.setGoogleId(null);
                socialId.setGoogleName(null);
              }
              this.changeSns(socialId, email);

              break;
          }
        });
  }

  @Override
  @Transactional
  public void changeSns(SocialId socialId, String email) {
    User _user = userRepository.findByEmail(email).orElse(null);

    log.debug("social Id ::: {}", socialId);

    // FACEBOOK
    if (StringUtils.isNotEmpty(socialId.getFacebookId())) {
      User user = userRepository.findBySocialIdFacebookId(socialId.getFacebookId()).orElse(null);

      if (user != null && !user.getEmail().equals(_user.getEmail())) {
        throw new BadRequestException("이미 다른 사용자 계정에 등록되어있습니다. 한 계정만 연동가능합니다.");
      }
      log.debug("facebook");
      _user.getSocialId().setFacebookId(socialId.getFacebookId());
      _user.getSocialId().setFacebookName(socialId.getFacebookName());
    } else {
      _user.getSocialId().setFacebookId(null);
      _user.getSocialId().setFacebookName(null);
    }

    // NAVER
    if (StringUtils.isNotEmpty(socialId.getNaverId())) {
      User user = userRepository.findBySocialIdNaverId(socialId.getNaverId()).orElse(null);

      if (user != null && !user.getEmail().equals(_user.getEmail())) {
        throw new BadRequestException("이미 다른 사용자 계정에 등록되어있습니다. 한 계정만 연동가능합니다.");
      }
      log.debug("naver");
      _user.getSocialId().setNaverId(socialId.getNaverId());
      _user.getSocialId().setNaverName(socialId.getNaverName());
    } else {
      _user.getSocialId().setNaverId(null);
      _user.getSocialId().setNaverName(null);
    }

    // KAKAO
    if (StringUtils.isNotEmpty(socialId.getKakaoId())) {
      User user = userRepository.findBySocialIdKakaoId(socialId.getKakaoId()).orElse(null);

      if (user != null && !user.getEmail().equals(_user.getEmail())) {
        throw new BadRequestException("이미 다른 사용자 계정에 등록되어있습니다. 한 계정만 연동가능합니다.");
      }
      log.debug("kakao");
      _user.getSocialId().setKakaoId(socialId.getKakaoId());
      _user.getSocialId().setKakaoName(socialId.getKakaoName());
    } else {
      _user.getSocialId().setKakaoId(null);
      _user.getSocialId().setKakaoName(null);
    }

    // GOOGLE
    if (StringUtils.isNotEmpty(socialId.getGoogleId())) {
      User user = userRepository.findBySocialIdGoogleId(socialId.getGoogleId()).orElse(null);

      if (user != null && !user.getEmail().equals(_user.getEmail())) {
        throw new BadRequestException("이미 다른 사용자 계정에 등록되어있습니다. 한 계정만 연동가능합니다.");
      }
      log.debug("google");
      _user.getSocialId().setGoogleId(socialId.getGoogleId());
      _user.getSocialId().setGoogleName(socialId.getGoogleName());
    } else {
      _user.getSocialId().setGoogleId(null);
      _user.getSocialId().setGoogleName(null);
    }

    // APPLE
    if (StringUtils.isNotEmpty(socialId.getAppleId())) {
      User user = userRepository.findBySocialIdAppleId(socialId.getAppleId()).orElse(null);

      if (user != null && !user.getEmail().equals(_user.getEmail())) {
        throw new BadRequestException("이미 다른 사용자 계정에 등록되어있습니다. 한 계정만 연동가능합니다.");
      }
      log.debug("google");
      _user.getSocialId().setAppleId(socialId.getAppleId());
      _user.getSocialId().setAppleName(socialId.getAppleName());
    } else {
      _user.getSocialId().setAppleId(null);
      _user.getSocialId().setAppleName(null);
    }

    if (_user.getSocialId().isEmpty() && StringUtils.isEmpty(_user.getPassword())) {
      throw new BadRequestException("비밀번호를 등록없이 모든 소셜로그인 해제는 불가능합니다.");
    }
  }

  @Override
  public Facebook connectFacebook(String code, String redirectUri) {

    Map accessTokenResult = facebookAPIService.getAccessToken(code, redirectUri);

    String accessToken = accessTokenResult.get("access_token").toString();
    String tokenType = accessTokenResult.get("token_type").toString();
    String expiresIn = accessTokenResult.get("expires_in").toString();

    log.debug("===facebook accessTokenResult");
    log.debug("accessToken ::: {}", accessToken);
    log.debug("tokenType ::: {}", tokenType);
    log.debug("expiresIn ::: {}", expiresIn);

    Map meResult = facebookAPIService.getMe(accessToken);

    String id = meResult.get("id").toString();
    String name = meResult.get("name").toString();
    String email = meResult.get("email") == null ? null : meResult.get("email").toString();
    Map pictureMap = meResult.get("picture") == null ? null : (Map) meResult.get("picture");
    String gender = meResult.get("gender") == null ? null : meResult.get("gender").toString();

    log.debug("===facebook meResult");
    log.debug("id ::: {}", id);
    log.debug("name ::: {}", name);
    log.debug("email ::: {}", email);
    log.debug("pictureMap ::: {}", pictureMap);
    log.debug("gender ::: {}", gender);

    return Facebook.of(id, name, email, pictureMap, gender);
  }

  @Override
  public GooglePojo connectGoogle(String code, String redirectUri) {

    log.debug("===google");
    log.debug("code ::: {}", code);
    log.debug("redirectUri ::: {}", redirectUri);

    Map accessTokenResult = googleAPIService.getAccessToken(code, redirectUri);
    String accessToken = accessTokenResult.get("access_token").toString();
    String tokenType = accessTokenResult.get("token_type").toString();
    String expiresIn = accessTokenResult.get("expires_in").toString();

    log.debug("===google accessTokenResult");
    log.debug("accessToken ::: {}", accessToken);
    log.debug("tokenType ::: {}", tokenType);
    log.debug("expiresIn ::: {}", expiresIn);

    GooglePojo meResult = googleAPIService.getMe(accessToken);

    log.debug(">> meResult : {}", meResult);
//    String id = meResult.get("id").toString();
//    String name = meResult.get("name").toString();
//    String email = meResult.get("email") == null ? null : meResult.get("email").toString();
//    Map pictureMap = meResult.get("picture") == null ? null : (Map) meResult.get("picture");
//    String gender = meResult.get("gender") == null ? null : meResult.get("gender").toString();

    String id = meResult.getId();

    String name = meResult.getName() == null ? null : meResult.getName();
    String email = meResult.getEmail() == null ? null : meResult.getEmail();
    String pictureMap = meResult.getPicture() == null ? null : meResult.getPicture();

    log.debug("===google meResult");
    log.debug("id ::: {}", id);
    log.debug("name ::: {}", name);
    log.debug("email ::: {}", email);
    log.debug("pictureMap ::: {}", pictureMap);

    return meResult;
  }

  @Override
  public Naver connectNaver(String code, String redirectUri, String state) {

    log.debug("===naver request");
    log.debug("code ::: {}", code);
    log.debug("redirectUri ::: {}", redirectUri);
    log.debug("state ::: {}", state);

    log.debug("===naver");
    log.debug("code ::: {}", code);
    log.debug("redirectUri ::: {}", redirectUri);

    Map accessTokenResult = naverAPIService.getAccessToken(code, redirectUri, state);

    String accessToken = accessTokenResult.get("access_token").toString();
    String refreshToken = accessTokenResult.get("refresh_token").toString();
    String tokenType = accessTokenResult.get("token_type").toString();
    String expiresIn = accessTokenResult.get("expires_in").toString();

    log.debug("===naver accessTokenResult");
    log.debug("accessToken ::: {}", accessToken);
    log.debug("refreshToken ::: {}", refreshToken);
    log.debug("tokenType ::: {}", tokenType);
    log.debug("expiresIn ::: {}", expiresIn);

    Map meResult = naverAPIService.getMe(accessToken);

    String resultcode = meResult.get("resultcode").toString();
    String message = meResult.get("message").toString();
    Map response = (Map) meResult.get("response");

    log.debug("===naver meResult");
    log.debug("resultcode ::: {}", resultcode);
    log.debug("message ::: {}", message);
    log.debug("response ::: {}", response);

    return Naver.of(response);
  }

  @Override
  public Kakao connectKakao(String code, String redirectUri) {

    log.debug("===kakao");
    log.debug("code ::: {}", code);
    log.debug("redirectUri ::: {}", redirectUri);

    Map accessTokenResult = kakaoAPIService.getAccessToken(code, redirectUri);

    String accessToken = accessTokenResult.get("access_token").toString();
    String refreshToken = accessTokenResult.get("refresh_token") != null ? accessTokenResult.get("refresh_token").toString() : null;
    String tokenType = accessTokenResult.get("token_type") != null ? accessTokenResult.get("token_type").toString() : null;
    String expiresIn = accessTokenResult.get("expires_in") != null ? accessTokenResult.get("expires_in").toString() : null;
    String scope = accessTokenResult.get("scope") != null ? accessTokenResult.get("scope").toString() : null;
    String refreshTokenExpiresIn = accessTokenResult.get("refresh_token_expires_in") != null ? accessTokenResult.get("refresh_token_expires_in").toString() : null;

    log.debug("===kakao accessTokenResult");
    log.debug("accessToken ::: {}", accessToken);
    log.debug("refreshToken ::: {}", refreshToken);
    log.debug("tokenType ::: {}", tokenType);
    log.debug("expiresIn ::: {}", expiresIn);
    log.debug("scope ::: {}", scope);
    log.debug("refreshTokenExpiresIn ::: {}", refreshTokenExpiresIn);

    Map meResult = kakaoAPIService.getMe(accessToken);

    log.debug("meResult ::: {}", meResult);

    String email = null;
    boolean emailVerified = false;
    String nickName = null;
    String image = null;

    if (meResult.get("kakao_account") != null) {
      Map account = (Map) meResult.get("kakao_account");

      Map profile = account != null ? (Map) account.get("profile") : null;

      if (profile != null) {
        log.debug("nickname ::: {}", profile.get("nickname"));
        log.debug("thumbnail_image_url ::: {}", profile.get("thumbnail_image_url"));
        log.debug("profile_image_url ::: {}", profile.get("profile_image_url"));
        nickName = profile.get("nickname") != null ? profile.get("nickname").toString() : null;
        image = profile.get("profile_image_url") != null ? profile.get("profile_image_url").toString() : null;
      }
      boolean email_needs_agreement = account.get("email_needs_agreement") != null ? (Boolean) account.get("email_needs_agreement") : false;
      boolean hasEmail = account.get("has_email") != null ? (Boolean) account.get("has_email") : false;
      boolean is_email_valid = account.get("is_email_valid") != null ? (Boolean) account.get("is_email_valid") : false;
      boolean age_range_needs_agreement = account.get("age_range_needs_agreement") != null ? (Boolean) account.get("age_range_needs_agreement") : false;
      boolean gender_needs_agreement = account.get("gender_needs_agreement") != null ? (Boolean) account.get("gender_needs_agreement") : false;
      String age_range = account.get("age_range") != null ? account.get("age_range").toString() : null;
      String birthday = account.get("birthday") != null ? account.get("birthday").toString() : null;
      String gender = account.get("gender") != null ? account.get("gender").toString() : null;


      emailVerified = account.get("is_email_verified") != null ? (Boolean) account.get("is_email_verified") : false;
      email = account.get("email") != null ? account.get("email").toString() : null;
    }

    String id = meResult.get("id").toString();
    Map properties = (Map) meResult.get("properties");

    log.debug("===kakao meResult");
//    log.debug("email ::: {}", email);
//    log.debug("kaccountEmailVerified ::: {}", emailVerified);
//    log.debug("id ::: {}", id);
//    log.debug("properties ::: {}", properties);

    Kakao kakao = Kakao.of(id, email, nickName, image, emailVerified, properties);
    log.debug("kakao login : {}", kakao);
    return kakao;
  }
  @Override
  public Apple connectApple(String code, String redirectUri) {
    return null;
  }
}
