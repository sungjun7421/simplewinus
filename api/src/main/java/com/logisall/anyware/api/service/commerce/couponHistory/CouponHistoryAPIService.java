package com.logisall.anyware.api.service.commerce.couponHistory;

import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistory;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.reqbody.commerce.coupon.CouponThumbBody;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;

import java.util.Locale;

public interface CouponHistoryAPIService {
    // C
    CouponHistory create(CouponHistory couponBox);
    void use(Long idCouponBoxList);
    void restore(Long idCouponBoxList);

    // Rest
    boolean registerCode(Long id, String email);
    boolean issueDefaultCoupon(Long idCoupon);
    PagedModel<CouponThumbBody> pagedResources(Locale locale, Filter filter, String email, Boolean isUsed);
    CollectionModel<EntityModel<CouponThumbBody>> resources(Locale locale, String email, Boolean used);
}
