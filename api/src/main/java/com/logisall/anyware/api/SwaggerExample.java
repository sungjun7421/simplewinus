package com.logisall.anyware.api;

public class SwaggerExample {

    public static final String RES_BODY = "{}";
    public static final String REQ_BODY = "{}";

    // TEST CRUD
    public static final String RES_TEST_POST = "{\"test1\":\"test\",\"test2\":10,\"test3\":true}";

    // Signup json (Payload) example
    public static final String SIGN_UP_1 = "{\n" +
            "  \"email\": \"test@logisall.com\",\n" +
            "  \"password\": \"abcd1234\",\n" +
            "  \"fullName\": \"홍길동\",\n" +
            "  \"mobile\": \"01011112222\",\n" +
            "  \"termsAgree\": {\n" +
            "    \"taService\": true,\n" +
            "    \"taPrivacy\": true,\n" +
            "    \"taYouth\": true,\n" +
            "    \"taEft\": true,\n" +
            "    \"taLocation\": true,\n" +
            "    \"smsRcv\": true,\n" +
            "    \"emailRcv\": true," +
            "    \"kakaoRcv\": true\n" +
            "  }," +
            "  \"socialId\": {\n" +
            "    \"facebookId\": \"\",\n" +
            "    \"facebookName\": \"\",\n" +
            "    \"googleId\": \"\",\n" +
            "    \"googleName\": \"\",\n" +
            "    \"kakaoId\": \"\",\n" +
            "    \"kakaoName\": \"\",\n" +
            "    \"appleId\": \"\",\n" +
            "    \"appleName\": \"\",\n" +
            "    \"naverId\": \"\",\n" +
            "    \"naverName\": \"\"\n" +
            "  }\n" +
//            "  \"verification\": { \"token\": \"[모바일인증코드삽입]\" }\n" +
            "}";
    public static final String CONTACT = "{\"countryCode\":\"+82\"," +
            "\"company\":\"aartkorea\"," +
            "\"fullname\":\"aartkorea\"," +
            "\"mobile\":\"01011112222\"," +
            "\"email\":\"test@logisall.com\"," +
            "\"content\":\"this is the content\"" +
            "\"privacyAgree\":\"true\"}";

    public static final String PASSWORD = "{\"password\":\"abcd1234\"," +
            "\"newpassword\":\"abcd1234\"," +
            "\"email\":\"test@logisall.com\"," +
            "\"phone\":\"01011112222\"}";
    public static final String CERT_MOBILE = "{\"code\":\"string\"," +
            "\"mobile\":\"01011112222\"," +
            "\"mode\":\"CERT_MOBILE\"}";
    public static final String SNS = "{\n" +
            "  \"snsType\":\"FACEBOOK\",\n" +
            "  \"code\": \"string\",\n" +
            "  \"redirectUri\":\"string\",\n" +
            "  \"state\": \"string\"\n" +
            "}";
    public static final String NATIVESNS ="{\n" +
            "\"sns type\":\"FACEBOOK\",\n" +
            "\"active\":\"true\",\n" +
            "\"id\":\"1\",\n" +
            "\"name\":\"홍길동\"\n" +
            "}";
    public static final String UPDATE_USER_INFOR = "{\n" +
            "\"fullName\":\"홍길동\",\n" +
            "\"password\":\"abcd1234\",\n" +
            "\"mobile\":\"01011112222\", \n" +
            "\"Email receive\":\"test@logisall.com\", \n" +
            "\"sms receive\":\"sms\"\n" +
            "}";
    public static final String FACEBOOL_LOGIN = "{\n" +
            "\"snsType\":\" FACEBOOK\",\n" +
            "\"code\":\"String\",\n" +
            "\"redirectUri\":\"String\",\n" +
            "\"state\":\"String\"\n" +
            "}";
    public static final String GOOGLE_LOGIN = "{\n" +
            "\"snsType\":\" GOOGLE\",\n" +
            "\"code\":\"String\",\n" +
            "\"redirectUri\":\"String\",\n" +
            "\"state\":\"String\"\n" +
            "}";

    public static final String LOGIN_1 = "{\n" +
        "    \"email\" : \"test@logisall.com\",\n" +
        "    \"password\" : \"abcd1234\"\n" +
        "}";

    public static final String VALID_EMAIL_1 = "{\n" +
        "    \"email\" : \"test@logisall.com\"\n" +
        "}";

    public static final String VALID_MOBILE_1 = "{\n" +
        "    \"mobile\" : \"01011112222\"\n" +
        "}";

    public static final String CERT_MOBILE_1 = "{\n" +
        "    \"mobile\" : \"01011112222\"\n" +
        "}";

    public static final String CERT_EMAIL_1 = "{\n" +
        "    \"email\" : \"test@logisall.com\"\n" +
        "}";

    public static final String CERT_CONFIRM_MOBILE_1 = "{\n" +
        "    \"mobile\" : \"01011112222\",\n" +
        "    \"code\" : \"[auth code]\"\n" +
        "}";

    public static final String FIND_ACCOUNT_MOBILE_1 = "{\n" +
        "    \"mobile\" : \"01011112222\",\n" +
        "    \"code\" : \"[auth code]\"\n" +
        "}";

    public static final String FIND_PASS_1 = "{\n" +
        "  \"email\": \"test@logisall.com\",\n" +
        "  \"fullName\": \"홍길동\",\n" +
        "  \"mobile\": \"01011112222\",\n" +
        "  \"code\": \"auth code\"\n" +
        "}";

    public static final String RESET_PASS_1 = "{\n" +
        "  \"password\": \"abcd1234\",\n" +
        "  \"newPassword\": \"ancd1234\",\n" +
        "  \"code\": \"[auth code]\"\n" +
        "}";

    public static final String QUESTION_1 = "{\n" +
        "  \"member\" : true,\n" +
        "  \"idCategory\" : 1,\n" +
        "  \"title\" : \"abc\",\n" +
        "  \"content\" : \"cba\",\n" +
        "  \"image\" : \"[list string images]\",\n" +
        "  \"fullName\" : \"홍길동\",\n" +
        "  \"email\" : \"test@logisall.com\"\n" +
        "}";

    public static final String COUPON_1 = "{\n" +
        "    \"idCoupon\": \"[idCoupon]\"\n" +
        "}";

    public static final String NAVER_LOGIN = "";

    public static final String KAKAO_LOGIN = "";

    public static final String COMMENT_1 = "";

    public static final String COMMENT_2 = "";

    public static final String DEVICE_1 = "{\n" +
            "  \"deviceToken\" : \"[deviceToken]\",\n" +
            "  \"email\" : \"test@logisall.com\"\n" +
            "}";

    public static final String PUSH_1 = "{\n" +
            "  \"deviceToken\" : \"[deviceToken]\",\n" +
            "  \"title\" : \"[title]\"\n" +
            "  \"content\" : \"[content]\"\n" +
            "}";
    public static final String TOGGLE_WISH="{\n" +
            "\t\"productId\":3\n" +
            "}";
    public static final String TOGGLE_WISH_RESP="{\n" +
            "    \"result\": \"[success/fail]\",\n" +
            "    \"message\": \"[wish/unwish/Product not exists/]\"\n" +
            "}";
    public static final String WISH_1="[\n" +
            "    {\n" +
            "        \"createdDate\": \"<time wish>\",\n" +
            "        \"id\": \"<product_id>\",\n" +
            "        \"information\": {\n" +
            "            \"thumbnail\": \"\",\n" +
            "            \"name\": \"\",\n" +
            "            \"subtitle\": \"\",\n" +
            "            \"summary\": \"\",\n" +
            "            \"content\": \"\"\n" +
            "        },\n" +
            "        \"cogs\": 0,\n" +
            "        \"price\": 0,\n" +
            "        \"dcRate\": 0,\n" +
            "        \"active\": true,\n" +
            "        \"orderDescending\": 0,\n" +
            "        \"productReqBody\": null,\n" +
            "        \"salePrice\": 0,\n" +
            "        \"discount\": 0\n" +
            "    }\n" +
            "]";
    public static final String CART_1="[\n" +
            "    {\n" +
            "        \"idCart\": 8,\n" +
            "        \"product\": {\n" +
            "            \"createdDate\": null,\n" +
            "            \"id\": 2,\n" +
            "            \"information\": {\n" +
            "                \"thumbnail\": null,\n" +
            "                \"name\": {\n" +
            "                    \"textKoKr\": \"B\",\n" +
            "                    \"textEnUs\": null,\n" +
            "                    \"textZhCn\": null,\n" +
            "                    \"textZhTw\": null,\n" +
            "                    \"textJaJp\": null,\n" +
            "                    \"value\": \"B\",\n" +
            "                    \"empty\": false\n" +
            "                },\n" +
            "                \"subtitle\": null,\n" +
            "                \"summary\": null,\n" +
            "                \"content\": null\n" +
            "            },\n" +
            "            \"cogs\": 0,\n" +
            "            \"price\": 0,\n" +
            "            \"dcRate\": 0,\n" +
            "            \"active\": true,\n" +
            "            \"orderDescending\": 3,\n" +
            "            \"productReqBody\": null,\n" +
            "            \"salePrice\": 0,\n" +
            "            \"discount\": 0\n" +
            "        },\n" +
            "        \"qty\": 1\n" +
            "    }\n" +
            "]";
    public static final String CART_ADD_1="{\n" +
            "    \"idCart\": 9,\n" +
            "    \"product\": {\n" +
            "        \"createdDate\": null,\n" +
            "        \"id\": 1,\n" +
            "        \"information\": {\n" +
            "            \"thumbnail\": null,\n" +
            "            \"name\": {\n" +
            "                \"textKoKr\": \"A\",\n" +
            "                \"textEnUs\": null,\n" +
            "                \"textZhCn\": null,\n" +
            "                \"textZhTw\": null,\n" +
            "                \"textJaJp\": null,\n" +
            "                \"value\": \"A\",\n" +
            "                \"empty\": false\n" +
            "            },\n" +
            "            \"subtitle\": null,\n" +
            "            \"summary\": null,\n" +
            "            \"content\": null\n" +
            "        },\n" +
            "        \"cogs\": 0,\n" +
            "        \"price\": 0,\n" +
            "        \"dcRate\": 0,\n" +
            "        \"active\": true,\n" +
            "        \"orderDescending\": 1,\n" +
            "        \"productReqBody\": null,\n" +
            "        \"salePrice\": 0,\n" +
            "        \"discount\": 0\n" +
            "    },\n" +
            "    \"qty\": 1\n" +
            "}";
    public static final String CART_TOTAL_1="{\n" +
            "    \"totalOriginPrice\": 1700,\n" +
            "    \"totalSalePrice\": 1550,\n" +
            "    \"totalProduct\": 3,\n" +
            "    \"totalQty\": 4\n" +
            "}";
}
