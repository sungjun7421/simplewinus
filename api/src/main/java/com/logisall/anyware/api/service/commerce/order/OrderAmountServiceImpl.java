package com.logisall.anyware.api.service.commerce.order;

import com.logisall.anyware.api.service.mathformula.MathFormulaService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.property.ProcessEnv;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.coupon.Coupon;
import com.logisall.anyware.core.domain.commerce.coupon.CouponRepository;
import com.logisall.anyware.core.domain.commerce.coupon.CouponType;
import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistory;
import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistoryRepository;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.domain.commerce.order.OrderRepository;
import com.logisall.anyware.core.domain.commerce.product.Product;
import com.logisall.anyware.core.model.reqbody.commerce.order.PaymentInfo;
import com.logisall.anyware.core.model.reqbody.commerce.order.PaymentInfoReqBody;
import com.logisall.anyware.core.model.reqbody.commerce.order.PaymentPriceInfo;
import com.logisall.anyware.core.model.reqbody.commerce.product.ProductReqBody;
import com.logisall.anyware.core.service.commerce.point.PointService;
import com.logisall.anyware.core.service.commerce.point.PointSettingService;
import com.logisall.anyware.core.service.commerce.product.ProductService;
import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.AtomicDouble;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Slf4j
@Service
public class OrderAmountServiceImpl implements OrderAmountService {


    @Autowired
    private PointService pointService;

    @Autowired
    private PointSettingService pointSettingService;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CouponRepository couponRepository;

    @Autowired
    private CouponHistoryRepository couponHistoryRepository;

    @Autowired
    private MathFormulaService mathFormulaService;

    @Autowired
    private OrderAPIService orderAPIService;


    @Autowired
    private ProcessEnv processEnv;
    @Autowired
    private ProductService productService;

//
//    @Override
//    @Transactional
//    public PaymentPriceInfo paymentPriceInfo(String oid,
//                                             Long idCoupons, // 주문쿠폰
//                                             Double point) {
//
//        return orderRepository.findByOid(oid)
//                .map(order -> {
//                   Buyer buyer = order.getRelativeBuyer();
//
//            /* 결제금액 산출 (상품쿠폰 포함) */
//            PaymentPriceInfo info = this.toPaymentPriceInfo(order, buyer, idCoupons);
////    if (processEnv.isProduction()) {
////      info = order.toPaymentPriceInfo();
////    } else {
////      info = this.toPaymentPriceInfo(order, buyer, idProductCoupons);
////    }
//            log.debug("PaymentPriceInfo ::: {}", info);
//            /* END : 결제금액 산출 (상품쿠폰 포함) */
//
//            // 주문쿠폰 할인 적용
//            if (idCoupons != null) {
//                log.debug("PaymentPriceInfo order coupon 111 ::: {}", info);
//                log.debug("idOrderCoupons ::: {}", idCoupons);
//                this.applyOrderCoupon(info, buyer, idCoupons);
//                log.debug("PaymentPriceInfo order coupon 222 ::: {}", info);
//            }
//
//            // 포인트사용
//            if (point != null && point > 0) {
//                info.setPoint(point);
//                info.setTotalByPaymentPrice(info.getTotalByPaymentPrice().subtract(BigDecimal.valueOf(info.getPoint())));
//            }
//
//            return info;
//        }).orElseThrow(BadRequestException::new);
//    }
//
//

    @Override
    @Transactional
    public PaymentInfo paymentInfo(PaymentInfoReqBody paymentInfoReqBody){

        Order order = orderRepository.findByOid(paymentInfoReqBody.getOid()).get();
        Buyer buyer = order.getRelativeBuyer();
        int totalPoint = pointService.total(buyer.getRelativeUser().getId());

        PaymentInfo info = new PaymentInfo();
        info.setTotalPriceOrder(order.getAmount());
        info.setTotalPaymentPrice(order.getTotalPrice());

        //coupon
        if(paymentInfoReqBody.getIdCouponHistoty() != null) {
            if(!couponHistoryRepository.existsById(paymentInfoReqBody.getIdCouponHistoty())){
                throw new BadRequestException();
            }
            BigDecimal coupon = this.getDiscountCoupon(paymentInfoReqBody.getIdCouponHistoty(), order.getTotalPrice());
            if(coupon.intValue()>0){
                List<CouponHistory> couponHistoryList = new ArrayList<>();
                CouponHistory couponHistory =couponHistoryRepository.getOne(paymentInfoReqBody.getIdCouponHistoty());
                couponHistory.setRelativeBuyer(buyer);
                couponHistoryList.add(couponHistory);
                order.setUsedCoupons(couponHistoryList);
                couponHistoryRepository.save(couponHistory);
                order.setFcPoint(coupon);

            }

            info.setTotalPriceCoupon(coupon.setScale(0, BigDecimal.ROUND_HALF_UP));
            info.setTotalPaymentPrice(info.getTotalPaymentPrice().subtract(coupon).setScale(0, BigDecimal.ROUND_HALF_UP));
        }
        // point
        if(paymentInfoReqBody.getPoint() != null) {
            BigDecimal point = paymentInfoReqBody.getPoint();
            if(point.compareTo(BigDecimal.ZERO) < 0 || point.compareTo(BigDecimal.valueOf(totalPoint)) > 0){
                throw new BadRequestException();
            }
            order.setFcPoint(point);
            info.setTotalPricePoint(point.setScale(0, BigDecimal.ROUND_HALF_UP));
            info.setTotalPaymentPrice(info.getTotalPaymentPrice().subtract(point).setScale(0, BigDecimal.ROUND_HALF_UP));

        }


//        save point
//        info.setTotalPriceSave(orderAPIService.cumulativePoint(order));
        orderRepository.save(order);

        return info;

    }

//    @Override
//    @Transactional
//    public PaymentInfo paymentInfo(PaymentInfoReqBody paymentInfoReqBody,String email){
//
//
//        Order order = orderRepository.findByOid(paymentInfoReqBody.getOid()).get();
//        order.setPayMethod(paymentInfoReqBody.getPayMethod());
//        Buyer buyer = order.getRelativeBuyer();
//        int totalPoint = pointService.total(buyer.getRelativeUser().getId());
//
//        PaymentInfo info = new PaymentInfo();
//        info.setTotalPriceOrder(order.getTotalPrice());
//        info.setTotalPaymentPrice(order.getAmount());
//
//        //coupon
//        if(paymentInfoReqBody.getIdCoupon() != null) {
//            if(!couponHistoryRepository.existsById(paymentInfoReqBody.getIdCoupon())){
//                throw new BadRequestException();
//            }
//            BigDecimal couponPrice = this.getDiscountCoupon(paymentInfoReqBody.getIdCoupon(), order.getAmount());
//
//
//            info.setTotalPriceCoupon(couponPrice.setScale(0, BigDecimal.ROUND_HALF_UP));
//            info.setTotalPaymentPrice(info.getTotalPaymentPrice().subtract(couponPrice).setScale(0, BigDecimal.ROUND_HALF_UP));
//        }
//        // point
//        if(paymentInfoReqBody.getPoint() != null) {
//            BigDecimal point = paymentInfoReqBody.getPoint();
//            if(point.compareTo(BigDecimal.ZERO) < 0 || point.compareTo(new BigDecimal(totalPoint)) > 0){
//                throw new BadRequestException();
//            }
//
//
//            info.setTotalPricePoint(point.setScale(0, BigDecimal.ROUND_HALF_UP));
//            info.setTotalPaymentPrice(info.getTotalPaymentPrice().subtract(point).setScale(0, BigDecimal.ROUND_HALF_UP));
//        }
//
//
////        save point
////        info.setTotalPriceSave(orderAPIService.cumulativePoint(order));
//
//        return info;
//
//    }
    private BigDecimal getDiscountCoupon(Long idCouponHistory, BigDecimal totalPrice){
        CouponHistory couponHistory = couponHistoryRepository.getOne(idCouponHistory);
        Coupon coupon = couponHistory.getRelativeCoupon();

        //check require couponHistory and coupon
        if(couponHistory.isUsed() || LocalDate.now().isBefore(coupon.getStartDate()) || LocalDate.now().isAfter(coupon.getEndDate())){
            throw new BadRequestException("Coupon not Invalid");
        }
        if( totalPrice.subtract(coupon.getMinimumPayment()).intValue()<=0){
            throw new BadRequestException("Total price has not reached to minimum payment");

        }

        if(coupon.getType().equals(CouponType.DISCOUNT_FIXED)){
            return coupon.getDiscountFixed();
        }else if(coupon.getType().equals(CouponType.DISCOUNT_PERCENT)){
            return totalPrice.multiply(coupon.getDiscountFixed()).divide(BigDecimal.valueOf(100));
        }


        return BigDecimal.ZERO;
    }


    @Override
    @Transactional
    public PaymentInfo createPayment(PaymentInfoReqBody paymentInfoReqBody){
        PaymentInfo paymentInfo = this.paymentInfo(paymentInfoReqBody);
        if(!paymentInfoReqBody.getFinalPrice().equals(paymentInfo.getTotalPaymentPrice())){
            throw new BadRequestException("Price not map!");
        }
        // set totalPrice = finalPrice to use for payment
        Order order = orderRepository.findByOid(paymentInfoReqBody.getOid()).get();

        order.setTotalPrice(paymentInfoReqBody.getFinalPrice());
        order.setPayMethod(paymentInfoReqBody.getPayMethod());
        orderRepository.save(order);
        return paymentInfo;
    }

//    private void applyOrderCoupon(PaymentPriceInfo info, Buyer buyer, Long idCoupons) {
//
//        final LocalDate now = LocalDate.now();
//        final BigDecimal finalValue = info.getTotalByPaymentPrice();
//        final double totalDoubleValue = finalValue.doubleValue();
//
//        BigDecimal totalValue = finalValue;
//        BigDecimal totalCouponDcValue = info.getTotalByDiscountCoupon();
//
//        // 1. Check coupon history
//        Boolean couponHistory = couponHistoryRepository.existsByRelativeBuyer_IdAndRelativeCoupon_Id(buyer.getId(), idCoupons);
//
//        if (!couponHistory) {
//            log.info("사용하지 않음 : {}", couponHistory);
//            throw new BadRequestException(ResponseErrorCode.ALREADY_USED_COUPON.name());
//        }
//
//        Coupon coupon = couponRepository.getOne(idCoupons);
//
//
//        // 2. check coupon min
//
//        // 3. date limit
//        boolean a = coupon.getStartDate() != null;
//        boolean b = coupon.getEndDate() != null;
//        if (!a) {
//            log.info("쿠폰 시작일 : {}", a);
//            throw new BadRequestException(ResponseErrorCode.NOT_EXIST_COUPON_START_DATE.name());
//        }
//        if (!b) {
//            log.info("쿠폰 종료일 : {}", b);
//            throw new BadRequestException(ResponseErrorCode.NOT_EXIST_COUPON_END_DATE.name());
//        }
//
//        // 정액 할인
//        BigDecimal discountFixed = coupon.getDiscountFixed();
//        log.debug("discountFixed 111 ::: {}", discountFixed);
//        if (discountFixed == null) {
//            throw new BadRequestException("정액이 입력되지 않았습니다.");
//        }
//        // 총 할인된 금액
//        if (totalValue.doubleValue() < discountFixed.doubleValue()) {
//            discountFixed = totalValue;
//            log.debug("discountFixed 333 ::: {}", discountFixed);
//        }
//        log.debug("discountFixed @@@ ::: {}", discountFixed);
//        totalValue = totalValue.subtract(discountFixed);
//        totalCouponDcValue = totalCouponDcValue.add(discountFixed);
//
//        info.setTotalByPaymentPrice(totalValue);
//        info.setTotalByDiscountCoupon(totalCouponDcValue);
//
//    }

    private PaymentPriceInfo toPaymentPriceInfo(Order order, Buyer buyer, Long idCoupons) {

        AtomicDouble price = new AtomicDouble(0);
        AtomicDouble dc = new AtomicDouble(0);
        AtomicDouble dcByCoupon = new AtomicDouble(0);
        AtomicDouble hwdc = new AtomicDouble(0);
        AtomicDouble amount = new AtomicDouble(0);
        order.setFcCoupon(BigDecimal.ZERO);
//        if (machine != null) {
//
//            // 상품쿠폰 할인 데이터 초기화
//            order.setFcCoupon(BigDecimal.ZERO);
//            order.getDcPriceForCouponDc();
//
//            // 상품쿠폰 할인 로직
//            if (idCoupons != null) {
//                final LocalDate now = LocalDate.now();
//                final BigDecimal dcPriceForCouponDc = order.getDcPriceForCouponDc(); // 총 할인된 상품금액 (쿠폰할인 계산용)
//                Coupon coupon = couponRepository.getOne(idCoupons);
//
//
//                // 정액 할인
//                BigDecimal discountFixed = coupon.getDiscountFixed();
//                if (discountFixed == null) {
//                    throw new BadRequestException("정액이 입력되지 않았습니다.");
//                }
//
//                BigDecimal dcP = this.getDiscountPrice(coupon, dcPriceForCouponDc, discountFixed);
//                log.debug("{} - {} 정액 dcP ::: {}", machine.getId(), coupon.getId(), dcP);
//                order.setFcCoupon(order.getFcCoupon().add(dcP));
//                order.resetPaymentAmountByCouponDc();
//
//            }
//
//        }


        price.addAndGet(order.getAmount().doubleValue());
        dcByCoupon.addAndGet(order.getFcCoupon().doubleValue());

//      amount.addAndGet(op.getAmount().doubleValue() - discountPriceByCoupon.doubleValue());
        amount.addAndGet(order.getAmount().doubleValue());

        if (order.getAmount() == null) {
            log.info("this.amount ::: {}", order.getAmount());
            throw new BadRequestException("최종 지불금액(결제금액) 이 존재하지 않음!");
        }

        return PaymentPriceInfo.builder()
                .totalByHwdc(BigDecimal.valueOf(hwdc.doubleValue()))
                .totalByPrice(BigDecimal.valueOf(amount.doubleValue()))
                .totalByDiscountCoupon(BigDecimal.valueOf(dcByCoupon.doubleValue()))
                .point(0) // 포인트 적용전이기 떄문에 0 으로 입력
                .totalByPaymentPrice(BigDecimal.valueOf(amount.doubleValue()))
                .build();
    }

    private BigDecimal getDiscountPrice(Coupon coupon,
                                        BigDecimal dcPriceForCouponDc,
                                        BigDecimal discountPrice) {

        // 총 할인된 금액
        if (discountPrice.doubleValue() > dcPriceForCouponDc.doubleValue()) {
            return dcPriceForCouponDc;
        }

        return discountPrice;
    }

//    @Override
//    @Transactional
//    public BigDecimal usedCoupon(BigDecimal total) {
//        log.debug("total ::: {}", total);
//        return total.setScale(0, RoundingMode.HALF_UP);
//    }
//
//    @Override
//    public BigDecimal usedPoint(BigDecimal total, int point, Buyer buyer) {
//
//        if (point > 0 && buyer != null) {
//
//            log.debug("buyer ::: {}", buyer);
//
//            // 사용가능한 포인트
//            final int availablePoint = pointService.total(buyer.getId());
//            log.debug("availablePoint ::: {}", availablePoint);
//
//            /* 1. 사용하려는 포인트가 보유포인트보다 많습니다. */
//            if (point > availablePoint) {
//                throw new BadRequestException(ResponseErrorCode.MORE_THAN_MY_POINTS.name());
//            }
//
//            total = total.subtract(BigDecimal.valueOf(point)); // 총합 = 총합 - 포인트 (포인트 차감 금액)
//        }
//
//        return total;
//    }

    @Override
    public BigDecimal savingPoint(BigDecimal amount, Buyer buyer) {

        BigDecimal savingPoint = BigDecimal.ZERO;
        if (buyer != null) {
            String savingExpression = buyer.getRelativeBuyerLevel().getDiscountExpression();
            double price = amount.doubleValue(); // 상품 판매가
            savingPoint = mathFormulaService.calculation(savingExpression, ImmutableMap.of("price", price), "price");
        }

        return savingPoint;
    }

    @Override
    public BigDecimal totalByProductAndQty(List<ProductReqBody> products) {
        BigDecimal total = BigDecimal.valueOf(0);
        //comment for test paypment

        if (products != null) {

            total = total.add(new BigDecimal(

                    products.stream().mapToInt(product -> {
                        //log.debug("product ::: {}", product);
                        Product _product = productService.get(Locale.KOREA, product.getId());
                        //log.debug("_product ::: {}", _product);
                        BigDecimal a = _product.getSalePrice().multiply(BigDecimal.valueOf(product.getQty())).setScale(-1, RoundingMode.FLOOR);

                        BigDecimal hwdcBig = BigDecimal.valueOf(0);
                        // 10 매이상 10% 할인
                        if(product.getQty() >= ProductReqBody.GROUP) {
                            hwdcBig = a.multiply(new BigDecimal(ProductReqBody.SALE));
                        }
                        BigDecimal c = a.subtract(hwdcBig);

                        return c.intValue();
                    }).sum()

            ));


        }

        if (total == null || total.intValue() == 0) {
            throw new BadRequestException("등록된 상품 또는 가격이 없습니다."); // 가격 없음 (제품 없음)
        }
        return total;
    }

}
