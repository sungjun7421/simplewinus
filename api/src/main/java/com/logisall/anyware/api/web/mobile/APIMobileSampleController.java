package com.logisall.anyware.api.web.mobile;

import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@RestController
@RequestMapping("/api/v1/samples")
public class APIMobileSampleController {

  @Operation(summary = "[mobile-sample-1] Mobile User Sample API", description = "", security = {@SecurityRequirement(name = "bearerAuth")},
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = " ")
          )))
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR")
  })
  @GetMapping
  public ResponseEntity<?> sample1(@Parameter(hidden = true) OAuth2Authentication oAuth2Authentication,
                                   HttpServletRequest request,
                                   Locale locale) {

    // TODO Mobile USER CHECK
    ValidUtils.isForbiddenUser(oAuth2Authentication);

    return ResponseEntity.ok("OK");
  }
}
