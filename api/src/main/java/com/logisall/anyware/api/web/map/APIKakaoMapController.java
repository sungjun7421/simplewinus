package com.logisall.anyware.api.web.map;


import com.logisall.anyware.core.service.map.kakao.MapKakaoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Slf4j
@Tag(name = "Kakao Map API", description = "카카오 맵 API")
@RestController
@RequestMapping("/api/v1/kakao-map")
public class APIKakaoMapController {

  @Autowired
  private MapKakaoService mapKakaoService;

  @Operation(summary = "[kakao-map-1] ", description = "주소검색 API",
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = "{\"key\":\"[\"}")
          ))
  )
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR"),
      @Parameter(in = ParameterIn.QUERY, name = "query", description = "(필수) 검색을 원하는 질의어", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "page", description = "결과 페이지 번호, 1-45 사이, 기본 값 1", example = "1"),
      @Parameter(in = ParameterIn.QUERY, name = "size", description = "한 페이지에 보여질 문서의 개수, 1-30 사이, 기본 값 10", example = "10"),
  })
  @GetMapping("/address")
  public ResponseEntity<?> address(@RequestParam String query,
                                   @RequestParam(required = false) Integer page,
                                   @RequestParam(required = false) Integer size) {

    Future<Map<String, Object>> future = mapKakaoService.address(query, page, size);

    try {
      return ResponseEntity.ok(future.get());
    } catch (InterruptedException | ExecutionException e) {
      log.error("kakao map error", e);
    }
    return ResponseEntity.badRequest().build();
  }

  @Operation(summary = "[kakao-map-2] ", description = "좌표로 행정구역정보 받기 API",
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = "{\"key\":\"[\"}")
          ))
  )
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR"),
      @Parameter(in = ParameterIn.QUERY, name = "x", description = "(필수) x 좌표로 경위도인 경우 longitude", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "y", description = "(필수) y 좌표로 경위도인 경우 latitude", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "inputCoord", description = "시x, y 로 입력되는 값에 대한 좌표 체계, 기본 값은 WGS84 지원 좌표계: WGS84, WCONGNAMUL, CONGNAMUL, WTM, TM작일", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "outputCoord", description = "결과에 출력될 좌표 체계, 기본 값은 WGS84 지원 좌표계: WGS84, WCONGNAMUL, CONGNAMUL, WTM, TM", example = ""),
  })
  @GetMapping("/coord2regioncode")
  public ResponseEntity<?> administrative(@RequestParam Double x,
                                          @RequestParam Double y,
                                          @RequestParam(required = false) String inputCoord,
                                          @RequestParam(required = false) String outputCoord) {

    Future<Map<String, Object>> future = mapKakaoService.coord2regioncode(x, y, inputCoord, outputCoord);

    try {
      return ResponseEntity.ok(future.get());
    } catch (InterruptedException | ExecutionException e) {
      log.error("kakao map error", e);
    }
    return ResponseEntity.badRequest().build();
  }

  @Operation(summary = "[kakao-map-3] ", description = "좌표로 주소 변환하기 API",
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = "{\"key\":\"[\"}")
          ))
  )
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR"),
      @Parameter(in = ParameterIn.QUERY, name = "x", description = "(필수) x 좌표로 경위도인 경우 longitude", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "y", description = "(필수) y 좌표로 경위도인 경우 latitude", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "inputCoord", description = "시x, y 로 입력되는 값에 대한 좌표 체계, 기본 값은 WGS84 지원 좌표계: WGS84, WCONGNAMUL, CONGNAMUL, WTM, TM작일", example = ""),
  })
  @GetMapping("/coord2address")
  public ResponseEntity<?> coord2address(@RequestParam Double x,
                                         @RequestParam Double y,
                                         @RequestParam(required = false) String inputCoord) {

    Future<Map<String, Object>> future = mapKakaoService.coord2address(x, y, inputCoord);

    try {
      return ResponseEntity.ok(future.get());
    } catch (InterruptedException | ExecutionException e) {
      log.error("kakao map error", e);
    }
    return ResponseEntity.badRequest().build();
  }

  @Operation(summary = "[kakao-map-4] ", description = "좌표계 변환 API",
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = "{\"key\":\"[\"}")
          ))
  )
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR"),
      @Parameter(in = ParameterIn.QUERY, name = "x", description = "(필수) x 좌표로 경위도인 경우 longitude", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "y", description = "(필수) y 좌표로 경위도인 경우 latitude", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "inputCoord", description = "시x, y 로 입력되는 값에 대한 좌표 체계, 기본 값은 WGS84 지원 좌표계: WGS84, WCONGNAMUL, CONGNAMUL, WTM, TM작일", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "outputCoord", description = "결과에 출력될 좌표 체계, 기본 값은 WGS84 지원 좌표계: WGS84, WCONGNAMUL, CONGNAMUL, WTM, TM", example = ""),
  })
  @GetMapping("/transcoord")
  public ResponseEntity<?> coordinate(@RequestParam Double x,
                                      @RequestParam Double y,
                                      @RequestParam(required = false) String inputCoord,
                                      @RequestParam(required = false) String outputCoord) {

    Future<Map<String, Object>> future = mapKakaoService.transcoord(x, y, inputCoord, outputCoord);

    try {
      return ResponseEntity.ok(future.get());
    } catch (InterruptedException | ExecutionException e) {
      log.error("kakao map error", e);
    }
    return ResponseEntity.badRequest().build();
  }

  @Operation(summary = "[kakao-map-5] ", description = "키워드로 장소 검색 API",
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = "{\"key\":\"[\"}")
          ))
  )
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR"),
      @Parameter(in = ParameterIn.QUERY, name = "query", description = "(필수) 검색을 원하는 질의어", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "categoryGroupCode", description = "카테고리 그룹 코드 결과를 카테고리로 필터링을 원하는 경우 사용", example = "MT1"),
      @Parameter(in = ParameterIn.QUERY, name = "x", description = "중심 좌표의 X값 혹은 longitude 특정 지역을 중심으로 검색하려고 할 경우 radius와 함께 사용 가능", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "y", description = "중심 좌표의 Y값 혹은 latitude 특정 지역을 중심으로 검색하려고 할 경우 radius와 함께 사용 가능", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "radius", description = "중심 좌표부터의 반경거리. 특정 지역을 중심으로 검색하려고 할 경우 중심좌표로 쓰일 x,y와 함께 사용 단위 meter, 0~20000 사이의 값", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "rect", description = "사각형 범위내에서 제한 검색을 위한 좌표. 지도 화면 내 검색시 등 제한 검색에서 사용 가능 좌측 X 좌표,좌측 Y 좌표, 우측 X 좌표, 우측 Y 좌표 형식", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "page", description = "결과 페이지 번호, 1~45 사이의 값, 기본 값 1", example = "1"),
      @Parameter(in = ParameterIn.QUERY, name = "size", description = "한 페이지에 보여질 문서의 개수, 1~15 사이의 값, 기본 값 15", example = "15"),
      @Parameter(in = ParameterIn.QUERY, name = "sort", description = "결과 정렬 순서, distance 정렬을 원할 때는 기준 좌표로 쓰일 x, y와 함께 사용 distance 또는 accuracy, 기본 accuracy", example = "accuracy"),
  })
  @GetMapping("/keyword")
  public ResponseEntity<?> keyword(@RequestParam String query,
                                   @RequestParam(required = false) MapKakaoService.CategoryType categoryGroupCode,
                                   @RequestParam(required = false) Double x,
                                   @RequestParam(required = false) Double y,
                                   @RequestParam(required = false) Integer radius,
                                   @RequestParam(required = false) String rect,
                                   @RequestParam(required = false) Integer page,
                                   @RequestParam(required = false) Integer size,
                                   @RequestParam(required = false) String sort) {

    Future<Map<String, Object>> future = mapKakaoService.keyword(query, categoryGroupCode, x, y, radius, rect, page, size, sort);

    try {
      return ResponseEntity.ok(future.get());
    } catch (InterruptedException | ExecutionException e) {
      log.error("kakao map error", e);
    }
    return ResponseEntity.badRequest().build();
  }

  @Operation(summary = "[kakao-map-6] ", description = "카테고리로 장소 검색 API",
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = "{\"key\":\"[\"}")
          ))
  )
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR"),
      @Parameter(in = ParameterIn.QUERY, name = "categoryGroupCode", description = "카테고리 그룹 코드 결과를 카테고리로 필터링을 원하는 경우 사용", example = "MT1"),
      @Parameter(in = ParameterIn.QUERY, name = "x", description = "((x,y,radius) 또는 rect 필수) 중심 좌표의 X값 혹은 longitude 특정 지역을 중심으로 검색하려고 할 경우 radius와 함께 사용 가능", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "y", description = "((x,y,radius) 또는 rect 필수) 중심 좌표의 Y값 혹은 latitude 특정 지역을 중심으로 검색하려고 할 경우 radius와 함께 사용 가능", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "radius", description = "((x,y,radius) 또는 rect 필수) 중심 좌표부터의 반경거리. 특정 지역을 중심으로 검색하려고 할 경우 중심좌표로 쓰일 x,y와 함께 사용. 단위 meter, 0~20000 사이의 값", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "rect", description = "사각형 범위내에서 제한 검색을 위한 좌표 지도 화면 내 검색시 등 제한 검색에서 사용 가능 좌측 X 좌표, 좌측 Y 좌표, 우측 X 좌표, 우측 Y 좌표 형식 x, y, radius 또는 rect 필수", example = ""),
      @Parameter(in = ParameterIn.QUERY, name = "page", description = "결과 페이지 번호, 1-45 사이의 값, 기본 값 1", example = "1"),
      @Parameter(in = ParameterIn.QUERY, name = "size", description = "한 페이지에 보여질 문서의 개수, 1~15 사이의 값, 기본 값 15", example = "15"),
      @Parameter(in = ParameterIn.QUERY, name = "sort", description = "결과 정렬 순서, distance 정렬을 원할 때는 기준 좌표로 쓰일 x, y와 함께 사용 distance 또는 accuracy, 기본 accuracy", example = "accuracy"),
  })
  @GetMapping("/category")
  public ResponseEntity<?> category(@RequestParam MapKakaoService.CategoryType categoryGroupCode,
                                   @RequestParam(required = false) Double x,
                                   @RequestParam(required = false) Double y,
                                   @RequestParam(required = false) Integer radius,
                                   @RequestParam(required = false) String rect,
                                   @RequestParam(required = false) Integer page,
                                   @RequestParam(required = false) Integer size,
                                   @RequestParam(required = false) String sort) {

    Future<Map<String, Object>> future = mapKakaoService.category(categoryGroupCode, x, y, radius, rect, page, size, sort);

    try {
      return ResponseEntity.ok(future.get());
    } catch (InterruptedException | ExecutionException e) {
      log.error("kakao map error", e);
    }
    return ResponseEntity.badRequest().build();
  }
}
