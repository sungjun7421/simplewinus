package com.logisall.anyware.api.web.board;

import com.logisall.anyware.api.service.board.event.EventAPIService;
import com.logisall.anyware.core.model.Filter;
import com.google.common.collect.ImmutableMap;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

@Tag(name = "Event", description = "Event 게시판 API")
@Slf4j
@RestController
@RequestMapping("api/v1/events")
public class APIEventController {

  @Autowired
  private EventAPIService eventService;

  @Operation(summary = "[event-1] 이벤트 검색 & 목록")
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR"),
      @Parameter(in = ParameterIn.QUERY, name = "size", description = "페이지 사이즈 (Size Page)  (default : 20)", example = "20"),
      @Parameter(in = ParameterIn.QUERY, name = "page", description = "현재 페이지 0부터 (Current Page)  (default : 0)", example = "0"),
      @Parameter(in = ParameterIn.QUERY, name = "sort", description = "정렬 (Sort Page)", example = "createdDate,desc"),
  })
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> page(@Parameter(hidden = true) @PageableDefault(size = 20, sort = {"top", "createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                                @Parameter(description = "검색어(Search word)") @RequestParam(required = false, defaultValue = "") String query,
                                @Parameter(description = "시작시간(Start Time) (yyyy-MM-dd HH:mm:ss)") @RequestParam(required = false) String startDate,
                                @Parameter(description = "종료시간(End Time) (yyyy-MM-dd HH:mm:ss)") @RequestParam(required = false) String endDate,
                                @Parameter(description = "종료여부") @RequestParam(required = false) Boolean end,
                                Locale locale) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    return ResponseEntity.ok(eventService.pagedResources(locale, filter, end));
  }

  @Operation(summary = "[event-2] 이벤트 게시물 상세")
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR")
  })
  @GetMapping(value = "/{id}")
  public ResponseEntity<?> get(@Parameter(description = "id") @PathVariable Long id,
                               Locale locale) {
    long pageView = eventService.pageViewUp(id);
    log.debug("pageView : {}", pageView);
    return ResponseEntity.ok(eventService.resource(locale, id));
  }

  @Operation(summary = "[event-3] 매거진 Sort Type")
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR")
  })
  @GetMapping(value = "/sort-types", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> sortTypes() {

    return ResponseEntity.ok(new EntityModel(ImmutableMap.builder()
        .put("최신순", "createdDate,desc")
        .put("조회순", "pageView,desc")
        .put("기간순", "")
        .build()));
  }

  @Operation(summary = "[event-4] 이벤트's 상품(와인)")
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR"),
      @Parameter(in = ParameterIn.QUERY, name = "size", description = "페이지 사이즈 (Size Page)  (default : 20)", example = "20"),
      @Parameter(in = ParameterIn.QUERY, name = "page", description = "현재 페이지 0부터 (Current Page)  (default : 0)", example = "0"),
      @Parameter(in = ParameterIn.QUERY, name = "sort", description = "정렬 (Sort Page)", example = "createdDate,desc"),
  })
  @GetMapping(value = "/{id}/products", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> products(@PathVariable Long id,
                                    @Parameter(hidden = true) @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                                    @Parameter(description = "검색어(Search word)") @RequestParam(required = false, defaultValue = "") String query,
                                    @Parameter(description = "시작시간(Start Time) (yyyy-MM-dd HH:mm:ss)") @RequestParam(required = false) String startDate,
                                    @Parameter(description = "종료시간(End Time) (yyyy-MM-dd HH:mm:ss)") @RequestParam(required = false) String endDate,
                                    Locale locale) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    return ResponseEntity.ok().build();
  }

  @Operation(summary = "[event-5] 이벤트's 상품(와인)의 종류")
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR")
  })
  @GetMapping(value = "/products/kinds", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> types(Locale locale) {

    return ResponseEntity.ok().build();
  }
}
