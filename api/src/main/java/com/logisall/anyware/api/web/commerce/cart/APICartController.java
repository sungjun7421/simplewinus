package com.logisall.anyware.api.web.commerce.cart;

import com.logisall.anyware.api.SwaggerExample;
import com.logisall.anyware.api.service.commerce.cart.CartAPIService;
import com.logisall.anyware.core.config.exception.ForbiddenException;
import com.logisall.anyware.core.model.reqbody.commerce.cart.CartDeleteReqBody;
import com.logisall.anyware.core.model.reqbody.commerce.cart.CartRequestBody;
import com.logisall.anyware.core.model.reqbody.commerce.cart.CartUpdateReqBody;
import com.logisall.anyware.core.model.resbody.commerce.cart.CartResBody;
import com.logisall.anyware.core.model.resbody.commerce.cart.CartResTotalBody;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Tag(name = "Cart", description = "Cart API")
@RestController
@RequestMapping("/api/v1/cart")
public class APICartController {
    @Autowired
    CartAPIService cartAPIService;

    @Operation(
            summary = "Get list cart of login user",
            description = "list cart of login user",
            security = {@SecurityRequirement(name = "bearerAuth")},
            responses = {
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(example = SwaggerExample.CART_1,implementation = CartResBody.class)
                            ),
                            description = "return empty list [] or list object [schema]",
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,

                                    schema = @Schema(example ="{\n" +
                                            "    \"code\": \"403\",\n" +
                                            "    \"message\": \"접근권한이 없습니다.\"\n" +
                                            "}")
                            ),
                            description="oath error",
                            responseCode = "403"
                    )
            }
    )
    @GetMapping(value = "/list",produces = "application/json")
    public ResponseEntity<?> wishList(OAuth2Authentication oAuth2Authentication){
        if(oAuth2Authentication==null || oAuth2Authentication.getPrincipal()==null ){
            throw new ForbiddenException();
        }
        return cartAPIService.getListCartOf(oAuth2Authentication.getPrincipal().toString());
    }


    @Operation(
            summary = "User add cart",
            description = "create cart if not exist , add more qty if exists cart",
            security = {@SecurityRequirement(name = "bearerAuth")},
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(
                                    name="Body", implementation = CartRequestBody.class
                            ),
                            examples = {
                                    @ExampleObject(
                                            name = "add cart body example",
                                            summary = "add cart body example",
                                            description = "add cart body example",
                                            value = "{\n" +
                                                    "\t\"idProduct\":1,\n" +
                                                    "\t\"qty\":<*OPTIONAL default=1>\n" +
                                                    "}")
                            }
                    )

            ),
            responses = {
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(example =SwaggerExample.CART_ADD_1,implementation = CartResBody.class)
                            ),
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(example ="{\n" +
                                            "    \"code\": \"404\",\n" +
                                            "    \"message\": \"페이지를 찾을수 없습니다.\"\n" +
                                            "}")
                            ),
                            responseCode = "404"
                    ),
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,

                                    schema = @Schema(example ="{\n" +
                                            "    \"code\": \"403\",\n" +
                                            "    \"message\": \"접근권한이 없습니다.\"\n" +
                                            "}")
                            ),
                            description="oath error",
                            responseCode = "403"
                    )
            }

    )
    @PostMapping(value = "/add",produces = "application/json",consumes = "application/json")
    public ResponseEntity<?> addCart(@Valid @RequestBody CartRequestBody body, OAuth2Authentication oAuth2Authentication){
        if(oAuth2Authentication==null || oAuth2Authentication.getPrincipal()==null ){
            throw new ForbiddenException();
        }
        return cartAPIService.addCart(body, oAuth2Authentication.getPrincipal().toString());
    }



    @Operation(
            summary = "User update cart",
            description = "update qty of cart",
            security = {@SecurityRequirement(name = "bearerAuth")},
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(
                                    name="body", implementation = CartUpdateReqBody.class
                            ),
                            examples = {
                                    @ExampleObject(
                                            name = "update cart body example",
                                            summary = "update cart body example",
                                            description = "update cart body example",
                                            value = "{\n" +
                                                    "\t\"idCart\":2,\n" +
                                                    "\t\"qty\":6\n" +
                                                    "}")
                            }
                    )

            ),
            responses = {
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(example =SwaggerExample.CART_ADD_1,implementation = CartResBody.class)
                            ),
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(example ="{\n" +
                                            "    \"code\": \"404\",\n" +
                                            "    \"message\": \"not exist cart ::: <idCart>\"\n" +
                                            "}")
                            ),
                            responseCode = "404"
                    ),
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,

                                    schema = @Schema(example ="{\n" +
                                            "    \"code\": \"403\",\n" +
                                            "    \"message\": \"접근권한이 없습니다.\"\n" +
                                            "}")
                            ),
                            description="oath error",
                            responseCode = "403"
                    )
            }

    )
    @PostMapping(value = "/updateqty",produces = "application/json",consumes = "application/json")
    public ResponseEntity<?> updateQty(@Valid @RequestBody CartUpdateReqBody body, OAuth2Authentication oAuth2Authentication){
        if(oAuth2Authentication==null || oAuth2Authentication.getPrincipal()==null ){
            throw new ForbiddenException();
        }
        return cartAPIService.updateCartQty(body, oAuth2Authentication.getPrincipal().toString());
    }


    @Operation(
            summary = "Delete cart",
            description = "Delete cart",
            security = {@SecurityRequirement(name = "bearerAuth")},
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(
                                    name="body", implementation = CartDeleteReqBody.class
                            ),
                            examples = {
                                    @ExampleObject(
                                            name = "delete list cart",
                                            summary = "delete list cart",
                                            description = "delete cart example, idCarts is List<Long>",
                                            value = "{\n" +
                                                    "\t\"idCarts\":[6,7]\n" +
                                                    "}")
                            }
                    )

            ),
            responses = {
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(example ="{\n" +
                                            "    \"result\": \"success\",\n" +
                                            "    \"message\": \"success\"\n" +
                                            "}")
                            ),
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,

                                    schema = @Schema(example ="{\n" +
                                            "    \"code\": \"401\",\n" +
                                            "    \"message\": \"인증되지 않은 계정입니다.\"\n" +
                                            "}")
                            ),
                            description="some cartId not exists or user not have it, but still delete legit cartId until search cartId error",
                            responseCode = "401"
                    ),
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,

                                    schema = @Schema(example ="{\n" +
                                            "    \"code\": \"403\",\n" +
                                            "    \"message\": \"접근권한이 없습니다.\"\n" +
                                            "}")
                            ),
                            description="oath error",
                            responseCode = "403"
                    )
            }

    )
    @PostMapping(value = "/delete",produces = "application/json",consumes = "application/json")
    public ResponseEntity<?> deleteCarts(@Valid @RequestBody CartDeleteReqBody body, OAuth2Authentication oAuth2Authentication){
        if(oAuth2Authentication==null || oAuth2Authentication.getPrincipal()==null ){
            throw new ForbiddenException();
        }
        return cartAPIService.deleteList(body, oAuth2Authentication.getPrincipal().toString());
    }



    @Operation(
            summary = "Delete all cart",
            description = "Delete all cart",
            security = {@SecurityRequirement(name = "bearerAuth")},
            responses = {
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(example ="{\n" +
                                            "    \"result\": \"success\",\n" +
                                            "    \"message\": \"success\"\n" +
                                            "}")
                            ),
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,

                                    schema = @Schema(example ="{\n" +
                                            "    \"code\": \"403\",\n" +
                                            "    \"message\": \"접근권한이 없습니다.\"\n" +
                                            "}")
                            ),
                            description="oath error",
                            responseCode = "403"
                    )
            }

    )
    @GetMapping(value = "/deleteall",produces = "application/json")
    public ResponseEntity<?> deleteAll( OAuth2Authentication oAuth2Authentication){
        if(oAuth2Authentication==null || oAuth2Authentication.getPrincipal()==null ){
            throw new ForbiddenException();
        }
        return cartAPIService.deleteAll(oAuth2Authentication.getPrincipal().toString());
    }

    @Operation(
            summary = "Get total price, sales, product, ...",
            description = "Get total price, sales, product, ...",
            security = {@SecurityRequirement(name = "bearerAuth")},
            responses = {
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(example = SwaggerExample.CART_TOTAL_1,implementation = CartResTotalBody.class)
                            ),
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,

                                    schema = @Schema(example ="{\n" +
                                            "    \"code\": \"403\",\n" +
                                            "    \"message\": \"접근권한이 없습니다.\"\n" +
                                            "}")
                            ),
                            description="oath error",
                            responseCode = "403"
                    )
            }
    )
    @GetMapping(value = "/total",produces = "application/json")
    public ResponseEntity<?> getTotalPrice(OAuth2Authentication oAuth2Authentication){
        if(oAuth2Authentication==null || oAuth2Authentication.getPrincipal()==null ){
            throw new ForbiddenException();
        }
        return cartAPIService.getTotal(oAuth2Authentication.getPrincipal().toString());

    }


}
