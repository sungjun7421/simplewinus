package com.logisall.anyware.api.service.commerce.payment.iamport;


import com.logisall.anyware.core.config.language.Messages;
import com.logisall.anyware.core.config.property.commerce.iamport.IamPortConfig;
import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.commerce.PayMethod;
import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistory;
import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistoryRepository;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.domain.commerce.order.OrderRepository;
import com.logisall.anyware.core.domain.commerce.order.TransactionInfo;
import com.logisall.anyware.core.domain.commerce.point.Point;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.model.reqbody.commerce.payment.IamPortReadyReqBody;
import com.logisall.anyware.core.service.commerce.cart.CartService;
import com.logisall.anyware.core.service.commerce.couponhistory.CouponHistoryServicre;
import com.logisall.anyware.core.service.commerce.order.OrderService;
import com.logisall.anyware.core.service.commerce.point.PointService;
import com.logisall.anyware.core.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service
public class IamPortAPIServiceImpl implements IamPortAPIService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderService orderService;

    @Autowired
    private IamPortConfig iamPortConfig;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private PointService pointService;

    @Autowired
    private CouponHistoryRepository couponHistoryRepository;

    @Autowired
    private CartService cartService;

    @Autowired
    private CouponHistoryServicre couponHistoryServicre;

    @Autowired
    private Messages messages;


    @Override
    @Transactional
    public IamPortParams ready(IamPortReadyReqBody paymentReqBody, String email) {
        User user = userRepository.findByEmail(email).get();
        if (user==null) {
            throw new RuntimeException("User not found!");
        }


        String oid = paymentReqBody.getOid();
        Order order = orderRepository.findByOid(oid).get();

        if (order == null){
            throw new RuntimeException("Order not found!");
        }

        String pg = "html5_inicis";
        String pay_method = "card";
        BigDecimal _amount = order.getTotalPrice();

        if(paymentReqBody.getPayMethod().equals(PayMethod.KAKAO_PAY)){
            pg = "html5_inicis";
            pay_method = "kakaopay";
            _amount = order.getTotalPrice();
        }else if(paymentReqBody.getPayMethod().equals(PayMethod.TOSS)){
            pg = "html5_inicis";
            pay_method = "tosspay";
            _amount = order.getTotalPrice();
        }else if(paymentReqBody.getPayMethod().equals(PayMethod.PAY_PAL)){
            pg = "paypal";
            pay_method = "card";
            BigDecimal exchange = BigDecimal.valueOf(1130);
            _amount = order.getTotalPrice().divide(exchange, 2, BigDecimal.ROUND_HALF_UP);
        }

        //re-save payment method
        order.setPayMethod(paymentReqBody.getPayMethod());
        orderRepository.save(order);

        IamPortParams iamPortParam = new IamPortParams();
        iamPortParam.setPg(pg);
        iamPortParam.setPay_method(pay_method);
        iamPortParam.setMerchant_uid(order.getOid());
        iamPortParam.setName(order.getTitle());
        iamPortParam.setAmount(_amount);
        iamPortParam.setBuyer_email(email);
        iamPortParam.setBuyer_name(user.getFullName());
        iamPortParam.setBuyer_tel(user.getMobile());
        return iamPortParam;
    }

    @Override
    public OrderStatus complete(String imp_uid,String merchant_uid) {
        Order order = orderService.get(merchant_uid);
        Map<String, Object> paymentData = this.lookUpIamPort(this.authAccessToken(), imp_uid);

        if (paymentData == null) {
            order.setOrderStatus(OrderStatus.FAILED); // 실패
        } else {
            Map<String, Object> response = (Map<String, Object>) paymentData.get("response");
            log.info("response ::: {}", response);

            final String status = response.get("status").toString();
            if (status.equals("paid")) {
                if(order.getTransactionInfo() == null) {
                    order.setTransactionInfo(new TransactionInfo());
                }
                order.getTransactionInfo().setTxStatus(response.get("status") == null ? null : String.valueOf(response.get("status").toString()));
                order.getTransactionInfo().setTxId(response.get("apply_num") == null ? null : String.valueOf(response.get("apply_num").toString()));
                order.getTransactionInfo().setTxJson(JsonUtils.toJson(response));
                //point
                if(order.getFcPoint() != null && order.getFcPoint().compareTo(BigDecimal.ZERO) > 0) {
                    BigDecimal point = order.getFcPoint().multiply(BigDecimal.valueOf(-1));
                    pointService.decrease(point, Point.getInterTextByMessage(messages, "point.use"), order.getRelativeBuyer().getRelativeUser().getEmail(), order.getId());
                }


                //coupon
                List<CouponHistory> coupons = order.getUsedCoupons();
                if(coupons.size() > 0) {
                    for (CouponHistory couponHistory : coupons) {
                        couponHistory.setUsed(true);
                        couponHistory.setUsedDate(LocalDate.now());
                        couponHistoryServicre.update(couponHistory);

                        //couponHistoryRepository.save(couponHistory);
                    }
                }


                //order = orderService.success(order); // 카드 결제 성공
                order.setOrderStatus(OrderStatus.COMPLETE); // 결제완료
                order.setPaymentDate(LocalDateTime.now()); // 결제 시간
                order.setPaymentComplete(true); // 결제 성공


                log.info("success oid ::: {}", order.getOid());

                // Cart 삭제
                 cartService.deleteAll(order.getBuyerInfo().getEmail());
                log.info("DELETE CART");
                orderRepository.save(order);

                // 주문완료 티켓 생성
                //ticketService.generateTicket(order.getOid());
                /* END : 주문완료 문자보내기 */
            } else {
                log.error("Paypal order Id ::: {} | {}", order.getOid(), status);
                order.setOrderStatus(OrderStatus.FAILED); // 결제완료
                order.setPaymentDate(null); // 결제 시간
                order.setPaymentComplete(false); // 결제 성공

                if (order.getTransactionInfo() == null) {
                    order.setTransactionInfo(new TransactionInfo());
                }

                order.getTransactionInfo().setTxStatus(response.get("status") == null ? null : String.valueOf(response.get("status").toString()));
                order.getTransactionInfo().setTxId(response.get("apply_num") == null ? null : String.valueOf(response.get("apply_num").toString()));
                order.getTransactionInfo().setTxFailReason(response.get("fail_reason") == null ? null : String.valueOf(response.get("fail_reason").toString()));
                order.getTransactionInfo().setTxJson(JsonUtils.toJson(response));

//                order.getPurchaseHistories().forEach(purchaseHistory -> {
//                    purchaseHistory.setStatus(PurchaseHistory.Status.WAITING);
//                });
            }
        }


        return order.getOrderStatus();
    }
    private String authAccessToken() {
        log.debug("authAccessToken");
        try {
            final String key = iamPortConfig.getRest().getImp_key();
            final String secret = iamPortConfig.getRest().getImp_secret();

            RestTemplate restTemplate = new RestTemplate();

            String url = "https://api.iamport.kr/users/getToken";

            Map<String, String> payLoadMap = new HashMap<>();
            payLoadMap.put("imp_key", key);
            payLoadMap.put("imp_secret", secret);

            String requestJson = JsonUtils.toString(payLoadMap);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);
            ResponseEntity<HashMap> responseEntity = restTemplate.postForEntity(url, entity, HashMap.class);
            if (responseEntity == null || !responseEntity.getStatusCode().equals(HttpStatus.OK)) {
                throw new RuntimeException("I_AM_PORT_NOT_COMPLETE");
            }

            Map<String, Object> response = (Map<String, Object>) responseEntity.getBody().get("response");
            return response.get("access_token").toString();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("I_AM_PORT_NOT_COMPLETE");
        }
    }

    //
    private Map<String, Object> lookUpIamPort(final String accessToken, String imp_uid) {
        log.debug("lookUpIamPort ::: {}, {}", accessToken, imp_uid);
        try {
            RestTemplate restTemplate = new RestTemplate();

            String url = String.format("https://api.iamport.kr/payments/%s", imp_uid);

            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", accessToken);

            HttpEntity<String> request = new HttpEntity<>(headers);
            ResponseEntity<HashMap> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, HashMap.class);
            log.debug("responseEntity ::: {}", responseEntity);
            if (responseEntity == null || !Objects.equals(responseEntity.getStatusCode(), HttpStatus.OK)) {
                throw new RuntimeException("I_AM_PORT_NOT_COMPLETE");
            }
            return responseEntity.getBody();
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
            e.printStackTrace();
            throw new RuntimeException("I_AM_PORT_NOT_COMPLETE");
        }
    }
}
