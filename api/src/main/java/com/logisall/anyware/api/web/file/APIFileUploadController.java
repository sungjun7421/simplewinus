package com.logisall.anyware.api.web.file;

import com.logisall.anyware.core.model.file.FileUploadService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Tag(name = "File", description = "파일 업로드 API")
@RestController
@RequestMapping("/api/v1/files")
public class APIFileUploadController {

  @Autowired
  private FileUploadService fileUploadService;

  /**
   * [file-1] 파일 업로드
   */
  @Operation(summary = "[upload-file-1] 파일업로드")
  @PostMapping
  public ResponseEntity<?> file(@Parameter(description = "File") @RequestParam("file") MultipartFile file) {

    return ResponseEntity.ok(fileUploadService.uploadFile(file));
  }

  /**
   * [file-2] 이미지 업로드
   */
  @Operation(summary = "[upload-file-2] 이미지 파일 업로드", description = "", security = {@SecurityRequirement(name = "bearerAuth")},
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = "{\n" +
                  "    \"data\": \"[file]\"\n" +
                  "}")
          )))
  @PostMapping(value = "images", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> upload(@Parameter(description = "File") @RequestParam MultipartFile[] files,
                                  OAuth2Authentication oAuth2Authentication) {

//    if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
//      throw new ForbiddenException();
//    }
    return ResponseEntity.ok(fileUploadService.uploadImages(files));
  }


}
