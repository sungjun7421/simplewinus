package com.logisall.anyware.api.service.commerce.payment.iamport;

import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.model.reqbody.commerce.payment.IamPortReadyReqBody;

public interface IamPortAPIService {
    IamPortParams ready(IamPortReadyReqBody paymentReqBody, String email);
    OrderStatus complete(String imp_uid, String merchant_uid);
}
