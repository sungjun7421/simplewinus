package com.logisall.anyware.api.web.map;

import com.logisall.anyware.core.domain.embedd.GPS;
import com.logisall.anyware.core.service.map.google.MapGoogleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Tag(name = "Google Map API", description = "구글지도 API")
@RestController
@RequestMapping("/api/v1/google/map")
public class APIGoogleMapController {

  @Autowired
  private MapGoogleService mapGoogleService;

  @Operation(summary = "[google-map] 구글 주소 검색 return GPS",
      description = "구글 주소 검색 API",
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = "")
          ))
  )
  @Parameters(value = {
      @Parameter(in = ParameterIn.QUERY, name = "address", description = "주소", example = "서울특별시 마포구 독막로 9길 13")
  })
  @GetMapping
  public ResponseEntity<?> getGps(String address) throws InterruptedException, ExecutionException, IOException {

    Future<GPS> gps = mapGoogleService.getLocationInfo(address);

    if (gps.isDone()) {
      return ResponseEntity.ok(gps.get());
    } else {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
  }
}
