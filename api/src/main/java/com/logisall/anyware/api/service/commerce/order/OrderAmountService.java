package com.logisall.anyware.api.service.commerce.order;


import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.model.reqbody.commerce.order.PaymentInfo;
import com.logisall.anyware.core.model.reqbody.commerce.order.PaymentInfoReqBody;
import com.logisall.anyware.core.model.reqbody.commerce.product.ProductReqBody;

import java.math.BigDecimal;
import java.util.List;

public interface OrderAmountService {

  // 최종 결제금액 정보
  // PaymentPriceInfo paymentPriceInfo(String oid,
//                                    Long idCoupons, // 주문쿠폰
//                                    Double point);

  PaymentInfo paymentInfo(PaymentInfoReqBody paymentInfoReqBody);


  PaymentInfo createPayment(PaymentInfoReqBody paymentInfoReqBody);

//  @Deprecated
//  // 총합 - 쿠폰
//  BigDecimal usedCoupon(BigDecimal total);
//
//  @Deprecated
//  // 총합 - 포인트
//  BigDecimal usedPoint(BigDecimal total, int point, Buyer buyer);

  @Deprecated
  // 적립예정포인트
  BigDecimal savingPoint(BigDecimal amount, Buyer buyer);
  // (제품 판매가 X 수량) 의 총합
  BigDecimal totalByProductAndQty(List<ProductReqBody> products);
}
