package com.logisall.anyware.api.web;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Tag(name = "Process Env", description = "프로세스 환경 API")
@RestController
@RequestMapping("/process-env")
public class ProcessEnvController {

  @Value("${process.env.set}")
  private String processEnv;

  @GetMapping
  public ResponseEntity<?> getProfile(HttpServletRequest request) {
    log.info(request.getRemoteHost());
    return ResponseEntity.ok(processEnv);
  }
}
