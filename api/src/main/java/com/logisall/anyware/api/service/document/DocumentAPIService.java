package com.logisall.anyware.api.service.document;

import com.logisall.anyware.core.domain.document.Document;
import com.logisall.anyware.core.model.resbody.document.DocumentResBody;
import org.springframework.hateoas.EntityModel;

import javax.servlet.http.HttpServletRequest;

public interface DocumentAPIService {

    EntityModel<DocumentResBody> resource(java.util.Locale locale, HttpServletRequest request, Document.Type type);
}
