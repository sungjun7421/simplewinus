package com.logisall.anyware.api.web.account.sns;

import com.logisall.anyware.api.config.oauth.JwtUtils;
import com.logisall.anyware.api.service.account.sns.SNSAPIService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.UnAuthorizedException;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.domain.user.sns.SNSStatus;
import com.logisall.anyware.core.model.resbody.account.SNSResBody;
import com.logisall.anyware.core.model.sns.GooglePojo;
import com.logisall.anyware.core.model.sns.SNSReqBody;
import com.logisall.anyware.core.model.sns.SNSType;
import com.logisall.anyware.core.service.account.SNSGoogleAPIService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Tag(name = "Google Login", description = "구글 로그인 API")
@RestController
@RequestMapping("/api/v1/google-login")
public class APIGoogleLoginController {

  @Autowired
  private SNSGoogleAPIService snsGoogleAPIService;

  @Autowired
  private SNSAPIService snsAPIService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private JwtUtils jwtUtils;

  @Operation(summary = "[sns-google-1] 웹용 구글 연동")
  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> google(@RequestBody SNSReqBody requestBody) {

    if (requestBody == null) {
      throw new BadRequestException();
    }

    String code = requestBody.getCode();
    String redirectUri = requestBody.getRedirectUri();

    return googleAuth(snsAPIService.connectGoogle(code, redirectUri));
  }

  @Operation(summary = "[sns-google-2] 모바일 앱용 구글 연동")
  @PostMapping(value = "native", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> googleNative(@RequestBody GooglePojo google) {
    return this.googleAuth(google);
  }

  private ResponseEntity<?> googleAuth(GooglePojo google) {
    SNSStatus status = snsGoogleAPIService.getStatus(google);
    SNSResBody body = SNSResBody.of(true);

    log.debug("google ::: {}", google);
    log.debug("status ::: {} ", status);

    switch (status) {

      // AccessToken 발급
      case CONNECT:
      case LINKED:

        User user = userRepository.findBySocialIdGoogleId(google.getId()).orElseThrow(RuntimeException::new);
        if (user.isWithd()) {
          throw new UnAuthorizedException("이용할수 없는 계정입니다.");
        }
        body.setSnsType(SNSType.GOOGLE);
        body.setId(google.getId());
        body.setName(google.getName());
        body.setEmail(google.getEmail());
        body.setAccess_token(jwtUtils.generator(user));
        return ResponseEntity.ok(body);

      case LEAVED_ACCOUNT:

        body = SNSResBody.of(false);
        body.setMessage(SNSStatus.LEAVED_ACCOUNT.name());
        body.setSnsType(SNSType.GOOGLE);
        body.setId(google.getId());
        body.setName(google.getName());
        body.setEmail(google.getEmail());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);

      case NOT_MATCH_SNS:

        body = SNSResBody.of(false);
        body.setMessage(SNSStatus.NOT_MATCH_SNS.name());
        body.setSnsType(SNSType.GOOGLE);
        body.setId(google.getId());
        body.setName(google.getName());
        body.setEmail(google.getEmail());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);

      case NOT_EXISTED_ACCOUNT:

        body = SNSResBody.of(false);
        body.setMessage(SNSStatus.NOT_EXISTED_ACCOUNT.name());
        body.setSnsType(SNSType.GOOGLE);
        body.setId(google.getId());
        body.setName(google.getName());
        body.setEmail(google.getEmail());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);

      case NOT_PROVIDED_EMAIL:

        body = SNSResBody.of(false);
        body.setMessage(SNSStatus.NOT_PROVIDED_EMAIL.name());
        body.setSnsType(SNSType.GOOGLE);
        body.setId(google.getId());
        body.setName(google.getName());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);

      default:
        throw new BadRequestException("구글 로그인에서 나올수 없는 경우");
    }
  }
}
