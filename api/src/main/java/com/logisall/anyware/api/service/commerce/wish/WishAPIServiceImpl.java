package com.logisall.anyware.api.service.commerce.wish;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.NotFoundException;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.buyer.BuyerRepository;
import com.logisall.anyware.core.domain.commerce.product.Product;
import com.logisall.anyware.core.domain.commerce.product.ProductRepository;
import com.logisall.anyware.core.domain.commerce.wish.Wish;
import com.logisall.anyware.core.domain.commerce.wish.WishRepository;
import com.logisall.anyware.core.model.resbody.ResultResBody;
import com.logisall.anyware.core.service.commerce.buyer.BuyerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class WishAPIServiceImpl implements WishAPIService {
    @Autowired
    BuyerService buyerService;

    @Autowired
    BuyerRepository buyerRepository;

    @Autowired
    WishRepository wishRepository;

    @Autowired
    ProductRepository productRepository;

    @Override
    public ResponseEntity<?> getListWishOf(String email) {
        Buyer buyer=buyerService.get(email);
//        Buyer buyer=buyerRepository.findByRelativeUser_Email(oAuth2Authentication.getPrincipal().toString());
        List<Product> list=new ArrayList<>();
        for(Wish w:buyer.getWishList()){
            list.add(w.getRelativeProduct());
        }
        return ResponseEntity.ok(list);
    }
    @Override
    public ResponseEntity<?> getListWishOf(Long userId) {
        Buyer buyer=buyerService.get(userId);
        if(buyer==null){
            throw new NotFoundException();
        }
//        Buyer buyer=buyerRepository.findByRelativeUser_Email(oAuth2Authentication.getPrincipal().toString());
        List<Product> list=new ArrayList<>();
        for(Wish w:buyer.getWishList()){
            list.add(w.getRelativeProduct());
        }
        return ResponseEntity.ok(list);
    }

    @Transactional
    @Modifying
    @Override
    public ResponseEntity<?> toggleProduct(Long idProduct,Long idUser){
        if(productRepository.existsById(idProduct)){
            String mess="wish";
            Wish wishExist=wishRepository.findById_IdBuyerAndId_IdProduct(idUser,idProduct);
            if(wishExist!=null){
                mess="unwish";
//                Wish.Id id = new Wish.Id(wishExist.getRelativeBuyer().getId(), wishExist.getRelativeProduct().getId());
                wishRepository.deleteCustom(idUser,idProduct);
            }else{
                Product show = productRepository.findById(idProduct).orElseThrow(BadRequestException::new);
                Buyer buyer = buyerRepository.findByRelativeUser_Id(idUser);
                Wish wish=new Wish(buyer,show);
                if(wishRepository.save(wish)==null){
                    throw new RuntimeException();
                }
            }
            return ResponseEntity.ok(ResultResBody.of(true,mess));
        }else{
            return ResponseEntity.ok(ResultResBody.of(false,"Product not exists"));
        }

    }

    @Override
    @Modifying
    @Transactional
    public void delete(Wish.Id id) {
        wishRepository.deleteById(id);
    }

    @Transactional
    @Modifying
    @Override
    public ResponseEntity<?> toggleProduct(Long idProduct,String emailUser){
        Buyer buyer=buyerService.get(emailUser);
        if(buyer==null){
            return ResponseEntity.ok(ResultResBody.of(false,"User not exists"));
        }else{
            return toggleProduct(idProduct,buyer.getRelativeUser().getId());
        }
    }
}
