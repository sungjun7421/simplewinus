package com.logisall.anyware.api.config.oauth;

import com.logisall.anyware.core.domain.user.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.security.KeyPair;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;

@Component
public class JwtUtils {

  // 토큰 만료일 50일 후
  public Date getExpireDate() {
    return new Date(LocalDateTime.now().plusDays(50).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
  }

  @Value("${oauth.password.client.id}")
  private String clientID; // 공개키

  @Autowired
  private KeyPair keyPair;

  public String generator(User user) {

    return Jwts.builder()
      .setHeaderParam("typ", "JWT")
      .setExpiration(getExpireDate())
      .setId(UUID.randomUUID().toString()) // 고유값
      .claim("client_id", clientID)
      .claim("user_name", user.getEmail())
      .claim("authorities", user.getRoles())
      .claim("scope", Collections.singletonList("read"))
      .signWith(keyPair.getPrivate(), SignatureAlgorithm.RS256)
      .compact();
  }

}
