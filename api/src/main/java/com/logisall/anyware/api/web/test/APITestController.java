package com.logisall.anyware.api.web.test;

import com.logisall.anyware.core.model.resbody.board.CommentResBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Locale;

@Slf4j
@RestController
@RequestMapping("/api/v1/test")
public class APITestController {

  @GetMapping
  public ResponseEntity<?> test(@RequestParam String value,
                                Locale locale) {

    CommentResBody body = new CommentResBody();
    body.setContent("content");
    body.setDelete(false);
    body.setLocale(locale);
    body.setId(10L);
    body.setRegTime(LocalDateTime.now().minusMonths(4));
    body.setProfileImage("");
    body.setWriter("chonglye");

    return ResponseEntity.ok(body);
  }


}
