package com.logisall.anyware.api.web.board;

import com.logisall.anyware.api.service.board.faq.FaqAPIService;
import com.logisall.anyware.api.service.board.faq.FaqCategoryAPIService;
import com.logisall.anyware.core.model.Filter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Tag(name = "FAQ", description = "자주하는 질문 API")
@Slf4j
@RestController
@RequestMapping("/api/v1")
public class APIFaqController {

  @Autowired
  private FaqAPIService FaqAPIService;

  @Autowired
  private FaqCategoryAPIService faqCategoryAPIService;


  @Operation(summary = "[faq-1] 자주하는 질문 페이징")
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code 언어코드", example = "ko-KR"),

      @Parameter(in = ParameterIn.QUERY, name = "size", description = "Page Size 페이지 크기 (default : 20)", example = "20"),
      @Parameter(in = ParameterIn.QUERY, name = "page", description = "현재 페이지 0부터 (Current Page)  현재 페이지 (default : 0)", example = "0"),
      @Parameter(in = ParameterIn.QUERY, name = "sort", description = "Sort Page 정렬", example = "createdDate,desc"),
  })

  @GetMapping(value = "/faqs", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> getNoticeAll(@Parameter(hidden = true) @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                                        @Parameter(description = "검색어 (Search Word)") @RequestParam(required = false, defaultValue = "") String query,
                                        @Parameter(description = "시작시간 (Start Time) (yyyy-MM-dd HH:mm:ss)") @RequestParam(required = false) String startDate,
                                        @Parameter(description = "종료시간 (End Time) (yyyy-MM-dd HH:mm:ss)") @RequestParam(required = false) String endDate,
                                        @Parameter(description = "카테고리 ID(Category ID)") @RequestParam(required = false) Long[] idCategories,
                                        HttpServletRequest request,
                                        Locale locale) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    return ResponseEntity.ok(FaqAPIService.pagedResources(locale, request, filter, idCategories));
  }

  @Operation(summary = "[faq-2] FAQ 카테고리")
  @GetMapping(value = "/faqs/categories", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> categories(Locale locale) {

    return ResponseEntity.ok(faqCategoryAPIService.categories(locale));
  }
}
