package com.logisall.anyware.api.service.commerce.cart;

import com.logisall.anyware.core.config.exception.NotFoundException;
import com.logisall.anyware.core.config.exception.crud.CreateErrorException;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.buyer.BuyerRepository;
import com.logisall.anyware.core.domain.commerce.cart.Cart;
import com.logisall.anyware.core.domain.commerce.cart.CartRepository;
import com.logisall.anyware.core.domain.commerce.product.Product;
import com.logisall.anyware.core.domain.commerce.product.ProductRepository;
import com.logisall.anyware.core.model.reqbody.commerce.cart.CartDeleteReqBody;
import com.logisall.anyware.core.model.reqbody.commerce.cart.CartRequestBody;
import com.logisall.anyware.core.model.reqbody.commerce.cart.CartUpdateReqBody;
import com.logisall.anyware.core.model.resbody.ResultResBody;
import com.logisall.anyware.core.model.resbody.commerce.cart.CartResBody;
import com.logisall.anyware.core.model.resbody.commerce.cart.CartResTotalBody;
import com.logisall.anyware.core.service.commerce.buyer.BuyerService;
import com.logisall.anyware.core.service.commerce.cart.CartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class CartAPIServiceImpl implements CartAPIService {
  @Autowired
  BuyerService buyerService;

  @Autowired
  BuyerRepository buyerRepository;

  @Autowired
  ProductRepository productRepository;

  @Autowired
  CartService cartService;

  @Autowired
  CartRepository cartRepository;

  @Override
  public ResponseEntity<?> getListCartOf(String email) {
    Buyer buyer = buyerService.get(email);
//        Buyer buyer=buyerRepository.findByRelativeUser_Email(oAuth2Authentication.getPrincipal().toString());
    List<CartResBody> list = new ArrayList<>();
    for (Cart c : buyer.getCartList()) {
      CartResBody _res = new CartResBody();
      _res.setIdCart(c.getId());
      _res.setProduct(c.getRelativeProduct());
      _res.setQty(c.getBuyCount() != null ? c.getBuyCount() : 0);
      list.add(_res);
    }
    return ResponseEntity.ok(list);
  }

  @Override
  public ResponseEntity<?> addCart(CartRequestBody body, String email) {
    Product p = productRepository.findById(body.getIdProduct()).orElse(null);
    if (p == null) {
      throw new NotFoundException();
    }
    if (body.getQty() == null || body.getQty() <= 0) body.setQty(1);
    Cart _cart = cartService.get(email, body.getIdProduct());

    if (_cart == null) {
      _cart = cartService.add(body, email);
      if (_cart == null) throw new CreateErrorException("Cart");

    } else {
      _cart.setBuyCount(_cart.getBuyCount() + body.getQty());
      cartService.update(_cart);
    }
    CartResBody resBody = new CartResBody();
    resBody.setQty(_cart.getBuyCount());
    resBody.setProduct(_cart.getRelativeProduct());
    resBody.setIdCart(_cart.getId());
    return ResponseEntity.ok(resBody);
  }

  @Override
  public ResponseEntity<?> updateCartQty(CartUpdateReqBody body, String email) {
    Cart cart = cartService.update(body, email);
    CartResBody resBody = new CartResBody();
    resBody.setQty(cart.getBuyCount());
    resBody.setProduct(cart.getRelativeProduct());
    resBody.setIdCart(cart.getId());
    return ResponseEntity.ok(resBody);
  }

  @Override
  public ResponseEntity<?> deleteList(CartDeleteReqBody body, String email) {
    cartService.deleteByCarts(body.getIdCarts(), email);
    return ResponseEntity.ok(ResultResBody.of(true, "success"));
  }

  @Override
  public ResponseEntity<?> deleteAll(String email) {
    cartService.deleteAll(email);
    return ResponseEntity.ok(ResultResBody.of(true, "success"));
  }

  @Override
  public ResponseEntity<?> getTotal(String email) {
    Buyer buyer = buyerService.get(email);
    Double totalOrigin = 0d;
    Double totalSales = 0d;
    Integer totalProduct = buyer.getCartList().size();
    Integer totalQty = 0;
    for (Cart c : buyer.getCartList()) {
      totalOrigin += c.getRelativeProduct().getPrice().doubleValue() * c.getBuyCount();
      totalSales += c.getRelativeProduct().getSalePrice().doubleValue() * c.getBuyCount();
      totalQty += c.getBuyCount();
    }
    CartResTotalBody resBody = new CartResTotalBody();
    resBody.setTotalOriginPrice(totalOrigin);
    resBody.setTotalSalePrice(totalSales);
    resBody.setTotalProduct(totalProduct);
    resBody.setTotalQty(totalQty);
    return ResponseEntity.ok(resBody);
  }

}
