package com.logisall.anyware.api.service.commerce.payment.kakaopay;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.ResponseErrorCode;
import com.logisall.anyware.core.config.property.commerce.kakaopay.KakaoPayConfig;
import com.logisall.anyware.core.config.property.sns.KakaoApiKey;
import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.buyer.BuyerRepository;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.domain.commerce.order.OrderRepository;
import com.logisall.anyware.core.domain.commerce.paymentproduct.PaymentProduct;
import com.logisall.anyware.core.domain.commerce.paymentproduct.PaymentProductRepository;
import com.logisall.anyware.core.domain.commerce.product.Product;
import com.logisall.anyware.core.domain.commerce.product.ProductRepository;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.model.payment.kakaopay.KakaoPayApproveReqBody;
import com.logisall.anyware.core.model.payment.kakaopay.KakaoPayOrder;
import com.logisall.anyware.core.model.payment.kakaopay.KakaoPayReadyReqBody;
import com.logisall.anyware.core.model.reqbody.commerce.product.ProductReqBody;
import com.logisall.anyware.core.service.commerce.order.OrderService;
import com.logisall.anyware.core.utils.JsonUtils;
import com.logisall.anyware.core.utils.OkHttpUtils;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@Service
public class KakaoPayAPIServiceImpl implements KakaoPayAPIService {

  @Autowired
  private KakaoPayConfig config;

  @Autowired
  private KakaoApiKey apiKey;

  @Autowired
  private OkHttpClient client;

  @Autowired
  private ProductRepository productRepository;

  @Autowired
  private BuyerRepository buyerRepository;

  @Autowired
  private OrderService orderService;

  @Autowired
  private OrderRepository orderRepository;

  @Autowired
  private PaymentProductRepository paymentProductRepository;

  private Map<String, Object> ready(KakaoPayReadyReqBody reqBody) {

    try {

      String url = config.getReadyUri();

      log.debug("url ::: {}", url);

      // RequestBody
      FormBody formBody = reqBody.toFormBody();
      log.debug("formBody :: {}", formBody);
      // --data-urlencode 'cid=TC0ONETIME' \
      // --data-urlencode 'partner_order_id=partner_order_id' \
      // --data-urlencode 'partner_user_id=partner_user_id' \
      // --data-urlencode 'item_name=초코파이' \
      // --data-urlencode 'quantity=1' \
      // --data-urlencode 'total_amount=2200' \
      // --data-urlencode 'vat_amount=200' \
      // --data-urlencode 'tax_free_amount=0' \
      // --data-urlencode 'approval_url=https://developers.kakao.com/success' \
      // --data-urlencode 'fail_url=https://developers.kakao.com/fail' \
      // --data-urlencode 'cancel_url=https://developers.kakao.com/cancel'

      Request request = new Request.Builder()
          .addHeader("Authorization", String.format("KakaoAK %s", apiKey.getAdminKey()))
          .post(formBody)
          .url(url)
          .build();

      Response response = client.newCall(request).execute();

      if (response.isSuccessful()) {
        Map<String, Object> resBody = OkHttpUtils.toMap(response.body());
        log.debug("KakaoPay ready resBody ::: {}", resBody);
        // {
        // "tid": "T1234567890123456789",
        // "next_redirect_app_url": "https://mockup-pg-web.kakao.com/v1/xxxxxxxxxx/aInfo",
        // "next_redirect_mobile_url": "https://mockup-pg-web.kakao.com/v1/xxxxxxxxxx/mInfo",
        // "next_redirect_pc_url": "https://mockup-pg-web.kakao.com/v1/xxxxxxxxxx/info",
        // "android_app_scheme": "kakaotalk://kakaopay/pg?url=https://mockup-pg-web.kakao.com/v1/xxxxxxxxxx/order",
        // "ios_app_scheme": "kakaotalk://kakaopay/pg?url=https://mockup-pg-web.kakao.com/v1/xxxxxxxxxx/order",
        // "created_at": "2016-11-15T21:18:22"
        //}
        return resBody;
      }

      log.error("KakaoPay ready response ::: code -> {}, msg -> {}", response.code(), response.message());
      if (response.body() != null) {
        Map<String, Object> resBody = OkHttpUtils.toMap(response.body());
        log.error("KakaoPay ready response body ::: {}", resBody);
      }

    } catch (Exception e) {
      e.printStackTrace();
    }

    throw new RuntimeException("KakaoPay Ready API ERROR.");
  }

  private Response approve(FormBody formBody) {

    try {

      String url = config.getApproveUri();
      log.debug("url ::: {}", url);

      // RequestBody
      log.debug("formBody :: {}", formBody);
      // --data-urlencode 'cid=TC0ONETIME' \
      // --data-urlencode 'tid=T1234567890123456789' \
      // --data-urlencode 'partner_order_id=partner_order_id' \
      // --data-urlencode 'partner_user_id=partner_user_id' \
      // --data-urlencode 'pg_token=xxxxxxxxxxxxxxxxxxxx'

      Request request = new Request.Builder()
          .addHeader("Authorization", String.format("KakaoAK %s", apiKey.getAdminKey()))
          .post(formBody)
          .url(url)
          .build();

      return client.newCall(request).execute();

    } catch (Exception e) {
      e.printStackTrace();
    }

    throw new RuntimeException("KakaoPay Approve API ERROR.");
  }

  private Map<String, Object> cancel(String tid, String cancel_amount) {

    try {

      String url = config.getCancelUri();

      log.debug("url ::: {}", url);

      // RequestBody
      FormBody formBody = new FormBody.Builder(Charset.forName("UTF-8"))
          .add("cid", apiKey.getClientId())
          .add("tid", tid)
          .add("cancel_amount", cancel_amount)
          .add("cancel_tax_free_amount", "0")
          .build();
      log.debug("formBody :: {}", formBody);

      Request request = new Request.Builder()
          .addHeader("Authorization", String.format("KakaoAK %s", apiKey.getAdminKey()))
          .post(formBody)
          .url(url)
          .build();

      Response response = client.newCall(request).execute();

      if (response.isSuccessful()) {
        Map<String, Object> resBody = OkHttpUtils.toMap(response.body());
        log.debug("KakaoPay cancel resBody ::: {}", resBody);
        return resBody;
      }

      log.error("KakaoPay cancel response ::: code -> {}, msg -> {}", response.code(), response.message());
      if (response.body() != null) {
        Map<String, Object> resBody = OkHttpUtils.toMap(response.body());
        log.error("KakaoPay cancel response body ::: {}", resBody);
        return resBody;
      }

    } catch (Exception e) {
      e.printStackTrace();
    }

    throw new RuntimeException("KakaoPay Cancel API ERROR.");
  }

  @Override
  @Transactional
  public Map<String, Object> ready(KakaoPayOrder kpOrder, String email) {

    // 구매자
    Buyer buyer = buyerRepository.findByRelativeUser_Email(email);
    User user = buyer.getRelativeUser();

    if (!user.getUserDetailsMeta().isValid()) {
      throw new BadRequestException();
    }

    Order order = kpOrder.toOrder();
    order.setRelativeBuyer(buyer);

    // 주문번호 생성
    final String oid = orderService.generateOid();
    order.setOid(oid);

    // 결제 상품 생성
    BigDecimal _amount = BigDecimal.ZERO; // DB 데이터(티켓) 에서의 티켓 합샨금액
    List<Product> products = new ArrayList<>();

    for (ProductReqBody productReqBody : order.getProducts()) {

      final Product product = productRepository.findById(productReqBody.getId()).orElseThrow(() -> new BadRequestException("결제할 상품이 존재하지 않습니다."));
      product.setLocale(Locale.KOREA); // 다국어 설정
      products.add(product);

      // TODO 옵션 계산
      // productReqBody.getIdOption();

      // TODO 상품별 가격 계산
      //

      _amount = _amount.add(product.getSalePrice()
          .multiply(BigDecimal.valueOf(productReqBody.getQty())));
    }

    // 결제금액확인
    BigDecimal amount = BigDecimal.valueOf(kpOrder.getAmount());
    if (!Objects.equals(amount, _amount)) {
      log.error("PAYMENT_AMOUNT_DISCORD ::: {}, {}", amount, _amount);
      throw new BadRequestException(ResponseErrorCode.PAYMENT_AMOUNT_DISCORD.name());
    }

    order.setAmount(amount);

    // 주문 명
    String title = products.get(0).getInformation().getName().getValue();
    if (products.size() > 1) {
      title = title + " 외 " + (products.size() - 1) + "개";
    }

    order.setTitle(title);

    //== KakaoPay 에서 주문서 생성
    Map<String, Object> resBody = this.ready(kpOrder.toKakaoPayReadyReqBody(apiKey.getClientId(), oid, email, title));

    // Transaction 정보 입력
    order.getTransactionInfo().setTxStatus("OK");
    order.getTransactionInfo().setTxId(resBody.get("tid").toString());
    order.getTransactionInfo().setTxJson(JsonUtils.toJson(resBody));

    Order createdOrder = orderRepository.save(order);

    //== 주문 내역
    products.forEach(product -> {

      PaymentProduct history = new PaymentProduct(createdOrder, product);

      history.setPrice(product.getPrice());
      history.setDc(product.getDiscount());
      history.setQty(product.getProductReqBody().getQty());
      PaymentProduct savedHistory = paymentProductRepository.save(history);

      log.debug("savedHistory ::: {}", savedHistory);
    });

    return resBody;
  }

  @Override
  @Transactional
  public ResponseEntity<?> approve(KakaoPayApproveReqBody reqBody, String email) {

    final String tid = reqBody.getTid();

    List<Order> orders = orderRepository.findByTransactionInfo_TxId(tid);
    if (orders == null && orders.size() == 0) {
      throw new BadRequestException(ResponseErrorCode.NON_EXIST_ORDER_TEMP.name());
    }

    final Order order = orders.get(0);

    if (!order.getRelativeBuyer().getRelativeUser().getEmail().equals(email)) {
      throw new BadRequestException("사용자 정보가 일치하지 않습니다.");
    }

    Response response = this.approve(reqBody.toFormBody(apiKey.getClientId(), order.getOid(), email));

    if (!response.isSuccessful()) {
      try {

        log.error("KakaoPay approve response ::: code -> {}, msg -> {}", response.code(), response.message());

        if (String.valueOf(response.code()).startsWith("40")) {
          Map<String, Object> resBody = OkHttpUtils.toMap(response.body());
          String code = resBody.get("code").toString(); // Request 고유 번호
          String msg = resBody.get("msg").toString(); // 결제 고유 번호

          order.setPaymentComplete(false); // 실패
          order.setOrderStatus(OrderStatus.FAILED);
          order.getTransactionInfo().setTxStatus(code);
          order.getTransactionInfo().setTxFailReason(msg);
          order.getTransactionInfo().setTxJson(JsonUtils.toJson(resBody));
          orderRepository.save(order);

          log.error("{} PAYMENT_ERROR ::: {}", order.getId(), msg);
          return ResponseEntity.badRequest().body(resBody);
        }

        String bodyStr = null;
        if (response.body() != null) {
          bodyStr = response.body().string();
          log.error("KakaoPay approve response body ::: {}", bodyStr);
        }

        String failReason = String.format("(%s) 카카오페이 결제 승인 API 호출에서 문제가 발생하였습니다.", response.code());
        order.setPaymentComplete(false); // 실패
        order.setOrderStatus(OrderStatus.FAILED);
        order.getTransactionInfo().setTxStatus(response.message());
        order.getTransactionInfo().setTxFailReason(failReason);
        order.getTransactionInfo().setTxJson(bodyStr);
        orderRepository.save(order);

        log.error("{} PAYMENT_ERROR ::: {}", order.getId(), failReason);
        return ResponseEntity.badRequest().body(bodyStr);
      } catch (IOException e) {
        e.printStackTrace();
        throw new RuntimeException(e);
      }
    } else {

      Map<String, Object> resBody = OkHttpUtils.toMap(response.body());
      log.debug("KakaoPay approve resBody ::: {}", resBody);

      Map amountMap = (Map) resBody.get("amount"); // 결제 금액 정보

      BigDecimal amount = new BigDecimal(amountMap.get("total").toString());

      if (!Objects.equals(amount, order.getAmount())) {
        String failReason = ResponseErrorCode.PAYMENT_AMOUNT_DISCORD.getMessage();
        order.setPaymentComplete(false); // 실패
        order.setOrderStatus(OrderStatus.FAILED); // 가격 불일치
        order.getTransactionInfo().setTxStatus("가격불일치 : " + response.message());
        order.getTransactionInfo().setTxFailReason(failReason);
        order.getTransactionInfo().setTxJson(JsonUtils.toJson(resBody));
        orderRepository.save(order);

        log.error("{} PAYMENT_ERROR ::: {}", order.getId(), failReason);
        this.cancel(tid, amount.toString());
        return ResponseEntity.badRequest().body(ResponseErrorCode.PAYMENT_AMOUNT_DISCORD.name());
      } else {

        final LocalDateTime now = LocalDateTime.now();

        //== 정상 결제완료 DB 처리
        order.setOrderStatus(OrderStatus.COMPLETE);
        order.setPaymentComplete(true);
        order.setPaymentDate(now);
        order.getTransactionInfo().setTxStatus(response.message());
        order.getTransactionInfo().setTxJson(JsonUtils.toJson(resBody));
        Order updated = orderRepository.save(order);

        // 주문완료 메일 발송
        if (!StringUtils.isEmpty(email)) {
          final String fullName = order.getBuyerInfo().getFullName();
//          shopEmailService.orderComplete(email, fullname, order);
        }

        resBody.put("oid", updated.getOid());
        return ResponseEntity.ok(resBody);
      }
    }
  }
}
