package com.logisall.anyware.api.web.account.sns;

import com.logisall.anyware.api.config.oauth.JwtUtils;
import com.logisall.anyware.api.service.account.sns.SNSAPIService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.domain.user.sns.SNSStatus;
import com.logisall.anyware.core.model.resbody.account.SNSResBody;
import com.logisall.anyware.core.model.sns.Naver;
import com.logisall.anyware.core.model.sns.SNSReqBody;
import com.logisall.anyware.core.model.sns.SNSType;
import com.logisall.anyware.core.service.account.SNSNaverAPIService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Tag(name = "Naver Login", description = "네이버 로그인 API")
@RestController
@RequestMapping("/api/v1/naver-login")
public class APINaverLoginController {

  @Autowired
  private SNSNaverAPIService snsNaverAPIService;

  @Autowired
  private SNSAPIService snsAPIService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private JwtUtils jwtUtils;

  @Operation(summary = "[sns-naver-1] 웹용 네이버 연동")
  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> naver(@RequestBody SNSReqBody requestBody) {

    if (requestBody == null) {
      throw new BadRequestException();
    }

    String code = requestBody.getCode();
    String redirectUri = requestBody.getRedirectUri();
    String state = requestBody.getState();

    return naverAuth(snsAPIService.connectNaver(code, redirectUri, state));
  }

  @Operation(summary = "[sns-naver-2] 모바일 앱용 네이버 연동")
  @PostMapping(value = "native", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> naverNative(@RequestBody Naver naver) {
    return this.naverAuth(naver);
  }

  private ResponseEntity<?> naverAuth(Naver naver) {

    SNSStatus status = snsNaverAPIService.getStatus(naver);
    SNSResBody body = SNSResBody.of(true);

    log.debug("naver ::: {}", naver);
    log.debug("status ::: {} ", status);

    switch (status) {

      // AccessToken 발급
      case CONNECT:
      case LINKED:

        body.setSnsType(SNSType.NAVER);
        body.setId(naver.getId());
        body.setName(naver.getName());
        body.setEmail(naver.getEmail());
        body.setAccess_token(jwtUtils.generator(userRepository.findBySocialIdNaverId(naver.getId()).orElseThrow(RuntimeException::new)));
        return ResponseEntity.ok(body);

      case LEAVED_ACCOUNT:

        body = SNSResBody.of(false);
        body.setMessage(SNSStatus.LEAVED_ACCOUNT.name());
        body.setSnsType(SNSType.NAVER);
        body.setId(naver.getId());
        body.setName(naver.getName());
        body.setEmail(naver.getEmail());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);

      case NOT_MATCH_SNS:

        body = SNSResBody.of(false);
        body.setMessage(SNSStatus.NOT_MATCH_SNS.name());
        body.setSnsType(SNSType.NAVER);
        body.setId(naver.getId());
        body.setName(naver.getName());
        body.setEmail(naver.getEmail());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);

      case NOT_EXISTED_ACCOUNT:

        body = SNSResBody.of(false);
        body.setMessage(SNSStatus.NOT_EXISTED_ACCOUNT.name());
        body.setSnsType(SNSType.NAVER);
        body.setId(naver.getId());
        body.setName(naver.getName());
        body.setEmail(naver.getEmail());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);

      case NOT_PROVIDED_EMAIL:

        body = SNSResBody.of(false);
        body.setMessage(SNSStatus.NOT_PROVIDED_EMAIL.name());
        body.setSnsType(SNSType.NAVER);
        body.setId(naver.getId());
        body.setName(naver.getName());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);

      default:
        throw new BadRequestException("네이버 로그인에서 나올수 없는 경우");
    }
  }
}
