package com.logisall.anyware.api.service.commerce.point;

import com.logisall.anyware.core.domain.commerce.point.Point;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.resbody.commerce.point.PointOfMemberResBody;
import com.logisall.anyware.core.model.resbody.commerce.point.PointResBody;
import org.springframework.hateoas.PagedModel;

import java.util.Locale;

public interface PointAPIService {

  // Rest
  PagedModel<PointResBody> pagedResources(Locale locale, Filter filter, Point.Status status, String email);
  PointOfMemberResBody getDetailPoint(Long buyerId);
}
