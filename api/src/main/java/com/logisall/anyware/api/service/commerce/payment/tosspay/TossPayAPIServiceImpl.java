package com.logisall.anyware.api.service.commerce.payment.tosspay;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.property.commerce.tosspay.TossConfig;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.domain.commerce.order.OrderRepository;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.model.payment.toss.*;
import com.logisall.anyware.core.service.commerce.order.OrderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Objects;

@Slf4j
@Service
public class TossPayAPIServiceImpl implements TossPayAPIService {

  @Autowired
  private TossConfig tossConfig;

  @Autowired
  private OrderService orderService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private OrderRepository orderRepository;


  @Override
  @Transactional
  public TossResponse payment(Toss toss, String email) {

    return null;
//    if (toss.getPayMethod() == null) {
//      throw new BadRequestException();
//    }
//
//    if (org.springframework.util.StringUtils.isEmpty(email)) {
//      throw new UnAuthorizedException();
//    }
//    User user = userRepository.findByEmail(email).orElseThrow(() -> new BadRequestException("존재하지 않은 회원입니다."));
//
//    if (user == null) {
//      throw new UnAuthorizedException();
//    }
//
//    log.info("toss ::: {}", toss);
//
//    Order savedOrder = orderService.order(toss.toOrder(), email);
//
//    TossBody tossBody = toss.toBody(savedOrder.getOid(), apiKey);
//    if (StringUtils.isEmpty(tossBody.getRetAppScheme())) {
//      tossBody.setResultCallback("https://api.fooddeuk.com/api/v1/toss/paystatus?oid=" + savedOrder.getOid());
//    }
//    TossResponse tossResponse = this.callPayment(tossBody);
//    tossResponse.setOid(savedOrder.getOid());
//    String payToken = tossResponse.getPayToken();
//
//    TossApproval tossApproval = new TossApproval();
//    tossApproval.setPayToken(payToken);
//    savedOrder.setRelativeTossApproval(tossApproval);
//    tossApproval.setRelativeOrder(savedOrder);
//
//    log.info("tossResponse ::: {}", tossResponse);
//
//    if (tossResponse.getCode() == 0) {
//      savedOrder.setOrderStatus(OrderStatus.INIT);
//    } else {
//      savedOrder.setOrderStatus(OrderStatus.FAILED);
//    }
//    return tossResponse;
  }

  private TossResponse callPayment(TossBody tossBody) {

    RestTemplate restTemplate = new RestTemplate();

    String body;
    try {
      body = new ObjectMapper().writeValueAsString(tossBody);
      log.debug("request body ::: {}", body);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    URI uri = UriComponentsBuilder.fromHttpUrl("https://pay.toss.im/api/v1/payments").build().toUri();

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));

    HttpEntity entity = new HttpEntity(body, headers);

    ResponseEntity<TossResponse> responseEntity = restTemplate.postForEntity(
        uri,
        entity,
        TossResponse.class);

    log.debug("response entity ::: {}", responseEntity);
    log.debug("response body ::: {}", responseEntity.getBody());
    if (!Objects.equals(responseEntity.getStatusCode(), HttpStatus.OK)) {
      log.error("response body ::: {}", responseEntity.getBody());
      throw new RuntimeException(responseEntity.toString());
    }

    TossResponse tossResponse = responseEntity.getBody();
    return tossResponse;
  }

  @Override
  @Transactional
  public void resultCallBack(TossResultCallback body) {

//    Order order = orderRepository.findByOid(body.getOrderNo()).orElseThrow(() -> new BadRequestException("존재하지 않은 주문입니다."));
//    if (body.getStatus().equals(TossStatus.PAY_COMPLETE)) {
////      order = orderService.success(order); // 카드 결제 성공
//      order = orderRepository.save(order);
//      if (order.getRelativeBuyer() != null) {
//        log.info("### ({} {}) SUCCESS ORDER ({}) ::: {}", order.getRelativeBuyer().getRelativeUser().getId(), order.getRelativeBuyer().getRelativeUser().getFullname(), order.getId(), order.getOid());
//      }
//      log.info("### SUCCESS ORDER TOSS ::: {}", order.getRelativeTossApproval());
//    } else {
//      order.setOrderStatus(OrderStatus.FAILED);
//      log.info("### FAILED ORDER TOSS ::: {}", order.getRelativeTossApproval());
//    }
  }

  @Override
  @Transactional
  public void complete(TossComplete body) {

//    Order order = orderRepository.findByOid(body.getOrderNo()).orElseThrow(() -> new BadRequestException("존재하지 않은 주문입니다."));
//
//    order = orderService.success(order); // 카드 결제 성공
//    order = orderRepository.save(order);
//    if (order.getRelativeBuyer() != null) {
//      log.info("### ({} {}) SUCCESS ORDER ({}) ::: {}", order.getRelativeBuyer().getRelativeUser().getId(), order.getRelativeBuyer().getRelativeUser().getFullname(), order.getId(), order.getOid());
//    }
//    log.info("### SUCCESS ORDER TOSS ::: {}", order.getRelativeTossApproval());

  }

  @Override
  @Transactional
  public void complete(String oid) {
    log.info("toss complete ::: {}", oid);
    Order order = orderRepository.findByOid(oid).orElseThrow(() -> new BadRequestException("존재하지 않은 주문입니다."));

//    if (order.getRelativeTossApproval() == null) {
//      throw new BadRequestException();
//    }
//    if (StringUtils.isEmpty(order.getRelativeTossApproval().getPayToken())) {
//      throw new BadRequestException();
//    }
//
//    RestTemplate restTemplate = new RestTemplate();
//
//    String body;
//    try {
//
//      body = new ObjectMapper().writeValueAsString(TossStatusReqBody.builder().apiKey(apiKey).payToken(order.getRelativeTossApproval().getPayToken()).build());
//      log.info("request body ::: {}", body);
//    } catch (IOException e) {
//      throw new RuntimeException(e);
//    }
//
//    URI uri = UriComponentsBuilder.fromHttpUrl("https://pay.toss.im/api/v1/status").build().toUri();
//
//    HttpHeaders headers = new HttpHeaders();
//    headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));
//
//    HttpEntity entity = new HttpEntity(body, headers);
//
//    ResponseEntity<TossStatusResBody> responseEntity = restTemplate.postForEntity(
//        uri,
//        entity,
//        TossStatusResBody.class);
//
//    log.info("response entity ::: {}", responseEntity);
//    log.info("response body ::: {}", responseEntity.getBody());
//    if (!Objects.equals(responseEntity.getStatusCode(), HttpStatus.OK)) {
//      log.error("response body ::: {}", responseEntity.getBody());
//      throw new RuntimeException(responseEntity.toString());
//    }
//
//    TossStatusResBody tossResponse = responseEntity.getBody();
//    if (tossResponse.getCode().equals("0")) {
//      TossComplete tc = new TossComplete();
//      tc.setCode(tossResponse.getCode());
//      tc.setOid(tossResponse.getOrderNo());
//      this.complete(tc);
//    }
  }
}
