package com.logisall.anyware.api.web.board;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import com.logisall.anyware.api.service.board.contact.ContactAPIService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.model.reqbody.board.ContactReqBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Contact", description = "문의하기 API")
@RestController
@RequestMapping("/api/v1/contacts")
public class APIContactController {

  @Autowired
  private ContactAPIService contactService;

  @Operation(summary = "[contact-1] Post Contact (문의하기)",
      description = "문의 등록",
      security = {@SecurityRequirement(name = "bearerAuth")}
  )
  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> post(@RequestBody ContactReqBody contact,
                                @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    if (!contact.isPrivacyAgree()) {
      throw new BadRequestException("개인정보 처리방침에 동의하세요.");
    }

    String email = null;

    if (oAuth2Authentication != null && oAuth2Authentication.getPrincipal() != null) {
      email = oAuth2Authentication.getPrincipal().toString();
    }

    Long id = contactService.post(contact, email);
    if (id != null) {
      return ResponseEntity.ok().build();
    }
    return ResponseEntity.badRequest().build();
  }
}
