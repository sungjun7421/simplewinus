package com.logisall.anyware.api.service.board.post;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.board.post.Post;
import com.logisall.anyware.core.domain.board.post.PostPredicate;
import com.logisall.anyware.core.domain.board.post.PostRepository;
import com.logisall.anyware.core.domain.board.post.category.PCategory;
import com.logisall.anyware.core.domain.board.post.category.PCategoryPredicate;
import com.logisall.anyware.core.domain.board.post.category.PCategoryRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.resbody.board.CategoryResBody;
import com.logisall.anyware.core.model.resbody.board.PostResBody;
import com.logisall.anyware.core.model.resbody.board.PostThumbResBody;
import com.logisall.anyware.core.service.setting.AppSettingService;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PostAPIServiceImpl implements PostAPIService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private PCategoryRepository categoryRepository;

    @Autowired
    private PagedResourcesAssembler pagedResourcesAssembler;

    @Autowired
    private AppSettingService appSettingService;

    @Override
    @Transactional
    public void pageView(Long id) {

        postRepository.findById(id)
            .ifPresent(post -> {
                post.setPageView(post.getPageView() + 1);
            });
    }

    @Override
    @Transactional(readOnly = true)
    public PagedModel<PostThumbResBody> pagedResources(Locale locale,
                                                       HttpServletRequest request,
                                                       Filter filter,
                                                       Post.Type type,
                                                       Long[] idCategories,
                                                       Boolean top) {

        Locale defaultLocale = appSettingService.getDefaultLocale();

        Page<Post> postPage = postRepository.findAll(
            PostPredicate.getInstance()

                .search(filter.getQuery())
                .startDate(filter.getStartDate())
                .endDate(filter.getEndDate())
                .type(type)
                .top(top)
                .categories(idCategories)

                .active(true)
                .locale(locale, defaultLocale)
                .values(),
            filter.getPageable());

        List<PostThumbResBody> list = postPage.getContent().stream()
            .map(post -> post.toThumbBody(locale))
            .collect(Collectors.toList());

        Page<PostThumbResBody> page = new PageImpl<>(list, filter.getPageable(), postPage.getTotalElements());
        return pagedResourcesAssembler.toModel(page);
    }

    @Override
    @Transactional(readOnly = true)
    public EntityModel<PostResBody> resource(Locale locale, HttpServletRequest request, Long id) {

        Post post = this.get(locale, id);

        if (post == null) {
            throw new BadRequestException();
        }
        post.setLocale(locale);
        PostResBody detail = post.toBody(locale);

        // 카테고리
        if (post.getCategories() != null) {
            detail.setCategories(post.getCategories().stream()
                .map(category -> category.toBody(locale))
                .collect(Collectors.toList()));
        }

        // 첨부파일
        if (post.getFiles().size() > 0) {
            detail.setFiles(post.getFiles().stream()
                .map(fileEntity -> fileEntity.toBody(locale)).collect(Collectors.toList()));
        }

        // 이전글/다음글
        List<Long> next = postRepository.next(post.getRegDate(), post.getType(), PageRequest.of(0, 1));
        List<Long> previous = postRepository.previous(post.getRegDate(), post.getType(), PageRequest.of(0, 1));

        if (next != null && next.size() > 0) {
            detail.setHasNext(true);
            detail.setIdNext(next.get(0));
        }
        if (previous != null && previous.size() > 0) {
            detail.setHasPrevious(true);
            detail.setIdPrevious(previous.get(0));
        }
        return new EntityModel(detail);
    }

    @Override
    @Transactional(readOnly = true)
    public Post get(Locale locale, Long id) {

        return postRepository.findById(id)
            .map(post -> {
                post.lazy();
                post.setLocale(locale);
                return post;
            }).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public CollectionModel<EntityModel<CategoryResBody>> categories(Locale locale, Post.Type type) {

        Locale defaultLocale = appSettingService.getDefaultLocale();

        List<PCategory> categories = Lists.newArrayList(categoryRepository.findAll(

            PCategoryPredicate.getInstance()
                .type(type)
                .active(true)
                .locale(locale, defaultLocale)
                .values(),

            Sort.by(Sort.Direction.ASC, "orderAscending")));

        return CollectionModel.wrap(categories.stream().map(category -> {
            category.setLocale(locale);
            return category.toBody(locale);
        }).collect(Collectors.toList()));
    }
}
