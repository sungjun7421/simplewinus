package com.logisall.anyware.api.service.board.qna;


import com.logisall.anyware.core.model.resbody.board.CategoryResBody;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;

import java.util.Locale;

public interface QnaCategoryAPIService {
    CollectionModel<EntityModel<CategoryResBody>> categories(Locale locale);
}
