package com.logisall.anyware.api.web.commerce.payment;

import com.logisall.anyware.api.service.commerce.payment.iamport.IamPortAPIService;
import com.logisall.anyware.api.service.commerce.payment.iamport.IamPortParams;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.ForbiddenException;
import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.model.reqbody.commerce.payment.IamPortReadyReqBody;
import com.logisall.anyware.core.service.commerce.order.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("api/v1/payment")
public class APIPaymentController {

    @Autowired
    private IamPortAPIService iamPortAPIService;
    @Autowired
    private OrderService orderService;


    @PostMapping(value = "/ready")
    @PreAuthorize("hasAnyRole('ROLE_USER')")
    public ResponseEntity<?> ready(OAuth2Authentication oAuth2Authentication,
                                   @RequestBody IamPortReadyReqBody paymentReqBody) {

        if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
            throw new ForbiddenException();
        }

        if (paymentReqBody.isBadRequest()) {
            throw new BadRequestException();
        }

        String email = oAuth2Authentication.getPrincipal().toString();
        IamPortParams iamPortParam = iamPortAPIService.ready(paymentReqBody,email);
        return ResponseEntity.ok(iamPortParam);
    }

    @GetMapping(value = "/notification")
    public ResponseEntity notification(@RequestParam String imp_uid,
                                       @RequestParam String merchant_uid) {

        if (imp_uid== null || merchant_uid == null) {
            throw new BadRequestException();
        }

        OrderStatus orderStatus = iamPortAPIService.complete(imp_uid,merchant_uid);
        Order order = orderService.get(merchant_uid);
        return ResponseEntity.ok(orderStatus);
    }
}
