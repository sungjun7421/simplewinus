package com.logisall.anyware.api.web.commerce.coupon;

import com.logisall.anyware.api.SwaggerExample;
import com.logisall.anyware.api.service.commerce.couponHistory.CouponHistoryAPIService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.ForbiddenException;
import com.logisall.anyware.core.config.exception.NotFoundException;
import com.logisall.anyware.core.domain.commerce.coupon.Coupon;
import com.logisall.anyware.core.domain.commerce.coupon.CouponRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.reqbody.commerce.coupon.CreateCouponReqBody;
import com.logisall.anyware.core.model.reqbody.commerce.coupon.RegisterCouponReqBody;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

@Slf4j
@Tag(name = "Coupon Box", description = "쿠폰함 API")
@RestController
@RequestMapping("/api/v1/coupon-box")
public class APICouponBoxController {

  @Autowired
  private CouponHistoryAPIService couponHistoryAPIService;

  @Autowired
  private CouponRepository couponRepository;

  /**
   * [coupon-box-1] 리스트
   */
//  @Operation(summary = "[coupon-1]  리스트", description = "has access_token")
//  @PostMapping("download/{idCoupon}")
//  public ResponseEntity<?> download(@PathVariable Long idCoupon,
//                                 OAuth2Authentication oAuth2Authentication) {
//
//    if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
//      throw new ForbiddenException();
//    }
//
//    String email = oAuth2Authentication.getPrincipal().toString();
//    boolean download = couponHistoryService.download(idCoupon, email);
//    return ResponseEntity.ok(BaseResponseBody.of(download));
//  }

  /**
   * [coupon-box-1] 리스트
   */
  @Operation(summary = "[coupon-box-1] Download coupon", description = "Get coupon to user",
      security = {@SecurityRequirement(name = "bearerAuth")},
      requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
          description = "",
          content =
          @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(name = "Create Coupon Body", implementation = CreateCouponReqBody.class),
              examples = {
                  @ExampleObject(
                      name = "Create Coupon Body Example 1",
                      summary = "Create Coupon Body Example 1",
                      description = " ",
                      value = SwaggerExample.COUPON_1)
              }
          )
      ),
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = "\"success\"")
          )))
  @PostMapping(value = "/download")
  public ResponseEntity<?> download(@RequestBody CreateCouponReqBody reqBody,
                                 OAuth2Authentication oAuth2Authentication) {

    if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
      throw new ForbiddenException();
    }

    if(reqBody == null || reqBody.getIdCoupon() == null){
      throw new BadRequestException("Params not found!");
    }

    String email = oAuth2Authentication.getPrincipal().toString();
    boolean registered = couponHistoryAPIService.registerCode(reqBody.getIdCoupon(), email);
    if( registered) {
      return ResponseEntity.ok("success");
    }
    return ResponseEntity.ok("fail");
  }

  /**
   * [coupon-box-2] Register coupon
   */
  @Operation(summary = "[coupon-box-1] Register coupon", description = "input code coupon for register",
      requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
          description = "",
          content =
          @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(name = "Register Coupon Body", implementation = CreateCouponReqBody.class),
              examples = {
                  @ExampleObject(
                      name = "Register Coupon Body Example 1",
                      summary = "Register Coupon Body Example 1",
                      description = " ",
                      value = SwaggerExample.COUPON_1)
              }
          )
      ),
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = "\"success\"")
          )))
  @PostMapping(value = "/register")
  public ResponseEntity<?> register(@RequestBody RegisterCouponReqBody reqBody,
                                 OAuth2Authentication oAuth2Authentication) {

    if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
      throw new ForbiddenException();
    }

    if(reqBody == null || reqBody.getCode() == null || reqBody.getCode() == ""){
      throw new BadRequestException("Params not found!");
    }

    String email = oAuth2Authentication.getPrincipal().toString();

    Coupon coupon = couponRepository.findByCode(reqBody.getCode());
    if(coupon == null){
      throw new NotFoundException("Coupon not found!");
    }

    boolean registered = couponHistoryAPIService.registerCode(coupon.getId(), email);
    if( registered) {
      return ResponseEntity.ok("success");
    }
    return ResponseEntity.ok("fail");
  }


  /**
   * [coupon-1] CouponHistory list
   */
  @Operation(summary = "[coupon-1]  Coupon list", description = "Get all coupon",
      security = {@SecurityRequirement(name = "bearerAuth")},
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = "{\n" +
                  "    \"status\": \"[status code]\",\n" +
                  "    \"msg\": \"[message]\",\n" +
                  "    \"data\": \"[list coupon]\"\n" +
                  "}")
          )))
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR"),
      @Parameter(in = ParameterIn.QUERY, name = "size", description = "Size Page (default : 20)", example = "20"),
      @Parameter(in = ParameterIn.QUERY, name = "page", description = "Current Page (default : 0)", example = "0"),
      @Parameter(in = ParameterIn.QUERY, name = "sort", description = "Sort Page", example = "createdDate, desc"),
  })
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> page(@Parameter(hidden = true) @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                                @Parameter(description = "Search word") @RequestParam(required = false, defaultValue = "") String query,
                                @Parameter(description = "Start Time (yyyy-MM-dd HH:mm:ss)") @RequestParam(required = false) String startDate,
                                @Parameter(description = "End Time (yyyy-MM-dd HH:mm:ss)") @RequestParam(required = false) String endDate,
                                @Parameter(description = "Is User?") @RequestParam(required = false ) Boolean isUsed,
                                @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication,
                                Locale locale) {

    if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
      throw new ForbiddenException();
    }

    Filter filter = new Filter(pageable, query, startDate, endDate);
    String email = oAuth2Authentication.getPrincipal().toString();
    return ResponseEntity.ok(couponHistoryAPIService.pagedResources(locale, filter, email, isUsed));
  }

  /**
   * [coupon-2] coupon list by user
   */
  @Operation(summary = "[coupon-2]  coupon list user", description = "Coupon list by user non use",
      security = {@SecurityRequirement(name = "bearerAuth")},
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = " ")
          )))
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR"),
  })
  @GetMapping(value = "/list")
  public ResponseEntity<?> list(OAuth2Authentication oAuth2Authentication,
                             Locale locale) {

    if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
      throw new ForbiddenException();
    }
    String email = oAuth2Authentication.getPrincipal().toString();
    return ResponseEntity.ok(couponHistoryAPIService.resources(locale, email, false));
  }
}
