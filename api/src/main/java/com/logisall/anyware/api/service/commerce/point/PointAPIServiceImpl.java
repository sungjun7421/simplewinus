package com.logisall.anyware.api.service.commerce.point;

import com.logisall.anyware.api.service.commerce.buyer.BuyerAPIService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.point.Point;
import com.logisall.anyware.core.domain.commerce.point.PointPredicate;
import com.logisall.anyware.core.domain.commerce.point.PointRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.resbody.commerce.point.PointOfMemberResBody;
import com.logisall.anyware.core.model.resbody.commerce.point.PointResBody;
import com.logisall.anyware.core.service.commerce.point.PointService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PointAPIServiceImpl implements PointAPIService {

  @Autowired
  private PointRepository pointRepository;

  @Autowired
  private PointService pointService;

  @Autowired
  private BuyerAPIService buyerAPIService;

  @Autowired
  private PagedResourcesAssembler pagedResourcesAssembler;


  @Override
  @Transactional
  public PagedModel<PointResBody> pagedResources(Locale locale, Filter filter, Point.Status status, String email) {

    Page<Point> page = pointRepository.findAll(
        PointPredicate.getInstance()
            .search(filter.getQuery())
            .startDate(filter.getStartDate())
            .endDate(filter.getEndDate())
            .status(status)
            .values(),
        filter.getPageable());

    List<PointResBody> list = page.getContent().stream()
        .map(point -> {
          PointResBody body = point.toBody(locale);
          body.setTotal(pointRepository.totalFollowDate(point.getRelativeBuyer().getId(), point.getCreatedDate()));
          return body;
        }).collect(Collectors.toList());

    Page<PointResBody> pageMap = new PageImpl<>(list, filter.getPageable(), page.getTotalElements());

    return pagedResourcesAssembler.toModel(pageMap);
  }

  @Override
  @Transactional
  public PointOfMemberResBody getDetailPoint(Long buyerId){
    Buyer buyer = buyerAPIService.get(buyerId);
    if (buyer == null) {
      throw new BadRequestException();
    }
    PointOfMemberResBody pointDetail = new PointOfMemberResBody();
    pointDetail.setIdBuyer(buyerId);
    pointDetail.setTotalPoint(pointService.total(buyerId));
    pointDetail.setExpirePoint(pointService.expireScheduledTotal(buyerId));

    return pointDetail;
  }

}
