package com.logisall.anyware.api.service.board.contact;


import com.logisall.anyware.core.model.reqbody.board.ContactReqBody;

public interface ContactAPIService {

  // Rest
  Long post(ContactReqBody contactReqBody, String email);
}

