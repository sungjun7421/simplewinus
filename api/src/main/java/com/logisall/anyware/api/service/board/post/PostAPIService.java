package com.logisall.anyware.api.service.board.post;

import com.logisall.anyware.core.domain.board.post.Post;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.resbody.board.CategoryResBody;
import com.logisall.anyware.core.model.resbody.board.PostResBody;
import com.logisall.anyware.core.model.resbody.board.PostThumbResBody;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public interface PostAPIService {

    void pageView(Long id);

    Post get(Locale locale, Long id);

    PagedModel<PostThumbResBody> pagedResources(Locale locale,
                                                HttpServletRequest request,
                                                Filter filter,
                                                Post.Type type,
                                                Long[] idCategories,
                                                Boolean top);

    EntityModel<PostResBody> resource(Locale locale, HttpServletRequest request, Long id);

    CollectionModel<EntityModel<CategoryResBody>> categories(Locale locale, Post.Type type);
}
