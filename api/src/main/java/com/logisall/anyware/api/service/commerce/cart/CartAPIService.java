package com.logisall.anyware.api.service.commerce.cart;

import com.logisall.anyware.core.model.reqbody.commerce.cart.CartDeleteReqBody;
import com.logisall.anyware.core.model.reqbody.commerce.cart.CartRequestBody;
import com.logisall.anyware.core.model.reqbody.commerce.cart.CartUpdateReqBody;
import org.springframework.http.ResponseEntity;

public interface CartAPIService {
    ResponseEntity<?> getListCartOf(String email);
    ResponseEntity<?> addCart(CartRequestBody body, String email);
    ResponseEntity<?> updateCartQty(CartUpdateReqBody body, String email);
    ResponseEntity<?> deleteList(CartDeleteReqBody body, String email);
    ResponseEntity<?> deleteAll( String email);
    ResponseEntity<?> getTotal( String email);
}
