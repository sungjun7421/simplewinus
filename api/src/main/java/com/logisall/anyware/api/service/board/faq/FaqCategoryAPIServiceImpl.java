package com.logisall.anyware.api.service.board.faq;

import com.logisall.anyware.core.domain.board.faq.category.FaqCategory;
import com.logisall.anyware.core.domain.board.faq.category.FaqCategoryPredicate;
import com.logisall.anyware.core.domain.board.faq.category.FaqCategoryRepository;
import com.logisall.anyware.core.model.resbody.board.FaqCategoryResBody;
import com.logisall.anyware.core.service.setting.AppSettingService;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@Service
public class FaqCategoryAPIServiceImpl implements FaqCategoryAPIService {

    @Autowired
    private FaqCategoryRepository faqCategoryRepository;

    @Autowired
    private AppSettingService appSettingService;

    @Override
    @Transactional
    public CollectionModel<EntityModel<FaqCategoryResBody>> categories(Locale locale) {

        Locale defaultLocale = appSettingService.getDefaultLocale();

        List<FaqCategory> faqCategories = Lists.newArrayList(faqCategoryRepository.findAll(
                FaqCategoryPredicate.getInstance()

                        .active(true)
                        .locale(locale, defaultLocale)
                        .values(),
                Sort.by(Sort.Direction.ASC, "orderAscending")));

        return CollectionModel.wrap(faqCategories.stream().map(category -> {
            category.setLocale(locale);
            return category.toBody(locale);
        }).collect(Collectors.toList()));
    }
}
