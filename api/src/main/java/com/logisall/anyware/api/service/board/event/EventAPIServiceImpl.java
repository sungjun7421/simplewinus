package com.logisall.anyware.api.service.board.event;

import com.logisall.anyware.core.domain.board.event.Event;
import com.logisall.anyware.core.domain.board.event.EventPredicate;
import com.logisall.anyware.core.domain.board.event.EventRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.resbody.board.EventResBody;
import com.logisall.anyware.core.model.resbody.board.EventThumbBody;
import com.logisall.anyware.core.service.board.event.EventService;
import com.logisall.anyware.core.service.setting.AppSettingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@Service
public class EventAPIServiceImpl implements EventAPIService {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventService eventService;

    @Autowired
    private AppSettingService appSettingService;

    @Autowired
    private PagedResourcesAssembler pagedResourcesAssembler;

    @Override
    @Transactional(readOnly = true)
    public PagedModel<?> pagedResources(Locale locale, Filter filter, Boolean end) {

        Locale defaultLocale = appSettingService.getDefaultLocale();

        Page<Event> page = eventRepository.findAll(
                EventPredicate.getInstance()

                    .search(filter.getQuery())
                    .startDate(filter.getStartDate())
                    .endDate(filter.getEndDate())
                    .end(end)

                    .active(true)
                    .locale(locale, defaultLocale)
                    .values(),
                filter.getPageable());

        List<EventThumbBody> list = page.getContent().stream()
                .map(event -> event.toThumbBody(locale))
                .collect(Collectors.toList());
        return pagedResourcesAssembler.toModel(new PageImpl<>(list, filter.getPageable(), page.getTotalElements()));
    }

    @Override
    @Transactional(readOnly = true)
    public EntityModel<EventResBody> resource(Locale locale, Long id) {
        Event event = eventService.get(locale, id);
        return new EntityModel<>(event.toBody(locale));
    }

    @Override
    @Transactional
    public long pageViewUp(Long id) {
        return eventRepository.findById(id).map(event -> {
            long pageView = event.getPageView() + 1;
            event.setPageView(pageView);
            return pageView;
        }).orElse(0L);
    }
}
