package com.logisall.anyware.api.web.sample;

import com.logisall.anyware.api.SwaggerExample;
import com.logisall.anyware.core.model.reqbody.sample.SampleReqBody;
import com.google.common.collect.ImmutableMap;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

@Tag(name = "Sample POST/PUT/GET/DELETE, Auth", description = "Sample API")
@Slf4j
@RestController
@RequestMapping("/api/v1/api-sample")
public class APISampleController {

  @Operation(summary = "[sample-1] Get 테스트",
      description = "Get",
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = SwaggerExample.RES_BODY)
          ))
  )
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code 언어코드", example = "ko-KR"),

      @Parameter(in = ParameterIn.QUERY, name = "query", description = "query", example = "테스트")
  })
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> get(@Parameter String query) {
    log.debug("query : {}", query);
    return ResponseEntity.ok(ImmutableMap.builder().put("result", query).build());
  }

  @Operation(summary = "[sample-2] Post 테스트",
      description = "Post ",
      requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
          description = "",
          content =
          @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(name = "Contact body", implementation = SampleReqBody.class),
              examples = {
                  @ExampleObject(
                      name = "Test Body Example 1",
                      summary = "Test Body Example 1",
                      description = "Test",
                      value = SwaggerExample.RES_TEST_POST
                  )
              }
          )
      ),
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = SwaggerExample.RES_BODY)
          ))
  )
  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> post(@RequestBody SampleReqBody body) {
    log.debug("body : {}", body);
    return ResponseEntity.ok(ImmutableMap.builder().put("result", body).build());
  }

  @Operation(summary = "[sample-3] Put 테스트",
      description = "Put ",
      requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
          description = "",
          content =
          @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(name = "TestReqBody", implementation = SampleReqBody.class),
              examples = {
                  @ExampleObject(
                      name = "Test Body Example 1",
                      summary = "Test Body Example 1",
                      description = "Test",
                      value = SwaggerExample.RES_TEST_POST
                  )
              }
          )
      ),
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = SwaggerExample.RES_BODY)
          ))
  )
  @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> put(@Parameter(in = ParameterIn.PATH, name = "id", description = "put id", example = "1") @PathVariable Long id,
                               @RequestBody SampleReqBody body) {
    log.debug("body : {}", body);
    return ResponseEntity.ok(ImmutableMap.builder().put("result", body).build());
  }

  @Operation(summary = "[sample-4] Delete 테스트",
      description = "Delete ",
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = SwaggerExample.RES_BODY)
          ))
  )
  @DeleteMapping(value = "/{id}")
  public ResponseEntity<?> delete(@Parameter(in = ParameterIn.PATH, name = "id", description = "delete id", example = "1") @PathVariable Long id) {
    log.debug("id : {}", id);
    return ResponseEntity.ok(ImmutableMap.builder().put("result", id).build());
  }

  @Operation(summary = "[sample-5] Auth & Language 테스트",
      description = "Post ",
      requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
          description = "",
          content =
          @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(name = "Contact body", implementation = SampleReqBody.class),
              examples = {
                  @ExampleObject(
                      name = "Test Body Example 1",
                      summary = "Test Body Example 1",
                      description = "Test",
                      value = SwaggerExample.RES_TEST_POST
                  )
              }
          )
      ),
      responses = @ApiResponse(
          content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
              schema = @Schema(example = SwaggerExample.RES_BODY)
          ))
  )
  @PostMapping(value = "/auth", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> auth(@RequestBody SampleReqBody body,
                                Locale locale,
                                @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    log.debug("body : {}", body);
    log.debug("oAuth2Authentication : {}", oAuth2Authentication);
    String email = "";
    if(oAuth2Authentication != null && oAuth2Authentication.getPrincipal() != null) {
      email = oAuth2Authentication.getPrincipal().toString(); // 창고주
    }

    return ResponseEntity.ok(ImmutableMap.builder()
        .put("email", email)
        .put("language", locale)
        .build());
  }
}
