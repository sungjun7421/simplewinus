package com.logisall.anyware.api.service.commerce.payment.iamport;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class IamPortCancel implements Serializable {

    private static final long serialVersionUID = 4588349926448182529L;

    private String merchant_uid;
    private String amount;
    private String reason;
}
