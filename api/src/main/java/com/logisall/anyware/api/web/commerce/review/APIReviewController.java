package com.logisall.anyware.api.web.commerce.review;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.ForbiddenException;
import com.logisall.anyware.core.domain.commerce.review.Review;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.reqbody.commerce.review.ReviewReqBody;
import com.logisall.anyware.core.service.commerce.review.ReviewService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

@Slf4j
@RestController
@RequestMapping("api/v1/review")
public class APIReviewController {

    @Autowired
    private ReviewService reviewService;
    @Autowired
    private UserRepository userRepository;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> page( @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                                  @RequestParam(required = false, defaultValue = "") String query,
                                  @RequestParam(required = false) String startDate,
                                  @RequestParam(required = false) String endDate,
                                  OAuth2Authentication oAuth2Authentication,
                                  Locale locale) {
        if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
            throw new ForbiddenException();
        }
        String email = oAuth2Authentication.getPrincipal().toString();

        if(email.isEmpty()){
            throw new BadRequestException("User do not log in");
        }
        Long id = userRepository.findByEmail(email).get().getId();

        Filter filter = new Filter(pageable, query, startDate, endDate);
        return ResponseEntity.ok(reviewService.pagedResources(locale,id, filter));

    }


    @PostMapping(value = "/create",produces = MediaType.APPLICATION_JSON_VALUE)
//    @PreAuthorize("hasAnyRole('ROLE_USER')")
    public ResponseEntity<?> create(OAuth2Authentication oAuth2Authentication,
                                    @RequestBody ReviewReqBody reviewReqBody){
        if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
            throw new ForbiddenException();
        }
        String email = oAuth2Authentication.getPrincipal().toString();

        if(email.isEmpty()){
            throw new BadRequestException("User do not log in");
        }

        Boolean ableToReview = reviewService.hasPayment(reviewReqBody.getIdRelativePaymentProduct());
        if(!ableToReview){
            throw new BadRequestException("User can not review");

        }
        Review review = reviewService.setValue(reviewReqBody,email);
        return ResponseEntity.ok(review);

    }
    @PostMapping(value = "/star",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> averageStar(OAuth2Authentication oAuth2Authentication){
        if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
            throw new ForbiddenException();
        }

        String email = oAuth2Authentication.getPrincipal().toString();
        if(email.isEmpty()){
           throw new BadRequestException("user do not login");
        }
        return ResponseEntity.ok(reviewService.averageStar());

    }

}
