package com.logisall.anyware.api.service.mathformula;

import com.google.common.collect.ImmutableMap;
import lombok.extern.slf4j.Slf4j;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import net.objecthunter.exp4j.ValidationResult;
import net.objecthunter.exp4j.tokenizer.UnknownFunctionOrVariableException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;

@Slf4j
@Service
public class MathFormulaServiceImpl implements MathFormulaService {

  @Override
  public BigDecimal calculation(String expression, Map<String, Double> variables, String... variableNames) throws UnknownFunctionOrVariableException {
    log.debug("calculation expression ::: {}", expression);
    log.debug("calculation variables ::: {}", variables);
    log.debug("calculation variableNames ::: {}", Arrays.toString(variableNames));
    Expression e = new ExpressionBuilder(expression)
        .variables(variableNames)
        .build()
        .setVariables(variables);
    double result = e.evaluate();
    log.debug("calculation result ::: {}", result);
    return BigDecimal.valueOf(result).setScale(2, BigDecimal.ROUND_HALF_UP);
  }

  @Override
  public BigDecimal calculationX(String expression, double valueX) throws UnknownFunctionOrVariableException {
    Expression e = new ExpressionBuilder(expression)
        .variables("x")
        .build()
        .setVariable("x", valueX);
    double result = e.evaluate();
    return BigDecimal.valueOf(result).setScale(2, BigDecimal.ROUND_HALF_UP);
  }

  @Override
  public BigDecimal calculationXY(String expression, double valueX, double valueY) throws UnknownFunctionOrVariableException {
    Expression e = new ExpressionBuilder(expression)
        .variables("x", "y")
        .build()
        .setVariable("x", valueX)
        .setVariable("y", valueY);
    double result = e.evaluate();
    return BigDecimal.valueOf(result).setScale(2, BigDecimal.ROUND_HALF_UP);
  }

  @Override
  public BigDecimal calculationXYZ(String expression, double valueX, double valueY, double valueZ) throws UnknownFunctionOrVariableException {
    Expression e = new ExpressionBuilder(expression)
        .variables("x", "y", "z")
        .build()
        .setVariable("x", valueX)
        .setVariable("y", valueY)
        .setVariable("z", valueZ);
    double result = e.evaluate();
    return BigDecimal.valueOf(result).setScale(2, BigDecimal.ROUND_HALF_UP);
  }

  @Override
  public void valid(String formula, String key) throws RuntimeException {
    try {
      BigDecimal value = this.calculation(formula, ImmutableMap.of(key, 1000D), key);
      log.debug(value.toString());
    } catch (UnknownFunctionOrVariableException e) {
      e.printStackTrace();
      throw new RuntimeException(e.getLocalizedMessage());
    }
  }

  @Override
  public void validate(String formula, String key) {
    Expression e = new ExpressionBuilder(formula)
      .variable(key)
      .build()
      .setVariable(key, 100D);
    ValidationResult result = e.validate();
    if (!result.isValid()) {
      result.getErrors().forEach(str -> log.debug("error ::: {}", str));
      throw new UnknownFunctionOrVariableException(formula, 0, 0);
    }
  }


}
