package com.logisall.anyware.api.service.board.qna;

import com.logisall.anyware.core.domain.board.qna.Qna;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.file.FileMeta;
import com.logisall.anyware.core.model.reqbody.board.QuestionReqBody;
import com.logisall.anyware.core.model.resbody.board.QnaResBody;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public interface QnaAPIService {

  PagedModel<QnaResBody> pagedResources(Locale locale, HttpServletRequest request, Filter filter, Long[] idCategories, String email);

  EntityModel<QnaResBody> resource(Locale locale, HttpServletRequest request, Long id, String email);

  Qna question(QuestionReqBody questionReqBody, String userEmail);

  Qna questionByNoMember(QuestionReqBody questionReqBody);

  void delete(Long idQna, String email);

  FileMeta uploadFile(MultipartFile file);
}
