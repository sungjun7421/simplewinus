package com.logisall.anyware.api.service.commerce.couponHistory;

import com.logisall.anyware.api.service.account.user.UserAPIService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.NotFoundException;
import com.logisall.anyware.core.config.exception.ResponseErrorCode;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.coupon.Coupon;
import com.logisall.anyware.core.domain.commerce.coupon.CouponRepository;
import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistory;
import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistoryPredicate;
import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistoryRepository;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.reqbody.commerce.coupon.CouponThumbBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CouponHistoryAPIServiceImpl implements CouponHistoryAPIService {
  @Autowired
  private CouponHistoryRepository couponHistoryRepository;

  @Autowired
  private CouponRepository couponRepository;

  @Autowired
  private PagedResourcesAssembler pagedResourcesAssembler;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private UserAPIService userAPIService;

  @Override
  @Transactional
  public CouponHistory create(CouponHistory couponHistory) {
    return couponHistoryRepository.save(couponHistory);
  }

  @Override
  @Transactional
  public void use(Long idCouponHistory) {
    if (idCouponHistory != null) {
      couponHistoryRepository.findById(idCouponHistory).ifPresent(couponBox -> {
        couponBox.setUsed(true);
        couponBox.setUsedDate(LocalDate.now());
        couponHistoryRepository.save(couponBox);
      });
    }
  }

  @Override
  @Transactional
  public void restore(Long idCouponHistory) {
    if (idCouponHistory != null) {
      couponHistoryRepository.findById(idCouponHistory).ifPresent(couponBox -> {
        couponBox.setUsed(false);
        couponBox.setUsedDate(null);
        couponHistoryRepository.save(couponBox);
      });
    }
  }

  @Override
  @Transactional
  public boolean registerCode(Long id, String email) {
    if (!couponRepository.existsById(id)) {
      throw new NotFoundException("Coupon not found!");
    }

    Coupon coupon = couponRepository.getOne(id);
    Buyer buyer = userAPIService.get(email).getRelativeBuyer();

    if (buyer == null) {
      throw new BadRequestException("Buyer not found!");
    }

    // 쿠폰이 활성화 되었는지 확인
    if (!coupon.isActive()) {
      throw new BadRequestException(ResponseErrorCode.INVALID_COUPON.name());
    }

    // 쿠폰 사용기간이 입력되어 있는지 확인
    if (coupon.getStartDate() == null || coupon.getEndDate() == null) {
      throw new RuntimeException("쿠폰사용기간이 입력되지 않았습니다.(관리자페이지에서 입력여부를 확인하세요.)");
    }

    // 쿠폰등록기간 제한
    LocalDate now = LocalDate.now();
    if (now.isBefore(coupon.getStartDate()) || now.isAfter(coupon.getEndDate())) {
      throw new BadRequestException(ResponseErrorCode.NOT_COUPON_REGISTRATION_PERIOD.name());
    }

    // 이미 등록된 쿠폰인지 확인
    if (couponHistoryRepository.existsByRelativeBuyer_IdAndRelativeCoupon_Id(buyer.getId(), coupon.getId())) {
      throw new BadRequestException(ResponseErrorCode.ALREADY_REGISTERED_COUPON.name());
    }

    CouponHistory box = new CouponHistory();
    box.setRelativeBuyer(buyer);
    box.setRelativeCoupon(coupon);
    CouponHistory couponHistory = this.create(box);
    if (couponHistory == null) {
      return false;
    }

    return true;
  }

  @Override
  @Transactional
  public boolean issueDefaultCoupon(Long idCoupon) {

    Coupon coupon = couponRepository.getOne(idCoupon);

    if (coupon == null) {
      throw new BadRequestException();
    }

    // 활성화 되었는지 확인
    if (!coupon.isActive()) {
      throw new BadRequestException("The coupon status is inactive.");
    }

    // 쿠폰 사용기간이 입력되어 있는지 확인
    if (coupon.getStartDate() == null || coupon.getEndDate() == null) {
      throw new RuntimeException("쿠폰사용기간이 입력되지 않았습니다.(관리자페이지에서 입력여부를 확인하세요.)");
    }

    AtomicInteger ai = new AtomicInteger(0);
    List<User> users = userRepository.findByUserDetailsMetaEnabledTrueAndLeaveMetaLeaveFalse();

    users.forEach(user -> {
      Buyer buyer = user.getRelativeBuyer();

      if (buyer != null) {

        CouponHistory couponBox = new CouponHistory();
        couponBox.setRelativeBuyer(buyer);
        couponBox.setRelativeCoupon(coupon);
        CouponHistory couponHistory = this.create(couponBox);
        if (couponHistory != null) {
          ai.incrementAndGet();
        }
      }

    });

    return ai.get() > 0;
  }

  @Override
  @Transactional
  public PagedModel<CouponThumbBody> pagedResources(Locale locale, Filter filter, String email, Boolean isUsed) {

    Page<CouponHistory> page = couponHistoryRepository.findAll(
        CouponHistoryPredicate.getInstance()
            .search(filter.getQuery())
            .startDate(filter.getStartDate())
            .endDate(filter.getEndDate())
            .used(isUsed)
            .buyer(email)
            .values(),
        filter.getPageable()
    );

    List<CouponThumbBody> list = page.getContent().stream()
        .map(couponBox -> couponBox.toCouponThumbBody(locale))
        .collect(Collectors.toList());

    return pagedResourcesAssembler.toModel(new PageImpl<>(list, filter.getPageable(), page.getTotalElements()));
  }


  @Override
  @Transactional
  public CollectionModel<EntityModel<CouponThumbBody>> resources(Locale locale, String email, Boolean used) {

    User user = userAPIService.get(email);
    List<CouponHistory> list = couponHistoryRepository.getAllByUser(user.getRelativeBuyer().getId(), used);

    return CollectionModel.wrap(list.stream()
        .map(couponBox -> couponBox.toCouponThumbBody(locale))
        .collect(Collectors.toList()));
  }


  public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
    Map<Object, Boolean> map = new HashMap<>();
    return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
  }
}
