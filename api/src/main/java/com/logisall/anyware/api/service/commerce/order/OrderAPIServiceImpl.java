package com.logisall.anyware.api.service.commerce.order;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.NotFoundException;
import com.logisall.anyware.core.config.language.Messages;
import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.commerce.buyer.Buyer;
import com.logisall.anyware.core.domain.commerce.cart.Cart;
import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistory;
import com.logisall.anyware.core.domain.commerce.couponhistory.CouponHistoryRepository;
import com.logisall.anyware.core.domain.commerce.order.BuyerInfo;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.domain.commerce.order.OrderRepository;
import com.logisall.anyware.core.domain.commerce.point.Point;
import com.logisall.anyware.core.domain.commerce.point.PointRepository;
import com.logisall.anyware.core.domain.commerce.product.Product;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.model.reqbody.commerce.order.OrderReqBody;
import com.logisall.anyware.core.model.resbody.commerce.order.OrderDetailResBody;
import com.logisall.anyware.core.service.commerce.buyer.BuyerService;
import com.logisall.anyware.core.service.commerce.point.PointService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


@Slf4j
@Service
public class OrderAPIServiceImpl implements OrderAPIService {

  @Autowired
  private OrderRepository orderRepository;

  @Autowired
  private Messages messages;

  @Autowired
  private BuyerService buyerService;

  @Autowired
  private PointRepository pointRepository;

  @Autowired
  private PointService pointService;

  @Autowired
  private CouponHistoryRepository couponHistoryRepository;


  @Override
  @Transactional
  public Order order(OrderReqBody orderReqBody, String email) {


    Buyer buyer = buyerService.get(email);
    Order order = new Order();
    order.setOid(this.generateOid(buyer.getId()));
    order.setRelativeBuyer(buyer);
    order.setTitle("Order No." + order.getOid());
    //  set buyer
    if (email != null) {

      if (buyer == null) {
        throw new NotFoundException("Buyer not found!");
      }
      order.setRelativeBuyer(buyer);
      // set buyerInfo
      BuyerInfo buyerInfo = new BuyerInfo();
      buyerInfo.setEmail(buyer.getRelativeUser().getEmail());
      buyerInfo.setFullName(buyer.getRelativeUser().getFullName());
      buyerInfo.setMobile(buyer.getRelativeUser().getMobile());
      order.setBuyerInfo(buyerInfo);
    }
    User user = buyer.getRelativeUser();

    if (!user.getUserDetailsMeta().isEnabled()) {
      throw new BadRequestException();
    }
    //get list card from Buyer
    // set Amount
    List<Cart> listCart = buyer.getCartList();
    BigDecimal amount = BigDecimal.ZERO;
    BigDecimal total = BigDecimal.ZERO;
    for (Cart cart : listCart) {
      BigDecimal qty = BigDecimal.valueOf(cart.getBuyCount());
      Product product = cart.getRelativeProduct();
      amount = amount.add(product.getPrice().multiply(qty));
      total = total.add(product.getSalePrice().multiply(qty));
    }

    order.setAmount(amount);
    order.setTotalPrice(total);
    order.setPaymentComplete(false);

    Order createdOrder = orderRepository.save(order);
    log.debug("createdOrder ::: {}", createdOrder);

    return orderRepository.save(createdOrder);

  }


  @Override
  public String generateOid(Long idBuyer) {
    DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yMMddHHmmss");
    return "oid" + "-" + LocalDateTime.now().format(FORMATTER) + (idBuyer != null ? idBuyer : "0");
  }

  @Override
  @Transactional
  public OrderDetailResBody getDetailOrder(Long id, Locale locale) {
    if (!orderRepository.existsById(id)) {
      throw new NotFoundException("Order not found");
    }
    Order order = orderRepository.findById(id).get();
    OrderDetailResBody orderDetailResBody = order.toBodyOrderDetail();
    return orderDetailResBody;
  }

  @Override
  @Transactional
  public Boolean cancelOrder(String oid) {

    Order order = orderRepository.findByOid(oid).get();
    if (order == null) {
      throw new NotFoundException("Order not found!");
    }

    order.setOrderStatus(OrderStatus.CANCEL1);
    Order orderSave = orderRepository.save(order);
    if (orderSave == null) {
      return false;
    }

    //refund point
    if (orderSave.getFcPoint() != null && orderSave.getFcPoint().compareTo(BigDecimal.ZERO) > 0) {
      BigDecimal point = orderSave.getFcPoint();
      BigDecimal pointSave = pointRepository.findByStatusAndRelativeBuyer_IdAndRelativeOrder_Id(Point.Status.DECREASE, orderSave.getRelativeBuyer().getId(), orderSave.getId()).getPoint();
      if (point.equals(pointSave)) {

        pointService.increase(point, Point.getInterTextByMessage(messages, "point.cancel"), orderSave.getRelativeBuyer().getRelativeUser().getEmail(), orderSave.getId());
      }
    }
    //refund coupon
    List<CouponHistory> couponHistories = orderSave.getUsedCoupons();
    if (couponHistories != null && couponHistories.size() > 0) {
      for (CouponHistory coupon : couponHistories) {
        coupon.setUsed(false);
        coupon.setUsedDate(null);
        coupon.setOrders(null);
        couponHistoryRepository.save(coupon);
      }
    }
    return true;
  }

  @Override
  @Transactional
  public List<OrderStatus> convertOrderStatus(String orderStatuses) {
    List<OrderStatus> orderStatusList = new ArrayList<>();
    if (orderStatuses != null && orderStatuses != "") {
      List<String> myList = new ArrayList<>(Arrays.asList(orderStatuses.split(",")));

      if (myList.contains("V_READY")) {
        orderStatusList.add(OrderStatus.V_READY);
      }
      if (myList.contains("COMPLETE")) {
        orderStatusList.add(OrderStatus.COMPLETE);
      }
      if (myList.contains("CANCEL")) {
        orderStatusList.add(OrderStatus.CANCEL1);
      }

      if (myList.contains("FAILED")) {
        orderStatusList.add(OrderStatus.FAILED);
      }
    }

    return orderStatusList;
  }

//    @Override
//    @Transactional
//    public PagedModel<OrderDetailResBody> getAllOrderDetailByUser(String email, List<OrderStatus> orderStatus, Filter filter, Locale locale, Long idStore) {
////        User user = userService.get(email);
////        Page<Order> orderList = orderRepository.getAllOrderDetailByUserAndOrderStatus(user.getId(), orderStatus, filter, locale, idStore);
////        List<OrderDetailResBody> orderDetailResBodyList = orderList.getContent().stream().map(order -> {
////            OrderDetailResBody orderResBody = order.toBodyOrderDetail(locale);
////
////            StoreResBody storeResBody = order.getRelativeOrderMachine().getRelativeMachine().getRelativeStore().toBody(locale);
////            //count machine
////
////
////            return orderResBody;
////        }).collect(Collectors.toList());
////
////        Page<OrderDetailResBody> qPage = new PageImpl<>(orderDetailResBodyList, filter.getPageable(), orderList.getTotalElements());
////        return pagedResourcesAssembler.toModel(qPage);
//    }
}
