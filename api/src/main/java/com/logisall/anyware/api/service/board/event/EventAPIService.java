package com.logisall.anyware.api.service.board.event;

import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.resbody.board.EventResBody;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;

import java.util.Locale;

public interface EventAPIService {

    // Rest
    PagedModel<?> pagedResources(Locale locale, Filter filter, Boolean end);
    EntityModel<EventResBody> resource(Locale locale, Long id);
    long pageViewUp(Long id);
}
