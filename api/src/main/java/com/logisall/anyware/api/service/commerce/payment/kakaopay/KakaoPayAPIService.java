package com.logisall.anyware.api.service.commerce.payment.kakaopay;

import com.logisall.anyware.core.model.payment.kakaopay.KakaoPayApproveReqBody;
import com.logisall.anyware.core.model.payment.kakaopay.KakaoPayOrder;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface KakaoPayAPIService {

    Map<String, Object> ready(KakaoPayOrder kpOrder, String email);
    ResponseEntity<?> approve(KakaoPayApproveReqBody reqBody, String email);
}
