package com.logisall.anyware.api.web.provider;

import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@RestController
@RequestMapping("/api/v1/provider/samples")
public class APIProviderSampleController {

  @Operation(summary = "[provider-sample-1] Provider Sample API", description = "", security = {@SecurityRequirement(name = "bearerAuth")})
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR")
  })
  @GetMapping
  public ResponseEntity<?> sample1(@Parameter(hidden = true) OAuth2Authentication oAuth2Authentication,
                                   HttpServletRequest request,
                                   Locale locale) {

    // TODO PROVIDER USER CHECK
    ValidUtils.isForbiddenProvider(oAuth2Authentication);

    return ResponseEntity.ok("OK");
  }
}
