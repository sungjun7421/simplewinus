package com.logisall.anyware.api.web.account;

import com.logisall.anyware.core.service.niceid.NiceIdService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Nice 본인인증", description = "Nice 본인인증 API")
@RestController
@RequestMapping("/api/v1/nice-id")
public class APINiceIdController {

  @Autowired
  private NiceIdService niceIdService;

  @Operation(summary = "[nice-id-1] 나이스 본인인증 팝업 요소",
      description = "Back 가이드 : https://www.niceid.co.kr/common/file_down.jsp?path=/documents/CheckPlusSafe_JSP.zip&file=CheckPlusSafe_JSP.zip" +
          "\n Mobile Webview 가이드 : https://www.niceid.co.kr/common/file_down.jsp?path=/documents/WebViewGuide.zip&file=WebViewGuide.zip")
  @GetMapping(value = "check", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> check() {
    return ResponseEntity.ok(niceIdService.check());
  }

  @Operation(summary = "[nice-id-2] 나이스 본인인증 성공")
  @GetMapping(value = "success", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> success(@RequestParam String encodeData,
                                   @RequestParam String requestNumber) {
    return ResponseEntity.ok(niceIdService.success(encodeData, requestNumber));
  }

  @Operation(summary = "[nice-id-3] 나이스 본인인증 실패")
  @GetMapping(value = "fail", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> fail(@RequestParam String encodeData) {
    return ResponseEntity.ok(niceIdService.fail(encodeData));
  }

}
