package com.logisall.anyware.api.service.board.faq;

import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.resbody.board.FaqResBody;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public interface FaqAPIService {

    PagedModel<FaqResBody> pagedResources(Locale locale, HttpServletRequest request, Filter filter, Long[] idCategories);
    EntityModel<FaqResBody> resource(Locale locale, HttpServletRequest request, Long id);
}
