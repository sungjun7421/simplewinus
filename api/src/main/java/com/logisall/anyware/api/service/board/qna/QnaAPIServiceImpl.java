package com.logisall.anyware.api.service.board.qna;

import com.logisall.anyware.api.service.account.user.UserAPIService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.NoContentException;
import com.logisall.anyware.core.domain.board.qna.Answer;
import com.logisall.anyware.core.domain.board.qna.Qna;
import com.logisall.anyware.core.domain.board.qna.QnaPredicate;
import com.logisall.anyware.core.domain.board.qna.QnaRepository;
import com.logisall.anyware.core.domain.board.qna.category.QnaCategoryRepository;
import com.logisall.anyware.core.domain.file.FileEntity;
import com.logisall.anyware.core.domain.file.FileEntityRepository;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.file.FileMeta;
import com.logisall.anyware.core.model.file.FileUploadService;
import com.logisall.anyware.core.model.reqbody.board.QuestionReqBody;
import com.logisall.anyware.core.model.resbody.board.QnaResBody;
import com.logisall.anyware.core.service.board.qna.QnaService;
import com.logisall.anyware.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@Service
public class QnaAPIServiceImpl implements QnaAPIService {

  @Autowired
  private QnaService qnaService;

  @Autowired
  private PagedResourcesAssembler pagedResourcesAssembler;

  @Autowired
  private QnaRepository qnaRepository;

  @Autowired
  private UserAPIService userAPIService;

  @Autowired
  private QnaCategoryRepository qnaCategoryRepository;

  @Autowired
  private FileEntityRepository fileEntityRepository;

  @Autowired
  private FileUploadService fileUploadService;

  @Override
  @Transactional(readOnly = true)
  public PagedModel<QnaResBody> pagedResources(Locale locale, HttpServletRequest request, Filter filter, Long[] idCategories, String email) {

    Page<Qna> page = qnaRepository.findAll(
        QnaPredicate.getInstance()

            .search(filter.getQuery())
            .startDate(filter.getStartDate())
            .endDate(filter.getEndDate())
            .email(email)
            .categories(idCategories)
            .delete(false)

            .active(true)
            .values(),
        filter.getPageable());

    page.forEach(Qna::lazy);

    List<QnaResBody> list = page.getContent().stream()
        .map(qna -> qna.toBody(locale)).collect(Collectors.toList());

    Page<QnaResBody> qPage = new PageImpl<>(list, filter.getPageable(), page.getTotalElements());

    return pagedResourcesAssembler.toModel(qPage);
  }

  @Override
  @Transactional(readOnly = true)
  public EntityModel<QnaResBody> resource(Locale locale, HttpServletRequest request, Long id, String email) {

    Qna qna = qnaRepository.getByUser(id, email);

    if (qna == null || qna.isDelete()) {
      throw new NoContentException();
    }

    if (qna == null || !qna.isActive()) {
      throw new BadRequestException();
    }
    QnaResBody qnaBody = qna.toBody(locale);
    return new EntityModel<>(qnaBody);
  }

  @Override
  @Transactional
  public Qna question(QuestionReqBody questionReqBody, String userEmail) {

    User user = userAPIService.get(userEmail);
    Qna qna = questionReqBody.toQna(user);

    List<FileEntity> fileEntities = new ArrayList<>();
    if (questionReqBody.getImages() != null
        && questionReqBody.getImages().size() > 0) {

      for (String fileUrl : questionReqBody.getImages()) {
        FileEntity fileEntitySave = fileEntityRepository.findOneByUrl(fileUrl);
        fileEntities.add(fileEntitySave);
      }
    }
    qna.setFiles(fileEntities);

    if (qna.getIdCategory() != null) {
      qnaCategoryRepository.findById(qna.getIdCategory()).ifPresent(category -> {
        qna.getCategories().add(category);
      });
    }
    Qna createdQna = qnaService.create(qna);

    if (createdQna == null) {
      throw new RuntimeException();
    }
    return createdQna;
  }

  @Override
  @Transactional
  public Qna questionByNoMember(QuestionReqBody questionReqBody) {

    Qna qna = questionReqBody.toQnaNoMember();

    List<FileEntity> fileEntities = new ArrayList<>();
    if (questionReqBody.getImages() != null
        && questionReqBody.getImages().size() > 0) {

      for (String fileUrl : questionReqBody.getImages()) {
        FileEntity fileEntitySave = fileEntityRepository.findOneByUrl(fileUrl);
        fileEntities.add(fileEntitySave);
      }
    }
    qna.setFiles(fileEntities);

    if (qna.getIdCategory() != null) {
      qnaCategoryRepository.findById(qna.getIdCategory()).ifPresent(category -> {
        qna.getCategories().add(category);
      });
    }
    Qna createdQna = qnaService.create(qna);

    if (createdQna == null) {
      throw new RuntimeException();
    }
    return createdQna;
  }

  @Override
  @Transactional
  public void delete(Long id, String email) {

    Qna qna = qnaRepository.getByUser(id, email);

    if (qna == null || qna.isDelete()) {
      throw new NoContentException();
    }
    qna.setDelete(true);
  }

  @Override
  public FileMeta uploadFile(MultipartFile file) {
    return fileUploadService.uploadFile(file);
  }
}
