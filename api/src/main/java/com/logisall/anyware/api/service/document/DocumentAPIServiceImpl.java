package com.logisall.anyware.api.service.document;

import com.logisall.anyware.core.domain.document.Document;
import com.logisall.anyware.core.domain.document.DocumentRepository;
import com.logisall.anyware.core.model.resbody.document.DocumentResBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Slf4j
@Service
public class DocumentAPIServiceImpl implements DocumentAPIService {

  @Autowired
  private DocumentRepository documentRepository;

  @Override
  @Transactional(readOnly = true)
  public EntityModel<DocumentResBody> resource(Locale locale, HttpServletRequest request, Document.Type type) {

    Document document = documentRepository.latestDoc(type);

    if (document == null) {
      return new EntityModel<>(new DocumentResBody());
    } else {
      return new EntityModel<>(document.toBody(locale));
    }
  }
}
