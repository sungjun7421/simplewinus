package com.logisall.anyware.api.service.commerce.buyer;

import com.logisall.anyware.core.model.resbody.commerce.level.BuyerLevelResBody;

import java.util.Locale;

public interface BuyerLevelAPIService {

  // R
  BuyerLevelResBody findByBuyerId(Long buyerId, Locale locale);
}
