package com.logisall.anyware.api.web.board;

import com.logisall.anyware.api.service.board.qna.QnaAPIService;
import com.logisall.anyware.api.service.board.qna.QnaCategoryAPIService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.UnAuthorizedException;
import com.logisall.anyware.core.domain.board.qna.Qna;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.reqbody.board.QuestionReqBody;
import com.logisall.anyware.core.model.resbody.ResultResBody;
import com.logisall.anyware.core.utils.StringUtils;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;

/**
 * 1:1 문의
 */
@Tag(name = "Q&A", description = "1:1 문의 API")
@RestController
@RequestMapping("/api/v1/qnas")
public class APIQnaController {

    @Autowired
    private QnaAPIService qnaAPIService;

    @Autowired
    private QnaCategoryAPIService qnaCategoryAPIService;

    @Operation(summary = "[qna-1] Q&A 목록",
        security = {@SecurityRequirement(name = "bearerAuth")})
    @Parameters(value = {
        @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code 언어코드", example = "ko-KR"),
        @Parameter(in = ParameterIn.QUERY, name = "size", description = "Page Size 페이지 크기 (default : 20)", example = "20"),
        @Parameter(in = ParameterIn.QUERY, name = "page", description = "현재 페이지 0부터 (Current Page)  현재 페이지 (default : 0)", example = "0"),
        @Parameter(in = ParameterIn.QUERY, name = "sort", description = "Sort Page 정렬", example = "createdDate,desc"),
        @Parameter(in = ParameterIn.QUERY, name = "idCategories", description = "카테고리 ID(Category ID)", example = "1"),
    })
    @GetMapping
    public ResponseEntity<?> page(@Parameter(hidden = true) @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                                  @Parameter(description = "검색어 (Search Word)") @RequestParam(required = false, defaultValue = "") String query,
                                  @Parameter(description = "시작시간 (Start Time) (yyyy-MM-dd HH:mm:ss)") @RequestParam(required = false) String startDate,
                                  @Parameter(description = "종료시간 (End Time) (yyyy-MM-dd HH:mm:ss)") @RequestParam(required = false) String endDate,
                                  @Parameter(description = "카테고리 ID(Category ID)") @RequestParam(required = false) Long[] idCategories,
                                  @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication,
                                  HttpServletRequest request,
                                  Locale locale) {

        if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
            throw new UnAuthorizedException();
        }

        Filter filter = new Filter(pageable, query, startDate, endDate);
        return ResponseEntity.ok(qnaAPIService.pagedResources(locale, request, filter, idCategories, oAuth2Authentication.getPrincipal().toString()));
    }

    @Operation(summary = "[qna-2] Q&A 상세", security = {@SecurityRequirement(name = "bearerAuth")})
    @Parameters(value = {
        @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code 언어코드", example = "ko-KR"),
    })
    @GetMapping("/{id}")
    public ResponseEntity<?> get(@Parameter(description = "Q&A ID") @PathVariable Long id,
                                 @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication,
                                 HttpServletRequest request,
                                 Locale locale) {

        if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
            throw new UnAuthorizedException();
        }
        return ResponseEntity.ok(qnaAPIService.resource(locale, request, id, oAuth2Authentication.getPrincipal().toString()));
    }

    @Operation(summary = "[qna-3] 질문하기",
        security = {@SecurityRequirement(name = "bearerAuth")})
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> post(@RequestBody QuestionReqBody questionReqBody,
                                  @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

        if (questionReqBody == null
            || StringUtils.isEmpty(questionReqBody.getTitle())
            || StringUtils.isEmpty(questionReqBody.getContent())) {
            throw new BadRequestException();
        }
        Qna qna = null;
        if (questionReqBody.isMember()) {
            if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
                throw new UnAuthorizedException();
            }
            qna = qnaAPIService.question(questionReqBody, oAuth2Authentication.getPrincipal().toString());
        } else {
            qna = qnaAPIService.questionByNoMember(questionReqBody);
        }

        URI redirect = null;
        try {
            if (qna != null) {
                redirect = new URI("/api/v1/qnas/" + qna.getId());
                return ResponseEntity.created(redirect).body(ResultResBody.of(true, "문의하기가 등록되었습니다."));
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return ResponseEntity.badRequest().build();
    }

    @Operation(summary = "[qna-4] 내 문의내용 삭제하기",
        security = {@SecurityRequirement(name = "bearerAuth")})
    @DeleteMapping(value = "{idQna}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@Parameter(description = "Q&A ID") @PathVariable Long idQna,
                                    @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

        ValidUtils.isForbidden(oAuth2Authentication);

        qnaAPIService.delete(idQna, oAuth2Authentication.getPrincipal().toString());
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "[qna-5] Q&A 카테고리 목록")
    @Parameters(value = {
        @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR")
    })
    @GetMapping(value = "/categories")
    public ResponseEntity<?> categories(Locale locale) {
        return ResponseEntity.ok(qnaCategoryAPIService.categories(locale));
    }

    @Operation(summary = "[qna-6] Q&A 파일업로드",
        security = {@SecurityRequirement(name = "bearerAuth")})
    @PostMapping
    public ResponseEntity<?> file(@Parameter(description = "File") @RequestParam("file") MultipartFile file) {

        return ResponseEntity.ok(qnaAPIService.uploadFile(file));
    }
}
