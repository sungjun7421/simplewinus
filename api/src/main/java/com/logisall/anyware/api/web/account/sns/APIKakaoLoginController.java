package com.logisall.anyware.api.web.account.sns;

import com.logisall.anyware.api.config.oauth.JwtUtils;
import com.logisall.anyware.api.service.account.sns.SNSAPIService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.UnAuthorizedException;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.domain.user.sns.SNSStatus;
import com.logisall.anyware.core.model.resbody.account.SNSResBody;
import com.logisall.anyware.core.model.sns.Kakao;
import com.logisall.anyware.core.model.sns.SNSReqBody;
import com.logisall.anyware.core.model.sns.SNSType;
import com.logisall.anyware.core.service.account.SNSKakaoAPIService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Tag(name = "Kakao Talk Login", description = "카카오톡 로그인 API")
@RestController
@RequestMapping("/api/v1/kakao-login")
public class APIKakaoLoginController {

  @Autowired
  private SNSKakaoAPIService snsKakaoAPIService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private SNSAPIService snsAPIService;

  @Autowired
  private JwtUtils jwtUtils;

  @Operation(summary = "[sns-kakao-1] 웹용 카카오 연동")
  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> kakao(@RequestBody SNSReqBody requestBody) {

    if (requestBody == null) {
      throw new BadRequestException();
    }

    String code = requestBody.getCode();
    String redirectUri = requestBody.getRedirectUri();

    return kakaoAuth(snsAPIService.connectKakao(code, redirectUri));
  }

  @Operation(summary = "[sns-kakao-2] Native용 카카오 연동")
  @PostMapping(value = "native", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> kakaoNative(@RequestBody Kakao kakao) {
    return kakaoAuth(kakao);
  }


  private ResponseEntity<?> kakaoAuth(Kakao kakao) {
    SNSStatus status = snsKakaoAPIService.getStatus(kakao);
    SNSResBody body = SNSResBody.of(true);

    log.debug("status ::: {} ", status);
    log.debug("kakao ::: {} ", kakao);

    switch (status) {
      // AccessToken 발급
      case CONNECT:
      case LINKED:
        User user = userRepository.findBySocialIdKakaoId(kakao.getId()).orElseThrow(RuntimeException::new);
        if (user.isWithd()) {
          throw new UnAuthorizedException("이용할수 없는 계정입니다.");
        }
        body.setSnsType(SNSType.KAKAO);
        body.setId(kakao.getId());
        body.setName(kakao.getNickName());
        body.setEmail(kakao.getEmail());
        body.setAccess_token(jwtUtils.generator(user));
        return ResponseEntity.ok(body);

      case LEAVED_ACCOUNT:

        body = SNSResBody.of(false);
        body.setMessage(SNSStatus.LEAVED_ACCOUNT.name());
        body.setSnsType(SNSType.KAKAO);
        body.setId(kakao.getId());
        body.setName(kakao.getNickName());
        body.setEmail(kakao.getEmail());
        body.setVerifiedEmail(kakao.isKaccountEmailVerified());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);

      case NOT_MATCH_SNS:

        body = SNSResBody.of(false);
        body.setMessage(SNSStatus.NOT_MATCH_SNS.name());
        body.setSnsType(SNSType.KAKAO);
        body.setId(kakao.getId());
        body.setName(kakao.getNickName());
        body.setEmail(kakao.getEmail());
        body.setVerifiedEmail(kakao.isKaccountEmailVerified());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);

      case NOT_EXISTED_ACCOUNT:

        body = SNSResBody.of(false);
        body.setMessage(SNSStatus.NOT_EXISTED_ACCOUNT.name());
        body.setSnsType(SNSType.KAKAO);
        body.setId(kakao.getId());
        body.setName(kakao.getNickName());
        body.setEmail(kakao.getEmail());
        body.setVerifiedEmail(kakao.isKaccountEmailVerified());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);

      case NOT_PROVIDED_EMAIL:

        body = SNSResBody.of(false);
        body.setMessage(SNSStatus.NOT_PROVIDED_EMAIL.name());
        body.setSnsType(SNSType.KAKAO);
        body.setId(kakao.getId());
        body.setName(kakao.getNickName());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);

      default:
        throw new BadRequestException("카카오 로그인에서 나올수 없는 경우");
    }
  }
}
