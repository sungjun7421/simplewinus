package com.logisall.anyware.api.web.board;

import com.logisall.anyware.api.service.board.post.PostAPIService;
import com.logisall.anyware.core.domain.board.post.Post;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.resbody.BaseStatusResBody;
import com.google.common.collect.Lists;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Tag(name = "게시판", description = "Post(Board) API")
@Slf4j
@RestController
@RequestMapping("/api/v1/posts")
public class APIPostController {


    @Autowired
    private PostAPIService postAPIService;

    @Operation(summary = "[post-1] 게시판 페이징")
    @Parameters(value = {
        @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR"),
        @Parameter(in = ParameterIn.QUERY, name = "size", description = "페이지 사이즈 (Size Page)  (default : 20)", example = "20"),
        @Parameter(in = ParameterIn.QUERY, name = "page", description = "현재 페이지 0부터 (Current Page)  (default : 0)", example = "0"),
        @Parameter(in = ParameterIn.QUERY, name = "sort", description = "정렬 (Sort Page)", example = ""),
        @Parameter(in = ParameterIn.QUERY, name = "type", description = "게시판 유형(Type of Post)", example = "NOTICE"),
        @Parameter(in = ParameterIn.QUERY, name = "idCategories", description = "카테고리 ID(Category ID)", example = "1"),
        @Parameter(in = ParameterIn.QUERY, name = "top", description = "상단고정 유무(Fix Top)", example = "true"),
    })
    @GetMapping
    public ResponseEntity<?> page(@Parameter(hidden = true) @PageableDefault(size = 20, sort = {"top", "regDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                                  @Parameter(description = "검색어 (Search Word)") @RequestParam(required = false, defaultValue = "") String query,
                                  @Parameter(description = "시작시간 (Start Time) (yyyy-MM-dd HH:mm:ss)") @RequestParam(required = false) String startDate,
                                  @Parameter(description = "종료시간 (End Time) (yyyy-MM-dd HH:mm:ss)") @RequestParam(required = false) String endDate,
                                  @Parameter(description = "게시판 유형(Type of Post)") @RequestParam Post.Type type,
                                  @Parameter(description = "카테고리 ID(Category ID)") @RequestParam(required = false) Long[] idCategories,
                                  @Parameter(description = "상단고정 유무(Fix Top)") @RequestParam(required = false) Boolean top,
                                  HttpServletRequest request,
                                  Locale locale) {

        Filter filter = new Filter(pageable, query, startDate, endDate);
        return ResponseEntity.ok(postAPIService.pagedResources(locale, request, filter, type, idCategories, top));
    }

    @Operation(summary = "[post-2] 게시판 상세")
    @Parameters(value = {
        @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR")
    })
    @GetMapping(value = "/{id}")
    public ResponseEntity<?> page(@Parameter(description = "Post ID") @PathVariable Long id,
                                  HttpServletRequest request,
                                  Locale locale) {
        if (request.getMethod().equals("GET")) {
            postAPIService.pageView(id);
        }
        return ResponseEntity.ok(postAPIService.resource(locale, request, id));
    }

    @Operation(summary = "[post-3] 게시판 카테고리")
    @Parameters(value = {
        @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR"),
    })
    @GetMapping(value = "/categories")
    public ResponseEntity<?> categories(@Parameter(description = "게시물(게시판) 유형") @RequestParam Post.Type type,
                                        Locale locale) {
        return ResponseEntity.ok(postAPIService.categories(locale, type));
    }

    @Operation(summary = "[post-4] 게시판 유형")
    @Parameters(value = {
        @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR")
    })
    @GetMapping(value = "/types")
    public ResponseEntity<?> types() {

        List<BaseStatusResBody> list = Lists.newArrayList(Post.Type.values()).stream().map(type -> {
            BaseStatusResBody baseStatusResBody = new BaseStatusResBody();
            baseStatusResBody.setKey(type.toString());
            baseStatusResBody.setValue(type.getValue());
            return baseStatusResBody;
        }).collect(Collectors.toList());

        return ResponseEntity.ok(list);
    }
}
