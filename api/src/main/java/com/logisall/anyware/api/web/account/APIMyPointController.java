package com.logisall.anyware.api.web.account;

import com.logisall.anyware.api.service.account.user.UserAPIService;
import com.logisall.anyware.api.service.commerce.buyer.BuyerLevelAPIService;
import com.logisall.anyware.api.service.commerce.point.PointAPIService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.ResponseErrorCode;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.model.resbody.account.MyPointInfoResBody;
import com.logisall.anyware.core.model.resbody.commerce.level.BuyerLevelResBody;
import com.logisall.anyware.core.model.resbody.commerce.point.PointOfMemberResBody;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@RestController
@RequestMapping("/api/v1/my-point")
public class APIMyPointController {

  @Autowired
  private UserAPIService userAPIService;

  @Autowired
  private BuyerLevelAPIService buyerLevelAPIService;

  @Autowired
  private PointAPIService pointAPIService;

  @Operation(summary = "[my-point-1] 적립금 정보",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> myPoint(Locale locale,
                                   @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    ValidUtils.isForbidden(oAuth2Authentication);

    User user = userAPIService.get(oAuth2Authentication.getPrincipal().toString());

    if (user == null) {
      throw new BadRequestException(ResponseErrorCode.NOT_EXIST_USER.name());
    }

    Long idBuyer = user.getRelativeBuyer().getId();

    BuyerLevelResBody buyerLevelResBody = buyerLevelAPIService.findByBuyerId(idBuyer, locale);
    buyerLevelResBody.setIdBuyer(idBuyer);

    PointOfMemberResBody pointOfMemberResBody = pointAPIService.getDetailPoint(idBuyer);

    return ResponseEntity.ok(
        new EntityModel<>(MyPointInfoResBody.builder()
            .level(buyerLevelResBody)
            .point(pointOfMemberResBody)
            .build())
    );
  }
}
