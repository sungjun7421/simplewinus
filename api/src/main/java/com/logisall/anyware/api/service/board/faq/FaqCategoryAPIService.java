package com.logisall.anyware.api.service.board.faq;


import com.logisall.anyware.core.model.resbody.board.FaqCategoryResBody;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;

import java.util.Locale;

public interface FaqCategoryAPIService {
    CollectionModel<EntityModel<FaqCategoryResBody>> categories(Locale locale);
}
