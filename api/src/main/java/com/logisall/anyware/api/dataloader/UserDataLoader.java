package com.logisall.anyware.api.dataloader;

import com.logisall.anyware.core.config.database.PwdEncConfig;
import com.logisall.anyware.core.domain.user.*;
import com.logisall.anyware.core.service.account.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 초기 유저 등록 세팅 값
 */

@Slf4j
@Component
@Order(1)
public class UserDataLoader implements CommandLineRunner {

  @Autowired
  private AuthorityRepository authorityRepository;

  @Autowired
  private UserService userService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private PwdEncConfig pwdEncConfig;

  @Override
  public void run(String... args) throws Exception {
    log.debug("UserDataLoader ::: {}", authorityRepository.count());

    // 권한이 없으면 처리
    if (authorityRepository.count() == 0) {
      authorityRepository.save(Authority.of(Authority.Role.SUPER));
      authorityRepository.save(Authority.of(Authority.Role.ADMIN));
      authorityRepository.save(Authority.of(Authority.Role.USER));
      authorityRepository.save(Authority.of(Authority.Role.MANAGER));
    }

    User user = userRepository.findByEmail("super@super.com").orElse(null);
    log.debug("> super user : {}", user);
    if (user == null) {
      createUser("super@super.com", "admin12345", "관리자", "0103456789", Authority.Role.SUPER);
    }


    User test1 = userRepository.findByEmail("test@logisall.com").orElse(null);
    log.debug("> test1 user : {}", test1);
    if (test1 == null) {
      createUser("test@logisall.com", "abcd1234", "테스터", "01011112222", Authority.Role.USER);
    }

  }

  private User createUser(String email, String password, String fullName, String mobile, Authority.Role role) {
    User createUser = new User();

    createUser.setEmail(email);
    createUser.setPassword(password);
    createUser.setFullName(fullName);
    createUser.setMobile(mobile);

    createUser.setTermsAgree(new TermsAgree());
    createUser.getTermsAgree().setTaService(true);
    createUser.getTermsAgree().setTaPrivacy(true);

    createUser.setRole(role);
    return userService.create(createUser);
  }
}
