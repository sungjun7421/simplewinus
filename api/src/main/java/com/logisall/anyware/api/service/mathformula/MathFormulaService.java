package com.logisall.anyware.api.service.mathformula;

import net.objecthunter.exp4j.tokenizer.UnknownFunctionOrVariableException;

import java.math.BigDecimal;
import java.util.Map;

public interface MathFormulaService {

  BigDecimal calculation(String expression, Map<String, Double> variables, String... variableNames) throws UnknownFunctionOrVariableException;
  BigDecimal calculationX(String expression, double valueX) throws UnknownFunctionOrVariableException;
  BigDecimal calculationXY(String expression, double valueX, double valueY) throws UnknownFunctionOrVariableException;
  BigDecimal calculationXYZ(String expression, double valueX, double valueY, double valueZ) throws UnknownFunctionOrVariableException;

  void valid(String formula, String key) throws RuntimeException;
  void validate(String formula, String key);
}
