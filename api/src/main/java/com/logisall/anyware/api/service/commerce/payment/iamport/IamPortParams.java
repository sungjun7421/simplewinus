package com.logisall.anyware.api.service.commerce.payment.iamport;

import com.logisall.anyware.core.domain.commerce.PayMethod;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@ToString
public class IamPortParams implements Serializable {

    private static final long serialVersionUID = 2499369411215717479L;

    private String pg; // (선택항목)
    private String pay_method; // (필수항목)
    private boolean escrow; // (선택항목)
    private String merchant_uid;// (필수항목)
    private String name; // (선택항목)
    private BigDecimal amount; // (필수항목)
//    private String custom_data; // (선택항목)
//    private int tax_free;  // (선택항목)
//    private int vat;
//    private String currency;
//    private String language = "ko";  // (선택항목)

    private String buyer_name; // (선택항목)
    private String buyer_tel; // (필수항목)
    private String buyer_email; // (선택항목)
    private String buyer_tel2; // (선택항목)
//    private String buyer_addr; // (선택항목)
//    private String buyer_postcode; // (선택항목)
//    private String notice_url; // (선택항목)
//    private Object display; // (선택항목) 구매자에게 제공되는 결제창 화면에 대한 UI옵션.
    private String m_redirect_url;

    public PayMethod toPayMethod() {
        if (this.pay_method.equals("card")) {
            return PayMethod.CARD;
        } else if (this.pay_method.equals("trans")) {
            return PayMethod.TRANS;
        } else if (this.pay_method.equals("vbank")) {
            return PayMethod.VBANK;
        } else {
//            if (this.pay_method.equals("phone")) {
//            return PayMethod.PHONE;
//        } else if (this.pay_method.equals("samsung")) {
//            return PayMethod.SAMSUNG;
//        } else if (this.pay_method.equals("kpay")) {
//            return PayMethod.KPAY;
//        } else if (this.pay_method.equals("cultureland")) {
//            return PayMethod.CULTURELAND;
//        } else if (this.pay_method.equals("smartculture")) {
//            return PayMethod.SMARTCULTURE;
//        } else if (this.pay_method.equals("happymoney")) {
//            return PayMethod.HAPPYMONEY;
//        } else if (this.pay_method.equals("booknlife")) {
//            return PayMethod.BOOKNLIFE;
        }
        return null;
    }
}
