package com.logisall.anyware.api.service.board.faq;

import com.logisall.anyware.core.config.exception.NoContentException;
import com.logisall.anyware.core.config.exception.NotFoundException;
import com.logisall.anyware.core.domain.board.faq.Faq;
import com.logisall.anyware.core.domain.board.faq.FaqPredicate;
import com.logisall.anyware.core.domain.board.faq.FaqRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.resbody.board.FaqResBody;
import com.logisall.anyware.core.service.setting.AppSettingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@Service
public class FaqAPIServiceImpl implements FaqAPIService {

    @Autowired
    private FaqRepository faqRepository;

    @Autowired
    private AppSettingService appSettingService;

    @Autowired
    private PagedResourcesAssembler pagedResourcesAssembler;

    @Override
    @Transactional(readOnly = true)
    public PagedModel<FaqResBody> pagedResources(Locale locale, HttpServletRequest request, Filter filter, Long[] idCategories) {

        Locale defaultLocale = appSettingService.getDefaultLocale();

        Page<Faq> faqPage = faqRepository.findAll(
                FaqPredicate.getInstance()
                        .search(filter.getQuery())
                        .startDate(filter.getStartDate())
                        .endDate(filter.getEndDate())
                        .active(true)
                        .categories(idCategories)
                        .locale(locale, defaultLocale)
                        .values()
                ,
                filter.getPageable());

        List<FaqResBody> list = faqPage.getContent().stream()
                .map(faq -> faq.toBody(locale))
                .collect(Collectors.toList());

        Page<FaqResBody> page = new PageImpl<>(list, filter.getPageable(), faqPage.getTotalElements());

        return pagedResourcesAssembler.toModel(page);
    }

    @Override
    @Transactional(readOnly = true)
    public EntityModel<FaqResBody> resource(Locale locale, HttpServletRequest request, Long id) {

        if (!faqRepository.existsById(id)) {
            throw new NotFoundException();
        }

        FaqResBody faqBody = faqRepository.findById(id)
                .filter(Faq::isActive)
                .map(faq -> {
                    return faq.toBody(locale);
                }).orElseThrow(NoContentException::new);

        return new EntityModel<>(faqBody);
    }
}
