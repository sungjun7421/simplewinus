package com.logisall.anyware.api.web.commerce.point;

import com.logisall.anyware.api.service.commerce.point.PointAPIService;
import com.logisall.anyware.core.config.exception.ForbiddenException;
import com.logisall.anyware.core.domain.commerce.point.Point;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.service.commerce.point.PointService;
import com.google.common.collect.ImmutableMap;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@Slf4j
@Tag(name = "Point", description = "적립금 API")
@RestController
@RequestMapping("/api/v1/point")
public class APIPointController {

    @Autowired
    private PointService pointService;

    @Autowired
    private PointAPIService pointAPIService;

    // 총합
    /**
     * [point-1] Total point
     */
    @Operation(summary = "[point-1] Get total point of user ", description = "Get total point of user",
        security = {@SecurityRequirement(name = "bearerAuth")},
        responses = @ApiResponse(
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                schema = @Schema(example = "{\n" +
                    "    \"data\": \"[total point]\"\n" +
                    "}")
            )))
    @GetMapping("/total")
    public ResponseEntity<?> total(OAuth2Authentication oAuth2Authentication) {

        if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
            throw new ForbiddenException();
        }
        return ResponseEntity.ok(pointService.total(oAuth2Authentication.getPrincipal().toString()));
    }

    // 페이지
    /**
     * [point-2] Point history
     */
    @Operation(summary = "[point-1] Get point history ", description = "Get total point of user",
        security = {@SecurityRequirement(name = "bearerAuth")},
        responses = @ApiResponse(
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                schema = @Schema(example = "{\n" +
                    "    \"list_point\": \"[list point history]\"\n" +
                    "}")
            )))
    @Parameters(value = {
        @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR"),
        @Parameter(in = ParameterIn.QUERY, name = "size", description = "Size Page (default : 20)", example = "20"),
        @Parameter(in = ParameterIn.QUERY, name = "page", description = "Current Page (default : 0)", example = "0"),
        @Parameter(in = ParameterIn.QUERY, name = "sort", description = "Sort Page", example = "createdDate, desc"),
    })
    @GetMapping
    public ResponseEntity<?> page(@Parameter(hidden = true) @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                                  @Parameter(description = "Search word") @RequestParam(required = false, defaultValue = "") String query,
                                  @Parameter(description = "Start Time (yyyy-MM-dd HH:mm:ss)") @RequestParam(required = false) String startDate,
                                  @Parameter(description = "End Time (yyyy-MM-dd HH:mm:ss)") @RequestParam(required = false) String endDate,
                                  @Parameter(description = "Point Status") @RequestParam(required = false) Point.Status status,
                                  @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication,
                                  Locale locale) {

        if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
            throw new ForbiddenException();
        }

        Filter filter = new Filter(pageable, query, startDate, endDate);

        return ResponseEntity.ok(
                ImmutableMap
                        .builder()
                        .put("list_point", pointAPIService.pagedResources(locale, filter, status, oAuth2Authentication.getPrincipal().toString()))
                        .build()
        );
    }
}
