package com.logisall.anyware.api.web.document;

import com.logisall.anyware.api.service.document.DocumentAPIService;
import com.logisall.anyware.core.domain.document.Document;
import com.logisall.anyware.core.model.resbody.BaseStatusResBody;
import com.google.common.collect.Lists;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@Tag(name = "Documents", description = "약관/개인정보 문서 API")
@RestController
@RequestMapping("/api/v1/documents")
public class APIDocumentController {

  @Autowired
  private DocumentAPIService documentAPIService;

  @Operation(summary = "[document-1] 문서 상세", description = "타입별 상세")
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR")
  })
  @GetMapping("/type/{type}")
  public ResponseEntity<?> document(@Parameter(description = "문서 Type") @PathVariable Document.Type type,
                                    HttpServletRequest request,
                                    Locale locale) {

    return ResponseEntity.ok(new EntityModel<>(documentAPIService.resource(locale, request, type)));
  }

  @Operation(summary = "[document-2] 문서타입")
  @GetMapping(value = "/types", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> types() {

    List<BaseStatusResBody> list = Lists.newArrayList(Document.Type.values())
        .stream().map(type -> {
          BaseStatusResBody baseStatusResBody = new BaseStatusResBody();
          baseStatusResBody.setKey(type.toString());
          baseStatusResBody.setValue(type.getValue());
          return baseStatusResBody;
        }).collect(Collectors.toList());

    return ResponseEntity.ok(list);
  }
}
