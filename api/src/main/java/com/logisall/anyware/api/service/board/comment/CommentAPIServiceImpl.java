package com.logisall.anyware.api.service.board.comment;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.UnAuthorizedException;
import com.logisall.anyware.core.config.exception.crud.UpdateErrorException;
import com.logisall.anyware.core.domain.board.comment.Comment;
import com.logisall.anyware.core.domain.board.comment.CommentRepository;
import com.logisall.anyware.core.domain.board.event.Event;
import com.logisall.anyware.core.domain.board.post.Post;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.reqbody.board.CommentReqBody;
import com.logisall.anyware.core.model.resbody.board.CommentResBody;
import com.logisall.anyware.core.service.account.user.UserService;
import com.logisall.anyware.core.service.board.comment.CommentService;
import com.logisall.anyware.core.service.board.event.EventService;
import com.logisall.anyware.core.service.board.post.PostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CommentAPIServiceImpl implements CommentAPIService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private EventService eventService;

    @Autowired
    private PostService postService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private UserService userService;

    @Autowired
    private PagedResourcesAssembler pagedResourcesAssembler;

    @Override
    @Transactional(readOnly = true)
    public PagedModel<?> pagedResources(Locale locale, Filter filter,
                                        Long idUser,
                                        Comment.Type type,
                                        Long idContent,
                                        Long idParent) {

        Page<Comment> page = commentService.page(filter, idUser, type, idContent, idParent, true);

        List<CommentResBody> list = page.getContent().stream()
                .map(comment -> comment.toBody(locale))
                .collect(Collectors.toList());

        return pagedResourcesAssembler.toModel(new PageImpl<>(list, filter.getPageable(), page.getTotalElements()));
    }

    @Override
    @Transactional(readOnly = true)
    public EntityModel<CommentResBody> resource(Locale locale, Long id) {
        Comment comment = commentService.get(locale, id);
        return new EntityModel<>(comment.toBody(locale));
    }

    @Override
    @Transactional
    public Comment register(Locale locale, CommentReqBody commentReqBody, String email) {

        if (commentService == null) {
            throw new BadRequestException();
        }

        User user = userService.get(email);

        if(user == null) {
            throw new UnAuthorizedException();
        }
        Comment comment = new Comment();
        comment.setRelativeUser(user);
        comment.setContent(commentReqBody.getContent());

        if (commentReqBody.getIdParent() != null) {

            Comment.Type type = commentService.get(locale, commentReqBody.getIdParent())
                    .getType();

            if (type == null
                    || !type.equals(commentReqBody.getType())) {
                throw new BadRequestException();
            }

            if (type == Comment.Type.EVENT) {

                Event event = eventService.get(locale, commentReqBody.getIdContent());
                comment.setRelativeEvent(event);
                comment.setRelativeParent(commentService.get(locale, commentReqBody.getIdParent()));
                comment.setType(Comment.Type.EVENT);
            } else {

                Post post = postService.get(locale, commentReqBody.getIdContent());
                comment.setRelativePost(post);
                comment.setRelativeParent(commentService.get(locale, commentReqBody.getIdParent()));
                comment.setType(Comment.Type.POST);
            }

        } else {

            Comment.Type type = commentReqBody.getType();

            if (type == Comment.Type.EVENT) {

                Event event = eventService.get(locale, commentReqBody.getIdContent());
                comment.setRelativeEvent(event);
                comment.setType(Comment.Type.EVENT);
            } else {

                Post post = postService.get(locale, commentReqBody.getIdContent());
                comment.setRelativePost(post);
                comment.setType(Comment.Type.POST);
            }
        }
        return commentRepository.save(comment);
    }

    @Override
    @Transactional
    public Comment modify(CommentReqBody commentReqBody, Long id, String email) {

        if (id == null) {
            throw new BadRequestException();
        }

        return commentRepository.findById(id)
                .map(ori -> {
                    ori.setContent(commentReqBody.getContent());
                    return commentRepository.save(ori);
                }).orElseThrow(() -> new UpdateErrorException(id, Comment.class.getName()));
    }

    @Override
    @Transactional
    public void delete(Long id, String email) {

        if (id == null) {
            throw new BadRequestException();
        }

        commentRepository.findById(id)
                .map(ori -> {
                    ori.setDelete(true);
                    return commentRepository.save(ori);
                }).orElseThrow(() -> new UpdateErrorException(id, Comment.class.getName()));
    }
}
