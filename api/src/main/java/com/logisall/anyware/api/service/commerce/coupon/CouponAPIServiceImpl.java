package com.logisall.anyware.api.service.commerce.coupon;

import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.NotFoundException;
import com.logisall.anyware.core.config.exception.crud.UpdateErrorException;
import com.logisall.anyware.core.domain.commerce.coupon.Coupon;
import com.logisall.anyware.core.domain.commerce.coupon.CouponPredicate;
import com.logisall.anyware.core.domain.commerce.coupon.CouponRepository;
import com.logisall.anyware.core.domain.commerce.coupon.CouponType;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.reqbody.commerce.coupon.CouponThumbBody;
import com.logisall.anyware.core.model.resbody.commerce.coupon.CouponResBody;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;


@Slf4j
@Service
@Transactional
public class CouponAPIServiceImpl implements CouponAPIService {

  @Autowired
  private CouponRepository couponRepository;

  @Autowired
  private UserRepository userRepository;

  @Override
  @Transactional
  public Coupon create(Coupon coupon) {
    return couponRepository.save(coupon);
  }

  @Override
  @Transactional
  public Coupon update(Coupon coupon) {

    if (coupon.getId() == null) {
      throw new BadRequestException();
    }

    return couponRepository.findById(coupon.getId())
      .map(ori -> {
        BeanUtils.copyProperties(coupon, ori, Coupon.IGNORE_PROPERTIES);
        return couponRepository.save(ori);
      }).orElseThrow(() -> new UpdateErrorException(coupon.getId(), Coupon.class.getName()));
  }

  @Override
  @Transactional
  public void delete(Long id) {
    couponRepository.findById(id)
      .ifPresent(coupon -> {
        coupon.delete();
        couponRepository.delete(coupon);
      });
  }

  @Override
  @Transactional
  public Coupon get(Locale locale, Long id) {
    return couponRepository.findById(id)
      .map(coupon -> {
        coupon.setLocale(locale);
        coupon.lazy();
        return coupon;
      }).orElse(null);
  }

  @Override
  @Transactional
  public Page<Coupon> page(Locale locale, Filter filter, CouponType type) {
    Page<Coupon> page = couponRepository.findAll(
            CouponPredicate.getInstance()
              .search(filter.getQuery())
              .startDate(filter.getStartDate())
              .endDate(filter.getEndDate())
              .values(), filter.getPageable());

    page.forEach(coupon -> {
      coupon.setLocale(locale);
      coupon.lazy();
    });

    return page;
  }

  @Override
  @Transactional
  public List<CouponResBody> list(Locale locale, CouponType type) {
    List<Coupon> list = Lists.newArrayList(couponRepository.findAll(
            CouponPredicate.getInstance()
                    .time(LocalDate.now())
                    .type(type)
                    .active(true)
                    .values()
    ));
    List<CouponResBody> resBodies =  list.stream().map(coupon -> {
      CouponResBody couponResBody = coupon.toBody(locale);
      return couponResBody;
    }).collect(Collectors.toList());

    return resBodies;
  }

  @Override
  @Transactional
  public CouponThumbBody getByCode(Locale locale, String code) {
    if (!couponRepository.existsByCode(code)) {
      throw new NotFoundException();
    }
    Coupon coupon = couponRepository.findByCode(code);
    return coupon.toCouponThumbBody(locale);
  }

//  @Override
//  @Transactional
//  public Resources<Resource<CouponThumbBody>> resourcesByDownloadAndProduct(Locale locale, Long idProduct, String email, Mall mall) {
//
//    List<ESCoupon> list = this.listByDownloadAndProduct(locale, idProduct, email, mall);
//    list.addAll(this.listByDownloadAndCategory(locale, idProduct, email, mall));
//
//    return Resources.wrap(list.stream()
//      .map(coupon -> coupon.toCouponThumbBody(locale))
//      .collect(Collectors.toList()));
//  }
//
//  @Override
//  @Transactional
//  public Resources<Resource<CouponThumbBody>> resourcesByDownloadAndBrand(Locale locale, Long idBrand, String email, Mall mall) {
//
//    Long idMall = mall.getId();
//    Locale defaultLocale = mall.getDefaultLocale();
//
//    Predicate predicate;
//    if (StringUtils.isEmpty(email)) {
//      predicate = ExpressionUtils.allOf(
//        ESCouponPredicate.brand(idBrand),
//        ESCouponPredicate.type(ESCoupon.Type.DOWNLOAD),
//        ESCouponPredicate.target(ESCoupon.Target.BRAND),
//        ESCouponPredicate.active(true),
//        ESCouponPredicate.mall(idMall),
//        ESCouponPredicate.locale(locale, defaultLocale),
//        ESCouponPredicate.userMode(ESCoupon.UserMode.ALL)
//      );
//    } else {
//
//      ESBuyer buyer = buyerRepository.findByRelativeUser_EmailAndRelativeUser_RelativeMall_Id(email, idMall);
//      ESBuyerLevel buyerLevel = buyer != null ? buyer.getRelativeBuyerLevel() : null;
//      Long idBuyerLevel = buyerLevel != null ? buyerLevel.getId() : null;
//
//      predicate =
//        ExpressionUtils.allOf(
//          ESCouponPredicate.brand(idBrand),
//          ESCouponPredicate.type(ESCoupon.Type.DOWNLOAD),
//          ESCouponPredicate.target(ESCoupon.Target.BRAND),
//          ESCouponPredicate.active(true),
//          ESCouponPredicate.mall(idMall),
//          ESCouponPredicate.locale(locale, defaultLocale),
//          ESCouponPredicate.user(email, idBuyerLevel)
//        );
//    }
//
//    List<ESCoupon> list = Lists.newArrayList(couponRepository.findAll(
//      predicate,
//      Sort.by(Sort.Direction.DESC, "createdDate")));
//
//    return Resources.wrap(list.stream()
//      .map(coupon -> coupon.toCouponThumbBody(locale))
//      .collect(Collectors.toList()));
//  }
//
//  @Override
//  @Transactional
//  public Resources<Resource<CouponThumbBody>> resourcesByDownloadAndPromotion(Locale locale, Long idPromotion, String email, Mall mall) {
//
//    Long idMall = mall.getId();
//    Locale defaultLocale = mall.getDefaultLocale();
//
//    Predicate predicate;
//    if (StringUtils.isEmpty(email)) {
//      predicate = ExpressionUtils.allOf(
//        ESCouponPredicate.promotion(idPromotion),
//        ESCouponPredicate.type(ESCoupon.Type.DOWNLOAD),
//        ESCouponPredicate.target(ESCoupon.Target.PROMOTION),
//        ESCouponPredicate.active(true),
//        ESCouponPredicate.mall(idMall),
//        ESCouponPredicate.locale(locale, defaultLocale),
//        ESCouponPredicate.userMode(ESCoupon.UserMode.ALL)
//      );
//    } else {
//
//      ESBuyer buyer = buyerRepository.findByRelativeUser_EmailAndRelativeUser_RelativeMall_Id(email, idMall);
//      ESBuyerLevel buyerLevel = buyer != null ? buyer.getRelativeBuyerLevel() : null;
//      Long idBuyerLevel = buyerLevel != null ? buyerLevel.getId() : null;
//
//      predicate =
//        ExpressionUtils.allOf(
//          ESCouponPredicate.promotion(idPromotion),
//          ESCouponPredicate.type(ESCoupon.Type.DOWNLOAD),
//          ESCouponPredicate.target(ESCoupon.Target.PROMOTION),
//          ESCouponPredicate.active(true),
//          ESCouponPredicate.mall(idMall),
//          ESCouponPredicate.locale(locale, defaultLocale),
//          ESCouponPredicate.user(email, idBuyerLevel)
//        );
//    }
//
//    List<ESCoupon> list = Lists.newArrayList(couponRepository.findAll(
//      predicate,
//      Sort.by(Sort.Direction.DESC, "createdDate")));
//
//    return Resources.wrap(list.stream()
//      .map(coupon -> coupon.toCouponThumbBody(locale))
//      .collect(Collectors.toList()));
//  }

//  @Override
//  @Transactional
//  public CollectionModel<EntityModel<CouponThumbBody>> resourcesByDownloadAndBuyerLevel(Locale locale, Long idBuyerLevel) {
//
//    Locale defaultLocale = settingService.getDefaultLocale();
//
//    List<Coupon> list = Lists.newArrayList(couponRepository.findAll(
//      ExpressionUtils.allOf(
//        CouponPredicate.type(Coupon.Type.DOWNLOAD),
//        CouponPredicate.user(),
//        CouponPredicate.active(true),
//        CouponPredicate.locale(locale, defaultLocale)
//      ),
//      Sort.by(Sort.Direction.DESC, "createdDate")));
//
//    return Resources.wrap(list.stream()
//      .map(coupon -> coupon.toCouponThumbBody(locale))
//      .collect(Collectors.toList()));
//  }

//  @Override
//  @Transactional
//  public void scheduleByBirthdayAll() {
//
//    ArrayList<ESCoupon> list = Lists.newArrayList(couponRepository.findAll(
//      ExpressionUtils.allOf(
//        ESCouponPredicate.type(ESCoupon.Type.BIRTHDAY),
//        ESCouponPredicate.userMode(ESCoupon.UserMode.ALL),
//        ESCouponPredicate.active(true)
//      )
//    ));
//
//    if (list.size() > 0) {
//
//      list.forEach(coupon -> {
//        Mall mall = coupon.getRelativeMall();
//
//        List<User> users = userRepository.listByTodayBirth(mall.getId());
//        log.debug("({}) users size ::: {}", mall.getId(), users.size());
//
//        users.forEach(user -> {
//          log.debug("user ::: {}, {}", user.getId(), user.getEmail());
//          ESBuyer buyer = user.getRelativeBuyer();
//
//          if (buyer != null) {
//
//            int days = coupon.getAutomaticDays() > 0 ? coupon.getAutomaticDays() : 7;
//            LocalDate startDate = LocalDate.now();
//            LocalDate endDate = startDate.plusDays(days);
//
//            ESCouponBox couponBox = new ESCouponBox(buyer, coupon, startDate, endDate);
//            couponBoxService.create(couponBox);
//          }
//
//        });
//
//      });
//    }
//  }
//
//  @Override
//  @Transactional
//  public void scheduleByBirthdayLevel() {
//
//    ArrayList<ESCoupon> list = Lists.newArrayList(couponRepository.findAll(
//      ExpressionUtils.allOf(
//        ESCouponPredicate.type(ESCoupon.Type.BIRTHDAY),
//        ESCouponPredicate.userMode(ESCoupon.UserMode.LEVEL),
//        ESCouponPredicate.active(true)
//      )
//    ));
//
//    list.forEach(coupon -> {
//      Mall mall = coupon.getRelativeMall();
//      coupon.getBuyerLevels().forEach(buyerLevel -> {
//
//        List<User> users = userRepository.listByTodayBirth(mall.getId(), buyerLevel.getId());
//        log.debug("({}) users size ::: {}", mall.getId(), users.size());
//
//        users.forEach(user -> {
//          log.debug("user ::: {}, {}", user.getId(), user.getEmail());
//          ESBuyer buyer = user.getRelativeBuyer();
//
//          if (buyer != null) {
//
//            int days = coupon.getAutomaticDays() > 0 ? coupon.getAutomaticDays() : 7;
//            LocalDate startDate = LocalDate.now();
//            LocalDate endDate = startDate.plusDays(days);
//
//            ESCouponBox couponBox = new ESCouponBox(buyer, coupon, startDate, endDate);
//            couponBoxService.create(couponBox);
//          }
//
//        });
//
//      });
//
//    });
//
//  }
//
//  @Override
//  public void scheduleByBirthdayPersonal() {
//
//    ArrayList<ESCoupon> list = Lists.newArrayList(couponRepository.findAll(
//      ExpressionUtils.allOf(
//        ESCouponPredicate.type(ESCoupon.Type.BIRTHDAY),
//        ESCouponPredicate.userMode(ESCoupon.UserMode.PERSONAL),
//        ESCouponPredicate.active(true)
//      )
//    ));
//
//    list.forEach(coupon -> {
//      Mall mall = coupon.getRelativeMall();
//      List<Long> userIds = coupon.getUsers().stream().map(User::getId).collect(Collectors.toList());
//
//      if (userIds.size() > 0) {
//
//      List<User> users = userRepository.listByTodayBirth(mall.getId(), userIds);
//      log.debug("({}) users size ::: {}", mall.getId(), users.size());
//
//      users.forEach(user -> {
//        log.debug("user ::: {}, {}", user.getId(), user.getEmail());
//        ESBuyer buyer = user.getRelativeBuyer();
//
//        if (buyer != null) {
//
//          int days = coupon.getAutomaticDays() > 0 ? coupon.getAutomaticDays() : 7;
//          LocalDate startDate = LocalDate.now();
//          LocalDate endDate = startDate.plusDays(days);
//
//          ESCouponBox couponBox = new ESCouponBox(buyer, coupon, startDate, endDate);
//          couponBoxService.create(couponBox);
//        }
//
//      });
//
//      }
//
//    });
//
//  }
//
//  @Override
//  @Transactional
//  public void scheduleByAutomatic(ESCoupon.RepeatCycle cycle) {
//
//    ArrayList<ESCoupon> list = Lists.newArrayList(couponRepository.findAll(
//      ExpressionUtils.allOf(
//        ESCouponPredicate.type(ESCoupon.Type.AUTOMATIC),
//        ESCouponPredicate.cycle(cycle),
//        ESCouponPredicate.active(true)
//      )
//    ));
//
//    if (list.size() > 0) {
//
//      list.forEach(coupon -> {
//        Mall mall = coupon.getRelativeMall();
//        Long idMall = mall.getId();
//
//        if (Objects.equals(coupon.getUserMode(), ESCoupon.UserMode.ALL)) {
//
//          List<User> users = userRepository.findByRelativeMall_IdAndUserDetailsMetaEnabledTrueAndLeaveMetaLeaveFalse(idMall);
//
//          users.forEach(user -> {
//            ESBuyer buyer = user.getRelativeBuyer();
//
//            if (buyer != null) {
//
//              int days = coupon.getAutomaticDays() > 0 ? coupon.getAutomaticDays() : 7;
//              LocalDate startDate = LocalDate.now();
//              LocalDate endDate = startDate.plusDays(days);
//
//              ESCouponBox couponBox = new ESCouponBox(buyer, coupon, startDate, endDate);
//              couponBoxService.create(couponBox);
//            }
//
//          });
//
//        } else if (Objects.equals(coupon.getUserMode(), ESCoupon.UserMode.LEVEL)) {
//
//          coupon.getBuyerLevels().forEach(level -> {
//            level.getBuyers().forEach(buyer -> {
//
//              int days = coupon.getAutomaticDays() > 0 ? coupon.getAutomaticDays() : 7;
//              LocalDate startDate = LocalDate.now();
//              LocalDate endDate = startDate.plusDays(days);
//
//              ESCouponBox couponBox = new ESCouponBox(buyer, coupon, startDate, endDate);
//              couponBoxService.create(couponBox);
//
//            });
//          });
//        } else if (Objects.equals(coupon.getUserMode(), ESCoupon.UserMode.PERSONAL)) {
//
//          coupon.getUsers().forEach(user -> {
//            ESBuyer buyer = user.getRelativeBuyer();
//
//            if (buyer != null) {
//
//              int days = coupon.getAutomaticDays() > 0 ? coupon.getAutomaticDays() : 7;
//              LocalDate startDate = LocalDate.now();
//              LocalDate endDate = startDate.plusDays(days);
//
//              ESCouponBox couponBox = new ESCouponBox(buyer, coupon, startDate, endDate);
//              couponBoxService.create(couponBox);
//            }
//
//          });
//        }
//      });
//    }
//  }

//  @Override
//  @Transactional
//  public List<CouponThumbBody> listByBuyer(Long idBuyer, Locale locale) {
//    List<Coupon> coupons = couponRepository.findAllByRelativeBuyer_Id(idBuyer);
//    List<CouponThumbBody> couponThumbBodies = coupons.stream().map(coupon -> {
//      CouponThumbBody couponThumbBody = coupon.toCouponThumbBody(locale);
//      return couponThumbBody;
//    }).collect(Collectors.toList());
//    return couponThumbBodies;
//  }
  @Override
  @Transactional
  public List<CouponThumbBody> listCouponsByUser(Long idUser, Boolean isUsed, Locale locale) {
    if(!userRepository.existsById(idUser)){
      throw new BadRequestException();
    }

    List<Coupon> couponList = couponRepository.findAllByUser(idUser, isUsed);
    List<CouponThumbBody> coupons = couponList.stream().map(coupon -> {
      CouponThumbBody couponResBody = coupon.toCouponThumbBody(locale);
      return couponResBody;
    }).collect(Collectors.toList());
    return coupons;
  }
}
