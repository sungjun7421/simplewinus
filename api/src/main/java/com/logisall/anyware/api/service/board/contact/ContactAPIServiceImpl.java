package com.logisall.anyware.api.service.board.contact;

import com.logisall.anyware.api.service.account.user.UserAPIService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.board.contact.Contact;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.model.reqbody.board.ContactReqBody;
import com.logisall.anyware.core.service.board.contact.ContactService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class ContactAPIServiceImpl implements ContactAPIService {

  @Autowired
  private ContactService contactService;

  @Autowired
  private UserAPIService userAPIService;

  @Override
  @Transactional
  public Long post(ContactReqBody contactReqBody, String email) {

    User user = null;

    if (!StringUtils.isEmpty(email)) {
      user = userAPIService.get(email);
    }
    Contact createdContact = contactService.create(contactReqBody.toContact(user));

    if (createdContact == null) {
      throw new BadRequestException();
    }
    return createdContact.getId();
  }
}
