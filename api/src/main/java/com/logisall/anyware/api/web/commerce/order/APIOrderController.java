package com.logisall.anyware.api.web.commerce.order;

import com.logisall.anyware.api.service.commerce.order.OrderAPIService;
import com.logisall.anyware.api.service.commerce.order.OrderAmountService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.model.reqbody.commerce.order.CancelOrderReqBody;
import com.logisall.anyware.core.model.reqbody.commerce.order.OrderReqBody;
import com.logisall.anyware.core.model.reqbody.commerce.order.PaymentInfo;
import com.logisall.anyware.core.model.reqbody.commerce.order.PaymentInfoReqBody;
import com.logisall.anyware.core.model.resbody.commerce.order.OrderDetailResBody;
import com.logisall.anyware.core.utils.ValidUtils;
import com.google.common.collect.ImmutableMap;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

@Slf4j
@RestController
@RequestMapping("api/v1/orders")
public class APIOrderController {

    @Autowired
    private OrderAPIService orderAPIService;

    @Autowired
    private OrderAmountService orderAmountService;

    /**
     * [order-0] price-info
     */
//    @Operation(summary = "[order-0] Get price info ", description = "Get price info of order")
//    @GetMapping("test/price-info")
//    public ResponseEntity<?> testPriceInfo(@RequestParam String oid) {
//
//        Order order = orderAPIService.get(oid);
//
//        Map<String, Object> result = new HashMap<>();
////    result.put("orderTemp", order.toPaymentPriceInfo());
////    result.put("orderComplete", order.toPaymentPriceInfo());
//
//        return ResponseEntity.ok(result);
//    }

    /**
     * [order-1] create
     */
    @Operation(summary = "[order-2] Create order ()", description = "Create order")
    @PostMapping(value = "/create")
    public ResponseEntity<?> order(@RequestBody OrderReqBody reqBody,
                                OAuth2Authentication oAuth2Authentication,
                                Locale locale) {

        ValidUtils.isForbidden(oAuth2Authentication);
        String email = oAuth2Authentication == null ? null : oAuth2Authentication.getPrincipal().toString();

        Order order = orderAPIService.order(reqBody, email);

        return ResponseEntity.ok(ImmutableMap.builder()
                .put("msg", "success")
                .put("data", order)
                .build());
    }

    /**
     * [order-2] 주문 상세
     */
    @Operation(summary = "[order-2] Get detail order (주문 상세)", description = "Get detail order")
    @GetMapping(value = "/get/{id}")
    public ResponseEntity<?> getDetail(@PathVariable Long id,
                                    @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication,
                                    Locale locale) {

//        ValidUtils.isForbidden(oAuth2Authentication);
        if(id == null) {
            throw new BadRequestException();
        }
        OrderDetailResBody order = orderAPIService.getDetailOrder(id, locale);
        return ResponseEntity.ok(
                ImmutableMap
                        .builder()
                        .put("order", order)
                        .build()
        );
    }

    /**
     * [order-3] 리스트
     */
//    @Operation(summary = "Get all order detail by User (리스트)", description = "Get all order detail by User")
//    @GetMapping(value = "/get-all-order-by-user")
//    public ResponseEntity<?> getAllOrderByUser(@PageableDefault(size = 20, sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable,
//                                               @RequestParam(required = false) String startDate,
//                                               @RequestParam(required = false) String endDate,
//                                               @RequestParam(required = false, defaultValue = "") String query,
//                                               @RequestParam(required = false) String orderStatuses,
//                                               @RequestParam(required = false) Long idStore,
//                                               @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication,
//                                               Locale locale) {
//
//        ValidUtils.isForbidden(oAuth2Authentication);
//        String email = oAuth2Authentication.getPrincipal().toString();
//        List<OrderStatus> orderStatusList = orderAPIService.convertOrderStatus(orderStatuses);
//        Filter filter = new Filter(pageable, query, startDate, endDate);
//        PagedModel<OrderDetailResBody> orderList = orderAPIService.getAllOrderDetailByUser(email, orderStatusList, filter, locale, idStore);
//
//        return ResponseEntity.ok(orderList);
//    }

    /**
     * [order-4]
     */
//    @Operation(summary = "Get all Store Order by User", description = "Get all Store Order by User")
//    @GetMapping(value = "/get-all-store-order-by-user")
//    public ResponseEntity<?> getAllStoreOrderByUser(@PageableDefault(size = 20, sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable,
//                                               @RequestParam(required = false) String startDate,
//                                               @RequestParam(required = false) String endDate,
//                                               @RequestParam(required = false, defaultValue = "") String query,
//                                               @RequestParam Long idUser,
//                                               @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication,
//                                               Locale locale) {
//
//        Filter filter = new Filter(pageable, query, startDate, endDate);
//        CollectionModel<EntityModel<StoreOrderResBody>> storeOrders = orderAPIService.getAllStoreOrderByUser(idUser, filter, locale);
//        return ResponseEntity.ok(
//                ImmutableMap
//                        .builder()
//                        .put("storeOrders", storeOrders != null ? storeOrders : Collections.emptyList())
//                        .build()
//        );
//    }



    /**
     * [order-5] update-order
     */

    @Operation(summary = "Update order", description = "Update order")
    @PostMapping("payment-info")
    public ResponseEntity<?> updateOrder(@RequestBody PaymentInfoReqBody reqBody,
                                      OAuth2Authentication oAuth2Authentication) {

        ValidUtils.isForbidden(oAuth2Authentication);
        if(reqBody == null || reqBody.getOid() == null || reqBody.getOid() == ""
                || reqBody.getPayMethod() == null){
            throw new BadRequestException();
        }
        return ResponseEntity.ok(orderAmountService.paymentInfo(reqBody));
    }

    /**
     * [order-6] create-payment
     */
    @Operation(summary = "create-payment", description = "create-payment")
    @PostMapping("create-payment")
    public ResponseEntity<?> createPayment(@RequestBody PaymentInfoReqBody reqBody,
                                        OAuth2Authentication oAuth2Authentication) {

        ValidUtils.isForbidden(oAuth2Authentication);

        if(reqBody == null || reqBody.getOid() == null || reqBody.getOid() == ""
                || reqBody.getPayMethod() == null || reqBody.getFinalPrice() == null){
            throw new BadRequestException();
        }
        PaymentInfo paymentInfo = orderAmountService.createPayment(reqBody);

        return ResponseEntity.ok(paymentInfo);
    }

    /**
     * [order-6] add-point
     */
//    @Operation(summary = "Add point", description = "add point")
//    @PostMapping("payment-price-info")
//    public ResponseEntity<?> paymentPriceInfo(@RequestBody OrderPriceReqBody reqBody) {
//        String oid = reqBody.getOid();
//        Long idCoupon = reqBody.getIdCoupon();
//        Double point = reqBody.getPoint();
//        return ResponseEntity.ok(orderAmountService.paymentPriceInfo(oid, idCoupon, point));
//    }

    // 결제정보
//    @GetMapping("payment-info/{oid}")
//    public ResponseEntity<?> paymentInfo(@PathVariable String oid,
//                                      Locale locale) {
//
//        return ResponseEntity.ok(orderAPIService.resourcePayment(locale, oid));
//    }

    // 결제금액정보
    /**
     * [order-7] price-info
     */
//    @Operation(summary = "[order-7] Get price info ", description = "Get price info of order")
//    @GetMapping("payment-price-info/{oid}")
//    public ResponseEntity<?> paymentPriceInfo(@PathVariable String oid,
//                                           @RequestParam(required = false) Long idCoupon, // 주문쿠폰
//                                           @RequestParam(required = false) Double point,
//                                           Locale locale) {
//
//        return ResponseEntity.ok(orderAmountService.paymentPriceInfo(oid, idCoupon, point));
//    }

    /**
     * [order-8] cancel order
     */
    @Operation(summary = "[order-9] Cancel order ", description = "Cancel order")
    @PostMapping("/cancel-order")
    public ResponseEntity<?> cancelOrder(@RequestBody CancelOrderReqBody cancelReqBody,
                                      OAuth2Authentication oAuth2Authentication,
                                      Locale locale) {
        ValidUtils.isForbidden(oAuth2Authentication);
        if(cancelReqBody == null || cancelReqBody.getOid() == null || cancelReqBody.getOid() == ""){
            throw new BadRequestException("Params not found!");
        }
        return ResponseEntity.ok(orderAPIService.cancelOrder(cancelReqBody.getOid()));
    }

}
