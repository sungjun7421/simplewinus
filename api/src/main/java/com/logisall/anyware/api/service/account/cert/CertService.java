package com.logisall.anyware.api.service.account.cert;

import com.logisall.anyware.core.model.AuthentiCodeResBody;
import com.logisall.anyware.core.model.reqbody.account.CertConfirmReqBody;
import com.logisall.anyware.core.model.reqbody.account.CertReqBody;

import javax.servlet.http.HttpServletRequest;

public interface CertService {

  /**
   * 모바일/이메일 인증
   *
   * @param certReqBody 인증 객체
   * @return 인증 코드
   */
  String cert(CertReqBody certReqBody, HttpServletRequest request);

  /**
   * 모바일/이메일 인증 확인
   *
   * @param confirm 확인 코드 객체
   * @return 확인
   */
  AuthentiCodeResBody confirm(CertConfirmReqBody confirm);

  String createCertificateNumber(CertReqBody certReqBody);
}
