package com.logisall.anyware.api.web.commerce.wish;


import com.logisall.anyware.api.SwaggerExample;
import com.logisall.anyware.api.service.commerce.wish.WishAPIService;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.ForbiddenException;
import com.logisall.anyware.core.domain.commerce.buyer.BuyerRepository;
import com.logisall.anyware.core.domain.commerce.wish.WishRepository;
import com.logisall.anyware.core.model.reqbody.commerce.wish.ToggleWishRequestBody;
import com.logisall.anyware.core.service.commerce.buyer.BuyerService;
import com.logisall.anyware.core.service.commerce.wish.WishService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Tag(name = "Wish", description = "Wish API")
@RestController
@RequestMapping("/api/v1/wish")
public class APIWishController {
    @Autowired
    WishService wishService;

    @Autowired
    WishRepository wishRepository;

    @Autowired
    BuyerService buyerService;

    @Autowired
    BuyerRepository buyerRepository;

    @Autowired
    WishAPIService wishAPIService;

    @Operation(
            summary = "Get list product wish of login user",
            description = "list product wish of login user",
            security = {@SecurityRequirement(name = "bearerAuth")},
            responses = {
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(example =SwaggerExample.WISH_1)
                            ),
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(example ="{\n" +
                                            "    \"code\": \"403\",\n" +
                                            "    \"message\": \"접근권한이 없습니다.\"\n" +
                                            "}")
                            ),
                            responseCode = "403"
                    )
            }
    )
    @GetMapping(value = "/list",produces = "application/json")
    public ResponseEntity<?> wishList(OAuth2Authentication oAuth2Authentication){
        if(oAuth2Authentication==null || oAuth2Authentication.getPrincipal()==null ){
            throw new ForbiddenException();
        }
        return wishAPIService.getListWishOf(oAuth2Authentication.getPrincipal().toString());

    }

    @Operation(
            summary = "Get list product wish of another user with userId",
            description = "list product wish of another user ",
            responses = {
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(example =SwaggerExample.WISH_1)
                            ),
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(example ="{\n" +
                                            "    \"code\": \"400\",\n" +
                                            "    \"message\": \"잘못된 요청입니다.\"\n" +
                                            "}")
                            ),
                            description = "Parameter not number",
                            responseCode = "400"
                    ),
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(example ="{\n" +
                                            "    \"code\": \"404\",\n" +
                                            "    \"message\": \"페이지를 찾을수 없습니다.\"\n" +
                                            "}")
                            ),
                            description = "user not found",
                            responseCode = "404"
                    )
            }
    )
//    @Parameters(value = {
//            @Parameter(in = ParameterIn.HEADER)
//    })
    @GetMapping(value = "/users/{userId}/list",produces = "application/json")
    public ResponseEntity<?> wishListAnother(@Parameter(description = "userId number only") @PathVariable(name="userId") Object userId){
        try{
            Long.parseLong(userId.toString());
        }catch (Exception e){
            throw new BadRequestException();
        }
        return wishAPIService.getListWishOf(Long.parseLong(userId.toString()));
    }


    @Operation(
            summary = "User trigger wish product",
            description = "Wish or un wish product",
            security = {@SecurityRequirement(name = "bearerAuth")},
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(
                                    name="Toggle Wish body", implementation = ToggleWishRequestBody.class
                            ),
                            examples = {
                            @ExampleObject(
                                    name = "Toggle wish body Example",
                                    summary = "Toggle wish body Example",
                                    description = "Info toggle wish example",
                                    value = SwaggerExample.TOGGLE_WISH)
                            }
                    )

            ),
            responses = {
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(example =SwaggerExample.TOGGLE_WISH_RESP)
                            ),
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(example ="{\n" +
                                            "    \"code\": \"403\",\n" +
                                            "    \"message\": \"접근권한이 없습니다.\"\n" +
                                            "}")
                            ),
                            responseCode = "403"
                    )
            }

    )
    @PostMapping(value = "/toggle",produces = "application/json",consumes = "application/json")
    public ResponseEntity<?> toggleWish(@Valid @RequestBody ToggleWishRequestBody body, OAuth2Authentication oAuth2Authentication){
        if(oAuth2Authentication==null || oAuth2Authentication.getPrincipal()==null ){
            throw new ForbiddenException();
        }

        return wishAPIService.toggleProduct(body.getProductId(), oAuth2Authentication.getPrincipal().toString());


    }


}
