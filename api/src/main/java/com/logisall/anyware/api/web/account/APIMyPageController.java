package com.logisall.anyware.api.web.account;

import com.logisall.anyware.api.service.account.cert.CertService;
import com.logisall.anyware.api.service.account.sns.SNSAPIService;
import com.logisall.anyware.api.service.account.user.UserAPIService;
import com.logisall.anyware.core.config.database.PwdEncConfig;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.ResponseErrorCode;
import com.logisall.anyware.core.config.exception.UnAuthorizedException;
import com.logisall.anyware.core.domain.file.FileEntity;
import com.logisall.anyware.core.domain.user.TermsAgree;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.model.file.FileMeta;
import com.logisall.anyware.core.model.reqbody.account.*;
import com.logisall.anyware.core.model.resbody.ResultResBody;
import com.logisall.anyware.core.model.resbody.account.ChangeSubscribeResBody;
import com.logisall.anyware.core.utils.ValidUtils;
import com.google.common.collect.ImmutableMap;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Objects;

@Tag(name = "Me", description = "내 사용자 관리 API")
@Slf4j
@RestController
@RequestMapping("/api/v1/me")
public class APIMyPageController {

  @Autowired
  private UserAPIService userAPIService;

  @Autowired
  private SNSAPIService snsapiService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CertService certService;

  @Autowired
  private PwdEncConfig pwdEncConfig;

  /**
   * [me-1] 내 정보 (access_token)
   */
  @Operation(summary = "[me-1] My Account Information (내 정보)",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @Parameters(value = {
      @Parameter(in = ParameterIn.HEADER, name = "Accept-Language", description = "Language Code", example = "ko-KR")
  })
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> me(Locale locale,
                              @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    if (oAuth2Authentication == null || StringUtils.isEmpty(oAuth2Authentication.getPrincipal())) {
      throw new UnAuthorizedException();
    }

    Object principal = oAuth2Authentication.getPrincipal();
    return ResponseEntity.ok(new EntityModel<>(userAPIService.profile(principal.toString(), locale)));
  }

  @Operation(summary = "[me-2] Modify password (비밀번호 수정)",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PatchMapping(value = "/password", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> changePassword(@RequestBody ChangePasswordReqBody changePassword,
                                          @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    ValidUtils.isForbidden(oAuth2Authentication);
    final String email = oAuth2Authentication.getPrincipal().toString();

    if (changePassword == null
        || StringUtils.isEmpty(changePassword.getPassword())
        || StringUtils.isEmpty(changePassword.getNewPassword())
        || StringUtils.isEmpty(changePassword.getConfirmNewPassword())
    ) {
      throw new BadRequestException();
    }

    if (!changePassword.getNewPassword().equals(changePassword.getConfirmNewPassword())) {
      throw new BadRequestException(ValidUtils.MESSAGE_CONFIRM_PASSWORD_NEW);
    }

    if (!ValidUtils.isPasswordPattern(changePassword.getNewPassword())) {
      throw new BadRequestException(ValidUtils.MESSAGE_PASSWORD_NEW);
    }

    userAPIService.changePassword(changePassword, email);
    return ResponseEntity.ok(ResultResBody.of(true, "비밀번호가 변경되었습니다."));
  }


  @Operation(summary = "[me-3] Modify mobile (휴대폰번호 수정)",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PatchMapping(value = "mobile", produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> changeMobile(@RequestBody CertConfirmReqBody confirm,
                                        @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    ValidUtils.isForbidden(oAuth2Authentication);
    final String email = oAuth2Authentication.getPrincipal().toString();

    if (confirm == null
        || StringUtils.isEmpty(confirm.getCode())
        || StringUtils.isEmpty(confirm.getMobile())
        || !Objects.equals(confirm.getMode(), CertReqBody.Mode.CERT_MOBILE)) {
      throw new BadRequestException();
    }

    userAPIService.changeMobile(confirm, email);
    return ResponseEntity.ok(ResultResBody.of(true, "휴대폰번호가 변경되었습니다."));
  }

  @Operation(summary = "[me-4] Modify profile image (이미지 수정)",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PatchMapping(value = "/image")
  public ResponseEntity<?> changeImage(@Parameter(description = "Choose a new Image") @RequestParam("file") MultipartFile file,
                                       @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication,
                                       Locale locale) {

    ValidUtils.isForbidden(oAuth2Authentication);
    final String email = oAuth2Authentication.getPrincipal().toString();

    FileMeta fileMeta = userAPIService.updateProfileImage(file, email);
    if (fileMeta != null) {
      FileEntity fileEntity = fileMeta.toFileEntity();
      return ResponseEntity.ok(new EntityModel(fileEntity.toBody(locale)));
    }
    throw new BadRequestException("이미지가 업로드 되지 않았습니다.");
  }

  @Operation(summary = "[me-5] Modify full name (성명(이름) 수정)",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PatchMapping(value = "/full-name", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> changeLastName(@RequestBody ChangeFullNameReqBody reqBody,
                                          @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    ValidUtils.isForbidden(oAuth2Authentication);
    final String email = oAuth2Authentication.getPrincipal().toString();

    if (reqBody == null
        || StringUtils.isEmpty(reqBody.getFullName())) {
      throw new BadRequestException();
    }

    userAPIService.changeFullName(reqBody.getFullName(), email);
    return ResponseEntity.ok(ResultResBody.of(true, "이름이 변경되었습니다."));
  }


  @Operation(summary = "[me-6] SNS account - Toggle(true/false) (SNS 계정 연동 - 토글방식)",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PatchMapping(value = "/sns", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> changeSns(@RequestBody ChangeSnsReqBody requestBody,
                                     @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    ValidUtils.isForbidden(oAuth2Authentication);
    final String email = oAuth2Authentication.getPrincipal().toString();

    if (requestBody == null
        || requestBody.getSnsType() == null) {
      throw new BadRequestException();
    }
    snsapiService.changeSns(requestBody, email);
    return ResponseEntity.ok(ResultResBody.of(true, "SNS 계정 연동이 변경되었습니다."));
  }


  @Operation(summary = "[me-7] Native App SNS account - Toggle(true/false) (Native 어플용 SNS 계정연동 - 토글방식)",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PatchMapping(value = "/sns/native", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> changeSnsNative(@RequestBody ChangeSnsNativeReqBody requestBody,
                                           @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    ValidUtils.isForbidden(oAuth2Authentication);
    final String email = oAuth2Authentication.getPrincipal().toString();

    if (requestBody == null) {
      throw new BadRequestException();
    }
    snsapiService.changeSnsNative(requestBody, email);
    return ResponseEntity.ok(ResultResBody.of(true, "SNS 계정 연동이 변경되었습니다."));
  }

  @Operation(summary = "[me-8] Withdrawal (회원 탈퇴)",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PostMapping(value = "/withdrawal", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> withdrawal(@RequestBody LeaveUserReqBody requestBody,
                                      @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    ValidUtils.isForbidden(oAuth2Authentication);


    if (requestBody == null
        || StringUtils.isEmpty(requestBody.getPassword())
        || StringUtils.isEmpty(requestBody.getLeaveReason())
//        || !requestBody.isLeaveTerms()
    ) {
      throw new BadRequestException();
    }

    Object principal = oAuth2Authentication.getPrincipal();
    User user = userAPIService.get(principal.toString());

    if (user == null) {
      throw new BadRequestException(ResponseErrorCode.NOT_EXIST_USER.name());
    }

    if (user.getLeaveMeta().isLeave()) {
      throw new BadRequestException("이미 탈퇴한 계정입니다.");
    }

    if (pwdEncConfig.getPasswordEncoder().matches(requestBody.getPassword(), user.getPassword())) {
      userAPIService.leave(user.getId(), requestBody.getLeaveReason());
      return ResponseEntity.ok(ResultResBody.of(true, "회원이 탈퇴되었습니다."));
    } else {
      throw new BadRequestException(ResponseErrorCode.INVALID_ENTERED_PASSWORD.name());
//      return ResponseEntity.ok(ResultResponseBody.of(false, "입력한 비밀번호가 일치 하지 않습니다."));
    }
  }

  @Operation(summary = "[me-9] Confirm password match (비밀번호 일치 확인)",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PostMapping(value = "/match-password", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> matchPassword(@RequestBody MatchingPasswordReqBody requestBody,
                                         @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    ValidUtils.isForbidden(oAuth2Authentication);

    if (requestBody == null
        || StringUtils.isEmpty(requestBody.getPassword())) {
      throw new BadRequestException("이메일 또는 비밀번호 값이 없습니다.");
    }

    String email = oAuth2Authentication.getPrincipal().toString();
    String password = requestBody.getPassword();

    User user = userRepository.findByEmail(email).orElse(null);
    if (user == null) {
      throw new BadRequestException(ResponseErrorCode.NOT_EXIST_USER.getMessage());
    }

    boolean matched = userAPIService.matchPassword(user, password);

    if (matched) {
      return ResponseEntity.ok(ResultResBody.of(true, "비밀번호가 일치합니다."));
    } else {
      return ResponseEntity.ok(ResultResBody.of(false, "비밀번호가 일치하지 않습니다."));
    }
  }


  @Operation(summary = "[me-10] Modify Email(ID) (이메일 수정)",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PatchMapping(value = "email", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> changeEmail(@RequestBody CertReqBody cert,
                                       @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication,
                                       HttpServletRequest request) {

    ValidUtils.isForbidden(oAuth2Authentication);
    final String email = oAuth2Authentication.getPrincipal().toString();

    if (cert == null
        || StringUtils.isEmpty(cert.getEmail())
        || !ValidUtils.isEmailPattern(cert.getEmail())
        || !Objects.equals(cert.getMode(), CertReqBody.Mode.CERT_EMAIL)) {
      throw new BadRequestException();
    }

    userAPIService.changeEmail(cert, email, request);
    certService.cert(cert, request);
    log.debug("email ::: {}", email);
    User user = userRepository.findByEmail(cert.getEmail()).orElse(null);

    // 계정이 존재하는지 체크
    if (user == null) {
      throw new BadRequestException(ResponseErrorCode.ENTERED_INVALID.getMessage());
    }

    return ResponseEntity.ok(
        ImmutableMap
            .builder()
            .put("access_token", userAPIService.getJWT(user))
            .build()
    );
  }

  @Operation(summary = "[me-11] Email subscription - Toggle way (이메일 구독 - 토글)",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PatchMapping(value = "/subscribe/email", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> subscribeEmail(@RequestBody SubscribeReqBody requestBody,
                                          @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    ValidUtils.isForbidden(oAuth2Authentication);
    final String email = oAuth2Authentication.getPrincipal().toString();

    if (requestBody == null
        || requestBody.getActive() == null) {
      throw new BadRequestException();
    }

    TermsAgree termsAgree = userAPIService.subscribeByEmail(requestBody.getActive(), email);

    ChangeSubscribeResBody resBody = ChangeSubscribeResBody.builder()
        .agree(termsAgree.isEmailRcv())
        .time(termsAgree.getEmailRcvDate())
        .build();

    return ResponseEntity.ok(new EntityModel<>(resBody));
  }

  @Operation(summary = "[me-12] SNS subscription - Toggle way (SNS 구독 - 토글)",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PatchMapping(value = "/subscribe/sms", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> subscribeSMS(@RequestBody SubscribeReqBody requestBody,
                                        @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    ValidUtils.isForbidden(oAuth2Authentication);
    final String email = oAuth2Authentication.getPrincipal().toString();

    if (requestBody == null
        || requestBody.getActive() == null) {
      throw new BadRequestException();
    }

    TermsAgree termsAgree = userAPIService.subscribeBySMS(requestBody.getActive(), email);

    ChangeSubscribeResBody resBody = ChangeSubscribeResBody.builder()
        .agree(termsAgree.isSmsRcv())
        .time(termsAgree.getSmsRcvDate())
        .build();
    return ResponseEntity.ok(new EntityModel<>(resBody));
  }

  @Operation(summary = "[me-13] Kakaotalk subscription - Toggle way (카카오톡 구독 - 토글)",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PatchMapping(value = "/subscribe/kakao", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> subscribeKakao(@RequestBody SubscribeReqBody requestBody,
                                          @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    ValidUtils.isForbidden(oAuth2Authentication);
    final String email = oAuth2Authentication.getPrincipal().toString();

    if (requestBody == null
        || requestBody.getActive() == null) {
      throw new BadRequestException();
    }

    TermsAgree termsAgree = userAPIService.subscribeByKakao(requestBody.getActive(), email);

    ChangeSubscribeResBody resBody = ChangeSubscribeResBody.builder()
        .agree(termsAgree.isKakaoRcv())
        .time(termsAgree.getKakaoRcvDate())
        .build();
    return ResponseEntity.ok(new EntityModel<>(resBody));
  }

  @Operation(summary = "[me-14] APP PUSH subscription - Toggle way (푸시 구독 - 토글)",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PatchMapping(value = "/subscribe/push", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> subscribePush(@RequestBody SubscribeReqBody requestBody,
                                         @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    ValidUtils.isForbidden(oAuth2Authentication);
    final String email = oAuth2Authentication.getPrincipal().toString();

    if (requestBody == null
        || requestBody.getActive() == null) {
      throw new BadRequestException();
    }

    TermsAgree termsAgree = userAPIService.subscribeByPush(requestBody.getActive(), email);

    ChangeSubscribeResBody resBody = ChangeSubscribeResBody.builder()
        .agree(termsAgree.isPushRcv())
        .time(termsAgree.getPushRcvDate())
        .build();
    return ResponseEntity.ok(new EntityModel<>(resBody));
  }

  @Operation(summary = "[me-15] Update user information. (회원정보 수정)",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> updateMe(@RequestBody UpdateUserInfoReqBody requestBody,
                                    @Parameter(hidden = true) OAuth2Authentication oAuth2Authentication) {

    ValidUtils.isForbidden(oAuth2Authentication);
    final String email = oAuth2Authentication.getPrincipal().toString();

    if (requestBody == null) {
      throw new BadRequestException();
    }

    userAPIService.updateUserInfo(email, requestBody);

    return ResponseEntity.ok(ResultResBody.of(true, "회원정보가 수정되었습니다."));
  }
}
