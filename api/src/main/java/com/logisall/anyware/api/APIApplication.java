package com.logisall.anyware.api;

import org.apache.commons.lang3.StringUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

import java.util.Enumeration;
import java.util.Properties;

@EnableAuthorizationServer
@SpringBootApplication(scanBasePackages = {
    "com.logisall.anyware.api",
    "com.logisall.anyware.core"
})
@EnableJpaRepositories(basePackages = {"com.logisall.anyware.core.domain"})
@EntityScan(basePackages = {"com.logisall.anyware.core.domain"})
@MapperScan(basePackages= {"com.logisall.anyware.core.domain"})
public class APIApplication {

  public static void main(String[] args) {
    System.out.println("# ApiApplication #");
    String profiles = System.getProperty("spring.profiles.default");

    if (StringUtils.isEmpty(profiles)) {
      System.setProperty("spring.profiles.default", "api");
    }
    System.out.println("> spring.profiles.default ::: " + System.getProperty("spring.profiles.default"));

//        SpringApplication.run(Server.class, args);
    Properties p = System.getProperties();
    Enumeration keys = p.keys();

    System.out.println("--------------------");
    while (keys.hasMoreElements()) {
      String key = (String) keys.nextElement();
      String value = (String) p.get(key);
      System.out.println("# Property : [" + key + " = " + value + "]");
    }
    System.out.println("--------------------");

    SpringApplication.run(APIApplication.class, args);
  }
}
