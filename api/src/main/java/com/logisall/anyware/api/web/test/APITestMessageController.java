package com.logisall.anyware.api.web.test;

import com.logisall.anyware.core.config.language.Messages;
import com.google.common.collect.ImmutableMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Slf4j
@RestController
@RequestMapping("/api/v1/test/messages")
public class APITestMessageController {

  @Autowired
  private Messages messages;

  @GetMapping
  public ResponseEntity<?> ko(Locale locale,
                              HttpServletRequest request) throws Exception {
    log.debug("request.getLocale() : {}", request.getLocale());
    return ResponseEntity.ok(ImmutableMap.builder()
        .put("1", messages.getMessage("message.default", "...", "ko", request))
        .put("2", messages.getMessage("message.default", new String[]{""}, "...", "ko", request))
        .put("3", messages.getMessage("message.default", "...", request))
        .build());
  }
}
