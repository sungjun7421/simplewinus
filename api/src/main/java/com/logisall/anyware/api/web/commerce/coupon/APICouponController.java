package com.logisall.anyware.api.web.commerce.coupon;

import com.logisall.anyware.api.service.commerce.buyer.BuyerAPIService;
import com.logisall.anyware.api.service.commerce.coupon.CouponAPIService;
import com.logisall.anyware.core.config.exception.ForbiddenException;
import com.logisall.anyware.core.model.reqbody.commerce.coupon.CouponThumbBody;
import com.google.common.collect.ImmutableMap;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@Slf4j
@Tag(name = "Coupon", description = "쿠폰 API")
@RestController
@RequestMapping("/api/v1/coupons")
public class APICouponController {

    @Autowired
    private CouponAPIService couponAPIService;


    @Autowired
    private BuyerAPIService buyerAPIService;

//    @Autowired
//    private OrderAmountService orderAmountService;

    @Operation(summary = "[coupon-1] Detail Coupon ", description = "Get detail coupon",
        security = {@SecurityRequirement(name = "bearerAuth")},
        responses = @ApiResponse(
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                schema = @Schema(example = "{\n" +
                    "    \"msg\": \"success\",\n" +
                    "    \"data\": \"[datail coupon]\"\n" +
                    "}")
            )))
    @GetMapping(value="/code/{code}")
    public ResponseEntity<?> getByCode(@PathVariable String code,
                                    Locale locale) {
        CouponThumbBody couponThumbBody = couponAPIService.getByCode(locale, code);
        return ResponseEntity.ok(ImmutableMap.builder()
                .put("msg", "success")
                .put("data", couponThumbBody)
                .build());
    }

    /**
     * [coupon-2] Coupon list
     */
    @Operation(summary = "[coupon-2]  Coupon list", description = "Get all coupon",
        security = {@SecurityRequirement(name = "bearerAuth")},
        responses = @ApiResponse(
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                schema = @Schema(example = "{\n" +
                    "    \"data\": \"[list coupon]\"\n" +
                    "}")
            )))
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> page(OAuth2Authentication oAuth2Authentication,
                                  Locale locale) {

        if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
            throw new ForbiddenException();
        }

        return ResponseEntity.ok(couponAPIService.list(locale, null));
    }


//    @Deprecated
//    @GetMapping("apply-amount")
//    public ResponseEntity<?> applyAmount(@RequestParam BigDecimal amount,
//                                      @RequestParam Long idCoupon,
//                                      OAuth2Authentication oAuth2Authentication) {
//        if (oAuth2Authentication == null || oAuth2Authentication.getPrincipal() == null) {
//            throw new ForbiddenException();
//        }
//
//        final String email = oAuth2Authentication.getPrincipal().toString();
//        Buyer buyer = buyerAPIService.get(email);
//
//        BigDecimal usedCoupon = orderAmountService.usedCoupon(amount);
//        log.debug("amount ::: {}", amount);
//        log.debug("usedCoupon ::: {}", usedCoupon);
//        log.debug("amount.subtract(usedCoupon) ::: {}", amount.subtract(usedCoupon));
//        return ResponseEntity.ok(amount.subtract(usedCoupon));
//    }
}
