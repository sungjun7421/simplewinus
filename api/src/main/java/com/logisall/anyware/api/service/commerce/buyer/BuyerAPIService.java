package com.logisall.anyware.api.service.commerce.buyer;

import com.logisall.anyware.core.domain.commerce.buyer.Buyer;

public interface BuyerAPIService {
    // R
    Buyer get(Long id);
    Buyer get(String email);
}
