package com.logisall.anyware.api.service.board.comment;

import com.logisall.anyware.core.domain.board.comment.Comment;
import com.logisall.anyware.core.model.Filter;
import com.logisall.anyware.core.model.reqbody.board.CommentReqBody;
import com.logisall.anyware.core.model.resbody.board.CommentResBody;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;

import java.util.Locale;

public interface CommentAPIService {


    Comment register(Locale locale, CommentReqBody commentReqBody, String email);

    Comment modify(CommentReqBody commentReqBody, Long idComment, String email);

    void delete(Long id, String email);

    PagedModel<?> pagedResources(Locale locale
            , Filter filter
            , Long idUser
            , Comment.Type type
            , Long idContent
            , Long idParent);

    EntityModel<CommentResBody> resource(Locale locale, Long id);
}
