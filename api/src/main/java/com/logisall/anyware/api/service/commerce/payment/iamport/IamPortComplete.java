package com.logisall.anyware.api.service.commerce.payment.iamport;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class IamPortComplete implements Serializable {

    private static final long serialVersionUID = 4071727729644795873L;
    private String imp_uid;
    private String merchant_uid;
}
