package com.logisall.anyware.api.web.account;

import com.logisall.anyware.api.service.account.cert.CertService;
import com.logisall.anyware.api.service.account.cert.CertServiceImpl;
import com.logisall.anyware.core.config.exception.BadRequestException;
import com.logisall.anyware.core.config.exception.ResponseErrorCode;
import com.logisall.anyware.core.domain.user.User;
import com.logisall.anyware.core.domain.user.UserRepository;
import com.logisall.anyware.core.model.AuthentiCodeResBody;
import com.logisall.anyware.core.model.reqbody.account.*;
import com.logisall.anyware.core.model.resbody.ResultResBody;
import com.logisall.anyware.core.service.send.sms.SMSHistoryService;
import com.logisall.anyware.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Tag(name = "Certificate", description = "사용자 모바일/이메일 인증 API")
@Slf4j
@RestController
@RequestMapping("/api/v1/cert")
public class APICertificationController {

  @Value("${app.client.host}")
  private String appHost;

  @Autowired
  private CertService certService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private SMSHistoryService smsHistoryService;

  @Operation(summary = "[account-cert-1] Mobile authentication (회원가입 모바일인증)",
      description = "사용자 회원가입시 모바일 인증")
  @PostMapping("/mobile")
  public ResponseEntity<?> certMobile(@RequestBody CertMobileReqBody cert,
                                      HttpServletRequest request) {

    // 모바일 채킹
    if (StringUtils.isEmpty(cert.getMobile())) {
      throw new BadRequestException(ResponseErrorCode.NOT_ENTERED_MOBILE.getMessage());
    }

    CertReqBody body = new CertReqBody();
    body.setMobile(cert.getMobile());
    body.setMode(CertReqBody.Mode.CERT_MOBILE);

    String code = certService.cert(body, request);

    log.debug("> {} Code ::: {}", body.getMode(), code);

    if (StringUtils.isEmpty(code)) {
      return ResponseEntity.ok(ResultResBody.of(false, ResponseErrorCode.FAILED_GENERATE_VERIFICATION_CODE.getMessage()));
    } else {
      return ResponseEntity.ok(ResultResBody.of(true, "인증번호가 발송되었습니다."));
    }
  }

  @Operation(summary = "[account-cert-2] Email authentication (이메일 모바일인증)",
      description = "Mobile authentication API when user registers. (사용자 회원가입시 모바일 인증)" +
          "\n 이메일 인증 : {\"email\": \"test@logisall.com\",\"mode\": \"CERT_EMAIL\"}")
  @PostMapping("/email")
  public ResponseEntity<?> certEmail(@RequestBody CertEmailReqBody cert,
                                     HttpServletRequest request) {

    // 이메일 채킹
    if (StringUtils.isEmpty(cert.getEmail())) {
      throw new BadRequestException(ResponseErrorCode.NOT_ENTERED_EMAIL.getMessage());
    }

    CertReqBody body = new CertReqBody();
    body.setMode(CertReqBody.Mode.CERT_EMAIL);
    body.setEmail(cert.getEmail());

    String code = certService.cert(body, request);

    log.debug("> {} Code ::: {}", body.getMode(), code);

    if (StringUtils.isEmpty(code)) {
//            throw new ConflictException(ResponseErrorCode.FAILED_GENERATE_VERIFICATION_CODE.name());
      return ResponseEntity.ok(ResultResBody.of(false, ResponseErrorCode.FAILED_GENERATE_VERIFICATION_CODE.getMessage()));
    } else {
      return ResponseEntity.ok(ResultResBody.of(true, "인증번호가 발송되었습니다."));
    }
  }

  @Operation(summary = "[account-cert-3] Check mobile authentication. (회원가입 모바일인증 확인)",
      description = "Check mobile authentication API when user registers. (사용자 회원가입시 모바일 인증)")
  @PostMapping("/mobile/confirm")
  public ResponseEntity<?> confirmCertByMobile(@RequestBody CertConfirmMobileReqBody confirm) {

    if (confirm == null
        || StringUtils.isEmpty(confirm.getCode())) {
      throw new BadRequestException();
    }

    // 모바일 채킹
    if (StringUtils.isEmpty(confirm.getMobile())) {
      throw new BadRequestException();
    }

    CertConfirmReqBody body = new CertConfirmReqBody();
    body.setCode(confirm.getCode());
    body.setMobile(confirm.getMobile());
    body.setMode(CertReqBody.Mode.CERT_MOBILE);

    AuthentiCodeResBody result = certService.confirm(body);

    if (result.isSuccess()) {

      return ResponseEntity.ok(CertServiceImpl.confirmResult(result.getResultCode()));
    } else {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED.value()).body(CertServiceImpl.confirmResult(result.getResultCode()));
    }
  }

  @Operation(summary = "[account-cert-4] Email authentication (회원가입 이메일인증)",
      description = "Email authentication API when user registers. (사용자 회원가입시 이메일 인증)")
  @GetMapping("/email/confirm")
  public ResponseEntity<?> confirmCertByEmail(@Parameter(description = "Auth Code") @RequestParam String code) {

    if (StringUtils.isEmpty(code)) {
      throw new BadRequestException();
    }

    CertConfirmReqBody confirm = new CertConfirmReqBody();
    confirm.setMode(CertReqBody.Mode.CERT_EMAIL);
    confirm.setCode(code);

    AuthentiCodeResBody result = certService.confirm(confirm);
    if (result.isSuccess()) {
      return ResponseEntity.ok("<script>alert('인증이 완료되업습니다.');window.location.href = '" + appHost + "'</script>");
    } else {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED.value()).body(CertServiceImpl.confirmResult(result.getResultCode()));
    }
  }


  @Operation(summary = "[account-cert-5] Resend email authentication. (이메일 인증 재발송)")
  @PostMapping(value = "/resend/email", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> resendEmail(@Parameter(hidden = true) OAuth2Authentication oAuth2Authentication,
                                       HttpServletRequest request) {

    ValidUtils.isForbidden(oAuth2Authentication);
    final String email = oAuth2Authentication.getPrincipal().toString();

    User user = userRepository.findByEmail(email).orElse(null);

    if (user == null) {
      throw new BadRequestException("존재하지 않는 회원입니다.");
    }

    if (user.getVerification() != null && user.getVerification().isEmail()) {
      throw new BadRequestException("이미 인증된 회원입니다.");
    }

    CertReqBody cert = new CertReqBody();
    cert.setEmail(email);
    cert.setMode(CertReqBody.Mode.CERT_EMAIL);
    certService.cert(cert, request);

    return ResponseEntity.ok(ResultResBody.of(true, "인증 이메일이 발송되었습니다."));
  }
}
