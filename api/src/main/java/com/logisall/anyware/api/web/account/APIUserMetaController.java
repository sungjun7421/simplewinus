package com.logisall.anyware.api.web.account;

import com.logisall.anyware.core.domain.document.Document;
import com.logisall.anyware.core.domain.user.Gender;
import com.logisall.anyware.core.model.resbody.BaseStatusResBody;
import com.logisall.anyware.core.utils.StringUtils;
import com.google.common.collect.Lists;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Tag(name = "User Meta", description = "사용자 정보 유형 API")
@RestController
@RequestMapping("/api/v1/users-meta")
public class APIUserMetaController {

  @Operation(summary = "[user-meta-1] Country List (국가 항목)")
  @GetMapping("/countries")
  public ResponseEntity<?> countries() {

    List<Map<String, String>> list = Lists.newArrayList(Locale.getAvailableLocales()).stream()
        .filter(locale -> StringUtils.isNotEmpty(locale.getCountry()))
        .map(locale -> {
          Map<String, String> map = new HashMap<>();
          map.put("code", locale.getCountry().toUpperCase());
          map.put("name", locale.getDisplayCountry());
          return map;
        }).collect(Collectors.toList());

    return ResponseEntity.ok(list);
  }

  @Operation(summary = "[user-meta-2] Gender (남/여)", description = "get gender")
  @GetMapping("/genders")
  public ResponseEntity<?> genders() {

    List<BaseStatusResBody> list = Lists.newArrayList(Gender.values()).stream()
        .map(type -> {
          BaseStatusResBody baseStatusResBody = new BaseStatusResBody();
          baseStatusResBody.setKey(type.toString());
          baseStatusResBody.setValue(type.getValue());
          return baseStatusResBody;
        }).collect(Collectors.toList());

    return ResponseEntity.ok(list);
  }
}
