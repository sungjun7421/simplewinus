package com.logisall.anyware.api.service.commerce.order;

import com.logisall.anyware.core.domain.commerce.OrderStatus;
import com.logisall.anyware.core.domain.commerce.order.Order;
import com.logisall.anyware.core.model.reqbody.commerce.order.OrderReqBody;
import com.logisall.anyware.core.model.resbody.commerce.order.OrderDetailResBody;

import java.util.List;
import java.util.Locale;

public interface OrderAPIService {


    Order order(OrderReqBody orderReqBody, String email);
    String generateOid(Long idBuyer);
    OrderDetailResBody getDetailOrder(Long id, Locale locale);

    Object cancelOrder(String oid);

    List<OrderStatus> convertOrderStatus(String orderStatuses);

//    PagedModel<OrderDetailResBody> getAllOrderDetailByUser(String email, List<OrderStatus> orderStatusList, Filter filter, Locale locale, Long idStore);

}
